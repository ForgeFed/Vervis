#############################################################################
## build
#############################################################################

FROM debian:bookworm AS builder

RUN apt-get -yq update && apt-get -yq install \
    libpq-dev zlib1g-dev libssl-dev libpcre3-dev libgmp-dev \
    darcs git \
    postgresql libpq-dev \
    locales locales-all \
    build-essential \
    ca-certificates \
    curl \
    liblapack-dev \
    liblzma-dev \
    libyaml-dev \
    netbase \
    openssh-client \
    pkg-config

RUN curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org -o /install-ghcup.sh && \
    BOOTSTRAP_HASKELL_MINIMAL=1 sh /install-ghcup.sh && \
    rm /install-ghcup.sh

RUN locale-gen en_US.UTF-8
ENV LANG=en_US.UTF-8
ENV LANGUAGE=en_US:en
ENV LC_ALL=en_US.UTF-8

ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/root/.ghcup/bin

RUN ghcup install ghc 9.6.6
RUN ghcup install stack

RUN mkdir /build \
          /build/.git \
          /build/app \
          /build/embed \
          /build/hook-darcs \
          /build/hook-git \
          /build/migrations \
          /build/src \
          /build/static \
          /build/templates \
          /build/th
COPY stack.yaml vervis.cabal COPYING.AGPL3 /build/
COPY .git                    /build/.git
COPY app                     /build/app
COPY embed                   /build/embed
COPY hook-darcs              /build/hook-darcs
COPY hook-git                /build/hook-git
COPY migrations              /build/migrations
COPY src                     /build/src
COPY static                  /build/static
COPY templates               /build/templates
COPY th                      /build/th

WORKDIR /build

RUN mkdir /build/lib && \
    darcs clone https://vervis.peers.community/repos/6r4Ao /build/lib/ssh

RUN stack build --flag vervis:-dev --only-dependencies

RUN ls -lh /build

RUN stack build --flag vervis:-dev

RUN mkdir -p /build/artifacts && \
    cp `stack exec which vervis`              /build/artifacts/ && \
    cp `stack exec which vervis-post-receive` /build/artifacts/ && \
    cp `stack exec which vervis-post-apply`   /build/artifacts/

RUN ls -lh /build/artifacts

#############################################################################
## deployment
#############################################################################

FROM debian:bookworm

# Linux UID (user id) for the vervis user, change with [--build-arg UID=1234]
ARG UID="981"
# Linux GID (group id) for the vervis user, change with [--build-arg GID=1234]
ARG GID="981"

RUN apt-get -yq update && apt-get -yq install \
    ca-certificates \
    curl \
    libgmp10 \
    liblapack3 \
    liblzma5 \
    libpq5 \
    libssl3 \
    libyaml-0-2 \
    netbase \
    openssh-client \
    zlib1g \
    libpcre3 darcs git \
    locales locales-all \
  && rm -rf /var/lib/apt/lists/*

RUN locale-gen en_US.UTF-8
ENV \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US:en \
    LC_ALL=en_US.UTF-8 \
    TZ=Etc/UTC

RUN groupadd -g "${GID}" vervis && \
    useradd -l -u "${UID}" -g vervis -m -d /app vervis

WORKDIR /app

EXPOSE 3000
EXPOSE 5022

RUN mkdir /app/static /app/log /app/config
COPY --from=builder /build/artifacts/* /app/

## The next 3 lines, which prepare the state dir and SSH key, aren't needed
## when using docker-compose, because it prepares and sets its own ./state
## volume.
##
## Probably this applies to COPYing config as well, and the VOLUME lines.
##
## But we keep these lines in case someone uses this Dockerfile without
## docker-compose.
RUN mkdir /app/state /app/state/repos /app/state/deliveries && \
    ssh-keygen -t rsa -m PEM -f /app/state/ssh-host-key && \
    chown vervis:vervis /app/state && \
    chown vervis:vervis /app/static /app/log
COPY config /app/config

VOLUME /app/config
VOLUME /app/state

RUN ls /app
USER vervis
ENTRYPOINT /app/vervis
