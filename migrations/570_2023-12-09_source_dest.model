------------------------------------------------------------------------------
-- Inheritance - Receiver tracking her givers
-- (Project tracking its children)
-- (Team tracking its parents)
------------------------------------------------------------------------------

Source
    role Role

SourceHolderProject
    source  SourceId
    project ProjectId

    UniqueSourceHolderProject source

SourceHolderGroup
    source SourceId
    group  GroupId

    UniqueSourceHolderGroup source

-------------------------------- Source topic --------------------------------

SourceTopicLocal
    source SourceId

    UniqueSourceTopicLocal source

SourceTopicProject
    holder SourceHolderProjectId
    topic  SourceTopicLocalId
    child  ProjectId

    UniqueSourceTopicProject      holder
    UniqueSourceTopicProjectTopic topic

SourceTopicGroup
    holder SourceHolderGroupId
    topic  SourceTopicLocalId
    parent GroupId

    UniqueSourceTopicGroup      holder
    UniqueSourceTopicGroupTopic topic

SourceTopicRemote
    source SourceId
    topic  RemoteActorId

    UniqueSourceTopicRemote source

-------------------------------- Source flow ---------------------------------

SourceOriginUs
    source SourceId

    UniqueSourceOriginUs source

SourceOriginThem
    source SourceId

    UniqueSourceOriginThem source

-- Our collaborator's gesture
--
-- OriginUs: The Add that started the sequence
-- OriginThem: N/A (they send their Accept but we don't record it)

SourceUsGestureLocal
    us  SourceOriginUsId
    add OutboxItemId

    UniqueSourceUsGestureLocal    us
    UniqueSourceUsGestureLocalAdd add

SourceUsGestureRemote
    us    SourceOriginUsId
    actor RemoteActorId
    add   RemoteActivityId

    UniqueSourceUsGestureRemote    us
    UniqueSourceUsGestureRemoteAdd add

-- Our accept
--
-- OriginUs: I checked the Add and sending my Accept
-- OriginThem: N/A

SourceUsAccept
    us    SourceOriginUsId
    accept OutboxItemId

    UniqueSourceUsAccept       us
    UniqueSourceUsAcceptAccept accept

-- Their collaborator's gesture
--
-- OriginUs: N/A (they send it but we don't record it)
-- OriginThem: The Add that started the sequence

SourceThemGestureLocal
    them SourceOriginThemId
    add  OutboxItemId

    UniqueSourceThemGestureLocal    them
    UniqueSourceThemGestureLocalAdd add

SourceThemGestureRemote
    them  SourceOriginThemId
    actor RemoteActorId
    add   RemoteActivityId

    UniqueSourceThemGestureRemote    them
    UniqueSourceThemGestureRemoteAdd add

-- Their accept
--
-- OriginUs: Seeing our accept and their collaborator's accept, they send their
--           own accept
-- OriginThem: Checking the Add, they send their Accept

SourceThemAcceptLocal
    topic  SourceTopicLocalId
    accept OutboxItemId

    UniqueSourceThemAcceptLocal       topic
    UniqueSourceThemAcceptLocalAccept accept

SourceThemAcceptRemote
    topic  SourceTopicRemoteId
    accept RemoteActivityId

    UniqueSourceThemAcceptRemote       topic
    UniqueSourceThemAcceptRemoteAccept accept

-------------------------------- Source enable -------------------------------

-- Witnesses that, seeing their approval and our collaborator's gesture, I've
-- sent then a delegator-Grant and now officially considering them a source of
-- us
SourceUsSendDelegator
    source SourceId
    grant  OutboxItemId

    UniqueSourceUsSendDelegator      source
    UniqueSourceUsSendDelegatorGrant grant

-- Witnesses that, using the delegator-Grant, they sent us a start-Grant or
-- extension-Grant to delegate further

SourceThemDelegateLocal
    source SourceThemAcceptLocalId
    grant  OutboxItemId

    UniqueSourceThemDelegateLocal      source
    UniqueSourceThemDelegateLocalGrant grant

SourceThemDelegateRemote
    source SourceThemAcceptRemoteId
    grant  RemoteActivityId

    UniqueSourceThemDelegateRemote      source
    UniqueSourceThemDelegateRemoteGrant grant

-- Witnesses that, seeing the delegation from them, I've sent an
-- extension-Grant to a Dest of mine

SourceUsGather
    source SourceUsSendDelegatorId
    dest   DestUsAcceptId
    grant  OutboxItemId

SourceUsGatherFromLocal
    gather SourceUsGatherId
    from   SourceThemDelegateLocalId

    UniqueSourceUsGatherFromLocal gather

SourceUsGatherFromRemote
    gather SourceUsGatherId
    from   SourceThemDelegateRemoteId

    UniqueSourceUsGatherFromRemote gather

SourceUsGatherToLocal
    gather SourceUsGatherId
    to     DestThemSendDelegatorLocalId

    UniqueSourceUsGatherToLocal gather

SourceUsGatherToRemote
    gather SourceUsGatherId
    to     DestThemSendDelegatorRemoteId

    UniqueSourceUsGatherToRemote gather

-- Witnesses that, seeing the delegation from them, I've sent a leaf-Grant to a
-- direct-collaborator of mine

SourceUsLeaf
    source SourceUsSendDelegatorId
    collab CollabEnableId
    grant  OutboxItemId

SourceUsLeafFromLocal
    leaf SourceUsLeafId
    from SourceThemDelegateLocalId

    UniqueSourceUsLeafFromLocal leaf

SourceUsLeafFromRemote
    leaf SourceUsLeafId
    from SourceThemDelegateRemoteId

    UniqueSourceUsLeafFromRemote leaf

SourceUsLeafToLocal
    leaf SourceUsLeafId
    to   CollabDelegLocalId

    UniqueSourceUsLeafToLocal leaf

SourceUsLeafToRemote
    leaf SourceUsLeafId
    to   CollabDelegRemoteId

    UniqueSourceUsLeafToRemote leaf

------------------------------------------------------------------------------
-- Inheritance - Giver tracking her receivers
-- (Project tracking its parents)
-- (Team tracking its children)
------------------------------------------------------------------------------

Dest
    role Role

DestHolderProject
    dest    DestId
    project ProjectId

    UniqueDestHolderProject dest

DestHolderGroup
    dest  DestId
    group GroupId

    UniqueDestHolderGroup dest

---------------------------------- Dest topic --------------------------------

DestTopicLocal
    dest DestId

    UniqueDestTopicLocal dest

DestTopicProject
    holder DestHolderProjectId
    topic  DestTopicLocalId
    parent ProjectId

    UniqueDestTopicProject      holder
    UniqueDestTopicProjectTopic topic

DestTopicGroup
    holder DestHolderGroupId
    topic  DestTopicLocalId
    child  GroupId

    UniqueDestTopicGroup      holder
    UniqueDestTopicGroupTopic topic

DestTopicRemote
    dest  DestId
    topic RemoteActorId

    UniqueDestTopicRemote dest

---------------------------------- Dest flow ---------------------------------

DestOriginUs
    dest DestId

    UniqueDestOriginUs dest

DestOriginThem
    dest DestId

    UniqueDestOriginThem dest

-- Our collaborator's gesture
--
-- OriginUs: The Add that started the sequence
-- OriginThem: Seeing the Add and their Accept, my collaborator has sent her
--             Accept

DestUsGestureLocal
    dest     DestId
    activity OutboxItemId

    UniqueDestUsGestureLocal         dest
    UniqueDestUsGestureLocalActivity activity

DestUsGestureRemote
    dest     DestId
    actor    RemoteActorId
    activity RemoteActivityId

    UniqueDestUsGestureRemote         dest
    UniqueDestUsGestureRemoteActivity activity

-- Our accept
--
-- OriginUs: Checking my collaborator's Add, I sent my Accept
-- OriginThem: Seeing the Add, their Accept and my collaborator's Accept, I
--             sent my Accept

DestUsAccept
    dest DestId
    accept OutboxItemId

    UniqueDestUsAccept       dest
    UniqueDestUsAcceptAccept accept

-- Their collaborator's gesture
--
-- OriginUs: N/A (they send it but we don't record it)
-- OriginThem: The Add that started the sequence

DestThemGestureLocal
    them DestOriginThemId
    add  OutboxItemId

    UniqueDestThemGestureLocal    them
    UniqueDestThemGestureLocalAdd add

DestThemGestureRemote
    them  DestOriginThemId
    actor RemoteActorId
    add   RemoteActivityId

    UniqueDestThemGestureRemote    them
    UniqueDestThemGestureRemoteAdd add

-- Their accept
--
-- OriginUs: N/A
-- OriginThem: Seeing their collaborator's Add, they sent an Accept

DestThemAcceptLocal
    them   DestOriginThemId
    topic  DestTopicLocalId
    accept OutboxItemId

    UniqueDestThemAcceptLocal       them
    UniqueDestThemAcceptLocalTopic  topic
    UniqueDestThemAcceptLocalAccept accept

DestThemAcceptRemote
    them   DestOriginThemId
    topic  DestTopicRemoteId
    accept RemoteActivityId

    UniqueDestThemAcceptRemote       them
    UniqueDestThemAcceptRemoteTopic  topic
    UniqueDestThemAcceptRemoteAccept accept

---------------------------------- Dest enable -------------------------------

-- Witnesses that, seeing our approval and their collaborator's gesture,
-- they've sent us a delegator-Grant, and we now officially consider them a
-- dest of us

DestThemSendDelegatorLocal
    dest  DestUsAcceptId
    topic DestTopicLocalId
    grant OutboxItemId

    UniqueDestThemSendDelegatorLocal      dest
    UniqueDestThemSendDelegatorLocalTopic topic
    UniqueDestThemSendDelegatorLocalGrant grant

DestThemSendDelegatorRemote
    dest  DestUsAcceptId
    topic DestTopicRemoteId
    grant RemoteActivityId

    UniqueDestThemSendDelegatorRemote      dest
    UniqueDestThemSendDelegatorRemoteTopic topic
    UniqueDestThemSendDelegatorRemoteGrant grant
