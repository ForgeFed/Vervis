Repo
Deck
Loom
Project
Group
RemoteActor
RemoteActivity
Inbox
FollowerSet

Outbox

OutboxItem
    outbox    OutboxId
    activity  PersistJSONObject
    published UTCTime

Actor
    name      Text
    desc      Text
    createdAt UTCTime
    inbox     InboxId
    outbox    OutboxId
    followers FollowerSetId
    justCreatedBy ActorId Maybe

    UniqueActorInbox     inbox
    UniqueActorOutbox    outbox
    UniqueActorFollowers followers

Person
    username            Username
    login               Text
    passphraseHash      ByteString
    email               EmailAddress
    verified            Bool
    verifiedKey         Text
    verifiedKeyCreated  UTCTime
    resetPassKey        Text
    resetPassKeyCreated UTCTime
    actor               ActorId
--  reviewFollow        Bool

    UniquePersonUsername username
    UniquePersonLogin    login
    UniquePersonEmail    email
    UniquePersonActor    actor

Collab
    role Role

CollabFulfillsLocalTopicCreation
    collab CollabId

    UniqueCollabFulfillsLocalTopicCreation collab

CollabFulfillsInvite
    collab CollabId
    accept OutboxItemId

    UniqueCollabFulfillsInvite       collab
    UniqueCollabFulfillsInviteAccept accept

CollabInviterLocal
    collab CollabFulfillsInviteId
    invite OutboxItemId

    UniqueCollabInviterLocal       collab
    UniqueCollabInviterLocalInvite invite

CollabInviterRemote
    collab CollabFulfillsInviteId
    actor  RemoteActorId
    invite RemoteActivityId

    UniqueCollabInviterRemote       collab
    UniqueCollabInviterRemoteInvite invite

CollabFulfillsJoin
    collab CollabId

    UniqueCollabFulfillsJoin collab

CollabApproverLocal
    collab CollabFulfillsJoinId
    accept OutboxItemId

    UniqueCollabApproverLocal       collab
    UniqueCollabApproverLocalAccept accept

CollabApproverRemote
    collab CollabFulfillsJoinId
    actor  RemoteActorId
    accept RemoteActivityId

    UniqueCollabApproverRemote       collab
    UniqueCollabApproverRemoteAccept accept

CollabRecipLocalJoin
    collab   CollabRecipLocalId
    fulfills CollabFulfillsJoinId
    join     OutboxItemId

    UniqueCollabRecipLocalJoinCollab   collab
    UniqueCollabRecipLocalJoinFulfills fulfills
    UniqueCollabRecipLocalJoinJoin     join

CollabTopicRepo
    collab CollabId
    repo   RepoId

    UniqueCollabTopicRepo collab

CollabTopicDeck
    collab CollabId
    deck   DeckId

    UniqueCollabTopicDeck collab

CollabTopicLoom
    collab CollabId
    loom   LoomId

    UniqueCollabTopicLoom collab

CollabTopicProject
    collab  CollabId
    project ProjectId

    UniqueCollabTopicProject collab

CollabTopicGroup
    collab CollabId
    group  GroupId

    UniqueCollabTopicGroup collab

CollabRecipLocal
    collab CollabId
    person PersonId

    UniqueCollabRecipLocal collab

CollabRecipLocalAccept
    collab CollabRecipLocalId
    invite CollabFulfillsInviteId
    accept OutboxItemId

    UniqueCollabRecipLocalAcceptCollab collab
    UniqueCollabRecipLocalAcceptInvite invite
    UniqueCollabRecipLocalAcceptAccept accept

CollabEnable
    collab CollabId
    grant  OutboxItemId

    UniqueCollabEnable      collab
    UniqueCollabEnableGrant grant

CollabDelegLocal
    enable CollabEnableId
    recip  CollabRecipLocalId
    grant  OutboxItemId

    UniqueCollabDelegLocal      enable
    UniqueCollabDelegLocalRecip recip
    UniqueCollabDelegLocalGrant grant

Permit
    person PersonId
    role   Role

PermitTopicLocal
    permit PermitId

    UniquePermitTopicLocal permit

PermitTopicRepo
    permit PermitTopicLocalId
    repo   RepoId

    UniquePermitTopicRepo permit

PermitTopicDeck
    permit PermitTopicLocalId
    deck   DeckId

    UniquePermitTopicDeck permit

PermitTopicLoom
    permit PermitTopicLocalId
    loom   LoomId

    UniquePermitTopicLoom permit

PermitTopicProject
    permit  PermitTopicLocalId
    project ProjectId

    UniquePermitTopicProject permit

PermitTopicGroup
    permit PermitTopicLocalId
    group  GroupId

    UniquePermitTopicGroup permit

PermitTopicRemote
    permit PermitId
    actor  RemoteActorId

    UniquePermitTopicRemote permit

PermitFulfillsTopicCreation
    permit PermitId

    UniquePermitFulfillsTopicCreation permit

PermitFulfillsInvite
    permit PermitId

    UniquePermitFulfillsInvite permit

PermitFulfillsJoin
    permit PermitId

    UniquePermitFulfillsJoin permit

PermitPersonGesture
    permit   PermitId
    activity OutboxItemId

    UniquePermitPersonGesture         permit
    UniquePermitPersonGestureActivity activity

PermitTopicGestureLocal
    fulfills PermitFulfillsInviteId
    invite   OutboxItemId

    UniquePermitTopicGestureLocal       fulfills
    UniquePermitTopicGestureLocalInvite invite

PermitTopicGestureRemote
    fulfills PermitFulfillsInviteId
    actor    RemoteActorId
    invite   RemoteActivityId

    UniquePermitTopicGestureRemote       fulfills
    UniquePermitTopicGestureRemoteInvite invite

PermitTopicAcceptLocal
    fulfills PermitFulfillsInviteId
    topic    PermitTopicLocalId
    accept   OutboxItemId

    UniquePermitTopicAcceptLocal       fulfills
    UniquePermitTopicAcceptLocalTopic  topic
    UniquePermitTopicAcceptLocalAccept accept

PermitTopicEnableLocal
    permit PermitPersonGestureId
    topic  PermitTopicLocalId
    grant  OutboxItemId

    UniquePermitTopicEnableLocal      permit
    UniquePermitTopicEnableLocalTopic topic
    UniquePermitTopicEnableLocalGrant grant

PermitPersonSendDelegator
    permit PermitPersonGestureId
    grant  OutboxItemId

    UniquePermitPersonSendDelegator      permit
    UniquePermitPersonSendDelegatorGrant grant

PermitTopicExtendLocal
    permit PermitPersonSendDelegatorId
    topic  PermitTopicEnableLocalId
    grant  OutboxItemId

    UniquePermitTopicExtendLocalGrant grant

Component
    project ProjectId
    role    Role

ComponentEnable
    component ComponentId
    grant     OutboxItemId

    UniqueComponentEnable      component
    UniqueComponentEnableGrant grant

ComponentFurtherLocal
    component ComponentEnableId
    collab    CollabDelegLocalId
    grant     OutboxItemId

    UniqueComponentFurtherLocal      component collab
    UniqueComponentFurtherLocalGrant grant
