{- This file is part of Vervis.
 -
 - Written in 2022 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Yesod.Form.Local
    ( runFormPostRedirect
    )
where

import Yesod.Core.Handler
import Yesod.Form

runFormPostRedirect here form = do
    ((result, _), _) <- runFormPost form
    case result of
        FormMissing -> do
            setMessage "Field(s) missing"
            redirect here
        FormFailure _l -> do
            setMessage "Operation failed, see below"
            redirect here
        FormSuccess v -> return v
