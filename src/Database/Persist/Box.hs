{- This file is part of Vervis.
 -
 - Written in 2023 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

-- | Example:
--
-- @
-- import Database.Persist.Box
-- import System.OsPath
--
-- data Person = Person
--     { personName :: Text
--     , personAge  :: Int
--     }
--     deriving Show
--     deriving 'Boxable' via ('BoxableShow' Person)
--
-- main :: IO ()
-- main = do
--     path <- decodeUtf "mydb.box"
--     alice <- 'loadBox' path $ Person "Alice" 50
--     'withBox' alice $ do
--         Person _name age <- 'obtain'
--         'bestow' $ Person "Alicia" (age + 1)
-- @
--
-- Notes:
--
-- * A 'Box' is meant to be used from a single thread! However, you can have
--   multiple threads with read-only access, see 'createBoxView'
-- * Instead of passing around the 'Box' and using 'withBox' to access it, you
--   can implement a 'MonadBox' instance for your monad and use 'runBox' to
--   access the box
-- * 'BoxableShow' is just one of several serialization methods
-- * Migrations not supported yet
module Database.Persist.Box
    ( -- * TH
      model
    , modelFile
    , makeBox

      -- * Making types boxable
    , BoxPersistT ()
    , Boxable ()
    , BoxableFormat ()
    , BoxableVia (..)
    , BoxableRecord ()
    , BoxableField ()
    , BoxableShow ()
    , BoxableJSON ()
    , BoxableSerialize ()

      -- * Box access
    , Box ()
    --, MigrationRecipes
    , loadBox
    , withBox
    , MonadBox (..)
    , runBox
    , bestow
    , obtain

      -- * Box viewer pool
    , BoxView ()
    , createBoxView
    , viewBox
    )
where

import Database.Persist.Box.Internal
import Database.Persist.Box.Via
