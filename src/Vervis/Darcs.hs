{- This file is part of Vervis.
 -
 - Written in 2016, 2018, 2019, 2020, 2022, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Darcs
    ( readSourceView
    --, readWikiView
    , readChangesView
    --, lastChange
    , readPatch
    , writePostApplyHooks
    , canApplyDarcsPatch
    , applyDarcsPatch
    )
where

import Prelude hiding (lookup)

import Control.Applicative ((<|>))
import Control.Exception.Base
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Except
import Data.Bifunctor
import Data.Bool (bool)
import Data.ByteString (ByteString)
import Data.Foldable hiding (find)
import Data.List.NonEmpty (NonEmpty (..), nonEmpty)
import Data.Maybe (listToMaybe, mapMaybe, fromMaybe)
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8, decodeUtf8With, decodeUtf8)
import Data.Text.Encoding.Error (strictDecode)
import Data.Time.Clock (UTCTime, getCurrentTime, diffUTCTime)
import Data.Traversable (for)
import Database.Persist
import System.Exit
import System.FilePath ((</>))
import System.Process.Typed
import Text.Email.Validate (emailAddress)

import qualified Data.Attoparsec.Text as A
import qualified Data.Attoparsec.ByteString.Char8 as AB
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import qualified Data.ByteString.Base16 as B16 (encode, decode)
import qualified Data.Foldable as F (find)
import qualified Data.List.NonEmpty as NE
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import qualified Data.Vector as V (empty)
import qualified Database.Esqueleto as E

import Data.ObjId
import Development.Darcs
import Development.PatchMediaType
import Network.FedURI
import Yesod.ActivityPub
import Yesod.Hashids
import Yesod.MonadSite

import qualified Data.VersionControl as VC

import Data.Either.Local (maybeRight)
import Data.EventTime.Local
import Data.List.Local
import Data.List.NonEmpty.Local
import Data.Patch.Local hiding (Patch)
import Data.Text.UTF8.Local (decodeStrict)
import Data.Time.Clock.Local ()
import System.Process.Typed.Local

import qualified Data.Patch.Local as P
import qualified Data.Text.UTF8.Local as TU

import Vervis.Changes
import Vervis.Foundation
import Vervis.Model
import Vervis.Model.Ident
import Vervis.Path
import Vervis.Readme
import Vervis.Settings
import Vervis.SourceTree

findReadme :: Text -> FilePath -> DirTree -> DarcsT IO (Maybe (Text, Text))
findReadme patch dirPath (DirTree _ files) =
    for (F.find (isReadme . T.pack) files) $ \ name -> do
        body <- darcsGetFileContent patch $ dirPath </> name
        return (T.pack name, body)

readSourceView
    :: [EntryName]
    -- ^ Path in the source tree pointing to a file or directory
    -> DarcsT IO (Maybe (SourceView Widget))
readSourceView dir = do
    let invalid t = T.null t || t == "." || t == ".." || T.any (== '/') t
    when (any invalid dir) $
        error $ "readSourceView invalid dir: " ++ show dir
    hash <- darcsGetHead
    top <- darcsGetTree hash
    msv <- for (lookupTreeItem (map T.unpack dir) top) $ \case
        Left () -> do
            let dir' = T.unpack $ T.intercalate "/" dir
            body <- darcsGetFileContent hash dir'
            return $ SourceFile $ FileView (last dir) body
        Right tree@(DirTree subdirs files) -> do
            let dir' = T.unpack $ T.intercalate "/" dir
            mreadme <- findReadme hash dir' tree
            let mname =
                    if null dir
                        then Nothing
                        else Just $ last dir
                ents =
                    map (DirEntry TypeTree . T.pack . fst) subdirs ++
                    map (DirEntry TypeBlob . T.pack) files
            return $ SourceDir $ DirectoryView mname ents mreadme
    return $ renderSources dir <$> msv

{-
readWikiView
    :: (EntryName -> EntryName -> Maybe Text)
    -- ^ Page name predicate. Returns 'Nothing' for a file which isn't a page.
    -- For a page file, returns 'Just' the page name, which is the filename
    -- with some parts possibly removed or added. For example, you may wish to
    -- remove any extensions, replace underscores with spaces and so on.
    -> (EntryName -> Bool)
    -- ^ Main page predicate. This is used to pick a top-level page to display
    -- as the wiki root page.
    -> FilePath
    -- ^ Repository path.
    -> [EntryName]
    -- ^ Path in the source tree pointing to a file. The last component doesn't
    -- have to be the full name of the file though, but it much match the page
    -- predicate for the actual file to be found.
    -> IO (Maybe WikiView)
readWikiView isPage isMain path dir = do
    stubbedTree <- readStubbedTree path
    let (parent, ispage, mfile) =
            if null dir
                then
                    ( []
                    , bool Nothing (Just Nothing) . isMain
                    , Nothing
                    )
                else
                    ( init dir
                    , maybe Nothing (Just . Just) . isPage lst
                    , Just $ decodeWhiteName $ encodeUtf8 lst
                    )
                    where
                    lst = last dir
        anch = dirToAnchoredPath parent
        matchBlob f (n, (File (Blob load _))) = f (nameToText n) load
        matchBlob _ _                         = Nothing
        matchBlob' f (File (Blob load _)) = Just $ f load
        matchBlob' _ _                    = Nothing
        page name load = (,) load . Just <$> ispage name
        matchP = listToMaybe . mapMaybe (matchBlob page) . listImmediate
        matchF t = mfile >>= lookup t >>= matchBlob' (flip (,) Nothing)
    expandedTree <- expandPath stubbedTree anch
    let mpage = case find expandedTree anch of
            Nothing             -> Nothing
            Just (File _)       -> Nothing
            Just (Stub _ _)     -> error "supposed to be expanded"
            Just (SubTree tree) -> matchP tree <|> matchF tree
        mkview Nothing b   = WikiViewRaw b
        mkview (Just mt) b = WikiViewPage mt b
    for mpage $ \ (load, mmtitle) -> mkview mmtitle <$> load
-}

readChangesView
    :: MonadIO m
    => Int
    -- ^ Offset, i.e. latest patches to skip
    -> Int
    -- ^ Limit, i.e. how many latest patches to take after the offset
    -> DarcsT m (Maybe (Int, [LogEntry]))
    -- ^ Total number of changes, and view of the chosen subset
readChangesView off lim = fmap maybeRight $ runExceptT $ lift $ do
    cs <- darcsLog (Just lim) (Just off)
    total <- darcsLogLength
    now <- liftIO getCurrentTime
    let toLE c = LogEntry
            { leAuthor  = VC.authorName $ fst $ VC.commitWritten c
            , leHash    = VC.commitHash c
            , leMessage = VC.commitTitle c
            , leTime    =
                ( snd $ VC.commitWritten c
                , intervalToEventTime $
                  FriendlyConvert $
                  now `diffUTCTime` snd (VC.commitWritten c)
                )
            }
    return (total, map toLE cs)

{-
lastChange :: FilePath -> UTCTime -> IO (Maybe EventTime)
lastChange path now = fmap maybeRight $ runExceptT $ do
    total <- ExceptT $ readLatestInventory path latestInventorySizeP
    let lim = 1
        off = total - lim
    (_, l) <- ExceptT $ readLatestInventory path $ latestInventoryPageP off lim
    return $ case reverse l of
        []                 -> Never
        (pi, _ih, _ch) : _ ->
            intervalToEventTime $
            FriendlyConvert $
            now `diffUTCTime` piTime pi
-}

{-
data Change
    = AddFile FilePath
    | AddDir FilePath
    | Move FilePath FilePath
    | RemoveFile FilePath
    | RemoveDir FilePath
    | Replace FilePath Text Text Text
    | Binary FilePath ByteString ByteString
    | Pref Text Text Text

splitChange :: P.Change -> Either P.Hunk Change
splitChange = f
    where
    text = decodeUtf8
    path = T.unpack . text
    f (P.EditFile h)          = Left h
    f (P.AddFile p)           = Right $ AddFile (path p)
    f (P.AddDir p)            = Right $ AddDir (path p)
    f (P.Move old new)        = Right $ Move (path old) (path new)
    f (P.RemoveFile p)        = Right $ RemoveFile (path p)
    f (P.RemoveDir p)         = Right $ RemoveDir (path p)
    f (P.Replace p r old new) = Right $ Replace (path p) (text r) (text old) (text new)
    f (P.Binary p old new)    = Right $ Binary (path p) old new
    f (P.Pref pref old new)   = Right $ Pref (text pref) (text old) (text new)

-- | Group hunks by filename, assuming all the hunks for a given file are
-- placed together in the patch file, and in increasing line number order.
groupHunksByFile
    :: NonEmpty (P.Hunk)
    -> NonEmpty (ByteString, NonEmpty (Int, [ByteString], [ByteString]))
groupHunksByFile = groupWithExtract1 P.hunkFile rest
    where
    rest h = (P.hunkLine h, P.hunkRemove h, P.hunkAdd h)

-- | Find groups of consecutive sequences of removes and adds, and turn each
-- such group into a single hunk. This may not actually be necessary, because
-- the small consecutive hunks would be joined later anyway when adding context
-- lines, but I'm still doing this here for consistency. In the "Vervis.Git"
-- module, the hunks are joined like this too.
joinHunks
    :: NonEmpty (Int, [ByteString], [ByteString])
    -> NonEmpty (Bool, Int, Hunk)
joinHunks =
    NE.map (mkHunk . second groupPairs) .
    groupMapBy1 consecutive lineNumber lines
  where
    consecutive (n1, r1, _) (n2, _, _) = n1 + length r1 == n2
    lineNumber (n, _, _) = n
    lines (_, rs, as) = (map decodeUtf8 rs, map decodeUtf8 as)
    mkHunk (line, (adds, pairs, rems)) = (False, line, Hunk adds pairs rems)
-}

-- Copied from Vervis.Git, perhaps move to a common module?
patch :: Text -> VC.Commit -> P.Patch
patch edits (VC.Commit a c _ t d) = P.Patch (mk a) (mk <$> c) t d edits
    where
    mk = first mk'
    mk' (VC.Author n e) = P.Author n e

readPatch :: ObjId -> MonadIO m => DarcsT m P.Patch
readPatch oid = do
    commit <- darcsShowCommit oid
    deltas <- darcsDiff oid
    return $ patch deltas commit

writePostApplyHooks :: WorkerDB ()
writePostApplyHooks = do
    hook <- asksSite $ appPostApplyHookFile . appSettings
    authority <- asksSite $ renderAuthority . siteInstanceHost
    repos <- selectKeysList [RepoVcs ==. VCSDarcs] []
    for_ repos $ \ repoID -> do
        repoHash <- encodeKeyHashid repoID
        path <- askRepoDir repoHash
        liftIO $
            writeDefaultsFile path hook authority (keyHashidText repoHash)

canApplyDarcsPatch repoPath patch = do
    let input = BL.fromStrict $ TE.encodeUtf8 patch
    exitCode <- runProcess $ setStdin (byteStringInput input) $ proc "darcs" ["apply", "--all", "--no-allow-conflicts", "--dry-run", "--repodir='" ++ repoPath ++ "'"]
    return $ exitCode == ExitSuccess

applyDarcsPatch repoPath patch = do
    let input = BL.fromStrict $ TE.encodeUtf8 patch
    runProcessE "darcs apply" $ setStdin (byteStringInput input) $ proc "darcs" ["apply", "--all", "--no-allow-conflicts", "--repodir='" ++ repoPath ++ "'"]
