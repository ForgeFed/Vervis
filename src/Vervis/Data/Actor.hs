{- This file is part of Vervis.
 -
 - Written in 2022, 2023, 2024 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Data.Actor
    ( parseLocalActivityURI
    , parseLocalActivityURI'
    , parseActivityURI
    , parseActivityURI'
    , activityRoute
    , stampRoute
    , parseStampRoute
    , grabLocalActorID
    , grabLocalResourceID
    , localResourceID
    , WA.parseLocalURI
    , parseFedURIOld
    , parseLocalActorE
    , parseLocalActorE'
    , parseLocalResourceE
    , parseLocalResourceE'
    , parseActorURI
    , parseActorURI'
    , parseResourceURI
    , parseResourceURI'
    , renderActivityURI
    , renderActivityURI'
    )
where

import Control.Concurrent.Chan
import Control.Concurrent.STM.TVar
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Except
import Data.Bitraversable
import Data.Text (Text)
import Database.Persist.Sql
import Database.Persist.Types
import UnliftIO.Exception (try, SomeException, displayException)

import qualified Data.HashMap.Strict as HM
import qualified Data.Text as T

import Network.FedURI
import Yesod.ActivityPub
import Yesod.Actor
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite

import qualified Web.Actor as WA
import qualified Web.Actor.Persist as WAP

import Control.Monad.Trans.Except.Local

import Vervis.FedURI
import Vervis.Foundation
import Vervis.Model
import Vervis.Recipient

import qualified Vervis.Actor as VA

parseOutboxItemRoute (PersonOutboxItemR p i) = Just (LocalActorPerson p, i)
parseOutboxItemRoute (GroupOutboxItemR g i)  = Just (LocalActorGroup g, i)
parseOutboxItemRoute (RepoOutboxItemR r i)   = Just (LocalActorRepo r, i)
parseOutboxItemRoute (DeckOutboxItemR d i)   = Just (LocalActorDeck d, i)
parseOutboxItemRoute (LoomOutboxItemR l i)   = Just (LocalActorLoom l, i)
parseOutboxItemRoute (ProjectOutboxItemR r i)   = Just (LocalActorProject r, i)
parseOutboxItemRoute (FactoryOutboxItemR r i)   = Just (LocalActorFactory r, i)
parseOutboxItemRoute _                       = Nothing

parseLocalActivityURI
    :: (MonadSite m, YesodHashids (SiteEnv m))
    => LocalURI
    -> ExceptT Text m (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
parseLocalActivityURI luAct = do
    route <- fromMaybeE (decodeRouteLocal luAct) "Not a valid route"
    (actorHash, outboxItemHash) <-
        fromMaybeE
            (parseOutboxItemRoute route)
            "Valid local route, but not an outbox item route"
    outboxItemID <- decodeKeyHashidE outboxItemHash "Invalid outbox item hash"
    actorKey <- unhashLocalActorE actorHash "Invalid actor hash"
    return (actorKey, actorHash, outboxItemID)

parseLocalActivityURI'
    :: LocalURI
    -> VA.ActE (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
parseLocalActivityURI' luAct = do
    route <- fromMaybeE (WA.decodeRouteLocal luAct) "Not a valid route"
    (actorHash, outboxItemHash) <-
        fromMaybeE
            (parseOutboxItemRoute route)
            "Valid local route, but not an outbox item route"
    outboxItemID <- WAP.decodeKeyHashidE outboxItemHash "Invalid outbox item hash"
    actorKey <- VA.unhashLocalActorE actorHash "Invalid actor hash"
    return (actorKey, actorHash, outboxItemID)

-- | If the given URI is remote, return as is. If the URI is local, verify that
-- it parses as an activity URI, i.e. an outbox item route, and return the
-- parsed route.
parseActivityURI
    :: (MonadSite m, SiteEnv m ~ App)
    => FedURI
    -> ExceptT Text m
        (Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
        )
parseActivityURI u@(ObjURI h lu) = do
    hl <- hostIsLocalOld h
    if hl
        then Left <$> parseLocalActivityURI lu
        else pure $ Right u

-- | If the given URI is remote, return as is. If the URI is local, verify that
-- it parses as an activity URI, i.e. an outbox item route, and return the
-- parsed route.
parseActivityURI'
    :: FedURI
    -> VA.ActE
        (Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
        )
parseActivityURI' u@(ObjURI h lu) = do
    hl <- WA.hostIsLocal h
    if hl
        then Left <$> parseLocalActivityURI' lu
        else pure $ Right u

activityRoute :: LocalActorBy KeyHashid -> KeyHashid OutboxItem -> Route App
activityRoute (LocalActorPerson p) = PersonOutboxItemR p
activityRoute (LocalActorGroup g) = GroupOutboxItemR g
activityRoute (LocalActorRepo r) = RepoOutboxItemR r
activityRoute (LocalActorDeck d) = DeckOutboxItemR d
activityRoute (LocalActorLoom l) = LoomOutboxItemR l
activityRoute (LocalActorProject r) = ProjectOutboxItemR r
activityRoute (LocalActorFactory f) = FactoryOutboxItemR f

stampRoute :: LocalActorBy KeyHashid -> KeyHashid SigKey -> Route App
stampRoute (LocalActorPerson p) = PersonStampR p
stampRoute (LocalActorGroup g) = GroupStampR g
stampRoute (LocalActorRepo r) = RepoStampR r
stampRoute (LocalActorDeck d) = DeckStampR d
stampRoute (LocalActorLoom l) = LoomStampR l
stampRoute (LocalActorProject r) = ProjectStampR r
stampRoute (LocalActorFactory f) = FactoryStampR f

parseStampRoute
    :: Route App -> Maybe (LocalActorBy KeyHashid, KeyHashid SigKey)
parseStampRoute (PersonStampR p i) = Just (LocalActorPerson p, i)
parseStampRoute (GroupStampR g i)  = Just (LocalActorGroup g, i)
parseStampRoute (RepoStampR r i)   = Just (LocalActorRepo r, i)
parseStampRoute (DeckStampR d i)   = Just (LocalActorDeck d, i)
parseStampRoute (LoomStampR l i)   = Just (LocalActorLoom l, i)
parseStampRoute (ProjectStampR r i)   = Just (LocalActorProject r, i)
parseStampRoute (FactoryStampR f i)   = Just (LocalActorFactory f, i)
parseStampRoute _                       = Nothing

grabLocalActorID :: MonadIO m => LocalActorBy Entity -> SqlPersistT m ActorId
grabLocalActorID (LocalActorPerson (Entity _ p)) = pure $ personActor p
grabLocalActorID (LocalActorGroup (Entity _ g))  = pure $ groupActor g
grabLocalActorID (LocalActorRepo (Entity _ r))   = pure $ repoActor r
grabLocalActorID (LocalActorDeck (Entity _ d))   = pure $ deckActor d
grabLocalActorID (LocalActorLoom (Entity _ l))   = pure $ loomActor l
grabLocalActorID (LocalActorProject (Entity _ r))   = pure $ projectActor r
grabLocalActorID (LocalActorFactory (Entity _ f))   = resourceActor <$> getJust (factoryResource f)

localResourceID :: LocalResourceBy Entity -> ResourceId
localResourceID (LocalResourceGroup (Entity _ g))  = groupResource g
localResourceID (LocalResourceRepo (Entity _ r))   = repoResource r
localResourceID (LocalResourceDeck (Entity _ d))   = deckResource d
localResourceID (LocalResourceLoom (Entity _ l))   = loomResource l
localResourceID (LocalResourceProject (Entity _ r))   = projectResource r
localResourceID (LocalResourceFactory (Entity _ f))   = factoryResource f

grabLocalResourceID :: MonadIO m => LocalResourceBy Entity -> SqlPersistT m ResourceId
grabLocalResourceID = pure . localResourceID

parseFedURIOld
    :: ( MonadSite m
       , SiteEnv m ~ site
       , YesodActivityPub site
       , SiteFedURIMode site ~ URIMode
       )
    => FedURI
    -> ExceptT Text m (Either (Route App) FedURI)
parseFedURIOld u@(ObjURI h lu) = do
    hl <- hostIsLocalOld h
    if hl
        then Left <$> WA.parseLocalURI lu
        else pure $ Right u

parseLocalActorE
    :: (MonadSite m, YesodHashids (SiteEnv m))
    => Route App -> ExceptT Text m (LocalActorBy Key)
parseLocalActorE route = do
    actorByHash <- fromMaybeE (parseLocalActor route) "Not an actor route"
    unhashLocalActorE actorByHash "Invalid actor keyhashid"

parseLocalActorE' :: Route App -> VA.ActE (LocalActorBy Key)
parseLocalActorE' route = do
    actorByHash <- fromMaybeE (parseLocalActor route) "Not an actor route"
    VA.unhashLocalActorE actorByHash "Invalid actor keyhashid"

parseLocalResourceE
    :: (MonadSite m, YesodHashids (SiteEnv m))
    => Route App -> ExceptT Text m (LocalResourceBy Key)
parseLocalResourceE route = do
    actorByHash <- fromMaybeE (parseLocalResource route) "Not a resource route"
    unhashLocalResourceE actorByHash "Invalid resource keyhashid"

parseLocalResourceE' :: Route App -> VA.ActE (LocalResourceBy Key)
parseLocalResourceE' route = do
    actorByHash <- fromMaybeE (parseLocalResource route) "Not a resource route"
    VA.unhashLocalResourceE actorByHash "Invalid resource keyhashid"

parseActorURI
    :: (MonadSite m, SiteEnv m ~ App)
    => FedURI
    -> ExceptT Text m (Either (LocalActorBy Key) FedURI)
parseActorURI u = do
    routeOrRemote <- parseFedURIOld u
    bitraverse
        parseLocalActorE
        pure
        routeOrRemote

parseActorURI' :: FedURI -> VA.ActE (Either (LocalActorBy Key) FedURI)
parseActorURI' u = do
    routeOrRemote <- WA.parseFedURI u
    bitraverse
        parseLocalActorE'
        pure
        routeOrRemote

parseResourceURI
    :: (MonadSite m, SiteEnv m ~ App)
    => FedURI
    -> ExceptT Text m (Either (LocalResourceBy Key) FedURI)
parseResourceURI u = do
    routeOrRemote <- parseFedURIOld u
    bitraverse
        parseLocalResourceE
        pure
        routeOrRemote

parseResourceURI' :: FedURI -> VA.ActE (Either (LocalResourceBy Key) FedURI)
parseResourceURI' u = do
    routeOrRemote <- WA.parseFedURI u
    bitraverse
        parseLocalResourceE'
        pure
        routeOrRemote

renderActivityURI
    :: (MonadSite m, SiteEnv m ~ App)
    => Either (LocalActorBy Key, OutboxItemId) FedURI
    -> m FedURI
renderActivityURI = \case
    Left (la, i) -> do
        encodeRouteHome <- getEncodeRouteHome
        lah <- hashLocalActor la
        ih <- encodeKeyHashid i
        return $ encodeRouteHome $ activityRoute lah ih
    Right u -> pure u

renderActivityURI'
    :: Either (LocalActorBy Key, OutboxItemId) FedURI
    -> VA.Act FedURI
renderActivityURI' = \case
    Left (la, i) -> do
        encodeRouteHome <- WA.getEncodeRouteHome
        lah <- VA.hashLocalActor la
        ih <- WAP.encodeKeyHashid i
        return $ encodeRouteHome $ activityRoute lah ih
    Right u -> pure u
