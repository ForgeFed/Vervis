{- This file is part of Vervis.
 -
 - Written in 2016, 2019, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Handler.Project
    ( getProjectR
    , getProjectInboxR
    , getProjectErrboxR
    , postProjectInboxR
    , getProjectOutboxR
    , getProjectOutboxItemR
    , getProjectFollowersR

    , getProjectMessageR

    , getProjectNewR
    , postProjectNewR

    , getProjectStampR

    , getProjectCollabsR
    , postProjectInviteR
    , postProjectRemoveR

    , getProjectComponentsR
    , getProjectCollabLiveR

    , postProjectInviteCompR

    , getProjectChildrenR
    , getProjectParentsR
    , getProjectParentLiveR

    , postProjectRemoveComponentR
    , postProjectRemoveChildR
    , postProjectRemoveParentR

    , postProjectAddChildR
    , postProjectAddParentR

    , postProjectApproveComponentR
    , postProjectApproveChildR
    , postProjectApproveParentR

    , getProjectTeamsR
    , postProjectAddTeamR
    , postProjectApproveTeamR
    , postProjectRemoveTeamR
    , getProjectTeamLiveR
    )
where

import Control.Applicative
import Control.Arrow ((&&&))
import Control.Monad
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Aeson
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Default.Class
import Data.Foldable
import Data.List
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Network.HTTP.Types.Method
import Optics.Core
import Text.Blaze.Html (Html)
import Yesod.Auth
import Yesod.Core
import Yesod.Core.Handler (redirect, setMessage, lookupPostParam, notFound)
import Yesod.Form.Functions (runFormPost, runFormGet)
import Yesod.Form
import Yesod.Form.Types (FormResult (..))
import Yesod.Persist.Core (runDB, get404, getBy404)

import qualified Data.ByteString.Lazy as BL
import qualified Database.Esqueleto as E

import Database.Persist.JSON
import Development.PatchMediaType
import Network.FedURI
import Web.ActivityPub hiding (Project (..), Repo (..), Actor (..), ActorDetail (..), ActorLocal (..))
import Yesod.ActivityPub
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Data.Paginate.Local
import Database.Persist.Local
import Yesod.Form.Local
import Yesod.Persist.Local

import Vervis.Access
import Vervis.Actor (resourceToActor)
import Vervis.Actor.Project
import Vervis.API
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Federation.Auth
import Vervis.Federation.Discussion
import Vervis.Federation.Offer
import Vervis.Federation.Ticket
import Vervis.FedURI
import Vervis.Form.Ticket
import Vervis.Form.Tracker
import Vervis.Foundation
import Vervis.Model
import Vervis.Paginate
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Recipient
import Vervis.Serve.Collab
import Vervis.Settings
import Vervis.Ticket
import Vervis.TicketFilter
import Vervis.Time
import Vervis.Web.Actor
import Vervis.Widget
import Vervis.Widget.Person
import Vervis.Widget.Ticket
import Vervis.Widget.Tracker

import qualified Vervis.Client as C

getProjectR :: KeyHashid Project -> Handler TypedContent
getProjectR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    mp <- maybeAuthId
    (project, actor, sigKeyIDs, permits) <- runDB $ do
        d <- get404 projectID
        let aid = projectActor d
        a <- getJust aid
        sigKeys <- selectKeysList [SigKeyActor ==. aid] [Asc SigKeyId]
        permits <-
            case mp of
                Nothing -> pure []
                Just personID -> getPermitsForResource personID (Left $ projectResource d)
        return (d, a, sigKeys, permits)

    encodeRouteLocal <- getEncodeRouteLocal
    hashSigKey <- getEncodeKeyHashid
    perActor <- asksSite $ appPerActorKeys . appSettings
    let projectAP = AP.Project
            { AP.projectActor = AP.Actor
                { AP.actorLocal = AP.ActorLocal
                    { AP.actorId         = encodeRouteLocal $ ProjectR projectHash
                    , AP.actorInbox      = encodeRouteLocal $ ProjectInboxR projectHash
                    , AP.actorOutbox     =
                        Just $ encodeRouteLocal $ ProjectOutboxR projectHash
                    , AP.actorFollowers  =
                        Just $ encodeRouteLocal $ ProjectFollowersR projectHash
                    , AP.actorFollowing  = Nothing
                    , AP.actorPublicKeys =
                        map (Left . encodeRouteLocal) $
                        if perActor
                            then map (ProjectStampR projectHash . hashSigKey) sigKeyIDs
                            else [ActorKey1R, ActorKey2R]
                    , AP.actorSshKeys    = []
                    }
                , AP.actorDetail = AP.ActorDetail
                    { AP.actorType       = AP.ActorTypeProject
                    , AP.actorUsername   = Nothing
                    , AP.actorName       = Just $ actorName actor
                    , AP.actorSummary    = Just $ actorDesc actor
                    , AP.actorOrigin     = Nothing
                    }
                }
            , AP.projectTracker    = Nothing
            , AP.projectChildren   = encodeRouteLocal $ ProjectChildrenR projectHash
            , AP.projectParents    = encodeRouteLocal $ ProjectParentsR projectHash
            , AP.projectComponents =
                encodeRouteLocal $ ProjectComponentsR projectHash
            , AP.projectCollaborators =
                encodeRouteLocal $ ProjectCollabsR projectHash
            , AP.projectTeams =
                encodeRouteLocal $ ProjectTeamsR projectHash
            }
    provideHtmlAndAP projectAP $(widgetFile "project/one")
    where
    here = ProjectR projectHash

getProjectInboxR :: KeyHashid Project -> Handler TypedContent
getProjectInboxR = getInbox ProjectInboxR projectActor

getProjectErrboxR :: KeyHashid Project -> Handler TypedContent
getProjectErrboxR = getInbox' actorErrbox ProjectErrboxR projectActor

postProjectInboxR :: KeyHashid Project -> Handler ()
postProjectInboxR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    postInbox LocalActorProject projectID

getProjectOutboxR :: KeyHashid Project -> Handler TypedContent
getProjectOutboxR = getOutbox ProjectOutboxR ProjectOutboxItemR projectActor

getProjectOutboxItemR
    :: KeyHashid Project -> KeyHashid OutboxItem -> Handler TypedContent
getProjectOutboxItemR = getOutboxItem ProjectOutboxItemR projectActor

getProjectFollowersR :: KeyHashid Project -> Handler TypedContent
getProjectFollowersR = getActorFollowersCollection ProjectFollowersR projectActor

getProjectMessageR :: KeyHashid Project -> KeyHashid LocalMessage -> Handler Html
getProjectMessageR _ _ = notFound

getProjectNewR :: Handler Html
getProjectNewR = do
    p <- requireAuthId
    ((_result, widget), enctype) <- runFormPost $ newProjectForm p
    defaultLayout $(widgetFile "project/new")

postProjectNewR :: Handler Html
postProjectNewR = do
    personEntity@(Entity personID person) <- requireAuth
    NewProject name desc (uFactory, uCap) <- runFormPostRedirect ProjectNewR $ newProjectForm personID

    personHash <- encodeKeyHashid personID
    result <- runExceptT $ do
        (maybeSummary, audience, detail) <- C.createProject personHash name desc uFactory
        (localRecips, remoteRecips, fwdHosts, action) <-
            lift $
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.CreateActivity $ AP.Create (AP.CreateProject detail Nothing) (Just uFactory)
        cap <- parseActivityURI uCap
        handleViaActor personID (Just cap) localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
            redirect ProjectNewR
        Right _createID -> do
            setMessage "Create activity sent"
            redirect HomeR

getProjectStampR :: KeyHashid Project -> KeyHashid SigKey -> Handler TypedContent
getProjectStampR = servePerActorKey projectActor LocalActorProject

getProjectCollabsR :: KeyHashid Project -> Handler TypedContent
getProjectCollabsR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    (project, actor) <- runDB $ do
        project <- get404 projectID
        actor <- getJust $ projectActor project
        return (project, actor)
    serveCollabs
        AP.RelHasCollab
        (projectResource project)
        (ProjectR projectHash)
        (ProjectCollabsR projectHash)
        (ProjectRemoveR projectHash)
        (ProjectInviteR projectHash)
        (Just
            ( ProjectRemoveTeamR projectHash
            , ProjectAddTeamR projectHash
            , ProjectApproveTeamR projectHash
            )
        )
        (projectNavW (Entity projectID project) actor)

postProjectInviteR :: KeyHashid Project -> Handler Html
postProjectInviteR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    resourceID <- runDB $ projectResource <$> get404 projectID
    serveInviteCollab resourceID (ProjectCollabsR projectHash)

postProjectRemoveR :: KeyHashid Project -> CollabId -> Handler Html
postProjectRemoveR projectHash collabID = do
    projectID <- decodeKeyHashid404 projectHash
    resourceID <- runDB $ projectResource <$> get404 projectID
    serveRemoveCollab resourceID (ProjectCollabsR projectHash) collabID

getProjectComponentsR :: KeyHashid Project -> Handler TypedContent
getProjectComponentsR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    components <- runDB $ do
        komponentIDs <-
            fmap (map E.unValue) $
            E.select $ E.from $ \ (comp `E.InnerJoin` enable `E.InnerJoin` local) -> do
                E.on $ comp E.^. ComponentId E.==. local E.^. ComponentLocalComponent
                E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
                return $ local E.^. ComponentLocalActor
        concat <$> sequence
            [ map (Left . ComponentRepo) <$> selectKeysList [RepoKomponent <-. komponentIDs] []
            , map (Left . ComponentDeck) <$> selectKeysList [DeckKomponent <-. komponentIDs] []
            , map (Left . ComponentLoom) <$> selectKeysList [LoomKomponent <-. komponentIDs] []
            , map Right <$> getRemotes projectID
            ]
    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    hashActor <- getHashLocalActor
    let componentsAP = Collection
            { collectionId         = encodeRouteLocal $ ProjectComponentsR projectHash
            , collectionType       = CollectionTypeUnordered
            , collectionTotalItems = Just $ length components
            , collectionCurrent    = Nothing
            , collectionFirst      = Nothing
            , collectionLast       = Nothing
            , collectionItems      =
                map (bimap
                        ( encodeRouteHome
                        . renderLocalActor
                        . hashActor
                        . resourceToActor
                        . componentResource
                        )
                        id
                    )
                    components
            , collectionContext    =
                Just $ encodeRouteLocal $ ProjectR projectHash
            }
    provideHtmlAndAP componentsAP $ getHtml projectID

    where

    getRemotes projectID =
        fmap (map $ uncurry ObjURI . bimap E.unValue E.unValue) $
        E.select $ E.from $ \ (comp `E.InnerJoin` enable `E.InnerJoin` remote `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ remote E.^. ComponentRemoteActor E.==. ra E.^. RemoteActorId
            E.on $ comp E.^. ComponentId E.==. remote E.^. ComponentRemoteComponent
            E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
            E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
            return (i E.^. InstanceHost, ro E.^. RemoteObjectIdent)

    getHtml projectID = do
        mp <- maybeAuthId
        haveAdmin <- fmap isJust $ handlerToWidget $ runDB $ runMaybeT $ do
            personID <- MaybeT $ pure mp
            project <- lift $ get404 projectID
            MaybeT $ getCapability personID (Left $ projectResource project) AP.RoleAdmin
        ((_, widgetIC), enctypeIC) <- handlerToWidget $ runFormPost projectInviteCompForm
        (project, actor, comps, drafts) <- handlerToWidget $ runDB $ do
            project <- get404 projectID
            actor <- getJust $ projectActor project
            cs <-
                E.select $ E.from $ \ (comp `E.InnerJoin` enable `E.InnerJoin` grant) -> do
                    E.on $ enable E.^. ComponentEnableGrant E.==. grant E.^. OutboxItemId
                    E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                    E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
                    return (comp, grant)
            cs' <- for cs $ \ (Entity cid c, Entity _ i) -> do
                byKeyOrRaid <- bimap snd snd <$> getComponentIdent cid
                identView <-
                    bitraverse
                        (\ byKey -> do
                            actorID <-
                                case byKey of
                                    ComponentRepo k -> repoActor <$> getJust k
                                    ComponentDeck k -> deckActor <$> getJust k
                                    ComponentLoom k -> loomActor <$> getJust k
                            actor <- getJust actorID
                            return (byKey, actor)
                        )
                        (\ remoteActorID -> do
                            remoteActor <- getJust remoteActorID
                            remoteObject <- getJust $ remoteActorIdent remoteActor
                            inztance <- getJust $ remoteObjectInstance remoteObject
                            return (inztance, remoteObject, remoteActor)
                        )
                        byKeyOrRaid
                return (identView, componentRole c, outboxItemPublished i, cid)
            ds <-
                E.select $ E.from $ \ (comp `E.LeftOuterJoin` enable) -> do
                    E.on $ E.just (comp E.^. ComponentId) E.==. enable E.?. ComponentEnableComponent
                    E.where_ $
                        comp E.^. ComponentProject E.==. E.val projectID E.&&.
                        E.isNothing (enable E.?. ComponentEnableId)
                    return comp
            ds' <- for ds $ \ (Entity cid c) -> do
                (component, accept) <- do
                    ident <- getComponentIdent cid
                    accept <-
                        case bimap fst fst ident of
                            Left localID -> isJust <$> getBy (UniqueComponentAcceptLocal localID)
                            Right remoteID -> isJust <$> getBy (UniqueComponentAcceptRemote remoteID)
                    (,accept) <$> bitraverse
                        (\ (_, byKey) -> do
                            actorID <-
                                case byKey of
                                    ComponentRepo k -> repoActor <$> getJust k
                                    ComponentDeck k -> deckActor <$> getJust k
                                    ComponentLoom k -> loomActor <$> getJust k
                            actor <- getJust actorID
                            return (byKey, actor)
                        )
                        (\ (_, actorID) -> getRemoteActorData actorID)
                        ident
                ((inviter, time), us) <- do
                    usOrThem <-
                        requireEitherAlt
                            (getKeyBy $ UniqueComponentOriginInvite cid)
                            (getKeyBy $ UniqueComponentOriginAdd cid)
                            "Neither us nor them"
                            "Both us and them"
                    (addOrActor, us) <-
                        case usOrThem of
                            Left _usID -> (,True) <$>
                                requireEitherAlt
                                    (fmap componentProjectGestureLocalActivity <$> getValBy (UniqueComponentProjectGestureLocal cid))
                                    (fmap (componentProjectGestureRemoteActor &&& componentProjectGestureRemoteActivity) <$> getValBy (UniqueComponentProjectGestureRemote cid))
                                    "Neither local not remote"
                                    "Both local and remote"
                            Right themID -> (,False) <$>
                                requireEitherAlt
                                    (fmap componentGestureLocalAdd <$> getValBy (UniqueComponentGestureLocal themID))
                                    (fmap (componentGestureRemoteActor &&& componentGestureRemoteAdd) <$> getValBy (UniqueComponentGestureRemote themID))
                                    "Neither local not remote"
                                    "Both local and remote"
                    (,us) <$> case addOrActor of
                        Left addID -> do
                            OutboxItem outboxID _ time <- getJust addID
                            Entity actorID actor <- getByJust $ UniqueActorOutbox outboxID
                            (,time) . Left . (,actor) <$> getLocalActor actorID
                        Right (actorID, addID) -> do
                            RemoteActivity _ _ time <- getJust addID
                            (,time) . Right <$> getRemoteActorData actorID
                return (inviter, us, component, accept, time, componentRole c, cid)
            return (project, actor, cs', ds')
        $(widgetFile "project/components")
        where
        getRemoteActorData actorID = do
            actor <- getJust actorID
            object <- getJust $ remoteActorIdent actor
            inztance <- getJust $ remoteObjectInstance object
            return (inztance, object, actor)

getProjectCollabLiveR
    :: KeyHashid Project -> KeyHashid CollabEnable -> Handler ()
getProjectCollabLiveR projectHash enableHash = do
    projectID <- decodeKeyHashid404 projectHash
    enableID <- decodeKeyHashid404 enableHash
    runDB $ do
        resourceID <- projectResource <$> get404 projectID
        CollabEnable collabID _ <- get404 enableID
        Collab _ resourceID' <- getJust collabID
        unless (resourceID == resourceID') notFound

postProjectInviteCompR :: KeyHashid Project -> Handler Html
postProjectInviteCompR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    uComp <-
        runFormPostRedirect (ProjectComponentsR projectHash) projectInviteCompForm

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome
    let uCollection = encodeRouteHome $ ProjectComponentsR projectHash

    result <- runExceptT $ do
        (maybeSummary, audience, add) <-
            C.add personID uComp uCollection AP.RoleAdmin
        cap <- do
            maybeItem <- lift $ runDB $ do
                resourceID <- projectResource <$> get404 projectID
                getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Project to add components"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AddActivity add
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> setMessage $ toHtml e
        Right _addID -> setMessage "Add sent"
    redirect $ ProjectComponentsR projectHash

getProjectChildrenR :: KeyHashid Project -> Handler TypedContent
getProjectChildrenR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    (actor, project, children) <- runDB $ do
        project <- get404 projectID
        actor <- getJust $ projectActor project
        children <- getChildren projectID
        return (actor, project, children)
    encodeRouteHome <- getEncodeRouteHome
    encodeRouteLocal <- getEncodeRouteLocal
    hashProject <- getEncodeKeyHashid
    h <- asksSite siteInstanceHost
    let makeId (Left (childID, _)) =
            encodeRouteHome $ ProjectR $ hashProject childID
        makeId (Right (i, ro, _)) =
            ObjURI (instanceHost i) (remoteObjectIdent ro)
        makeItem (role, time, i, _) = AP.Relationship
            { AP.relationshipId           = Nothing
            , AP.relationshipExtraTypes   = []
            , AP.relationshipSubject      = encodeRouteHome $ ProjectR projectHash
            , AP.relationshipProperty     = Left AP.RelHasChild
            , AP.relationshipObject       = makeId i
            , AP.relationshipAttributedTo = encodeRouteLocal $ ProjectR projectHash
            , AP.relationshipPublished    = Just time
            , AP.relationshipUpdated      = Nothing
            , AP.relationshipInstrument   = Just role
            }
        childrenAP = Collection
            { collectionId         = encodeRouteLocal $ ProjectChildrenR projectHash
            , collectionType       = CollectionTypeUnordered
            , collectionTotalItems = Just $ length children
            , collectionCurrent    = Nothing
            , collectionFirst      = Nothing
            , collectionLast       = Nothing
            , collectionItems      = map (Doc h . makeItem) children
            , collectionContext    =
                Just $ encodeRouteLocal $ ProjectR projectHash
            }
    provideHtmlAndAP childrenAP $ getHtml projectID project actor children

    where

    getChildren projectID = fmap (sortOn $ view _2) $ liftA2 (++)
        (map (\ (E.Value role, E.Value time, E.Value child, Entity _ actor, E.Value sourceID) ->
                (role, time, Left (child, actor), sourceID)
             )
            <$> getLocals projectID
        )
        (map (\ (E.Value role, E.Value time, Entity _ i, Entity _ ro, Entity _ ra, E.Value sourceID) ->
                (role, time, Right (i, ro, ra), sourceID)
             )
            <$> getRemotes projectID
        )

    getLocals projectID =
        E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` project `E.InnerJoin` actor `E.InnerJoin` deleg `E.InnerJoin` grant) -> do
            E.on $ deleg E.^. SourceUsSendDelegatorGrant E.==. grant E.^. OutboxItemId
            E.on $ source E.^. SourceId E.==. deleg E.^. SourceUsSendDelegatorSource
            E.on $ project E.^. ProjectActor E.==. actor E.^. ActorId
            E.on $ topic E.^. SourceTopicProjectChild E.==. project E.^. ProjectId
            E.on $ holder E.^. SourceHolderProjectId E.==. topic E.^. SourceTopicProjectHolder
            E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
            E.where_ $ holder E.^. SourceHolderProjectProject E.==. E.val projectID
            E.orderBy [E.asc $ deleg E.^. SourceUsSendDelegatorId]
            return
                ( source E.^. SourceRole
                , grant E.^. OutboxItemPublished
                , topic E.^. SourceTopicProjectChild
                , actor
                , source E.^. SourceId
                )

    getRemotes projectID =
        E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` grant `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ topic E.^. SourceTopicRemoteTopic E.==. ra E.^. RemoteActorId
            E.on $ deleg E.^. SourceUsSendDelegatorGrant E.==. grant E.^. OutboxItemId
            E.on $ source E.^. SourceId E.==. deleg E.^. SourceUsSendDelegatorSource
            E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicRemoteSource
            E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
            E.where_ $ holder E.^. SourceHolderProjectProject E.==. E.val projectID
            E.orderBy [E.asc $ deleg E.^. SourceUsSendDelegatorId]
            return
                ( source E.^. SourceRole
                , grant E.^. OutboxItemPublished
                , i
                , ro
                , ra
                , source E.^. SourceId
                )

    getHtml projectID project actor children = do
        mp <- maybeAuthId
        haveAdmin <- fmap isJust $ handlerToWidget $ runDB $ runMaybeT $ do
            personID <- MaybeT $ pure mp
            MaybeT $ getCapability personID (Left $ projectResource project) AP.RoleAdmin
        ((_, widgetAC), enctypeAC) <- runFormPost addChildForm
        invites <- handlerToWidget $ runDB $ do
            sources <- E.select $ E.from $ \ (source `E.InnerJoin` holder `E.LeftOuterJoin` deleg) -> do
                E.on $ E.just (source E.^. SourceId) E.==. deleg E.?. SourceUsSendDelegatorSource
                E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
                E.where_ $
                    holder E.^. SourceHolderProjectProject E.==. E.val projectID E.&&.
                    E.isNothing (deleg E.?. SourceUsSendDelegatorId)
                E.orderBy [E.asc $ source E.^. SourceId]
                return source
            for sources $ \ (Entity sourceID (Source role)) -> do
                (child, accept) <- do
                    topic <- getSourceTopic sourceID
                    accept <-
                        case bimap fst fst topic of
                            Left localID -> isJust <$> getBy (UniqueSourceThemAcceptLocal localID)
                            Right remoteID -> isJust <$> getBy (UniqueSourceThemAcceptRemote remoteID)
                    (,accept) <$> bitraverse
                        (\ (_, e) -> do
                            jID <-
                                case e of
                                    Left j -> pure j
                                    Right _ -> error "I'm a Project but my child is a Group"
                            j <- getJust jID
                            actor <- getJust $ projectActor j
                            return (jID, actor)
                        )
                        (\ (_, actorID) -> getRemoteActorData actorID)
                        topic
                ((inviter, time), us) <- do
                    usOrThem <-
                        requireEitherAlt
                            (getKeyBy $ UniqueSourceOriginUs sourceID)
                            (getKeyBy $ UniqueSourceOriginThem sourceID)
                            "Neither us nor them"
                            "Both us and them"
                    (addOrActor, us) <-
                        case usOrThem of
                            Left usID -> (,True) <$>
                                requireEitherAlt
                                    (fmap sourceUsGestureLocalAdd <$> getValBy (UniqueSourceUsGestureLocal usID))
                                    (fmap (sourceUsGestureRemoteActor &&& sourceUsGestureRemoteAdd) <$> getValBy (UniqueSourceUsGestureRemote usID))
                                    "Neither local not remote"
                                    "Both local and remote"
                            Right themID -> (,False) <$>
                                requireEitherAlt
                                    (fmap sourceThemGestureLocalAdd <$> getValBy (UniqueSourceThemGestureLocal themID))
                                    (fmap (sourceThemGestureRemoteActor &&& sourceThemGestureRemoteAdd) <$> getValBy (UniqueSourceThemGestureRemote themID))
                                    "Neither local not remote"
                                    "Both local and remote"
                    (,us) <$> case addOrActor of
                        Left addID -> do
                            OutboxItem outboxID _ time <- getJust addID
                            Entity actorID actor <- getByJust $ UniqueActorOutbox outboxID
                            (,time) . Left . (,actor) <$> getLocalActor actorID
                        Right (actorID, addID) -> do
                            RemoteActivity _ _ time <- getJust addID
                            (,time) . Right <$> getRemoteActorData actorID
                return (inviter, us, child, accept, time, role, sourceID)
        $(widgetFile "project/children")
        where
        getRemoteActorData actorID = do
            actor <- getJust actorID
            object <- getJust $ remoteActorIdent actor
            inztance <- getJust $ remoteObjectInstance object
            return (inztance, object, actor)

getProjectParentsR :: KeyHashid Project -> Handler TypedContent
getProjectParentsR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    (actor, project, parents) <- runDB $ do
        project <- get404 projectID
        actor <- getJust $ projectActor project
        parents <- getParents projectID
        return (actor, project, parents)
    encodeRouteHome <- getEncodeRouteHome
    encodeRouteLocal <- getEncodeRouteLocal
    hashProject <- getEncodeKeyHashid
    h <- asksSite siteInstanceHost
    let makeId (Left (parentID, _)) =
            encodeRouteHome $ ProjectR $ hashProject parentID
        makeId (Right (i, ro, _)) =
            ObjURI (instanceHost i) (remoteObjectIdent ro)
        makeItem (role, time, i, _) = AP.Relationship
            { AP.relationshipId           = Nothing
            , AP.relationshipExtraTypes   = []
            , AP.relationshipSubject      = encodeRouteHome $ ProjectR projectHash
            , AP.relationshipProperty     = Left AP.RelHasParent
            , AP.relationshipObject       = makeId i
            , AP.relationshipAttributedTo = encodeRouteLocal $ ProjectR projectHash
            , AP.relationshipPublished    = Just time
            , AP.relationshipUpdated      = Nothing
            , AP.relationshipInstrument   = Just role
            }
        parentsAP = Collection
            { collectionId         = encodeRouteLocal $ ProjectParentsR projectHash
            , collectionType       = CollectionTypeUnordered
            , collectionTotalItems = Just $ length parents
            , collectionCurrent    = Nothing
            , collectionFirst      = Nothing
            , collectionLast       = Nothing
            , collectionItems      = map (Doc h . makeItem) parents
            , collectionContext    =
                Just $ encodeRouteLocal $ ProjectR projectHash
            }
    provideHtmlAndAP parentsAP $ getHtml projectID project actor parents

    where

    getParents projectID = fmap (sortOn $ view _2) $ liftA2 (++)
        (map (\ (E.Value role, E.Value time, E.Value parent, Entity _ actor, E.Value destID) ->
                (role, time, Left (parent, actor), destID)
             )
            <$> getLocals projectID
        )
        (map (\ (E.Value role, E.Value time, Entity _ i, Entity _ ro, Entity _ ra, E.Value destID) ->
                (role, time, Right (i, ro, ra), destID)
             )
            <$> getRemotes projectID
        )

    getLocals projectID =
        E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` project `E.InnerJoin` actor `E.InnerJoin` accept `E.InnerJoin` deleg `E.InnerJoin` grant) -> do
            E.on $ deleg E.^. DestThemSendDelegatorLocalGrant E.==. grant E.^. OutboxItemId
            E.on $ accept E.^. DestUsAcceptId E.==. deleg E.^. DestThemSendDelegatorLocalDest
            E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
            E.on $ project E.^. ProjectActor E.==. actor E.^. ActorId
            E.on $ topic E.^. DestTopicProjectParent E.==. project E.^. ProjectId
            E.on $ holder E.^. DestHolderProjectId E.==. topic E.^. DestTopicProjectHolder
            E.on $ dest E.^. DestId E.==. holder E.^. DestHolderProjectDest
            E.where_ $ holder E.^. DestHolderProjectProject E.==. E.val projectID
            E.orderBy [E.asc $ grant E.^. OutboxItemPublished]
            return
                ( dest E.^. DestRole
                , grant E.^. OutboxItemPublished
                , topic E.^. DestTopicProjectParent
                , actor
                , dest E.^. DestId
                )

    getRemotes projectID =
        E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` deleg `E.InnerJoin` grant `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ topic E.^. DestTopicRemoteTopic E.==. ra E.^. RemoteActorId
            E.on $ deleg E.^. DestThemSendDelegatorRemoteGrant E.==. grant E.^. RemoteActivityId
            E.on $ accept E.^. DestUsAcceptId E.==. deleg E.^. DestThemSendDelegatorRemoteDest
            E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
            E.on $ dest E.^. DestId E.==. topic E.^. DestTopicRemoteDest
            E.on $ dest E.^. DestId E.==. holder E.^. DestHolderProjectDest
            E.where_ $ holder E.^. DestHolderProjectProject E.==. E.val projectID
            E.orderBy [E.asc $ grant E.^. RemoteActivityReceived]
            return
                ( dest E.^. DestRole
                , grant E.^. RemoteActivityReceived
                , i
                , ro
                , ra
                , dest E.^. DestId
                )

    getHtml projectID project actor parents = do
        mp <- maybeAuthId
        haveAdmin <- fmap isJust $ handlerToWidget $ runDB $ runMaybeT $ do
            personID <- MaybeT $ pure mp
            MaybeT $ getCapability personID (Left $ projectResource project) AP.RoleAdmin
        ((_, widgetAP), enctypeAP) <- runFormPost addParentForm
        invites <- handlerToWidget $ runDB $ do
            dests <- E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.LeftOuterJoin` accept `E.LeftOuterJoin` delegl `E.LeftOuterJoin` delegr) -> do
                E.on $ accept E.?. DestUsAcceptId E.==. delegr E.?. DestThemSendDelegatorRemoteDest
                E.on $ accept E.?. DestUsAcceptId E.==. delegl E.?. DestThemSendDelegatorLocalDest
                E.on $ E.just (dest E.^. DestId) E.==. accept E.?. DestUsAcceptDest
                E.on $ dest E.^. DestId E.==. holder E.^. DestHolderProjectDest
                E.where_ $
                    holder E.^. DestHolderProjectProject E.==. E.val projectID E.&&.
                    E.isNothing (delegl E.?. DestThemSendDelegatorLocalId) E.&&.
                    E.isNothing (delegr E.?. DestThemSendDelegatorRemoteId)
                E.orderBy [E.asc $ dest E.^. DestId]
                return dest
            for dests $ \ (Entity destID (Dest role)) -> do
                parent <- do
                    topic <- getDestTopic destID
                    bitraverse
                        (\ (_, e) -> do
                            jID <-
                                case e of
                                    Left j -> pure j
                                    Right _ -> error "I'm a Project but my parent is a Group"
                            j <- getJust jID
                            actor <- getJust $ projectActor j
                            return (jID, actor)
                        )
                        (\ (_, actorID) -> getRemoteActorData actorID)
                        topic
                accept <- isJust <$> getBy (UniqueDestUsAccept destID)
                ((inviter, time), us) <- do
                    usOrThem <-
                        requireEitherAlt
                            (getKeyBy $ UniqueDestOriginUs destID)
                            (getKeyBy $ UniqueDestOriginThem destID)
                            "Neither us nor them"
                            "Both us and them"
                    (addOrActor, us) <-
                        case usOrThem of
                            Left _usID -> (,True) <$>
                                requireEitherAlt
                                    (fmap destUsGestureLocalActivity <$> getValBy (UniqueDestUsGestureLocal destID))
                                    (fmap (destUsGestureRemoteActor &&& destUsGestureRemoteActivity) <$> getValBy (UniqueDestUsGestureRemote destID))
                                    "Neither local not remote"
                                    "Both local and remote"
                            Right themID -> (,False) <$>
                                requireEitherAlt
                                    (fmap destThemGestureLocalAdd <$> getValBy (UniqueDestThemGestureLocal themID))
                                    (fmap (destThemGestureRemoteActor &&& destThemGestureRemoteAdd) <$> getValBy (UniqueDestThemGestureRemote themID))
                                    "Neither local not remote"
                                    "Both local and remote"
                    (,us) <$> case addOrActor of
                        Left addID -> do
                            OutboxItem outboxID _ time <- getJust addID
                            Entity actorID actor <- getByJust $ UniqueActorOutbox outboxID
                            (,time) . Left . (,actor) <$> getLocalActor actorID
                        Right (actorID, addID) -> do
                            RemoteActivity _ _ time <- getJust addID
                            (,time) . Right <$> getRemoteActorData actorID
                return (inviter, us, parent, accept, time, role, destID)
        $(widgetFile "project/parents")
        where
        getRemoteActorData actorID = do
            actor <- getJust actorID
            object <- getJust $ remoteActorIdent actor
            inztance <- getJust $ remoteObjectInstance object
            return (inztance, object, actor)

getProjectParentLiveR :: KeyHashid Project -> KeyHashid DestUsStart -> Handler ()
getProjectParentLiveR projectHash startHash = do
    projectID <- decodeKeyHashid404 projectHash
    startID <- decodeKeyHashid404 startHash
    runDB $ do
        _ <- get404 projectID
        DestUsStart usAcceptID _ <- get404 startID
        DestUsAccept destID _ <- getJust usAcceptID
        Entity _ (DestHolderProject _ j) <-
            getBy404 $ UniqueDestHolderProject destID
        unless (j == projectID) notFound

postProjectRemoveComponentR :: KeyHashid Project -> ComponentId -> Handler Html
postProjectRemoveComponentR projectHash componentID = do
    projectID <- decodeKeyHashid404 projectHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            project <- MaybeT $ get projectID
            Component j _ <- MaybeT $ get componentID
            guard $ projectID == j
            _ <- MaybeT $ getBy $ UniqueComponentEnable componentID

            topic <- lift $ bimap snd snd <$> getComponentIdent componentID
            lift $
                (projectResource project,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uComponent <-
                case pidOrU of
                    Left c -> encodeRouteHome . renderLocalResource <$> hashLocalResource (componentResource c)
                    Right u -> pure u
            let uCollection = encodeRouteHome $ ProjectComponentsR projectHash
            C.remove personID uComponent uCollection
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Project to remove components"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Remove sent"
    redirect $ ProjectComponentsR projectHash

postProjectRemoveChildR :: KeyHashid Project -> SourceId -> Handler Html
postProjectRemoveChildR projectHash sourceID = do
    projectID <- decodeKeyHashid404 projectHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            project <- MaybeT $ get projectID
            _ <- MaybeT $ get sourceID
            SourceHolderProject _ j <-
                MaybeT $ getValBy $ UniqueSourceHolderProject sourceID
            guard $ projectID == j
            _ <- MaybeT $ getBy $ UniqueSourceUsSendDelegator sourceID

            topic <- lift $ do
                t <- bimap snd snd <$> getSourceTopic sourceID
                bitraverse
                    (\case
                        Left j' -> pure j'
                        Right _g -> error "I'm a project, I have a Source with topic being Group"
                    )
                    pure
                    t
            lift $
                (projectResource project,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uChild <-
                case pidOrU of
                    Left j -> encodeRouteHome . ProjectR <$> encodeKeyHashid j
                    Right u -> pure u
            let uCollection = encodeRouteHome $ ProjectChildrenR projectHash
            C.remove personID uChild uCollection
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Project to remove children"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Remove sent"
    redirect $ ProjectChildrenR projectHash

postProjectRemoveParentR :: KeyHashid Project -> DestId -> Handler Html
postProjectRemoveParentR projectHash destID = do
    projectID <- decodeKeyHashid404 projectHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            project <- MaybeT $ get projectID
            _ <- MaybeT $ get destID
            DestHolderProject _ j <-
                MaybeT $ getValBy $ UniqueDestHolderProject destID
            guard $ projectID == j
            acceptID <- MaybeT $ getKeyBy $ UniqueDestUsAccept destID
            _ <- MaybeT $ getBy $ UniqueDestUsStart acceptID

            topic <- lift $ do
                t <- bimap snd snd <$> getDestTopic destID
                bitraverse
                    (\case
                        Left j' -> pure j'
                        Right _g -> error "I'm a project, I have a Dest with topic being Group"
                    )
                    pure
                    t
            lift $
                (projectResource project,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uParent <-
                case pidOrU of
                    Left j -> encodeRouteHome . ProjectR <$> encodeKeyHashid j
                    Right u -> pure u
            let uCollection = encodeRouteHome $ ProjectParentsR projectHash
            C.remove personID uParent uCollection
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Project to remove parents"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Remove sent"
    redirect $ ProjectParentsR projectHash

addChildForm = renderDivs $
    areq fedUriField "(URI) Child project" Nothing

postProjectAddChildR :: KeyHashid Project -> Handler Html
postProjectAddChildR projectHash = do
    uChild <- runFormPostRedirect (ProjectChildrenR projectHash) addChildForm
    encodeRouteHome <- getEncodeRouteHome
    let uCollection = encodeRouteHome $ ProjectChildrenR projectHash

    projectID <- decodeKeyHashid404 projectHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID

    result <- runExceptT $ do
        project <- lift $ runDB $ get404 projectID
        (maybeSummary, audience, add) <- C.add personID uChild uCollection AP.RoleAdmin
        cap <- do
            let resourceID = projectResource project
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Project to add children"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AddActivity add
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Add sent"
    redirect $ ProjectChildrenR projectHash

addParentForm = renderDivs $
    areq fedUriField "(URI) Parent project" Nothing

postProjectAddParentR :: KeyHashid Project -> Handler Html
postProjectAddParentR projectHash = do
    uParent <- runFormPostRedirect (ProjectParentsR projectHash) addParentForm
    encodeRouteHome <- getEncodeRouteHome
    let uCollection = encodeRouteHome $ ProjectParentsR projectHash

    projectID <- decodeKeyHashid404 projectHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID

    result <- runExceptT $ do
        project <- lift $ runDB $ get404 projectID
        (maybeSummary, audience, add) <- C.add personID uParent uCollection AP.RoleAdmin
        cap <- do
            let resourceID = projectResource project
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Project to add parents"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AddActivity add
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Add sent"
    redirect $ ProjectParentsR projectHash

postProjectApproveComponentR :: KeyHashid Project -> ComponentId -> Handler Html
postProjectApproveComponentR projectHash compID = do
    projectID <- decodeKeyHashid404 projectHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            project <- MaybeT $ get projectID
            Component j _ <- MaybeT $ get compID
            guard $ projectID == j

            uAdd <- lift $ do
                add <- getComponentAdd compID
                renderActivityURI add

            topic <- lift $ bimap snd snd <$> getComponentIdent compID
            lift $
                (projectResource project,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, accept) <- do
            uComponent <-
                case pidOrU of
                    Left c -> encodeRouteHome . renderLocalResource <$> hashLocalResource (componentResource c)
                    Right u -> pure u
            let uProject = encodeRouteHome $ ProjectR projectHash
            C.acceptParentChild personID uAdd uProject uComponent
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Project to approve components"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AcceptActivity accept
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Accept sent"
    redirect $ ProjectComponentsR projectHash

postProjectApproveChildR :: KeyHashid Project -> SourceId -> Handler Html
postProjectApproveChildR projectHash sourceID = do
    projectID <- decodeKeyHashid404 projectHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            project <- MaybeT $ get projectID
            _ <- MaybeT $ get sourceID
            SourceHolderProject _ j <-
                MaybeT $ getValBy $ UniqueSourceHolderProject sourceID
            guard $ projectID == j

            uAdd <- lift $ do
                add <- getSourceAdd sourceID
                renderActivityURI add

            topic <- lift $ do
                t <- bimap snd snd <$> getSourceTopic sourceID
                bitraverse
                    (\case
                        Left j' -> pure j'
                        Right _g -> error "I'm a project, I have a Source with topic being Group"
                    )
                    pure
                    t
            lift $
                (projectResource project,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, accept) <- do
            uChild <-
                case pidOrU of
                    Left j -> encodeRouteHome . ProjectR <$> encodeKeyHashid j
                    Right u -> pure u
            let uParent = encodeRouteHome $ ProjectR projectHash
            C.acceptParentChild personID uAdd uParent uChild
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Project to approve children"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AcceptActivity accept
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Accept sent"
    redirect $ ProjectChildrenR projectHash

postProjectApproveParentR :: KeyHashid Project -> DestId -> Handler Html
postProjectApproveParentR projectHash destID = do
    projectID <- decodeKeyHashid404 projectHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            project <- MaybeT $ get projectID
            _ <- MaybeT $ get destID
            DestHolderProject _ j <-
                MaybeT $ getValBy $ UniqueDestHolderProject destID
            guard $ projectID == j

            uAdd <- lift $ do
                add <- getDestAdd destID
                renderActivityURI add

            topic <- lift $ do
                t <- bimap snd snd <$> getDestTopic destID
                bitraverse
                    (\case
                        Left j' -> pure j'
                        Right _g -> error "I'm a project, I have a Dest with topic being Group"
                    )
                    pure
                    t
            lift $
                (projectResource project,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, accept) <- do
            uParent <-
                case pidOrU of
                    Left j -> encodeRouteHome . ProjectR <$> encodeKeyHashid j
                    Right u -> pure u
            let uChild = encodeRouteHome $ ProjectR projectHash
            C.acceptParentChild personID uAdd uParent uChild
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Project to approve parents"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AcceptActivity accept
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Accept sent"
    redirect $ ProjectParentsR projectHash

getProjectTeamsR :: KeyHashid Project -> Handler TypedContent
getProjectTeamsR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    resourceID <- runDB $ projectResource <$> getJust projectID
    serveTeamsCollection (ProjectR projectHash) (ProjectTeamsR projectHash) resourceID

postProjectAddTeamR :: KeyHashid Project -> Handler ()
postProjectAddTeamR projectHash = do
    projectID <- decodeKeyHashid404 projectHash
    (uTeam, role) <-
        runFormPostRedirect (ProjectCollabsR projectHash) addTeamForm

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    let uCollection = encodeRouteHome $ ProjectTeamsR projectHash

    result <- runExceptT $ do
        (maybeSummary, audience, add) <- C.add personID uTeam uCollection role
        cap <- do
            maybeItem <- lift $ runDB $ do
                resourceID <- projectResource <$> get404 projectID
                getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Project to add teams"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AddActivity add
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> setMessage $ toHtml e
        Right inviteID -> setMessage "Add sent"
    redirect $ ProjectCollabsR projectHash

postProjectApproveTeamR :: KeyHashid Project -> SquadId -> Handler Html
postProjectApproveTeamR projectHash squadID = do
    projectID <- decodeKeyHashid404 projectHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            project <- MaybeT $ get projectID
            Squad _ resourceID <- MaybeT $ get squadID
            guard $ resourceID == projectResource project

            uAdd <- lift $ do
                add <- getSquadAdd squadID
                renderActivityURI add

            topic <- lift $ bimap snd snd <$> getSquadTeam squadID
            lift $
                (projectResource project,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, accept) <- do
            uTeam <-
                case pidOrU of
                    Left g -> encodeRouteHome . GroupR <$> encodeKeyHashid g
                    Right u -> pure u
            let uProject = encodeRouteHome $ ProjectR projectHash
            C.acceptParentChild personID uAdd uTeam uProject
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Project to approve teams"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AcceptActivity accept
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Accept sent"
    redirect $ ProjectCollabsR projectHash

postProjectRemoveTeamR :: KeyHashid Project -> SquadId -> Handler Html
postProjectRemoveTeamR projectHash squadID = do
    projectID <- decodeKeyHashid404 projectHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            project <- MaybeT $ get projectID
            Squad _ resourceID <- MaybeT $ get squadID
            guard $ resourceID == projectResource project
            acceptID <- MaybeT $ getKeyBy $ UniqueSquadUsAccept squadID
            _ <- MaybeT $ getBy $ UniqueSquadUsStart acceptID

            uAdd <- lift $ do
                add <- getSquadAdd squadID
                renderActivityURI add

            topic <- lift $ bimap snd snd <$> getSquadTeam squadID
            lift $
                (projectResource project,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uTeam <-
                case pidOrU of
                    Left g -> encodeRouteHome . GroupR <$> encodeKeyHashid g
                    Right u -> pure u
            let uCollection = encodeRouteHome $ ProjectTeamsR projectHash
            C.remove personID uTeam uCollection
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Project to remove teams"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Remove sent"
    redirect $ ProjectCollabsR projectHash

getProjectTeamLiveR :: KeyHashid Project -> KeyHashid SquadUsStart -> Handler ()
getProjectTeamLiveR projectHash startHash = do
    projectID <- decodeKeyHashid404 projectHash
    startID <- decodeKeyHashid404 startHash
    runDB $ do
        project <- get404 projectID
        SquadUsStart usAcceptID _ <- get404 startID
        SquadUsAccept squadID _ <- getJust usAcceptID
        Squad _ resourceID <- getJust squadID
        unless (resourceID == projectResource project) notFound
