{- This file is part of Vervis.
 -
 - Written in 2022, 2023, 2024 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Handler.Loom
    ( getLoomR
    , getLoomInboxR
    , getLoomErrboxR
    , postLoomInboxR
    , getLoomOutboxR
    , getLoomOutboxItemR
    , getLoomFollowersR
    , getLoomClothsR

    , getLoomMessageR

    , getLoomNewR
    , postLoomNewR

    , getLoomStampR

    , getLoomCollabsR
    , postLoomInviteR
    , postLoomRemoveR
    , getLoomProjectsR
    , postLoomAddProjectR
    , postLoomApproveProjectR
    , postLoomRemoveProjectR

    , getLoomTeamsR

    , postLoomAddTeamR
    , postLoomApproveTeamR
    , postLoomRemoveTeamR
    , getLoomTeamLiveR
    )
where

import Control.Monad
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Aeson
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Default.Class
import Data.Foldable
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Optics.Core
import Text.Blaze.Html (Html)
import Yesod.Auth
import Yesod.Core
import Yesod.Core.Handler (redirect, setMessage, lookupPostParam, notFound)
import Yesod.Form.Functions (runFormPost, runFormGet)
import Yesod.Form.Types (FormResult (..))
import Yesod.Form
import Yesod.Persist.Core (runDB, get404, getBy404)

import qualified Data.ByteString.Lazy as BL
import qualified Database.Esqueleto as E

import Database.Persist.JSON
import Development.PatchMediaType
import Network.FedURI
import Yesod.ActivityPub
import Yesod.Hashids
import Yesod.FedURI
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Data.Paginate.Local
import Database.Persist.Local
import Yesod.Form.Local
import Yesod.Persist.Local

import Vervis.Access
import Vervis.API
import Vervis.Data.Actor
import Vervis.Federation.Auth
import Vervis.Federation.Discussion
import Vervis.Federation.Offer
import Vervis.Federation.Ticket
import Vervis.FedURI
import Vervis.Form.Ticket
import Vervis.Form.Tracker
import Vervis.Foundation
import Vervis.Model
import Vervis.Paginate
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Ticket
import Vervis.Recipient
import Vervis.Serve.Collab
import Vervis.Settings
import Vervis.Ticket
import Vervis.TicketFilter
import Vervis.Time
import Vervis.Web.Actor
import Vervis.Web.Collab
import Vervis.Widget.Person
import Vervis.Widget.Ticket
import Vervis.Widget.Tracker

import qualified Vervis.Client as C

getLoomR :: KeyHashid Loom -> Handler TypedContent
getLoomR loomHash = do
    loomID <- decodeKeyHashid404 loomHash
    (loom, actor, sigKeyIDs) <- runDB $ do
        l <- get404 loomID
        let aid = loomActor l
        a <- getJust aid
        sigKeys <- selectKeysList [SigKeyActor ==. aid] [Asc SigKeyId]
        return (l, a, sigKeys)

    encodeRouteLocal <- getEncodeRouteLocal
    hashSigKey <- getEncodeKeyHashid
    perActor <- asksSite $ appPerActorKeys . appSettings
    let route mk = encodeRouteLocal $ mk loomHash
        loomAP = AP.PatchTracker
            { AP.patchTrackerActor = AP.Actor
                { AP.actorLocal = AP.ActorLocal
                    { AP.actorId         = route LoomR
                    , AP.actorInbox      = route LoomInboxR
                    , AP.actorOutbox     = Just $ route LoomOutboxR
                    , AP.actorFollowers  = Just $ route LoomFollowersR
                    , AP.actorFollowing  = Nothing
                    , AP.actorPublicKeys =
                        map (Left . encodeRouteLocal) $
                        if perActor
                            then map (LoomStampR loomHash . hashSigKey) sigKeyIDs
                            else [ActorKey1R, ActorKey2R]
                    , AP.actorSshKeys    = []
                    }
                , AP.actorDetail = AP.ActorDetail
                    { AP.actorType       = AP.ActorTypePatchTracker
                    , AP.actorUsername   = Nothing
                    , AP.actorName       = Just $ actorName actor
                    , AP.actorSummary    = Just $ actorDesc actor
                    , AP.actorOrigin     = Nothing
                    }
                }
            , AP.patchTrackerCollaborators =
                encodeRouteLocal $ LoomCollabsR loomHash
            , AP.patchTrackerProjects =
                encodeRouteLocal $ LoomProjectsR loomHash
            , AP.patchTrackerTeams =
                encodeRouteLocal $ LoomTeamsR loomHash
            }

    provideHtmlAndAP loomAP $ redirect $ LoomClothsR loomHash

getLoomInboxR :: KeyHashid Loom -> Handler TypedContent
getLoomInboxR = getInbox LoomInboxR loomActor

getLoomErrboxR :: KeyHashid Loom -> Handler TypedContent
getLoomErrboxR = getInbox' actorErrbox LoomErrboxR loomActor

postLoomInboxR :: KeyHashid Loom -> Handler ()
postLoomInboxR loomHash = do
    loomID <- decodeKeyHashid404 loomHash
    postInbox LocalActorLoom loomID

{-
            AP.AcceptActivity accept ->
                loomAcceptF now recipLoomHash author body mfwd luActivity accept
            AP.ApplyActivity apply->
                loomApplyF now recipLoomHash author body mfwd luActivity apply
            AP.CreateActivity (AP.Create obj _mtarget) ->
                case obj of
                    AP.CreateNote _ note ->
                        (,Nothing) <$> loomCreateNoteF now recipLoomHash author body mfwd luActivity note
                    _ -> return ("Unsupported create object type for looms", Nothing)
            AP.FollowActivity follow ->
                loomFollowF now recipLoomHash author body mfwd luActivity follow
            AP.InviteActivity invite ->
                topicInviteF now (GrantResourceLoom recipLoomHash) author body mfwd luActivity invite
            AP.JoinActivity join ->
                loomJoinF now recipLoomHash author body mfwd luActivity join
            AP.OfferActivity (AP.Offer obj target) ->
                case obj of
                    AP.OfferTicket ticket ->
                        loomOfferTicketF now recipLoomHash author body mfwd luActivity ticket target
                    _ -> return ("Unsupported offer object type for looms", Nothing)
            AP.ResolveActivity resolve ->
                loomResolveF now recipLoomHash author body mfwd luActivity resolve
            AP.UndoActivity undo ->
                (,Nothing) <$> loomUndoF now recipLoomHash author body mfwd luActivity undo
            _ -> return ("Unsupported activity type for looms", Nothing)
-}

getLoomOutboxR :: KeyHashid Loom -> Handler TypedContent
getLoomOutboxR = getOutbox LoomOutboxR LoomOutboxItemR loomActor

getLoomOutboxItemR
    :: KeyHashid Loom -> KeyHashid OutboxItem -> Handler TypedContent
getLoomOutboxItemR = getOutboxItem LoomOutboxItemR loomActor

getLoomFollowersR :: KeyHashid Loom -> Handler TypedContent
getLoomFollowersR = getActorFollowersCollection LoomFollowersR loomActor

getLoomClothsR :: KeyHashid Loom -> Handler TypedContent
getLoomClothsR loomHash = selectRep $ do
    provideRep $ do
        let tf = def
        {-
        ((filtResult, filtWidget), filtEnctype) <- runFormPost ticketFilterForm
        let tf =
                case filtResult of
                    FormSuccess filt -> filt
                    FormMissing      -> def
                    FormFailure l    ->
                        error $ "Ticket filter form failed: " ++ show l
        -}
        loomID <- decodeKeyHashid404 loomHash
        (loom, actor, (total, pages, mpage)) <- runDB $ do
            loom <- get404 loomID
            actor <- getJust $ loomActor loom
            let countAllTickets = count [TicketLoomLoom ==. loomID]
                selectTickets off lim =
                    getClothSummaries
                        (filterTickets tf)
                        (Just $ \ t -> [E.desc $ t E.^. TicketId])
                        (Just (off, lim))
                        loomID
            (loom,actor,) <$> getPageAndNavCount countAllTickets selectTickets
        permits <- do
            mp <- maybeAuthId
            case mp of
                Nothing -> pure []
                Just personID -> runDB $ getPermitsForResource personID (Left $ loomResource loom)
        case mpage of
            Nothing -> redirectFirstPage here
            Just (rows, navModel) ->
                let pageNav = navWidget navModel
                in  defaultLayout $(widgetFile "cloth/list")
    AP.provideAP' $ do
        loomID <- decodeKeyHashid404 loomHash
        (total, pages, mpage) <- runDB $ do
            let countAllTickets = count [TicketLoomLoom ==. loomID]
                selectTickets off lim =
                    selectKeysList
                        [TicketLoomLoom ==. loomID]
                        [OffsetBy off, LimitTo lim, Desc TicketLoomTicket]
            getPageAndNavCount countAllTickets selectTickets

        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal
        hashTicket <- getEncodeKeyHashid
        encodeRoutePageLocal <- getEncodeRoutePageLocal
        let pageUrl = encodeRoutePageLocal here
        host <- asksSite siteInstanceHost
        return $
            case mpage of
                Nothing -> encodeStrict $ AP.Doc host $ AP.Collection
                    { AP.collectionId         = encodeRouteLocal here
                    , AP.collectionType       = AP.CollectionTypeOrdered
                    , AP.collectionTotalItems = Just total
                    , AP.collectionCurrent    = Nothing
                    , AP.collectionFirst      = Just $ pageUrl 1
                    , AP.collectionLast       = Just $ pageUrl pages
                    , AP.collectionItems      = [] :: [Text]
                    , AP.collectionContext    = Nothing
                    }
                Just (tickets, navModel) ->
                    let current = nmCurrent navModel
                    in  encodeStrict $ AP.Doc host $ AP.CollectionPage
                            { AP.collectionPageId         = pageUrl current
                            , AP.collectionPageType       = AP.CollectionPageTypeOrdered
                            , AP.collectionPageTotalItems = Nothing
                            , AP.collectionPageCurrent    = Just $ pageUrl current
                            , AP.collectionPageFirst      = Just $ pageUrl 1
                            , AP.collectionPageLast       = Just $ pageUrl pages
                            , AP.collectionPagePartOf     = encodeRouteLocal here
                            , AP.collectionPagePrev       =
                                if current > 1
                                    then Just $ pageUrl $ current - 1
                                    else Nothing
                            , AP.collectionPageNext       =
                                if current < pages
                                    then Just $ pageUrl $ current + 1
                                    else Nothing
                            , AP.collectionPageStartIndex = Nothing
                            , AP.collectionPageItems      =
                                encodeRouteHome . ClothR loomHash . hashTicket <$> tickets
                            }
    where
    here = LoomClothsR loomHash
    encodeStrict = BL.toStrict . encode

getLoomMessageR
    :: KeyHashid Loom -> KeyHashid LocalMessage -> Handler TypedContent
getLoomMessageR _ _ = notFound

getLoomNewR :: Handler Html
getLoomNewR = do
    ((_result, widget), enctype) <- runFormPost newLoomForm
    defaultLayout
        [whamlet|
            <form method=POST action=@{LoomNewR} enctype=#{enctype}>
              ^{widget}
              <div class="submit">
                  <input type="submit">
        |]

postLoomNewR :: Handler Html
postLoomNewR = do
    NewLoom name desc repoID <- runFormPostRedirect LoomNewR newLoomForm

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    repoHash <- encodeKeyHashid repoID
    (maybeSummary, audience, detail, repos) <-
        C.createLoom personHash name desc repoHash
    actor <- runDB $ do

        -- Find the specified repo in DB
        repo <- getJust repoID

        -- Make sure the repo has a single, full-access collab, granted to the
        -- creator of the loom
        maybeApproved <- runMaybeT $ do
            collabs <- lift $ selectKeysList [CollabTopic ==. repoResource repo] []
            collabID <-
                case collabs of
                    [c] -> return c
                    _ -> mzero
            CollabRecipLocal _ recipID <-
                MaybeT $ getValBy $ UniqueCollabRecipLocal collabID
            guard $ recipID == personID
            _ <- MaybeT $ getBy $ UniqueCollabEnable collabID
            _ <- MaybeT $ getBy $ UniqueCollabFulfillsLocalTopicCreation collabID
            return ()
        unless (isJust maybeApproved) $ do
            setMessage "Can't link with the repo chosen"
            redirect LoomNewR

        getJust $ personActor person

    result <- do
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput Nothing maybeSummary audience $ AP.CreateActivity $ AP.Create (AP.CreatePatchTracker detail repos Nothing) Nothing
        runExceptT $ createPatchTrackerC personEntity actor Nothing localRecips remoteRecips fwdHosts action detail repos Nothing Nothing

    case result of
        Left e -> do
            setMessage $ toHtml e
            redirect LoomNewR
        Right _createID -> do
            setMessage "Create activity sent"
            redirect HomeR

getLoomStampR :: KeyHashid Loom -> KeyHashid SigKey -> Handler TypedContent
getLoomStampR = servePerActorKey loomActor LocalActorLoom

getLoomCollabsR :: KeyHashid Loom -> Handler TypedContent
getLoomCollabsR loomHash = do
    loomID <- decodeKeyHashid404 loomHash
    (loom, actor) <- runDB $ do
        loom <- get404 loomID
        actor <- getJust $ loomActor loom
        return (loom, actor)
    serveCollabs
        AP.RelHasCollab
        (loomResource loom)
        (LoomR loomHash)
        (LoomCollabsR loomHash)
        (LoomRemoveR loomHash)
        (LoomInviteR loomHash)
        (Just
            ( LoomRemoveTeamR loomHash
            , LoomAddTeamR loomHash
            , LoomApproveTeamR loomHash
            )
        )
        (loomNavW (Entity loomID loom) actor)

postLoomInviteR :: KeyHashid Loom -> Handler Html
postLoomInviteR loomHash = do
    loomID <- decodeKeyHashid404 loomHash
    resourceID <- runDB $ loomResource <$> get404 loomID
    serveInviteCollab resourceID (LoomCollabsR loomHash)

postLoomRemoveR :: KeyHashid Loom -> CollabId -> Handler Html
postLoomRemoveR loomHash collabID = do
    loomID <- decodeKeyHashid404 loomHash
    resourceID <- runDB $ loomResource <$> get404 loomID
    serveRemoveCollab resourceID (LoomCollabsR loomHash) collabID

getLoomProjectsR :: KeyHashid Loom -> Handler TypedContent
getLoomProjectsR loomHash = do
    loomID <- decodeKeyHashid404 loomHash
    stems <- runDB $ do
        loom <- get404 loomID
        getStems $ loomKomponent loom
    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    hashProject <- getEncodeKeyHashid
    let projectsAP = AP.Collection
            { AP.collectionId         = encodeRouteLocal $ LoomProjectsR loomHash
            , AP.collectionType       = AP.CollectionTypeUnordered
            , AP.collectionTotalItems = Just $ length stems
            , AP.collectionCurrent    = Nothing
            , AP.collectionFirst      = Nothing
            , AP.collectionLast       = Nothing
            , AP.collectionItems      =
                map ( either
                        ( encodeRouteHome
                        . ProjectR
                        . hashProject
                        . fst
                        )
                        (\ (Instance h, RemoteObject _ lu , _) -> ObjURI h lu)
                    . view _1
                    )
                    stems
            , AP.collectionContext    =
                Just $ encodeRouteLocal $ LoomR loomHash
            }
    provideHtmlAndAP projectsAP $ getHtml loomID stems

    where

    getHtml loomID stems = do
        mp <- maybeAuthId
        haveAdmin <- handlerToWidget $ fmap isJust $ runDB $ runMaybeT $ do
            personID <- MaybeT $ pure mp
            loom <- lift $ get404 loomID
            MaybeT $ getCapability personID (Left $ loomResource loom) AP.RoleAdmin
        ((_, widgetAP), enctypeAP) <- handlerToWidget $ runFormPost addProjectForm
        (loom, actor, drafts) <- handlerToWidget $ runDB $ do
            loom <- get404 loomID
            actor <- getJust $ loomActor loom
            drafts <- getStemDrafts $ loomKomponent loom
            return (loom, actor, drafts)
        $(widgetFile "loom/projects")

addProjectForm = renderDivs $
    areq fedUriField "(URI) Project" Nothing

postLoomAddProjectR :: KeyHashid Loom -> Handler ()
postLoomAddProjectR loomHash = do
    loomID <- decodeKeyHashid404 loomHash
    uProject <-
        runFormPostRedirect (LoomProjectsR loomHash) addProjectForm

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    let uCollection = encodeRouteHome $ LoomProjectsR loomHash

    result <- runExceptT $ do
        (maybeSummary, audience, add) <- C.add personID uProject uCollection AP.RoleAdmin
        cap <- do
            maybeItem <- lift $ runDB $ do
                resourceID <- loomResource <$> get404 loomID
                getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Loom to add projects"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AddActivity add
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> setMessage $ toHtml e
        Right inviteID -> setMessage "Add sent"
    redirect $ LoomProjectsR loomHash

postLoomApproveProjectR :: KeyHashid Loom -> StemId -> Handler Html
postLoomApproveProjectR loomHash stemID = do
    loomID <- decodeKeyHashid404 loomHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            loom <- MaybeT $ get loomID
            Stem _ kompID <- MaybeT $ get stemID
            guard $ kompID == loomKomponent loom

            uAdd <- lift $ do
                add <- getStemAdd stemID
                renderActivityURI add

            topic <- lift $ getStemProject stemID
            lift $
                (loomResource loom,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, accept) <- do
            uProject <-
                case pidOrU of
                    Left j -> encodeRouteHome . ProjectR <$> encodeKeyHashid j
                    Right u -> pure u
            let uLoom = encodeRouteHome $ LoomR loomHash
            C.acceptParentChild personID uAdd uProject uLoom
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Loom to approve projects"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AcceptActivity accept
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Accept sent"
    redirect $ LoomProjectsR loomHash

postLoomRemoveProjectR :: KeyHashid Loom -> StemId -> Handler Html
postLoomRemoveProjectR loomHash stemID = do
    loomID <- decodeKeyHashid404 loomHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            loom <- MaybeT $ get loomID
            Stem _ kompID <- MaybeT $ get stemID
            guard $ kompID == loomKomponent loom
            acceptID <- MaybeT $ getKeyBy $ UniqueStemComponentAccept stemID
            _ <- MaybeT $ getBy $ UniqueStemDelegateLocal acceptID

            uAdd <- lift $ do
                add <- getStemAdd stemID
                renderActivityURI add

            topic <- lift $ getStemProject stemID
            lift $
                (loomResource loom,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uProject <-
                case pidOrU of
                    Left j -> encodeRouteHome . ProjectR <$> encodeKeyHashid j
                    Right u -> pure u
            let uCollection = encodeRouteHome $ LoomProjectsR loomHash
            C.remove personID uProject uCollection
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Loom to remove projects"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Remove sent"
    redirect $ LoomProjectsR loomHash

getLoomTeamsR :: KeyHashid Loom -> Handler TypedContent
getLoomTeamsR loomHash = do
    loomID <- decodeKeyHashid404 loomHash
    resourceID <- runDB $ do
        komponentID <- loomKomponent <$> get404 loomID
        komponentResource <$> getJust komponentID
    serveTeamsCollection (LoomR loomHash) (LoomTeamsR loomHash) resourceID

postLoomAddTeamR :: KeyHashid Loom -> Handler ()
postLoomAddTeamR loomHash = do
    loomID <- decodeKeyHashid404 loomHash
    (uTeam, role) <-
        runFormPostRedirect (LoomCollabsR loomHash) addTeamForm

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    let uCollection = encodeRouteHome $ LoomTeamsR loomHash

    result <- runExceptT $ do
        (maybeSummary, audience, add) <- C.add personID uTeam uCollection role
        cap <- do
            maybeItem <- lift $ runDB $ do
                resourceID <- loomResource <$> get404 loomID
                getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Loom to add teams"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AddActivity add
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> setMessage $ toHtml e
        Right inviteID -> setMessage "Add sent"
    redirect $ LoomCollabsR loomHash

postLoomApproveTeamR :: KeyHashid Loom -> SquadId -> Handler Html
postLoomApproveTeamR loomHash squadID = do
    loomID <- decodeKeyHashid404 loomHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            loom <- MaybeT $ get loomID
            Squad _ resourceID <- MaybeT $ get squadID
            guard $ resourceID == loomResource loom

            uAdd <- lift $ do
                add <- getSquadAdd squadID
                renderActivityURI add

            topic <- lift $ bimap snd snd <$> getSquadTeam squadID
            lift $
                (loomResource loom,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, accept) <- do
            uTeam <-
                case pidOrU of
                    Left g -> encodeRouteHome . GroupR <$> encodeKeyHashid g
                    Right u -> pure u
            let uLoom = encodeRouteHome $ LoomR loomHash
            C.acceptParentChild personID uAdd uTeam uLoom
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Loom to approve teams"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AcceptActivity accept
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Accept sent"
    redirect $ LoomCollabsR loomHash

postLoomRemoveTeamR :: KeyHashid Loom -> SquadId -> Handler Html
postLoomRemoveTeamR loomHash squadID = do
    loomID <- decodeKeyHashid404 loomHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            loom <- MaybeT $ get loomID
            Squad _ resourceID <- MaybeT $ get squadID
            guard $ resourceID == loomResource loom
            acceptID <- MaybeT $ getKeyBy $ UniqueSquadUsAccept squadID
            _ <- MaybeT $ getBy $ UniqueSquadUsStart acceptID

            uAdd <- lift $ do
                add <- getSquadAdd squadID
                renderActivityURI add

            topic <- lift $ bimap snd snd <$> getSquadTeam squadID
            lift $
                (loomResource loom,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uTeam <-
                case pidOrU of
                    Left g -> encodeRouteHome . GroupR <$> encodeKeyHashid g
                    Right u -> pure u
            let uCollection = encodeRouteHome $ LoomTeamsR loomHash
            C.remove personID uTeam uCollection
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Loom to remove teams"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Remove sent"
    redirect $ LoomCollabsR loomHash

getLoomTeamLiveR :: KeyHashid Loom -> KeyHashid SquadUsStart -> Handler ()
getLoomTeamLiveR loomHash startHash = do
    loomID <- decodeKeyHashid404 loomHash
    startID <- decodeKeyHashid404 startHash
    runDB $ do
        loom <- get404 loomID
        SquadUsStart usAcceptID _ <- get404 startID
        SquadUsAccept squadID _ <- getJust usAcceptID
        Squad _ resourceID <- getJust squadID
        unless (resourceID == loomResource loom) notFound
