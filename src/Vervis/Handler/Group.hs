{- This file is part of Vervis.
 -
 - Written in 2016, 2019, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Handler.Group
    ( getGroupNewR
    , postGroupNewR

    , getGroupR
    , getGroupInboxR
    , getGroupErrboxR
    , postGroupInboxR
    , getGroupOutboxR
    , getGroupOutboxItemR
    , getGroupFollowersR
    , getGroupMessageR

    , getGroupStampR

    , getGroupMembersR
    , postGroupInviteR
    , postGroupRemoveR

    , getGroupChildrenR
    , getGroupChildLiveR
    , getGroupParentsR

    , getGroupMemberLiveR
    , postGroupRemoveChildR
    , postGroupRemoveParentR
    , postGroupAddChildR
    , postGroupAddParentR
    , postGroupApproveChildR
    , postGroupApproveParentR

    , getGroupEffortsR
    , postGroupAddEffortR
    , postGroupApproveEffortR
    , postGroupRemoveEffortR





    {-
    , getGroupsR
    , postGroupMembersR
    , getGroupMemberNewR
    , getGroupMemberR
    , deleteGroupMemberR
    , postGroupMemberR
    -}
    )
where

import Control.Applicative
import Control.Arrow ((&&&))
import Control.Monad
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Aeson
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Default.Class
import Data.Foldable
import Data.List
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Network.HTTP.Types.Method
import Optics.Core
import Text.Blaze.Html (Html)
import Yesod.Auth
import Yesod.Core
import Yesod.Core.Handler (redirect, setMessage, lookupPostParam, notFound)
import Yesod.Form.Functions (runFormPost, runFormGet)
import Yesod.Form.Types (FormResult (..))
import Yesod.Form
import Yesod.Persist.Core (runDB, get404, getBy404)

import qualified Data.ByteString.Lazy as BL
import qualified Database.Esqueleto as E

import Database.Persist.JSON
import Development.PatchMediaType
import Network.FedURI
import Web.ActivityPub hiding (Project (..), Repo (..), Actor (..), ActorDetail (..), ActorLocal (..))
import Yesod.ActivityPub
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Data.Paginate.Local
import Database.Persist.Local
import Yesod.Form.Local
import Yesod.Persist.Local

import Vervis.Access
import Vervis.Actor.Group
import Vervis.API
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Federation.Auth
import Vervis.Federation.Discussion
import Vervis.Federation.Offer
import Vervis.Federation.Ticket
import Vervis.FedURI
import Vervis.Form.Ticket
import Vervis.Form.Tracker
import Vervis.Foundation
import Vervis.Model
import Vervis.Paginate
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Recipient
import Vervis.Serve.Collab
import Vervis.Settings
import Vervis.Ticket
import Vervis.TicketFilter
import Vervis.Time
import Vervis.Web.Actor
import Vervis.Widget
import Vervis.Widget.Person
import Vervis.Widget.Ticket
import Vervis.Widget.Tracker

import qualified Vervis.Client as C

getGroupNewR :: Handler Html
getGroupNewR = do
    p <- requireAuthId
    ((_result, widget), enctype) <- runFormPost $ newGroupForm p
    defaultLayout $(widgetFile "group/new")

postGroupNewR :: Handler Html
postGroupNewR = do
    personEntity@(Entity personID person) <- requireAuth
    NewGroup name desc (uFactory, uCap) <- runFormPostRedirect GroupNewR $ newGroupForm personID

    personHash <- encodeKeyHashid personID
    result <- runExceptT $ do
        (maybeSummary, audience, detail) <- C.createGroup personHash name desc uFactory
        (localRecips, remoteRecips, fwdHosts, action) <-
            lift $
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.CreateActivity $ AP.Create (AP.CreateTeam detail Nothing) (Just uFactory)
        cap <- parseActivityURI uCap
        handleViaActor personID (Just cap) localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
            redirect GroupNewR
        Right _createID -> do
            setMessage "Create activity sent"
            redirect HomeR

getGroupR :: KeyHashid Group -> Handler TypedContent
getGroupR groupHash = do
    groupID <- decodeKeyHashid404 groupHash
    mp <- maybeAuthId
    (group, actor, sigKeyIDs, permits) <- runDB $ do
        g <- get404 groupID
        let aid = groupActor g
        a <- getJust aid
        sigKeys <- selectKeysList [SigKeyActor ==. aid] [Asc SigKeyId]
        permits <-
            case mp of
                Nothing -> pure []
                Just personID -> getPermitsForResource personID (Left $ groupResource g)
        return (g, a, sigKeys, permits)

    encodeRouteLocal <- getEncodeRouteLocal
    hashSigKey <- getEncodeKeyHashid
    perActor <- asksSite $ appPerActorKeys . appSettings

    let route mk = encodeRouteLocal $ mk groupHash
        actorAP = AP.Actor
            { AP.actorLocal = AP.ActorLocal
                { AP.actorId         = route GroupR
                , AP.actorInbox      = route GroupInboxR
                , AP.actorOutbox     = Just $ route GroupOutboxR
                , AP.actorFollowers  = Just $ route GroupFollowersR
                , AP.actorFollowing  = Nothing
                , AP.actorPublicKeys =
                    map (Left . encodeRouteLocal) $
                    if perActor
                        then map (GroupStampR groupHash . hashSigKey) sigKeyIDs
                        else [ActorKey1R, ActorKey2R]
                , AP.actorSshKeys    = []
                }
            , AP.actorDetail = AP.ActorDetail
                { AP.actorType       = AP.ActorTypeTeam
                , AP.actorUsername   = Nothing
                , AP.actorName       = Just $ actorName actor
                , AP.actorSummary    = Just $ actorDesc actor
                , AP.actorOrigin     = Nothing
                }
            }
        groupAP = AP.Team
            { AP.teamActor    = actorAP
            , AP.teamChildren = encodeRouteLocal $ GroupChildrenR groupHash
            , AP.teamParents  = encodeRouteLocal $ GroupParentsR groupHash
            , AP.teamMembers  = encodeRouteLocal $ GroupMembersR groupHash
            , AP.teamResources = encodeRouteLocal $ GroupEffortsR groupHash
            }

    provideHtmlAndAP groupAP $(widgetFile "group/one")

getGroupInboxR :: KeyHashid Group -> Handler TypedContent
getGroupInboxR = getInbox GroupInboxR groupActor

getGroupErrboxR :: KeyHashid Group -> Handler TypedContent
getGroupErrboxR = getInbox' actorErrbox GroupErrboxR groupActor

postGroupInboxR :: KeyHashid Group -> Handler ()
postGroupInboxR groupHash = do
    groupID <- decodeKeyHashid404 groupHash
    postInbox LocalActorGroup groupID

getGroupOutboxR :: KeyHashid Group -> Handler TypedContent
getGroupOutboxR = getOutbox GroupOutboxR GroupOutboxItemR groupActor

getGroupOutboxItemR
    :: KeyHashid Group -> KeyHashid OutboxItem -> Handler TypedContent
getGroupOutboxItemR = getOutboxItem GroupOutboxItemR groupActor

getGroupFollowersR :: KeyHashid Group -> Handler TypedContent
getGroupFollowersR = getActorFollowersCollection GroupFollowersR groupActor

getGroupMessageR
    :: KeyHashid Group -> KeyHashid LocalMessage -> Handler TypedContent
getGroupMessageR _ _ = notFound

getGroupStampR :: KeyHashid Group -> KeyHashid SigKey -> Handler TypedContent
getGroupStampR = servePerActorKey groupActor LocalActorGroup

getGroupMembersR :: KeyHashid Group -> Handler TypedContent
getGroupMembersR groupHash = do
    groupID <- decodeKeyHashid404 groupHash
    (group, actor) <- runDB $ do
        group <- get404 groupID
        actor <- getJust $ groupActor group
        return (group, actor)
    serveCollabs
        AP.RelHasMember
        (groupResource group)
        (GroupR groupHash)
        (GroupMembersR groupHash)
        (GroupRemoveR groupHash)
        (GroupInviteR groupHash)
        Nothing
        (groupNavW (Entity groupID group) actor)

postGroupInviteR :: KeyHashid Group -> Handler Html
postGroupInviteR groupHash = do
    groupID <- decodeKeyHashid404 groupHash
    resourceID <- runDB $ groupResource <$> get404 groupID
    serveInviteCollab resourceID (GroupMembersR groupHash)

postGroupRemoveR :: KeyHashid Group -> CollabId -> Handler Html
postGroupRemoveR groupHash collabID = do
    groupID <- decodeKeyHashid404 groupHash
    resourceID <- runDB $ groupResource <$> get404 groupID
    serveRemoveCollab resourceID (GroupMembersR groupHash) collabID

getGroupChildrenR :: KeyHashid Group -> Handler TypedContent
getGroupChildrenR groupHash = do
    groupID <- decodeKeyHashid404 groupHash
    (actor, group, children) <- runDB $ do
        group <- get404 groupID
        actor <- getJust $ groupActor group
        children <- getChildren groupID
        return (actor, group, children)
    encodeRouteHome <- getEncodeRouteHome
    encodeRouteLocal <- getEncodeRouteLocal
    hashGroup <- getEncodeKeyHashid
    h <- asksSite siteInstanceHost
    let makeId (Left (childID, _)) =
            encodeRouteHome $ GroupR $ hashGroup childID
        makeId (Right (i, ro, _)) =
            ObjURI (instanceHost i) (remoteObjectIdent ro)
        makeItem (role, time, i, _) = AP.Relationship
            { AP.relationshipId           = Nothing
            , AP.relationshipExtraTypes   = []
            , AP.relationshipSubject      = encodeRouteHome $ GroupR groupHash
            , AP.relationshipProperty     = Left AP.RelHasChild
            , AP.relationshipObject       = makeId i
            , AP.relationshipAttributedTo = encodeRouteLocal $ GroupR groupHash
            , AP.relationshipPublished    = Just time
            , AP.relationshipUpdated      = Nothing
            , AP.relationshipInstrument   = Just role
            }
        childrenAP = Collection
            { collectionId         = encodeRouteLocal $ GroupChildrenR groupHash
            , collectionType       = CollectionTypeUnordered
            , collectionTotalItems = Just $ length children
            , collectionCurrent    = Nothing
            , collectionFirst      = Nothing
            , collectionLast       = Nothing
            , collectionItems      = map (Doc h . makeItem) children
            , collectionContext    =
                Just $ encodeRouteLocal $ GroupR groupHash
            }
    provideHtmlAndAP childrenAP $ getHtml groupID group actor children

    where

    getChildren groupID = fmap (sortOn $ view _2) $ liftA2 (++)
        (map (\ (E.Value role, E.Value time, E.Value child, Entity _ actor, E.Value destID) ->
                (role, time, Left (child, actor), destID)
             )
            <$> getLocals groupID
        )
        (map (\ (E.Value role, E.Value time, Entity _ i, Entity _ ro, Entity _ ra, E.Value destID) ->
                (role, time, Right (i, ro, ra), destID)
             )
            <$> getRemotes groupID
        )

    getLocals groupID =
        E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` group `E.InnerJoin` actor `E.InnerJoin` accept `E.InnerJoin` deleg `E.InnerJoin` grant) -> do
            E.on $ deleg E.^. DestThemSendDelegatorLocalGrant E.==. grant E.^. OutboxItemId
            E.on $ accept E.^. DestUsAcceptId E.==. deleg E.^. DestThemSendDelegatorLocalDest
            E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
            E.on $ group E.^. GroupActor E.==. actor E.^. ActorId
            E.on $ topic E.^. DestTopicGroupChild E.==. group E.^. GroupId
            E.on $ holder E.^. DestHolderGroupId E.==. topic E.^. DestTopicGroupHolder
            E.on $ dest E.^. DestId E.==. holder E.^. DestHolderGroupDest
            E.where_ $ holder E.^. DestHolderGroupGroup E.==. E.val groupID
            E.orderBy [E.asc $ grant E.^. OutboxItemPublished]
            return
                ( dest E.^. DestRole
                , grant E.^. OutboxItemPublished
                , topic E.^. DestTopicGroupChild
                , actor
                , dest E.^. DestId
                )

    getRemotes groupID =
        E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` deleg `E.InnerJoin` grant `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ topic E.^. DestTopicRemoteTopic E.==. ra E.^. RemoteActorId
            E.on $ deleg E.^. DestThemSendDelegatorRemoteGrant E.==. grant E.^. RemoteActivityId
            E.on $ accept E.^. DestUsAcceptId E.==. deleg E.^. DestThemSendDelegatorRemoteDest
            E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
            E.on $ dest E.^. DestId E.==. topic E.^. DestTopicRemoteDest
            E.on $ dest E.^. DestId E.==. holder E.^. DestHolderGroupDest
            E.where_ $ holder E.^. DestHolderGroupGroup E.==. E.val groupID
            E.orderBy [E.asc $ grant E.^. RemoteActivityReceived]
            return
                ( dest E.^. DestRole
                , grant E.^. RemoteActivityReceived
                , i
                , ro
                , ra
                , dest E.^. DestId
                )

    getHtml groupID group actor children = do
        mp <- maybeAuthId
        haveAdmin <- fmap isJust $ handlerToWidget $ runDB $ runMaybeT $ do
            personID <- MaybeT $ pure mp
            MaybeT $ getCapability personID (Left $ groupResource group) AP.RoleAdmin
        ((_, widgetAP), enctypeAP) <- runFormPost addChildForm
        invites <- handlerToWidget $ runDB $ do
            dests <- E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.LeftOuterJoin` accept `E.LeftOuterJoin` delegl `E.LeftOuterJoin` delegr) -> do
                E.on $ accept E.?. DestUsAcceptId E.==. delegr E.?. DestThemSendDelegatorRemoteDest
                E.on $ accept E.?. DestUsAcceptId E.==. delegl E.?. DestThemSendDelegatorLocalDest
                E.on $ E.just (dest E.^. DestId) E.==. accept E.?. DestUsAcceptDest
                E.on $ dest E.^. DestId E.==. holder E.^. DestHolderGroupDest
                E.where_ $
                    holder E.^. DestHolderGroupGroup E.==. E.val groupID E.&&.
                    E.isNothing (delegl E.?. DestThemSendDelegatorLocalId) E.&&.
                    E.isNothing (delegr E.?. DestThemSendDelegatorRemoteId)
                E.orderBy [E.asc $ dest E.^. DestId]
                return dest
            for dests $ \ (Entity destID (Dest role)) -> do
                child <- do
                    topic <- getDestTopic destID
                    bitraverse
                        (\ (_, e) -> do
                            jID <-
                                case e of
                                    Right j -> pure j
                                    Left _ -> error "I'm a Group but my child is a Project"
                            j <- getJust jID
                            actor <- getJust $ groupActor j
                            return (jID, actor)
                        )
                        (\ (_, actorID) -> getRemoteActorData actorID)
                        topic
                accept <- isJust <$> getBy (UniqueDestUsAccept destID)
                ((inviter, time), us) <- do
                    usOrThem <-
                        requireEitherAlt
                            (getKeyBy $ UniqueDestOriginUs destID)
                            (getKeyBy $ UniqueDestOriginThem destID)
                            "Neither us nor them"
                            "Both us and them"
                    (addOrActor, us) <-
                        case usOrThem of
                            Left _usID -> (,True) <$>
                                requireEitherAlt
                                    (fmap destUsGestureLocalActivity <$> getValBy (UniqueDestUsGestureLocal destID))
                                    (fmap (destUsGestureRemoteActor &&& destUsGestureRemoteActivity) <$> getValBy (UniqueDestUsGestureRemote destID))
                                    "Neither local not remote"
                                    "Both local and remote"
                            Right themID -> (,False) <$>
                                requireEitherAlt
                                    (fmap destThemGestureLocalAdd <$> getValBy (UniqueDestThemGestureLocal themID))
                                    (fmap (destThemGestureRemoteActor &&& destThemGestureRemoteAdd) <$> getValBy (UniqueDestThemGestureRemote themID))
                                    "Neither local not remote"
                                    "Both local and remote"
                    (,us) <$> case addOrActor of
                        Left addID -> do
                            OutboxItem outboxID _ time <- getJust addID
                            Entity actorID actor <- getByJust $ UniqueActorOutbox outboxID
                            (,time) . Left . (,actor) <$> getLocalActor actorID
                        Right (actorID, addID) -> do
                            RemoteActivity _ _ time <- getJust addID
                            (,time) . Right <$> getRemoteActorData actorID
                return (inviter, us, child, accept, time, role, destID)
        $(widgetFile "group/children")
        where
        getRemoteActorData actorID = do
            actor <- getJust actorID
            object <- getJust $ remoteActorIdent actor
            inztance <- getJust $ remoteObjectInstance object
            return (inztance, object, actor)

getGroupChildLiveR :: KeyHashid Group -> KeyHashid DestUsStart -> Handler ()
getGroupChildLiveR groupHash startHash = do
    groupID <- decodeKeyHashid404 groupHash
    startID <- decodeKeyHashid404 startHash
    runDB $ do
        _ <- get404 groupID
        DestUsStart usAcceptID _ <- get404 startID
        DestUsAccept destID _ <- getJust usAcceptID
        Entity _ (DestHolderGroup _ g) <-
            getBy404 $ UniqueDestHolderGroup destID
        unless (g == groupID) notFound

getGroupParentsR :: KeyHashid Group -> Handler TypedContent
getGroupParentsR groupHash = do
    groupID <- decodeKeyHashid404 groupHash
    (actor, group, parents) <- runDB $ do
        group <- get404 groupID
        actor <- getJust $ groupActor group
        parents <- getParents groupID
        return (actor, group, parents)
    encodeRouteHome <- getEncodeRouteHome
    encodeRouteLocal <- getEncodeRouteLocal
    hashGroup <- getEncodeKeyHashid
    h <- asksSite siteInstanceHost
    let makeId (Left (parentID, _)) =
            encodeRouteHome $ GroupR $ hashGroup parentID
        makeId (Right (i, ro, _)) =
            ObjURI (instanceHost i) (remoteObjectIdent ro)
        makeItem (role, time, i, _) = AP.Relationship
            { AP.relationshipId           = Nothing
            , AP.relationshipExtraTypes   = []
            , AP.relationshipSubject      = encodeRouteHome $ GroupR groupHash
            , AP.relationshipProperty     = Left AP.RelHasParent
            , AP.relationshipObject       = makeId i
            , AP.relationshipAttributedTo = encodeRouteLocal $ GroupR groupHash
            , AP.relationshipPublished    = Just time
            , AP.relationshipUpdated      = Nothing
            , AP.relationshipInstrument   = Just role
            }
        parentsAP = Collection
            { collectionId         = encodeRouteLocal $ GroupParentsR groupHash
            , collectionType       = CollectionTypeUnordered
            , collectionTotalItems = Just $ length parents
            , collectionCurrent    = Nothing
            , collectionFirst      = Nothing
            , collectionLast       = Nothing
            , collectionItems      = map (Doc h . makeItem) parents
            , collectionContext    =
                Just $ encodeRouteLocal $ GroupR groupHash
            }
    provideHtmlAndAP parentsAP $ getHtml groupID group actor parents

    where

    getParents groupID = fmap (sortOn $ view _2) $ liftA2 (++)
        (map (\ (E.Value role, E.Value time, E.Value parent, Entity _ actor, E.Value sourceID) ->
                (role, time, Left (parent, actor), sourceID)
             )
            <$> getLocals groupID
        )
        (map (\ (E.Value role, E.Value time, Entity _ i, Entity _ ro, Entity _ ra, E.Value sourceID) ->
                (role, time, Right (i, ro, ra), sourceID)
             )
            <$> getRemotes groupID
        )

    getLocals groupID =
        E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` group `E.InnerJoin` actor `E.InnerJoin` deleg `E.InnerJoin` grant) -> do
            E.on $ deleg E.^. SourceUsSendDelegatorGrant E.==. grant E.^. OutboxItemId
            E.on $ source E.^. SourceId E.==. deleg E.^. SourceUsSendDelegatorSource
            E.on $ group E.^. GroupActor E.==. actor E.^. ActorId
            E.on $ topic E.^. SourceTopicGroupParent E.==. group E.^. GroupId
            E.on $ holder E.^. SourceHolderGroupId E.==. topic E.^. SourceTopicGroupHolder
            E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderGroupSource
            E.where_ $ holder E.^. SourceHolderGroupGroup E.==. E.val groupID
            E.orderBy [E.asc $ deleg E.^. SourceUsSendDelegatorId]
            return
                ( source E.^. SourceRole
                , grant E.^. OutboxItemPublished
                , topic E.^. SourceTopicGroupParent
                , actor
                , source E.^. SourceId
                )

    getRemotes groupID =
        E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` grant `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ topic E.^. SourceTopicRemoteTopic E.==. ra E.^. RemoteActorId
            E.on $ deleg E.^. SourceUsSendDelegatorGrant E.==. grant E.^. OutboxItemId
            E.on $ source E.^. SourceId E.==. deleg E.^. SourceUsSendDelegatorSource
            E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicRemoteSource
            E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderGroupSource
            E.where_ $ holder E.^. SourceHolderGroupGroup E.==. E.val groupID
            E.orderBy [E.asc $ deleg E.^. SourceUsSendDelegatorId]
            return
                ( source E.^. SourceRole
                , grant E.^. OutboxItemPublished
                , i
                , ro
                , ra
                , source E.^. SourceId
                )

    getHtml groupID group actor parents = do
        mp <- maybeAuthId
        haveAdmin <- fmap isJust $ handlerToWidget $ runDB $ runMaybeT $ do
            personID <- MaybeT $ pure mp
            MaybeT $ getCapability personID (Left $ groupResource group) AP.RoleAdmin
        ((_, widgetAC), enctypeAC) <- runFormPost addParentForm
        invites <- handlerToWidget $ runDB $ do
            sources <- E.select $ E.from $ \ (source `E.InnerJoin` holder `E.LeftOuterJoin` deleg) -> do
                E.on $ E.just (source E.^. SourceId) E.==. deleg E.?. SourceUsSendDelegatorSource
                E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderGroupSource
                E.where_ $
                    holder E.^. SourceHolderGroupGroup E.==. E.val groupID E.&&.
                    E.isNothing (deleg E.?. SourceUsSendDelegatorId)
                E.orderBy [E.asc $ source E.^. SourceId]
                return source
            for sources $ \ (Entity sourceID (Source role)) -> do
                (parent, accept) <- do
                    topic <- getSourceTopic sourceID
                    accept <-
                        case bimap fst fst topic of
                            Left localID -> isJust <$> getBy (UniqueSourceThemAcceptLocal localID)
                            Right remoteID -> isJust <$> getBy (UniqueSourceThemAcceptRemote remoteID)
                    (,accept) <$> bitraverse
                        (\ (_, e) -> do
                            jID <-
                                case e of
                                    Right j -> pure j
                                    Left _ -> error "I'm a Group but my parent is a Project"
                            j <- getJust jID
                            actor <- getJust $ groupActor j
                            return (jID, actor)
                        )
                        (\ (_, actorID) -> getRemoteActorData actorID)
                        topic
                ((inviter, time), us) <- do
                    usOrThem <-
                        requireEitherAlt
                            (getKeyBy $ UniqueSourceOriginUs sourceID)
                            (getKeyBy $ UniqueSourceOriginThem sourceID)
                            "Neither us nor them"
                            "Both us and them"
                    (addOrActor, us) <-
                        case usOrThem of
                            Left usID -> (,True) <$>
                                requireEitherAlt
                                    (fmap sourceUsGestureLocalAdd <$> getValBy (UniqueSourceUsGestureLocal usID))
                                    (fmap (sourceUsGestureRemoteActor &&& sourceUsGestureRemoteAdd) <$> getValBy (UniqueSourceUsGestureRemote usID))
                                    "Neither local not remote"
                                    "Both local and remote"
                            Right themID -> (,False) <$>
                                requireEitherAlt
                                    (fmap sourceThemGestureLocalAdd <$> getValBy (UniqueSourceThemGestureLocal themID))
                                    (fmap (sourceThemGestureRemoteActor &&& sourceThemGestureRemoteAdd) <$> getValBy (UniqueSourceThemGestureRemote themID))
                                    "Neither local not remote"
                                    "Both local and remote"
                    (,us) <$> case addOrActor of
                        Left addID -> do
                            OutboxItem outboxID _ time <- getJust addID
                            Entity actorID actor <- getByJust $ UniqueActorOutbox outboxID
                            (,time) . Left . (,actor) <$> getLocalActor actorID
                        Right (actorID, addID) -> do
                            RemoteActivity _ _ time <- getJust addID
                            (,time) . Right <$> getRemoteActorData actorID
                return (inviter, us, parent, accept, time, role, sourceID)
        $(widgetFile "group/parents")
        where
        getRemoteActorData actorID = do
            actor <- getJust actorID
            object <- getJust $ remoteActorIdent actor
            inztance <- getJust $ remoteObjectInstance object
            return (inztance, object, actor)

getGroupMemberLiveR
    :: KeyHashid Group -> KeyHashid CollabEnable -> Handler ()
getGroupMemberLiveR groupHash enableHash = do
    groupID <- decodeKeyHashid404 groupHash
    enableID <- decodeKeyHashid404 enableHash
    runDB $ do
        resourceID <- groupResource <$> get404 groupID
        CollabEnable collabID _ <- get404 enableID
        Collab _ resourceID' <- getJust collabID
        unless (resourceID == resourceID') notFound

postGroupRemoveParentR :: KeyHashid Group -> SourceId -> Handler Html
postGroupRemoveParentR groupHash sourceID = do
    groupID <- decodeKeyHashid404 groupHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            group <- MaybeT $ get groupID
            _ <- MaybeT $ get sourceID
            SourceHolderGroup _ j <-
                MaybeT $ getValBy $ UniqueSourceHolderGroup sourceID
            guard $ groupID == j
            _ <- MaybeT $ getBy $ UniqueSourceUsSendDelegator sourceID

            topic <- lift $ do
                t <- bimap snd snd <$> getSourceTopic sourceID
                bitraverse
                    (\case
                        Right g' -> pure g'
                        Left _j -> error "I'm a group, I have a Source with topic being Project"
                    )
                    pure
                    t
            lift $
                (groupResource group,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uParent <-
                case pidOrU of
                    Left j -> encodeRouteHome . GroupR <$> encodeKeyHashid j
                    Right u -> pure u
            let uCollection = encodeRouteHome $ GroupParentsR groupHash
            C.remove personID uParent uCollection
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Group to remove parents"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Remove sent"
    redirect $ GroupParentsR groupHash

postGroupRemoveChildR :: KeyHashid Group -> DestId -> Handler Html
postGroupRemoveChildR groupHash destID = do
    groupID <- decodeKeyHashid404 groupHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            group <- MaybeT $ get groupID
            _ <- MaybeT $ get destID
            DestHolderGroup _ j <-
                MaybeT $ getValBy $ UniqueDestHolderGroup destID
            guard $ groupID == j
            acceptID <- MaybeT $ getKeyBy $ UniqueDestUsAccept destID
            _ <- MaybeT $ getBy $ UniqueDestUsStart acceptID

            topic <- lift $ do
                t <- bimap snd snd <$> getDestTopic destID
                bitraverse
                    (\case
                        Right g' -> pure g'
                        Left _j -> error "I'm a group, I have a Dest with topic being Project"
                    )
                    pure
                    t
            lift $
                (groupResource group,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uChild <-
                case pidOrU of
                    Left j -> encodeRouteHome . GroupR <$> encodeKeyHashid j
                    Right u -> pure u
            let uCollection = encodeRouteHome $ GroupChildrenR groupHash
            C.remove personID uChild uCollection
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Group to remove children"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Remove sent"
    redirect $ GroupChildrenR groupHash

addChildForm = renderDivs $
    areq fedUriField "(URI) Child group" Nothing

postGroupAddChildR :: KeyHashid Group -> Handler Html
postGroupAddChildR groupHash = do
    uChild <- runFormPostRedirect (GroupChildrenR groupHash) addChildForm
    encodeRouteHome <- getEncodeRouteHome
    let uCollection = encodeRouteHome $ GroupChildrenR groupHash

    groupID <- decodeKeyHashid404 groupHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID

    result <- runExceptT $ do
        group <- lift $ runDB $ get404 groupID
        (maybeSummary, audience, add) <- C.add personID uChild uCollection AP.RoleAdmin
        cap <- do
            let resourceID = groupResource group
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Group to add children"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AddActivity add
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Add sent"
    redirect $ GroupChildrenR groupHash

addParentForm = renderDivs $
    areq fedUriField "(URI) Parent group" Nothing

postGroupAddParentR :: KeyHashid Group -> Handler Html
postGroupAddParentR groupHash = do
    uParent <- runFormPostRedirect (GroupParentsR groupHash) addParentForm
    encodeRouteHome <- getEncodeRouteHome
    let uCollection = encodeRouteHome $ GroupParentsR groupHash

    groupID <- decodeKeyHashid404 groupHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID

    result <- runExceptT $ do
        group <- lift $ runDB $ get404 groupID
        (maybeSummary, audience, add) <- C.add personID uParent uCollection AP.RoleAdmin
        cap <- do
            let resourceID = groupResource group
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Group to add parents"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AddActivity add
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Add sent"
    redirect $ GroupParentsR groupHash

postGroupApproveParentR :: KeyHashid Group -> SourceId -> Handler Html
postGroupApproveParentR groupHash sourceID = do
    groupID <- decodeKeyHashid404 groupHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            group <- MaybeT $ get groupID
            _ <- MaybeT $ get sourceID
            SourceHolderGroup _ j <-
                MaybeT $ getValBy $ UniqueSourceHolderGroup sourceID
            guard $ groupID == j

            uAdd <- lift $ do
                add <- getSourceAdd sourceID
                renderActivityURI add

            topic <- lift $ do
                t <- bimap snd snd <$> getSourceTopic sourceID
                bitraverse
                    (\case
                        Right g' -> pure g'
                        Left _j -> error "I'm a group, I have a Source with topic being Project"
                    )
                    pure
                    t
            lift $
                (groupResource group,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, accept) <- do
            uParent <-
                case pidOrU of
                    Left j -> encodeRouteHome . GroupR <$> encodeKeyHashid j
                    Right u -> pure u
            let uChild = encodeRouteHome $ GroupR groupHash
            C.acceptParentChild personID uAdd uParent uChild
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Group to approve parents"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AcceptActivity accept
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Accept sent"
    redirect $ GroupParentsR groupHash

postGroupApproveChildR :: KeyHashid Group -> DestId -> Handler Html
postGroupApproveChildR groupHash destID = do
    groupID <- decodeKeyHashid404 groupHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            group <- MaybeT $ get groupID
            _ <- MaybeT $ get destID
            DestHolderGroup _ j <-
                MaybeT $ getValBy $ UniqueDestHolderGroup destID
            guard $ groupID == j

            uAdd <- lift $ do
                add <- getDestAdd destID
                renderActivityURI add

            topic <- lift $ do
                t <- bimap snd snd <$> getDestTopic destID
                bitraverse
                    (\case
                        Right g' -> pure g'
                        Left _j -> error "I'm a group, I have a Dest with topic being Project"
                    )
                    pure
                    t
            lift $
                (groupResource group,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, accept) <- do
            uChild <-
                case pidOrU of
                    Left j -> encodeRouteHome . GroupR <$> encodeKeyHashid j
                    Right u -> pure u
            let uParent = encodeRouteHome $ GroupR groupHash
            C.acceptParentChild personID uAdd uParent uChild
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Group to approve children"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AcceptActivity accept
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Accept sent"
    redirect $ GroupChildrenR groupHash

getGroupEffortsR :: KeyHashid Group -> Handler TypedContent
getGroupEffortsR groupHash = do
    groupID <- decodeKeyHashid404 groupHash
    (group, actor, efforts) <- runDB $ do
        group <- get404 groupID
        actor <- getJust $ groupActor group
        efforts <- getTeamResources groupID
        return (group, actor, efforts)
    h <- asksSite siteInstanceHost
    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    hashActor <- getHashLocalActor
    let meR = GroupR groupHash
        makeItem (role, time, resource, _) = AP.Relationship
            { AP.relationshipId           = Nothing
            , AP.relationshipExtraTypes   = []
            , AP.relationshipSubject       =
                case resource of
                    Left (la, _) ->
                        encodeRouteHome $ renderLocalActor $ hashActor la
                    Right (i, ro, _) ->
                        ObjURI (instanceHost i) (remoteObjectIdent ro)
            , AP.relationshipProperty     = Left AP.RelHasRecCollab
            , AP.relationshipObject      = encodeRouteHome meR
            , AP.relationshipAttributedTo = encodeRouteLocal meR
            , AP.relationshipPublished    = Just time
            , AP.relationshipUpdated      = Nothing
            , AP.relationshipInstrument   = Just role
            }
        effortsAP = AP.Collection
            { AP.collectionId         = encodeRouteLocal $ GroupEffortsR groupHash
            , AP.collectionType       = CollectionTypeUnordered
            , AP.collectionTotalItems = Just $ length efforts
            , AP.collectionCurrent    = Nothing
            , AP.collectionFirst      = Nothing
            , AP.collectionLast       = Nothing
            , AP.collectionItems      = map (Doc h . makeItem) efforts
            , AP.collectionContext    = Just $ encodeRouteLocal meR
            }
    provideHtmlAndAP effortsAP $ getHtml groupID group actor efforts
    where
    getHtml groupID group actor efforts = do
        mp <- maybeAuthId
        haveAdmin <- fmap isJust $ handlerToWidget $ runDB $ runMaybeT $ do
            personID <- MaybeT $ pure mp
            MaybeT $ getCapability personID (Left $ groupResource group) AP.RoleAdmin
        ((_, widgetAE), enctypeAE) <- handlerToWidget $ runFormPost addEffortForm
        drafts <- handlerToWidget $ runDB $ getTeamResourceDrafts groupID
        $(widgetFile "group/efforts")

addEffortForm = renderDivs $ (,)
    <$> areq fedUriField "Resource actor URI*" Nothing
    <*> areq selectRole  "Role*"               Nothing
    where
    selectRole :: Field Handler AP.Role
    selectRole = selectField optionsEnum

postGroupAddEffortR :: KeyHashid Group -> Handler Html
postGroupAddEffortR groupHash = do
    (uEffort, role) <-
        runFormPostRedirect (GroupChildrenR groupHash) addEffortForm
    encodeRouteHome <- getEncodeRouteHome
    let uCollection = encodeRouteHome $ GroupEffortsR groupHash

    groupID <- decodeKeyHashid404 groupHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID

    result <- runExceptT $ do
        group <- lift $ runDB $ get404 groupID
        (maybeSummary, audience, add) <- C.add personID uEffort uCollection role
        cap <- do
            let resourceID = groupResource group
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Group to add resources"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AddActivity add
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Add sent"
    redirect $ GroupEffortsR groupHash

postGroupApproveEffortR :: KeyHashid Group -> EffortId -> Handler Html
postGroupApproveEffortR groupHash effortID = do
    groupID <- decodeKeyHashid404 groupHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            group <- MaybeT $ get groupID
            Effort _ g <- MaybeT $ get effortID
            guard $ groupID == g

            uAdd <- lift $ do
                add <- getEffortAdd effortID
                renderActivityURI add

            topic <- lift $ bimap snd snd <$> getEffortTopic effortID
            lift $
                (groupResource group,uAdd,) <$>
                bitraverse
                    getLocalResource
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, accept) <- do
            uEffort <-
                case pidOrU of
                    Left lr -> encodeRouteHome . renderLocalResource <$> hashLocalResource lr
                    Right u -> pure u
            let uMe = encodeRouteHome $ GroupR groupHash
            C.acceptParentChild personID uAdd uEffort uMe
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Group to approve resources"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AcceptActivity accept
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Accept sent"
    redirect $ GroupEffortsR groupHash

postGroupRemoveEffortR :: KeyHashid Group -> EffortId -> Handler Html
postGroupRemoveEffortR groupHash effortID = do
    groupID <- decodeKeyHashid404 groupHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            group <- MaybeT $ get groupID
            Effort _ g <- MaybeT $ get effortID
            guard $ groupID == g
            _ <- MaybeT $ getBy $ UniqueEffortUsSendDelegator effortID

            topic <- lift $ bimap snd snd <$> getEffortTopic effortID
            lift $
                (groupResource group,) <$>
                bitraverse
                    getLocalResource
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uEffort <-
                case pidOrU of
                    Left lr -> encodeRouteHome . renderLocalResource <$> hashLocalResource lr
                    Right u -> pure u
            let uCollection = encodeRouteHome $ GroupEffortsR groupHash
            C.remove personID uEffort uCollection
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Group to remove resources"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Remove sent"
    redirect $ GroupEffortsR groupHash













{-
getGroupsR :: Handler Html
getGroupsR = do
    groups <- runDB $ select $ from $ \ (sharer, group) -> do
        where_ $ sharer ^. SharerId E.==. group ^. GroupIdent
        orderBy [asc $ sharer ^. SharerIdent]
        return sharer
    defaultLayout $(widgetFile "group/list")

getgid :: ShrIdent -> AppDB GroupId
getgid shar = do
    Entity s _ <- getBy404 $ UniqueSharer shar
    Entity g _ <- getBy404 $ UniqueGroup s
    return g

postGroupMembersR :: ShrIdent -> Handler Html
postGroupMembersR shar = do
    ((result, widget), enctype) <-
        runFormPost $ newGroupMemberForm $ getgid shar
    case result of
        FormSuccess ngm -> do
            now <- liftIO getCurrentTime
            runDB $ do
                gid <- getgid shar
                pid <- do
                    Entity s _ <- getBy404 $ UniqueSharer $ ngmIdent ngm
                    Entity p _ <- getBy404 $ UniquePersonIdent s
                    return p
                let member = GroupMember
                        { groupMemberPerson = pid
                        , groupMemberGroup  = gid
                        , groupMemberRole   = ngmRole ngm
                        , groupMemberJoined = now
                        }
                insert_ member
            redirect $ GroupMemberR shar $ ngmIdent ngm
        FormMissing -> do
            setMessage "Field(s) missing"
            defaultLayout $(widgetFile "group/member/new")
        FormFailure _l -> do
            setMessage "Member insertion failed, see errors below"
            defaultLayout $(widgetFile "group/member/new")

getGroupMemberNewR :: ShrIdent -> Handler Html
getGroupMemberNewR shar = do
    ((_result, widget), enctype) <-
        runFormPost $ newGroupMemberForm $ getgid shar
    defaultLayout $(widgetFile "group/member/new")

getGroupMemberR :: ShrIdent -> ShrIdent -> Handler Html
getGroupMemberR grp memb = do
    member <- runDB $ do
        gid <- do
            Entity s _ <- getBy404 $ UniqueSharer grp
            Entity g _ <- getBy404 $ UniqueGroup s
            return g
        pid <- do
            Entity s _ <- getBy404 $ UniqueSharer memb
            Entity p _ <- getBy404 $ UniquePersonIdent s
            return p
        Entity _mid m <- getBy404 $ UniqueGroupMember pid gid
        return m
    defaultLayout $(widgetFile "group/member/one")

deleteGroupMemberR :: ShrIdent -> ShrIdent -> Handler Html
deleteGroupMemberR grp memb = do
    succ <- runDB $ do
        gid <- do
            Entity s _ <- getBy404 $ UniqueSharer grp
            Entity g _ <- getBy404 $ UniqueGroup s
            return g
        pid <- do
            Entity s _ <- getBy404 $ UniqueSharer memb
            Entity p _ <- getBy404 $ UniquePersonIdent s
            return p
        mm <-
            selectFirst
                [ GroupMemberGroup  ==. gid
                , GroupMemberPerson !=. pid
                , GroupMemberRole   ==. GRAdmin
                ]
                []
        case mm of
            Nothing -> return False
            Just _  -> do
                Entity mid _m <- getBy404 $ UniqueGroupMember pid gid
                delete mid
                return True
    setMessage $
        if succ
            then "Group member removed."
            else "Can’t leave a group without an admin."
    redirect $ GroupMembersR grp

postGroupMemberR :: ShrIdent -> ShrIdent -> Handler Html
postGroupMemberR grp memb = do
    mmethod <- lookupPostParam "_method"
    case mmethod of
        Just "DELETE" -> deleteGroupMemberR grp memb
        _             -> notFound
-}
