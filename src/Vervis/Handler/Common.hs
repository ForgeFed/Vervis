{- This file is part of Vervis.
 -
 - Written in 2016, 2019, 2022 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

-- | Common handler functions.
module Vervis.Handler.Common
    ( getFaviconSvgR
    , getFaviconPngR
    , getRobotsR
    )
where

import Data.FileEmbed
import Yesod.Core

import Vervis.Foundation

-- These handlers embed files in the executable at compile time to avoid a
-- runtime dependency, and for efficiency.

getFaviconSvgR :: Handler TypedContent
getFaviconSvgR =
    return $
    TypedContent "image/svg+xml" $
    toContent $(embedFile "embed/favicon.svg")

getFaviconPngR :: Handler TypedContent
getFaviconPngR =
    return $
    TypedContent "image/png" $
    toContent $(embedFile "embed/favicon.png")

getRobotsR :: Handler TypedContent
getRobotsR =
    return $
    TypedContent typePlain $
    toContent $(embedFile "embed/robots.txt")
