{- This file is part of Vervis.
 -
 - Written in 2016, 2019, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Handler.Factory
    ( getFactoryR
    , getFactoryInboxR
    , getFactoryErrboxR
    , postFactoryInboxR
    , getFactoryOutboxR
    , getFactoryOutboxItemR
    , getFactoryFollowersR

    , getFactoryMessageR

    , getFactoryNewR
    , postFactoryNewR
    , postFactoryEditR

    , getFactoryStampR

    , getFactoryCollabsR
    , postFactoryInviteR
    , postFactoryRemoveR

    , getFactoryTeamsR

    , postFactoryAddTeamR
    , postFactoryApproveTeamR
    , postFactoryRemoveTeamR
    , getFactoryTeamLiveR
    )
where

import Control.Applicative
import Control.Arrow ((&&&))
import Control.Monad
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Aeson
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Default.Class
import Data.Foldable
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Network.HTTP.Types.Method
import Optics.Core
import Text.Blaze.Html (Html)
import Yesod.Auth
import Yesod.Core
import Yesod.Core.Handler (redirect, setMessage, lookupPostParam, notFound)
import Yesod.Form
import Yesod.Form.Functions (runFormPost, runFormGet)
import Yesod.Form.Types (FormResult (..))
import Yesod.Persist.Core (runDB, get404, getBy404)

import qualified Data.ByteString.Lazy as BL
import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Database.Persist.JSON
import Development.PatchMediaType
import Network.FedURI
import Web.ActivityPub hiding (Project (..), Repo (..), Actor (..), ActorDetail (..), ActorLocal (..), Factory)
import Yesod.ActivityPub
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Data.Paginate.Local
import Database.Persist.Local
import Yesod.Form.Local
import Yesod.Persist.Local

import Vervis.Access
import Vervis.API
import Vervis.Actor.Factory
import Vervis.Data.Actor
import Vervis.Federation.Auth
import Vervis.Federation.Discussion
import Vervis.Federation.Offer
import Vervis.Federation.Ticket
import Vervis.FedURI
import Vervis.Form.Ticket
import Vervis.Form.Tracker
import Vervis.Foundation
import Vervis.Model
import Vervis.Paginate
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Recipient
import Vervis.Serve.Collab
import Vervis.Settings
import Vervis.Ticket
import Vervis.TicketFilter
import Vervis.Time
import Vervis.Web.Actor
import Vervis.Web.Collab
import Vervis.Widget
import Vervis.Widget.Person
import Vervis.Widget.Ticket
import Vervis.Widget.Tracker

import qualified Vervis.Client as C

getFactoryR :: KeyHashid Factory -> Handler TypedContent
getFactoryR factoryHash = do
    factoryID <- decodeKeyHashid404 factoryHash
    mp <- maybeAuthId
    (factory, actorID, actor, sigKeyIDs, permits) <- runDB $ do
        f <- get404 factoryID
        Resource aid <- getJust $ factoryResource f
        a <- getJust aid
        sigKeys <- selectKeysList [SigKeyActor ==. aid] [Asc SigKeyId]
        permits <-
            case mp of
                Nothing -> pure []
                Just personID -> getPermitsForResource personID (Left $ factoryResource f)
        return (f, aid, a, sigKeys, permits)

    let defTypes =
            mapMaybe
            (\ (allow, typ) ->
                if allow factory
                    then Just typ
                    else Nothing
            )
            [ (factoryAllowDeck   , AP.ActorTypeTicketTracker)
            , (factoryAllowProject, AP.ActorTypeProject)
            , (factoryAllowTeam   , AP.ActorTypeTeam)
            , (factoryAllowRepo   , AP.ActorTypeRepo)
            ]
    ((_result, widget), enctype) <-
        runFormPost $ renderDivs $ areq typeField "Allowed types" (Just defTypes)

    encodeRouteLocal <- getEncodeRouteLocal
    hashSigKey <- getEncodeKeyHashid
    perActor <- asksSite $ appPerActorKeys . appSettings
    let factoryAP = AP.Factory
            { AP.factoryActor = AP.Actor
                { AP.actorLocal = AP.ActorLocal
                    { AP.actorId         = encodeRouteLocal $ FactoryR factoryHash
                    , AP.actorInbox      = encodeRouteLocal $ FactoryInboxR factoryHash
                    , AP.actorOutbox     =
                        Just $ encodeRouteLocal $ FactoryOutboxR factoryHash
                    , AP.actorFollowers  =
                        Just $ encodeRouteLocal $ FactoryFollowersR factoryHash
                    , AP.actorFollowing  = Nothing
                    , AP.actorPublicKeys =
                        map (Left . encodeRouteLocal) $
                        if perActor
                            then map (FactoryStampR factoryHash . hashSigKey) sigKeyIDs
                            else [ActorKey1R, ActorKey2R]
                    , AP.actorSshKeys    = []
                    }
                , AP.actorDetail = AP.ActorDetail
                    { AP.actorType       = AP.ActorTypeFactory
                    , AP.actorUsername   = Nothing
                    , AP.actorName       = Just $ actorName actor
                    , AP.actorSummary    = Just $ actorDesc actor
                    , AP.actorOrigin     = Nothing
                    }
                }
            , AP.factoryCollabs =
                encodeRouteLocal $ FactoryCollabsR factoryHash
            , AP.factoryTeams =
                encodeRouteLocal $ FactoryTeamsR factoryHash
            , AP.factoryTypes =
                mapMaybe
                (\ (allow, typ) ->
                    if allow factory
                        then Just typ
                        else Nothing
                )
                [ (factoryAllowDeck   , AP.ActorTypeTicketTracker)
                , (factoryAllowProject, AP.ActorTypeProject)
                , (factoryAllowTeam   , AP.ActorTypeTeam)
                , (factoryAllowRepo   , AP.ActorTypeRepo)
                ]
            }

    provideHtmlAndAP factoryAP $(widgetFile "factory/one")

grabActorID = fmap resourceActor . getJust . factoryResource

getFactoryInboxR :: KeyHashid Factory -> Handler TypedContent
getFactoryInboxR = getInbox'' actorInbox FactoryInboxR grabActorID

getFactoryErrboxR :: KeyHashid Factory -> Handler TypedContent
getFactoryErrboxR = getInbox'' actorErrbox FactoryErrboxR grabActorID

postFactoryInboxR :: KeyHashid Factory -> Handler ()
postFactoryInboxR factoryHash = do
    factoryID <- decodeKeyHashid404 factoryHash
    postInbox LocalActorFactory factoryID

getFactoryOutboxR :: KeyHashid Factory -> Handler TypedContent
getFactoryOutboxR = getOutbox' FactoryOutboxR FactoryOutboxItemR grabActorID

getFactoryOutboxItemR
    :: KeyHashid Factory -> KeyHashid OutboxItem -> Handler TypedContent
getFactoryOutboxItemR = getOutboxItem' FactoryOutboxItemR grabActorID

getFactoryFollowersR :: KeyHashid Factory -> Handler TypedContent
getFactoryFollowersR = getActorFollowersCollection' FactoryFollowersR grabActorID

getFactoryMessageR :: KeyHashid Factory -> KeyHashid LocalMessage -> Handler Html
getFactoryMessageR _ _ = notFound

typeField =
    checkboxesFieldList
        [ ("Repo", AP.ActorTypeRepo) :: (Text, AP.ActorType)
        , ("Ticket Tracker", AP.ActorTypeTicketTracker)
        , ("Project", AP.ActorTypeProject)
        , ("Team", AP.ActorTypeTeam)
        ]

allTypes = [AP.ActorTypeTicketTracker, AP.ActorTypeProject, AP.ActorTypeTeam, AP.ActorTypeRepo]

newFactoryForm = renderDivs $ (,,)
    <$> areq textField "Name*"        Nothing
    <*> areq textField "Description*" Nothing
    <*> areq typeField "Allowed types*" (Just allTypes)

getFactoryNewR :: Handler Html
getFactoryNewR = do
    ((_result, widget), enctype) <- runFormPost newFactoryForm
    defaultLayout $(widgetFile "factory/new")

postFactoryNewR :: Handler Html
postFactoryNewR = do
    (name, desc, types) <- runFormPostRedirect FactoryNewR newFactoryForm

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    (maybeSummary, audience, detail) <- C.createFactory personHash name desc
    (localRecips, remoteRecips, fwdHosts, action) <-
        C.makeServerInput Nothing maybeSummary audience $ AP.CreateActivity $ AP.Create (AP.CreateFactory detail Nothing types) Nothing
    result <-
        runExceptT $
        handleViaActor personID Nothing localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
            redirect FactoryNewR
        Right createID -> do
            maybeFactoryID <- runDB $ runMaybeT $ do
                ActorCreateLocal actorID _ <-
                    MaybeT $ getValBy $ UniqueActorCreateLocalCreate createID
                resourceID <- MaybeT $ getKeyBy $ UniqueResource actorID
                MaybeT $ getKeyBy $ UniqueFactory resourceID
            case maybeFactoryID of
                Nothing -> error "Can't find the newly created factory"
                Just factoryID -> do
                    factoryHash <- encodeKeyHashid factoryID
                    setMessage "New factory created"
                    redirect $ FactoryR factoryHash

postFactoryEditR :: KeyHashid Factory -> Handler Html
postFactoryEditR factoryHash = do
    factoryID <- decodeKeyHashid404 factoryHash
    types <-
        runFormPostRedirect (FactoryR factoryHash) $
            renderDivs $ areq typeField "Allowed types" (Just allTypes)
    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    (maybeSummary, audience, patch) <- C.editFactory personHash factoryHash types
    result <- runExceptT $ do
        cap <- do
            maybeItem <- lift $ runDB $ do
                resourceID <- factoryResource <$> get404 factoryID
                getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Factory to edit settings"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            lift $
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.PatchActivity patch
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
            redirect $ FactoryR factoryHash
        Right _itemID -> do
            setMessage "Patch activity sent"
            redirect $ FactoryR factoryHash

getFactoryStampR :: KeyHashid Factory -> KeyHashid SigKey -> Handler TypedContent
getFactoryStampR = servePerActorKey'' grabActorID LocalActorFactory

getFactoryCollabsR :: KeyHashid Factory -> Handler TypedContent
getFactoryCollabsR factoryHash = do
    factoryID <- decodeKeyHashid404 factoryHash
    (factory, actor) <- runDB $ do
        factory <- get404 factoryID
        Resource actorID <- getJust $ factoryResource factory
        actor <- getJust actorID
        return (factory, actor)
    serveCollabs
        AP.RelHasCollab
        (factoryResource factory)
        (FactoryR factoryHash)
        (FactoryCollabsR factoryHash)
        (FactoryRemoveR factoryHash)
        (FactoryInviteR factoryHash)
        (Just
            ( FactoryRemoveTeamR factoryHash
            , FactoryAddTeamR factoryHash
            , FactoryApproveTeamR factoryHash
            )
        )
        (factoryNavW (Entity factoryID factory) actor)

postFactoryInviteR :: KeyHashid Factory -> Handler Html
postFactoryInviteR factoryHash = do
    factoryID <- decodeKeyHashid404 factoryHash
    resourceID <- runDB $ factoryResource <$> get404 factoryID
    serveInviteCollab resourceID (FactoryCollabsR factoryHash)

postFactoryRemoveR :: KeyHashid Factory -> CollabId -> Handler Html
postFactoryRemoveR factoryHash collabID = do
    factoryID <- decodeKeyHashid404 factoryHash
    resourceID <- runDB $ factoryResource <$> get404 factoryID
    serveRemoveCollab resourceID (FactoryCollabsR factoryHash) collabID

getFactoryTeamsR :: KeyHashid Factory -> Handler TypedContent
getFactoryTeamsR factoryHash = do
    factoryID <- decodeKeyHashid404 factoryHash
    resourceID <- runDB $ factoryResource <$> get404 factoryID
    serveTeamsCollection (FactoryR factoryHash) (FactoryTeamsR factoryHash) resourceID

postFactoryAddTeamR :: KeyHashid Factory -> Handler ()
postFactoryAddTeamR factoryHash = do
    factoryID <- decodeKeyHashid404 factoryHash
    (uTeam, role) <-
        runFormPostRedirect (FactoryCollabsR factoryHash) addTeamForm

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    let uCollection = encodeRouteHome $ FactoryTeamsR factoryHash

    result <- runExceptT $ do
        (maybeSummary, audience, add) <- C.add personID uTeam uCollection role
        cap <- do
            maybeItem <- lift $ runDB $ do
                resourceID <- factoryResource <$> get404 factoryID
                getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Factory to add teams"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AddActivity add
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> setMessage $ toHtml e
        Right inviteID -> setMessage "Add sent"
    redirect $ FactoryCollabsR factoryHash

postFactoryApproveTeamR :: KeyHashid Factory -> SquadId -> Handler Html
postFactoryApproveTeamR factoryHash squadID = do
    factoryID <- decodeKeyHashid404 factoryHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            factory <- MaybeT $ get factoryID
            Squad _ resourceID <- MaybeT $ get squadID
            guard $ resourceID == factoryResource factory

            uAdd <- lift $ do
                add <- getSquadAdd squadID
                renderActivityURI add

            topic <- lift $ bimap snd snd <$> getSquadTeam squadID
            lift $
                (factoryResource factory,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, accept) <- do
            uTeam <-
                case pidOrU of
                    Left g -> encodeRouteHome . GroupR <$> encodeKeyHashid g
                    Right u -> pure u
            let uFactory = encodeRouteHome $ FactoryR factoryHash
            C.acceptParentChild personID uAdd uTeam uFactory
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Factory to approve teams"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AcceptActivity accept
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Accept sent"
    redirect $ FactoryCollabsR factoryHash

postFactoryRemoveTeamR :: KeyHashid Factory -> SquadId -> Handler Html
postFactoryRemoveTeamR factoryHash squadID = do
    factoryID <- decodeKeyHashid404 factoryHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            factory <- MaybeT $ get factoryID
            Squad _ resourceID <- MaybeT $ get squadID
            guard $ resourceID == factoryResource factory
            acceptID <- MaybeT $ getKeyBy $ UniqueSquadUsAccept squadID
            _ <- MaybeT $ getBy $ UniqueSquadUsStart acceptID

            uAdd <- lift $ do
                add <- getSquadAdd squadID
                renderActivityURI add

            topic <- lift $ bimap snd snd <$> getSquadTeam squadID
            lift $
                (factoryResource factory,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uTeam <-
                case pidOrU of
                    Left g -> encodeRouteHome . GroupR <$> encodeKeyHashid g
                    Right u -> pure u
            let uCollection = encodeRouteHome $ FactoryTeamsR factoryHash
            C.remove personID uTeam uCollection
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Factory to remove teams"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Remove sent"
    redirect $ FactoryCollabsR factoryHash

getFactoryTeamLiveR :: KeyHashid Factory -> KeyHashid SquadUsStart -> Handler ()
getFactoryTeamLiveR factoryHash startHash = do
    factoryID <- decodeKeyHashid404 factoryHash
    startID <- decodeKeyHashid404 startHash
    runDB $ do
        factory <- get404 factoryID
        SquadUsStart usAcceptID _ <- get404 startID
        SquadUsAccept squadID _ <- getJust usAcceptID
        Squad _ resourceID <- getJust squadID
        unless (resourceID == factoryResource factory) notFound
