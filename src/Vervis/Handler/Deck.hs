{- This file is part of Vervis.
 -
 - Written in 2016, 2019, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Handler.Deck
    ( getDeckR
    , getDeckInboxR
    , getDeckErrboxR
    , postDeckInboxR
    , getDeckOutboxR
    , getDeckOutboxItemR
    , getDeckFollowersR
    , getDeckTicketsR

    , getDeckTreeR

    , getDeckMessageR

    , getDeckNewR
    , postDeckNewR
    , postDeckDeleteR
    , getDeckEditR
    , postDeckEditR

    , getDeckStampR

    , getDeckCollabsR
    , postDeckInviteR
    , postDeckRemoveR
    , getDeckProjectsR
    , postDeckAddProjectR
    , postDeckApproveProjectR
    , postDeckRemoveProjectR

    , getDeckTeamsR

    , postDeckAddTeamR
    , postDeckApproveTeamR
    , postDeckRemoveTeamR
    , getDeckTeamLiveR

    , getDeckForksR




    {-
    , getProjectsR
    , getProjectR
    , putProjectR
    , postProjectDevsR
    , getProjectDevNewR
    , getProjectDevR
    , deleteProjectDevR
    , postProjectDevR
    , getProjectTeamR
    -}
    )
where

import Control.Applicative
import Control.Arrow ((&&&))
import Control.Monad
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Aeson
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Default.Class
import Data.Foldable
import Data.Maybe (fromMaybe, isJust)
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Data.Tree
import Database.Persist
import Network.HTTP.Types.Method
import Optics.Core
import Text.Blaze.Html (Html)
import Yesod.Auth
import Yesod.Core
import Yesod.Core.Handler (redirect, setMessage, lookupPostParam, notFound)
import Yesod.Form
import Yesod.Form.Functions (runFormPost, runFormGet)
import Yesod.Form.Types (FormResult (..))
import Yesod.Persist.Core (runDB, get404, getBy404)

import qualified Data.ByteString.Lazy as BL
import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Database.Persist.JSON
import Development.PatchMediaType
import Network.FedURI
import Web.ActivityPub hiding (Project (..), Repo (..), Actor (..), ActorDetail (..), ActorLocal (..), Ticket (..))
import Yesod.ActivityPub
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite

import qualified Data.F3 as F3
import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Data.Paginate.Local
import Database.Persist.Local
import Yesod.Form.Local
import Yesod.Persist.Local

import Vervis.Access
import Vervis.API
import Vervis.Data.Actor
import Vervis.Data.Discussion
import Vervis.Federation.Auth
import Vervis.Federation.Discussion
import Vervis.Federation.Offer
import Vervis.Federation.Ticket
import Vervis.FedURI
import Vervis.Form.Ticket
import Vervis.Form.Tracker
import Vervis.Foundation
import Vervis.Model
import Vervis.Paginate
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.Persist.Ticket
import Vervis.Recipient
import Vervis.Serve.Collab
import Vervis.Settings
import Vervis.Ticket
import Vervis.TicketFilter
import Vervis.Time
import Vervis.Web.Actor
import Vervis.Web.Collab
import Vervis.Widget
import Vervis.Widget.Person
import Vervis.Widget.Ticket
import Vervis.Widget.Tracker

import qualified Vervis.Client as C

getDeckR :: KeyHashid Deck -> Handler TypedContent
getDeckR deckHash = do
    deckID <- decodeKeyHashid404 deckHash
    (deck, repoIDs, actor, sigKeyIDs, muOrigin) <- runDB $ do
        d <- get404 deckID
        rs <- selectKeysList [RepoProject ==. Just deckID] [Asc RepoId]
        let aid = deckActor d
        a <- getJust aid
        sigKeys <- selectKeysList [SigKeyActor ==. aid] [Asc SigKeyId]
        origin <- do
            Komponent resourceID <- getJust $ deckKomponent d
            morigin <- getResourceOrigin resourceID
            for morigin $ \case
                Left (Entity _ (ResourceOriginLocal _ o)) -> do
                    Resource a <- getJust o
                    k <- getKeyByJust $ UniqueDeckActor a
                    h <- encodeKeyHashid k
                    encodeRouteHome <- getEncodeRouteHome
                    return $ encodeRouteHome $ DeckR h
                Right (Entity _ (ResourceOriginRemote _ o)) -> do
                    ra <- getJust o
                    getRemoteActorURI ra
        return (d, rs, a, sigKeys, origin)

    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    hashSigKey <- getEncodeKeyHashid
    perActor <- asksSite $ appPerActorKeys . appSettings
    let deckAP = AP.TicketTracker
            { AP.ticketTrackerActor = AP.Actor
                { AP.actorLocal = AP.ActorLocal
                    { AP.actorId         = encodeRouteLocal $ DeckR deckHash
                    , AP.actorInbox      = encodeRouteLocal $ DeckInboxR deckHash
                    , AP.actorOutbox     =
                        Just $ encodeRouteLocal $ DeckOutboxR deckHash
                    , AP.actorFollowers  =
                        Just $ encodeRouteLocal $ DeckFollowersR deckHash
                    , AP.actorFollowing  = Nothing
                    , AP.actorPublicKeys =
                        map (Left . encodeRouteLocal) $
                        if perActor
                            then map (DeckStampR deckHash . hashSigKey) sigKeyIDs
                            else [ActorKey1R, ActorKey2R]
                    , AP.actorSshKeys    = []
                    }
                , AP.actorDetail = AP.ActorDetail
                    { AP.actorType       = ActorTypeTicketTracker
                    , AP.actorUsername   = Nothing
                    , AP.actorName       = Just $ actorName actor
                    , AP.actorSummary    = Just $ actorDesc actor
                    , AP.actorOrigin     = muOrigin
                    }
                }
            , AP.ticketTrackerTeam = Nothing
            , AP.ticketTrackerCollaborators =
                encodeRouteLocal $ DeckCollabsR deckHash
            , AP.ticketTrackerProjects =
                encodeRouteLocal $ DeckProjectsR deckHash
            , AP.ticketTrackerTeams =
                encodeRouteLocal $ DeckTeamsR deckHash
            }

    tickets <- runDB $ do
        ts <- E.select $ E.from $ \ (t `E.InnerJoin` td) -> do
            E.on $ t E.^. TicketId E.==. td E.^. TicketDeckTicket
            E.where_ $ td E.^. TicketDeckDeck E.==. E.val deckID
            return (t, td)
        for ts $ \ (Entity tid t, Entity tdid _) -> do
            author <-
                requireEitherAlt
                    (getValBy $ UniqueTicketAuthorLocal tid)
                    (getValBy $ UniqueTicketAuthorRemote tid)
                    "Ticket doesn't have author"
                    "Ticket has both local and remote author"
            author' <-
                bitraverse
                    (\ (TicketAuthorLocal _ openID) -> do
                        o <- outboxItemOutbox <$> getJust openID
                        aid <- getKeyByJust $ UniqueActorOutbox o
                        getLocalActor aid
                    )
                    (\ (TicketAuthorRemote _ actorID _) -> do
                        a <- getJust actorID
                        getRemoteActorURI a
                    )
                    author
            resolved <- do
                mr <- getValBy $ UniqueTicketResolve tid
                for mr $ \ (TicketResolve _ acceptID) ->
                    outboxItemPublished <$> getJust acceptID
            discussion <- getDiscussionTree' $ ticketDiscuss t
            return (tdid, t, author', resolved, discussion)

    hashTD <- getEncodeKeyHashid
    hashLM <- getEncodeKeyHashid
    hashLA <- getHashLocalActor

    let makeComment (Node (MessageTreeNode _ message author _) forest) = F3.Comment
            { F3.commentIndex     =
                renderObjURI $
                case author of
                    MessageTreeNodeLocal lmid la _ _ ->
                        encodeRouteHome $ messageRoute (hashLA la) (hashLM lmid)
                    MessageTreeNodeRemote h luMsg _ _ ->
                        ObjURI h luMsg
            , F3.commentPoster    =
                renderObjURI $
                case author of
                    MessageTreeNodeLocal _ la _ _ ->
                        encodeRouteHome $ renderLocalActor $ hashLA la
                    MessageTreeNodeRemote h _ luAuthor _ ->
                        ObjURI h luAuthor
            , F3.commentCreated   = messageCreated message
            , F3.commentUpdated   = messageCreated message
            , F3.commentContent   = messageSource message
            , F3.commentReactions = []
            , F3.extCommentReplies = map makeComment forest
            }

        makeIssue (tdid, t, author, resolved, discussion) = F3.Issue
            { F3.issueIndex     = renderObjURI $ encodeRouteHome $ TicketR deckHash (hashTD tdid)
            , F3.issuePoster    =
                renderObjURI $
                    case author of
                        Left la ->
                            encodeRouteHome $ renderLocalActor $ hashLA la
                        Right u -> u
            , F3.issueTitle     = ticketTitle t
            , F3.issueContent   = ticketSource t
            , F3.issueMilestone = Nothing
            , F3.issueState     =
                if isJust resolved
                    then F3.IssueClosed
                    else F3.IssueOpen
            , F3.issueIsLocked  = False
            , F3.issueCreated   = ticketCreated t
            , F3.issueUpdated   = ticketCreated t
            , F3.issueClosed    = resolved
            , F3.issueDue       = Nothing
            , F3.issueLabels    = []
            , F3.issueReactions = []
            , F3.issueAssignees = []
            , F3.extIssueComments = map makeComment discussion
            }

        deckF3 = F3.IssueTracker
            { F3.issueTrackerIndex       = renderObjURI $ encodeRouteHome $ DeckR deckHash
            , F3.issueTrackerName        = actorName actor
            , F3.issueTrackerDescription = actorDesc actor
            , F3.issueTrackerCreated     = actorCreatedAt actor
            , F3.issueTrackerUpdated     = actorCreatedAt actor
            , F3.issueTrackerIssues      = map makeIssue tickets
            }

    provideHtmlAndAP_F3 deckAP (redirect $ DeckTicketsR deckHash) deckF3

getDeckInboxR :: KeyHashid Deck -> Handler TypedContent
getDeckInboxR = getInbox DeckInboxR deckActor

getDeckErrboxR :: KeyHashid Deck -> Handler TypedContent
getDeckErrboxR = getInbox' actorErrbox DeckErrboxR deckActor

postDeckInboxR :: KeyHashid Deck -> Handler ()
postDeckInboxR deckHash = do
    deckID <- decodeKeyHashid404 deckHash
    postInbox LocalActorDeck deckID

{-
            AP.AcceptActivity accept ->
                deckAcceptF now recipDeckHash author body mfwd luActivity accept
            AP.CreateActivity (AP.Create obj mtarget) ->
                case obj of
                    AP.CreateNote _ note ->
                        (,Nothing) <$> deckCreateNoteF now recipDeckHash author body mfwd luActivity note
                    _ -> return ("Unsupported create object type for decks", Nothing)
            AP.FollowActivity follow ->
                deckFollowF now recipDeckHash author body mfwd luActivity follow
            AP.InviteActivity invite ->
                topicInviteF now (GrantResourceDeck recipDeckHash) author body mfwd luActivity invite
            AP.JoinActivity join ->
                deckJoinF now recipDeckHash author body mfwd luActivity join
            OfferActivity (Offer obj target) ->
                case obj of
                    OfferTicket ticket ->
                        (,Nothing) <$> deckOfferTicketF now recipDeckHash author body mfwd luActivity ticket target
                    {-
                    OfferDep dep ->
                        projectOfferDepF now shrRecip prjRecip remoteAuthor body mfwd luActivity dep target
                    -}
                    _ -> return ("Unsupported offer object type for decks", Nothing)
            AP.ResolveActivity resolve ->
                deckResolveF now recipDeckHash author body mfwd luActivity resolve
            AP.UndoActivity undo ->
                (,Nothing) <$> deckUndoF now recipDeckHash author body mfwd luActivity undo
            _ -> return ("Unsupported activity type for decks", Nothing)
-}

getDeckOutboxR :: KeyHashid Deck -> Handler TypedContent
getDeckOutboxR = getOutbox DeckOutboxR DeckOutboxItemR deckActor

getDeckOutboxItemR
    :: KeyHashid Deck -> KeyHashid OutboxItem -> Handler TypedContent
getDeckOutboxItemR = getOutboxItem DeckOutboxItemR deckActor

getDeckFollowersR :: KeyHashid Deck -> Handler TypedContent
getDeckFollowersR = getActorFollowersCollection DeckFollowersR deckActor

getDeckTicketsR :: KeyHashid Deck -> Handler TypedContent
getDeckTicketsR deckHash = selectRep $ do
    provideRep $ do
        let tf = def
        {-
        ((filtResult, filtWidget), filtEnctype) <- runFormPost ticketFilterForm
        let tf =
                case filtResult of
                    FormSuccess filt -> filt
                    FormMissing      -> def
                    FormFailure l    ->
                        error $ "Ticket filter form failed: " ++ show l
        -}
        deckID <- decodeKeyHashid404 deckHash
        (deck, actor, morigin, (total, pages, mpage)) <- runDB $ do
            deck <- get404 deckID
            Komponent resourceID <- getJust $ deckKomponent deck
            Resource actorID <- getJust resourceID
            actor <- getJust actorID
            morigin <- do
                mo <- getResourceOrigin resourceID
                for mo $ bitraverse
                    (\ (Entity _ (ResourceOriginLocal _ o)) -> do
                        Resource aid <- getJust o
                        k <- getKeyByJust $ UniqueDeckActor aid
                        a <- getJust aid
                        return (LocalActorDeck k, a)
                    )
                    (\ (Entity _ (ResourceOriginRemote _ o)) ->
                        getRemoteActorData o
                    )
            let countAllTickets = count [TicketDeckDeck ==. deckID]
                selectTickets off lim =
                    getTicketSummaries
                        (filterTickets tf)
                        (Just $ \ t -> [E.desc $ t E.^. TicketId])
                        (Just (off, lim))
                        deckID
            (deck,actor,morigin,) <$> getPageAndNavCount countAllTickets selectTickets
        permits <- do
            mp <- maybeAuthId
            case mp of
                Nothing -> pure []
                Just personID -> runDB $ getPermitsForResource personID (Left $ deckResource deck)
        case mpage of
            Nothing -> redirectFirstPage here
            Just (rows, navModel) ->
                let pageNav = navWidget navModel
                in  defaultLayout $(widgetFile "ticket/list")
    provideAP' $ do
        deckID <- decodeKeyHashid404 deckHash
        (total, pages, mpage) <- runDB $ do
            _ <- get404 deckID
            let countAllTickets = count [TicketDeckDeck ==. deckID]
                selectTickets off lim =
                    selectKeysList
                        [TicketDeckDeck ==. deckID]
                        [OffsetBy off, LimitTo lim, Desc TicketDeckTicket]
            getPageAndNavCount countAllTickets selectTickets

        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal
        hashTicket <- getEncodeKeyHashid
        encodeRoutePageLocal <- getEncodeRoutePageLocal
        let pageUrl = encodeRoutePageLocal here
        host <- asksSite siteInstanceHost
        return $
            case mpage of
                Nothing -> encodeStrict $ Doc host $ Collection
                    { collectionId         = encodeRouteLocal here
                    , collectionType       = CollectionTypeOrdered
                    , collectionTotalItems = Just total
                    , collectionCurrent    = Nothing
                    , collectionFirst      = Just $ pageUrl 1
                    , collectionLast       = Just $ pageUrl pages
                    , collectionItems      = [] :: [Text]
                    , collectionContext    = Nothing
                    }
                Just (tickets, navModel) ->
                    let current = nmCurrent navModel
                    in  encodeStrict $ Doc host $ CollectionPage
                            { collectionPageId         = pageUrl current
                            , collectionPageType       = CollectionPageTypeOrdered
                            , collectionPageTotalItems = Nothing
                            , collectionPageCurrent    = Just $ pageUrl current
                            , collectionPageFirst      = Just $ pageUrl 1
                            , collectionPageLast       = Just $ pageUrl pages
                            , collectionPagePartOf     = encodeRouteLocal here
                            , collectionPagePrev       =
                                if current > 1
                                    then Just $ pageUrl $ current - 1
                                    else Nothing
                            , collectionPageNext       =
                                if current < pages
                                    then Just $ pageUrl $ current + 1
                                    else Nothing
                            , collectionPageStartIndex = Nothing
                            , collectionPageItems      =
                                encodeRouteHome . TicketR deckHash . hashTicket <$> tickets
                            }
    where
    here = DeckTicketsR deckHash
    encodeStrict = BL.toStrict . encode

getDeckTreeR :: KeyHashid Deck -> Handler Html
getDeckTreeR _ = error "Temporarily disabled"
    {-
    (summaries, deps) <- runDB $ do
        Entity sid _ <- getBy404 $ UniqueSharer shr
        Entity jid _ <- getBy404 $ UniqueProject prj sid
        (,) <$> getTicketSummaries Nothing Nothing Nothing jid
            <*> getTicketDepEdges jid
    defaultLayout $ ticketTreeDW shr prj summaries deps
    -}

getDeckMessageR :: KeyHashid Deck -> KeyHashid LocalMessage -> Handler Html
getDeckMessageR _ _ = notFound

getDeckNewR :: Handler Html
getDeckNewR = do
    p <- requireAuthId
    ((_result, widget), enctype) <- runFormPost $ newDeckForm p
    defaultLayout $(widgetFile "deck/new")

postDeckNewR :: Handler Html
postDeckNewR = do
    personEntity@(Entity personID person) <- requireAuth
    NewDeck name desc (uFactory, uCap) muOrigin <- runFormPostRedirect DeckNewR $ newDeckForm personID

    personHash <- encodeKeyHashid personID
    result <- runExceptT $ do
        allowOrigin <- asksSite $ appDeckOrigin . appSettings
        when (not allowOrigin && isJust muOrigin) $
            throwE "Migration disabled on this server"
        (maybeSummary, audience, detail) <- C.createDeck personHash name desc uFactory muOrigin
        (localRecips, remoteRecips, fwdHosts, action) <-
            lift $
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.CreateActivity $ AP.Create (AP.CreateTicketTracker detail Nothing) (Just uFactory)
        cap <- parseActivityURI uCap
        handleViaActor personID (Just cap) localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
            redirect DeckNewR
        Right _createID -> do
            setMessage "Create activity sent"
            redirect HomeR

postDeckDeleteR :: KeyHashid Deck -> Handler Html
postDeckDeleteR _ = error "Temporarily disabled"

getDeckEditR :: KeyHashid Deck -> Handler Html
getDeckEditR _ = do
    error "Temporarily disabled"
    {-
    (sid, ep) <- runDB $ do
        Entity sid _sharer <- getBy404 $ UniqueSharer shr
        ep <- getBy404 $ UniqueProject prj sid
        return (sid, ep)
    ((_result, widget), enctype) <- runFormPost $ editProjectForm sid ep
    defaultLayout $(widgetFile "project/edit")
    -}

postDeckEditR :: KeyHashid Deck -> Handler Html
postDeckEditR _ = do
    error "Temporarily disabled"
    {-
    (sid, ep@(Entity jid _)) <- runDB $ do
        Entity sid _sharer <- getBy404 $ UniqueSharer shr
        eproj <- getBy404 $ UniqueProject prj sid
        return (sid, eproj)
    ((result, widget), enctype) <- runFormPost $ editProjectForm sid ep
    case result of
        FormSuccess project' -> do
            runDB $ replace jid project'
            setMessage "Project updated."
            redirect $ ProjectR shr prj
        FormMissing -> do
            setMessage "Field(s) missing."
            defaultLayout $(widgetFile "project/edit")
        FormFailure _l -> do
            setMessage "Project update failed, see errors below."
            defaultLayout $(widgetFile "project/edit")
    -}

getDeckStampR :: KeyHashid Deck -> KeyHashid SigKey -> Handler TypedContent
getDeckStampR = servePerActorKey deckActor LocalActorDeck

getDeckCollabsR :: KeyHashid Deck -> Handler TypedContent
getDeckCollabsR deckHash = do
    deckID <- decodeKeyHashid404 deckHash
    (deck, actor) <- runDB $ do
        deck <- get404 deckID
        actor <- getJust $ deckActor deck
        return (deck, actor)
    serveCollabs
        AP.RelHasCollab
        (deckResource deck)
        (DeckR deckHash)
        (DeckCollabsR deckHash)
        (DeckRemoveR deckHash)
        (DeckInviteR deckHash)
        (Just
            ( DeckRemoveTeamR deckHash
            , DeckAddTeamR deckHash
            , DeckApproveTeamR deckHash
            )
        )
        (deckNavW (Entity deckID deck) actor)

postDeckInviteR :: KeyHashid Deck -> Handler Html
postDeckInviteR deckHash = do
    deckID <- decodeKeyHashid404 deckHash
    resourceID <- runDB $ deckResource <$> get404 deckID
    serveInviteCollab resourceID (DeckCollabsR deckHash)

postDeckRemoveR :: KeyHashid Deck -> CollabId -> Handler Html
postDeckRemoveR deckHash collabID = do
    deckID <- decodeKeyHashid404 deckHash
    resourceID <- runDB $ deckResource <$> get404 deckID
    serveRemoveCollab resourceID (DeckCollabsR deckHash) collabID

getDeckProjectsR :: KeyHashid Deck -> Handler TypedContent
getDeckProjectsR deckHash = do
    deckID <- decodeKeyHashid404 deckHash
    stems <- runDB $ do
        deck <- get404 deckID
        getStems $ deckKomponent deck
    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    hashProject <- getEncodeKeyHashid
    let projectsAP = Collection
            { collectionId         = encodeRouteLocal $ DeckProjectsR deckHash
            , collectionType       = CollectionTypeUnordered
            , collectionTotalItems = Just $ length stems
            , collectionCurrent    = Nothing
            , collectionFirst      = Nothing
            , collectionLast       = Nothing
            , collectionItems      =
                map ( either
                        ( encodeRouteHome
                        . ProjectR
                        . hashProject
                        . fst
                        )
                        (\ (Instance h, RemoteObject _ lu , _) -> ObjURI h lu)
                    . view _1
                    )
                    stems
            , collectionContext    =
                Just $ encodeRouteLocal $ DeckR deckHash
            }
    provideHtmlAndAP projectsAP $ getHtml deckID stems

    where

    getHtml deckID stems = do
        mp <- maybeAuthId
        haveAdmin <- handlerToWidget $ fmap isJust $ runDB $ runMaybeT $ do
            personID <- MaybeT $ pure mp
            deck <- lift $ get404 deckID
            MaybeT $ getCapability personID (Left $ deckResource deck) AP.RoleAdmin
        ((_, widgetAP), enctypeAP) <- handlerToWidget $ runFormPost addProjectForm
        (deck, actor, drafts) <- handlerToWidget $ runDB $ do
            deck <- get404 deckID
            actor <- getJust $ deckActor deck
            drafts <- getStemDrafts $ deckKomponent deck
            return (deck, actor, drafts)
        $(widgetFile "deck/projects")

addProjectForm = renderDivs $
    areq fedUriField "(URI) Project" Nothing

postDeckAddProjectR :: KeyHashid Deck -> Handler ()
postDeckAddProjectR deckHash = do
    deckID <- decodeKeyHashid404 deckHash
    uProject <-
        runFormPostRedirect (DeckProjectsR deckHash) addProjectForm

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    let uCollection = encodeRouteHome $ DeckProjectsR deckHash

    result <- runExceptT $ do
        (maybeSummary, audience, add) <- C.add personID uProject uCollection AP.RoleAdmin
        cap <- do
            maybeItem <- lift $ runDB $ do
                resourceID <- deckResource <$> get404 deckID
                getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Deck to add projects"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AddActivity add
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> setMessage $ toHtml e
        Right inviteID -> setMessage "Add sent"
    redirect $ DeckProjectsR deckHash

postDeckApproveProjectR :: KeyHashid Deck -> StemId -> Handler Html
postDeckApproveProjectR deckHash stemID = do
    deckID <- decodeKeyHashid404 deckHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            deck <- MaybeT $ get deckID
            Stem _ kompID <- MaybeT $ get stemID
            guard $ kompID == deckKomponent deck

            uAdd <- lift $ do
                add <- getStemAdd stemID
                renderActivityURI add

            topic <- lift $ getStemProject stemID
            lift $
                (deckResource deck,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, accept) <- do
            uProject <-
                case pidOrU of
                    Left j -> encodeRouteHome . ProjectR <$> encodeKeyHashid j
                    Right u -> pure u
            let uDeck = encodeRouteHome $ DeckR deckHash
            C.acceptParentChild personID uAdd uProject uDeck
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Deck to approve projects"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AcceptActivity accept
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Accept sent"
    redirect $ DeckProjectsR deckHash

postDeckRemoveProjectR :: KeyHashid Deck -> StemId -> Handler Html
postDeckRemoveProjectR deckHash stemID = do
    deckID <- decodeKeyHashid404 deckHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            deck <- MaybeT $ get deckID
            Stem _ kompID <- MaybeT $ get stemID
            guard $ kompID == deckKomponent deck
            acceptID <- MaybeT $ getKeyBy $ UniqueStemComponentAccept stemID
            _ <- MaybeT $ getBy $ UniqueStemDelegateLocal acceptID

            uAdd <- lift $ do
                add <- getStemAdd stemID
                renderActivityURI add

            topic <- lift $ getStemProject stemID
            lift $
                (deckResource deck,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uProject <-
                case pidOrU of
                    Left j -> encodeRouteHome . ProjectR <$> encodeKeyHashid j
                    Right u -> pure u
            let uCollection = encodeRouteHome $ DeckProjectsR deckHash
            C.remove personID uProject uCollection
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Deck to remove projects"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Remove sent"
    redirect $ DeckProjectsR deckHash

getDeckTeamsR :: KeyHashid Deck -> Handler TypedContent
getDeckTeamsR deckHash = do
    deckID <- decodeKeyHashid404 deckHash
    resourceID <- runDB $ do
        komponentID <- deckKomponent <$> get404 deckID
        komponentResource <$> getJust komponentID
    serveTeamsCollection (DeckR deckHash) (DeckTeamsR deckHash) resourceID

postDeckAddTeamR :: KeyHashid Deck -> Handler ()
postDeckAddTeamR deckHash = do
    deckID <- decodeKeyHashid404 deckHash
    (uTeam, role) <-
        runFormPostRedirect (DeckCollabsR deckHash) addTeamForm

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    let uCollection = encodeRouteHome $ DeckTeamsR deckHash

    result <- runExceptT $ do
        (maybeSummary, audience, add) <- C.add personID uTeam uCollection role
        cap <- do
            maybeItem <- lift $ runDB $ do
                resourceID <- deckResource <$> get404 deckID
                getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Deck to add teams"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AddActivity add
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> setMessage $ toHtml e
        Right inviteID -> setMessage "Add sent"
    redirect $ DeckCollabsR deckHash

postDeckApproveTeamR :: KeyHashid Deck -> SquadId -> Handler Html
postDeckApproveTeamR deckHash squadID = do
    deckID <- decodeKeyHashid404 deckHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            deck <- MaybeT $ get deckID
            Squad _ resourceID <- MaybeT $ get squadID
            guard $ resourceID == deckResource deck

            uAdd <- lift $ do
                add <- getSquadAdd squadID
                renderActivityURI add

            topic <- lift $ bimap snd snd <$> getSquadTeam squadID
            lift $
                (deckResource deck,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, accept) <- do
            uTeam <-
                case pidOrU of
                    Left g -> encodeRouteHome . GroupR <$> encodeKeyHashid g
                    Right u -> pure u
            let uDeck = encodeRouteHome $ DeckR deckHash
            C.acceptParentChild personID uAdd uTeam uDeck
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Deck to approve teams"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.AcceptActivity accept
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Accept sent"
    redirect $ DeckCollabsR deckHash

postDeckRemoveTeamR :: KeyHashid Deck -> SquadId -> Handler Html
postDeckRemoveTeamR deckHash squadID = do
    deckID <- decodeKeyHashid404 deckHash

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            deck <- MaybeT $ get deckID
            Squad _ resourceID <- MaybeT $ get squadID
            guard $ resourceID == deckResource deck
            acceptID <- MaybeT $ getKeyBy $ UniqueSquadUsAccept squadID
            _ <- MaybeT $ getBy $ UniqueSquadUsStart acceptID

            uAdd <- lift $ do
                add <- getSquadAdd squadID
                renderActivityURI add

            topic <- lift $ bimap snd snd <$> getSquadTeam squadID
            lift $
                (deckResource deck,uAdd,) <$>
                bitraverse
                    pure
                    (getRemoteActorURI <=< getJust)
                    topic
        (resourceID, uAdd, pidOrU) <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uTeam <-
                case pidOrU of
                    Left g -> encodeRouteHome . GroupR <$> encodeKeyHashid g
                    Right u -> pure u
            let uCollection = encodeRouteHome $ DeckTeamsR deckHash
            C.remove personID uTeam uCollection
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to be have Admin access to the Deck to remove teams"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> do
            setMessage $ toHtml e
        Right removeID ->
            setMessage "Remove sent"
    redirect $ DeckCollabsR deckHash

getDeckTeamLiveR :: KeyHashid Deck -> KeyHashid SquadUsStart -> Handler ()
getDeckTeamLiveR deckHash startHash = do
    deckID <- decodeKeyHashid404 deckHash
    startID <- decodeKeyHashid404 startHash
    runDB $ do
        deck <- get404 deckID
        SquadUsStart usAcceptID _ <- get404 startID
        SquadUsAccept squadID _ <- getJust usAcceptID
        Squad _ resourceID <- getJust squadID
        unless (resourceID == deckResource deck) notFound

getDeckForksR :: KeyHashid Deck -> Handler TypedContent
getDeckForksR deckHash = do
    deckID <- decodeKeyHashid404 deckHash
    resourceID <- runDB $ do
        komponentID <- deckKomponent <$> get404 deckID
        komponentResource <$> getJust komponentID
    serveForks (DeckR deckHash) (DeckForksR deckHash) resourceID

{-
getProjectsR :: ShrIdent -> Handler Html
getProjectsR ident = do
    projects <- runDB $ select $ from $ \ (sharer, project) -> do
        where_ $
            sharer ^. SharerIdent E.==. val ident                &&.
            sharer ^. SharerId    E.==. project ^. ProjectSharer
        orderBy [asc $ project ^. ProjectIdent]
        return $ project ^. ProjectIdent
    defaultLayout $(widgetFile "project/list")

postProjectDevsR :: ShrIdent -> PrjIdent -> Handler Html
postProjectDevsR shr rp = do
    (sid, jid, obid) <- runDB $ do
        Entity sid _ <- getBy404 $ UniqueSharer shr
        Entity jid j <- getBy404 $ UniqueProject rp sid
        a <- getJust $ projectActor j
        return (sid, jid, actorOutbox a)
    ((result, widget), enctype) <- runFormPost $ newProjectCollabForm sid jid
    case result of
        FormSuccess nc -> do
            now <- liftIO getCurrentTime
            host <- asksSite siteInstanceHost
            runDB $ do
                obiid <-
                    insert $
                        OutboxItem
                            obid
                            (persistJSONObjectFromDoc $ Doc host emptyActivity)
                            now
                cid <- insert Collab
                for_ (ncRole nc) $ \ rlid -> insert_ $ CollabRoleLocal cid rlid
                insert_ $ CollabTopicLocalProject cid jid
                insert_ $ CollabSenderLocal cid obiid
                insert_ $ CollabRecipLocal cid (ncPerson nc)
            setMessage "Collaborator added."
            redirect $ ProjectDevsR shr rp
        FormMissing -> do
            setMessage "Field(s) missing"
            defaultLayout $(widgetFile "project/collab/new")
        FormFailure _l -> do
            setMessage "Operation failed, see errors below"
            defaultLayout $(widgetFile "project/collab/new")

getProjectDevNewR :: ShrIdent -> PrjIdent -> Handler Html
getProjectDevNewR shr rp = do
    (sid, jid) <- runDB $ do
        Entity s _ <- getBy404 $ UniqueSharer shr
        Entity j _ <- getBy404 $ UniqueProject rp s
        return (s, j)
    ((_result, widget), enctype) <- runFormPost $ newProjectCollabForm sid jid
    defaultLayout $(widgetFile "project/collab/new")

getProjectDevR :: ShrIdent -> PrjIdent -> ShrIdent -> Handler Html
getProjectDevR shr prj dev = do
    mrl <- runDB $ do
        jid <- do
            Entity s _ <- getBy404 $ UniqueSharer shr
            Entity j _ <- getBy404 $ UniqueProject prj s
            return j
        pid <- do
            Entity s _ <- getBy404 $ UniqueSharer dev
            Entity p _ <- getBy404 $ UniquePersonIdent s
            return p
        l <- E.select $ E.from $ \ (topic `E.InnerJoin` recip) -> do
            E.on $ topic E.^. CollabTopicLocalProjectCollab E.==. recip E.^. CollabRecipLocalCollab
            E.where_ $
                topic E.^. CollabTopicLocalProjectProject E.==. E.val jid E.&&.
                recip E.^. CollabRecipLocalPerson E.==. E.val pid
            return $ recip E.^. CollabRecipLocalCollab
        cid <-
            case l of
                [] -> notFound
                [E.Value cid] -> return cid
                _ -> error "Multiple collabs for project+person"
        mcrole <- getValBy $ UniqueCollabRoleLocal cid
        for mcrole $
            \ (CollabRoleLocal _cid rlid) -> roleIdent <$> getJust rlid
    defaultLayout $(widgetFile "project/collab/one")

deleteProjectDevR :: ShrIdent -> PrjIdent -> ShrIdent -> Handler Html
deleteProjectDevR shr rp dev = do
    runDB $ do
        jid <- do
            Entity s _ <- getBy404 $ UniqueSharer shr
            Entity j _ <- getBy404 $ UniqueProject rp s
            return j
        pid <- do
            Entity s _ <- getBy404 $ UniqueSharer dev
            Entity p _ <- getBy404 $ UniquePersonIdent s
            return p
        collabs <- E.select $ E.from $ \ (recip `E.InnerJoin` topic) -> do
            E.on $ recip E.^. CollabRecipLocalCollab E.==. topic E.^. CollabTopicLocalProjectCollab
            E.where_ $
                recip E.^. CollabRecipLocalPerson E.==. E.val pid E.&&.
                topic E.^. CollabTopicLocalProjectProject E.==. E.val jid
            return
                ( recip E.^. CollabRecipLocalId
                , topic E.^. CollabTopicLocalProjectId
                , recip E.^. CollabRecipLocalCollab
                )
        (E.Value crid, E.Value ctid, E.Value cid) <-
            case collabs of
                [] -> notFound
                [c] -> return c
                _ -> error "More than 1 collab for project+person"
        deleteWhere [CollabRoleLocalCollab ==. cid]
        delete ctid
        deleteWhere [CollabSenderLocalCollab ==. cid]
        deleteWhere [CollabSenderRemoteCollab ==. cid]
        delete crid
        delete cid
    setMessage "Collaborator removed."
    redirect $ ProjectDevsR shr rp

postProjectDevR :: ShrIdent -> PrjIdent -> ShrIdent -> Handler Html
postProjectDevR shr rp dev = do
    mmethod <- lookupPostParam "_method"
    case mmethod of
        Just "DELETE" -> deleteProjectDevR shr rp dev
        _             -> notFound

getProjectTeamR :: ShrIdent -> PrjIdent -> Handler TypedContent
getProjectTeamR shr prj = do
    memberShrs <- runDB $ do
        sid <- getKeyBy404 $ UniqueSharer shr
        _jid <- getKeyBy404 $ UniqueProject prj sid
        id_ <-
            requireEitherAlt
                (getKeyBy $ UniquePersonIdent sid)
                (getKeyBy $ UniqueGroup sid)
                "Found sharer that is neither person nor group"
                "Found sharer that is both person and group"
        case id_ of
            Left pid -> return [shr]
            Right gid -> do
                pids <-
                    map (groupMemberPerson . entityVal) <$>
                        selectList [GroupMemberGroup ==. gid] []
                sids <-
                    map (personIdent . entityVal) <$>
                        selectList [PersonId <-. pids] []
                map (sharerIdent . entityVal) <$>
                    selectList [SharerId <-. sids] []

    let here = ProjectTeamR shr prj

    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    let team = Collection
            { collectionId         = encodeRouteLocal here
            , collectionType       = CollectionTypeUnordered
            , collectionTotalItems = Just $ length memberShrs
            , collectionCurrent    = Nothing
            , collectionFirst      = Nothing
            , collectionLast       = Nothing
            , collectionItems      = map (encodeRouteHome . SharerR) memberShrs
            , collectionContext    = Nothing
            }
    provideHtmlAndAP team $ redirect (here, [("prettyjson", "true")])
-}
