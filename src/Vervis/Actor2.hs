{- This file is part of Vervis.
 -
 - Written in 2019, 2020, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

-- For the ugly existential-type trick that avoids Env depending on App
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

-- | Everything I'd put in 'Vervis.Actor' but currently depends on
-- 'Vervis.Foundation', and therefore needs a separate module.
module Vervis.Actor2
    ( -- * Sending messages to actors
      sendActivity
    , forwardActivity
      -- * Preparing a new activity
    , makeAudSenderOnly
    , makeAudSenderWithFollowers
    , getActivityURI
    , getActorURI
      -- * Running actor pieces in Handler
    , runAct
    , runActE
      -- * Fetching remote objects
    , fetchRemoteResource
    )
where

import Control.Applicative
import Control.Concurrent.STM.TVar
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Barbie
import Data.Bifunctor
import Data.ByteString (ByteString)
import Data.Either
import Data.Foldable
import Data.Hashable
import Data.List.NonEmpty (NonEmpty)
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Data.Typeable
import Database.Persist.Sql
import GHC.Generics
import UnliftIO.Exception hiding (Handler)
import Web.Hashids

import qualified Data.Aeson as A
import qualified Data.ByteString.Lazy as BL
import qualified Data.List.NonEmpty as NE
import qualified Data.Text as T

import Control.Concurrent.Actor hiding (Handler)
import Network.FedURI
import Web.Actor
import Web.Actor.Deliver
import Web.Actor.Persist

import qualified Crypto.ActorKey as AK
import qualified Web.ActivityPub as AP
import qualified Yesod.MonadSite as YM

import Control.Monad.Trans.Except.Local
import Database.Persist.Local

import Vervis.Actor
import Vervis.Data.Actor
import Vervis.FedURI
import Vervis.Fetch
import Vervis.Foundation
import Vervis.Model hiding (Actor, Message)
import Vervis.Recipient (renderLocalActor, localRecipSieve', localActorFollowers, Aud (..), ParsedAudience (..), parseAudience')
import Vervis.RemoteActorStore
import Vervis.Settings

askLatestInstanceKey :: Act (Maybe (Route App, AK.ActorKey))
askLatestInstanceKey = do
    maybeTVar <- asksEnv envActorKeys
    for maybeTVar $ \ tvar -> do
        (akey1, akey2, new1) <- liftIO $ readTVarIO tvar
        return $
            if new1
                then (ActorKey1R, akey1)
                else (ActorKey2R, akey2)

prepareSendIK
    :: (Route App, AK.ActorKey)
    -> LocalActorBy KeyHashid
    -> OutboxItemId
    -> AP.Action URIMode
    -> Act (AP.Envelope URIMode)
prepareSendIK (keyR, akey) actorByHash itemID action = do
    itemHash <- encodeKeyHashid itemID
    let sign = AK.actorKeySign akey
        actorR = renderLocalActor actorByHash
        idR = activityRoute actorByHash itemHash
    prepareToSend keyR sign True actorR idR action

prepareSendAK
    :: ActorId
    -> LocalActorBy KeyHashid
    -> OutboxItemId
    -> AP.Action URIMode
    -> ActDB (AP.Envelope URIMode)
prepareSendAK actorID actorByHash itemID action = do
    Entity keyID key <- do
        mk <- getBy $ UniqueSigKey actorID
        case mk of
            Nothing -> error "Actor has no keys!"
            Just k -> return k
    itemHash <- encodeKeyHashid itemID
    keyHash <- encodeKeyHashid keyID
    let keyR = stampRoute actorByHash keyHash
        sign = AK.actorKeySign $ sigKeyMaterial key
        actorR = renderLocalActor actorByHash
        idR = activityRoute actorByHash itemHash
    prepareToSend keyR sign False actorR idR action

prepareSendP
    :: ActorId
    -> LocalActorBy KeyHashid
    -> OutboxItemId
    -> AP.Action URIMode
    -> ActDB (AP.Envelope URIMode)
prepareSendP actorID actorByHash itemID action = do
    maybeKey <- lift askLatestInstanceKey
    case maybeKey of
        Nothing -> prepareSendAK actorID actorByHash itemID action
        Just key -> lift $ prepareSendIK key actorByHash itemID action

prepareSendH
    :: ActorId
    -> LocalActorBy KeyHashid
    -> OutboxItemId
    -> AP.Action URIMode
    -> Act (AP.Envelope URIMode)
prepareSendH actorID actorByHash itemID action = do
    maybeKey <- askLatestInstanceKey
    case maybeKey of
        Nothing -> withDB $ prepareSendAK actorID actorByHash itemID action
        Just key -> prepareSendIK key actorByHash itemID action

-- | Given a list of local and remote recipients, which may include actors and
-- collections,
--
-- * Insert event to message queues of local actors listed
-- * Insert event to message queues of local members of local collections
--   listed
-- * Launch asynchronously sending activity to remote recipients and remote
--   member of local collections listed
--
-- This function reads the follower sets, remote recipient data and the
-- sender's signing key from the PostgreSQL database. Don't use it inside a
-- database transaction.
sendActivity
    :: LocalActorBy Key
    -- ^ Activity author and sender
    --
    -- * Its collections are excluded from requiring an owner, i.e.
    --   even if owner is required, this actor's collections will be delivered
    --   to, even if this actor isn't addressed
    -- * Its inbox is excluded from delivery, even if this actor is listed in
    --   the recipient set
    -> ActorId
    -- ^ Actor key for the sender, for fetching its signing key from the DB
    -> RecipientRoutes
    -- ^ Local recipients
    -> [(Host, NonEmpty LocalURI)]
    -- ^ Remote recipients
    -> [Host]
    -- ^ Instances for which the sender is approving to forward this activity
    -> OutboxItemId
    -- ^ DB ID of the item in the author's outbox
    -> AP.Action URIMode
    -- ^ Activity to send to remote actors
    -> Act ()
sendActivity senderByKey senderActorID localRecips remoteRecips fwdHosts itemID action = do
    envelope <- do
        senderByHash <- hashLocalActor senderByKey
        prepareSendH senderActorID senderByHash itemID action
    sendByHttp envelope remoteRecips
    moreRemoteRecips <- do
        let justSender = Just senderByKey
            author = (senderByKey, senderActorID, itemID)
        encodeRouteLocal <- getEncodeRouteLocal
        itemHash <- encodeKeyHashid itemID
        senderByHash <- hashLocalActor senderByKey
        hLocal <- asksEnv stageInstanceHost
        let act =
                let luId = encodeRouteLocal $ activityRoute senderByHash itemHash
                    luActor = encodeRouteLocal $ renderLocalActor senderByHash
                in  AP.makeActivity luId luActor action
            bodyBL = A.encode $ AP.Doc hLocal act
        bodyO <-
            case A.eitherDecode' bodyBL of
                Left s -> error $ "Parsing encoded activity failed: " ++ s
                Right o -> return o
        let body = ActivityBody bodyBL bodyO act
        sendToLocalActors (Left author) body True justSender justSender localRecips
    sendByHttp envelope $
        map (\ ((_, h), rrs) -> (h, NE.map remoteRecipientId rrs))
        moreRemoteRecips
    where
    decideFwd h =
        if h `elem` fwdHosts
            then Left . ObjURI h
            else Right . ObjURI h
    sendByHttp envelope recips = do
        let recipsDecided =
                concatMap
                    (\ (h, lus) -> NE.toList $ NE.map (decideFwd h) lus)
                    recips
            (yesFwd, noFwd) = partitionEithers recipsDecided
        dt <- asksEnv stageDeliveryTheater
        liftIO $ do
            sendHttp dt (MethodDeliverLocal envelope True) yesFwd
            sendHttp dt (MethodDeliverLocal envelope False) noFwd

prepareForwardIK
    :: (Route App, AK.ActorKey)
    -> LocalActorBy KeyHashid
    -> BL.ByteString
    -> Maybe ByteString
    -> Act (AP.Errand URIMode)
prepareForwardIK (keyR, akey) fwderByHash body mproof = do
    let sign = AK.actorKeySign akey
        fwderR = renderLocalActor fwderByHash
    prepareToForward keyR sign True fwderR body mproof

prepareForwardAK
    :: ActorId
    -> LocalActorBy KeyHashid
    -> BL.ByteString
    -> Maybe ByteString
    -> ActDB (AP.Errand URIMode)
prepareForwardAK actorID fwderByHash body mproof = do
    Entity keyID key <- do
        mk <- getBy $ UniqueSigKey actorID
        case mk of
            Nothing -> error "Actor has no keys!"
            Just k -> return k
    keyHash <- encodeKeyHashid keyID
    let keyR = stampRoute fwderByHash keyHash
        sign = AK.actorKeySign $ sigKeyMaterial key
        fwderR = renderLocalActor fwderByHash
    prepareToForward keyR sign False fwderR body mproof

prepareForwardP
    :: ActorId
    -> LocalActorBy KeyHashid
    -> BL.ByteString
    -> Maybe ByteString
    -> ActDB (AP.Errand URIMode)
prepareForwardP actorID fwderByHash body mproof = do
    maybeKey <- lift askLatestInstanceKey
    case maybeKey of
        Nothing -> prepareForwardAK actorID fwderByHash body mproof
        Just key -> lift $ prepareForwardIK key fwderByHash body mproof

prepareForwardH
    :: ActorId
    -> LocalActorBy KeyHashid
    -> BL.ByteString
    -> Maybe ByteString
    -> Act (AP.Errand URIMode)
prepareForwardH actorID fwderByHash body mproof = do
    maybeKey <- askLatestInstanceKey
    case maybeKey of
        Nothing -> withDB $ prepareForwardAK actorID fwderByHash body mproof
        Just key -> prepareForwardIK key fwderByHash body mproof

-- | Given a list of local recipients, which may include actors and
-- collections,
--
-- * Insert event to message queues of actors listed
-- * Insert event to message queues of local members of collections listed
-- * Launch asynchronously sending activity, with a forwarded signature, to
--   remote member of collections listed
--
-- This function reads remote recipient data and the sender's signing key from
-- the PostgreSQL database. Don't use it inside a database transaction.
--
-- For a remote author, no forwarding is done if a signature isn't provided.
forwardActivity
    :: Either (LocalActorBy Key, ActorId, OutboxItemId) (RemoteAuthor, LocalURI, Maybe ByteString)
    -> ActivityBody
    -> LocalActorBy Key
    -> ActorId
    -> RecipientRoutes
    -> ActE ()
forwardActivity sourceMaybeForward body fwderByKey fwderActorID sieve = do
    let maybeForward =
            case sourceMaybeForward of
                Left l -> Just $ Left l
                Right (author, luAct, msig) ->
                    Right . (author,luAct,) <$> msig
    for_ maybeForward $ \ source -> do
        localRecips <- do
            mrecips <- parseAudience' $ AP.activityAudience $ actbActivity body
            paudLocalRecips <$> fromMaybeE mrecips "Activity with no recipients"
        remoteRecips <-
            let localRecipsFinal = localRecipSieve' sieve False False localRecips
                justSender = Just fwderByKey
                authorAndId =
                    second (\ (author, luAct, _sig) -> (author, luAct)) source
            in  lift $ sendToLocalActors authorAndId body False justSender justSender localRecipsFinal
        errand <- lift $ do
            fwderByHash <- hashLocalActor fwderByKey
            let msig =
                    case source of
                        Left _ -> Nothing
                        Right (_, _, b) -> Just b
            prepareForwardH fwderActorID fwderByHash (actbBL body) msig
        let remoteRecipsList =
                concatMap
                    (\ ((_, h), rrs) -> NE.toList $ NE.map (ObjURI h . remoteRecipientId) rrs)
                    remoteRecips
        dt <- lift $ asksEnv stageDeliveryTheater
        lift $  liftIO $ sendHttp dt (MethodForwardRemote errand) remoteRecipsList

makeAudSenderOnly
    :: Either
        (LocalActorBy Key, ActorId, OutboxItemId)
        (RemoteAuthor, LocalURI, Maybe ByteString)
    -> Act (Aud URIMode)
makeAudSenderOnly (Left (actorByKey, _, _)) = do
    actorByHash <- hashLocalActor actorByKey
    return $ AudLocal [actorByHash] []
makeAudSenderOnly (Right (author, _, _)) = do
    let ObjURI hAuthor luAuthor = remoteAuthorURI author
    pure $ AudRemote hAuthor [luAuthor] []

makeAudSenderWithFollowers
    :: Either
        (LocalActorBy Key, ActorId, OutboxItemId)
        (RemoteAuthor, LocalURI, Maybe ByteString)
    -> ActDB (Aud URIMode)
makeAudSenderWithFollowers (Left (actorByKey, _, _)) = do
    actorByHash <- hashLocalActor actorByKey
    return $ AudLocal [actorByHash] [localActorFollowers actorByHash]
makeAudSenderWithFollowers (Right (author, _, _)) = do
    let ObjURI hAuthor luAuthor = remoteAuthorURI author
    ra <- getJust $ remoteAuthorId author
    return $
        AudRemote hAuthor [luAuthor] (maybeToList $ remoteActorFollowers ra)

getActivityURI
    :: Either
        (LocalActorBy Key, ActorId, OutboxItemId)
        (RemoteAuthor, LocalURI, Maybe ByteString)
    -> Act FedURI
getActivityURI (Left (actorByKey, _, outboxItemID)) = do
    encodeRouteHome <- getEncodeRouteHome
    actorByHash <- hashLocalActor actorByKey
    outboxItemHash <- encodeKeyHashid outboxItemID
    return $ encodeRouteHome $ activityRoute actorByHash outboxItemHash
getActivityURI (Right (author, luAct, _)) = do
    let ObjURI hAuthor _ = remoteAuthorURI author
    pure $ ObjURI hAuthor luAct

getActorURI
    :: Either
        (LocalActorBy Key, ActorId, OutboxItemId)
        (RemoteAuthor, LocalURI, Maybe ByteString)
    -> Act FedURI
getActorURI (Left (actorByKey, _, _)) = do
    encodeRouteHome <- getEncodeRouteHome
    actorByHash <- hashLocalActor actorByKey
    return $ encodeRouteHome $ renderLocalActor actorByHash
getActorURI (Right (author, _, _)) = pure $ remoteAuthorURI author

runAct :: Act a -> Handler a
runAct act = do
    theater <- YM.asksSite appTheater
    env <- YM.asksSite appEnv
    liftIO $ runActor theater env act

runActE :: ActE a -> ExceptT Text Handler a
runActE (ExceptT act) = ExceptT $ runAct act

fetchRemoteResource instanceID host localURI = do
    maybeActor <- withDB $ runMaybeT $ do
        roid <- MaybeT $ getKeyBy $ UniqueRemoteObject instanceID localURI
        MaybeT $ getBy $ UniqueRemoteActor roid
    case maybeActor of
        Just actor -> return $ Right $ Left actor
        Nothing -> do
            manager <- asksEnv envHttpManager
            errorOrResource <- AP.fetchResource manager host localURI
            case errorOrResource of
                Left maybeError ->
                    return $ Left $ maybe ResultIdMismatch ResultGetError maybeError
                Right resource -> do
                    case resource of
                        AP.ResourceActor (AP.Actor local detail) -> withDB $ do
                            roid <- either entityKey id <$> insertBy' (RemoteObject instanceID localURI)
                            let ra = RemoteActor
                                    { remoteActorIdent      = roid
                                    , remoteActorName       =
                                        AP.actorName detail <|> AP.actorUsername detail
                                    , remoteActorInbox      = AP.actorInbox local
                                    , remoteActorFollowers  = AP.actorFollowers local
                                    , remoteActorErrorSince = Nothing
                                    , remoteActorType       = AP.actorType detail
                                    }
                            Right . Left . either id id <$> insertByEntity' ra
                        AP.ResourceChild luId luManager -> do
                            roid <- withDB $ either entityKey id <$> insertBy' (RemoteObject instanceID localURI)
                            result <- fetchRemoteActor' instanceID host luManager
                            return $
                                case result of
                                    Left e -> Left $ ResultSomeException e
                                    Right (Left Nothing) -> Left ResultIdMismatch
                                    Right (Left (Just e)) -> Left $ ResultGetError e
                                    Right (Right Nothing) -> Left ResultNotActor
                                    Right (Right (Just actor)) -> Right $ Right (roid, luManager, actor)
