{- This file is part of Vervis.
 -
 - Written in 2019, 2020, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE RankNTypes #-}

-- These are for the Barbie-based generated instances
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE UndecidableInstances #-}

-- For launchActor not to need to take a Proxy
{-# LANGUAGE AllowAmbiguousTypes #-}

module Vervis.Actor
    ( -- * Local actors
      LocalActorBy (..)
    , LocalResourceBy (..)
    , LocalResourceNonGroupBy (..)
    , LocalActor
    , actorToResource
    , resourceToActor
    , resourceToNG
    , resourceFromNG

      -- * Converting between KeyHashid, Key, Identity and Entity
      --
      -- Adapted from 'Vervis.Recipient'
    , hashLocalActorPure
    , getHashLocalActor
    , hashLocalActor

    , unhashLocalActorPure
    , unhashLocalActor
    , unhashLocalActorF
    , unhashLocalActorM
    , unhashLocalActorE
    , unhashLocalActor404

    , hashLocalResourcePure
    , getHashLocalResource
    , hashLocalResource

    , unhashLocalResourcePure
    , unhashLocalResource
    , unhashLocalResourceF
    , unhashLocalResourceM
    , unhashLocalResourceE
    , unhashLocalResource404

      -- * Local recipient set
    , TicketRoutes (..)
    , ClothRoutes (..)
    , PersonRoutes (..)
    , GroupRoutes (..)
    , RepoRoutes (..)
    , DeckRoutes (..)
    , LoomRoutes (..)
    , ProjectRoutes (..)
    , FactoryRoutes (..)
    , DeckFamilyRoutes (..)
    , LoomFamilyRoutes (..)
    , RecipientRoutes (..)

      -- * AP system base types
    , RemoteAuthor (..)
    , ActivityBody (..)
    , Verse (..)
    , ClientMsg (..)

      -- * Behavior utility types
    , KeyAndRef_
    , StageEnv (..)
    , Staje
    , Act
    , ActE
    , ActDB
    , ActDBE
    , Theater

      -- * Behavior utilities
    , withDB
    , withDBExcept
    , adaptHandlerResult
    --, VervisActor (..)
    --, VervisActorLaunch (..)
    --, ActorMessage (..)
    , launchActorIO
    , launchActor

    , RemoteRecipient (..)
    , sendToLocalActors

    , actorIsAddressed

    , localActorType
    )
where

import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Except
import Control.Monad.Trans.Reader
import Data.Barbie
import Data.Bifunctor
import Data.ByteString (ByteString)
import Data.Foldable
import Data.Function
import Data.Hashable
import Data.HashMap.Strict (HashMap)
import Data.HashSet (HashSet)
import Data.HList (HList (..))
import Data.Kind
import Data.List.NonEmpty (NonEmpty)
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Data.Typeable
import Database.Persist.Sql
import Fcf
import GHC.Generics
import Network.HTTP.Client
import UnliftIO.Exception
import Web.Hashids
import Yesod.Core

import qualified Control.Monad.Fail as F
import qualified Data.Aeson as A
import qualified Data.ByteString.Lazy as BL
import qualified Data.HashSet as HS
import qualified Data.HashMap.Strict as HM
import qualified Data.HList as H
import qualified Data.List.NonEmpty as NE
import qualified Data.List.Ordered as LO
import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Control.Concurrent.Actor
import Network.FedURI
import Web.Actor
import Web.Actor.Deliver
import Web.Actor.Persist
import Yesod.Hashids
import Yesod.MonadSite

import qualified Crypto.ActorKey as AK
import qualified Web.ActivityPub as AP

import Data.List.NonEmpty.Local

import Vervis.FedURI
import Vervis.Model hiding (Actor, Message)
import Vervis.RemoteActorStore.Types
import Vervis.Settings

data LocalActorBy f
    = LocalActorPerson  (f Person)
    | LocalActorGroup   (f Group)
    | LocalActorRepo    (f Repo)
    | LocalActorDeck    (f Deck)
    | LocalActorLoom    (f Loom)
    | LocalActorProject (f Project)
    | LocalActorFactory (f Factory)
    deriving (Generic, FunctorB, ConstraintsB)

deriving instance AllBF Eq f LocalActorBy => Eq (LocalActorBy f)
deriving instance AllBF Ord f LocalActorBy => Ord (LocalActorBy f)
deriving instance AllBF Hashable f LocalActorBy => Hashable (LocalActorBy f)
deriving instance AllBF Show f LocalActorBy => Show (LocalActorBy f)

data LocalResourceBy f
    = LocalResourceGroup   (f Group)
    | LocalResourceRepo    (f Repo)
    | LocalResourceDeck    (f Deck)
    | LocalResourceLoom    (f Loom)
    | LocalResourceProject (f Project)
    | LocalResourceFactory (f Factory)
    deriving (Generic, FunctorB, ConstraintsB)

deriving instance AllBF Eq f LocalResourceBy => Eq (LocalResourceBy f)

data LocalResourceNonGroupBy f
    = LocalResourceRepo'    (f Repo)
    | LocalResourceDeck'    (f Deck)
    | LocalResourceLoom'    (f Loom)
    | LocalResourceProject' (f Project)
    | LocalResourceFactory' (f Factory)
    deriving (Generic, FunctorB, ConstraintsB)

deriving instance AllBF Eq f LocalResourceNonGroupBy => Eq (LocalResourceNonGroupBy f)

type LocalActor = LocalActorBy KeyHashid

actorToResource = \case
    LocalActorPerson _ -> Nothing
    LocalActorGroup g -> Just $ LocalResourceGroup g
    LocalActorRepo r -> Just $ LocalResourceRepo r
    LocalActorDeck d -> Just $ LocalResourceDeck d
    LocalActorLoom l -> Just $ LocalResourceLoom l
    LocalActorProject j -> Just $ LocalResourceProject j
    LocalActorFactory f -> Just $ LocalResourceFactory f

resourceToActor = \case
    LocalResourceGroup g -> LocalActorGroup g
    LocalResourceRepo r -> LocalActorRepo r
    LocalResourceDeck d -> LocalActorDeck d
    LocalResourceLoom l -> LocalActorLoom l
    LocalResourceProject j -> LocalActorProject j
    LocalResourceFactory f -> LocalActorFactory f

resourceToNG = \case
    LocalResourceGroup _ -> Nothing
    LocalResourceRepo r -> Just $ LocalResourceRepo' r
    LocalResourceDeck d -> Just $ LocalResourceDeck' d
    LocalResourceLoom l -> Just $ LocalResourceLoom' l
    LocalResourceProject j -> Just $ LocalResourceProject' j
    LocalResourceFactory f -> Just $ LocalResourceFactory' f

resourceFromNG = \case
    LocalResourceRepo' r -> LocalResourceRepo r
    LocalResourceDeck' d -> LocalResourceDeck d
    LocalResourceLoom' l -> LocalResourceLoom l
    LocalResourceProject' j -> LocalResourceProject j
    LocalResourceFactory' f -> LocalResourceFactory f

hashLocalActorPure
    :: HashidsContext -> LocalActorBy Key -> LocalActorBy KeyHashid
hashLocalActorPure ctx = f
    where
    f (LocalActorPerson p)  = LocalActorPerson $ encodeKeyHashidPure ctx p
    f (LocalActorGroup g)   = LocalActorGroup $ encodeKeyHashidPure ctx g
    f (LocalActorRepo r)    = LocalActorRepo $ encodeKeyHashidPure ctx r
    f (LocalActorDeck d)    = LocalActorDeck $ encodeKeyHashidPure ctx d
    f (LocalActorLoom l)    = LocalActorLoom $ encodeKeyHashidPure ctx l
    f (LocalActorProject j) = LocalActorProject $ encodeKeyHashidPure ctx j
    f (LocalActorFactory f) = LocalActorFactory $ encodeKeyHashidPure ctx f

getHashLocalActor
    :: (MonadActor m, StageHashids (MonadActorStage m))
    => m (LocalActorBy Key -> LocalActorBy KeyHashid)
getHashLocalActor = do
    ctx <- asksEnv stageHashidsContext
    return $ hashLocalActorPure ctx

hashLocalActor
    :: (MonadActor m, StageHashids (MonadActorStage m))
    => LocalActorBy Key -> m (LocalActorBy KeyHashid)
hashLocalActor actor = do
    hash <- getHashLocalActor
    return $ hash actor

unhashLocalActorPure
    :: HashidsContext -> LocalActorBy KeyHashid -> Maybe (LocalActorBy Key)
unhashLocalActorPure ctx = f
    where
    f (LocalActorPerson p)  = LocalActorPerson <$> decodeKeyHashidPure ctx p
    f (LocalActorGroup g)   = LocalActorGroup <$> decodeKeyHashidPure ctx g
    f (LocalActorRepo r)    = LocalActorRepo <$> decodeKeyHashidPure ctx r
    f (LocalActorDeck d)    = LocalActorDeck <$> decodeKeyHashidPure ctx d
    f (LocalActorLoom l)    = LocalActorLoom <$> decodeKeyHashidPure ctx l
    f (LocalActorProject j) = LocalActorProject <$> decodeKeyHashidPure ctx j
    f (LocalActorFactory f) = LocalActorFactory <$> decodeKeyHashidPure ctx f

unhashLocalActor
    :: (MonadActor m, StageHashids (MonadActorStage m))
    => LocalActorBy KeyHashid -> m (Maybe (LocalActorBy Key))
unhashLocalActor actor = do
    ctx <- asksEnv stageHashidsContext
    return $ unhashLocalActorPure ctx actor

unhashLocalActorF
    :: (F.MonadFail m, MonadActor m, StageHashids (MonadActorStage m))
    => LocalActorBy KeyHashid -> String -> m (LocalActorBy Key)
unhashLocalActorF actor e = maybe (F.fail e) return =<< unhashLocalActor actor

unhashLocalActorM
    :: (MonadActor m, StageHashids (MonadActorStage m))
    => LocalActorBy KeyHashid -> MaybeT m (LocalActorBy Key)
unhashLocalActorM = MaybeT . unhashLocalActor

unhashLocalActorE
    :: (MonadActor m, StageHashids (MonadActorStage m))
    => LocalActorBy KeyHashid -> e -> ExceptT e m (LocalActorBy Key)
unhashLocalActorE actor e =
    ExceptT $ maybe (Left e) Right <$> unhashLocalActor actor

unhashLocalActor404
    :: ( MonadSite m
       , MonadHandler m
       , HandlerSite m ~ SiteEnv m
       , YesodHashids (HandlerSite m)
       )
    => LocalActorBy KeyHashid
    -> m (LocalActorBy Key)
unhashLocalActor404 actor = maybe notFound return =<< unhashLocalActor actor
    where
    unhashLocalActor byHash = do
        ctx <- asksSite siteHashidsContext
        return $ unhashLocalActorPure ctx byHash

hashLocalResourcePure
    :: HashidsContext -> LocalResourceBy Key -> LocalResourceBy KeyHashid
hashLocalResourcePure ctx = f
    where
    f (LocalResourceGroup g)   = LocalResourceGroup $ encodeKeyHashidPure ctx g
    f (LocalResourceRepo r)    = LocalResourceRepo $ encodeKeyHashidPure ctx r
    f (LocalResourceDeck d)    = LocalResourceDeck $ encodeKeyHashidPure ctx d
    f (LocalResourceLoom l)    = LocalResourceLoom $ encodeKeyHashidPure ctx l
    f (LocalResourceProject j) = LocalResourceProject $ encodeKeyHashidPure ctx j
    f (LocalResourceFactory f) = LocalResourceFactory $ encodeKeyHashidPure ctx f

getHashLocalResource
    :: (MonadActor m, StageHashids (MonadActorStage m))
    => m (LocalResourceBy Key -> LocalResourceBy KeyHashid)
getHashLocalResource = do
    ctx <- asksEnv stageHashidsContext
    return $ hashLocalResourcePure ctx

hashLocalResource
    :: (MonadActor m, StageHashids (MonadActorStage m))
    => LocalResourceBy Key -> m (LocalResourceBy KeyHashid)
hashLocalResource actor = do
    hash <- getHashLocalResource
    return $ hash actor

unhashLocalResourcePure
    :: HashidsContext -> LocalResourceBy KeyHashid -> Maybe (LocalResourceBy Key)
unhashLocalResourcePure ctx = f
    where
    f (LocalResourceGroup g)   = LocalResourceGroup <$> decodeKeyHashidPure ctx g
    f (LocalResourceRepo r)    = LocalResourceRepo <$> decodeKeyHashidPure ctx r
    f (LocalResourceDeck d)    = LocalResourceDeck <$> decodeKeyHashidPure ctx d
    f (LocalResourceLoom l)    = LocalResourceLoom <$> decodeKeyHashidPure ctx l
    f (LocalResourceProject j) = LocalResourceProject <$> decodeKeyHashidPure ctx j
    f (LocalResourceFactory f) = LocalResourceFactory <$> decodeKeyHashidPure ctx f

unhashLocalResource
    :: (MonadActor m, StageHashids (MonadActorStage m))
    => LocalResourceBy KeyHashid -> m (Maybe (LocalResourceBy Key))
unhashLocalResource actor = do
    ctx <- asksEnv stageHashidsContext
    return $ unhashLocalResourcePure ctx actor

unhashLocalResourceF
    :: (F.MonadFail m, MonadActor m, StageHashids (MonadActorStage m))
    => LocalResourceBy KeyHashid -> String -> m (LocalResourceBy Key)
unhashLocalResourceF actor e = maybe (F.fail e) return =<< unhashLocalResource actor

unhashLocalResourceM
    :: (MonadActor m, StageHashids (MonadActorStage m))
    => LocalResourceBy KeyHashid -> MaybeT m (LocalResourceBy Key)
unhashLocalResourceM = MaybeT . unhashLocalResource

unhashLocalResourceE
    :: (MonadActor m, StageHashids (MonadActorStage m))
    => LocalResourceBy KeyHashid -> e -> ExceptT e m (LocalResourceBy Key)
unhashLocalResourceE actor e =
    ExceptT $ maybe (Left e) Right <$> unhashLocalResource actor

unhashLocalResource404
    :: ( MonadSite m
       , MonadHandler m
       , HandlerSite m ~ SiteEnv m
       , YesodHashids (HandlerSite m)
       )
    => LocalResourceBy KeyHashid
    -> m (LocalResourceBy Key)
unhashLocalResource404 actor = maybe notFound return =<< unhashLocalResource actor
    where
    unhashLocalResource byHash = do
        ctx <- asksSite siteHashidsContext
        return $ unhashLocalResourcePure ctx byHash

data TicketRoutes = TicketRoutes
    { routeTicketFollowers :: Bool
    }
    deriving Eq

data ClothRoutes = ClothRoutes
    { routeClothFollowers :: Bool
    }
    deriving Eq

data PersonRoutes = PersonRoutes
    { routePerson          :: Bool
    , routePersonFollowers :: Bool
    }
    deriving Eq

data GroupRoutes = GroupRoutes
    { routeGroup          :: Bool
    , routeGroupFollowers :: Bool
    }
    deriving Eq

data RepoRoutes = RepoRoutes
    { routeRepo          :: Bool
    , routeRepoFollowers :: Bool
    }
    deriving Eq

data DeckRoutes = DeckRoutes
    { routeDeck          :: Bool
    , routeDeckFollowers :: Bool
    }
    deriving Eq

data LoomRoutes = LoomRoutes
    { routeLoom          :: Bool
    , routeLoomFollowers :: Bool
    }
    deriving Eq

data ProjectRoutes = ProjectRoutes
    { routeProject          :: Bool
    , routeProjectFollowers :: Bool
    }
    deriving Eq

data FactoryRoutes = FactoryRoutes
    { routeFactory          :: Bool
    , routeFactoryFollowers :: Bool
    }
    deriving Eq

data DeckFamilyRoutes = DeckFamilyRoutes
    { familyDeck    :: DeckRoutes
    , familyTickets :: [(KeyHashid TicketDeck, TicketRoutes)]
    }
    deriving Eq

data LoomFamilyRoutes = LoomFamilyRoutes
    { familyLoom   :: LoomRoutes
    , familyCloths :: [(KeyHashid TicketLoom, ClothRoutes)]
    }
    deriving Eq

data RecipientRoutes = RecipientRoutes
    { recipPeople   :: [(KeyHashid Person , PersonRoutes)]
    , recipGroups   :: [(KeyHashid Group  , GroupRoutes)]
    , recipRepos    :: [(KeyHashid Repo   , RepoRoutes)]
    , recipDecks    :: [(KeyHashid Deck   , DeckFamilyRoutes)]
    , recipLooms    :: [(KeyHashid Loom   , LoomFamilyRoutes)]
    , recipProjects :: [(KeyHashid Project, ProjectRoutes)]
    , recipFactories :: [(KeyHashid Factory, FactoryRoutes)]
    }
    deriving Eq

data RemoteAuthor = RemoteAuthor
    { remoteAuthorURI      :: FedURI
    , remoteAuthorInstance :: InstanceId
    , remoteAuthorId       :: RemoteActorId
    }

data ActivityBody = ActivityBody
    { actbBL         :: BL.ByteString
    , actbObject     :: A.Object
    , actbActivity   :: AP.Activity URIMode
    }

data Verse = Verse
    { verseSource
        :: Either
            (LocalActorBy Key, ActorId, OutboxItemId)
            (RemoteAuthor, LocalURI, Maybe ByteString)
    , verseBody :: ActivityBody
    }

data ClientMsg = ClientMsg
    { cmMaybeCap     :: Maybe (Either (LocalActorBy Key, OutboxItemId) FedURI)
    , cmLocalRecips  :: RecipientRoutes
    , cmRemoteRecips :: [(Host, NonEmpty LocalURI)]
    , cmFwdHosts     :: [Host]
    , cmAction       :: AP.Action URIMode
    }

data Staje

type Ret :: Signature
type Ret = Return (Either Text Text)

instance Actor Person where
    type ActorStage   Person = Staje
    type ActorIdentity     Person = PersonId
    type ActorInterface Person =
        [ "verse" ::: Verse :-> Ret
        , "client" ::: ClientMsg :-> Return (Either Text OutboxItemId)
        , "init" ::: Ret
        ]
instance Actor Deck where
    type ActorStage   Deck = Staje
    type ActorIdentity     Deck = DeckId
    type ActorInterface Deck =
        [ "verse" ::: Verse :-> Ret
        , "init" ::: Either (LocalActorBy Key, ActorId, OutboxItemId) (RemoteAuthor, LocalURI) :-> Maybe (Either DeckId FedURI) :-> Ret
        ]
instance Actor Loom where
    type ActorStage   Loom = Staje
    type ActorIdentity     Loom = LoomId
    type ActorInterface Loom =
        '[ "verse" ::: Verse :-> Ret
         ]
instance Actor Repo where
    type ActorStage   Repo = Staje
    type ActorIdentity     Repo = RepoId
    type ActorInterface Repo =
        [ "verse" ::: Verse :-> Ret
        , "wait-during-push" ::: IO () :-> Ret
        , "init" ::: Either (LocalActorBy Key, ActorId, OutboxItemId) (RemoteAuthor, LocalURI) :-> Maybe (Either RepoId FedURI) :-> Ret
        ]
instance Actor Project where
    type ActorStage   Project = Staje
    type ActorIdentity     Project = ProjectId
    type ActorInterface Project =
        [ "verse" ::: Verse :-> Ret
        , "init" ::: (Either (LocalActorBy Key, ActorId, OutboxItemId) (RemoteAuthor, LocalURI)) :-> Ret
        ]
instance Actor Group where
    type ActorStage   Group = Staje
    type ActorIdentity     Group = GroupId
    type ActorInterface Group =
        [ "verse" ::: Verse :-> Ret
        , "init" ::: (Either (LocalActorBy Key, ActorId, OutboxItemId) (RemoteAuthor, LocalURI)) :-> Ret
        ]
instance Actor Factory where
    type ActorStage   Factory = Staje
    type ActorIdentity     Factory = FactoryId
    type ActorInterface Factory =
        [ "verse" ::: Verse :-> Ret
        , "verified" ::: PersonId :-> Ret
        ]

{-
instance VervisActor Person where
    actorVerse = PersonMsgVerse
    toVerse (PersonMsgVerse v) = Just v
    toVerse _                  = Nothing
instance VervisActor Project where
    actorVerse = ProjectMsgVerse
    toVerse (ProjectMsgVerse v) = Just v
    toVerse _                   = Nothing
instance VervisActor Group where
    actorVerse = TeamMsgVerse
    toVerse (TeamMsgVerse v) = Just v
    toVerse _                = Nothing
instance VervisActor Deck where
    actorVerse = DeckMsgVerse
    toVerse (DeckMsgVerse v) = Just v
    toVerse _                = Nothing
instance VervisActor Loom where
    actorVerse = MsgL
    toVerse (MsgL v) = Just v
instance VervisActor Repo where
    actorVerse = MsgR . Left
    toVerse (MsgR e) =
        case e of
            Left v -> Just v
            Right _ -> Nothing
instance VervisActor Factory where
    actorVerse = FactoryMsgVerse
    toVerse (FactoryMsgVerse v) = Just v
    toVerse _                   = Nothing
-}

data KeyAndRef_ :: Type -> Exp Type
type instance Eval (KeyAndRef_ a) = TVar (HashMap (Key a) (Ref a))

instance Stage Staje where
    data StageEnv Staje = forall y. (Typeable y, Yesod y) => Env
        -- | Data to which every actor has access. Since such data can be passed to the
        -- behavior function when launching the actor, having a dedicated datatype is
        -- just convenience. The main reason is to allow 'runDB' not to take a
        -- connection pool parameter, instead grabbing it from the ReaderT. Another
        -- reason is to avoid the clutter of passing the same arguments manually
        -- everywhere.
        --
        -- The purpose of Env is to hold the system stuff: DB connection pool,
        -- settings, HTTP manager, etc. etc. while the data stuff (actual info of the
        -- actor) is meant to be passed as parameters of the behavior function.
        --
        -- Maybe in the future there won't be data shared by all actors, and then this
        -- type can be removed.
        { envSettings        :: AppSettings
        , envDbPool          :: ConnectionPool
        , envHashidsContext  :: HashidsContext
        , envActorKeys       :: Maybe (TVar (AK.ActorKey, AK.ActorKey, Bool))
        , envDeliveryTheater :: DeliveryTheater URIMode
        , envYesodRender     :: YesodRender y
        , envHttpManager     :: Manager
        , envFetch           :: ActorFetchShare
        , envActors          :: HList (Eval (Map KeyAndRef_ (StageActors Staje)))
        }
        deriving Typeable
    type StageActors Staje = [Person, Project, Group, Deck, Loom, Repo, Factory]
    type StageSpawn Staje = AllowSpawn

type YesodRender y = Route y -> [(Text, Text)] -> Text

instance StageWeb Staje where
    type StageURIMode Staje = URIMode
    --type StageRoute Staje = Route Site
    stageInstanceHost = appInstanceHost . envSettings
    stageDeliveryTheater = envDeliveryTheater

instance StageHashids Staje where
    stageHashidsContext = envHashidsContext

type Act = ActFor Staje

type ActE = ActForE Staje

type ActDB = SqlPersistT Act

type ActDBE = ExceptT Text ActDB

type Theater = TheaterFor Staje

-- | Run a database transaction. If an exception is thrown, the whole
-- transaction is aborted.
withDB :: ActDB a -> Act a
withDB action = do
    env <- askEnv
    runPool (appDatabaseConf $ envSettings env) action (envDbPool env)

newtype FedError = FedError Text deriving Show

instance Exception FedError

-- | Like 'withDB', but supports errors via 'ExceptT. If an exception is
-- thrown, either via the 'ExceptT' or via regular throwing, the whole
-- transaction is aborted.
withDBExcept :: ExceptT Text (SqlPersistT Act) a -> ExceptT Text Act a
withDBExcept action = do
    result <- lift $ try $ withDB $ either abort return =<< runExceptT action
    case result of
        Left (FedError t) -> throwE t
        Right r -> return r
    where
    abort = throwIO . FedError

adaptHandlerResult
    :: ExceptT Text Act (a, Act (), Next)
    -> Act (Either Text a, Act (), Next)
adaptHandlerResult handler = do
    result <- runExceptT handler
    case result of
        Left e -> done $ Left e
        Right (r, after, next) -> return (Right r, after, next)

{-
class VervisActor a where
    actorVerse :: Verse -> ActorMessage a
    toVerse    :: ActorMessage a -> Maybe Verse

class VervisActor a => VervisActorLaunch a where
    actorBehavior' :: UTCTime -> ActorKey a -> ActorMessage a -> ActE (Text, Act (), Next)

instance (Actor a, VervisActorLaunch a, ActorReturn a ~ Either Text Text, ActorStage a ~ Staje) => ActorLaunch a where
    actorBehavior = behave actorBehavior'
-}

launchActorIO
    :: forall a ms b l .
       ( ActorLaunch a, ActorStage a ~ Staje
       , Hashable (ActorIdentity a)
       , ActorInterface a ~ ms
       , H.HOccurs
            (TVar (ActorRefMap a))
            (HList (Eval (Map ActorRefMapTVar_ (StageActors Staje))))
       , Eval (Map (AdaptedHandler Staje) ms)
         ~
         Eval
            (Map
                (Func (AdaptedAction Staje, Text))
                (Eval (Map Parcel_ ms))
            )
       , H.SameLength'
            (Eval (Map (Func (AdaptedAction Staje, Text)) (Eval (Map Parcel_ ms))))
            (Eval (Map (Handler_ a) ms))
       , H.SameLength'
            (Eval (Map (Handler_ a) ms))
            (Eval (Map (Func (AdaptedAction Staje, Text)) (Eval (Map Parcel_ ms))))
       , Eval (Constraints (Eval (Map (AdaptHandlerConstraint a) ms)))
       , Handle' (Eval (Map Parcel_ ms)) (AdaptedAction Staje, Text)
       , H.HMapAux
            HList
            (HAdaptHandler a)
            (Eval (Map (Handler_ a) ms))
            (Eval
                (Map
                    (Func (AdaptedAction Staje, Text))
                    (Eval (Map Parcel_ ms))
                )
            )
       , H.HEq
                                  (TVar (HashMap (ActorIdentity a) (Ref a)))
                                  (TVar (HashMap (Key Person) (Ref Person)))
                                  b
       , H.HOccurrence'
                          b
                          (TVar (HashMap (ActorIdentity a) (Ref a)))
                          [TVar (HashMap (Key Person) (Ref Person)),
                           TVar (HashMap (Key Project) (Ref Project)),
                           TVar (HashMap (Key Group) (Ref Group)),
                           TVar (HashMap (Key Deck) (Ref Deck)),
                           TVar (HashMap (Key Loom) (Ref Loom)),
                           TVar (HashMap (Key Repo) (Ref Repo)),
                           TVar (HashMap (Key Factory) (Ref Factory))]
                          l
       , H.HOccurs'
                          (TVar (HashMap (ActorIdentity a) (Ref a)))
                          l
                          [TVar (HashMap (Key Person) (Ref Person)),
                           TVar (HashMap (Key Project) (Ref Project)),
                           TVar (HashMap (Key Group) (Ref Group)),
                           TVar (HashMap (Key Deck) (Ref Deck)),
                           TVar (HashMap (Key Loom) (Ref Loom)),
                           TVar (HashMap (Key Repo) (Ref Repo)),
                           TVar (HashMap (Key Factory) (Ref Factory))]
       )
    => Theater
    -> StageEnv Staje
    -> ActorIdentity a
    -> IO Bool
launchActorIO theater env ident = do
    let tvar = H.hOccurs (envActors env)
    maybeRef <- HM.lookup ident <$> readTVarIO tvar
    case maybeRef of
        Just _ -> pure False
        Nothing -> do
            ref <- spawnIO @a theater ident (pure env)
            atomically $ modifyTVar' tvar $ HM.insert ident ref
            return True

launchActor
    :: forall a ms b l .
       ( ActorLaunch a, ActorStage a ~ Staje
       , Hashable (ActorIdentity a)
       , ActorInterface a ~ ms
       , H.HOccurs
            (TVar (ActorRefMap a))
            (HList (Eval (Map ActorRefMapTVar_ (StageActors Staje))))
       , Eval (Map (AdaptedHandler Staje) ms)
         ~
         Eval
            (Map
                (Func (AdaptedAction Staje, Text))
                (Eval (Map Parcel_ ms))
            )
       , H.SameLength'
            (Eval (Map (Func (AdaptedAction Staje, Text)) (Eval (Map Parcel_ ms))))
            (Eval (Map (Handler_ a) ms))
       , H.SameLength'
            (Eval (Map (Handler_ a) ms))
            (Eval (Map (Func (AdaptedAction Staje, Text)) (Eval (Map Parcel_ ms))))
       , Eval (Constraints (Eval (Map (AdaptHandlerConstraint a) ms)))
       , Handle' (Eval (Map Parcel_ ms)) (AdaptedAction Staje, Text)
       , H.HMapAux
            HList
            (HAdaptHandler a)
            (Eval (Map (Handler_ a) ms))
            (Eval
                (Map
                    (Func (AdaptedAction Staje, Text))
                    (Eval (Map Parcel_ ms))
                )
            )
       , H.HEq
                                  (TVar (HashMap (ActorIdentity a) (Ref a)))
                                  (TVar (HashMap (Key Person) (Ref Person)))
                                  b
       , H.HOccurrence'
                          b
                          (TVar (HashMap (ActorIdentity a) (Ref a)))
                          [TVar (HashMap (Key Person) (Ref Person)),
                           TVar (HashMap (Key Project) (Ref Project)),
                           TVar (HashMap (Key Group) (Ref Group)),
                           TVar (HashMap (Key Deck) (Ref Deck)),
                           TVar (HashMap (Key Loom) (Ref Loom)),
                           TVar (HashMap (Key Repo) (Ref Repo)),
                           TVar (HashMap (Key Factory) (Ref Factory))]
                          l
       , H.HOccurs'
                          (TVar (HashMap (ActorIdentity a) (Ref a)))
                          l
                          [TVar (HashMap (Key Person) (Ref Person)),
                           TVar (HashMap (Key Project) (Ref Project)),
                           TVar (HashMap (Key Group) (Ref Group)),
                           TVar (HashMap (Key Deck) (Ref Deck)),
                           TVar (HashMap (Key Loom) (Ref Loom)),
                           TVar (HashMap (Key Repo) (Ref Repo)),
                           TVar (HashMap (Key Factory) (Ref Factory))]
       )
    => ActorIdentity a
    -> Act Bool
launchActor ident = do
    env <- askEnv
    let tvar = H.hOccurs (envActors env)
    maybeRef <- liftIO $ HM.lookup ident <$> readTVarIO tvar
    case maybeRef of
        Just _ -> pure False
        Nothing -> do
            ref <- spawn @a ident (pure env)
            liftIO $ atomically $ modifyTVar' tvar $ HM.insert ident ref
            return True

data RemoteRecipient = RemoteRecipient
    { remoteRecipientActor      :: RemoteActorId
    , remoteRecipientId         :: LocalURI
    , remoteRecipientInbox      :: LocalURI
    , remoteRecipientErrorSince :: Maybe UTCTime
    }

--data MapAndSet_ :: Type -> Exp Type
--type instance Eval (MapAndSet_ a) = (Eval (KeyAndRef_ a), HashSet (ActorIdentity a))

sendVerses
    :: ( Actor a
       , ActorStage a ~ Staje
       , ActorHasMethod a "verse" (Verse :-> Return (Either Text Text))
       , Eq (ActorIdentity a)
       , H.HEq
                                  (TVar (ActorRefMap a)) (TVar (ActorRefMap Person)) b
       , H.HOccurrence'
                          b
                          (TVar (ActorRefMap a))
                          [TVar (ActorRefMap Person), TVar (ActorRefMap Project),
                           TVar (ActorRefMap Group), TVar (ActorRefMap Deck),
                           TVar (ActorRefMap Loom), TVar (ActorRefMap Repo),
                           TVar (ActorRefMap Factory)]
                          l
       , H.HOccurs'
                          (TVar (ActorRefMap a))
                          l
                          [TVar (ActorRefMap Person), TVar (ActorRefMap Project),
                           TVar (ActorRefMap Group), TVar (ActorRefMap Deck),
                           TVar (ActorRefMap Loom), TVar (ActorRefMap Repo),
                           TVar (ActorRefMap Factory)]
       )
    => Verse
    -> (TVar (HashMap (ActorIdentity a) (Ref a)), HashSet (ActorIdentity a))
    -> Act ()
sendVerses verse (tvar, s) = do
    actorMap <- liftIO $ readTVarIO tvar
    let refs = HM.elems $ actorMap `HM.intersection` HS.toMap s
    for_ refs $ \ ref -> void $ send @"verse" ref verse

data HSendVerses = HSendVerses Verse
instance
    ( Actor a
    , ActorStage a ~ Staje
    , ActorHasMethod a "verse" (Verse :-> Return (Either Text Text))
    , Eq (ActorIdentity a)
    , i ~ (TVar (HashMap (ActorIdentity a) (Ref a)), HashSet (ActorIdentity a))
    , H.HEq
                                  (TVar (ActorRefMap a)) (TVar (ActorRefMap Person)) b
    , H.HOccurrence'
                          b
                          (TVar (ActorRefMap a))
                          [TVar (ActorRefMap Person), TVar (ActorRefMap Project),
                           TVar (ActorRefMap Group), TVar (ActorRefMap Deck),
                           TVar (ActorRefMap Loom), TVar (ActorRefMap Repo),
                           TVar (ActorRefMap Factory)]
                          l
    , H.HOccurs'
                          (TVar (ActorRefMap a))
                          l
                          [TVar (ActorRefMap Person), TVar (ActorRefMap Project),
                           TVar (ActorRefMap Group), TVar (ActorRefMap Deck),
                           TVar (ActorRefMap Loom), TVar (ActorRefMap Repo),
                           TVar (ActorRefMap Factory)]
    ) =>
    H.ApplyAB HSendVerses i (Act ()) where
        applyAB (HSendVerses verse) = sendVerses verse

-- Given a list of local recipients, which may include actors and collections,
--
-- * Insert activity to message queues of live actors
-- * If collections are listed, insert activity to message queues of local
--   members and return the remote members
--
-- This function reads the follower sets and remote recipient data from the
-- PostgreSQL database. Don't use it inside a database transaction.
sendToLocalActors
    :: Either (LocalActorBy Key, ActorId, OutboxItemId) (RemoteAuthor, LocalURI)
    -- ^ Author of the activity being sent
    -> ActivityBody
    -- ^ Activity to send
    -> Bool
    -- ^ Whether to deliver to collection only if owner actor is addressed
    -> Maybe (LocalActorBy Key)
    -- ^ An actor whose collections are excluded from requiring an owner, i.e.
    --   even if owner is required, this actor's collections will be delivered
    --   to, even if this actor isn't addressed. This is meant to be the
    --   activity's sender.
    -> Maybe (LocalActorBy Key)
    -- ^ An actor whose inbox to exclude from delivery, even if this actor is
    --   listed in the recipient set. This is meant to be the activity's
    --   sender.
    -> RecipientRoutes
    -> Act [((InstanceId, Host), NonEmpty RemoteRecipient)]
sendToLocalActors authorAndId body requireOwner mauthor maidAuthor recips = do

    -- Unhash actor and work item hashids
    people <- unhashKeys $ recipPeople recips
    groups <- unhashKeys $ recipGroups recips
    repos <- unhashKeys $ recipRepos recips
    decksAndTickets <- do
        decks <- unhashKeys $ recipDecks recips
        for decks $ \ (deckID, (DeckFamilyRoutes deck tickets)) ->
            (deckID,) . (deck,) <$> unhashKeys tickets
    loomsAndCloths <- do
        looms <- unhashKeys $ recipLooms recips
        for looms $ \ (loomID, (LoomFamilyRoutes loom cloths)) ->
            (loomID,) . (loom,) <$> unhashKeys cloths
    projects <- unhashKeys $ recipProjects recips
    factories <- unhashKeys $ recipFactories recips

    -- Grab local actor sets whose stages are allowed for delivery
    let allowStages'
            :: (famili -> routes)
            -> (routes -> Bool)
            -> (Key record -> LocalActorBy Key)
            -> (Key record, famili)
            -> Bool
        allowStages' = allowStages isAuthor

        peopleForStages =
            filter (allowStages' id routePerson LocalActorPerson) people
        groupsForStages =
            filter (allowStages' id routeGroup LocalActorGroup) groups
        reposForStages =
            filter (allowStages' id routeRepo LocalActorRepo) repos
        decksAndTicketsForStages =
            filter (allowStages' fst routeDeck LocalActorDeck) decksAndTickets
        loomsAndClothsForStages =
            filter (allowStages' fst routeLoom LocalActorLoom) loomsAndCloths
        projectsForStages =
            filter (allowStages' id routeProject LocalActorProject) projects
        factoriesForStages =
            filter (allowStages' id routeFactory LocalActorFactory) factories

    -- Grab local actors being addressed
    let localActorsForSelf = concat
            [ [ LocalActorPerson key | (key, routes) <- people, routePerson routes ]
            , [ LocalActorGroup key | (key, routes) <- groups, routeGroup routes ]
            , [ LocalActorRepo key | (key, routes) <- repos, routeRepo routes ]
            , [ LocalActorDeck key | (key, (routes, _)) <- decksAndTickets, routeDeck routes ]
            , [ LocalActorLoom key | (key, (routes, _)) <- loomsAndCloths, routeLoom routes ]
            , [ LocalActorProject key | (key, routes) <- projects, routeProject routes ]
            , [ LocalActorFactory key | (key, routes) <- factories, routeFactory routes ]
            ]

    -- Grab local actors whose followers are going to be delivered to
    let personIDsForFollowers =
            [ key | (key, routes) <- peopleForStages, routePersonFollowers routes ]
        groupIDsForFollowers =
            [ key | (key, routes) <- groupsForStages, routeGroupFollowers routes ]
        repoIDsForFollowers =
            [ key | (key, routes) <- reposForStages, routeRepoFollowers routes ]
        deckIDsForFollowers =
            [ key | (key, (routes, _)) <- decksAndTicketsForStages, routeDeckFollowers routes ]
        loomIDsForFollowers =
            [ key | (key, (routes, _)) <- loomsAndClothsForStages, routeLoomFollowers routes ]
        projectIDsForFollowers =
            [ key | (key, routes) <- projectsForStages, routeProjectFollowers routes ]
        factoryIDsForFollowers =
            [ key | (key, routes) <- factoriesForStages, routeFactoryFollowers routes ]

    -- Grab tickets and cloths whose followers are going to be delivered to
    let ticketSetsForFollowers =
            mapMaybe
                (\ (deckID, (_, tickets)) -> (deckID,) <$>
                        NE.nonEmpty
                        [ ticketDeckID | (ticketDeckID, routes) <- tickets
                                       , routeTicketFollowers routes
                        ]
                )
                decksAndTicketsForStages
        clothSetsForFollowers =
            mapMaybe
                (\ (loomID, (_, cloths)) -> (loomID,) <$>
                        NE.nonEmpty
                        [ ticketLoomID | (ticketLoomID, routes) <- cloths
                                       , routeClothFollowers routes
                        ]
                )
                loomsAndClothsForStages

    (localFollowers, remoteFollowers) <- withDB $ do
        -- Get actor and work item FollowerSet IDs from DB
        followerSetIDs <- do
            actorIDs <- concat <$> sequenceA
                [ selectActorIDs personActor personIDsForFollowers
                , selectActorIDs groupActor groupIDsForFollowers
                , selectActorIDs repoActor repoIDsForFollowers
                , selectActorIDs deckActor deckIDsForFollowers
                , selectActorIDs loomActor loomIDsForFollowers
                , selectActorIDs projectActor projectIDsForFollowers
                , selectActorIDs' factoryResource factoryIDsForFollowers
                ]
            ticketIDs <-
                concat <$>
                    ((++)
                        <$> traverse
                                (selectTicketIDs ticketDeckTicket TicketDeckDeck)
                                ticketSetsForFollowers
                        <*> traverse
                                (selectTicketIDs ticketLoomTicket TicketLoomLoom)
                                clothSetsForFollowers
                    )
            (++)
                <$> (map (actorFollowers . entityVal) <$>
                        selectList [ActorId <-. actorIDs] []
                    )
                <*> (map (ticketFollowers . entityVal) <$>
                        selectList [TicketId <-. ticketIDs] []
                    )

        -- Get the local and remote followers of the follower sets from DB
        locals <- concat <$> sequenceA
                [ selectFollowers LocalActorPerson  PersonActor  followerSetIDs
                , selectFollowers LocalActorGroup   GroupActor   followerSetIDs
                , selectFollowers LocalActorRepo    RepoActor    followerSetIDs
                , selectFollowers LocalActorDeck    DeckActor    followerSetIDs
                , selectFollowers LocalActorLoom    LoomActor    followerSetIDs
                , selectFollowers LocalActorProject ProjectActor followerSetIDs
                , selectFollowers' LocalActorFactory FactoryResource followerSetIDs
                ]
        remotes <- getRemoteFollowers followerSetIDs
        return (locals, remotes)

    -- Insert activity to message queues of all local live actors who are
    -- recipients, i.e. either directly addressed or listed in a local stage
    -- addressed
    --
    -- Since 'sendMany' is temporarily unavailable, we just use plain send
    let liveRecips =
            let s = HS.fromList $ localFollowers ++ localActorsForSelf
            in  case maidAuthor of
                    Nothing -> s
                    Just a -> HS.delete a s
        authorAndId' =
            second (\ (author, luAct) -> (author, luAct, Nothing)) authorAndId
        (liveRecipsP, liveRecipsJ, liveRecipsG, liveRecipsD, liveRecipsL, liveRecipsR, liveRecipsF) =
            partitionByActor liveRecips
        verse = Verse authorAndId' body
    {-
    sendMany $
        (Just (liveRecipsP, actorVerse verse)) `H.HCons`
        (Just (liveRecipsJ, actorVerse verse)) `H.HCons`
        (Just (liveRecipsG, actorVerse verse)) `H.HCons`
        (Just (liveRecipsD, actorVerse verse)) `H.HCons`
        (Just (liveRecipsL, actorVerse verse)) `H.HCons`
        (Just (liveRecipsR, actorVerse verse)) `H.HCons`
        (Just (liveRecipsF, actorVerse verse)) `H.HCons` H.HNil
    -}
    let actorSets =
            liveRecipsP `HCons` liveRecipsJ `HCons` liveRecipsG `HCons`
            liveRecipsD `HCons` liveRecipsL `HCons` liveRecipsR `HCons`
            liveRecipsF `HCons` HNil
    actorMaps <- envActors <$> askEnv
    {-
    let sendVerses'
            :: ( ActorStage a ~ Staje
               , ActorHasMethod a "verse" (Verse :-> Return (Either Text Text))
               )
            => (TVar (HashMap (ActorIdentity a) (Ref a)), HashSet (ActorIdentity a))
            -> Act ()
        sendVerses' = sendVerses verse
    -}
    H.hMapM_ (HSendVerses verse) (H.hZip actorMaps actorSets) -- :: HList (Eval (Map MapAndSet_ (StageActors Staje))))

    -- Return remote followers, to whom we need to deliver via HTTP
    return remoteFollowers
    where
    orderedUnion = foldl' LO.union []

    unhashKeys
        :: ToBackendKey SqlBackend record
        => [(KeyHashid record, routes)]
        -> Act [(Key record, routes)]
    unhashKeys actorSets = do
        unhash <- decodeKeyHashidPure <$> asksEnv stageHashidsContext
        return $ mapMaybe (unhashKey unhash) actorSets
        where
        unhashKey unhash (hash, famili) = (,famili) <$> unhash hash

    isAuthor =
        case mauthor of
            Nothing -> const False
            Just author -> (== author)

    allowStages
        :: (LocalActorBy Key -> Bool)
        -> (famili -> routes)
        -> (routes -> Bool)
        -> (Key record -> LocalActorBy Key)
        -> (Key record, famili)
        -> Bool
    allowStages isAuthor familyActor routeActor makeActor (actorID, famili)
        =  routeActor (familyActor famili)
        || not requireOwner
        || isAuthor (makeActor actorID)

    selectActorIDs
        :: (MonadIO m, PersistRecordBackend record SqlBackend)
        => (record -> ActorId)
        -> [Key record]
        -> ReaderT SqlBackend m [ActorId]
    selectActorIDs grabActor ids =
        map (grabActor . entityVal) <$> selectList [persistIdField <-. ids] []

    selectActorIDs'
        :: (MonadIO m, PersistRecordBackend record SqlBackend)
        => (record -> ResourceId)
        -> [Key record]
        -> ReaderT SqlBackend m [ActorId]
    selectActorIDs' grabResource ids = do
        resourceIDs <- map (grabResource . entityVal) <$> selectList [persistIdField <-. ids] []
        map (resourceActor . entityVal) <$> selectList [ResourceId <-. resourceIDs] []

    selectTicketIDs
        :: ( MonadIO m
           , PersistRecordBackend tracker SqlBackend
           , PersistRecordBackend item SqlBackend
           )
        => (item -> TicketId)
        -> EntityField item (Key tracker)
        -> (Key tracker, NonEmpty (Key item))
        -> ReaderT SqlBackend m [TicketId]
    selectTicketIDs grabTicket trackerField (trackerID, workItemIDs) = do
        maybeTracker <- get trackerID
        case maybeTracker of
            Nothing -> pure []
            Just _ ->
                map (grabTicket . entityVal) <$>
                    selectList [persistIdField <-. NE.toList workItemIDs, trackerField ==. trackerID] []

    getRemoteFollowers
        :: MonadIO m
        => [FollowerSetId]
        -> ReaderT SqlBackend m
            [((InstanceId, Host), NonEmpty RemoteRecipient)]
    getRemoteFollowers fsids =
        fmap groupRemotes $
            E.select $ E.from $ \ (rf `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
                E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
                E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
                E.on $ rf E.^. RemoteFollowActor E.==. ra E.^. RemoteActorId
                E.where_ $ rf E.^. RemoteFollowTarget `E.in_` E.valList fsids
                E.orderBy [E.asc $ i E.^. InstanceId, E.asc $ ra E.^. RemoteActorId]
                return
                    ( i E.^. InstanceId
                    , i E.^. InstanceHost
                    , ra E.^. RemoteActorId
                    , ro E.^. RemoteObjectIdent
                    , ra E.^. RemoteActorInbox
                    , ra E.^. RemoteActorErrorSince
                    )
        where
        groupRemotes = groupWithExtractBy ((==) `on` fst) fst snd . map toTuples
            where
            toTuples (E.Value iid, E.Value h, E.Value raid, E.Value luA, E.Value luI, E.Value ms) = ((iid, h), RemoteRecipient raid luA luI ms)

    selectFollowers makeLocalActor actorField followerSetIDs =
        fmap (map (makeLocalActor . E.unValue)) $
        E.select $ E.from $ \ (f `E.InnerJoin` p) -> do
            E.on $ f E.^. FollowActor E.==. p E.^. actorField
            E.where_ $ f E.^. FollowTarget `E.in_` E.valList followerSetIDs
            return $ p E.^. persistIdField

    selectFollowers' makeLocalActor resourceField followerSetIDs =
        fmap (map (makeLocalActor . E.unValue)) $
        E.select $ E.from $ \ (f `E.InnerJoin` r `E.InnerJoin` p) -> do
            E.on $ r E.^. ResourceId E.==. p E.^. resourceField
            E.on $ f E.^. FollowActor E.==. r E.^. ResourceActor
            E.where_ $ f E.^. FollowTarget `E.in_` E.valList followerSetIDs
            return $ p E.^. persistIdField

    partitionByActor
        :: HashSet (LocalActorBy Key)
        -> ( HashSet PersonId
           , HashSet ProjectId
           , HashSet GroupId
           , HashSet DeckId
           , HashSet LoomId
           , HashSet RepoId
           , HashSet FactoryId
           )
    partitionByActor = foldl' f (HS.empty, HS.empty, HS.empty, HS.empty, HS.empty, HS.empty, HS.empty)
        where
        f (p, j, g, d, l, r, f') (LocalActorPerson k) =
            (HS.insert k p, j, g, d, l, r, f')
        f (p, j, g, d, l, r, f') (LocalActorProject k) =
            (p, HS.insert k j, g, d, l, r, f')
        f (p, j, g, d, l, r, f') (LocalActorGroup k) =
            (p, j, HS.insert k g, d, l, r, f')
        f (p, j, g, d, l, r, f') (LocalActorDeck k) =
            (p, j, g, HS.insert k d, l, r, f')
        f (p, j, g, d, l, r, f') (LocalActorLoom k) =
            (p, j, g, d, HS.insert k l, r, f')
        f (p, j, g, d, l, r, f') (LocalActorRepo k) =
            (p, j, g, d, l, HS.insert k r, f')
        f (p, j, g, d, l, r, f') (LocalActorFactory k) =
            (p, j, g, d, l, r, HS.insert k f')

actorIsAddressed :: RecipientRoutes -> LocalActor -> Bool
actorIsAddressed recips = isJust . verify
    where
    verify (LocalActorPerson p) = do
        routes <- lookup p $ recipPeople recips
        guard $ routePerson routes
    verify (LocalActorGroup g) = do
        routes <- lookup g $ recipGroups recips
        guard $ routeGroup routes
    verify (LocalActorRepo r) = do
        routes <- lookup r $ recipRepos recips
        guard $ routeRepo routes
    verify (LocalActorDeck d) = do
        routes <- lookup d $ recipDecks recips
        guard $ routeDeck $ familyDeck routes
    verify (LocalActorLoom l) = do
        routes <- lookup l $ recipLooms recips
        guard $ routeLoom $ familyLoom routes
    verify (LocalActorProject j) = do
        routes <- lookup j $ recipProjects recips
        guard $ routeProject routes
    verify (LocalActorFactory f) = do
        routes <- lookup f $ recipFactories recips
        guard $ routeFactory routes

localActorType :: LocalActorBy f -> AP.ActorType
localActorType = \case
    LocalActorPerson _ -> AP.ActorTypePerson
    LocalActorRepo _ -> AP.ActorTypeRepo
    LocalActorDeck _ -> AP.ActorTypeTicketTracker
    LocalActorLoom _ -> AP.ActorTypePatchTracker
    LocalActorProject _ -> AP.ActorTypeProject
    LocalActorGroup _ -> AP.ActorTypeTeam
    LocalActorFactory _ -> AP.ActorTypeFactory
