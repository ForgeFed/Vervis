{- This file is part of Vervis.
 -
 - Written in 2016, 2018, 2019, 2020, 2022, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Git
    ( readSourceView
    , readChangesView
    , readPatch
    , writePostReceiveHooks
    , generateGitPatches
    , canApplyGitPatches
    , applyGitPatches
    )
where

import Control.Arrow ((***))
import Control.Exception.Base
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Except
import Data.Bifunctor
import Data.Foldable
import Data.List.NonEmpty (NonEmpty ((:|)))
import Data.Maybe
import Data.Set (Set)
import Data.String (fromString)
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8, decodeUtf8)
import Data.Time.Calendar (Day (..))
import Data.Time.Clock
import Data.Traversable (for)
import Data.Word (Word32)
import Database.Persist
import System.Exit
import System.FilePath
import System.Process.Typed
--import Text.Diff.Parse
--import Text.Diff.Parse.Types
import Text.Email.Validate (emailAddress)

import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import qualified Data.DList as D (DList, empty, snoc, toList)
import qualified Data.List.NonEmpty as NE
import qualified Data.Set as S (member, mapMonotonic, toList)
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import qualified Data.Text.Encoding.Error as TE (lenientDecode)
import qualified Data.Vector as V
import qualified Database.Esqueleto as E

import Data.ObjId
import Development.Git
import Development.PatchMediaType
import Network.FedURI
import Yesod.ActivityPub
import Yesod.Hashids
import Yesod.MonadSite

import qualified Data.VersionControl as VC

import Control.Monad.Trans.Except.Local
import Data.ByteString.Char8.Local (takeLine)
--import Data.DList.Local
import Data.EventTime.Local
import Data.List.Local
import Data.Time.Clock.Local
import System.Process.Typed.Local

import qualified Data.Patch.Local as P
import qualified Data.Text.UTF8.Local as TU

import Vervis.Changes
import Vervis.Foundation
import Vervis.Model
import Vervis.Model.Ident
import Vervis.Path
import Vervis.Readme
import Vervis.Settings
import Vervis.SourceTree

matchReadme (TreeEntry _ (TETFile _) _ name) = isReadme name
matchReadme _                                = False

-- | Find a README file in a directory. Return the filename and the file
-- content.
findReadme :: [TreeEntry] -> GitT IO (Maybe (Text, Text))
findReadme entries =
    case find matchReadme entries of
        Nothing -> return Nothing
        Just (TreeEntry _ _ hash name) ->
            Just . (name,) <$> gitGetFileContentByHash hash

matchType (TETFile _) = TypeBlob
matchType TETDir      = TypeTree

rowToEntry (TreeEntry _ typ _ name) = DirEntry (matchType typ) name

loadSourceView
    :: Text
    -> [Text]
    -> GitT IO (Set Text, Set Text, Maybe (SourceView Text))
loadSourceView ref dir = do
    let invalid t = T.null t || t == "." || t == ".." || T.any (== '/') t
    when (any invalid dir) $
        error $ "loadSourceView invalid dir: " ++ show dir
    branches <- gitListBranches
    tags <- gitListTags
    msv <-
        if null branches
            then return $ Just $ SourceDir $ DirectoryView Nothing [] Nothing
            else if ref `S.member` branches || ref `S.member` tags
                then Just <$> do
                    let dir' =
                            if null dir
                                then Nothing
                                else Just $ T.unpack $ T.intercalate "/" dir
                    pt <-
                        case dir' of
                            Nothing -> pure PTTree
                            Just s -> gitGetPathType ref s
                    case pt of
                        PTTree -> do
                            entries <- gitListDir ref dir'
                            mreadme <- findReadme entries
                            let ents = map rowToEntry entries
                                mname =
                                    if isNothing dir'
                                        then Nothing
                                        else Just $ last dir
                            return $
                                SourceDir $ DirectoryView mname ents mreadme
                        PTBlob -> do
                            (name, path) <-
                                case dir' of
                                    Nothing -> error "loadSourceView: Top-level is expected to be a dir, not a file"
                                    Just s -> pure (last dir, s)
                            body <- gitGetFileContentByPath ref path
                            return $ SourceFile $ FileView name body
                else return Nothing
    return (branches, tags, msv)

readSourceView
    :: Text
    -- ^ Name of branch or tag
    -> [Text]
    -- ^ Path in the source tree pointing to a file or directory
    -> GitT IO (Set Text, Set Text, Maybe (SourceView Widget))
    -- ^ Branches, tags, view of the selected item
readSourceView ref dir = do
    (bs, ts, msv) <- loadSourceView ref dir
    return (bs, ts, renderSources dir <$> msv)

readChangesView
    :: Text
    -- ^ Name of branch or tag
    -> Int
    -- ^ Offset, i.e. latest commits to skip
    -> Int
    -- ^ Limit, i.e. how many latest commits to take after the offset
    -> GitT IO (Int, [LogEntry])
    -- ^ Total number of ref's changes, and view of selected ref's change log
readChangesView ref off lim = do
    commits <- gitGetCommitInfos ref [] Nothing Nothing
    now <- liftIO getCurrentTime
    let commits' = take lim $ drop off commits
        mkrow commit = LogEntry
            { leAuthor  = VC.authorName $ fst $ VC.commitWritten commit
            , leHash    = VC.commitHash commit
            , leMessage = VC.commitTitle commit
            , leTime    =
                let t = snd $ VC.commitWritten commit
                in  ( t
                    , intervalToEventTime $ FriendlyConvert $
                        now `diffUTCTime` t
                    )
            }
    return (length commits, map mkrow commits')

{-
gatherLines (Hunk _ _ lines) = map lineContent lines

toEdit (FileDelta Created _ dest Binary) =
    P.AddBinaryFile (T.unpack dest) 0 0
toEdit (FileDelta Created _ dest (Hunks hunks)) =
    P.AddTextFile (T.unpack dest) 0 $ concatMap gatherLines hunks
toEdit (FileDelta Deleted source _ Binary) =
    P.RemoveBinaryFile (T.unpack source) 0 0
toEdit (FileDelta Deleted source _ (Hunks hunks)) =
    P.RemoveTextFile (T.unpack source) 0 $ concatMap gatherLines hunks
toEdit (FileDelta Modified _ dest Binary) =
    P.EditBinaryFile (T.unpack dest) 0 0 0 0
toEdit (FileDelta Modified _ dest (Hunks [])) = error "Modified into empty?"
toEdit (FileDelta Modified _ dest (Hunks (h:unks))) =
    P.EditTextFile (T.unpack dest) V.empty (NE.map adaptHunk $ h:|unks) 0 0
    where
    adaptHunk _ = error "TODO adaptHunk: implement properly"
-}

patch :: Text -> VC.Commit -> P.Patch
patch edits (VC.Commit a c _ t d) = P.Patch (mk a) (mk <$> c) t d edits
    where
    mk = first mk'
    mk' (VC.Author n e) = P.Author n e

readPatch :: ObjId -> GitT IO (P.Patch, [ObjId])
readPatch oid = do
    commit <- gitGetCommitInfo oid
    deltas <- gitDiff oid
    parents <- gitGetCommitParents oid
    return (patch deltas commit, parents)

writePostReceiveHooks :: WorkerDB ()
writePostReceiveHooks = do
    hook <- asksSite $ appPostReceiveHookFile . appSettings
    authority <- asksSite $ renderAuthority . siteInstanceHost
    repos <- selectKeysList [RepoVcs ==. VCSGit] []
    for_ repos $ \ repoID -> do
        repoHash <- encodeKeyHashid repoID
        path <- askRepoDir repoHash
        liftIO $ writeHookFile path hook authority (keyHashidText repoHash)

-- | Given a temporary directory to use freely for this operation, generate
-- patches from the difference between the origin branch and the target branch
-- (origin branch must be an ancestor of target branch)
--
-- Target repo must be local, origin repo may be remote on the network
generateGitPatches
    :: FilePath -- ^ Absolute path to target repo
    -> String   -- ^ Target branch
    -> String   -- ^ Absolute path or HTTP URI of origin repo
    -> String   -- ^ Origin branch
    -> FilePath -- ^ Temporary directory to use for the operation
    -> ExceptT Text IO (NonEmpty Text)
generateGitPatches targetRepoPath targetBranch originRepoURI originBranch tempDir = do
    runProcessE "git clone" $ proc "git" ["clone", "--bare", "--verbose", "--single-branch", "--branch", targetBranch, "--", targetRepoPath, tempDir]
    runProcessE "git remote add" $ proc "git" ["-C", tempDir, "remote", "--verbose", "add", "-t", originBranch, "real-origin", originRepoURI]
    runProcessE "git fetch" $ proc "git" ["-C", tempDir, "fetch", "real-origin", originBranch]
    runProcessE "git merge-base --is-ancestor" $ proc "git" ["-C", tempDir, "merge-base", "--is-ancestor", targetBranch, "real-origin/" ++ originBranch]
    patchFileNames <- do
        names <- T.lines <$> readProcessE "git format-patch" (proc "git" ["-C", tempDir, "format-patch", targetBranch ++ "..real-origin/" ++ originBranch])
        fromMaybeE (NE.nonEmpty names) "No new patches found in origin branch"
    for patchFileNames $ \ name -> do
        b <- lift $ B.readFile $ tempDir </> T.unpack name
        case TE.decodeUtf8' b of
            Left e -> throwE $ T.concat
                [ "UTF-8 decoding error while reading Git patch file "
                , name, ": " , T.pack $ displayException e
                ]
            Right t -> return t

canApplyGitPatches repoPath branch patches tempDir = do
    runProcessE "git clone" $ proc "git" ["clone", "--verbose", "--single-branch", "--branch", branch, "--", repoPath, tempDir]
    runProcessE "git config" $ proc "git" ["-C", tempDir, "config", "user.name", "vervis"]
    runProcessE "git config" $ proc "git" ["-C", tempDir, "config", "user.email", "vervis@vervis.vervis"]
    let input = BL.concat $ NE.toList $ NE.map (BL.fromStrict . TE.encodeUtf8) patches
    exitCode <- lift $ runProcess $ setStdin (byteStringInput input) $ proc "git" ["-C", tempDir, "am"]
    return $ exitCode == ExitSuccess

-- Since 'git am' doesn't work on a bare repo, clone target repo into the given
-- temporary directory, apply there, and finally push
applyGitPatches repoPath branch patches tempDir = do
    runProcessE "git clone" $ proc "git" ["clone", "--verbose", "--single-branch", "--branch", branch, "--", repoPath, tempDir]
    runProcessE "git config" $ proc "git" ["-C", tempDir, "config", "user.name", "vervis"]
    runProcessE "git config" $ proc "git" ["-C", tempDir, "config", "user.email", "vervis@vervis.vervis"]
    let input = BL.concat $ NE.toList $ NE.map (BL.fromStrict . TE.encodeUtf8) patches
    runProcessE "git am" $ setStdin (byteStringInput input) $ proc "git" ["-C", tempDir, "am"]
    runProcessE "git push" $ proc "git" ["-C", tempDir, "push"]
