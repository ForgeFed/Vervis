{- This file is part of Vervis.
 -
 - Written in 2016, 2019, 2024 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Field.Person
    ( passField
    , fedUriField
    , capField
    , factoryField
    )
where

import Control.Monad.Trans.Except
import Data.Bitraversable
import Data.Char (isDigit)
import Data.Maybe
import Data.Text (Text)
import Data.Traversable
import Database.Persist
import Yesod.Core
import Yesod.Form.Fields
import Yesod.Form.Functions
import Yesod.Form.Types
import Yesod.Persist.Core

import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Network.FedURI
import Yesod.FedURI
import Yesod.Hashids

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Char.Local (isAsciiLetter)
import Database.Persist.Local

import Vervis.Actor
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.FedURI
import Vervis.Foundation
import Vervis.Model
import Vervis.Model.Ident (text2shr)
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Recipient
import Vervis.Settings

import qualified Vervis.Recipient as VR

checkPassLength :: Field Handler Text -> Field Handler Text
checkPassLength =
    let msg :: Text
        msg =
            "The password must be at least 8 characters long. Yes, I know, \
            \having so many different passwords for many different sites is \
            \annoying and cumbersome. I'm trying to figure out an \
            \alternative, such as a client TLS certificate, that can work \
            \somewhat like SSH and GPG keys."
        minlen = 8
    in  checkBool ((>= minlen) . T.length) msg

passConfirmField :: Field Handler Text
passConfirmField = Field
    { fieldParse = \ vals _files ->
        return $ case vals of
            [a, b] ->
                if a == b
                    then Right $ Just a
                    else Left "Passwords don’t match"
            [] -> Right Nothing
            _ -> Left "You must enter the password twice"
    , fieldView = \ idAttr nameAttr otherAttrs _eResult _isReq ->
        $(widgetFile "password-field")
    , fieldEnctype = UrlEncoded
    }

passField :: Field Handler Text
passField = checkPassLength passConfirmField

fedUriField
    :: (Monad m, RenderMessage (HandlerSite m) FormMessage) => Field m FedURI
fedUriField = Field
    { fieldParse = parseHelper $ \ t ->
        case parseObjURI t of
            Left e  -> Left $ MsgInvalidUrl $ T.pack e <> ": " <> t
            Right u -> Right u
    , fieldView = \theId name attrs val isReq ->
        [whamlet|<input ##{theId} name=#{name} *{attrs} type=url :isReq:required value=#{either id renderObjURI val}>|]
    , fieldEnctype = UrlEncoded
    }

capField
    :: Field Handler
        ( FedURI
        , Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
        )
capField = checkMMap toCap fst fedUriField
    where
    toCap u =
        runExceptT $ (u,) <$> nameExceptT "Capability URI" (parseActivityURI u)

factoryField personID = selectField $ do
    rows <- runDB $ do
        resourceIDs <- map (factoryResource . entityVal) <$> selectList [] []
        actorIDs <- selectKeysList [RemoteActorType ==. AP.ActorTypeFactory] []
        permits <- getPermitsForResources personID (Just resourceIDs) (Just actorIDs) AP.RoleWrite
        for permits $ \ (resource, grant, _, _) -> do
            factory <-
                bitraverse
                    (\ resourceID -> do
                        Resource actorID <- getJust resourceID
                        actor <- getJust actorID
                        factoryID <- getKeyByJust $ UniqueFactory resourceID
                        return (factoryID, actorName actor)
                    )
                    (\ actorID -> do
                        actor <- getJust actorID
                        uActor <- getRemoteActorURI actor
                        return (uActor, remoteActorName actor)
                    )
                    resource
            return (factory, grant)

    hashActor <- VR.getHashLocalActor
    hashFactory <- getEncodeKeyHashid
    hashItem <- getEncodeKeyHashid
    encodeRouteHome <- getEncodeRouteHome
    let renderGrant = \case
            Left (byKey, grantID) ->
                encodeRouteHome $ activityRoute (hashActor byKey) (hashItem grantID)
            Right uGrant ->
                uGrant
    optionsPairs $
        map (\ (factory, grant) ->
            case factory of
                Left (factoryID, name) ->
                    ( T.concat
                        [ "*", keyHashidText $ hashFactory factoryID
                        , " ", name
                        ]
                    , ( encodeRouteHome $ FactoryR $ hashFactory factoryID
                      , renderGrant grant
                      )
                    )
                Right (uActor@(ObjURI h lu), mname) ->
                    ( T.concat
                        [ renderObjURI $ ObjURI h lu
                        , " "
                        , fromMaybe "(?)" mname
                        ]
                    , ( uActor
                      , renderGrant grant
                      )
                    )
            )
        rows
