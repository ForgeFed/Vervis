{- This file is part of Vervis.
 -
 - Written in 2019, 2022, 2024 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE DeriveGeneric #-}

module Vervis.Hook
    ( HookSecret ()
    , hookSecretText
    , Config (..)
    , Author (..)
    , Commit (..)
    , Push (..)
    , writeHookConfig
    , postReceive
    , postApply
    )
where

import Control.Applicative
import Control.Exception
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Class
import Crypto.Random
import Data.Aeson
import Data.Bifunctor
import Data.ByteString (ByteString)
import Data.Char
import Data.Graph.Inductive.Graph
import Data.Int
import Data.Maybe
import Data.List.NonEmpty (NonEmpty, nonEmpty)
import Data.Text (Text)
import Data.Time.Clock
import Data.Time.Clock.POSIX
import Data.Time.Format
import Data.Word
import GHC.Generics
import Network.HTTP.Client
import Network.HTTP.Types.Header
import System.Directory
import System.Environment
import System.Exit
import System.FilePath
import System.IO
import Text.Email.Aeson.Instances ()
import Text.Email.Validate
import Text.Read (readMaybe)
import Text.XML.Light
import Yesod.Core.Content

import qualified Control.Monad.Catch as MC
import qualified Data.Attoparsec.Text as A
import qualified Data.ByteString as B
import qualified Data.ByteString.Base16 as B16
import qualified Data.List.NonEmpty as NE
import qualified Data.Set as S
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import qualified Data.Text.IO as TIO

import Data.KeyFile
import Data.ObjId
import Data.VersionControl
import Development.Darcs
import Development.Git
import Network.FedURI

import Control.Monad.Trans.Except.Local
--import Data.DList.Local
import Data.List.NonEmpty.Local

data HookSecret = HookSecret ByteString

instance KeyFile HookSecret where
    generateKey = HookSecret <$> getRandomBytes 32
    parseKey b =
        if B.length b == 32
            then return $ HookSecret b
            else error "HookSecret invalid length"
    renderKey (HookSecret b) = b

hookSecretText :: HookSecret -> Text
hookSecretText (HookSecret b) = TE.decodeUtf8 $ B16.encode b

data Config = Config
    { configSecret     :: Text
    , configPort       :: Word16
    , configMaxCommits :: Int
    }
    deriving Generic

instance FromJSON Config

instance ToJSON Config

data Push = Push
    { pushSecret :: Text
    , pushUser   :: Int64
    , pushRepo   :: Text
    , pushBranch :: Maybe Text
    , pushBefore :: Maybe Text
    , pushAfter  :: Maybe Text
    , pushInit   :: NonEmpty Commit
    , pushLast   :: Maybe (Int, NonEmpty Commit)
    }
    deriving Generic

instance FromJSON Push

instance ToJSON Push

getVervisCachePath :: String -> IO FilePath
getVervisCachePath host = (</> host) <$> getXdgDirectory XdgCache "vervis"

hookConfigFileName :: String
hookConfigFileName = "hook-config.json"

writeHookConfig :: String -> Config -> IO ()
writeHookConfig host config = do
    cachePath <- getVervisCachePath host
    createDirectoryIfMissing True cachePath
    encodeFile (cachePath </> hookConfigFileName) config

splitCommits
    :: Monad m
    => Config
    -> NonEmpty a
    -> ExceptT Text m (NonEmpty a, Maybe (Int, NonEmpty a))
splitCommits config commits =
    if length commits <= maxCommits
        then return (commits, Nothing)
        else do
            let half = maxCommits `div` 2
                middle = length commits - 2 * half
                (e, r) = NE.splitAt half commits
                l = drop middle r
            eNE <- nonEmptyE e "early is empty"
            lNE <- nonEmptyE l "late is empty"
            return (eNE, Just (middle, lNE))
    where
    maxCommits = configMaxCommits config

sendPush :: (MonadIO m, MC.MonadThrow m) => Config -> Manager -> Push -> ExceptT Text m (Response ())
sendPush config manager push = do
    let uri :: ObjURI Dev
        uri =
            ObjURI
                (Authority "localhost" (Just $ configPort config))
                (LocalURI "/post-receive")
    req <- requestFromURI $ uriFromObjURI uri
    let req' =
            setRequestCheckStatus $
            consHeader hContentType typeJson $
            req { method      = "POST"
                , requestBody = RequestBodyLBS $ encode push
                }
    ExceptT $ liftIO $ first adaptErr <$> try (httpNoBody req' manager)
    where
    adaptErr :: HttpException -> Text
    adaptErr = T.pack . displayException
    consHeader n b r = r { requestHeaders = (n, b) : requestHeaders r }

data ChangeType = Create ObjId | Delete ObjId | Update ObjId ObjId

data Situation = UnannotatedTag Text | AnnotatedTag Text | Branch Text | TrackingBranch Text

reportNewCommits :: Config -> Text -> IO ()
reportNewCommits config repo = do
    user <- read <$> getEnv "VERVIS_SSH_USER"
    manager <- newManager defaultManagerSettings
    withGitRepo "." $ loop user manager
    where
    -- Written while looking at contrib/hooks/post-receive-email in Git source repo
    loop user manager = do
        eof <- liftIO isEOF
        unless eof $ do
            result <- runExceptT $ do
                line <- liftIO TIO.getLine
                (old, new, refname) <-
                    case T.words line of
                        [o, n, r] -> lift $ do
                            o' <- gitGetObjectHash o
                            n' <- gitGetObjectHash n
                            return (o', n', r)
                        _ -> throwE $ "Weird line: " <> line
                let change =
                        if isZeroObj old
                            then Create new
                        else if isZeroObj new
                            then Delete old
                        else Update old new
                    rev =
                        case change of
                            Create h -> h
                            Delete h -> h
                            Update o n -> n
                revType <- lift $ gitGetRevType $ renderObjId $ rev
                situation <-
                    case (T.stripPrefix "refs/tags/" refname, T.stripPrefix "refs/heads/" refname, T.stripPrefix "refs/remotes/" refname, revType) of
                        (Just tag, _, _, RTCommit) -> pure $ UnannotatedTag tag
                        (Just tag, _, _, RTTag) -> pure $ AnnotatedTag tag
                        (_, Just branch, _, RTCommit) -> pure $ Branch branch
                        (_, _, Just branch, RTCommit) -> pure $ TrackingBranch branch
                        _ -> throwE $ "Unknown type of update to " <> refname <> " (" <> T.pack (show revType) <> ")"

                branch <-
                    case situation of
                        Branch b -> pure b
                        _ -> throwE "Non-branch situation, not supported yet"

                refSpec <-
                    case change of
                        Create h -> pure $ renderObjId h
                        Delete _ -> throwE "Branch deletion, not supported yet"
                        Update o n -> pure $ renderObjId o <> ".." <> renderObjId n
                otherBranchHashes <- lift $ do
                    otherBranches <- S.delete branch <$> gitListBranches
                    gitGetObjectHashes $ map ("refs/heads/" <>) $ S.toList otherBranches
                commits <- lift $ gitGetCommitInfos refSpec otherBranchHashes Nothing Nothing
                commits' <-
                    case NE.nonEmpty commits of
                        Nothing -> throwE "No commits"
                        Just ne -> pure ne

                (early, late) <- splitCommits config commits'
                let push = Push
                        { pushSecret = configSecret config
                        , pushUser   = user
                        , pushRepo   = repo
                        , pushBranch = Just branch
                        , pushBefore =
                            renderObjId <$>
                            case change of
                                Create _ -> Nothing
                                Delete h -> Just h
                                Update h _ -> Just h
                        , pushAfter  = Just $ renderObjId new
                        , pushInit   = early
                        , pushLast   = late
                        }
                sendPush config manager push
            case result of
                Left e -> lift $ TIO.hPutStrLn stderr $ "HOOK ERROR: " <> e
                Right _resp -> return ()
            loop user manager
        where
        isZeroObj (ObjId b) = B.all (== 0) b

postReceive :: IO ()
postReceive = do
    (host, repo) <- do
        args <- getArgs
        case args of
            [h, r] -> return (h, T.pack r)
            _ -> die "Unexpected number of arguments"
    cachePath <- getVervisCachePath host
    config <- do
        mc <- decodeFileStrict' $ cachePath </> hookConfigFileName
        case mc of
            Nothing -> die "Parsing hook config failed"
            Just c -> return c
    reportNewCommits config repo

reportNewPatches :: Config -> Text -> IO ()
reportNewPatches config repo = do
    user <- read <$> getEnv "VERVIS_SSH_USER"
    manager <- newManager defaultManagerSettings
    melem <- parseXMLDoc <$> getEnv "DARCS_PATCHES_XML"
    result <- runExceptT $ do
        push <- ExceptT . pure . runExcept $ do
            elem <- fromMaybeE melem "parseXMLDoc failed"
            children <- nonEmptyE (elChildren elem) "No patches"
            patches <- traverse xml2patch children
            (early, late) <- splitCommits config patches
            return Push
                { pushSecret = configSecret config
                , pushUser   = user
                , pushRepo   = repo
                , pushBranch = Nothing
                , pushBefore = Nothing
                , pushAfter  = Nothing
                , pushInit   = early
                , pushLast   = late
                }
        sendPush config manager push
    case result of
        Left e -> dieT $ "Post-apply hook error: " <> e
        Right _resp -> return ()
    where
    dieT err = TIO.hPutStrLn stderr err >> exitFailure

postApply :: IO ()
postApply = do
    (host, repo) <- do
        args <- getArgs
        case args of
            [h, r] -> return (h, T.pack r)
            _ -> die "Unexpected number of arguments"
    cachePath <- getVervisCachePath host
    config <- do
        mc <- decodeFileStrict' $ cachePath </> hookConfigFileName
        case mc of
            Nothing -> die "Parsing hook config failed"
            Just c -> return c
    reportNewPatches config repo
