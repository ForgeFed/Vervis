{- This file is part of Vervis.
 -
 - Written in 2019, 2020, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Web.Actor
    ( getInbox
    , getInbox'
    , getInbox''
    , postInbox
    , getOutbox
    , getOutbox'
    , getOutboxItem
    , getOutboxItem'
    , getFollowersCollection
    , getActorFollowersCollection
    , getActorFollowersCollection'
    , getFollowingCollection
    , handleRobotInbox
    , serveInstanceKey
    , servePerActorKey
    , servePerActorKey''
    , serveForks
    )
where

import Control.Applicative ((<|>))
import Control.Concurrent.STM.TVar (readTVarIO, modifyTVar', TVar)
import Control.Exception hiding (Handler)
import Control.Monad
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Logger.CallStack
import Control.Monad.STM (atomically)
import Control.Monad.Trans.Except
import Control.Monad.Trans.Reader
import Data.Aeson hiding (Key)
import Data.Aeson.Encode.Pretty
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Foldable (for_)
import Data.Hashable
import Data.HList (HList (..))
import Data.List
import Data.Maybe
import Data.Proxy
import Data.Text (Text)
import Data.Text.Lazy.Encoding (decodeUtf8)
import Data.Time.Clock
import Data.Time.Interval (TimeInterval, toTimeUnit)
import Data.Time.Units (Second)
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Fcf
import Network.HTTP.Types.Status
import Optics.Core
import Text.Blaze.Html (Html, preEscapedToHtml)
import Text.Blaze.Html.Renderer.Text
import Text.HTML.SanitizeXSS
import Text.Shakespeare.I18N (RenderMessage)
import Yesod.Core hiding (logDebug)
import Yesod.Core.Handler
import Yesod.Form.Fields
import Yesod.Form.Functions
import Yesod.Form.Types
import Yesod.Persist.Core

import qualified Data.Aeson.KeyMap as AM
import qualified Data.ByteString.Char8 as BC (unpack)
import qualified Data.ByteString.Lazy as BL
import qualified Data.HashMap.Strict as M
import qualified Data.HList as H
import qualified Data.Text as T
import qualified Data.Text.Lazy as TL
import qualified Data.Vector as V
import qualified Database.Esqueleto as E

import Control.Concurrent.Actor hiding (Actor)
import Database.Persist.JSON
import Network.FedURI
import Yesod.ActivityPub
import Yesod.Auth.Unverified
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite
import Yesod.RenderSource

import qualified Control.Concurrent.Actor as CCA
import qualified Crypto.ActorKey as AK
import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Aeson.Local
import Data.Either.Local
import Data.EventTime.Local
import Data.Paginate.Local
import Data.Time.Clock.Local
import Database.Persist.Local
import Yesod.Persist.Local

import qualified Data.Aeson.Encode.Pretty.ToEncoding as P
import qualified Web.ActivityPub as AP

import Vervis.Actor (RemoteAuthor (..), ActivityBody (..), Verse (..))
import Vervis.Actor2
import Vervis.ActivityPub
import Vervis.API
import Vervis.Data.Actor
import Vervis.Data.Ticket
import Vervis.FedURI
import Vervis.Federation.Auth
import Vervis.Foundation
import Vervis.Model hiding (Ticket)
import Vervis.Model.Ident
import Vervis.Paginate
import Vervis.Persist.Actor
import Vervis.Recipient
import Vervis.Settings
import Vervis.Ticket

getShowTime = showTime <$> liftIO getCurrentTime
    where
    showTime now =
        showEventTime .
        intervalToEventTime .
        FriendlyConvert .
        diffUTCTime now

objectSummary o =
    case AM.lookup "summary" o of
        Just (String t) | not (T.null t) -> Just t
        _ -> Nothing

objectId o =
    case AM.lookup "id" o <|> AM.lookup "@id" o of
        Just (String t) | not (T.null t) -> t
        _ ->
            error $
                "'id' field not found" ++
                TL.unpack (P.encodePrettyToLazyText o)

getInbox = getInbox' actorInbox

getInbox' grabInbox here actor hash =
    getInbox'' grabInbox here (pure . actor) hash

getInbox'' grabInbox here getActorID hash = do
    key <- decodeKeyHashid404 hash
    (total, pages, mpage) <- runDB $ do
        inboxID <- do
            rec <- get404 key
            actorID <- getActorID rec
            grabInbox <$> getJust actorID
        getPageAndNavCount
            (countItems inboxID)
            (\ off lim -> map adaptItem <$> getItems inboxID off lim)

    encodeRouteLocal <- getEncodeRouteLocal
    encodeRoutePageLocal <- getEncodeRoutePageLocal
    let here' = here hash
        pageUrl = encodeRoutePageLocal here'
    host <- getsYesod $ appInstanceHost . appSettings
    selectRep $
        case mpage of
            Nothing -> do
                AP.provideAP $ pure $ AP.Doc host $ AP.Collection
                    { AP.collectionId         = encodeRouteLocal here'
                    , AP.collectionType       = AP.CollectionTypeOrdered
                    , AP.collectionTotalItems = Just total
                    , AP.collectionCurrent    = Nothing
                    , AP.collectionFirst      = Just $ pageUrl 1
                    , AP.collectionLast       = Just $ pageUrl pages
                    , AP.collectionItems      = [] :: [Text]
                    , AP.collectionContext    = Nothing
                    }
                provideRep (redirectFirstPage here' :: Handler Html)
            Just (items, navModel) -> do
                let current = nmCurrent navModel
                AP.provideAP $ pure $ AP.Doc host $ AP.CollectionPage
                    { AP.collectionPageId         = pageUrl current
                    , AP.collectionPageType       = AP.CollectionPageTypeOrdered
                    , AP.collectionPageTotalItems = Nothing
                    , AP.collectionPageCurrent    = Just $ pageUrl current
                    , AP.collectionPageFirst      = Just $ pageUrl 1
                    , AP.collectionPageLast       = Just $ pageUrl pages
                    , AP.collectionPagePartOf     = encodeRouteLocal here'
                    , AP.collectionPagePrev       =
                        if current > 1
                            then Just $ pageUrl $ current - 1
                            else Nothing
                    , AP.collectionPageNext       =
                        if current < pages
                            then Just $ pageUrl $ current + 1
                            else Nothing
                    , AP.collectionPageStartIndex = Nothing
                    , AP.collectionPageItems      = map (view _1) items
                    }
                provideRep $ do
                    let pageNav = navWidget navModel
                    showTime <- getShowTime
                    defaultLayout $(widgetFile "person/inbox")
    where
    countItems ibid =
        (+) <$> count [InboxItemLocalInbox ==. ibid]
            <*> count [InboxItemRemoteInbox ==. ibid]
    getItems ibid off lim =
        E.select $ E.from $
            \ (ib `E.LeftOuterJoin` (ibl `E.InnerJoin` ob) `E.LeftOuterJoin` (ibr `E.InnerJoin` ract)) -> do
                E.on $ ibr E.?. InboxItemRemoteActivity E.==. ract E.?. RemoteActivityId
                E.on $ E.just (ib E.^. InboxItemId) E.==. ibr E.?. InboxItemRemoteItem
                E.on $ ibl E.?. InboxItemLocalActivity E.==. ob E.?. OutboxItemId
                E.on $ E.just (ib E.^. InboxItemId) E.==. ibl E.?. InboxItemLocalItem
                E.where_
                    $ ( E.isNothing (ibr E.?. InboxItemRemoteInbox) E.||.
                        ibr E.?. InboxItemRemoteInbox E.==. E.just (E.val ibid)
                      )
                    E.&&.
                      ( E.isNothing (ibl E.?. InboxItemLocalInbox) E.||.
                        ibl E.?. InboxItemLocalInbox E.==. E.just (E.val ibid)
                      )
                E.orderBy [E.desc $ ib E.^. InboxItemId]
                E.offset $ fromIntegral off
                E.limit $ fromIntegral lim
                return
                    ( ib E.^. InboxItemId
                    , ob E.?. OutboxItemActivity
                    , ob E.?. OutboxItemPublished
                    , ract E.?. RemoteActivityContent
                    , ract E.?. RemoteActivityReceived
                    , ib E.^. InboxItemResult
                    )
    adaptItem
        (E.Value ibid, E.Value mact, E.Value mpub, E.Value mobj, E.Value mrec, E.Value result) =
            case (mact, mpub, mobj, mrec) of
                (Nothing, Nothing, Nothing, Nothing) ->
                    error $ ibiidString ++ " neither local nor remote"
                (Just _, Just _, Just _, Just _) ->
                    error $ ibiidString ++ " both local and remote"
                (Just act, Just pub, Nothing, Nothing) ->
                    (persistJSONObject act, (pub, False), result)
                (Nothing, Nothing, Just obj, Just rec) ->
                    (persistJSONObject obj, (rec, True), result)
                _ -> error $ "Unexpected query result for " ++ ibiidString
        where
        ibiidString = "InboxItem #" ++ show (fromSqlKey ibid)

postInbox
    :: forall a b l b0 l0 .
       ( CCA.Actor a
       , ActorLaunch a
       , ActorHasMethod a "verse" (Verse :-> Return (Either Text Text))
       , ActorIdentity a ~ Key a
       , Eq (Key a)
       , Hashable (Key a)
       , H.HEq
                                  (TVar (M.HashMap (Key a) (Ref a)))
                                  (TVar (M.HashMap (Key Person) (Ref Person)))
                                  b
       , H.HOccurrence'
                          b
                          (TVar (M.HashMap (Key a) (Ref a)))
                          [TVar (M.HashMap (Key Person) (Ref Person)),
                           TVar (M.HashMap (Key Project) (Ref Project)),
                           TVar (M.HashMap (Key Group) (Ref Group)),
                           TVar (M.HashMap (Key Deck) (Ref Deck)),
                           TVar (M.HashMap (Key Loom) (Ref Loom)),
                           TVar (M.HashMap (Key Repo) (Ref Repo)),
                           TVar (M.HashMap (Key Factory) (Ref Factory))]
                          l
       , H.HOccurs'
                          (TVar (M.HashMap (Key a) (Ref a)))
                          l
                          [TVar (M.HashMap (Key Person) (Ref Person)),
                           TVar (M.HashMap (Key Project) (Ref Project)),
                           TVar (M.HashMap (Key Group) (Ref Group)),
                           TVar (M.HashMap (Key Deck) (Ref Deck)),
                           TVar (M.HashMap (Key Loom) (Ref Loom)),
                           TVar (M.HashMap (Key Repo) (Ref Repo)),
                           TVar (M.HashMap (Key Factory) (Ref Factory))]
       , H.HEq
                                  (TVar (ActorRefMap a)) (TVar (ActorRefMap Person)) b0
       , H.HOccurrence'
                          b0
                          (TVar (ActorRefMap a))
                          [TVar (ActorRefMap Person), TVar (ActorRefMap Project),
                           TVar (ActorRefMap Group), TVar (ActorRefMap Deck),
                           TVar (ActorRefMap Loom), TVar (ActorRefMap Repo),
                           TVar (ActorRefMap Factory)]
                          l0
       , H.HOccurs'
                          (TVar (ActorRefMap a))
                          l0
                          [TVar (ActorRefMap Person), TVar (ActorRefMap Project),
                           TVar (ActorRefMap Group), TVar (ActorRefMap Deck),
                           TVar (ActorRefMap Loom), TVar (ActorRefMap Repo),
                           TVar (ActorRefMap Factory)]
       )
    => (Key a -> LocalActorBy Key) -> Key a -> Handler ()
postInbox toLA recipID = do
    let recipByKey = toLA recipID
    federation <- getsYesod $ appFederation . appSettings
    unless federation badMethod
    contentTypes <- lookupHeaders "Content-Type"
    now <- liftIO getCurrentTime
    result <- runExceptT $ do
        (auth, body) <- authenticateActivity now
        authorIdMsig <-
            case auth of
                ActivityAuthLocal authorByKey -> Left <$> do
                    outboxItemID <-
                        parseAuthenticatedLocalActivityURI
                            authorByKey
                            (AP.activityId $ actbActivity body)
                    actorID <- runDBExcept $ do
                        ment <- lift $ getLocalActorEntity authorByKey
                        case ment of
                            Nothing -> throwE "Author not found in DB"
                            Just ent -> lift $ grabLocalActorID ent
                    return (authorByKey, actorID, outboxItemID)
                ActivityAuthRemote author -> Right <$> do
                    luActivity <-
                        fromMaybeE (AP.activityId $ actbActivity body) "Activity without 'id'"
                    recipByHash <- hashLocalActor recipByKey
                    msig <- checkForwarding recipByHash
                    return (author, luActivity, msig)
        ref <- lift $ do
            tvar <- H.hOccurs <$> getsYesod appActors
            actors <- liftIO $ readTVarIO tvar
            case M.lookup recipID actors of
                Nothing -> notFound
                Just (ref :: Ref a) -> pure ref
        theater <- getsYesod appTheater
        r <- liftIO $ callIO' @"verse" @a theater Proxy ref $ Verse authorIdMsig body `HCons` HNil
        case r of
            Nothing -> notFound
            Just (Left e) -> throwE e
            Just (Right t) -> return (actbObject body, t)
    recordActivity now result contentTypes
    case result of
        Left err -> do
            logDebug err
            sendResponseStatus badRequest400 err
        Right _ -> return ()
    where
    recordActivity :: UTCTime -> Either Text (Object, Text) -> [ContentType] -> Handler ()
    recordActivity now result contentTypes = do
        let (msg, body) =
                case result of
                    Left t -> (t, "{?}")
                    Right (o, t) -> (t, BL.toStrict $ encodePretty o)
            item = Report now msg contentTypes body
        runDB $ insert_ item
    parseAuthenticatedLocalActivityURI
        :: (MonadSite m, YesodHashids (SiteEnv m))
        => LocalActorBy Key -> Maybe LocalURI -> ExceptT Text m OutboxItemId
    parseAuthenticatedLocalActivityURI author maybeActivityURI = do
        luAct <- fromMaybeE maybeActivityURI "No 'id'"
        (actorByKey, _, outboxItemID) <- parseLocalActivityURI luAct
        unless (actorByKey == author) $
            throwE "'actor' actor and 'id' actor mismatch"
        return outboxItemID

getOutbox here itemRoute grabActorID hash =
    getOutbox' here itemRoute (pure . grabActorID) hash

getOutbox' here itemRoute grabActorID hash = do
    key <- decodeKeyHashid404 hash
    (total, pages, mpage) <- runDB $ do
        outboxID <- do
            actorID <- grabActorID =<< get404 key
            actorOutbox <$> getJust actorID
        let countAllItems = count [OutboxItemOutbox ==. outboxID]
            selectItems off lim = selectList [OutboxItemOutbox ==. outboxID] [Desc OutboxItemId, OffsetBy off, LimitTo lim]
        getPageAndNavCount countAllItems selectItems

    encodeRouteLocal <- getEncodeRouteLocal
    encodeRoutePageLocal <- getEncodeRoutePageLocal
    let here' = here hash
        pageUrl = encodeRoutePageLocal here'
    host <- getsYesod $ appInstanceHost . appSettings
    selectRep $
        case mpage of
            Nothing -> do
                AP.provideAP $ pure $ AP.Doc host $ AP.Collection
                    { AP.collectionId         = encodeRouteLocal here'
                    , AP.collectionType       = AP.CollectionTypeOrdered
                    , AP.collectionTotalItems = Just total
                    , AP.collectionCurrent    = Nothing
                    , AP.collectionFirst      = Just $ pageUrl 1
                    , AP.collectionLast       = Just $ pageUrl pages
                    , AP.collectionItems      = [] :: [Text]
                    , AP.collectionContext    = Nothing
                    }
                provideRep (redirectFirstPage here' :: Handler Html)
            Just (items, navModel) -> do
                let current = nmCurrent navModel
                AP.provideAP $ pure $ AP.Doc host $ AP.CollectionPage
                    { AP.collectionPageId         = pageUrl current
                    , AP.collectionPageType       = AP.CollectionPageTypeOrdered
                    , AP.collectionPageTotalItems = Nothing
                    , AP.collectionPageCurrent    = Just $ pageUrl current
                    , AP.collectionPageFirst      = Just $ pageUrl 1
                    , AP.collectionPageLast       = Just $ pageUrl pages
                    , AP.collectionPagePartOf     = encodeRouteLocal here'
                    , AP.collectionPagePrev       =
                        if current > 1
                            then Just $ pageUrl $ current - 1
                            else Nothing
                    , AP.collectionPageNext       =
                        if current < pages
                            then Just $ pageUrl $ current + 1
                            else Nothing
                    , AP.collectionPageStartIndex = Nothing
                    , AP.collectionPageItems      = map (persistJSONObject . outboxItemActivity . entityVal) items
                    }
                provideRep $ do
                    let pageNav = navWidget navModel
                    showTime <- getShowTime
                    hashItem <- getEncodeKeyHashid
                    defaultLayout $(widgetFile "person/outbox")

getOutboxItem here actor topicHash itemHash =
    getOutboxItem' here (pure . actor) topicHash itemHash

getOutboxItem' here actor topicHash itemHash = do
    topicID <- decodeKeyHashid404 topicHash
    itemID <- decodeKeyHashid404 itemHash
    body <- runDB $ do
        outboxID <- do
            actorID <- actor =<< get404 topicID
            actorOutbox <$> getJust actorID
        item <- get404 itemID
        unless (outboxItemOutbox item == outboxID) notFound
        return $ outboxItemActivity item
    let here' = here topicHash itemHash
    provideHtmlAndAP'' body $ redirectToPrettyJSON here'

getLocalActors
    :: [ActorId] -> ReaderT SqlBackend Handler [LocalActorBy Key]
getLocalActors actorIDs = do
    resourceIDs <- selectKeysList [ResourceActor <-. actorIDs] []
    localActors <-
        concat <$> sequenceA
            [ map LocalActorPerson <$>
                selectKeysList [PersonActor <-. actorIDs] []
            , map LocalActorGroup <$>
                selectKeysList [GroupActor <-. actorIDs] []
            , map LocalActorRepo <$>
                selectKeysList [RepoActor <-. actorIDs] []
            , map LocalActorDeck <$>
                selectKeysList [DeckActor <-. actorIDs] []
            , map LocalActorLoom <$>
                selectKeysList [LoomActor <-. actorIDs] []
            , map LocalActorProject <$>
                selectKeysList [ProjectActor <-. actorIDs] []
            , map LocalActorFactory <$>
                selectKeysList [FactoryResource <-. resourceIDs] []
            ]
    case compare (length localActors) (length actorIDs) of
        LT -> error "Found actor ID not used by any specific actor"
        GT -> error "Found actor ID used by multiple specific actors"
        EQ -> return localActors

getFollowersCollection
    :: Route App -> AppDB FollowerSetId -> Handler TypedContent
getFollowersCollection here getFsid = do
    (locals, remotes, l, r) <- runDB $ do
        fsid <- getFsid
        (,,,) <$> do actorIDs <-
                        map (followActor . entityVal) <$>
                            selectList
                                [FollowTarget ==. fsid, FollowPublic ==. True]
                                []
                     getLocalActors actorIDs
              <*> do E.select $ E.from $ \ (rf `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
                        E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
                        E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
                        E.on $ rf E.^. RemoteFollowActor E.==. ra E.^. RemoteActorId
                        E.where_
                            $     rf E.^. RemoteFollowTarget E.==. E.val fsid
                            E.&&. rf E.^. RemoteFollowPublic E.==. E.val True
                        return
                            ( i E.^. InstanceHost
                            , ro E.^. RemoteObjectIdent
                            )
              <*> count [FollowTarget ==. fsid]
              <*> count [RemoteFollowTarget ==. fsid]

    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    hashActor <- getHashLocalActor
    let followersAP = AP.Collection
            { AP.collectionId         = encodeRouteLocal here
            , AP.collectionType       = AP.CollectionTypeUnordered
            , AP.collectionTotalItems = Just $ l + r
            , AP.collectionCurrent    = Nothing
            , AP.collectionFirst      = Nothing
            , AP.collectionLast       = Nothing
            , AP.collectionItems      =
                map (encodeRouteHome . renderLocalActor . hashActor) locals ++
                map (uncurry ObjURI . bimap E.unValue E.unValue) remotes
            , AP.collectionContext    = Nothing
            }
    provideHtmlAndAP followersAP $ redirectToPrettyJSON here

getActorFollowersCollection here actor hash =
    getActorFollowersCollection' here (pure . actor) hash

getActorFollowersCollection' here actor hash = do
    key <- decodeKeyHashid404 hash
    getFollowersCollection (here hash) (getFsid key)
    where
    getFsid key = do
        actorID <- actor =<< get404 key
        actorFollowers <$> getJust actorID

getFollowingCollection here actor hash = do
    key <- decodeKeyHashid404 hash
    (localTotal, localActors, workItems, remotes) <- runDB $ do
        followerActorID <- actor <$> get404 key
        followerSetIDs <-
            map (followTarget . entityVal) <$>
                selectList [FollowActor ==. followerActorID] []
        actorIDs <- selectKeysList [ActorFollowers <-. followerSetIDs] []
        ticketIDs <- selectKeysList [TicketFollowers <-. followerSetIDs] []
        (,,,) (length followerSetIDs)
            <$> getLocalActors actorIDs
            <*> ((++) <$> getTickets ticketIDs <*> getCloths ticketIDs)
            <*> getRemotes followerActorID

    hashActor <- getHashLocalActor
    hashItem <- runAct getHashWorkItem
    let locals =
            map (renderLocalActor . hashActor) localActors ++
            map (workItemRoute . hashItem) workItems
    unless (length locals == localTotal) $
        error "Bug! List length mismatch"

    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    let here' = here hash
        followingAP = AP.Collection
            { AP.collectionId         = encodeRouteLocal here'
            , AP.collectionType       = AP.CollectionTypeUnordered
            , AP.collectionTotalItems = Just $ localTotal + length remotes
            , AP.collectionCurrent    = Nothing
            , AP.collectionFirst      = Nothing
            , AP.collectionLast       = Nothing
            , AP.collectionItems      = map encodeRouteHome locals ++ remotes
            , AP.collectionContext    = Nothing
            }
    provideHtmlAndAP followingAP $ redirectToPrettyJSON here'
    where
    getTickets tids =
        map workItem <$> selectList [TicketDeckTicket <-. tids] []
        where
        workItem (Entity t (TicketDeck _ d)) = WorkItemTicket d t
    getCloths tids =
        map workItem <$> selectList [TicketLoomTicket <-. tids] []
        where
        workItem (Entity c (TicketLoom _ l _)) = WorkItemCloth l c
    getRemotes aid =
        map (followRemoteTarget . entityVal) <$>
            selectList [FollowRemoteActor ==. aid] []

handleRobotInbox
    :: LocalActorBy KeyHashid
    -> (  UTCTime
       -> RemoteAuthor
       -> ActivityBody
       -> Maybe (RecipientRoutes, ByteString)
       -> LocalURI
       -> AP.SpecificActivity URIMode
       -> ExceptT Text Handler (Text, Maybe (ExceptT Text Worker Text))
       )
    -> UTCTime
    -> ActivityAuthentication
    -> ActivityBody
    -> ExceptT Text Handler (Text, Maybe (ExceptT Text Worker Text))
handleRobotInbox recipByHash handleSpecific now auth body = do
    remoteAuthor <-
        case auth of
            ActivityAuthLocal _ -> throwE "Got a forwarded local activity, I don't need those"
            ActivityAuthRemote ra -> return ra
    luActivity <-
        fromMaybeE (AP.activityId $ actbActivity body) "Activity without 'id'"
    localRecips <- do
        mrecips <- parseAudience $ AP.activityAudience $ actbActivity body
        paudLocalRecips <$> fromMaybeE mrecips "Activity with no recipients"
    msig <- checkForwarding recipByHash
    let mfwd = (localRecips,) <$> msig
    handleSpecific now remoteAuthor body mfwd luActivity (AP.activitySpecific $ actbActivity body)

actorKeyAP
    :: ( MonadSite m, SiteEnv m ~ site
       , SiteFedURI site, SiteFedURIMode site ~ u
       )
    => Maybe (Route site) -> Route site -> AK.ActorKey -> m (AP.PublicKey u)
actorKeyAP maybeHolderR keyR akey = do
    encodeRouteLocal <- getEncodeRouteLocal
    return AP.PublicKey
        { AP.publicKeyId       = LocalRefURI $ Left $ encodeRouteLocal keyR
        , AP.publicKeyExpires  = Nothing
        , AP.publicKeyOwner    =
            case maybeHolderR of
                Nothing -> AP.OwnerInstance
                Just holderR -> AP.OwnerActor $ encodeRouteLocal holderR
        , AP.publicKeyMaterial = AK.actorKeyPublicBin akey
        }

serveInstanceKey
    :: ((AK.ActorKey, AK.ActorKey) -> AK.ActorKey)
    -> Route App
    -> Handler TypedContent
serveInstanceKey choose keyR = do
    maybeKeys <- asksSite appActorKeys
    case maybeKeys of
        Nothing -> notFound
        Just keys -> do
            akey <- liftIO $ do
                (akey1, akey2, _) <- readTVarIO keys
                return $ choose (akey1, akey2)
            keyAP <- actorKeyAP Nothing keyR akey
            provideHtmlAndAP keyAP $ redirectToPrettyJSON keyR

servePerActorKey'
    :: LocalActorBy KeyHashid
    -> KeyHashid SigKey
    -> AK.ActorKey
    -> Handler TypedContent
servePerActorKey' holderByHash keyHash akey = do
    let holderR = renderLocalActor holderByHash
        keyR = stampRoute holderByHash keyHash
    keyAP <- actorKeyAP (Just holderR) keyR akey
    provideHtmlAndAP keyAP $ redirectToPrettyJSON keyR

servePerActorKey
    :: (PersistRecordBackend holder SqlBackend, ToBackendKey SqlBackend holder)
    => (holder -> ActorId)
    -> (KeyHashid holder -> LocalActorBy KeyHashid)
    -> KeyHashid holder
    -> KeyHashid SigKey
    -> Handler TypedContent
servePerActorKey holderActor localActorHolder holderHash keyHash =
    servePerActorKey'' (pure . holderActor) localActorHolder holderHash keyHash

servePerActorKey''
    :: (PersistRecordBackend holder SqlBackend, ToBackendKey SqlBackend holder)
    => (holder -> AppDB ActorId)
    -> (KeyHashid holder -> LocalActorBy KeyHashid)
    -> KeyHashid holder
    -> KeyHashid SigKey
    -> Handler TypedContent
servePerActorKey'' holderActor localActorHolder holderHash keyHash = do
    holderID <- decodeKeyHashid404 holderHash
    keyID <- decodeKeyHashid404 keyHash
    akey <- runDB $ do
        actorID <- holderActor =<< get404 holderID
        SigKey actorID' akey <- get404 keyID
        unless (actorID' == actorID) notFound
        return akey
    servePerActorKey' (localActorHolder holderHash) keyHash akey

serveForks :: Route App -> Route App -> ResourceId -> Handler TypedContent
serveForks meR here resourceID = do
    forks <- runDB $ do
        l <- E.select $ E.from $ \ (fork `E.InnerJoin` accept `E.InnerJoin` create) -> do
            E.on $ accept E.^. ResourceForkAcceptCreate E.==. create E.^. ResourceForkCreateId
            E.on $ fork E.^. ResourceForkLocalAccept E.==. accept E.^. ResourceForkAcceptId
            E.where_ $ create E.^. ResourceForkCreateResource E.==. E.val resourceID
            return
                ( accept E.^. ResourceForkAcceptId
                , fork E.^. ResourceForkLocalFork
                )
        l' <- for l $ \ (E.Value acceptID, E.Value forkID) -> do
            Resource actorID <- getJust forkID
            actor <- getJust actorID
            byKey <- getLocalActor actorID
            return (acceptID, Left (byKey, actor))
        r <- E.select $ E.from $ \ (fork `E.InnerJoin` accept `E.InnerJoin` create) -> do
            E.on $ accept E.^. ResourceForkAcceptCreate E.==. create E.^. ResourceForkCreateId
            E.on $ fork E.^. ResourceForkRemoteAccept E.==. accept E.^. ResourceForkAcceptId
            E.where_ $ create E.^. ResourceForkCreateResource E.==. E.val resourceID
            return
                ( accept E.^. ResourceForkAcceptId
                , fork E.^. ResourceForkRemoteFork
                )
        r' <- for r $ \ (E.Value acceptID, E.Value forkID) -> do
            actor <- getRemoteActorData forkID
            return (acceptID, Right actor)
        return $ map snd $ reverse $ sortOn fst $ l' ++ r'

    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    hashActor <- getHashLocalActor
    let forksAP = AP.Collection
            { AP.collectionId         = encodeRouteLocal here
            , AP.collectionType       = AP.CollectionTypeOrdered
            , AP.collectionTotalItems = Just $ length forks
            , AP.collectionCurrent    = Nothing
            , AP.collectionFirst      = Nothing
            , AP.collectionLast       = Nothing
            , AP.collectionItems      =
                map (either
                        (encodeRouteHome . renderLocalActor . hashActor . fst)
                        (\ (i, o, _) -> ObjURI (instanceHost i) (remoteObjectIdent o))
                    )
                    forks
            , AP.collectionContext    = Just $ encodeRouteLocal meR
            }
    provideHtmlAndAP forksAP $ redirectToPrettyJSON here
