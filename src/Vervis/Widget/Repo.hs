{- This file is part of Vervis.
 -
 - Written in 2016, 2018, 2020, 2024 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Widget.Repo
    ( refSelectW
    , changesW
    , repoNavW
    )
where

import Data.Foldable (foldl')
import Data.List.NonEmpty (NonEmpty)
import Data.Set (Set)
import Data.Text (Text)
import Data.Vector (Vector)
import Database.Persist
import Yesod.Core.Handler (getsYesod)

import qualified Data.List.NonEmpty as N
import qualified Data.Text as T (take)
import qualified Data.Vector as V

import Yesod.Hashids

import Vervis.Changes
import Vervis.Foundation
import Vervis.Model
import Vervis.Model.Ident
import Vervis.Settings (widgetFile, appDiffContextLines)
import Vervis.Style

refSelectW :: KeyHashid Repo -> Set Text -> Set Text -> Widget
refSelectW hash branches tags = $(widgetFile "repo/widget/ref-select")

changesW :: Foldable f => KeyHashid Repo -> f LogEntry -> Widget
changesW hash entries = $(widgetFile "repo/widget/changes")

repoNavW :: Entity Repo -> Actor -> Widget
repoNavW (Entity repoID repo) actor = do
    repoHash <- encodeKeyHashid repoID
    hashLoom <- getEncodeKeyHashid
    $(widgetFile "repo/widget/nav")
