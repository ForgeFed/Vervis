{- This file is part of Vervis.
 -
 - Written in 2023, 2024 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Actor.Repo
    (
    )
where

import Control.Applicative
import Control.Exception.Base hiding (handle)
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Foldable
import Data.HList (HList (..))
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Optics.Core
import System.Directory
import Yesod.Persist.Core

import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Control.Concurrent.Actor
import Development.Darcs
import Development.Git
import Development.PatchMediaType
import Network.FedURI
import Web.Actor
import Web.Actor.Persist
import Web.Text
import Yesod.MonadSite

import qualified Data.F3 as F3
import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Database.Persist.Local

import Vervis.Access
import Vervis.ActivityPub
import Vervis.Actor
import Vervis.Actor.Common
import Vervis.Actor2
import Vervis.Cloth
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Data.Discussion
import Vervis.Data.Ticket
import Vervis.FedURI

import Vervis.Foundation
import Vervis.Model
import Vervis.Recipient (makeRecipientSet, LocalStageBy (..), Aud (..), collectAudience, localActorFollowers)
import Vervis.Path
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.Persist.Ticket
import Vervis.RemoteActorStore
import Vervis.Ticket
import Vervis.Web.Collab

-- Meaning: Someone is creating something
-- Behavior:
--  If someone is creating a repo where I'm the origin, record this in my DB
--  records, where I track my forks
repoCreate
    :: UTCTime
    -> RepoId
    -> Verse
    -> AP.Create URIMode
    -> ActE (Text, Act (), Next)
repoCreate now repoID (Verse authorIdMsig body) (AP.Create obj muFactory) = do

    -- Check input
    (factory, vcs) <- do
        uMe <- do
            encodeRouteHome <- getEncodeRouteHome
            repoHash <- encodeKeyHashid repoID
            return $ encodeRouteHome $ RepoR repoHash
        case obj of
            AP.CreateRepository detail vcs mlocal -> do
                verifyNothingE mlocal "Object's id must not be provided"
                uFactory <- fromMaybeE muFactory "'origin' expected in Create-Repo"
                let muOrigin = AP.actorOrigin detail
                uOrigin <- fromMaybeE muOrigin "'origin' not specified in Repo - why did I receive this Create then?"
                unless (uOrigin == uMe) $
                    throwE "This Create-Repo isn't for me"
                f <- parseActorURI' uFactory
                f' <-
                    bitraverse
                        (\case
                            LocalActorFactory k -> withDBExcept $ getEntityE k "Factory not found in DB"
                            _ -> throwE "Create.origin isn't a Factory"
                        )
                        (\ u@(ObjURI h lu) -> do
                            instanceID <-
                                lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                            result <-
                                ExceptT $ first (T.pack . displayException) <$>
                                    fetchRemoteActor' instanceID h lu
                            case result of
                                Left Nothing -> throwE "Factory @id mismatch"
                                Left (Just err) -> throwE $ T.pack $ displayException err
                                Right Nothing -> throwE "Factory isn't an actor"
                                Right (Just actor) -> do
                                    case remoteActorType $ entityVal actor of
                                        AP.ActorTypeFactory -> pure ()
                                        _ -> throwE "Remote factory type isn't Factory"
                                    return (u, actor)
                        )
                        f
                return (f', vcs)
            _ -> throwE "Not a Create-Repo"

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        repo <- lift $ getJust repoID
        let komponentMeID = repoKomponent repo
        Komponent resourceMeID <- lift $ getJust komponentMeID
        resourceMe <- lift $ getJust resourceMeID
        let actorMeID = resourceActor resourceMe
        actorMe <- lift $ getJust actorMeID

        unless (repoVcs repo == vcs) $
            throwE "New repo's VCS and my VCS differ"

        -- Insert the Create to my inbox
        mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorMe) False
        lift $ for mractid $ \ (inboxItemID, _createDB) -> do

            -- Record the fork attempt in DB
            createID <- insert $ ResourceForkCreate resourceMeID inboxItemID
            case factory of
                Left (Entity factoryID _) ->
                    insert_ $ ResourceForkFactoryLocal createID factoryID
                Right (_, Entity actorID _) ->
                    insert_ $ ResourceForkFactoryRemote createID actorID

            -- Prepare forwarding the Offer to my followers
            repoHash <- encodeKeyHashid repoID
            let sieve = makeRecipientSet [] [LocalStageRepoFollowers repoHash]

            return (actorMeID, sieve, inboxItemID)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (actorMeID, sieve, inboxItemID) -> do
            forwardActivity
                authorIdMsig body (LocalActorRepo repoID) actorMeID sieve
            doneDB inboxItemID "Recorded the fork request"

-- Clone data from an existing repo to a local, newly created repo
repoFillData
    :: RepoId
    -> Either (Entity Repo) (FedURI, Entity RemoteActor, F3.ExtRepo)
    -> ActDBE ()
repoFillData repoID (Left (Entity rid r)) = do
    repo <- lift $ getJust repoID
    Komponent resourceID <- lift $ getJust $ repoKomponent repo
    Komponent originID <- lift $ getJust $ repoKomponent r
    repoHash <- encodeKeyHashid repoID
    ridHash <- encodeKeyHashid rid
    usPath <- lift . lift $ askRepoDir' repoHash
    themPath <- lift . lift $ askRepoDir' ridHash
    case (repoVcs repo, repoVcs r) of
        (VCSDarcs, VCSDarcs) ->
            withDarcsRepo usPath $ darcsPull $ T.pack themPath
        (VCSGit, VCSGit) -> do
            themPath' <- liftIO $ makeAbsolute themPath
            withGitRepo usPath $ gitFetch (repoMainBranch repo) (T.pack themPath') (repoMainBranch r)
        _ -> liftIO $ throwIO $ userError "Origin repo VCS mismatch"
    lift $ insert_ $ ResourceOriginLocal resourceID originID
repoFillData repoID (Right (ObjURI hOrigin _ , Entity originID _, r)) = do
    repo <- lift $ getJust repoID
    Komponent resourceID <- lift $ getJust $ repoKomponent repo
    repoHash <- encodeKeyHashid repoID
    path <- lift . lift $ askRepoDir' repoHash
    case (repoVcs repo, F3.repoVcs r) of
        (VCSDarcs, F3.Darcs) -> do
            let u = F3.repoIndex r
            withDarcsRepo path $ darcsPull u
        (VCSGit, F3.Git) -> do
            let u = F3.repoIndex r
            withGitRepo path $ gitFetch (repoMainBranch repo) u (F3.repoMain r)
        _ -> liftIO $ throwIO $ userError "Origin repo VCS mismatch"
    lift $ insert_ $ ResourceOriginRemote resourceID originID

repoVerse :: RepoId -> Verse -> ActE (Text, Act (), Next)
repoVerse repoID verse@(Verse _authorIdMsig body) = do
    now <- liftIO getCurrentTime
    case AP.activitySpecific $ actbActivity body of
        AP.AcceptActivity accept ->
            topicAccept repoResource ComponentRepo now repoID verse accept
        AP.CreateActivity create ->
            repoCreate now repoID verse create
        _ -> throwE "Unsupported activity type for Repo"

instance ActorLaunch Repo where
    actorBehavior _ =
        (handleMethod @"verse" := \ repoID verse -> adaptHandlerResult $ do
            errboxID <- lift $ withDB $ do
                resourceID <- repoResource <$> getJust repoID
                Resource actorID <- getJust resourceID
                actorErrbox <$> getJust actorID
            adaptErrbox errboxID False (repoVerse repoID) verse
        )
        `HCons`
        (handleMethod @"wait-during-push" := \ repoID waitValue -> adaptHandlerResult $ do
            liftIO waitValue
            done "Waited for push to complete"
        )
        `HCons`
        (handleMethod @"init" := \ repoID creator morigin -> adaptHandlerResult $ do
            now <- liftIO getCurrentTime
            let grabResource = fmap komponentResource . getJust . repoKomponent
            topicInit AP.ActorTypeRepo repoFillData grabResource LocalResourceRepo now repoID creator morigin
        )
        `HCons`
        HNil
