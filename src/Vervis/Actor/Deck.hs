{- This file is part of Vervis.
 -
 - Written in 2019, 2020, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Actor.Deck
    (
    )
where

import Control.Applicative
import Control.Exception.Base hiding (handle)
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Foldable
import Data.HList (HList (..))
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Optics.Core
import Yesod.Persist.Core

import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Control.Concurrent.Actor
import Network.FedURI
import Web.Actor
import Web.Actor.Persist
import Web.Text
import Yesod.MonadSite

import qualified Data.F3 as F3
import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Database.Persist.Local

import Vervis.Access
import Vervis.ActivityPub
import Vervis.Actor
import Vervis.Actor.Common
import Vervis.Actor2
import Vervis.Cloth
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Data.Discussion
import Vervis.Data.Ticket
import Vervis.FedURI

import Vervis.Foundation
import Vervis.Model hiding (deckCreate)
import Vervis.Recipient (makeRecipientSet, LocalStageBy (..), Aud (..), collectAudience, localActorFollowers)
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.Persist.Ticket
import Vervis.RemoteActorStore
import Vervis.Ticket
import Vervis.Web.Collab

-- Meaning: An actor is adding some object to some target
-- Behavior:
--  * If the object is me:
--      * Verify that the object is me
--      * Verify the target is some project's components collection URI
--      * Verify the Add is authorized
--      * For all the Stem records I have for this project:
--          * Verify I'm not yet a member of the project
--          * Verify I haven't already Accepted an our-Add to this project
--          * Verify I haven't already seen an them-Invite-and-Project-accept for
--            this project
--      * Insert the Add to my inbox
--      * Create a Stem record in DB
--      * Forward the Add activity to my followers
--      * Send an Accept on the Add:
--          * To:
--              * The author of the Add
--              * The project
--          * CC:
--              * Author's followers
--              * Project's followers
--              * My followers
deckAdd
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Add URIMode
    -> ActE (Text, Act (), Next)
deckAdd = componentAdd deckKomponent ComponentDeck

-- Meaning: An actor A is offering a ticket or a ticket dependency
-- Behavior:
--      * Verify I'm the target
--      * Insert the Offer to my inbox
--      * Create the new ticket in my DB
--      * Forward the Offer to my followers
--      * Publish an Accept to:
--          - My followers
--          - Offer sender+followers
deckOffer
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Offer URIMode
    -> ActE (Text, Act (), Next)
deckOffer now deckID (Verse authorIdMsig body) (AP.Offer object uTarget) = do

    -- Check input
    (title, desc, source) <- do
        ticket <-
            case object of
                AP.OfferTicket t -> pure t
                _ -> throwE "Unsupported Offer.object type"
        ObjURI hAuthor _ <- lift $ getActorURI authorIdMsig
        let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
        WorkItemOffer {..} <- checkOfferTicket hAuthor ticket uTarget
        unless (bimap LocalActorPerson id wioAuthor == author) $
            throwE "Offering a Ticket attributed to someone else"
        case wioRest of
            TAM_Task deckID' ->
                if deckID' == deckID
                    then return ()
                    else throwE
                            "Offer target is some other local deck, so I have \
                            \no use for this Offer. Was I supposed to receive \
                            \it?"
            TAM_Merge _ _ ->
                throwE
                    "Offer target is some local loom, so I have no use for \
                    \this Offer. Was I supposed to receive it?"
            TAM_Remote _ _ ->
                throwE
                    "Offer target is some remote tracker, so I have no use \
                    \for this Offer. Was I supposed to receive it?"
        return (wioTitle, wioDesc, wioSource)

    -- Verify the capability URI, if provided, is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCapability <-
        for (AP.activityCapability $ actbActivity body) $ \ uCap ->
            nameExceptT "Offer.capability" $
                first (\ (actor, _, item) -> (actor, item)) <$>
                    parseActivityURI' uCap

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (deckRecip, actorRecip) <- lift $ do
            d <- getJust deckID
            (d,) <$> getJust (deckActor d)

        -- Insert the Offer to my inbox
        mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
        for mractid $ \ (inboxItemID, offerDB) -> do

            -- If a capability is provided, check it
            for_ maybeCapability $ \ cap -> do
                lcap <-
                    case cap of
                        Left c -> pure c
                        Right _ -> throwE "Capability is a remote URI, i.e. not authored by me"
                verifyCapability'
                    lcap
                    authorIdMsig
                    (LocalResourceDeck deckID)
                    AP.RoleReport

            -- Prepare forwarding the Offer to my followers
            let recipByID = LocalActorDeck deckID
            recipByHash <- hashLocalActor recipByID
            let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

            -- Insert the new ticket to our DB
            acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorRecip) now
            offerDB' <-
                bitraverse
                (traverseOf _1 $ \case
                    LocalActorPerson personID -> pure personID
                    _ -> throwE "Local non-Person ticket authors not allowed"
                )
                pure
                offerDB
            taskID <- lift $ insertTask now title desc source deckID offerDB' acceptID

            -- Prepare an Accept activity and insert to my outbox
            accept@(actionAccept, _, _, _) <- lift $ prepareAccept taskID
            let recipByKey = LocalActorDeck deckID
            _luAccept <- lift $ updateOutboxItem' recipByKey acceptID actionAccept

            return (deckActor deckRecip, sieve, acceptID, accept, inboxItemID)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (deckActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
            forwardActivity
                authorIdMsig body (LocalActorDeck deckID) deckActorID sieve
            lift $ sendActivity
                (LocalActorDeck deckID) deckActorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            doneDB inboxItemID "Opened a ticket and forwarded the Offer"

    where

    insertTask now title desc source deckID offerDB acceptID = do
        did <- insert Discussion
        fsid <- insert FollowerSet
        tid <- insert Ticket
            { ticketNumber      = Nothing
            , ticketCreated     = now
            , ticketTitle       = title
            , ticketSource      = source
            , ticketDescription = desc
            , ticketDiscuss     = did
            , ticketFollowers   = fsid
            , ticketAccept      = acceptID
            }
        case offerDB of
            Left (personID, _, offerID) ->
                insert_ TicketAuthorLocal
                    { ticketAuthorLocalTicket = tid
                    , ticketAuthorLocalOpen   = offerID
                    }
            Right (author, _, offerID) ->
                insert_ TicketAuthorRemote
                    { ticketAuthorRemoteTicket = tid
                    , ticketAuthorRemoteAuthor = remoteAuthorId author
                    , ticketAuthorRemoteOpen   = offerID
                    }
        insert $ TicketDeck tid deckID

    prepareAccept taskID = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audSender <- makeAudSenderWithFollowers authorIdMsig
        deckHash <- encodeKeyHashid deckID
        taskHash <- encodeKeyHashid taskID
        let audDeck = AudLocal [] [LocalStageDeckFollowers deckHash]
        uOffer <- lift $ getActivityURI authorIdMsig

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audSender, audDeck]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uOffer]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uOffer
                    , AP.acceptResult   =
                        Just $ encodeRouteLocal $ TicketR deckHash taskHash
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor is asking to close a ticket
-- Behavior:
--      * Verify it's my ticket
--      * Verify the Resolve is authorized
--      * Insert the Resolve to my inbox
--      * Close the ticket in my DB
--      * Forward the Resolve to my followers & ticket followers
--      * Publish an Accept to:
--          - My followers
--          - Ticket's followers
--          - Resolve sender+followers
deckResolve
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Resolve URIMode
    -> ActE (Text, Act (), Next)
deckResolve now deckID (Verse authorIdMsig body) (AP.Resolve uObject) = do

    -- Check input
    deckHash <- encodeKeyHashid deckID
    taskHash <- nameExceptT "Resolve.object" $ do
        route <- do
            routeOrRemote <- parseFedURI uObject
            case routeOrRemote of
                Left route -> pure route
                Right _ -> throwE "Remote, so definitely not mine"
        case route of
            TicketR deckHash' taskHash | deckHash' == deckHash ->
                return taskHash
            _ -> throwE "Local route but not a ticket of mine"
    taskID <- decodeKeyHashidE taskHash "Invalid TicketDeck keyhashid"

    -- Verify that a capability is provided
    uCap <- do
        let muCap = AP.activityCapability $ actbActivity body
        fromMaybeE muCap "No capability provided"

    -- Verify the sender is authorized by the tracker to resolve a ticket
    verifyCapability''
        uCap
        authorIdMsig
        (LocalResourceDeck deckID)
        AP.RoleTriage

    {-
    -- Check capability
    capability <- do

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the capability URI is one of:
        --   * Outbox item URI of a local actor, i.e. a local activity
        --   * A remote URI
        cap <- nameExceptT "Resolve.capability" $ parseActivityURI' uCap

        -- Verify the capability is local
        case cap of
            Left (actorByKey, _, outboxItemID) ->
                return (actorByKey, outboxItemID)
            _ -> throwE "Capability is remote i.e. definitely not by me"
    -}

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (deckRecip, actorRecip) <- lift $ do
            d <- getJust deckID
            (d,) <$> getJust (deckActor d)

        -- Find ticket in DB, verify it's not resolved
        ticketID <- do
            maybeTicket <- lift $ getTicket deckID taskID
            (_deck, _task, Entity ticketID _, _author, maybeResolve) <-
                fromMaybeE maybeTicket "I don't have such a ticket in DB"
            unless (isNothing maybeResolve) $
                throwE "Ticket is already resolved"
            return ticketID

        -- Insert the Resolve to my inbox
        mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
        for mractid $ \ (inboxItemID, resolveDB) -> do

            {-
            -- Verify the sender is authorized by the tracker to resolve a ticket
            verifyCapability'
                capability
                authorIdMsig
                (GrantResourceDeck deckID)
                AP.RoleTriage
            -}

            -- Prepare forwarding the Resolve to my followers & ticket
            -- followers
            let sieve =
                    makeRecipientSet []
                        [ LocalStageDeckFollowers deckHash
                        , LocalStageTicketFollowers deckHash taskHash
                        ]

            -- Mark ticket in DB as resolved by the Resolve
            acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorRecip) now
            lift $ insertResolve ticketID resolveDB acceptID

            -- Prepare an Accept activity and insert to my outbox
            accept@(actionAccept, _, _, _) <- lift $ prepareAccept taskID
            let recipByKey = LocalActorDeck deckID
            _luAccept <- lift $ updateOutboxItem' recipByKey acceptID actionAccept

            return (deckActor deckRecip, sieve, acceptID, accept, inboxItemID)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (deckActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
            forwardActivity
                authorIdMsig body (LocalActorDeck deckID) deckActorID sieve
            lift $ sendActivity
                (LocalActorDeck deckID) deckActorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            doneDB inboxItemID "Resolved ticket and forwarded the Resolve"

    where

    insertResolve ticketID resolveDB acceptID = do
        trid <- insert TicketResolve
            { ticketResolveTicket = ticketID
            , ticketResolveAccept = acceptID
            }
        case resolveDB of
            Left (_actorByKey, _, resolveID) ->
                insert_ TicketResolveLocal
                    { ticketResolveLocalTicket   = trid
                    , ticketResolveLocalActivity = resolveID
                    }
            Right (author, _, resolveID) ->
                insert_ TicketResolveRemote
                    { ticketResolveRemoteTicket   = trid
                    , ticketResolveRemoteActivity = resolveID
                    , ticketResolveRemoteActor    = remoteAuthorId author
                    }

    prepareAccept taskID = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audSender <- makeAudSenderWithFollowers authorIdMsig
        deckHash <- encodeKeyHashid deckID
        taskHash <- encodeKeyHashid taskID
        let audDeck =
                AudLocal
                    []
                    [ LocalStageDeckFollowers deckHash
                    , LocalStageTicketFollowers deckHash taskHash
                    ]
        uResolve <- lift $ getActivityURI authorIdMsig

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audSender, audDeck]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uResolve]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uResolve
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

deckRevoke
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Revoke URIMode
    -> ActE (Text, Act (), Next)
deckRevoke = componentRevoke deckKomponent ComponentDeck

------------------------------------------------------------------------------
-- Following
------------------------------------------------------------------------------

-- Meaning: A remote actor is following someone/something
-- Behavior:
--      * Verify the target is me or a ticket of mine
--      * Record the follow in DB
--      * Publish and send an Accept to the sender and its followers
deckFollow
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Follow URIMode
    -> ActE (Text, Act (), Next)
deckFollow now recipDeckID verse follow = do
    recipDeckHash <- encodeKeyHashid recipDeckID
    actorFollow
        (\case
            DeckR d | d == recipDeckHash -> pure Nothing
            TicketR d t | d == recipDeckHash ->
                Just <$> decodeKeyHashidE t "Invalid task keyhashid"
            _ -> throwE "Asking to follow someone else"
        )
        deckActor
        False
        (\ recipDeckActor maybeTaskID ->
            case maybeTaskID of
                Nothing -> pure $ actorFollowers recipDeckActor
                Just taskID -> do
                    maybeTicket <- lift $ getTicket recipDeckID taskID
                    (_deck, _task, Entity _ ticket, _author, _resolve) <-
                        fromMaybeE maybeTicket "I don't have this ticket in DB"
                    return $ ticketFollowers ticket
        )
        (\ _ -> pure $ makeRecipientSet [] [])
        LocalActorDeck
        (\ _ -> pure [])
        now recipDeckID verse follow

------------------------------------------------------------------------------
-- Access
------------------------------------------------------------------------------

deckAccept
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Accept URIMode
    -> ActE (Text, Act (), Next)
deckAccept = topicAccept deckResource ComponentDeck

-- Meaning: An actor rejected something
-- Behavior:
--     * If it's on an Invite where I'm the resource:
--         * Verify the Reject is by the Invite target
--         * Remove the relevant Collab record from DB
--         * Forward the Reject to my followers
--         * Send a Reject on the Invite:
--             * To: Rejecter (i.e. Invite target)
--             * CC: Invite sender, Rejecter's followers, my followers
--     * If it's on a Join where I'm the resource:
--         * Verify the Reject is authorized
--         * Remove the relevant Collab record from DB
--         * Forward the Reject to my followers
--         * Send a Reject:
--             * To: Join sender
--             * CC: Reject sender, Join sender's followers, my followers
--     * Otherwise respond with error
deckReject
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Reject URIMode
    -> ActE (Text, Act (), Next)
deckReject = topicReject deckResource LocalResourceDeck

deckInvite
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Invite URIMode
    -> ActE (Text, Act (), Next)
deckInvite = componentInvite deckKomponent ComponentDeck

deckRemove
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Remove URIMode
    -> ActE (Text, Act (), Next)
deckRemove = componentRemove deckKomponent ComponentDeck

-- Meaning: An actor A asked to join a resource
-- Behavior:
--      * Verify the resource is me
--      * Verify A doesn't already have an invite/join/grant for me
--      * Remember the join in DB
--      * Forward the Join to my followers
deckJoin
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Join URIMode
    -> ActE (Text, Act (), Next)
deckJoin = topicJoin deckResource LocalResourceDeck

deckGrant
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Grant URIMode
    -> ActE (Text, Act (), Next)
deckGrant = componentGrant deckResource ComponentDeck

------------------------------------------------------------------------------
-- Ambiguous: Following/Resolving
------------------------------------------------------------------------------

-- Meaning: An actor is undoing some previous action
-- Behavior:
--      * If they're undoing their Following of me, or a ticket of mine:
--          * Record it in my DB
--          * Publish and send an Accept only to the sender
--      * If they're unresolving a resolved ticket of mine:
--          * Verify they're authorized via a Grant
--          * Record it in my DB
--          * Forward the Undo to my+ticket followers
--          * Send an Accept to sender+followers and to my+ticket followers
--      * Otherwise respond with an error
deckUndo
    :: UTCTime
    -> DeckId
    -> Verse
    -> AP.Undo URIMode
    -> ActE (Text, Act (), Next)
deckUndo now recipDeckID (Verse authorIdMsig body) (AP.Undo uObject) = do

    -- Check input
    undone <-
        first (\ (actor, _, item) -> (actor, item)) <$>
            parseActivityURI' uObject

    -- Verify the capability URI, if provided, is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCapability <-
        for (AP.activityCapability $ actbActivity body) $ \ uCap ->
            nameExceptT "Undo capability" $
                first (\ (actor, _, item) -> (actor, item)) <$>
                    parseActivityURI' uCap

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (deckRecip, actorRecip) <- lift $ do
            p <- getJust recipDeckID
            (p,) <$> getJust (deckActor p)

        -- Insert the Undo to my inbox
        mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
        for mractid $ \ (inboxItemID, _undoDB) -> do

            maybeUndo <- runMaybeT $ do

                -- Find the undone activity in our DB
                undoneDB <- MaybeT $ getActivity undone

                let followers = actorFollowers actorRecip
                asum
                    [ tryUnfollow followers undoneDB authorIdMsig
                    , tryUnresolve maybeCapability undoneDB
                    ]

            (sieve, audience) <-
                fromMaybeE
                    maybeUndo
                    "Undone activity isn't a Follow or Resolve related to me"

            -- Prepare an Accept activity and insert to deck's outbox
            acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorRecip) now
            accept@(actionAccept, _, _, _) <- lift $ lift $ prepareAccept audience
            _luAccept <- lift $ updateOutboxItem' (LocalActorDeck recipDeckID) acceptID actionAccept

            return (deckActor deckRecip, sieve, acceptID, accept, inboxItemID)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (actorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
            forwardActivity
                authorIdMsig body (LocalActorDeck recipDeckID) actorID sieve
            lift $ sendActivity
                (LocalActorDeck recipDeckID) actorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            doneDB inboxItemID
                "Undid the Follow/Resolve, forwarded the Undo and published \
                \Accept"

    where

    verifyTargetTicket followerSetID = do
        ticketID <-
            MaybeT $ lift $ getKeyBy $ UniqueTicketFollowers followerSetID
        TicketDeck _ d <-
            MaybeT $ lift $ getValBy $ UniqueTicketDeck ticketID
        guard $ d == recipDeckID

    tryUnfollow deckFollowersID (Left (_actorByKey, _actorE, outboxItemID)) (Left (_, actorID, _)) = do
        Entity followID follow <-
            MaybeT $ lift $ getBy $ UniqueFollowFollow outboxItemID
        let followerID = followActor follow
            followerSetID = followTarget follow
        verifyTargetMe followerSetID <|> verifyTargetTicket followerSetID
        unless (followerID == actorID) $
            lift $ throwE "You're trying to Undo someone else's Follow"
        lift $ lift $ delete followID
        audSenderOnly <- lift $ lift $ lift $ makeAudSenderOnly authorIdMsig
        return (makeRecipientSet [] [], [audSenderOnly])
        where
        verifyTargetMe followerSetID = guard $ followerSetID == deckFollowersID
    tryUnfollow deckFollowersID (Right remoteActivityID) (Right (author, _, _)) = do
        Entity remoteFollowID remoteFollow <-
            MaybeT $ lift $ getBy $ UniqueRemoteFollowFollow remoteActivityID
        let followerID = remoteFollowActor remoteFollow
            followerSetID = remoteFollowTarget remoteFollow
        verifyTargetMe followerSetID <|> verifyTargetTicket followerSetID
        unless (followerID == remoteAuthorId author) $
            lift $ throwE "You're trying to Undo someone else's Follow"
        lift $ lift $ delete remoteFollowID
        audSenderOnly <- lift $ lift $ lift $ makeAudSenderOnly authorIdMsig
        return (makeRecipientSet [] [], [audSenderOnly])
        where
        verifyTargetMe followerSetID = guard $ followerSetID == deckFollowersID
    tryUnfollow _ _ _ = mzero

    tryUnresolve maybeCapability undone = do
        (deleteFromDB, ticketID) <- findTicket undone
        Entity taskID (TicketDeck _ d) <-
            MaybeT $ lift $ getBy $ UniqueTicketDeck ticketID
        guard $ d == recipDeckID

        -- Verify the sender is authorized by the deck to unresolve a ticket
        capability <- lift $ do
            cap <-
                fromMaybeE
                    maybeCapability
                    "Asking to unresolve ticket but no capability provided"
            case cap of
                Left c -> pure c
                Right _ -> throwE "Capability is a remote URI, i.e. not authored by me"
        lift $
            verifyCapability'
                capability
                authorIdMsig
                (LocalResourceDeck recipDeckID)
                AP.RoleTriage

        lift $ lift deleteFromDB

        recipDeckHash <- encodeKeyHashid recipDeckID
        taskHash <- encodeKeyHashid taskID
        audSender <- lift $ lift $ makeAudSenderWithFollowers authorIdMsig
        return
            ( makeRecipientSet
                []
                [ LocalStageDeckFollowers recipDeckHash
                , LocalStageTicketFollowers recipDeckHash taskHash
                ]
            , [ AudLocal
                    []
                    [ LocalStageDeckFollowers recipDeckHash
                    , LocalStageTicketFollowers recipDeckHash taskHash
                    ]
              , audSender
              ]
            )
        where
        findTicket (Left (_actorByKey, _actorEntity, itemID)) = do
            Entity resolveLocalID resolveLocal <-
                MaybeT $ lift $ getBy $ UniqueTicketResolveLocalActivity itemID
            let resolveID = ticketResolveLocalTicket resolveLocal
            resolve <- lift $ lift $ getJust resolveID
            let ticketID = ticketResolveTicket resolve
            return
                ( delete resolveLocalID >> delete resolveID
                , ticketID
                )
        findTicket (Right remoteActivityID) = do
            Entity resolveRemoteID resolveRemote <-
                MaybeT $ lift $ getBy $
                    UniqueTicketResolveRemoteActivity remoteActivityID
            let resolveID = ticketResolveRemoteTicket resolveRemote
            resolve <- lift $ lift $ getJust resolveID
            let ticketID = ticketResolveTicket resolve
            return
                ( delete resolveRemoteID >> delete resolveID
                , ticketID
                )

    prepareAccept audience = do
        encodeRouteHome <- getEncodeRouteHome

        uUndo <- getActivityURI authorIdMsig
        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = []
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uUndo
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

------------------------------------------------------------------------------
-- Main behavior function
------------------------------------------------------------------------------

-- Migrate data from a remote deck to a local, newly created deck
deckFillData
    :: DeckId
    -> Either (Entity Deck) (FedURI, Entity RemoteActor, F3.ExtIssueTracker)
    -> ActDBE ()
deckFillData _ (Left _) = throwE "Migrating from local Deck not supported"
deckFillData deckID (Right (ObjURI hOrigin _ , Entity originID _, it)) = do
    now <- liftIO getCurrentTime

    komponentID <- lift $ deckKomponent <$> getJust deckID
    Komponent resourceID <- lift $ getJust komponentID
    Resource actorID <- lift $ getJust resourceID
    actor <- lift $ getJust actorID

    lift $ insert_ $ ResourceOriginRemote resourceID originID

    for_ (F3.issueTrackerIssues it) $ \ issue -> do
        desc <- except $ renderPandocMarkdown $ F3.issueContent issue
        acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actor) now
        offerID <- lift $ insertEmptyOutboxItem' (actorOutbox actor) now
        did <- lift $ insert Discussion
        fsid <- lift $ insert FollowerSet
        ticketID <- lift $ insert Ticket
            { ticketNumber      = Nothing
            , ticketCreated     = F3.issueCreated issue
            , ticketTitle       = F3.issueTitle issue
            , ticketSource      = F3.issueContent issue
            , ticketDescription = desc
            , ticketDiscuss     = did
            , ticketFollowers   = fsid
            , ticketAccept      = acceptID
            }
        lift $ insert_ TicketAuthorLocal
            { ticketAuthorLocalTicket = ticketID
            , ticketAuthorLocalOpen   = offerID
            }
        lift $ insert_ $ TicketDeck ticketID deckID
        case F3.issueState issue of
            F3.IssueOpen -> pure ()
            F3.IssueClosed -> lift $ do
                resolveID <- insertEmptyOutboxItem' (actorOutbox actor) (fromMaybe now $ F3.issueClosed issue)
                acceptID <- insertEmptyOutboxItem' (actorOutbox actor) (fromMaybe now $ F3.issueClosed issue)
                trid <- insert $ TicketResolve ticketID acceptID
                insert_ $ TicketResolveLocal trid resolveID
        roid <- do
            ObjURI hIssue luIssue <- except $ first T.pack $ parseObjURI $ F3.issueIndex issue
            unless (hIssue == hOrigin) $
                throwE "Origin: Deck and Ticket hosts differ"
            iid <- lift $ do
                o <- remoteActorIdent <$> getJust originID
                remoteObjectInstance <$> getJust o
            lift $ either entityKey id <$> insertBy (RemoteObject iid luIssue)
        lift $ insert_ $ TicketOriginRemote ticketID roid

        traverse (fillComment actorID actor did Nothing) (F3.extIssueComments issue)

        where

        fillComment meActorID meActor did mparent c = do
            origin <- do
                msg <- ExceptT . lift . runExceptT $ do
                    uMsg <- except $ first T.pack $ parseObjURI $ F3.commentIndex c
                    parseMessageURI' uMsg
                bitraverse
                    (pure . view _3)
                    (\ (ObjURI h lu) -> lift $ do
                        iid <- either entityKey id <$> insertBy (Instance h)
                        either entityKey id <$> insertBy (RemoteObject iid lu)
                    )
                    msg

            html <- except $ renderPandocMarkdown $ F3.commentContent c
            mid <- lift $ insert Message
                { messageCreated = F3.commentCreated c
                , messageSource  = F3.commentContent c
                , messageContent = html
                , messageParent  = mparent
                , messageRoot    = did
                }
            createID <- lift $ insertEmptyOutboxItem' (actorOutbox meActor) (F3.commentCreated c)
            lmid <- lift $ insert LocalMessage
                { localMessageAuthor = meActorID
                , localMessageRest   = mid
                , localMessageCreate = createID
                , localMessageUnlinkedParent = Nothing
                }
            case origin of
                Left lmidO -> lift $ insert_ $ MessageOriginLocal lmid lmidO
                Right roidO -> lift $ insert_ $ MessageOriginRemote lmid roidO
            traverse_
                (fillComment meActorID meActor did (Just mid))
                (F3.extCommentReplies c)

deckVerse :: DeckId -> Verse -> ActE (Text, Act (), Next)
deckVerse deckID verse@(Verse _authorIdMsig body) = do
    now <- liftIO getCurrentTime
    case AP.activitySpecific $ actbActivity body of
        AP.AcceptActivity accept -> deckAccept now deckID verse accept
        AP.AddActivity add       -> deckAdd now deckID verse add
        AP.FollowActivity follow -> deckFollow now deckID verse follow
        AP.GrantActivity grant   -> deckGrant now deckID verse grant
        AP.InviteActivity invite -> deckInvite now deckID verse invite
        AP.JoinActivity join     -> deckJoin now deckID verse join
        AP.OfferActivity offer   -> deckOffer now deckID verse offer
        AP.RejectActivity reject -> deckReject now deckID verse reject
        AP.RemoveActivity remove -> deckRemove now deckID verse remove
        AP.ResolveActivity resolve -> deckResolve now deckID verse resolve
        AP.RevokeActivity revoke -> deckRevoke now deckID verse revoke
        AP.UndoActivity undo     -> deckUndo now deckID verse undo
        _ -> throwE "Unsupported activity type for Deck"

instance ActorLaunch Deck where
    actorBehavior _ =
        (handleMethod @"verse" := \ deckID verse -> adaptHandlerResult $ do
            errboxID <- lift $ withDB $ do
                resourceID <- deckResource <$> getJust deckID
                Resource actorID <- getJust resourceID
                actorErrbox <$> getJust actorID
            adaptErrbox errboxID False (deckVerse deckID) verse
        )
        `HCons`
        (handleMethod @"init" := \ deckID creator morigin -> adaptHandlerResult $ do
            now <- liftIO getCurrentTime
            let grabResource = fmap komponentResource . getJust . deckKomponent
            topicInit AP.ActorTypeTicketTracker deckFillData grabResource LocalResourceDeck now deckID creator morigin
        )
        `HCons`
        HNil
