{- This file is part of Vervis.
 -
 - Written in 2023, 2024 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Actor.Group
    (
    )
where

import Control.Applicative
import Control.Exception.Base hiding (handle)
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Barbie
import Data.Bifoldable
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Either
import Data.Foldable
import Data.HList (HList (..))
import Data.Maybe
import Data.List.NonEmpty (NonEmpty (..))
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Optics.Core
import Yesod.Persist.Core

import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Control.Concurrent.Actor
import Network.FedURI
import Web.Actor
import Web.Actor.Persist
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Database.Persist.Local

import Vervis.Access
import Vervis.ActivityPub
import Vervis.Actor
import Vervis.Actor.Common
import Vervis.Actor2
import Vervis.Cloth
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Data.Discussion
import Vervis.FedURI

import Vervis.Foundation
import Vervis.Model
import Vervis.Recipient (makeRecipientSet, LocalStageBy (..), Aud (..), collectAudience, localActorFollowers, renderLocalActor)
import Vervis.RemoteActorStore
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.Ticket
import Vervis.Web.Collab

-- Meaning: An actor is adding some object to some target
-- Behavior:
--      * If the target is my children list:
--        * Verify the object is a project, find in DB/HTTP
--        * Verify the Add is authorized
--        * Verify it's not already an active child of mine
--        * Verify it's not already an active parent of mine
--        * Verify it's not already in an Origin-Us process where I saw the Add
--          and sent my Accept
--        * Verify it's not already in an Origin-Them process, where I saw the
--          Add and the potential child's Accept
--        * Insert the Add to my inbox
--        * Create a Dest record in DB
--        * Forward the Add to my followers
--        * Publish an Accept to:
--          * The object project + followers
--          * Add sender + followers
--          * My followers
--        * Record my Accept in the Dest record
--
--      * If the target is my parents list:
--        * Verify the object is a project, find in DB/HTTP
--        * Verify the Add is authorized
--        * Verify it's not already an active parent of mine
--        * Verify it's not already an active child of mine
--        * Verify it's not already in an Origin-Us process where I saw the Add
--          and sent my Accept
--        * Verify it's not already in an Origin-Them process, where I saw the
--          Add and the potential parent's Accept
--        * Insert the Add to my inbox
--        * Create a Source record in DB
--        * Forward the Add to my followers
--        * Publish an Accept to:
--          * The object project + followers
--          * Add sender + followers
--          * My followers
--        * Record my Accept in the Source record
--
--      * If the target is my resources list:
--        * Verify the object is a resource (i.e. project or component), find in DB/HTTP
--        * Verify the Add is authorized
--        * Verify it's not already an active resource of mine
--        * Verify it's not already in an Origin-Us process where I saw the Add
--          and sent my Accept
--        * Verify it's not already in an Origin-Them process, where I saw the
--          Add and the resource's Accept
--        * Insert the Add to my inbox
--        * Create a Effort record in DB
--        * Forward the Add to my followers
--        * Publish an Accept to:
--          * The object resource + followers
--          * Add sender + followers
--          * My followers
--        * Record my Accept in the Effort record
--
--      * If I'm the object, being added to someone's parents/children list:
--        * Verify the target is a project, find in DB/HTTP
--        * Verify it's not already an active parent of mine
--        * Verify it's not already an active child of mine
--        * Verify it's not already in an Origin-Us process where I saw the Add
--          and sent my Accept
--        * Verify it's not already in an Origin-Them process, where I saw the
--          Add and the potential parent/child's Accept
--        * Insert the Add to my inbox
--        * Create a Source/Dest record in DB
--        * Forward the Add to my followers
--
--      * If I'm the object, being added to some resource's teams list:
--        * Verify the target is a non-team resource's teams list, find in DB/HTTP
--        * Verify it's not already an active resource of mine
--        * Verify it's not already in an Origin-Us process where I saw the Add
--          and sent my Accept
--        * Verify it's not already in an Origin-Them process, where I saw the
--          Add and the potential resource's Accept
--        * Insert the Add to my inbox
--        * Create an Effort record in DB
--        * Forward the Add to my followers
--
--      * Otherwise, error
groupAdd
    :: UTCTime
    -> GroupId
    -> Verse
    -> AP.Add URIMode
    -> ActE (Text, Act (), Next)
groupAdd now groupID (Verse authorIdMsig body) add = do

    let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
    (object, target, role) <- parseAdd author add
    --unless (role == AP.RoleAdmin) $
    --    throwE "Add role isn't admin"
    case (target, object) of
        (Left (ATGroupChildren j), _) | j == groupID ->
            addChildActive role object
        (Left (ATGroupParents j), _) | j == groupID ->
            addParentActive role object
        (Left (ATGroupEfforts j), _) | j == groupID ->
            addResourceActive role object
        (_, Left (LocalActorGroup j)) | j == groupID ->
            case target of
                Left (ATGroupParents j) | j /= groupID ->
                    addChildPassive role $ Left j
                Left (ATGroupChildren j) | j /= groupID ->
                    addParentPassive role $ Left j
                Left at | isJust $ addTargetResourceTeams at ->
                    addResourcePassive role $ Left $ fromJust $ addTargetResourceTeams at
                Right (ObjURI h luColl) -> do
                    -- NOTE this is HTTP GET done synchronously in the activity
                    -- handler
                    manager <- asksEnv envHttpManager
                    c <- AP.fetchAPID_T manager (AP.collectionId :: AP.Collection FedURI URIMode -> LocalURI) h luColl
                    lu <- fromMaybeE (AP.collectionContext c) "No context"
                    rwc <- AP.fetchRWC_T manager h lu
                    AP.Actor l d <-
                        case AP.rwcResource rwc of
                            AP.ResourceActor a -> pure a
                            AP.ResourceChild _ _ -> throwE "Add.target remote ResourceChild"
                    let typ = AP.actorType d
                    if typ == AP.ActorTypeTeam && Just luColl == AP.rwcSubteams rwc
                        then addParentPassive role $ Right $ ObjURI h lu
                    else if typ == AP.ActorTypeTeam && Just luColl == AP.rwcParentsOrProjects rwc
                        then addChildPassive role $ Right $ ObjURI h lu
                    else if AP.actorTypeIsResourceNT typ && Just luColl == AP.rwcTeams rwc
                        then addResourcePassive role $ Right $ ObjURI h lu
                    else throwE "Weird collection situation"
                _ -> throwE "I'm being added somewhere irrelevant"
        _ -> throwE "This Add isn't for me"

    where

    prepareAccept childDB = do
        encodeRouteHome <- getEncodeRouteHome

        audAdder <- makeAudSenderWithFollowers authorIdMsig
        audChild <-
            case childDB of
                Left (Entity j _) -> do
                    jh <- encodeKeyHashid j
                    return $ AudLocal [LocalActorGroup jh] []
                Right (ObjURI h lu, Entity _ ra) ->
                    return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
        audMe <-
            AudLocal [] . pure . LocalStageGroupFollowers <$>
                encodeKeyHashid groupID
        uAdd <- lift $ getActivityURI authorIdMsig

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audAdder, audChild, audMe]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uAdd]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uAdd
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    addParentActive role parent = do

        unless (role == AP.RoleAdmin) $
            throwE "Add role isn't admin"

        -- If parent is local, find it in our DB
        -- If parent is remote, HTTP GET it, verify it's an actor of Group
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        parentDB <-
            bitraverse
                (\case
                    LocalActorGroup j -> withDBExcept $ getEntityE j "Parent not found in DB"
                    _ -> throwE "Local proposed parent of non-group type"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Parent @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Parent isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote parent type isn't Group"
                            return (u, actor)
                )
                parent
        let parentDB' = second (entityKey . snd) parentDB

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the sender is authorized by me to add a parent
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceGroup groupID)
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            -- Verify the object isn't a child of mine
            verifyNoEnabledGroupChildren groupID parentDB'

            -- Verify the object isn't already a parent of mine, and that no
            -- Source record is already in Add-Accept state
            verifyNoStartedGroupParents groupID parentDB'

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Source record in DB
                acceptID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                insertSource parentDB' addDB acceptID

                -- Prepare forwarding the Add to my followers
                sieve <- do
                    groupHash <- encodeKeyHashid groupID
                    return $ makeRecipientSet [] [LocalStageGroupFollowers groupHash]

                -- Prepare an Accept activity and insert to my outbox
                accept@(actionAccept, _, _, _) <- prepareAccept parentDB
                _luAccept <- updateOutboxItem' (LocalActorGroup groupID) acceptID actionAccept

                return (groupActor group, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (groupActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                forwardActivity
                    authorIdMsig body (LocalActorGroup groupID) groupActorID sieve
                lift $ sendActivity
                    (LocalActorGroup groupID) groupActorID localRecipsAccept
                    remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "Recorded a parent-group-in-progress, forwarded the Add, sent an Accept"

        where

        insertSource topicDB addDB acceptID = do
            sourceID <- insert $ Source AP.RoleAdmin
            holderID <- insert $ SourceHolderGroup sourceID groupID
            case topicDB of
                Left (Entity j _) -> do
                    localID <- insert $ SourceTopicLocal sourceID
                    insert_ $ SourceTopicGroup holderID localID j
                Right a ->
                    insert_ $ SourceTopicRemote sourceID a
            usID <- insert $ SourceOriginUs sourceID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ SourceUsGestureLocal usID addID
                Right (author, _, addID) ->
                    insert_ $ SourceUsGestureRemote usID (remoteAuthorId author) addID

            insert_ $ SourceUsAccept usID acceptID

    addChildActive role child = do

        unless (role == AP.RoleAdmin) $
            throwE "Add role isn't admin"

        -- If child is local, find it in our DB
        -- If child is remote, HTTP GET it, verify it's an actor of Group
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        childDB <-
            bitraverse
                (\case
                    LocalActorGroup j -> withDBExcept $ getEntityE j "Child not found in DB"
                    _ -> throwE "Local proposed child of non-group type"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Child @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Child isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote child type isn't Group"
                            return (u, actor)
                )
                child
        let childDB' = second (entityKey . snd) childDB

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the sender is authorized by me to add a child
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceGroup groupID)
            AP.RoleTriage

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            -- Verify the object isn't a parent of mine
            verifyNoEnabledGroupParents groupID childDB'

            -- Verify the object isn't already a child of mine, and that no
            -- Dest record is already in Add-Accept state
            verifyNoStartedGroupChildren groupID childDB'

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Dest record in DB
                acceptID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                insertDest childDB' addDB acceptID

                -- Prepare forwarding the Add to my followers
                sieve <- do
                    groupHash <- encodeKeyHashid groupID
                    return $ makeRecipientSet [] [LocalStageGroupFollowers groupHash]

                -- Prepare an Accept activity and insert to my outbox
                accept@(actionAccept, _, _, _) <- prepareAccept childDB
                _luAccept <- updateOutboxItem' (LocalActorGroup groupID) acceptID actionAccept

                return (groupActor group, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (groupActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                forwardActivity
                    authorIdMsig body (LocalActorGroup groupID) groupActorID sieve
                lift $ sendActivity
                    (LocalActorGroup groupID) groupActorID localRecipsAccept
                    remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "Recorded a child-group-in-progress, forwarded the Add, sent an Accept"

        where

        insertDest topicDB addDB acceptID = do
            destID <- insert $ Dest AP.RoleAdmin
            holderID <- insert $ DestHolderGroup destID groupID
            case topicDB of
                Left (Entity j _) -> do
                    localID <- insert $ DestTopicLocal destID
                    insert_ $ DestTopicGroup holderID localID j
                Right a ->
                    insert_ $ DestTopicRemote destID a
            insert_ $ DestOriginUs destID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ DestUsGestureLocal destID addID
                Right (author, _, addID) ->
                    insert_ $ DestUsGestureRemote destID (remoteAuthorId author) addID

            insert_ $ DestUsAccept destID acceptID

    addParentPassive role parent = do

        unless (role == AP.RoleAdmin) $
            throwE "Add role isn't admin"

        -- If parent is local, find it in our DB
        -- If parent is remote, HTTP GET it, verify it's an actor of Group
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        parentDB <-
            bitraverse
                (\ j -> withDBExcept $ getEntityE j "Parent not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Parent @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Parent isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote parent type isn't Group"
                            return (u, actor)
                )
                parent
        let parentDB' = second (entityKey . snd) parentDB

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            -- Verify the target isn't a child of mine
            verifyNoEnabledGroupChildren groupID parentDB'

            -- Verify the target isn't already a parent of mine, and that no
            -- Source record is already in Add-Accept state
            verifyNoStartedGroupParents groupID parentDB'

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Source record in DB
                insertSource parentDB' addDB

                -- Prepare forwarding the Add to my followers
                sieve <- do
                    groupHash <- encodeKeyHashid groupID
                    return $ makeRecipientSet [] [LocalStageGroupFollowers groupHash]

                return (groupActor group, sieve, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (groupActorID, sieve, inboxItemID) -> do
                forwardActivity
                    authorIdMsig body (LocalActorGroup groupID) groupActorID sieve
                doneDB inboxItemID "Recorded a parent-group-in-progress, forwarded the Add"

        where

        insertSource topicDB addDB = do
            sourceID <- insert $ Source AP.RoleAdmin
            holderID <- insert $ SourceHolderGroup sourceID groupID
            case topicDB of
                Left (Entity j _) -> do
                    localID <- insert $ SourceTopicLocal sourceID
                    insert_ $ SourceTopicGroup holderID localID j
                Right a ->
                    insert_ $ SourceTopicRemote sourceID a
            themID <- insert $ SourceOriginThem sourceID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ SourceThemGestureLocal themID addID
                Right (author, _, addID) ->
                    insert_ $ SourceThemGestureRemote themID (remoteAuthorId author) addID

    addChildPassive role child = do

        unless (role == AP.RoleAdmin) $
            throwE "Add role isn't admin"

        -- If child is local, find it in our DB
        -- If child is remote, HTTP GET it, verify it's an actor of Group
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        childDB <-
            bitraverse
                (\ j -> withDBExcept $ getEntityE j "Child not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Child @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Child isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote child type isn't Group"
                            return (u, actor)
                )
                child
        let childDB' = second (entityKey . snd) childDB

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            -- Verify the object isn't a parent of mine
            verifyNoEnabledGroupParents groupID childDB'

            -- Verify the object isn't already a child of mine, and that no
            -- Dest record is already in Add-Accept state
            verifyNoStartedGroupChildren groupID childDB'

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Dest record in DB
                insertDest childDB' addDB

                -- Prepare forwarding the Add to my followers
                sieve <- do
                    groupHash <- encodeKeyHashid groupID
                    return $ makeRecipientSet [] [LocalStageGroupFollowers groupHash]

                return (groupActor group, sieve, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (groupActorID, sieve, inboxItemID) -> do
                forwardActivity
                    authorIdMsig body (LocalActorGroup groupID) groupActorID sieve
                doneDB inboxItemID "Recorded a child-group-in-progress, forwarded the Add"

        where

        insertDest topicDB addDB = do
            destID <- insert $ Dest AP.RoleAdmin
            holderID <- insert $ DestHolderGroup destID groupID
            case topicDB of
                Left (Entity j _) -> do
                    localID <- insert $ DestTopicLocal destID
                    insert_ $ DestTopicGroup holderID localID j
                Right a ->
                    insert_ $ DestTopicRemote destID a
            themID <- insert $ DestOriginThem destID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ DestThemGestureLocal themID addID
                Right (author, _, addID) ->
                    insert_ $ DestThemGestureRemote themID (remoteAuthorId author) addID

    addResourceActive role resource = do

        -- If resource is local, find it in our DB
        -- If resource is remote, HTTP GET it, verify it's an actor of Group
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        resourceDB <-
            bitraverse
                (\ la ->
                    case resourceToNG =<< actorToResource la of
                        Just ng -> withDBExcept $ getLocalResourceEntityE (resourceFromNG ng) "Resource not found in DB"
                        Nothing -> throwE "Local proposed resource of non-resource type"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Resource @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Resource isn't an actor"
                        Right (Just actor) -> do
                            if AP.actorTypeIsResourceNT $ remoteActorType $ entityVal actor
                                then pure ()
                                else throwE "Remote resource type isn't a resource"
                            return (u, actor)
                )
                resource
        let resourceDB' = bimap localResourceID (entityKey . snd) resourceDB

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the sender is authorized by me to add a resource
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceGroup groupID)
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            -- Verify the object isn't already a resource of mine, and that no
            -- Effort record is already in Add-Accept state
            verifyNoStartedGroupResources groupID resourceDB'

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create an Effort record in DB
                acceptID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                insertEffort resourceDB' addDB acceptID

                -- Prepare forwarding the Add to my followers
                sieve <- do
                    groupHash <- encodeKeyHashid groupID
                    return $ makeRecipientSet [] [LocalStageGroupFollowers groupHash]

                -- Prepare an Accept activity and insert to my outbox
                accept@(actionAccept, _, _, _) <- prepareAccept resourceDB
                _luAccept <- updateOutboxItem' (LocalActorGroup groupID) acceptID actionAccept

                return (groupActor group, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (groupActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                forwardActivity
                    authorIdMsig body (LocalActorGroup groupID) groupActorID sieve
                lift $ sendActivity
                    (LocalActorGroup groupID) groupActorID localRecipsAccept
                    remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "[Resource-active] Recorded a resource-in-progress, forwarded the Add, sent an Accept"

        where

        insertEffort topicDB addDB acceptID = do
            effortID <- insert $ Effort role groupID
            case topicDB of
                Left r -> insert_ $ EffortTopicLocal effortID r
                Right a -> insert_ $ EffortTopicRemote effortID a
            usID <- insert $ EffortOriginUs effortID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ EffortUsGestureLocal usID addID
                Right (author, _, addID) ->
                    insert_ $ EffortUsGestureRemote usID (remoteAuthorId author) addID

            insert_ $ EffortUsAccept usID acceptID

        prepareAccept resourceDB = do
            encodeRouteHome <- getEncodeRouteHome

            audAdder <- makeAudSenderWithFollowers authorIdMsig
            audResource <-
                case resourceDB of
                    Left r -> do
                        a <- hashLocalActor $ resourceToActor $ bmap entityKey r
                        return $ AudLocal [a] [localActorFollowers a]
                    Right (ObjURI h lu, Entity _ ra) ->
                        return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
            audMe <-
                AudLocal [] . pure . LocalStageGroupFollowers <$>
                    encodeKeyHashid groupID
            uAdd <- lift $ getActivityURI authorIdMsig

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audAdder, audResource, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uAdd]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uAdd
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    addResourcePassive role resource = do

        -- If resource is local, find it in our DB
        -- If resource is remote, HTTP GET it, verify it's an actor of Group
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        resourceDB <-
            bitraverse
                (\ ng ->
                    withDBExcept $
                        getLocalResourceEntityE (resourceFromNG ng) "Resource not found in DB"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Resource @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Resource isn't an actor"
                        Right (Just actor) -> do
                            if AP.actorTypeIsResourceNT $ remoteActorType $ entityVal actor
                                then pure ()
                                else throwE "Remote resource type isn't a resource"
                            return (u, actor)
                )
                resource
        let resourceDB' = bimap localResourceID (entityKey . snd) resourceDB

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            -- Verify the target isn't already a resource of mine, and that no
            -- Effort record is already in Add-Accept state
            verifyNoStartedGroupResources groupID resourceDB'

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create an Effort record in DB
                insertEffort resourceDB' addDB

                -- Prepare forwarding the Add to my followers
                sieve <- do
                    groupHash <- encodeKeyHashid groupID
                    return $ makeRecipientSet [] [LocalStageGroupFollowers groupHash]

                return (groupActor group, sieve, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (groupActorID, sieve, inboxItemID) -> do
                forwardActivity
                    authorIdMsig body (LocalActorGroup groupID) groupActorID sieve
                doneDB inboxItemID "[Resource-passive] Recorded a resource-in-progress, forwarded the Add"

        where

        insertEffort topicDB addDB = do
            effortID <- insert $ Effort role groupID
            case topicDB of
                Left r -> insert_ $ EffortTopicLocal effortID r
                Right a -> insert_ $ EffortTopicRemote effortID a
            themID <- insert $ EffortOriginThem effortID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ EffortThemGestureLocal themID addID
                Right (author, _, addID) ->
                    insert_ $ EffortThemGestureRemote themID (remoteAuthorId author) addID

-- Meaning: An actor accepted something
-- Behavior:
--  * Is it an Invite to be a collaborator in me?
--      * Verify the Accept is by the Invite target
--      * Verify the Collab isn't enabled yet
--      * Insert the Accept to my inbox
--      * Record the Accept and enable the Collab in DB
--      * Forward the Accept to my followers
--      * Send a regular collaborator-Grant
--          * To: Accepter (i.e. Invite target)
--          * CC: Invite sender, Accepter's followers, my followers
--
--  * Is it a Join to be a collaborator in me?
--      * Verify the Accept is authorized
--      * Verify the Collab isn't enabled yet
--      * Insert the Accept to my inbox
--      * Record the Accept and enable the Collab in DB
--      * Forward the Accept to my followers
--      * Send a regular collaborator-Grant
--          * To: Join sender
--          * CC: Accept sender, Join sender's followers, my followers
--
--  * Give me a new parent active   SourceOriginUs
--      * Verify we haven't yet seen parent's Accept
--      * Insert the Accept to my inbox
--      * If sender is the parent, record the Accept into the Source record
--          * Prepare to send degelator-Grant
--          * Otherwise nothing to do
--      * Forward the Accept to my followers
--      * Possibly send a Grant:
--              * If sender is the parent
--              * delegator-Grant
--              * To: Parent
--              * CC:
--                  - Parent's followers
--                  - My followers
--
--  * Give me a new parent passive  SourceOriginThem
--      * Option 1: We haven't seen parent's Accept yet
--          * Verify sender is the parent
--      * Option 2: We saw it, but not my collaborator's Accept
--          * Verify the Accept is authorized
--      * Otherwise respond with error, no Accept is needed
--      * Insert the Accept to my inbox
--      * Option 1: Record parent's Accept in Source record
--      * Option 2: Record my collaborator's Accept
--          * Prepare to send delegator-Grant
--      * Forward the Accept to my followers
--      * Possibly send a Grant:
--          * In option 2
--          * delegator-Grant
--          * To: Parent
--          * CC:
--              - Parent's followers
--              - My followers
--              - The Accept sender (my collaborator)
--
--  * Give me a new child active DestOriginUs
--      * Respond with error, we aren't supposed to get any Accept
--
--  * Give me a new child passive DestOriginThem
--      * Option 1: I haven't yet seen child's Accept
--          * Verify sender is the child
--      * Option 2: I saw it, but not my collaborator's Accept
--          * Verify the accept is authorized
--      * Otherwise respond with error, no Accept is needed
--      * Insert the Accept to my inbox
--      * Option 1: Record child's Accept in the Dest record
--      * Option 2: Record my collaborator's Accept in the Dest record
--          * Prepare to send my own Accept
--      * Forward the Accept to my followers
--      * Possibly send an Accept:
--          * In option 2
--          * Object: The Add
--          * Fulfills: My collaborator's Accept
--          * To: Child
--          * CC:
--              - Child's followers
--              - My followers
--              - The Accept sender (my collaborator)
--
-- * Remove-Parent-Passive mode:
--      * Verify the Source is enabled
--      * Verify the sender is the child
--      * Delete the entire Source record
--      * Forward the Accept to my followers
--      * Send a Revoke on the delegator-Grant I had for B:
--          * To: Actor B
--          * CC: Actor A, B's followers, my followers
--      * Send a Revoke on every extention-Grant I extended on every
--        delegation Grant I got from B
--          * To: The parent/collaborator/team to whom I'd sent the Grant
--          * CC: -
--
--  * Give me a new resource active   EffortOriginUs
--      * Verify we haven't yet seen resource's Accept
--      * Insert the Accept to my inbox
--      * If sender is the resource, record the Accept into the Effort record
--          * Prepare to send degelator-Grant
--          * Otherwise nothing to do
--      * Forward the Accept to my followers
--      * Possibly send a Grant:
--              * If sender is the resource
--              * delegator-Grant
--              * To: Resource
--              * CC:
--                  - Resource's followers
--                  - My followers
--
--  * Give me a new resource passive  EffortOriginThem
--      * Option 1: We haven't seen resource's Accept yet
--          * Verify sender is the resource
--      * Option 2: We saw it, but not my collaborator's Accept
--          * Verify the Accept is authorized
--      * Otherwise respond with error, no Accept is needed
--      * Insert the Accept to my inbox
--      * Option 1: Record resource's Accept in Effort record
--      * Option 2: Record my collaborator's Accept
--          * Prepare to send delegator-Grant
--      * Forward the Accept to my followers
--      * Possibly send a Grant:
--          * In option 2
--          * delegator-Grant
--          * To: Resource
--          * CC:
--              - Resource's followers
--              - My followers
--              - The Accept sender (my collaborator)
--
-- * Remove-Resource-Passive mode:
--      * Verify the Effort is enabled
--      * Verify the sender is the resource
--      * Delete the entire Effort record
--      * Forward the Accept to my followers
--      * Send a Revoke on the delegator-Grant I had for B:
--          * To: Actor B
--          * CC: Actor A, B's followers, my followers
--      * Send a Revoke on every extention-Grant I extended on every
--        delegation Grant I got from B
--          * To: The child/member to whom I'd sent the Grant
--          * CC: -
groupAccept
    :: UTCTime
    -> GroupId
    -> Verse
    -> AP.Accept URIMode
    -> ActE (Text, Act (), Next)
groupAccept now groupID (Verse authorIdMsig body) accept = do

    -- Check input
    acceptee <- parseAccept accept

    collabOrComp_or_child <- withDBExcept $ do

        myInboxID <- lift $ do
            group <- getJust groupID
            actor <- getJust $ groupActor group
            return $ actorInbox actor

        -- Find the accepted activity in our DB
        accepteeDB <- do
            a <- getActivity acceptee
            fromMaybeE a "Can't find acceptee in DB"

        -- See if the accepted activity is an Invite or Join where my collabs
        -- URI is the resource, grabbing the Collab record from our DB,
        let adapt = maybe (Right Nothing) (either Left (Right . Just))
        maybeCollab <-
            ExceptT $ fmap adapt $ runMaybeT $
                runExceptT (Left . Left <$> tryInviteCollab accepteeDB) <|>
                runExceptT (Left . Left <$> tryJoinCollab accepteeDB) <|>
                runExceptT (Left . Right <$> tryAddResourceActive accepteeDB) <|>
                runExceptT (Left . Right <$> tryAddResourcePassive accepteeDB) <|>
                runExceptT (Right . Left <$> tryAddChildActive accepteeDB) <|>
                runExceptT (Right . Left <$> tryAddChildPassive accepteeDB) <|>
                runExceptT (Right . Left <$> tryAddParentActive accepteeDB) <|>
                runExceptT (Right . Left <$> tryAddParentPassive accepteeDB) <|>
                runExceptT (Right . Right . Left <$> tryRemoveParent myInboxID accepteeDB) <|>
                runExceptT (Right . Right . Right <$> tryRemoveResource myInboxID accepteeDB)
        fromMaybeE
            maybeCollab
            "Accepted activity isn't an Invite/Join/Add/Remove I'm aware of"

    case collabOrComp_or_child of
        Left (Left collab) -> addCollab collab
        Left (Right resource) -> addResource resource
        Right (Left cp) -> addChildParent cp
        Right (Right (Left parent)) -> removeParent parent
        Right (Right (Right resource)) -> removeResource resource

    where

    verifyCollabTopic collabID = do
        topic <- lift $ getCollabTopic collabID
        unless (LocalResourceGroup groupID == topic) $
            throwE "Accept object is an Invite/Join for some other resource"

    verifyInviteCollabTopic fulfillsID = do
        collabID <- lift $ collabFulfillsInviteCollab <$> getJust fulfillsID
        verifyCollabTopic collabID
        return collabID

    verifyJoinCollabTopic fulfillsID = do
        collabID <- lift $ collabFulfillsJoinCollab <$> getJust fulfillsID
        verifyCollabTopic collabID
        return collabID

    tryInviteCollab (Left (actorByKey, _actorEntity, itemID)) = do
        fulfillsID <-
            lift $ collabInviterLocalCollab <$>
                MaybeT (getValBy $ UniqueCollabInviterLocalInvite itemID)
        collabID <-
            ExceptT $ lift $ runExceptT $ verifyInviteCollabTopic fulfillsID
        return (collabID, Left fulfillsID, Left actorByKey)
    tryInviteCollab (Right remoteActivityID) = do
        CollabInviterRemote fulfillsID actorID _ <-
            lift $ MaybeT $ getValBy $
                UniqueCollabInviterRemoteInvite remoteActivityID
        collabID <-
            ExceptT $ lift $ runExceptT $ verifyInviteCollabTopic fulfillsID
        sender <- lift $ lift $ do
            actor <- getJust actorID
            (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (collabID, Left fulfillsID, Right sender)

    tryJoinCollab (Left (actorByKey, _actorEntity, itemID)) = do
        fulfillsID <-
            lift $ collabRecipLocalJoinFulfills <$>
                MaybeT (getValBy $ UniqueCollabRecipLocalJoinJoin itemID)
        collabID <-
            ExceptT $ lift $ runExceptT $ verifyJoinCollabTopic fulfillsID
        return (collabID, Right fulfillsID, Left actorByKey)
    tryJoinCollab (Right remoteActivityID) = do
        CollabRecipRemoteJoin recipID fulfillsID _ <-
            lift $ MaybeT $ getValBy $
                UniqueCollabRecipRemoteJoinJoin remoteActivityID
        collabID <-
            ExceptT $ lift $ runExceptT $ verifyJoinCollabTopic fulfillsID
        joiner <- lift $ lift $ do
            remoteActorID <- collabRecipRemoteActor <$> getJust recipID
            actor <- getJust remoteActorID
            (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (collabID, Right fulfillsID, Right joiner)

    verifySourceHolder :: SourceId -> MaybeT ActDB ()
    verifySourceHolder sourceID = do
        SourceHolderGroup _ j <- MaybeT $ getValBy $ UniqueSourceHolderGroup sourceID
        guard $ j == groupID

    tryAddParentActive' usID = do
        SourceOriginUs sourceID <- lift . lift $ getJust usID
        lift $ verifySourceHolder sourceID
        topic <- do
            t <- lift . lift $ getSourceTopic sourceID
            bitraverse
                (\ (l, k) ->
                    case k of
                        Right j -> pure (l, j)
                        Left _ -> error "Group Source topic is a Project, impossible"
                )
                pure
                t
        return $ Left (sourceID, topic, Left ())

    tryAddParentActive (Left (_actorByKey, _actorEntity, itemID)) = do
        SourceUsGestureLocal usID _ <-
            lift $ MaybeT $ getValBy $ UniqueSourceUsGestureLocalAdd itemID
        tryAddParentActive' usID
    tryAddParentActive (Right remoteActivityID) = do
        SourceUsGestureRemote usID _ _ <-
            lift $ MaybeT $ getValBy $ UniqueSourceUsGestureRemoteAdd remoteActivityID
        tryAddParentActive' usID

    tryAddParentPassive' themID = do
        SourceOriginThem sourceID <- lift . lift $ getJust themID
        lift $ verifySourceHolder sourceID
        topic <- do
            t <- lift . lift $ getSourceTopic sourceID
            bitraverse
                (\ (l, k) ->
                    case k of
                        Right j -> pure (l, j)
                        Left _ -> error "Group Source topic is a Project, impossible"
                )
                pure
                t
        return $ Left (sourceID, topic, Right ())

    tryAddParentPassive (Left (_actorByKey, _actorEntity, itemID)) = do
        SourceThemGestureLocal themID _ <-
            lift $ MaybeT $ getValBy $ UniqueSourceThemGestureLocalAdd itemID
        tryAddParentPassive' themID
    tryAddParentPassive (Right remoteActivityID) = do
        SourceThemGestureRemote themID _ _ <-
            lift $ MaybeT $ getValBy $ UniqueSourceThemGestureRemoteAdd remoteActivityID
        tryAddParentPassive' themID

    verifyDestHolder :: DestId -> MaybeT ActDB ()
    verifyDestHolder destID = do
        DestHolderGroup _ j <- MaybeT $ getValBy $ UniqueDestHolderGroup destID
        guard $ j == groupID

    tryAddChildActive' destID = do
        usID <- lift $ MaybeT $ getKeyBy $ UniqueDestOriginUs destID
        lift $ verifyDestHolder destID
        topic <- do
            t <- lift . lift $ getDestTopic destID
            bitraverse
                (\ (l, k) ->
                    case k of
                        Right j -> pure (l, j)
                        Left _ -> error "Group Dest topic is a Project, impossible"
                )
                pure
                t
        return $ Right (destID, topic, Left ())

    tryAddChildActive (Left (_actorByKey, _actorEntity, itemID)) = do
        DestUsGestureLocal destID _ <-
            lift $ MaybeT $ getValBy $ UniqueDestUsGestureLocalActivity itemID
        tryAddChildActive' destID
    tryAddChildActive (Right remoteActivityID) = do
        DestUsGestureRemote destID _ _ <-
            lift $ MaybeT $ getValBy $ UniqueDestUsGestureRemoteActivity remoteActivityID
        tryAddChildActive' destID

    tryAddChildPassive' themID = do
        DestOriginThem destID <- lift . lift $ getJust themID
        lift $ verifyDestHolder destID
        topic <- do
            t <- lift . lift $ getDestTopic destID
            bitraverse
                (\ (l, k) ->
                    case k of
                        Right j -> pure (l, j)
                        Left _ -> error "Group Dest topic is a Project, impossible"
                )
                pure
                t
        return $ Right (destID, topic, Right themID)

    tryAddChildPassive (Left (_actorByKey, _actorEntity, itemID)) = do
        DestThemGestureLocal themID _ <-
            lift $ MaybeT $ getValBy $ UniqueDestThemGestureLocalAdd itemID
        tryAddChildPassive' themID
    tryAddChildPassive (Right remoteActivityID) = do
        DestThemGestureRemote themID _ _ <-
            lift $ MaybeT $ getValBy $ UniqueDestThemGestureRemoteAdd remoteActivityID
        tryAddChildPassive' themID

    tryRemoveParent' itemID = do
        SourceRemove sendID _ <-
            lift $ MaybeT $ getValBy $ UniqueSourceRemove itemID
        SourceUsSendDelegator sourceID grantID <- lift $ lift $ getJust sendID
        lift $ verifySourceHolder sourceID
        topic <- do
            t <- lift . lift $ getSourceTopic sourceID
            bitraverse
                (\ (l, k) ->
                    case k of
                        Right j -> pure (l, j)
                        Left _ -> error "Group Source topic is a Project, impossible"
                )
                pure
                t
        return (sourceID, sendID, grantID, topic)

    tryRemoveParent inboxID (Left (_actorByKey, _actorEntity, itemID)) = do
        InboxItemLocal _ _ i <-
            lift $ MaybeT $ getValBy $ UniqueInboxItemLocal inboxID itemID
        tryRemoveParent' i
    tryRemoveParent inboxID (Right remoteActivityID) = do
        InboxItemRemote _ _ i <-
            lift $ MaybeT $ getValBy $ UniqueInboxItemRemote inboxID remoteActivityID
        tryRemoveParent' i

    verifyEffortHolder :: EffortId -> MaybeT ActDB ()
    verifyEffortHolder effortID = do
        Effort _ g <- lift $ getJust effortID
        guard $ g == groupID

    tryAddResourceActive' usID = do
        EffortOriginUs effortID <- lift . lift $ getJust usID
        lift $ verifyEffortHolder effortID
        topic <- lift . lift $ getEffortTopic effortID
        return (effortID, topic, Left ())

    tryAddResourceActive (Left (_actorByKey, _actorEntity, itemID)) = do
        EffortUsGestureLocal usID _ <-
            lift $ MaybeT $ getValBy $ UniqueEffortUsGestureLocalAdd itemID
        tryAddResourceActive' usID
    tryAddResourceActive (Right remoteActivityID) = do
        EffortUsGestureRemote usID _ _ <-
            lift $ MaybeT $ getValBy $ UniqueEffortUsGestureRemoteAdd remoteActivityID
        tryAddResourceActive' usID

    tryAddResourcePassive' themID = do
        EffortOriginThem effortID <- lift . lift $ getJust themID
        lift $ verifyEffortHolder effortID
        topic <- lift . lift $ getEffortTopic effortID
        return (effortID, topic, Right ())

    tryAddResourcePassive (Left (_actorByKey, _actorEntity, itemID)) = do
        EffortThemGestureLocal themID _ <-
            lift $ MaybeT $ getValBy $ UniqueEffortThemGestureLocalAdd itemID
        tryAddResourcePassive' themID
    tryAddResourcePassive (Right remoteActivityID) = do
        EffortThemGestureRemote themID _ _ <-
            lift $ MaybeT $ getValBy $ UniqueEffortThemGestureRemoteAdd remoteActivityID
        tryAddResourcePassive' themID

    tryRemoveResource' itemID = do
        EffortRemove sendID _ <-
            lift $ MaybeT $ getValBy $ UniqueEffortRemove itemID
        EffortUsSendDelegator effortID grantID <- lift $ lift $ getJust sendID
        lift $ verifyEffortHolder effortID
        topic <- lift . lift $ getEffortTopic effortID
        return (effortID, sendID, grantID, topic)

    tryRemoveResource inboxID (Left (_actorByKey, _actorEntity, itemID)) = do
        InboxItemLocal _ _ i <-
            lift $ MaybeT $ getValBy $ UniqueInboxItemLocal inboxID itemID
        tryRemoveResource' i
    tryRemoveResource inboxID (Right remoteActivityID) = do
        InboxItemRemote _ _ i <-
            lift $ MaybeT $ getValBy $ UniqueInboxItemRemote inboxID remoteActivityID
        tryRemoveResource' i

    componentIsAuthor ident =
        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        in  author == bimap (resourceToActor . componentResource . snd) snd ident

    theyIsAuthor :: Either (a, GroupId) (b, RemoteActorId) -> Bool
    theyIsAuthor ident =
        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        in  author == bimap (LocalActorGroup . snd) snd ident

    theyIsAuthor' :: Either (a, LocalResourceBy Key) (b, RemoteActorId) -> Bool
    theyIsAuthor' ident =
        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        in  author == bimap (resourceToActor . snd) snd ident

    addCollab (collabID, fulfills, inviterOrJoiner) = do

        collab <-
            bitraverse

                -- If accepting an Invite, find the Collab recipient and verify
                -- it's the sender of the Accept
                (\ fulfillsID -> withDBExcept $ do
                    recip <-
                        lift $
                        requireEitherAlt
                            (getBy $ UniqueCollabRecipLocal collabID)
                            (getBy $ UniqueCollabRecipRemote collabID)
                            "Found Collab with no recip"
                            "Found Collab with multiple recips"
                    case (recip, authorIdMsig) of
                        (Left (Entity crlid crl), Left (LocalActorPerson personID, _, _))
                            | collabRecipLocalPerson crl == personID ->
                                return (fulfillsID, Left crlid)
                        (Right (Entity crrid crr), Right (author, _, _))
                            | collabRecipRemoteActor crr == remoteAuthorId author ->
                                return (fulfillsID, Right crrid)
                        _ -> throwE "Accepting an Invite whose recipient is someone else"
                )

                -- If accepting a Join, verify accepter has permission
                (\ fulfillsID -> do
                    let muCap = AP.activityCapability $ actbActivity body
                    uCap <- fromMaybeE muCap "No capability provided"
                    verifyCapability''
                        uCap
                        authorIdMsig
                        (LocalResourceGroup groupID)
                        AP.RoleAdmin
                    return fulfillsID
                )

                fulfills

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust groupID
                let actorID = groupActor recip
                (actorID,) <$> getJust actorID

            -- In collab mode, verify the Collab isn't already validated
            maybeEnabled <- lift $ getBy $ UniqueCollabEnable collabID
            verifyNothingE maybeEnabled "I already sent a Grant for this Invite/Join"

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                -- Record the Accept and enable the Collab
                (grantID, enableID) <- do
                    case (collab, acceptDB) of
                        (Left (fulfillsID, Left recipID), Left (_, _, acceptID)) -> do
                            maybeAccept <- lift $ insertUnique $ CollabRecipLocalAccept recipID fulfillsID acceptID
                            unless (isJust maybeAccept) $
                                throwE "This Invite already has an Accept by recip"
                        (Left (fulfillsID, Right recipID), Right (_, _, acceptID)) -> do
                            maybeAccept <- lift $ insertUnique $ CollabRecipRemoteAccept recipID fulfillsID acceptID
                            unless (isJust maybeAccept) $
                                throwE "This Invite already has an Accept by recip"
                        (Right fulfillsID, Left (_, _, acceptID)) -> do
                            maybeAccept <- lift $ insertUnique $ CollabApproverLocal fulfillsID acceptID
                            unless (isJust maybeAccept) $
                                throwE "This Join already has an Accept"
                        (Right fulfillsID, Right (author, _, acceptID)) -> do
                            maybeAccept <- lift $ insertUnique $ CollabApproverRemote fulfillsID (remoteAuthorId author) acceptID
                            unless (isJust maybeAccept) $
                                throwE "This Join already has an Accept"
                        _ -> error "groupAccept impossible"
                    grantID <- lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
                    enableID <- lift $ insert $ CollabEnable collabID grantID
                    return (grantID, enableID)

                -- Prepare forwarding of Accept to my followers
                let recipByID = LocalActorGroup groupID
                recipByHash <- hashLocalActor recipByID
                let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

                -- Prepare a regular Grant
                let isInvite = isLeft collab
                grant@(actionGrant, _, _, _) <- lift $ do
                    Collab role _ <- getJust collabID
                    prepareCollabGrant isInvite inviterOrJoiner role
                let recipByKey = LocalActorGroup groupID
                _luGrant <- lift $ updateOutboxItem' recipByKey grantID actionGrant

                return (recipActorID, sieve, grantID, grant, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant), inboxItemID) -> do
                let recipByID = LocalActorGroup groupID
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $
                    sendActivity
                        recipByID recipActorID localRecipsGrant
                        remoteRecipsGrant fwdHostsGrant grantID actionGrant
                doneDB inboxItemID "[Collab mode] Forwarded the Accept and published a Grant"

    -- Add-a-parent mode
    addChildParent (Left (sourceID, topic, mode)) = do

        mode' <-
            bitraverse

                -- Parent-active mode
                -- Verify we haven't yet seen parent's Accept
                (\ () -> do
                    maybeParentAccept <-
                        lift $ withDB $
                        case bimap fst fst topic of
                            Left localID -> (() <$) <$> getBy (UniqueSourceThemAcceptLocal localID)
                            Right remoteID -> (() <$) <$> getBy (UniqueSourceThemAcceptRemote remoteID)
                    verifyNothingE maybeParentAccept "I already saw parent's Accept"
                )

                -- Parent-passive mode
                -- Option 1: We haven't seen parent's Accept yet
                --   * Verify sender is the parent
                -- Option 2: We saw it, but not my collaborator's Accept
                --   * Verify the Accept is authorized
                -- Otherwise respond with error, no Accept is needed
                (\ () -> do
                    (maybeParentAccept, maybeGrant) <-
                        lift $ withDB $ liftA2 (,)
                            (case bimap fst fst topic of
                                Left localID -> (() <$) <$> getBy (UniqueSourceThemAcceptLocal localID)
                                Right remoteID -> (() <$) <$> getBy (UniqueSourceThemAcceptRemote remoteID)
                            )
                            (getBy $ UniqueSourceUsSendDelegator sourceID)
                    case (isJust maybeParentAccept, isJust maybeGrant) of
                        (False, True) -> error "Impossible/bug, didn't see parent's Accept but sent a Grant"
                        (False, False) -> do
                            unless (theyIsAuthor topic) $
                                throwE "The Accept I'm waiting for is from my new parent"
                            return $ Left ()
                        (True, False) -> do
                            let muCap = AP.activityCapability $ actbActivity body
                            uCap <- fromMaybeE muCap "No capability provided"
                            verifyCapability''
                                uCap
                                authorIdMsig
                                (LocalResourceGroup groupID)
                                AP.RoleAdmin
                            return $ Right ()
                        (True, True) -> throwE "Parent already enabled, not needing any further Accept"
                )

                mode

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust groupID
                let actorID = groupActor recip
                (actorID,) <$> getJust actorID

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                idsForGrant <-
                    lift $
                    bitraverse

                        -- Parent-active mode
                        -- If sender is parent, record the Accept into the
                        -- Source record & prepare to send degelator-Grant
                        -- Othrerwise do nothing
                        (\ () ->
                            if theyIsAuthor topic
                                then Just <$> do
                                    case (topic, acceptDB) of
                                        (Left (localID, _), Left (_, _, acceptID)) ->
                                            insert_ $ SourceThemAcceptLocal localID acceptID
                                        (Right (remoteID, _), Right (_, _, acceptID)) ->
                                            insert_ $ SourceThemAcceptRemote remoteID acceptID
                                        _ -> error "groupAccept impossible iv"
                                    grantID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                                    insert_ $ SourceUsSendDelegator sourceID grantID
                                    return grantID
                                else pure Nothing
                        )

                        -- Parent-passive mode
                        (\case

                            -- Getting an Accept from the parent
                            -- Record parent's Accept in Source record
                            Left () -> do
                                case (topic, acceptDB) of
                                    (Left (localID, _), Left (_, _, acceptID)) ->
                                        insert_ $ SourceThemAcceptLocal localID acceptID
                                    (Right (remoteID, _), Right (_, _, acceptID)) ->
                                        insert_ $ SourceThemAcceptRemote remoteID acceptID
                                    _ -> error "groupAccept impossible v"
                                return Nothing

                            -- Getting an Accept from my collaborator
                            -- Record my collaborator's Accept
                            -- Prepare to send delegator-Grant
                            Right () -> Just <$> do
                                {-
                                case (topic, acceptDB) of
                                    (Left (localID, _), Left (_, _, acceptID)) ->
                                        insert_ $ ? localID acceptID
                                    (Right (remoteID, _), Right (_, _, acceptID)) ->
                                        insert_ $ ? remoteID acceptID
                                    _ -> error "groupAccept impossible iv"
                                -}
                                grantID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                                insert_ $ SourceUsSendDelegator sourceID grantID
                                return grantID
                        )

                        mode'


                -- Prepare forwarding of Accept to my followers
                let recipByID = LocalActorGroup groupID
                recipByHash <- hashLocalActor recipByID
                let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

                maybeAct <-
                    case idsForGrant of
                        Left mg -> for mg $ \ grantID -> lift $ do
                            grant@(actionGrant, _, _, _) <-
                                prepareSourceDelegGrant (bimap snd snd topic) False
                            let recipByKey = LocalActorGroup groupID
                            _luGrant <- updateOutboxItem' recipByKey grantID actionGrant
                            return (grantID, grant)

                        Right mg -> for mg $ \ grantID -> lift $ do
                            grant@(actionGrant, _, _, _) <-
                                prepareSourceDelegGrant (bimap snd snd topic) True
                            let recipByKey = LocalActorGroup groupID
                            _luGrant <- updateOutboxItem' recipByKey grantID actionGrant
                            return (grantID, grant)

                return (recipActorID, sieve, maybeAct, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, maybeGrant, inboxItemID) -> do
                let recipByID = LocalActorGroup groupID
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ for_ maybeGrant $ \ (grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant)) ->
                    sendActivity
                        recipByID recipActorID localRecipsGrant
                        remoteRecipsGrant fwdHostsGrant grantID actionGrant
                doneDB inboxItemID "[Parent mode] Forwarded the Accept and maybe published a Grant/Accept"

    -- Add-a-child mode
    addChildParent (Right (destID, topic, mode)) = do

        (themID, mode') <-
            case mode of

                -- Child-active mode
                -- Respond with error, we aren't supposed to get any Accept
                Left () -> throwE "Child-active (DestOriginUs) mode, I'm not expecting any Accept"

                -- Child-passive mode
                -- Option 1: I haven't yet seen child's Accept
                --   * Verify sender is the child
                -- Option 2: I saw it, but not my collaborator's Accept
                --   * Verify the accept is authorized
                -- Otherwise respond with error, no Accept is needed
                Right themID -> (themID,) <$> do
                    (maybeChildAccept, maybeUsGesture) <-
                        lift $ withDB $ liftA2 (,)
                            (case bimap fst fst topic of
                                Left localID -> (() <$) <$> getBy (UniqueDestThemAcceptLocalTopic localID)
                                Right remoteID -> (() <$) <$> getBy (UniqueDestThemAcceptRemoteTopic remoteID)
                            )
                            (do l <- getBy $ UniqueDestUsGestureLocal destID
                                r <- getBy $ UniqueDestUsGestureRemote destID
                                case (isJust l, isJust r) of
                                    (False, False) -> pure Nothing
                                    (False, True) -> pure $ Just ()
                                    (True, False) -> pure $ Just ()
                                    (True, True) -> error "Both DestUsGestureLocal and DestUsGestureRemote"
                            )
                    case (isJust maybeChildAccept, isJust maybeUsGesture) of
                        (False, True) -> error "Impossible/bug, didn't see child's Accept but recorded my collaborator's Accept"
                        (False, False) -> do
                            unless (theyIsAuthor topic) $
                                throwE "The Accept I'm waiting for is from my new child"
                            return $ Left ()
                        (True, False) -> do
                            let muCap = AP.activityCapability $ actbActivity body
                            uCap <- fromMaybeE muCap "No capability provided"
                            verifyCapability''
                                uCap
                                authorIdMsig
                                (LocalResourceGroup groupID)
                                AP.RoleAdmin
                            return $ Right ()
                        (True, True) -> throwE "Just waiting for Grant from child, or already have it, anyway not needing any further Accept"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust groupID
                let actorID = groupActor recip
                (actorID,) <$> getJust actorID

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                idsForGrant <-
                    lift $ case mode' of

                        -- Getting an Accept from the child
                        -- Record child's Accept in the Dest record
                        Left () -> do
                            case (topic, acceptDB) of
                                (Left (localID, _), Left (_, _, acceptID)) ->
                                    insert_ $ DestThemAcceptLocal themID localID acceptID
                                (Right (remoteID, _), Right (_, _, acceptID)) ->
                                    insert_ $ DestThemAcceptRemote themID remoteID acceptID
                                _ -> error "groupAccept impossible v"
                            return Nothing

                        -- Getting an Accept from my collaborator
                        -- Record my collaborator's Accept in the Dest record
                        -- Prepare to send my own Accept
                        Right () -> Just <$> do
                            case acceptDB of
                                Left (_, _, acceptID) ->
                                    insert_ $ DestUsGestureLocal destID acceptID
                                Right (author, _, acceptID) ->
                                    insert_ $ DestUsGestureRemote destID (remoteAuthorId author) acceptID
                            acceptID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                            insert_ $ DestUsAccept destID acceptID
                            return acceptID

                -- Prepare forwarding of Accept to my followers
                let recipByID = LocalActorGroup groupID
                recipByHash <- hashLocalActor recipByID
                let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

                maybeAct <-
                    for idsForGrant $ \ acceptID -> lift $ do
                        accept@(actionAccept, _, _, _) <-
                            prepareDestAccept (bimap snd snd topic)
                        let recipByKey = LocalActorGroup groupID
                        _luAccept <- updateOutboxItem' recipByKey acceptID actionAccept
                        return (acceptID, accept)

                return (recipActorID, sieve, maybeAct, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, maybeGrant, inboxItemID) -> do
                let recipByID = LocalActorGroup groupID
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ for_ maybeGrant $ \ (grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant)) ->
                    sendActivity
                        recipByID recipActorID localRecipsGrant
                        remoteRecipsGrant fwdHostsGrant grantID actionGrant
                doneDB inboxItemID "[Child mode] Forwarded the Accept and maybe published a Grant/Accept"

    prepareCollabGrant isInvite sender role = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audAccepter <- makeAudSenderWithFollowers authorIdMsig
        audApprover <- lift $ makeAudSenderOnly authorIdMsig
        recipHash <- encodeKeyHashid groupID
        let topicByHash = LocalActorGroup recipHash

        senderHash <- bitraverse hashLocalActor pure sender

        uAccepter <- lift $ getActorURI authorIdMsig

        let audience =
                if isInvite
                    then
                        let audInviter =
                                case senderHash of
                                    Left actor -> AudLocal [actor] []
                                    Right (ObjURI h lu, _followers) ->
                                        AudRemote h [lu] []
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audInviter, audAccepter, audTopic]
                    else
                        let audJoiner =
                                case senderHash of
                                    Left actor -> AudLocal [actor] [localActorFollowers actor]
                                    Right (ObjURI h lu, followers) ->
                                        AudRemote h [lu] (maybeToList followers)
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audJoiner, audApprover, audTopic]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [AP.acceptObject accept]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXRole role
                    , AP.grantContext   =
                        encodeRouteHome $ renderLocalActor topicByHash
                    , AP.grantTarget    =
                        if isInvite
                            then uAccepter
                            else case senderHash of
                                Left actor ->
                                    encodeRouteHome $ renderLocalActor actor
                                Right (ObjURI h lu, _) -> ObjURI h lu
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    prepareSourceDelegGrant ident includeAuthor = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        (uSource, audSource) <-
            case ident of
                Left j -> do
                    h <- encodeKeyHashid j
                    return
                        ( encodeRouteHome $ GroupR h
                        , AudLocal [LocalActorGroup h] [LocalStageGroupFollowers h]
                        )
                Right raID -> do
                    ra <- getJust raID
                    u@(ObjURI h lu) <- getRemoteActorURI ra
                    return
                        ( u
                        , AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
                        )
        audAuthor <- lift $ makeAudSenderOnly authorIdMsig
        groupHash <- encodeKeyHashid groupID
        let audGroup = AudLocal [] [LocalStageGroupFollowers groupHash]

            audience =
                if includeAuthor
                    then [audSource, audGroup, audAuthor]
                    else [audSource, audGroup]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [AP.acceptObject accept]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXDelegator
                    , AP.grantContext   = encodeRouteHome $ GroupR groupHash
                    , AP.grantTarget    = uSource
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    prepareDestAccept topic = do
        encodeRouteHome <- getEncodeRouteHome

        audMyCollab <- lift $ makeAudSenderOnly authorIdMsig
        audDest <-
            case topic of
                Left j -> do
                    h <- encodeKeyHashid j
                    return $
                        AudLocal [LocalActorGroup h] [LocalStageGroupFollowers h]
                Right raID -> do
                    ra <- getJust raID
                    ObjURI h lu <- getRemoteActorURI ra
                    return $
                        AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
        audMe <-
            AudLocal [] . pure . LocalStageGroupFollowers <$>
                encodeKeyHashid groupID
        uCollabAccept <- lift $ getActivityURI authorIdMsig
        let uAdd = AP.acceptObject accept

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audMyCollab, audDest, audMe]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uCollabAccept]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uAdd
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    removeParent (sourceID, sendID, grantID, child) = do

        -- Verify the sender is the topic
        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        unless (author == bimap (LocalActorGroup . snd) snd child) $
            throwE "The Accept isn't by the to-be-removed child group"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                -- Grab extension-Grants that I'm about to revoke
                gathers <- selectList [SourceUsGatherSource ==. sendID] []
                leafs <- selectList [SourceUsLeafSource ==. sendID] []

                -- Delete the whole Source record
                deleteWhere [SourceRemoveSend ==. sendID]
                let gatherIDs = map entityKey gathers
                deleteWhere [SourceUsGatherFromLocalGather <-. gatherIDs]
                deleteWhere [SourceUsGatherFromRemoteGather <-. gatherIDs]
                deleteWhere [SourceUsGatherId <-. gatherIDs]
                let leafIDs = map entityKey leafs
                deleteWhere [SourceUsLeafFromLocalLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafFromRemoteLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafToLocalLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafToRemoteLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafId <-. leafIDs]
                case child of
                    Left (localID, _) -> do
                        acceptID <- getKeyByJust $ UniqueSourceThemAcceptLocal localID
                        deleteWhere [SourceThemDelegateLocalSource ==. acceptID]
                        delete acceptID
                    Right (remoteID, _) -> do
                        acceptID <- getKeyByJust $ UniqueSourceThemAcceptRemote remoteID
                        deleteWhere [SourceThemDelegateRemoteSource ==. acceptID]
                        delete acceptID
                delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueSourceOriginUs sourceID)
                        (getKeyBy $ UniqueSourceOriginThem sourceID)
                        "Neither us nor them"
                        "Both us and them"
                case origin of
                    Left usID -> do
                        deleteBy $ UniqueSourceUsAccept usID
                        deleteBy $ UniqueSourceUsGestureLocal usID
                        deleteBy $ UniqueSourceUsGestureRemote usID
                        delete usID
                    Right themID -> do
                        deleteBy $ UniqueSourceThemGestureLocal themID
                        deleteBy $ UniqueSourceThemGestureRemote themID
                        delete themID
                case child of
                    Left (l, _) -> do
                        deleteBy $ UniqueSourceTopicGroupTopic l
                        delete l
                    Right (r, _) ->
                        delete r
                deleteBy $ UniqueSourceHolderGroup sourceID
                delete sourceID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid groupID
                    let topicByHash =
                            LocalActorGroup topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare main Revoke activity and insert to my outbox
                revoke@(actionRevoke, _, _, _) <- prepareMainRevoke (bimap snd snd child) grantID
                let recipByKey = LocalActorGroup groupID
                revokeID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                _luRevoke <- updateOutboxItem' recipByKey revokeID actionRevoke

                -- Prepare and insert Revokes on all the extension-Grants
                revokesG <- for gathers $ \ (Entity _ (SourceUsGather _ startID grantID)) -> do
                    DestUsStart acceptID _ <- getJust startID
                    DestUsAccept destID _ <- getJust acceptID
                    parent <- do
                        p <- getDestTopic destID
                        bitraverse
                            (\case
                                Right j -> pure $ LocalActorGroup j
                                Left _ -> error "I'm a group but I have a parent who is a Project"
                            )
                            pure
                            (bimap snd snd p)
                    return (parent, grantID)
                revokesL <- for leafs $ \ (Entity _ (SourceUsLeaf _ enableID grantID)) -> do
                    CollabEnable collabID _ <- getJust enableID
                    recip <- getCollabRecip collabID
                    return
                        ( bimap
                            (LocalActorPerson . collabRecipLocalPerson . entityVal)
                            (collabRecipRemoteActor . entityVal)
                            recip
                        , grantID
                        )
                revokes <- for (revokesG ++ revokesL) $ \ (actor, grantID) -> do
                    ext@(actionExt, _, _, _) <- prepareExtRevoke actor grantID
                    let recipByKey = LocalActorGroup groupID
                    extID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return (groupActor group, sieve, revokeID, revoke, revokes, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, revokeID, (actionRevoke, localRecipsRevoke, remoteRecipsRevoke, fwdHostsRevoke), revokes, inboxItemID) -> do
                let topicByID = LocalActorGroup groupID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $ do
                    sendActivity
                        topicByID topicActorID localRecipsRevoke
                        remoteRecipsRevoke fwdHostsRevoke revokeID actionRevoke
                    for_ revokes $ \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            topicByID topicActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "[Remove-Parent mode] Deleted the Parent/Source, forwarded Accept, sent Revokes"

        where

        prepareMainRevoke child grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            recipHash <- encodeKeyHashid groupID
            let topicByHash = LocalActorGroup recipHash

            childHash <- bitraverse encodeKeyHashid pure child

            audRemover <- lift $ makeAudSenderOnly authorIdMsig
            audChild <-
                    case childHash of
                        Left j ->
                            pure $
                            AudLocal [LocalActorGroup j] [LocalStageGroupFollowers j]
                        Right actorID -> do
                            actor <- getJust actorID
                            ObjURI h lu <- getRemoteActorURI actor
                            return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)
            let audMe = AudLocal [] [localActorFollowers topicByHash]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRemover, audChild, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote

            let uRemove = AP.acceptObject accept
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtRevoke recipient grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            groupHash <- encodeKeyHashid groupID
            let topicByHash = LocalActorGroup groupHash

            audRecip <-
                case recipient of
                    Left a -> do
                        h <- hashLocalActor a
                        return $ AudLocal [h] [localActorFollowers h]
                    Right actorID -> do
                        actor <- getJust actorID
                        ObjURI h lu <- getRemoteActorURI actor
                        return $
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRecip]

                recips = map encodeRouteHome audLocal ++ audRemote

            let uRemove = AP.acceptObject accept
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    addResource (effortID, topic', mode) = do

        topic <-
            lift $ traverseOf _Left (traverseOf _2 $ withDB . getLocalResource) topic'
        mode' <-
            bitraverse

                -- Resource-active mode
                -- Verify we haven't yet seen resource's Accept
                (\ () -> do
                    maybeResourceAccept <-
                        lift $ withDB $
                        case bimap fst fst topic of
                            Left localID -> (() <$) <$> getBy (UniqueEffortThemAcceptLocal localID)
                            Right remoteID -> (() <$) <$> getBy (UniqueEffortThemAcceptRemote remoteID)
                    verifyNothingE maybeResourceAccept "I already saw resource's Accept"
                )

                -- Resource-passive mode
                -- Option 1: We haven't seen resource's Accept yet
                --   * Verify sender is the resource
                -- Option 2: We saw it, but not my collaborator's Accept
                --   * Verify the Accept is authorized
                -- Otherwise respond with error, no Accept is needed
                (\ () -> do
                    (maybeResourceAccept, maybeGrant) <-
                        lift $ withDB $ liftA2 (,)
                            (case bimap fst fst topic of
                                Left localID -> (() <$) <$> getBy (UniqueEffortThemAcceptLocal localID)
                                Right remoteID -> (() <$) <$> getBy (UniqueEffortThemAcceptRemote remoteID)
                            )
                            (getBy $ UniqueEffortUsSendDelegator effortID)
                    case (isJust maybeResourceAccept, isJust maybeGrant) of
                        (False, True) -> error "Impossible/bug, didn't see resource's Accept but sent a Grant"
                        (False, False) -> do
                            unless (theyIsAuthor' topic) $
                                throwE "The Accept I'm waiting for is from my new resource"
                            return $ Left ()
                        (True, False) -> do
                            let muCap = AP.activityCapability $ actbActivity body
                            uCap <- fromMaybeE muCap "No capability provided"
                            verifyCapability''
                                uCap
                                authorIdMsig
                                (LocalResourceGroup groupID)
                                AP.RoleAdmin
                            return $ Right ()
                        (True, True) -> throwE "Resource already enabled, not needing any further Accept"
                )

                mode

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust groupID
                let actorID = groupActor recip
                (actorID,) <$> getJust actorID

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                idsForGrant <-
                    lift $
                    bitraverse

                        -- Resource-active mode
                        -- If sender is resource, record the Accept into the
                        -- Effort record & prepare to send degelator-Grant
                        -- Othrerwise do nothing
                        (\ () ->
                            if theyIsAuthor' topic
                                then Just <$> do
                                    case (topic, acceptDB) of
                                        (Left (localID, _), Left (_, _, acceptID)) ->
                                            insert_ $ EffortThemAcceptLocal localID acceptID
                                        (Right (remoteID, _), Right (_, _, acceptID)) ->
                                            insert_ $ EffortThemAcceptRemote remoteID acceptID
                                        _ -> error "groupAccept impossible iv"
                                    grantID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                                    insert_ $ EffortUsSendDelegator effortID grantID
                                    return grantID
                                else pure Nothing
                        )

                        -- Resource-passive mode
                        (\case

                            -- Getting an Accept from the resource
                            -- Record resource's Accept in Effort record
                            Left () -> do
                                case (topic, acceptDB) of
                                    (Left (localID, _), Left (_, _, acceptID)) ->
                                        insert_ $ EffortThemAcceptLocal localID acceptID
                                    (Right (remoteID, _), Right (_, _, acceptID)) ->
                                        insert_ $ EffortThemAcceptRemote remoteID acceptID
                                    _ -> error "groupAccept impossible v"
                                return Nothing

                            -- Getting an Accept from my collaborator
                            -- Record my collaborator's Accept
                            -- Prepare to send delegator-Grant
                            Right () -> Just <$> do
                                {-
                                case (topic, acceptDB) of
                                    (Left (localID, _), Left (_, _, acceptID)) ->
                                        insert_ $ ? localID acceptID
                                    (Right (remoteID, _), Right (_, _, acceptID)) ->
                                        insert_ $ ? remoteID acceptID
                                    _ -> error "groupAccept impossible iv"
                                -}
                                grantID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                                insert_ $ EffortUsSendDelegator effortID grantID
                                return grantID
                        )

                        mode'

                -- Prepare forwarding of Accept to my followers
                let recipByID = LocalActorGroup groupID
                recipByHash <- hashLocalActor recipByID
                let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

                maybeAct <-
                    case idsForGrant of
                        Left mg -> for mg $ \ grantID -> lift $ do
                            grant@(actionGrant, _, _, _) <-
                                prepareEffortDelegGrant (bimap snd snd topic) False
                            let recipByKey = LocalActorGroup groupID
                            _luGrant <- updateOutboxItem' recipByKey grantID actionGrant
                            return (grantID, grant)

                        Right mg -> for mg $ \ grantID -> lift $ do
                            grant@(actionGrant, _, _, _) <-
                                prepareEffortDelegGrant (bimap snd snd topic) True
                            let recipByKey = LocalActorGroup groupID
                            _luGrant <- updateOutboxItem' recipByKey grantID actionGrant
                            return (grantID, grant)

                return (recipActorID, sieve, maybeAct, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, maybeGrant, inboxItemID) -> do
                let recipByID = LocalActorGroup groupID
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ for_ maybeGrant $ \ (grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant)) ->
                    sendActivity
                        recipByID recipActorID localRecipsGrant
                        remoteRecipsGrant fwdHostsGrant grantID actionGrant
                doneDB inboxItemID "[Resource] Forwarded the Accept and maybe published a Grant/Accept"

        where

        prepareEffortDelegGrant ident includeAuthor = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            (uResource, audResource) <-
                case ident of
                    Left lr -> do
                        la <- resourceToActor <$> hashLocalResource lr
                        return
                            ( encodeRouteHome $ renderLocalActor la
                            , AudLocal [la] [localActorFollowers la]
                            )
                    Right raID -> do
                        ra <- getJust raID
                        u@(ObjURI h lu) <- getRemoteActorURI ra
                        return
                            ( u
                            , AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
                            )
            audAuthor <- lift $ makeAudSenderOnly authorIdMsig
            groupHash <- encodeKeyHashid groupID
            let audGroup = AudLocal [] [LocalStageGroupFollowers groupHash]

                audience =
                    if includeAuthor
                        then [audResource, audGroup, audAuthor]
                        else [audResource, audGroup]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience audience

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [AP.acceptObject accept]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXDelegator
                        , AP.grantContext   = encodeRouteHome $ GroupR groupHash
                        , AP.grantTarget    = uResource
                        , AP.grantResult    = Nothing
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Invoke
                        , AP.grantDelegates = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    removeResource (effortID, sendID, grantID, resource) = do

        topic <-
            lift $ traverseOf _Left (traverseOf _2 $ withDB . getLocalResource) resource

        -- Verify the sender is the topic
        unless (theyIsAuthor' topic) $
            throwE "The Accept isn't by the to-be-removed resource"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                -- Grab extension-Grants that I'm about to revoke
                distributes <- selectList [EffortUsDistributeEffort ==. sendID] []
                leafs <- selectList [EffortUsLeafEffort ==. sendID] []

                -- Delete the whole Effort record
                deleteWhere [EffortRemoveSend ==. sendID]
                let distributeIDs = map entityKey distributes
                deleteWhere [EffortUsDistributeFromLocalDistribute <-. distributeIDs]
                deleteWhere [EffortUsDistributeFromRemoteDistribute <-. distributeIDs]
                deleteWhere [EffortUsDistributeId <-. distributeIDs]
                let leafIDs = map entityKey leafs
                deleteWhere [EffortUsLeafFromLocalLeaf <-. leafIDs]
                deleteWhere [EffortUsLeafFromRemoteLeaf <-. leafIDs]
                deleteWhere [EffortUsLeafToLocalLeaf <-. leafIDs]
                deleteWhere [EffortUsLeafToRemoteLeaf <-. leafIDs]
                deleteWhere [EffortUsLeafId <-. leafIDs]
                case resource of
                    Left (localID, _) -> do
                        acceptID <- getKeyByJust $ UniqueEffortThemAcceptLocal localID
                        deleteWhere [EffortThemDelegateLocalEffort ==. acceptID]
                        delete acceptID
                    Right (remoteID, _) -> do
                        acceptID <- getKeyByJust $ UniqueEffortThemAcceptRemote remoteID
                        deleteWhere [EffortThemDelegateRemoteEffort ==. acceptID]
                        delete acceptID
                delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueEffortOriginUs effortID)
                        (getKeyBy $ UniqueEffortOriginThem effortID)
                        "Neither us nor them"
                        "Both us and them"
                case origin of
                    Left usID -> do
                        deleteBy $ UniqueEffortUsAccept usID
                        deleteBy $ UniqueEffortUsGestureLocal usID
                        deleteBy $ UniqueEffortUsGestureRemote usID
                        delete usID
                    Right themID -> do
                        deleteBy $ UniqueEffortThemGestureLocal themID
                        deleteBy $ UniqueEffortThemGestureRemote themID
                        delete themID
                case resource of
                    Left (l, _) -> delete l
                    Right (r, _) -> delete r
                delete effortID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid groupID
                    let topicByHash =
                            LocalActorGroup topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare main Revoke activity and insert to my outbox
                revoke@(actionRevoke, _, _, _) <- prepareMainRevoke (bimap snd snd topic) grantID
                let recipByKey = LocalActorGroup groupID
                revokeID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                _luRevoke <- updateOutboxItem' recipByKey revokeID actionRevoke

                -- Prepare and insert Revokes on all the extension-Grants
                revokesD <- for distributes $ \ (Entity _ (EffortUsDistribute _ startID grantID)) -> do
                    DestUsStart acceptID _ <- getJust startID
                    DestUsAccept destID _ <- getJust acceptID
                    resource <- do
                        p <- getDestTopic destID
                        bitraverse
                            (\case
                                Right j -> pure $ LocalActorGroup j
                                Left _ -> error "I'm a group but I have a resource who is a Project"
                            )
                            pure
                            (bimap snd snd p)
                    return (resource, grantID)
                revokesL <- for leafs $ \ (Entity _ (EffortUsLeaf _ enableID grantID)) -> do
                    CollabEnable collabID _ <- getJust enableID
                    recip <- getCollabRecip collabID
                    return
                        ( bimap
                            (LocalActorPerson . collabRecipLocalPerson . entityVal)
                            (collabRecipRemoteActor . entityVal)
                            recip
                        , grantID
                        )
                revokes <- for (revokesD ++ revokesL) $ \ (actor, grantID) -> do
                    ext@(actionExt, _, _, _) <- prepareExtRevoke actor grantID
                    let recipByKey = LocalActorGroup groupID
                    extID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return (groupActor group, sieve, revokeID, revoke, revokes, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, revokeID, (actionRevoke, localRecipsRevoke, remoteRecipsRevoke, fwdHostsRevoke), revokes, inboxItemID) -> do
                let topicByID = LocalActorGroup groupID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $ do
                    sendActivity
                        topicByID topicActorID localRecipsRevoke
                        remoteRecipsRevoke fwdHostsRevoke revokeID actionRevoke
                    for_ revokes $ \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            topicByID topicActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "[Remove-Resource mode] Deleted the Resource/Effort, forwarded Accept, sent Revokes"

        where

        prepareMainRevoke resource grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            recipHash <- encodeKeyHashid groupID
            let topicByHash = LocalActorGroup recipHash

            resourceHash <- bitraverse hashLocalResource pure resource

            audRemover <- lift $ makeAudSenderOnly authorIdMsig
            audResource <-
                case resourceHash of
                    Left lr ->
                        let la = resourceToActor lr
                        in  pure $ AudLocal [la] [localActorFollowers la]
                    Right actorID -> do
                        actor <- getJust actorID
                        ObjURI h lu <- getRemoteActorURI actor
                        return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)
            let audMe = AudLocal [] [localActorFollowers topicByHash]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRemover, audResource, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote

            let uRemove = AP.acceptObject accept
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtRevoke recipient grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            groupHash <- encodeKeyHashid groupID
            let topicByHash = LocalActorGroup groupHash

            audRecip <-
                case recipient of
                    Left a -> do
                        h <- hashLocalActor a
                        return $ AudLocal [h] [localActorFollowers h]
                    Right actorID -> do
                        actor <- getJust actorID
                        ObjURI h lu <- getRemoteActorURI actor
                        return $
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRecip]

                recips = map encodeRouteHome audLocal ++ audRemote

            let uRemove = AP.acceptObject accept
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor is following someone/something
-- Behavior:
--      * Verify the target is me
--      * Record the follow in DB
--      * Publish and send an Accept to the sender and its followers
groupFollow
    :: UTCTime
    -> GroupId
    -> Verse
    -> AP.Follow URIMode
    -> ActE (Text, Act (), Next)
groupFollow now recipGroupID verse follow = do
    recipGroupHash <- encodeKeyHashid recipGroupID
    actorFollow
        (\case
            GroupR d | d == recipGroupHash -> pure ()
            _ -> throwE "Asking to follow someone else"
        )
        groupActor
        False
        (\ recipGroupActor () -> pure $ actorFollowers recipGroupActor)
        (\ _ -> pure $ makeRecipientSet [] [])
        LocalActorGroup
        (\ _ -> pure [])
        now recipGroupID verse follow

data GrantKind
    = GKDelegationStart AP.Role
    | GKDelegationExtend AP.Role (Either (LocalActorBy Key) FedURI)
    | GKDelegator

-- Meaning: An actor is granting access-to-some-resource to another actor
-- Behavior:
--      * Option 1 - Resource sending me a delegation-start or delegation-extension
--          * Verify they're authorized, i.e. they're using the delegator-Grant
--            I gave them
--          * Verify the role isn't delegator
--          * Store the Grant in the Effort record in DB
--          * Send extension-Grants and record them in the DB:
--              * To each of my direct collaborators
--              * To each of my children
--
--      * Option 2 - Collaborator sending me a delegator-Grant - Verify that:
--          * The sender is a collaborator of mine, A
--          * The Grant's context is A
--          * The Grant's target is me
--          * The Grant's usage is invoke & role is delegate
--          * The Grant doesn't specify 'delegates'
--          * The activity is authorized via a valid direct-Grant I had sent
--            to A
--      * Verify I don't yet have a delegator-Grant from A
--      * Insert the Grant to my inbox
--      * Record the delegator-Grant in the Collab record in DB
--      * Forward the Grant to my followers
--      * For each project of mine J, prepare and send an
--        extension-Grant to A, and store it in the Source record in DB
--      * For each start-grant or extension-grant G that I received from a
--        parent of mine, prepare and send an extension-Grant to A, and store
--        it in the Source record in DB
--
--      * Option 3 - Parent sending me a delegation-start or delegation-extension
--          * Verify they're authorized, i.e. they're using the delegator-Grant
--            I gave them
--          * Verify the role isn't delegator
--          * Store the Grant in the Source record in DB
--          * Send extension-Grants and record them in the DB:
--              * To each of my direct collaborators
--              * To each of my children
--
--      * Option 4 - Almost-Child sending me the delegator-Grant
--          * Update the Dest record, enabling the child
--          * Send a start-Grant giving access-to-me
--          * For each of my projects, send an extension-Grant to the new
--            parent
--          * For each grant I've been delegated from my parents, send an
--            extension-Grant to the new child
--
--      * If neither of those, raise an error
groupGrant
    :: UTCTime
    -> GroupId
    -> Verse
    -> AP.Grant URIMode
    -> ActE (Text, Act (), Next)
groupGrant now groupID (Verse authorIdMsig body) grant = do

    grant' <- checkGrant grant
    let adapt = maybe (Right Nothing) (either Left (Right . Just))
    maybeMode <-
        withDBExcept $ ExceptT $ fmap adapt $ runMaybeT $
            runExceptT (Left . Left <$> tryResource grant') <|>
            runExceptT (Left . Right <$> tryCollab grant') <|>
            runExceptT (Right . Left <$> tryParent grant') <|>
            runExceptT (Right . Right <$> tryAlmostChild grant')
    mode <-
        fromMaybeE
            maybeMode
            "Not a relevant Grant that I'm aware of"
    case mode of
        Left (Left (role, sendID, topic)) ->
            handleResource role sendID topic
        Left (Right (enableID, role, recip)) ->
            handleCollab enableID role recip
        Right (Left (role, sendID, topic)) ->
            handleParent role sendID topic
        Right (Right (role, topic, acceptID)) ->
            handleAlmostChild role topic acceptID

    where

    checkCapability = do
        -- Verify that a capability is provided
        uCap <- lift $ hoistMaybe $ AP.activityCapability $ actbActivity body

        -- Verify the capability URI is one of:
        --   * Outbox item URI of a local actor, i.e. a local activity
        --   * A remote URI
        cap <-
            ExceptT . lift . lift . runExceptT $
                nameExceptT "Grant capability" $ parseActivityURI' uCap

        -- Verify the capability is local
        case cap of
            Left (actorByKey, _, outboxItemID) ->
                return (actorByKey, outboxItemID)
            _ -> lift mzero

    checkGrant g = do
        (role, resource, recipient, _mresult, mstart, mend, usage, mdeleg) <-
            parseGrant' g
        case recipient of
            Left (LocalActorGroup j) | j == groupID -> pure ()
            _ -> throwE "Target isn't me"
        for_ mstart $ \ start ->
            unless (start < now) $ throwE "Start time is in the future"
        for_ mend $ \ _ ->
            throwE "End time is specified"

        let resourceIsAuthor =
                case (resource, authorIdMsig) of
                    (Left a, Left (a', _, _)) -> a == a'
                    (Right u, Right (ra, _, _)) -> remoteAuthorURI ra == u
                    _ -> False

        case (role, resourceIsAuthor, usage, mdeleg) of
            (AP.RXRole r, True, AP.Distribute, Nothing) ->
                pure $ GKDelegationStart r
            (AP.RXRole r, False, AP.Distribute, Just _) ->
                pure $ GKDelegationExtend r resource
            (AP.RXDelegator, True, AP.Invoke, Nothing) ->
                pure GKDelegator
            _ -> throwE "A kind of Grant that I don't use"

    tryResource gk = do
        capability <- checkCapability
        role <-
            case gk of
                GKDelegationStart role    -> pure role
                GKDelegationExtend role _ -> pure role
                GKDelegator               -> lift mzero
        -- Find the Effort record from the capability
        Entity sendID (EffortUsSendDelegator effortID _) <- lift $ do
            -- Capability isn't mine
            guard $ fst capability == LocalActorGroup groupID
            -- I don't have a Effort with this capability
            MaybeT $ getBy $ UniqueEffortUsSendDelegatorGrant $ snd capability
        Effort role' g <- lift $ lift $ getJust effortID
        -- Found a Effort for this Grant but it's not mine
        lift $ guard $ g == groupID
        topic <- lift $ lift $ getEffortTopic effortID
        topicForCheck <-
            lift $ lift $
            bitraverse
                (\ (_, resourceID) -> getLocalResource resourceID)
                (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                topic
        unless (first resourceToActor topicForCheck == bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig) $
            throwE "Capability's effort and Grant author aren't the same actor"
        return (min role role', sendID, topic)

    handleResource role sendID topic = do

        uCap <- lift $ getActivityURI authorIdMsig
        checkCapabilityBeforeExtending uCap (LocalActorGroup groupID)

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            resourceID <- lift $ groupResource <$> getJust groupID
            Resource recipActorID <- lift $ getJust resourceID
            recipActor <- lift $ getJust recipActorID

            topicWithAccept <-
                lift $
                bitraverse
                    (\ (localID, rID) ->
                        (localID, rID,) <$>
                            getKeyByJust (UniqueEffortThemAcceptLocal localID)
                    )
                    (\ (remoteID, aID) ->
                        (remoteID, aID,) <$>
                            getKeyByJust (UniqueEffortThemAcceptRemote remoteID)
                    )
                    topic

            maybeGrantDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeGrantDB $ \ (inboxItemID, grantDB) -> do

                -- Record the delegation in DB
                from <- case (grantDB, bimap (view _3) (view _3) topicWithAccept) of
                    (Left (_, _, grantID), Left localID) -> Left <$> do
                        mk <- lift $ insertUnique $ EffortThemDelegateLocal localID grantID
                        fromMaybeE mk "I already have such a EffortThemDelegateLocal"
                    (Right (_, _, grantID), Right remoteID) -> Right <$> do
                        mk <- lift $ insertUnique $ EffortThemDelegateRemote remoteID grantID
                        fromMaybeE mk "I already have such a EffortThemDelegateRemote"
                    _ -> error "groupGrant.resource impossible"

                -- For each Collab in me, prepare a delegation-extension Grant
                localCollabs <-
                    lift $
                    E.select $ E.from $ \ (collab `E.InnerJoin` enable `E.InnerJoin` recipL `E.InnerJoin` deleg) -> do
                        E.on $ enable E.^. CollabEnableId E.==. deleg E.^. CollabDelegLocalEnable
                        E.on $ enable E.^. CollabEnableCollab E.==. recipL E.^. CollabRecipLocalCollab
                        E.on $ collab E.^. CollabId E.==. enable E.^. CollabEnableCollab
                        E.where_ $ collab E.^. CollabTopic E.==. E.val resourceID
                        return
                            ( collab E.^. CollabRole
                            , recipL E.^. CollabRecipLocalPerson
                            , deleg
                            )
                localExtensions <- lift $ for localCollabs $ \ (E.Value role', E.Value personID, Entity delegID (CollabDelegLocal enableID _recipID grantID)) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    leafID <- insert $ EffortUsLeaf sendID enableID extID
                    case from of
                        Left localID -> insert_ $ EffortUsLeafFromLocal leafID localID
                        Right remoteID -> insert_ $ EffortUsLeafFromRemote leafID remoteID
                    insert_ $ EffortUsLeafToLocal leafID delegID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForCollab (Left (personID, grantID)) (min role role') enableID
                    let recipByKey = LocalActorGroup groupID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                remoteCollabs <-
                    lift $
                    E.select $ E.from $ \ (collab `E.InnerJoin` enable `E.InnerJoin` recipR `E.InnerJoin` deleg) -> do
                        E.on $ enable E.^. CollabEnableId E.==. deleg E.^. CollabDelegRemoteEnable
                        E.on $ enable E.^. CollabEnableCollab E.==. recipR E.^. CollabRecipRemoteCollab
                        E.on $ collab E.^. CollabId E.==. enable E.^. CollabEnableCollab
                        E.where_ $ collab E.^. CollabTopic E.==. E.val resourceID
                        return
                            ( collab E.^. CollabRole
                            , recipR E.^. CollabRecipRemoteActor
                            , deleg
                            )
                remoteExtensions <- lift $ for remoteCollabs $ \ (E.Value role', E.Value raID, Entity delegID (CollabDelegRemote enableID _recipID grantID)) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    leafID <- insert $ EffortUsLeaf sendID enableID extID
                    case from of
                        Left localID -> insert_ $ EffortUsLeafFromLocal leafID localID
                        Right remoteID -> insert_ $ EffortUsLeafFromRemote leafID remoteID
                    insert_ $ EffortUsLeafToRemote leafID delegID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForCollab (Right (raID, grantID)) (min role role') enableID
                    let recipByKey = LocalActorGroup groupID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                -- For each child of mine, prepare a delegation-extension Grant
                localChildren <-
                    lift $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. DestUsAcceptId E.==. start E.^. DestUsStartDest
                        E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
                        E.on $ topic E.^. DestTopicGroupTopic E.==. deleg E.^. DestThemSendDelegatorLocalTopic
                        E.on $ holder E.^. DestHolderGroupId E.==. topic E.^. DestTopicGroupHolder
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderGroupDest
                        E.where_ $ holder E.^. DestHolderGroupGroup E.==. E.val groupID
                        return
                            ( dest E.^. DestRole
                            , topic E.^. DestTopicGroupChild
                            , deleg E.^. DestThemSendDelegatorLocalId
                            , deleg E.^. DestThemSendDelegatorLocalGrant
                            , accept E.^. DestUsAcceptId
                            , start E.^. DestUsStartId
                            )
                localExtensionsForChildren <- lift $ for localChildren $ \ (E.Value role', E.Value childID, E.Value _delegID, E.Value grantID, E.Value _acceptID, E.Value startID) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    distributeID <- insert $ EffortUsDistribute sendID startID extID
                    case from of
                        Left localID -> insert_ $ EffortUsDistributeFromLocal distributeID localID
                        Right remoteID -> insert_ $ EffortUsDistributeFromRemote distributeID remoteID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForChild (Left (childID, grantID)) (min role role') startID
                    let recipByKey = LocalActorGroup groupID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                remoteChildren <-
                    lift $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. DestUsAcceptId E.==. start E.^. DestUsStartDest
                        E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
                        E.on $ topic E.^. DestTopicRemoteId E.==. deleg E.^. DestThemSendDelegatorRemoteTopic
                        E.on $ dest E.^. DestId E.==. topic E.^. DestTopicRemoteDest
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderGroupDest
                        E.where_ $ holder E.^. DestHolderGroupGroup E.==. E.val groupID
                        return
                            ( dest E.^. DestRole
                            , topic E.^. DestTopicRemoteTopic
                            , deleg E.^. DestThemSendDelegatorRemoteId
                            , deleg E.^. DestThemSendDelegatorRemoteGrant
                            , accept E.^. DestUsAcceptId
                            , start E.^. DestUsStartId
                            )
                remoteExtensionsForChildren <- lift $ for remoteChildren $ \ (E.Value role', E.Value childID, E.Value _delegID, E.Value grantID, E.Value _acceptID, E.Value startID) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    distributeID <- insert $ EffortUsDistribute sendID startID extID
                    case from of
                        Left localID -> insert_ $ EffortUsDistributeFromLocal distributeID localID
                        Right remoteID -> insert_ $ EffortUsDistributeFromRemote distributeID remoteID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForChild (Right (childID, grantID)) (min role role') startID
                    let recipByKey = LocalActorGroup groupID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return
                    ( recipActorID
                    , localExtensions ++ localExtensionsForChildren
                    , remoteExtensions ++ remoteExtensionsForChildren
                    , inboxItemID
                    )

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, localExts, remoteExts, inboxItemID) -> do
                let recipByID = LocalActorGroup groupID
                lift $ for_ (localExts ++ remoteExts) $
                    \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            recipByID recipActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "[Resource] Sent extensions to collabs & children"

    tryCollab (GKDelegationStart _)    = lift mzero
    tryCollab (GKDelegationExtend _ _) = lift mzero
    tryCollab GKDelegator              = do
        capability <- checkCapability
        -- Find the Collab record from the capability
        Entity enableID (CollabEnable collabID _) <- lift $ do
            -- Capability isn't mine
            guard $ fst capability == LocalActorGroup groupID
            -- I don't have a Collab with this capability
            MaybeT $ getBy $ UniqueCollabEnableGrant $ snd capability
        Collab role _ <- lift $ lift $ getJust collabID
        topic <- lift $ lift $ getCollabTopic collabID
        -- Found a Collab for this direct-Grant but it's not mine
        lift $ guard $ topic == LocalResourceGroup groupID
        recip <- lift $ lift $ getCollabRecip collabID
        recipForCheck <-
            lift $ lift $
            bitraverse
                (pure . collabRecipLocalPerson . entityVal)
                (getRemoteActorURI <=< getJust . collabRecipRemoteActor . entityVal)
                recip
        unless (first LocalActorPerson recipForCheck == bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig) $
            throwE "Capability's collaborator and Grant author aren't the same actor"
        return (enableID, role, recip)

    handleCollab enableID role recip = do

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust groupID
                let actorID = groupActor recip
                (actorID,) <$> getJust actorID

            -- Verify I don't yet have a delegator-Grant from the collaborator
            maybeDeleg <-
                lift $ case bimap entityKey entityKey recip of
                    Left localID -> (() <$) <$> getBy (UniqueCollabDelegLocalRecip localID)
                    Right remoteID -> (() <$) <$> getBy (UniqueCollabDelegRemoteRecip remoteID)
            verifyNothingE maybeDeleg "I already have a delegator-Grant from this collaborator"

            maybeGrantDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeGrantDB $ \ (inboxItemID, grantDB) -> do

                -- Record the delegator-Grant in the Collab record
                (insertLeaf, insertEffortLeafTo, uDeleg) <-
                    lift $ case (grantDB, bimap entityKey entityKey recip) of
                        (Left (grantActor, _, grantID), Left localID) -> do
                            delegID <- insert $ CollabDelegLocal enableID localID grantID
                            encodeRouteHome <- getEncodeRouteHome
                            delegR <-
                                activityRoute
                                    <$> hashLocalActor grantActor
                                    <*> encodeKeyHashid grantID
                            return
                                ( \ leafID ->
                                    insert_ $ SourceUsLeafToLocal leafID delegID
                                , \ leafID ->
                                    insert_ $ EffortUsLeafToLocal leafID delegID
                                , encodeRouteHome delegR
                                )
                        (Right (_, _, grantID), Right remoteID) -> do
                            delegID <- insert $ CollabDelegRemote enableID remoteID grantID
                            u <- getRemoteActivityURI =<< getJust grantID
                            return
                                ( \ leafID ->
                                    insert_ $ SourceUsLeafToRemote leafID delegID
                                , \ leafID ->
                                    insert_ $ EffortUsLeafToRemote leafID delegID
                                , u
                                )
                        _ -> error "groupGrant impossible 2"

                -- Prepare forwarding of Accept to my followers
                groupHash <- encodeKeyHashid groupID
                let sieve = makeRecipientSet [] [LocalStageGroupFollowers groupHash]

                extensions <- lift $ do
                    (uCollab, audCollab) <-
                        case recip of
                            Left (Entity _ (CollabRecipLocal _ personID)) -> do
                                personHash <- encodeKeyHashid personID
                                encodeRouteHome <- getEncodeRouteHome
                                return
                                    ( encodeRouteHome $ PersonR personHash
                                    , AudLocal [LocalActorPerson personHash] []
                                    )
                            Right (Entity _ (CollabRecipRemote _ raID)) -> do
                                ra <- getJust raID
                                u@(ObjURI h lu) <- getRemoteActorURI ra
                                return (u, AudRemote h [lu] [])
                    -- For each Grant I got from a project, prepare a
                    -- delegation-extension Grant
                    l' <-
                        fmap (map $ over _2 Left) $
                        E.select $ E.from $ \ (effort `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send `E.InnerJoin` deleg) -> do
                            E.on $ accept E.^. EffortThemAcceptLocalId E.==. deleg E.^. EffortThemDelegateLocalEffort
                            E.on $ effort E.^. EffortId E.==. send E.^. EffortUsSendDelegatorEffort
                            E.on $ topic E.^. EffortTopicLocalId E.==. accept E.^. EffortThemAcceptLocalTopic
                            E.on $ effort E.^. EffortId E.==. topic E.^. EffortTopicLocalEffort
                            E.where_ $ effort E.^. EffortHolder E.==. E.val groupID
                            return
                                ( send E.^. EffortUsSendDelegatorId
                                , deleg
                                )
                    r' <-
                        fmap (map $ over _2 Right) $
                        E.select $ E.from $ \ (effort `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send `E.InnerJoin` deleg) -> do
                            E.on $ accept E.^. EffortThemAcceptRemoteId E.==. deleg E.^. EffortThemDelegateRemoteEffort
                            E.on $ effort E.^. EffortId E.==. send E.^. EffortUsSendDelegatorEffort
                            E.on $ topic E.^. EffortTopicRemoteId E.==. accept E.^. EffortThemAcceptRemoteTopic
                            E.on $ effort E.^. EffortId E.==. topic E.^. EffortTopicRemoteEffort
                            E.where_ $ effort E.^. EffortHolder E.==. E.val groupID
                            return
                                ( send E.^. EffortUsSendDelegatorId
                                , deleg
                                )
                    fromProjects <- for (l' ++ r') $ \ (E.Value sendID, deleg) -> do
                        extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                        leafID <- insert $ EffortUsLeaf sendID enableID extID
                        case bimap entityKey entityKey deleg of
                            Left fromID -> insert_ $ EffortUsLeafFromLocal leafID fromID
                            Right fromID -> insert_ $ EffortUsLeafFromRemote leafID fromID
                        insertEffortLeafTo leafID
                        (AP.Doc h a, grant) <- getGrantActivityBody $ bimap (effortThemDelegateLocalGrant . entityVal) (effortThemDelegateRemoteGrant . entityVal) deleg
                        uStart <-
                            case AP.activityId a of
                                Nothing -> error "EffortThemDelegate grant has no 'id'"
                                Just lu -> pure $ ObjURI h lu
                        ext@(actionExt, _, _, _) <-
                            prepareExtensionGrantFromParentOrProject uCollab audCollab uDeleg uStart grant role enableID
                        let recipByKey = LocalActorGroup groupID
                        _luExt <- updateOutboxItem' recipByKey extID actionExt
                        return (extID, ext)

                    -- For each Grant I got from a parent, prepare a
                    -- delegation-extension Grant
                    l <-
                        fmap (map $ over _2 Left) $
                        E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send `E.InnerJoin` deleg) -> do
                            E.on $ accept E.^. SourceThemAcceptLocalId E.==. deleg E.^. SourceThemDelegateLocalSource
                            E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                            E.on $ topic E.^. SourceTopicLocalId E.==. accept E.^. SourceThemAcceptLocalTopic
                            E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicLocalSource
                            E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderGroupSource
                            E.where_ $ holder E.^. SourceHolderGroupGroup E.==. E.val groupID
                            return
                                ( send E.^. SourceUsSendDelegatorId
                                , deleg
                                )
                    r <-
                        fmap (map $ over _2 Right) $
                        E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send `E.InnerJoin` deleg) -> do
                            E.on $ accept E.^. SourceThemAcceptRemoteId E.==. deleg E.^. SourceThemDelegateRemoteSource
                            E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                            E.on $ topic E.^. SourceTopicRemoteId E.==. accept E.^. SourceThemAcceptRemoteTopic
                            E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicRemoteSource
                            E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderGroupSource
                            E.where_ $ holder E.^. SourceHolderGroupGroup E.==. E.val groupID
                            return
                                ( send E.^. SourceUsSendDelegatorId
                                , deleg
                                )
                    fromParents <- for (l ++ r) $ \ (E.Value sendID, deleg) -> do
                        extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                        leafID <- insert $ SourceUsLeaf sendID enableID extID
                        case bimap entityKey entityKey deleg of
                            Left fromID -> insert_ $ SourceUsLeafFromLocal leafID fromID
                            Right fromID -> insert_ $ SourceUsLeafFromRemote leafID fromID
                        insertLeaf leafID
                        (AP.Doc h a, grant) <- getGrantActivityBody $ bimap (sourceThemDelegateLocalGrant . entityVal) (sourceThemDelegateRemoteGrant . entityVal) deleg
                        uStart <-
                            case AP.activityId a of
                                Nothing -> error "SourceThemDelegate grant has no 'id'"
                                Just lu -> pure $ ObjURI h lu
                        ext@(actionExt, _, _, _) <-
                            prepareExtensionGrantFromParentOrProject uCollab audCollab uDeleg uStart grant role enableID
                        let recipByKey = LocalActorGroup groupID
                        _luExt <- updateOutboxItem' recipByKey extID actionExt
                        return (extID, ext)

                    return $ fromProjects ++ fromParents

                return (recipActorID, sieve, extensions, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, extensions, inboxItemID) -> do
                let recipByID = LocalActorGroup groupID
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ for_ extensions $
                    \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            recipByID recipActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "[Collab] Forwarded the delegator-Grant, updated DB and published delegation extensions"

        where

        prepareExtensionGrantFromParentOrProject uCollab audCollab uDeleg uStart grant role enableID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            groupHash <- encodeKeyHashid groupID
            enableHash <- encodeKeyHashid enableID
            finalRole <-
                case AP.grantObject grant of
                    AP.RXRole r -> pure $ min role r
                    AP.RXDelegator -> error "Why was I delegated a Grant with object=delegator?"

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audCollab]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole finalRole
                        , AP.grantContext   = AP.grantContext grant
                        , AP.grantTarget    = uCollab
                        , AP.grantResult    =
                            Just
                                (encodeRouteLocal $
                                    GroupMemberLiveR groupHash enableHash
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Invoke
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    tryParent gk = do
        capability <- checkCapability
        role <-
            case gk of
                GKDelegationStart role    -> pure role
                GKDelegationExtend role _ -> pure role
                GKDelegator               -> lift mzero
        -- Find the Source record from the capability
        Entity sendID (SourceUsSendDelegator sourceID _) <- lift $ do
            -- Capability isn't mine
            guard $ fst capability == LocalActorGroup groupID
            -- I don't have a Source with this capability
            MaybeT $ getBy $ UniqueSourceUsSendDelegatorGrant $ snd capability
        Source role' <- lift $ lift $ getJust sourceID
        SourceHolderGroup _ g <-
            lift $ MaybeT $ getValBy $ UniqueSourceHolderGroup sourceID
        -- Found a Source for this Grant but it's not mine
        lift $ guard $ g == groupID
        topic <- do
            t <- lift $ lift $ getSourceTopic sourceID
            bitraverse
                (bitraverse
                    pure
                    (\case
                        Right g -> pure g
                        Left _j -> error "I have a SourceTopic that is a Project"
                    )
                )
                pure
                t
        topicForCheck <-
            lift $ lift $
            bitraverse
                (pure . snd)
                (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                topic
        unless (first LocalActorGroup topicForCheck == bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig) $
            throwE "Capability's source and Grant author aren't the same actor"
        return (min role role', sendID, topic)

    handleParent role sendID topic = do

        uCap <- lift $ getActivityURI authorIdMsig
        checkCapabilityBeforeExtending uCap (LocalActorGroup groupID)

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            resourceID <- lift $ groupResource <$> getJust groupID
            Resource recipActorID <- lift $ getJust resourceID
            recipActor <- lift $ getJust recipActorID

            topicWithAccept <-
                lift $
                bitraverse
                    (\ (localID, jID) ->
                        (localID, jID,) <$>
                            getKeyByJust (UniqueSourceThemAcceptLocal localID)
                    )
                    (\ (remoteID, aID) ->
                        (remoteID, aID,) <$>
                            getKeyByJust (UniqueSourceThemAcceptRemote remoteID)
                    )
                    topic

            maybeGrantDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeGrantDB $ \ (inboxItemID, grantDB) -> do

                -- Record the delegation in DB
                from <- case (grantDB, bimap (view _3) (view _3) topicWithAccept) of
                    (Left (_, _, grantID), Left localID) -> Left <$> do
                        mk <- lift $ insertUnique $ SourceThemDelegateLocal localID grantID
                        fromMaybeE mk "I already have such a SourceThemDelegateLocal"
                    (Right (_, _, grantID), Right remoteID) -> Right <$> do
                        mk <- lift $ insertUnique $ SourceThemDelegateRemote remoteID grantID
                        fromMaybeE mk "I already have such a SourceThemDelegateRemote"
                    _ -> error "projectGrant.child impossible"

                -- For each Collab in me, prepare a delegation-extension Grant
                localCollabs <-
                    lift $
                    E.select $ E.from $ \ (collab `E.InnerJoin` enable `E.InnerJoin` recipL `E.InnerJoin` deleg) -> do
                        E.on $ enable E.^. CollabEnableId E.==. deleg E.^. CollabDelegLocalEnable
                        E.on $ enable E.^. CollabEnableCollab E.==. recipL E.^. CollabRecipLocalCollab
                        E.on $ collab E.^. CollabId E.==. enable E.^. CollabEnableCollab
                        E.where_ $ collab E.^. CollabTopic E.==. E.val resourceID
                        return
                            ( collab E.^. CollabRole
                            , recipL E.^. CollabRecipLocalPerson
                            , deleg
                            )
                localExtensions <- lift $ for localCollabs $ \ (E.Value role', E.Value personID, Entity delegID (CollabDelegLocal enableID _recipID grantID)) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    leafID <- insert $ SourceUsLeaf sendID enableID extID
                    case from of
                        Left localID -> insert_ $ SourceUsLeafFromLocal leafID localID
                        Right remoteID -> insert_ $ SourceUsLeafFromRemote leafID remoteID
                    insert_ $ SourceUsLeafToLocal leafID delegID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForCollab (Left (personID, grantID)) (min role role') enableID
                    let recipByKey = LocalActorGroup groupID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                remoteCollabs <-
                    lift $
                    E.select $ E.from $ \ (collab `E.InnerJoin` enable `E.InnerJoin` recipR `E.InnerJoin` deleg) -> do
                        E.on $ enable E.^. CollabEnableId E.==. deleg E.^. CollabDelegRemoteEnable
                        E.on $ enable E.^. CollabEnableCollab E.==. recipR E.^. CollabRecipRemoteCollab
                        E.on $ collab E.^. CollabId E.==. enable E.^. CollabEnableCollab
                        E.where_ $ collab E.^. CollabTopic E.==. E.val resourceID
                        return
                            ( collab E.^. CollabRole
                            , recipR E.^. CollabRecipRemoteActor
                            , deleg
                            )
                remoteExtensions <- lift $ for remoteCollabs $ \ (E.Value role', E.Value raID, Entity delegID (CollabDelegRemote enableID _recipID grantID)) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    leafID <- insert $ SourceUsLeaf sendID enableID extID
                    case from of
                        Left localID -> insert_ $ SourceUsLeafFromLocal leafID localID
                        Right remoteID -> insert_ $ SourceUsLeafFromRemote leafID remoteID
                    insert_ $ SourceUsLeafToRemote leafID delegID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForCollab (Right (raID, grantID)) (min role role') enableID
                    let recipByKey = LocalActorGroup groupID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                -- For each child of mine, prepare a delegation-extension Grant
                localChildren <-
                    lift $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. DestUsAcceptId E.==. start E.^. DestUsStartDest
                        E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
                        E.on $ topic E.^. DestTopicGroupTopic E.==. deleg E.^. DestThemSendDelegatorLocalTopic
                        E.on $ holder E.^. DestHolderGroupId E.==. topic E.^. DestTopicGroupHolder
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderGroupDest
                        E.where_ $ holder E.^. DestHolderGroupGroup E.==. E.val groupID
                        return
                            ( dest E.^. DestRole
                            , topic E.^. DestTopicGroupChild
                            , deleg E.^. DestThemSendDelegatorLocalId
                            , deleg E.^. DestThemSendDelegatorLocalGrant
                            , accept E.^. DestUsAcceptId
                            , start E.^. DestUsStartId
                            )
                localExtensionsForChildren <- lift $ for localChildren $ \ (E.Value role', E.Value childID, E.Value _delegID, E.Value grantID, E.Value _acceptID, E.Value startID) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    gatherID <- insert $ SourceUsGather sendID startID extID
                    case from of
                        Left localID -> insert_ $ SourceUsGatherFromLocal gatherID localID
                        Right remoteID -> insert_ $ SourceUsGatherFromRemote gatherID remoteID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForChild (Left (childID, grantID)) (min role role') startID
                    let recipByKey = LocalActorGroup groupID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                remoteChildren <-
                    lift $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. DestUsAcceptId E.==. start E.^. DestUsStartDest
                        E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
                        E.on $ topic E.^. DestTopicRemoteId E.==. deleg E.^. DestThemSendDelegatorRemoteTopic
                        E.on $ dest E.^. DestId E.==. topic E.^. DestTopicRemoteDest
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderGroupDest
                        E.where_ $ holder E.^. DestHolderGroupGroup E.==. E.val groupID
                        return
                            ( dest E.^. DestRole
                            , topic E.^. DestTopicRemoteTopic
                            , deleg E.^. DestThemSendDelegatorRemoteId
                            , deleg E.^. DestThemSendDelegatorRemoteGrant
                            , accept E.^. DestUsAcceptId
                            , start E.^. DestUsStartId
                            )
                remoteExtensionsForChildren <- lift $ for remoteChildren $ \ (E.Value role', E.Value childID, E.Value _delegID, E.Value grantID, E.Value _acceptID, E.Value startID) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    gatherID <- insert $ SourceUsGather sendID startID extID
                    case from of
                        Left localID -> insert_ $ SourceUsGatherFromLocal gatherID localID
                        Right remoteID -> insert_ $ SourceUsGatherFromRemote gatherID remoteID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForChild (Right (childID, grantID)) (min role role') startID
                    let recipByKey = LocalActorGroup groupID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return
                    ( recipActorID
                    , localExtensions ++ localExtensionsForChildren
                    , remoteExtensions ++ remoteExtensionsForChildren
                    , inboxItemID
                    )

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, localExts, remoteExts, inboxItemID) -> do
                let recipByID = LocalActorGroup groupID
                lift $ for_ (localExts ++ remoteExts) $
                    \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            recipByID recipActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "[Parent] Sent extensions to collabs & children"

    tryAlmostChild (GKDelegationStart _)    = lift mzero
    tryAlmostChild (GKDelegationExtend _ _) = lift mzero
    tryAlmostChild GKDelegator              = do
        uFulfills <-
            case AP.activityFulfills $ actbActivity body of
                [] -> throwE "No fulfills"
                [u] -> pure u
                _ -> throwE "Multiple fulfills"
        fulfills <- ExceptT $ lift $ lift $ runExceptT $ first (\ (a, _, i) -> (a, i)) <$> parseActivityURI' uFulfills
        fulfillsDB <- ExceptT $ MaybeT $ either (Just . Left) (fmap Right) <$> runExceptT (getActivity fulfills)
        -- Find the Dest record from the fulfills
        destID <-
            lift $
            case fulfillsDB of
                Left (_, _, addID) ->
                    (do DestUsGestureLocal destID _ <- MaybeT $ getValBy $ UniqueDestUsGestureLocalActivity addID
                        _ <- MaybeT $ getBy $ UniqueDestOriginUs destID
                        return destID
                    )
                    <|>
                    (do DestThemGestureLocal themID _ <- MaybeT $ getValBy $ UniqueDestThemGestureLocalAdd addID
                        DestOriginThem destID <- lift $ getJust themID
                        return destID
                    )
                Right addID ->
                    (do DestUsGestureRemote destID _ _ <- MaybeT $ getValBy $ UniqueDestUsGestureRemoteActivity addID
                        _ <- MaybeT $ getBy $ UniqueDestOriginUs destID
                        return destID
                    )
                    <|>
                    (do DestThemGestureRemote themID _ _ <- MaybeT $ getValBy $ UniqueDestThemGestureRemoteAdd addID
                        DestOriginThem destID <- lift $ getJust themID
                        return destID
                    )
        -- Verify this Dest record is mine
        DestHolderGroup _ g <- lift $ MaybeT $ getValBy $ UniqueDestHolderGroup destID
        lift $ guard $ g == groupID
        -- Verify the Grant sender is the Dest topic
        topic <- do
            t <- lift $ lift $ getDestTopic destID
            bitraverse
                (bitraverse
                    pure
                    (\case
                        Right g -> pure g
                        Left _j -> error "I have a DestTopic that is a Project"
                    )
                )
                pure
                t
        topicForCheck <-
            lift $ lift $
            bitraverse
                (pure . snd)
                (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                topic
        unless (first LocalActorGroup topicForCheck == bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig) $
            throwE "Dest topic and Grant author aren't the same actor"
        -- Verify I sent my Accept
        maybeMe <- lift $ lift $ getKeyBy $ UniqueDestUsAccept destID
        meAcceptID <- fromMaybeE maybeMe "I haven't sent my Accept"
        -- Verify I haven't yet seen a delegator-Grant from the parent
        case bimap fst fst topic of
            Left localID -> do
                m <- lift $ lift $ getBy $ UniqueDestThemSendDelegatorLocalTopic localID
                verifyNothingE m "Already have a DestThemSendDelegatorLocal"
            Right remoteID -> do
                m <- lift $ lift $ getBy $ UniqueDestThemSendDelegatorRemoteTopic remoteID
                verifyNothingE m "Already have a DestThemSendDelegatorRemote"
        Dest role <- lift $ lift $ getJust destID
        return (role, topic, meAcceptID)

    handleAlmostChild role topic acceptID = do

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust groupID
                let actorID = groupActor recip
                (actorID,) <$> getJust actorID

            maybeGrantDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeGrantDB $ \ (inboxItemID, grantDB) -> do

                -- Record the delegator-Grant in DB
                to <- case (grantDB, bimap fst fst topic) of
                    (Left (_, _, grantID), Left localID) -> Left <$> do
                        mk <- lift $ insertUnique $ DestThemSendDelegatorLocal acceptID localID grantID
                        fromMaybeE mk "I already have such a DestThemSendDelegatorLocal"
                    (Right (_, _, grantID), Right remoteID) -> Right <$> do
                        mk <- lift $ insertUnique $ DestThemSendDelegatorRemote acceptID remoteID grantID
                        fromMaybeE mk "I already have such a DestThemSendDelegatorRemote"
                    _ -> error "groupGrant.child impossible"

                startID <- lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
                destStartID <- lift $ insert $ DestUsStart acceptID startID

                -- Prepare a start-Grant
                start@(actionStart, _, _, _) <- lift $ prepareStartGrant role destStartID
                let recipByKey = LocalActorGroup groupID
                _luStart <- lift $ updateOutboxItem' recipByKey startID actionStart

                -- For each Project in me, prepare a delegation-extension Grant
                l' <-
                    lift $ fmap (map $ over _2 Left) $
                    E.select $ E.from $ \ (effort `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send `E.InnerJoin` deleg) -> do
                        E.on $ accept E.^. EffortThemAcceptLocalId E.==. deleg E.^. EffortThemDelegateLocalEffort
                        E.on $ effort E.^. EffortId E.==. send E.^. EffortUsSendDelegatorEffort
                        E.on $ topic E.^. EffortTopicLocalId E.==. accept E.^. EffortThemAcceptLocalTopic
                        E.on $ effort E.^. EffortId E.==. topic E.^. EffortTopicLocalEffort
                        E.where_ $ effort E.^. EffortHolder E.==. E.val groupID
                        return
                            ( send E.^. EffortUsSendDelegatorId
                            , deleg
                            )
                r' <-
                    lift $ fmap (map $ over _2 Right) $
                    E.select $ E.from $ \ (effort `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send `E.InnerJoin` deleg) -> do
                        E.on $ accept E.^. EffortThemAcceptRemoteId E.==. deleg E.^. EffortThemDelegateRemoteEffort
                        E.on $ effort E.^. EffortId E.==. send E.^. EffortUsSendDelegatorEffort
                        E.on $ topic E.^. EffortTopicRemoteId E.==. accept E.^. EffortThemAcceptRemoteTopic
                        E.on $ effort E.^. EffortId E.==. topic E.^. EffortTopicRemoteEffort
                        E.where_ $ effort E.^. EffortHolder E.==. E.val groupID
                        return
                            ( send E.^. EffortUsSendDelegatorId
                            , deleg
                            )
                fromProjects <- lift $ for (l' ++ r') $ \ (E.Value sendID, deleg) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now

                    distributeID <- insert $ EffortUsDistribute sendID destStartID extID
                    case bimap entityKey entityKey deleg of
                        Left localID -> insert_ $ EffortUsDistributeFromLocal distributeID localID
                        Right remoteID -> insert_ $ EffortUsDistributeFromRemote distributeID remoteID

                    (AP.Doc h a, grant) <- getGrantActivityBody $ bimap (effortThemDelegateLocalGrant . entityVal) (effortThemDelegateRemoteGrant . entityVal) deleg
                    uStart <-
                        case AP.activityId a of
                            Nothing -> error "EffortThemDelegate grant has no 'id'"
                            Just lu -> pure $ ObjURI h lu
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantFromParentOrProject uStart grant role destStartID
                    let recipByKey = LocalActorGroup groupID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                -- For each Grant I got from a child, prepare a
                -- delegation-extension Grant
                l <-
                    lift $ fmap (map $ over _2 Left) $
                    E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send `E.InnerJoin` deleg) -> do
                        E.on $ accept E.^. SourceThemAcceptLocalId E.==. deleg E.^. SourceThemDelegateLocalSource
                        E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                        E.on $ topic E.^. SourceTopicLocalId E.==. accept E.^. SourceThemAcceptLocalTopic
                        E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicLocalSource
                        E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderGroupSource
                        E.where_ $ holder E.^. SourceHolderGroupGroup E.==. E.val groupID
                        return
                            ( send E.^. SourceUsSendDelegatorId
                            , deleg
                            )
                r <-
                    lift $ fmap (map $ over _2 Right) $
                    E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send `E.InnerJoin` deleg) -> do
                        E.on $ accept E.^. SourceThemAcceptRemoteId E.==. deleg E.^. SourceThemDelegateRemoteSource
                        E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                        E.on $ topic E.^. SourceTopicRemoteId E.==. accept E.^. SourceThemAcceptRemoteTopic
                        E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicRemoteSource
                        E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderGroupSource
                        E.where_ $ holder E.^. SourceHolderGroupGroup E.==. E.val groupID
                        return
                            ( send E.^. SourceUsSendDelegatorId
                            , deleg
                            )
                fromParents <- lift $ for (l ++ r) $ \ (E.Value sendID, deleg) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now

                    gatherID <- insert $ SourceUsGather sendID destStartID extID
                    case bimap entityKey entityKey deleg of
                        Left localID -> insert_ $ SourceUsGatherFromLocal gatherID localID
                        Right remoteID -> insert_ $ SourceUsGatherFromRemote gatherID remoteID

                    (AP.Doc h a, grant) <- getGrantActivityBody $ bimap (sourceThemDelegateLocalGrant . entityVal) (sourceThemDelegateRemoteGrant . entityVal) deleg
                    uStart <-
                        case AP.activityId a of
                            Nothing -> error "SourceThemDelegate grant has no 'id'"
                            Just lu -> pure $ ObjURI h lu
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantFromParentOrProject uStart grant role destStartID
                    let recipByKey = LocalActorGroup groupID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return
                    ( recipActorID
                    , (startID, start) : fromProjects ++ fromParents
                    , inboxItemID
                    )

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, exts, inboxItemID) -> do
                let recipByID = LocalActorGroup groupID
                lift $ for_ exts $
                    \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            recipByID recipActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "[Almost-child] Sent start-Grant and extensions from projects and parents"

        where

        prepareStartGrant role startID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            groupHash <- encodeKeyHashid groupID

            uDeleg <- lift $ getActivityURI authorIdMsig

            audChild <- lift $ makeAudSenderOnly authorIdMsig
            uChild <- lift $ getActorURI authorIdMsig

            resultR <- do
                startHash <- encodeKeyHashid startID
                return $ GroupChildLiveR groupHash startHash

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audChild]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uDeleg]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = encodeRouteHome $ GroupR groupHash
                        , AP.grantTarget    = uChild
                        , AP.grantResult    =
                            Just
                                ( encodeRouteLocal resultR
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Distribute
                        , AP.grantDelegates = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtensionGrantFromParentOrProject uStart grant role startID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            groupHash <- encodeKeyHashid groupID
            finalRole <-
                case AP.grantObject grant of
                    AP.RXRole r -> pure $ min role r
                    AP.RXDelegator -> error "Why was I delegated a Grant with object=delegator?"

            uDeleg <- lift $ getActivityURI authorIdMsig
            audChild <- lift $ makeAudSenderOnly authorIdMsig
            uChild <- lift $ getActorURI authorIdMsig

            resultR <- do
                startHash <- encodeKeyHashid startID
                return $ GroupChildLiveR groupHash startHash

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audChild]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole finalRole
                        , AP.grantContext   = AP.grantContext grant
                        , AP.grantTarget    = uChild
                        , AP.grantResult    =
                            Just
                                ( encodeRouteLocal resultR
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Distribute
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    prepareExtensionGrantForCollab collab role enableID = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        groupHash <- encodeKeyHashid groupID
        uStart <- lift $ getActivityURI authorIdMsig

        (uCollab, audCollab, uDeleg) <-
            case collab of
                Left (personID, itemID) -> do
                    personHash <- encodeKeyHashid personID
                    itemHash <- encodeKeyHashid itemID
                    return
                        ( encodeRouteHome $ PersonR personHash
                        , AudLocal [LocalActorPerson personHash] []
                        , encodeRouteHome $
                            PersonOutboxItemR personHash itemHash
                        )
                Right (raID, ractID) -> do
                    ra <- getJust raID
                    u@(ObjURI h lu) <- getRemoteActorURI ra
                    uAct <- do
                        ract <- getJust ractID
                        getRemoteActivityURI ract
                    return (u, AudRemote h [lu] [], uAct)

        enableHash <- encodeKeyHashid enableID

        let audience = [audCollab]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Just uDeleg
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uStart]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXRole role
                    , AP.grantContext   = AP.grantContext grant
                    , AP.grantTarget    = uCollab
                    , AP.grantResult    =
                        Just
                            (encodeRouteLocal $
                                GroupMemberLiveR groupHash enableHash
                            , Nothing
                            )
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Just uStart
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    prepareExtensionGrantForChild child role startID = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        groupHash <- encodeKeyHashid groupID
        uStart <- lift $ getActivityURI authorIdMsig

        (uChild, audChild, uDeleg) <-
            case child of
                Left (g, itemID) -> do
                    h <- encodeKeyHashid g
                    itemHash <- encodeKeyHashid itemID
                    return
                        ( encodeRouteHome $ GroupR h
                        , AudLocal [LocalActorGroup h] []
                        , encodeRouteHome $
                            GroupOutboxItemR h itemHash
                        )
                Right (raID, ractID) -> do
                    ra <- getJust raID
                    u@(ObjURI h lu) <- getRemoteActorURI ra
                    uAct <- do
                        ract <- getJust ractID
                        getRemoteActivityURI ract
                    return (u, AudRemote h [lu] [], uAct)

        resultR <- do
            startHash <- encodeKeyHashid startID
            return $
                GroupChildLiveR groupHash startHash

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audChild]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Just uDeleg
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uStart]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXRole role
                    , AP.grantContext   = AP.grantContext grant
                    , AP.grantTarget    = uChild
                    , AP.grantResult    =
                        Just (encodeRouteLocal resultR, Nothing)
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Distribute
                    , AP.grantDelegates = Just uStart
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor A invited actor B to a resource
-- Behavior:
--      * Verify the resource is my collabs list
--      * If resource is collabs and B is local, verify it's a Person
--      * Verify A isn't inviting themselves
--      * Verify A is authorized by me to invite collabs to me
--
--      * Verify B doesn't already have an invite/join/grant for me
--
--      * Insert the Invite to my inbox
--
--      * Insert a Collab record to DB
--
--      * Forward the Invite to my followers
--      * Send Accept to A, B, my-followers
groupInvite
    :: UTCTime
    -> GroupId
    -> Verse
    -> AP.Invite URIMode
    -> ActE (Text, Act (), Next)
groupInvite now groupID (Verse authorIdMsig body) invite = do

    -- Check capability
    capability <- do

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the capability URI is one of:
        --   * Outbox item URI of a local actor, i.e. a local activity
        --   * A remote URI
        cap <- nameExceptT "Invite capability" $ parseActivityURI' uCap

        -- Verify the capability is local
        case cap of
            Left (actorByKey, _, outboxItemID) ->
                return (actorByKey, outboxItemID)
            _ -> throwE "Capability is remote i.e. definitely not by me"

    -- Check invite
    (role, invited) <- do
        let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
        (role, resourceOrComps, recipientOrComp) <- parseInvite author invite
        mode <-
            case resourceOrComps of
                Left (Left (LocalResourceGroup j)) | j == groupID ->
                    bitraverse
                        (\case
                            Left r -> pure r
                            Right _ -> throwE "Not accepting local component actors as collabs"
                        )
                        pure
                        recipientOrComp
                _ -> throwE "Invite topic isn't my collabs URI"
        return (role, mode)

    -- If target is local, find it in our DB
    -- If target is remote, HTTP GET it, verify it's an actor, and store in
    -- our DB (if it's already there, no need for HTTP)
    --
    -- NOTE: This is a blocking HTTP GET done right here in the Invite handler,
    -- which is NOT a good idea. Ideally, it would be done async, and the
    -- handler result (approve/disapprove the Invite) would be sent later in a
    -- separate (e.g. Accept) activity. But for the PoC level, the current
    -- situation will hopefully do.
    invitedDB <-
        bitraverse
            (withDBExcept . flip getGrantRecip "Invitee not found in DB")
            getRemoteActorFromURI
            invited

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        resourceID <- lift $ groupResource <$> getJust groupID
        Resource topicActorID <- lift $ getJust resourceID
        topicActor <- lift $ getJust topicActorID

        -- Verify the specified capability gives relevant access
        verifyCapability'
            capability authorIdMsig (LocalResourceGroup groupID) AP.RoleAdmin

        -- Verify that target doesn't already have a Collab for me
        existingCollabIDs <- lift $ getExistingCollabs resourceID invitedDB
        case existingCollabIDs of
            [] -> pure ()
            [_] -> throwE "I already have a Collab for the target"
            _ -> error "Multiple collabs found for target"

        maybeInviteDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
        lift $ for maybeInviteDB $ \ (inboxItemID, inviteDB) -> do

            -- Insert Collab or Component record to DB
            acceptID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
            insertCollab resourceID role invitedDB inviteDB acceptID

            -- Prepare forwarding Invite to my followers
            sieve <- do
                groupHash <- encodeKeyHashid groupID
                return $ makeRecipientSet [] [LocalStageGroupFollowers groupHash]

            -- Prepare an Accept activity and insert to my outbox
            accept@(actionAccept, _, _, _) <- prepareAccept invitedDB
            _luAccept <- updateOutboxItem' (LocalActorGroup groupID) acceptID actionAccept

            return (topicActorID, sieve, acceptID, accept, inboxItemID)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (groupActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
            forwardActivity
                authorIdMsig body (LocalActorGroup groupID) groupActorID sieve
            lift $ sendActivity
                (LocalActorGroup groupID) groupActorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            doneDB inboxItemID "Recorded and forwarded the Invite, sent an Accept"

    where

    getRemoteActorFromURI (ObjURI h lu) = do
        instanceID <-
            lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
        result <-
            ExceptT $ first (T.pack . displayException) <$>
                fetchRemoteActor' instanceID h lu
        case result of
            Left Nothing -> throwE "Target @id mismatch"
            Left (Just err) -> throwE $ T.pack $ displayException err
            Right Nothing -> throwE "Target isn't an actor"
            Right (Just actor) -> return $ entityKey actor

    getExistingCollabs resourceID (Left (GrantRecipPerson (Entity personID _))) =
        E.select $ E.from $ \ (collab `E.InnerJoin` recipl) -> do
            E.on $
                collab E.^. CollabId E.==.
                recipl E.^. CollabRecipLocalCollab
            E.where_ $
                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                recipl E.^. CollabRecipLocalPerson E.==. E.val personID
            return $ recipl E.^. CollabRecipLocalCollab
    getExistingCollabs resourceID (Right remoteActorID) =
        E.select $ E.from $ \ (collab `E.InnerJoin` recipr) -> do
            E.on $
                collab E.^. CollabId E.==.
                recipr E.^. CollabRecipRemoteCollab
            E.where_ $
                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                recipr E.^. CollabRecipRemoteActor E.==. E.val remoteActorID
            return $ recipr E.^. CollabRecipRemoteCollab

    insertCollab resourceID role recipient inviteDB acceptID = do
        collabID <- insert $ Collab role resourceID
        fulfillsID <- insert $ CollabFulfillsInvite collabID acceptID
        case inviteDB of
            Left (_, _, inviteID) ->
                insert_ $ CollabInviterLocal fulfillsID inviteID
            Right (author, _, inviteID) -> do
                let authorID = remoteAuthorId author
                insert_ $ CollabInviterRemote fulfillsID authorID inviteID
        case recipient of
            Left (GrantRecipPerson (Entity personID _)) ->
                insert_ $ CollabRecipLocal collabID personID
            Right remoteActorID ->
                insert_ $ CollabRecipRemote collabID remoteActorID

    prepareAccept invitedDB = do
        encodeRouteHome <- getEncodeRouteHome

        audInviter <- lift $ makeAudSenderOnly authorIdMsig
        audInvited <-
            case invitedDB of
                Left (GrantRecipPerson (Entity p _)) -> do
                    ph <- encodeKeyHashid p
                    return $ AudLocal [LocalActorPerson ph] []
                Right remoteActorID -> do
                    ra <- getJust remoteActorID
                    ObjURI h lu <- getRemoteActorURI ra
                    return $ AudRemote h [lu] []
        audTopic <-
            AudLocal [] . pure . LocalStageGroupFollowers <$>
                encodeKeyHashid groupID
        uInvite <- lift $ getActivityURI authorIdMsig

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audInviter, audInvited, audTopic]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uInvite]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uInvite
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor A asked to join a resource
-- Behavior:
--      * Verify the resource is me
--      * Verify A doesn't already have an invite/join/grant for me
--      * Remember the join in DB
--      * Forward the Join to my followers
groupJoin
    :: UTCTime
    -> GroupId
    -> Verse
    -> AP.Join URIMode
    -> ActE (Text, Act (), Next)
groupJoin = topicJoin groupResource LocalResourceGroup

-- Meaning: An actor rejected something
-- Behavior:
--     * If it's on an Invite where I'm the resource:
--         * Verify the Reject is by the Invite target
--         * Remove the relevant Collab record from DB
--         * Forward the Reject to my followers
--         * Send a Reject on the Invite:
--             * To: Rejecter (i.e. Invite target)
--             * CC: Invite sender, Rejecter's followers, my followers
--     * If it's on a Join where I'm the resource:
--         * Verify the Reject is authorized
--         * Remove the relevant Collab record from DB
--         * Forward the Reject to my followers
--         * Send a Reject:
--             * To: Join sender
--             * CC: Reject sender, Join sender's followers, my followers
--     * Otherwise respond with error
groupReject
    :: UTCTime
    -> GroupId
    -> Verse
    -> AP.Reject URIMode
    -> ActE (Text, Act (), Next)
groupReject = topicReject groupResource LocalResourceGroup

-- Meaning: An actor A is removing actor B from collection C
-- Behavior:
--      * If C is my collaborators collection:
--          * Verify A isn't removing themselves
--          * Verify A is authorized by me to remove actors from me
--          * Verify B already has a Grant for me
--          * Remove the whole Collab record from DB
--          * Forward the Remove to my followers
--          * Send a Revoke:
--              * To: Actor B
--              * CC: Actor A, B's followers, my followers
--      * If C is my parents collection:
--          * Verify A isn't removing themselves
--          * Verify A is authorized by me to remove actors from me
--          * Verify B is an active child of mine
--          * Remove the whole Source record from DB
--          * Forward the Remove to my followers
--          * Send a Revoke on the delegator-Grant I had for B:
--              * To: Actor B
--              * CC: Actor A, B's followers, my followers
--          * Send a Revoke on every extention-Grant I extended on every
--            delegation Grant I got from B
--              * To: The child/member to whom I'd sent the Grant
--              * CC: -
--      * If C is my children collection:
--          * Verify A isn't removing themselves
--          * Verify A is authorized by me to remove actors from me
--          * Verify B is an active parent of mine
--          * Remove the whole Dest record from DB
--          * Forward the Remove to my followers
--          * Send an Accept on the Remove:
--              * To: Actor B
--              * CC: Actor A, B's followers, my followers
--      * If C is my resources collection:
--          * Verify A is authorized by me to remove resources from me
--          * Verify B is an active resource of mine
--          * Remove the whole Effort record from DB
--          * Forward the Remove to my followers
--          * Send a Revoke on the delegator-Grant I had for B:
--              * To: Actor B
--              * CC: Actor A, B's followers, my followers
--          * Send a Revoke on every extention-Grant I extended on every
--            delegation Grant I got from B
--              * To: The child/member to whom I'd sent the Grant
--              * CC: -
--      * If I'm B, being removed from the children of a parent of mine:
--          * Record this Remove in the Source record
--          * Forward to followers
--      * If I'm B, being removed from the parents of a child of mine:
--          * Do nothing, just waiting for parent to send a Revoke on the
--            delegator-Grant
--      * If I'm B, being removed from the teams of a resource of mine:
--          * Record this Remove in the Effort record
--          * Forward to followers
groupRemove
    :: UTCTime
    -> GroupId
    -> Verse
    -> AP.Remove URIMode
    -> ActE (Text, Act (), Next)
groupRemove now groupID (Verse authorIdMsig body) remove = do

    let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
    (collection, item) <- parseRemove author remove
    case (collection, item) of
        (Left (Left (LocalResourceGroup j)), _) | j == groupID ->
            removeCollab item
        (Left (Right (ATGroupChildren j)), _) | j == groupID ->
            removeChildActive item
        (Left (Right (ATGroupParents j)), _) | j == groupID ->
            removeParentActive item
        (Left (Right (ATGroupEfforts j)), _) | j == groupID ->
            removeResourceActive item
        (_, Left (LocalActorGroup j)) | j == groupID ->
            case collection of
                Left (Right (ATGroupParents j)) | j /= groupID ->
                    removeChildPassive $ Left j
                Left (Right (ATGroupChildren j)) | j /= groupID ->
                    removeParentPassive $ Left j
                Left (Right at) | isJust $ addTargetResourceTeams at ->
                    removeResourcePassive $ Left $ fromJust $ addTargetResourceTeams at
                Right (ObjURI h luColl) -> do
                    -- NOTE this is HTTP GET done synchronously in the activity
                    -- handler
                    manager <- asksEnv envHttpManager
                    c <- AP.fetchAPID_T manager (AP.collectionId :: AP.Collection FedURI URIMode -> LocalURI) h luColl
                    lu <- fromMaybeE (AP.collectionContext c) "No context"
                    rwc <- AP.fetchRWC_T manager h lu
                    AP.Actor l d <-
                        case AP.rwcResource rwc of
                            AP.ResourceActor a -> pure a
                            AP.ResourceChild _ _ -> throwE "Remove.origin remote ResourceChild"
                    let typ = AP.actorType d
                    if typ == AP.ActorTypeTeam && Just luColl == AP.rwcSubteams rwc
                        then removeParentPassive $ Right $ ObjURI h lu
                    else if typ == AP.ActorTypeTeam && Just luColl == AP.rwcParentsOrProjects rwc
                        then removeChildPassive $ Right $ ObjURI h lu
                    else if AP.actorTypeIsResourceNT typ && Just luColl == AP.rwcTeams rwc
                        then removeResourcePassive $ Right $ ObjURI h lu
                    else throwE "Weird collection situation"
                _ -> throwE "I'm being removed from somewhere irrelevant"
        _ -> throwE "This Remove isn't for me"

    where

    removeCollab member = do

        -- Check remove
        memberByKey <-
            bitraverse
                (\case
                    LocalActorPerson p -> pure p
                    _ -> throwE "Not accepting non-person actors as collabs"
                )
                pure
                member

        -- Verify the specified capability gives relevant access
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceGroup groupID)
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Find member in our DB
            memberDB <-
                bitraverse
                    (flip getEntityE "Member not found in DB")
                    (\ u@(ObjURI h lu) -> (,u) <$> do
                        maybeActor <- lift $ runMaybeT $ do
                            iid <- MaybeT $ getKeyBy $ UniqueInstance h
                            roid <- MaybeT $ getKeyBy $ UniqueRemoteObject iid lu
                            MaybeT $ getBy $ UniqueRemoteActor roid
                        fromMaybeE maybeActor "Remote removee not found in DB"
                    )
                    memberByKey

            -- Grab me from DB
            resourceID <- lift $ groupResource <$> getJust groupID
            Resource topicActorID <- lift $ getJust resourceID
            topicActor <- lift $ getJust topicActorID

            -- Find the collab that the member already has for me
            existingCollabIDs <-
                lift $ case memberDB of
                    Left (Entity personID _) ->
                        fmap (map $ over _1 Left) $
                        E.select $ E.from $ \ (collab `E.InnerJoin` recipl) -> do
                            E.on $
                                collab E.^. CollabId E.==.
                                recipl E.^. CollabRecipLocalCollab
                            E.where_ $
                                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                                recipl E.^. CollabRecipLocalPerson E.==. E.val personID
                            return
                                ( recipl E.^. persistIdField
                                , recipl E.^. CollabRecipLocalCollab
                                )
                    Right (Entity remoteActorID _, _) ->
                        fmap (map $ over _1 Right) $
                        E.select $ E.from $ \ (collab `E.InnerJoin` recipr) -> do
                            E.on $
                                collab E.^. CollabId E.==.
                                recipr E.^. CollabRecipRemoteCollab
                            E.where_ $
                                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                                recipr E.^. CollabRecipRemoteActor E.==. E.val remoteActorID
                            return
                                ( recipr E.^. persistIdField
                                , recipr E.^. CollabRecipRemoteCollab
                                )
            (recipID, E.Value collabID) <-
                case existingCollabIDs of
                    [] -> throwE "Remove object isn't a member of me"
                    [collab] -> return collab
                    _ -> error "Multiple collabs found for removee"

            -- Verify the Collab is enabled
            maybeEnabled <- lift $ getBy $ UniqueCollabEnable collabID
            Entity enableID (CollabEnable _ grantID) <-
                fromMaybeE maybeEnabled "Remove object isn't a member of me yet"

            -- Verify that at least 1 more enabled Admin collab for me exists
            otherCollabIDs <-
               lift $  E.select $ E.from $ \ (collab `E.InnerJoin` enable) -> do
                    E.on $
                        collab E.^. CollabId E.==.
                        enable E.^. CollabEnableCollab
                    E.where_ $
                        collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                        collab E.^. CollabId E.!=. E.val collabID E.&&.
                        collab E.^. CollabRole E.==. E.val AP.RoleAdmin
                    return $ collab E.^. CollabId
            when (null otherCollabIDs) $
                throwE "No other admins exist, can't remove"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                -- Delete the whole Collab record
                deleteBy $ UniqueCollabDelegLocal enableID
                deleteBy $ UniqueCollabDelegRemote enableID
                delete enableID
                case recipID of
                    Left (E.Value l) -> do
                        deleteBy $ UniqueCollabRecipLocalJoinCollab l
                        deleteBy $ UniqueCollabRecipLocalAcceptCollab l
                        delete l
                    Right (E.Value r) -> do
                        deleteBy $ UniqueCollabRecipRemoteJoinCollab r
                        deleteBy $ UniqueCollabRecipRemoteAcceptCollab r
                        delete r
                fulfills <- do
                    mf <- runMaybeT $ asum
                        [ Left <$> MaybeT (getKeyBy $ UniqueCollabFulfillsLocalTopicCreation collabID)
                        , Right . Left <$> MaybeT (getKeyBy $ UniqueCollabFulfillsInvite collabID)
                        , Right . Right <$> MaybeT (getKeyBy $ UniqueCollabFulfillsJoin collabID)
                        ]
                    maybe (error $ "No fulfills for collabID#" ++ show collabID) pure mf
                case fulfills of
                    Left fc -> delete fc
                    Right (Left fi) -> do
                        deleteBy $ UniqueCollabInviterLocal fi
                        deleteBy $ UniqueCollabInviterRemote fi
                        delete fi
                    Right (Right fj) -> do
                        deleteBy $ UniqueCollabApproverLocal fj
                        deleteBy $ UniqueCollabApproverRemote fj
                        delete fj
                delete collabID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid groupID
                    let topicByHash =
                            LocalActorGroup topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare a Revoke activity and insert to my outbox
                revoke@(actionRevoke, _, _, _) <-
                    lift $ prepareRevoke memberDB grantID
                let recipByKey = LocalActorGroup groupID
                revokeID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
                _luRevoke <- updateOutboxItem' recipByKey revokeID actionRevoke

                return (topicActorID, sieve, revokeID, revoke, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, revokeID, (actionRevoke, localRecipsRevoke, remoteRecipsRevoke, fwdHostsRevoke), inboxItemID) -> do
                let topicByID = LocalActorGroup groupID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $ sendActivity
                    topicByID topicActorID localRecipsRevoke
                    remoteRecipsRevoke fwdHostsRevoke revokeID actionRevoke
                doneDB inboxItemID "[Collab] Deleted the Grant/Collab, forwarded Remove, sent Revoke"

        where

        prepareRevoke member grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            recipHash <- encodeKeyHashid groupID
            let topicByHash = LocalActorGroup recipHash

            memberHash <- bitraverse (encodeKeyHashid . entityKey) pure member

            audRemover <- makeAudSenderOnly authorIdMsig
            let audience =
                    let audMember =
                            case memberHash of
                                Left p ->
                                    AudLocal [LocalActorPerson p] [LocalStagePersonFollowers p]
                                Right (Entity _ actor, ObjURI h lu) ->
                                    AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)
                        audTopic = AudLocal [] [localActorFollowers topicByHash]
                    in  [audRemover, audMember, audTopic]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience audience

                recips = map encodeRouteHome audLocal ++ audRemote
            uRemove <- getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    removeParentActive parent = do

        -- If parent is local, find it in our DB
        -- If parent is remote, HTTP GET it, verify it's an actor of Group
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        parentDB <-
            bitraverse
                (\case
                    LocalActorGroup j -> withDBExcept $ getEntityE j "Parent not found in DB"
                    _ -> throwE "Local proposed parent of non-group type"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Parent @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Parent isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote parent type isn't Group"
                            return (u, actor)
                )
                parent

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the sender is authorized by me to remove a parent
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceGroup groupID)
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            -- Verify it's an active parent of mine
            sources <- lift $ case parentDB of
                Left (Entity j _) ->
                    fmap (map $ \ (s, h, d, E.Value a, E.Value t, E.Value i) -> (s, h, d, Left (a, t, i))) $
                    E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send) -> do
                        E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                        E.on $ topic E.^. SourceTopicGroupTopic E.==. accept E.^. SourceThemAcceptLocalTopic
                        E.on $ holder E.^. SourceHolderGroupId E.==. topic E.^. SourceTopicGroupHolder
                        E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderGroupSource
                        E.where_ $
                            holder E.^. SourceHolderGroupGroup E.==. E.val groupID E.&&.
                            topic E.^. SourceTopicGroupParent E.==. E.val j
                        return
                            ( source E.^. SourceId
                            , holder E.^. SourceHolderGroupId
                            , send
                            , accept E.^. SourceThemAcceptLocalId
                            , topic E.^. SourceTopicGroupTopic
                            , topic E.^. SourceTopicGroupId
                            )
                Right (_, Entity a _) ->
                    fmap (map $ \ (s, h, d, E.Value a, E.Value t) -> (s, h, d, Right (a, t))) $
                    E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send) -> do
                        E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                        E.on $ topic E.^. SourceTopicRemoteId E.==. accept E.^. SourceThemAcceptRemoteTopic
                        E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicRemoteSource
                        E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderGroupSource
                        E.where_ $
                            holder E.^. SourceHolderGroupGroup E.==. E.val groupID E.&&.
                            topic E.^. SourceTopicRemoteTopic E.==. E.val a
                        return
                            ( source E.^. SourceId
                            , holder E.^. SourceHolderGroupId
                            , send
                            , accept E.^. SourceThemAcceptRemoteId
                            , topic E.^. SourceTopicRemoteId
                            )
            (E.Value sourceID, E.Value holderID, Entity sendID (SourceUsSendDelegator _ grantID), topic) <-
                verifySingleE sources "No source" "Multiple sources"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                -- Grab extension-Grants that I'm about to revoke
                gathers <- selectList [SourceUsGatherSource ==. sendID] []
                leafs <- selectList [SourceUsLeafSource ==. sendID] []

                -- Delete the whole Source record
                deleteWhere [SourceRemoveSend ==. sendID]
                let gatherIDs = map entityKey gathers
                deleteWhere [SourceUsGatherFromLocalGather <-. gatherIDs]
                deleteWhere [SourceUsGatherFromRemoteGather <-. gatherIDs]
                deleteWhere [SourceUsGatherId <-. gatherIDs]
                let leafIDs = map entityKey leafs
                deleteWhere [SourceUsLeafFromLocalLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafFromRemoteLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafToLocalLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafToRemoteLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafId <-. leafIDs]
                case topic of
                    Left (localID, _, _) -> do
                        deleteWhere [SourceThemDelegateLocalSource ==. localID]
                        delete localID
                    Right (remoteID, _) -> do
                        deleteWhere [SourceThemDelegateRemoteSource ==. remoteID]
                        delete remoteID
                delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueSourceOriginUs sourceID)
                        (getKeyBy $ UniqueSourceOriginThem sourceID)
                        "Neither us nor them"
                        "Both us and them"
                case origin of
                    Left usID -> do
                        deleteBy $ UniqueSourceUsAccept usID
                        deleteBy $ UniqueSourceUsGestureLocal usID
                        deleteBy $ UniqueSourceUsGestureRemote usID
                        delete usID
                    Right themID -> do
                        deleteBy $ UniqueSourceThemGestureLocal themID
                        deleteBy $ UniqueSourceThemGestureRemote themID
                        delete themID
                case topic of
                    Left (_, l, j) -> delete j >> delete l
                    Right (_, r) -> delete r
                delete holderID
                delete sourceID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid groupID
                    let topicByHash =
                            LocalActorGroup topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare main Revoke activity and insert to my outbox
                revoke@(actionRevoke, _, _, _) <-
                    lift $ prepareMainRevoke parentDB grantID
                let recipByKey = LocalActorGroup groupID
                revokeID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                _luRevoke <- updateOutboxItem' recipByKey revokeID actionRevoke

                -- Prepare and insert Revokes on all the extension-Grants
                revokesG <- for gathers $ \ (Entity _ (SourceUsGather _ startID grantID)) -> do
                    DestUsStart acceptID _ <- getJust startID
                    DestUsAccept destID _ <- getJust acceptID
                    parent <- do
                        p <- getDestTopic destID
                        bitraverse
                            (\case
                                Right j -> pure $ LocalActorGroup j
                                Left _ -> error "I'm a group but I have a parent who is a Project"
                            )
                            pure
                            (bimap snd snd p)
                    return (parent, grantID)
                revokesL <- for leafs $ \ (Entity _ (SourceUsLeaf _ enableID grantID)) -> do
                    CollabEnable collabID _ <- getJust enableID
                    recip <- getCollabRecip collabID
                    return
                        ( bimap
                            (LocalActorPerson . collabRecipLocalPerson . entityVal)
                            (collabRecipRemoteActor . entityVal)
                            recip
                        , grantID
                        )
                revokes <- for (revokesG ++ revokesL) $ \ (actor, grantID) -> do
                    ext@(actionExt, _, _, _) <- prepareExtRevoke actor grantID
                    let recipByKey = LocalActorGroup groupID
                    extID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return (groupActor group, sieve, revokeID, revoke, revokes, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, revokeID, (actionRevoke, localRecipsRevoke, remoteRecipsRevoke, fwdHostsRevoke), revokes, inboxItemID) -> do
                let topicByID = LocalActorGroup groupID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $ do
                    sendActivity
                        topicByID topicActorID localRecipsRevoke
                        remoteRecipsRevoke fwdHostsRevoke revokeID actionRevoke
                    for_ revokes $ \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            topicByID topicActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "[Parent-active] Deleted the Source, forwarded Remove, sent Revokes"

        where

        prepareMainRevoke parent grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            recipHash <- encodeKeyHashid groupID
            let topicByHash = LocalActorGroup recipHash

            parentHash <- bitraverse (encodeKeyHashid . entityKey) pure parent

            audRemover <- makeAudSenderOnly authorIdMsig
            let audParent =
                    case parentHash of
                        Left j ->
                            AudLocal [LocalActorGroup j] [LocalStageGroupFollowers j]
                        Right (ObjURI h lu, Entity _ actor) ->
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)
                audMe = AudLocal [] [localActorFollowers topicByHash]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRemover, audParent, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote

            uRemove <- getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtRevoke recipient grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            groupHash <- encodeKeyHashid groupID
            let topicByHash = LocalActorGroup groupHash

            audRecip <-
                case recipient of
                    Left a -> do
                        h <- hashLocalActor a
                        return $ AudLocal [h] [localActorFollowers h]
                    Right actorID -> do
                        actor <- getJust actorID
                        ObjURI h lu <- getRemoteActorURI actor
                        return $
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRecip]

                recips = map encodeRouteHome audLocal ++ audRemote

            uRemove <- lift $ getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    removeChildActive child = do

        -- If child is local, find it in our DB
        -- If child is remote, HTTP GET it, verify it's an actor of Group
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        childDB <-
            bitraverse
                (\case
                    LocalActorGroup j -> withDBExcept $ getEntityE j "Child not found in DB"
                    _ -> throwE "Local proposed child of non-group type"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Child @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Child isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote child type isn't Group"
                            return (u, actor)
                )
                child

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the sender is authorized by me to remove a child
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceGroup groupID)
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            -- Verify it's an active child of mine
            dests <- lift $ case childDB of
                Left (Entity j _) ->
                    fmap (map $ \ (d, h, a, z, E.Value l, E.Value t, E.Value s) -> (d, h, a, z, Left (l, t, s))) $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` send `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. DestUsAcceptId E.==. start E.^. DestUsStartDest
                        E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
                        E.on $ topic E.^. DestTopicGroupTopic E.==. send E.^. DestThemSendDelegatorLocalTopic
                        E.on $ holder E.^. DestHolderGroupId E.==. topic E.^. DestTopicGroupHolder
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderGroupDest
                        E.where_ $
                            holder E.^. DestHolderGroupGroup E.==. E.val groupID E.&&.
                            topic E.^. DestTopicGroupChild E.==. E.val j
                        return
                            ( dest E.^. DestId
                            , holder E.^. DestHolderGroupId
                            , send E.^. DestThemSendDelegatorLocalDest
                            , start E.^. DestUsStartId
                            , topic E.^. DestTopicGroupTopic
                            , topic E.^. DestTopicGroupId
                            , send E.^. DestThemSendDelegatorLocalId
                            )
                Right (_, Entity a _) ->
                    fmap (map $ \ (d, h, a, z, E.Value t, E.Value s) -> (d, h, a, z, Right (t, s))) $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` send `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. DestUsAcceptId E.==. start E.^. DestUsStartDest
                        E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
                        E.on $ topic E.^. DestTopicRemoteId E.==. send E.^. DestThemSendDelegatorRemoteTopic
                        E.on $ dest E.^. DestId E.==. topic E.^. DestTopicRemoteDest
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderGroupDest
                        E.where_ $
                            holder E.^. DestHolderGroupGroup E.==. E.val groupID E.&&.
                            topic E.^. DestTopicRemoteTopic E.==. E.val a
                        return
                            ( dest E.^. DestId
                            , holder E.^. DestHolderGroupId
                            , send E.^. DestThemSendDelegatorRemoteDest
                            , start E.^. DestUsStartId
                            , topic E.^. DestTopicRemoteId
                            , send E.^. DestThemSendDelegatorRemoteId
                            )

            (E.Value destID, E.Value holderID, E.Value usAcceptID, E.Value destStartID, topic) <-
                verifySingleE dests "No dest" "Multiple dests"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                -- Delete uses of this Dest from my Project records
                --deleteWhere [ComponentGatherChild ==. destStartID]

                -- Delete uses of this Dest from my Source records
                gatherIDs <- selectKeysList [SourceUsGatherDest ==. destStartID] []
                deleteWhere [SourceUsGatherFromLocalGather <-. gatherIDs]
                deleteWhere [SourceUsGatherFromRemoteGather <-. gatherIDs]
                deleteWhere [SourceUsGatherId <-. gatherIDs]

                -- Delete the whole Dest record
                delete destStartID
                case topic of
                    Left (_, _, sendID) -> delete sendID
                    Right (_, sendID) -> delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueDestOriginUs destID)
                        (getKeyBy $ UniqueDestOriginThem destID)
                        "Neither us nor them"
                        "Both us and them"
                deleteBy $ UniqueDestUsGestureLocal destID
                deleteBy $ UniqueDestUsGestureRemote destID
                case origin of
                    Left usID -> delete usID
                    Right themID -> do
                        deleteBy $ UniqueDestThemAcceptLocal themID
                        deleteBy $ UniqueDestThemAcceptRemote themID
                        deleteBy $ UniqueDestThemGestureLocal themID
                        deleteBy $ UniqueDestThemGestureRemote themID
                        delete themID
                delete usAcceptID
                case topic of
                    Left (l, j, _) -> delete j >> delete l
                    Right (r, _) -> delete r
                delete holderID
                delete destID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid groupID
                    let topicByHash =
                            LocalActorGroup topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare Accept activity
                accept@(actionAccept, _, _, _) <- prepareAccept childDB
                let recipByKey = LocalActorGroup groupID
                acceptID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                _luAccept <- updateOutboxItem' recipByKey acceptID actionAccept

                return (groupActor group, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                let topicByID = LocalActorGroup groupID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $
                    sendActivity
                        topicByID topicActorID localRecipsAccept
                        remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "[Child-active] Deleted the Dest, forwarded Remove, sent Accept"

        where

        prepareAccept childDB = do
            encodeRouteHome <- getEncodeRouteHome

            audRemover <- lift $ makeAudSenderOnly authorIdMsig
            audChild <-
                case childDB of
                    Left (Entity j _) -> do
                        h <- encodeKeyHashid j
                        return $ AudLocal [LocalActorGroup h] [LocalStageGroupFollowers h]
                    Right (ObjURI h lu, Entity _ ra) ->
                        return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
            audMe <-
                AudLocal [] . pure . LocalStageGroupFollowers <$>
                    encodeKeyHashid groupID
            uRemove <- lift $ getActivityURI authorIdMsig

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRemover, audChild, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uRemove
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    removeParentPassive parent = do

        -- If parent is local, find it in our DB
        -- If parent is remote, HTTP GET it, verify it's an actor of Group
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        parentDB <-
            bitraverse
                (\ j -> withDBExcept $ getEntityE j "Parent not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Parent @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Parent isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote parent type isn't Group"
                            return (u, actor)
                )
                parent

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            -- Verify it's an active parent of mine
            sources <- lift $ case parentDB of
                Left (Entity j _) ->
                    fmap (map $ \ (s, h, d, E.Value a, E.Value t, E.Value i) -> (s, h, d, Left (a, t, i))) $
                    E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send) -> do
                        E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                        E.on $ topic E.^. SourceTopicGroupTopic E.==. accept E.^. SourceThemAcceptLocalTopic
                        E.on $ holder E.^. SourceHolderGroupId E.==. topic E.^. SourceTopicGroupHolder
                        E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderGroupSource
                        E.where_ $
                            holder E.^. SourceHolderGroupGroup E.==. E.val groupID E.&&.
                            topic E.^. SourceTopicGroupParent E.==. E.val j
                        return
                            ( source E.^. SourceId
                            , holder E.^. SourceHolderGroupId
                            , send
                            , accept E.^. SourceThemAcceptLocalId
                            , topic E.^. SourceTopicGroupTopic
                            , topic E.^. SourceTopicGroupId
                            )
                Right (_, Entity a _) ->
                    fmap (map $ \ (s, h, d, E.Value a, E.Value t) -> (s, h, d, Right (a, t))) $
                    E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send) -> do
                        E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                        E.on $ topic E.^. SourceTopicRemoteId E.==. accept E.^. SourceThemAcceptRemoteTopic
                        E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicRemoteSource
                        E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderGroupSource
                        E.where_ $
                            holder E.^. SourceHolderGroupGroup E.==. E.val groupID E.&&.
                            topic E.^. SourceTopicRemoteTopic E.==. E.val a
                        return
                            ( source E.^. SourceId
                            , holder E.^. SourceHolderGroupId
                            , send
                            , accept E.^. SourceThemAcceptRemoteId
                            , topic E.^. SourceTopicRemoteId
                            )
            (E.Value sourceID, E.Value holderID, Entity sendID (SourceUsSendDelegator _ grantID), topic) <-
                verifySingleE sources "No source" "Multiple sources"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRemoveDB $ \ (removeID, _) -> do

                -- Record the removal attempt
                insert_ $ SourceRemove sendID removeID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid groupID
                    let topicByHash =
                            LocalActorGroup topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                return (groupActor group, sieve, removeID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, inboxItemID) -> do
                let topicByID = LocalActorGroup groupID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                doneDB inboxItemID "[Parent-passive] Recorded removal attempt, forwarded Remove"

    removeChildPassive child = do

        -- If child is local, find it in our DB
        -- If child is remote, HTTP GET it, verify it's an actor of Group
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        childDB <-
            bitraverse
                (\ j -> withDBExcept $ getEntityE j "Child not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Child @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Child isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote child type isn't Group"
                            return (u, actor)
                )
                child

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            -- Verify it's an active child of mine
            dests <- lift $ case childDB of
                Left (Entity j _) ->
                    fmap (map $ \ (d, h, a, E.Value l, E.Value t, E.Value s) -> (d, h, a, Left (l, t, s))) $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` send) -> do
                        E.on $ topic E.^. DestTopicGroupTopic E.==. send E.^. DestThemSendDelegatorLocalTopic
                        E.on $ holder E.^. DestHolderGroupId E.==. topic E.^. DestTopicGroupHolder
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderGroupDest
                        E.where_ $
                            holder E.^. DestHolderGroupGroup E.==. E.val groupID E.&&.
                            topic E.^. DestTopicGroupChild E.==. E.val j
                        return
                            ( dest E.^. DestId
                            , holder E.^. DestHolderGroupId
                            , send E.^. DestThemSendDelegatorLocalDest
                            , topic E.^. DestTopicGroupTopic
                            , topic E.^. DestTopicGroupId
                            , send E.^. DestThemSendDelegatorLocalId
                            )
                Right (_, Entity a _) ->
                    fmap (map $ \ (d, h, a, E.Value t, E.Value s) -> (d, h, a, Right (t, s))) $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` send) -> do
                        E.on $ topic E.^. DestTopicRemoteId E.==. send E.^. DestThemSendDelegatorRemoteTopic
                        E.on $ dest E.^. DestId E.==. topic E.^. DestTopicRemoteDest
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderGroupDest
                        E.where_ $
                            holder E.^. DestHolderGroupGroup E.==. E.val groupID E.&&.
                            topic E.^. DestTopicRemoteTopic E.==. E.val a
                        return
                            ( dest E.^. DestId
                            , holder E.^. DestHolderGroupId
                            , send E.^. DestThemSendDelegatorRemoteDest
                            , topic E.^. DestTopicRemoteId
                            , send E.^. DestThemSendDelegatorRemoteId
                            )

            (E.Value destID, E.Value holderID, E.Value usAcceptID, topic) <-
                verifySingleE dests "No dest" "Multiple dests"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                return inboxItemID

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just inboxItemID ->
                doneDB inboxItemID "[Child-passive] Saw the removal attempt, just waiting for the Revoke"

    removeResourceActive resource = do

        -- If resource is local, find it in our DB
        -- If resource is remote, HTTP GET it, verify it's an actor of Group
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        resourceDB <-
            bitraverse
                (\ la ->
                    case resourceToNG =<< actorToResource la of
                        Just ng -> withDBExcept $ getLocalResourceEntityE (resourceFromNG ng) "Resource not found in DB"
                        Nothing -> throwE "Local resource of non-resource type"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Resource @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Resource isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote resource type isn't Group"
                            return (u, actor)
                )
                resource

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the sender is authorized by me to remove a resource
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceGroup groupID)
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            -- Verify it's an active resource of mine
            efforts <- lift $ case resourceDB of
                Left r ->
                    fmap (map $ \ (s, d, E.Value a, E.Value t) -> (s, d, Left (a, t))) $
                    E.select $ E.from $ \ (effort `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send) -> do
                        E.on $ effort E.^. EffortId E.==. send E.^. EffortUsSendDelegatorEffort
                        E.on $ topic E.^. EffortTopicLocalId E.==. accept E.^. EffortThemAcceptLocalTopic
                        E.on $ effort E.^. EffortId E.==. topic E.^. EffortTopicLocalEffort
                        E.where_ $
                            effort E.^. EffortHolder E.==. E.val groupID E.&&.
                            topic E.^. EffortTopicLocalTopic E.==. E.val (localResourceID r)
                        return
                            ( effort E.^. EffortId
                            , send
                            , accept E.^. EffortThemAcceptLocalId
                            , topic E.^. EffortTopicLocalId
                            )
                Right (_, Entity a _) ->
                    fmap (map $ \ (s, d, E.Value a, E.Value t) -> (s, d, Right (a, t))) $
                    E.select $ E.from $ \ (effort `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send) -> do
                        E.on $ effort E.^. EffortId E.==. send E.^. EffortUsSendDelegatorEffort
                        E.on $ topic E.^. EffortTopicRemoteId E.==. accept E.^. EffortThemAcceptRemoteTopic
                        E.on $ effort E.^. EffortId E.==. topic E.^. EffortTopicRemoteEffort
                        E.where_ $
                            effort E.^. EffortHolder E.==. E.val groupID E.&&.
                            topic E.^. EffortTopicRemoteTopic E.==. E.val a
                        return
                            ( effort E.^. EffortId
                            , send
                            , accept E.^. EffortThemAcceptRemoteId
                            , topic E.^. EffortTopicRemoteId
                            )
            (E.Value effortID, Entity sendID (EffortUsSendDelegator _ grantID), topic) <-
                verifySingleE efforts "No effort" "Multiple efforts"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                -- Grab extension-Grants that I'm about to revoke
                distributes <- selectList [EffortUsDistributeEffort ==. sendID] []
                leafs <- selectList [EffortUsLeafEffort ==. sendID] []

                -- Delete the whole Effort record
                deleteWhere [EffortRemoveSend ==. sendID]
                let distributeIDs = map entityKey distributes
                deleteWhere [EffortUsDistributeFromLocalDistribute <-. distributeIDs]
                deleteWhere [EffortUsDistributeFromRemoteDistribute <-. distributeIDs]
                deleteWhere [EffortUsDistributeId <-. distributeIDs]
                let leafIDs = map entityKey leafs
                deleteWhere [EffortUsLeafFromLocalLeaf <-. leafIDs]
                deleteWhere [EffortUsLeafFromRemoteLeaf <-. leafIDs]
                deleteWhere [EffortUsLeafToLocalLeaf <-. leafIDs]
                deleteWhere [EffortUsLeafToRemoteLeaf <-. leafIDs]
                deleteWhere [EffortUsLeafId <-. leafIDs]
                case topic of
                    Left (localID, _) -> do
                        deleteWhere [EffortThemDelegateLocalEffort ==. localID]
                        delete localID
                    Right (remoteID, _) -> do
                        deleteWhere [EffortThemDelegateRemoteEffort ==. remoteID]
                        delete remoteID
                delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueEffortOriginUs effortID)
                        (getKeyBy $ UniqueEffortOriginThem effortID)
                        "Neither us nor them"
                        "Both us and them"
                case origin of
                    Left usID -> do
                        deleteBy $ UniqueEffortUsAccept usID
                        deleteBy $ UniqueEffortUsGestureLocal usID
                        deleteBy $ UniqueEffortUsGestureRemote usID
                        delete usID
                    Right themID -> do
                        deleteBy $ UniqueEffortThemGestureLocal themID
                        deleteBy $ UniqueEffortThemGestureRemote themID
                        delete themID
                case topic of
                    Left (_, l) -> delete l
                    Right (_, r) -> delete r
                delete effortID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid groupID
                    let topicByHash =
                            LocalActorGroup topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare main Revoke activity and insert to my outbox
                revoke@(actionRevoke, _, _, _) <-
                    lift $ prepareMainRevoke resourceDB grantID
                let recipByKey = LocalActorGroup groupID
                revokeID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                _luRevoke <- updateOutboxItem' recipByKey revokeID actionRevoke

                -- Prepare and insert Revokes on all the extension-Grants
                revokesD <- for distributes $ \ (Entity _ (EffortUsDistribute _ startID grantID)) -> do
                    DestUsStart acceptID _ <- getJust startID
                    DestUsAccept destID _ <- getJust acceptID
                    child <- do
                        c <- getDestTopic destID
                        bitraverse
                            (\case
                                Right j -> pure $ LocalActorGroup j
                                Left _ -> error "I'm a group but I have a child who is a Project"
                            )
                            pure
                            (bimap snd snd c)
                    return (child, grantID)
                revokesL <- for leafs $ \ (Entity _ (EffortUsLeaf _ enableID grantID)) -> do
                    CollabEnable collabID _ <- getJust enableID
                    recip <- getCollabRecip collabID
                    return
                        ( bimap
                            (LocalActorPerson . collabRecipLocalPerson . entityVal)
                            (collabRecipRemoteActor . entityVal)
                            recip
                        , grantID
                        )
                revokes <- for (revokesD ++ revokesL) $ \ (actor, grantID) -> do
                    ext@(actionExt, _, _, _) <- prepareExtRevoke actor grantID
                    let recipByKey = LocalActorGroup groupID
                    extID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return (groupActor group, sieve, revokeID, revoke, revokes, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, revokeID, (actionRevoke, localRecipsRevoke, remoteRecipsRevoke, fwdHostsRevoke), revokes, inboxItemID) -> do
                let topicByID = LocalActorGroup groupID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $ do
                    sendActivity
                        topicByID topicActorID localRecipsRevoke
                        remoteRecipsRevoke fwdHostsRevoke revokeID actionRevoke
                    for_ revokes $ \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            topicByID topicActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "[Resource-active] Deleted the Effort, forwarded Remove, sent Revokes"

        where

        prepareMainRevoke resource grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            recipHash <- encodeKeyHashid groupID
            let topicByHash = LocalActorGroup recipHash

            resourceHash <- bitraverse (hashLocalResource . bmap entityKey) pure resource

            audRemover <- makeAudSenderOnly authorIdMsig
            let audResource =
                    case resourceHash of
                        Left lr ->
                            let la = resourceToActor lr
                            in  AudLocal [la] [localActorFollowers la]
                        Right (ObjURI h lu, Entity _ actor) ->
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)
                audMe = AudLocal [] [localActorFollowers topicByHash]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRemover, audResource, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote

            uRemove <- getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtRevoke recipient grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            groupHash <- encodeKeyHashid groupID
            let topicByHash = LocalActorGroup groupHash

            audRecip <-
                case recipient of
                    Left a -> do
                        h <- hashLocalActor a
                        return $ AudLocal [h] [localActorFollowers h]
                    Right actorID -> do
                        actor <- getJust actorID
                        ObjURI h lu <- getRemoteActorURI actor
                        return $
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRecip]

                recips = map encodeRouteHome audLocal ++ audRemote

            uRemove <- lift $ getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    removeResourcePassive resource = do

        -- If resource is local, find it in our DB
        -- If resource is remote, HTTP GET it, verify it's an actor of Group
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        resourceDB <-
            bitraverse
                (\ ng ->
                    withDBExcept $
                        getLocalResourceEntityE (resourceFromNG ng) "Resource not found in DB"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Resource @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Resource isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote resource type isn't Group"
                            return (u, actor)
                )
                resource

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            -- Verify it's an active resource of mine
            efforts <- lift $ case resourceDB of
                Left r ->
                    fmap (map $ \ (s, d, E.Value a, E.Value t) -> (s, d, Left (a, t))) $
                    E.select $ E.from $ \ (effort `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send) -> do
                        E.on $ effort E.^. EffortId E.==. send E.^. EffortUsSendDelegatorEffort
                        E.on $ topic E.^. EffortTopicLocalId E.==. accept E.^. EffortThemAcceptLocalTopic
                        E.on $ effort E.^. EffortId E.==. topic E.^. EffortTopicLocalEffort
                        E.where_ $
                            effort E.^. EffortHolder E.==. E.val groupID E.&&.
                            topic E.^. EffortTopicLocalTopic E.==. E.val (localResourceID r)
                        return
                            ( effort E.^. EffortId
                            , send
                            , accept E.^. EffortThemAcceptLocalId
                            , topic E.^. EffortTopicLocalId
                            )
                Right (_, Entity a _) ->
                    fmap (map $ \ (s, d, E.Value a, E.Value t) -> (s, d, Right (a, t))) $
                    E.select $ E.from $ \ (effort `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send) -> do
                        E.on $ effort E.^. EffortId E.==. send E.^. EffortUsSendDelegatorEffort
                        E.on $ topic E.^. EffortTopicRemoteId E.==. accept E.^. EffortThemAcceptRemoteTopic
                        E.on $ effort E.^. EffortId E.==. topic E.^. EffortTopicRemoteEffort
                        E.where_ $
                            effort E.^. EffortHolder E.==. E.val groupID E.&&.
                            topic E.^. EffortTopicRemoteTopic E.==. E.val a
                        return
                            ( effort E.^. EffortId
                            , send
                            , accept E.^. EffortThemAcceptRemoteId
                            , topic E.^. EffortTopicRemoteId
                            )
            (E.Value effortID, Entity sendID (EffortUsSendDelegator _ grantID), topic) <-
                verifySingleE efforts "No effort" "Multiple efforts"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRemoveDB $ \ (removeID, _) -> do

                -- Record the removal attempt
                insert_ $ EffortRemove sendID removeID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid groupID
                    let topicByHash =
                            LocalActorGroup topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                return (groupActor group, sieve, removeID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, inboxItemID) -> do
                let topicByID = LocalActorGroup groupID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                doneDB inboxItemID "[Resource-passive] Recorded removal attempt, forwarded Remove"

-- Meaning: An actor is revoking Grant activities
-- Behavior:
--  * For each revoked activity:
--      * If it's a child revoking a delegator-Grant it gave me:
--          * Delete the whole Dest record
--          * Forward the Revoke to my followers
--          * Send Accept to child+followers & my followers
--      * If it's a parent revoking a Grant it had extended to me:
--          * Delete that extension from my Source record
--          * For each further extension I did on that Grant (to a
--            child/collab), send a Revoke
--      * If it's a resource revoking a Grant it had extended to me:
--          * Delete that extension from my Effort record
--          * For each further extension I did on that Grant (to a
--            child/collab), send a Revoke
groupRevoke
    :: UTCTime
    -> GroupId
    -> Verse
    -> AP.Revoke URIMode
    -> ActE (Text, Act (), Next)
groupRevoke now groupID (Verse authorIdMsig body) (AP.Revoke (luFirst :| lusRest)) = do

    ObjURI h _ <- lift $ getActorURI authorIdMsig
    parseRevoked <- do
        hl <- hostIsLocal h
        return $
            \ lu ->
                if hl
                    then
                        Left . (\ (a, _, i) -> (a, i)) <$>
                            parseLocalActivityURI' lu
                    else pure $ Right lu
    revokedFirst <- parseRevoked luFirst
    revokedRest <- traverse parseRevoked lusRest

    mode <- withDBExcept $ do

        revokedFirstDB <- do
            a <- getActivity $ second (ObjURI h) revokedFirst
            fromMaybeE a "Can't find revoked in DB"

        let adapt = maybe (Right Nothing) (either Left (Right . Just))
        maybeMode <-
            ExceptT $ fmap adapt $ runMaybeT $
                runExceptT (Left <$> tryChild revokedFirstDB) <|>
                runExceptT (Right . Left <$> tryParent revokedFirstDB) <|>
                runExceptT (Right . Right <$> tryResource revokedFirstDB)
        fromMaybeE
            maybeMode
            "Revoked activity isn't a relevant Grant I'm aware of"

    case mode of
        Left p -> revokeChild revokedRest p
        Right (Left c) -> revokeParent revokedRest c
        Right (Right r) -> revokeResource revokedRest r

    where

    verifyDestHolder :: DestId -> MaybeT ActDB ()
    verifyDestHolder destID = do
        DestHolderGroup _ j <- MaybeT $ getValBy $ UniqueDestHolderGroup destID
        guard $ j == groupID

    tryChild' usAcceptID send = do
        DestUsAccept destID _ <- lift $ lift $ getJust usAcceptID
        lift $ verifyDestHolder destID
        topic <- do
            t <- lift . lift $ getDestTopic destID
            bitraverse
                (\ (l, k) ->
                    case k of
                        Right j -> pure (l, j)
                        Left _ -> error "Group Dest topic is a Project, impossible"
                )
                pure
                t
        return (destID, usAcceptID, topic, send)

    tryChild (Left (_actorByKey, _actorEntity, itemID)) = do
        Entity sendID (DestThemSendDelegatorLocal usAcceptID _localID _) <-
            lift $ MaybeT $ getBy $ UniqueDestThemSendDelegatorLocalGrant itemID
        tryChild' usAcceptID (Left sendID) --(Left localID)
    tryChild (Right remoteActivityID) = do
        Entity sendID (DestThemSendDelegatorRemote usAcceptID _remoteID _) <-
            lift $ MaybeT $ getBy $ UniqueDestThemSendDelegatorRemoteGrant remoteActivityID
        tryChild' usAcceptID (Right sendID) --(Right remoteID)

    verifySourceHolder :: SourceId -> MaybeT ActDB ()
    verifySourceHolder sourceID = do
        SourceHolderGroup _ j <- MaybeT $ getValBy $ UniqueSourceHolderGroup sourceID
        guard $ j == groupID

    tryParent' sourceID child = do
        lift $ verifySourceHolder sourceID
        sendID <- lift $ MaybeT $ getKeyBy $ UniqueSourceUsSendDelegator sourceID
        return (sendID, child)

    tryParent (Left (_actorByKey, _actorEntity, itemID)) = do
        Entity delegID (SourceThemDelegateLocal themAcceptID _) <-
            lift $ MaybeT $ getBy $ UniqueSourceThemDelegateLocal itemID
        SourceThemAcceptLocal topicID _ <- lift $ lift $ getJust themAcceptID
        SourceTopicLocal sourceID <- lift $ lift $ getJust topicID
        SourceTopicGroup _ _ j <- do
            mj <- lift $ lift $ getValBy $ UniqueSourceTopicGroupTopic topicID
            fromMaybeE mj "The parent to whom this revoked Grant was sent isn't a Group"
        tryParent' sourceID $ Left (topicID, j, delegID, themAcceptID)
    tryParent (Right remoteActivityID) = do
        Entity delegID (SourceThemDelegateRemote themAcceptID _) <-
            lift $ MaybeT $ getBy $ UniqueSourceThemDelegateRemote remoteActivityID
        SourceThemAcceptRemote topicID _ <- lift $ lift $ getJust themAcceptID
        SourceTopicRemote sourceID actorID <- lift $ lift $ getJust topicID
        tryParent' sourceID $ Right (topicID, actorID, delegID, themAcceptID)

    verifyEffortHolder :: EffortId -> MaybeT ActDB ()
    verifyEffortHolder effortID = do
        Effort _ g <- lift $ getJust effortID
        guard $ g == groupID

    tryResource' effortID resource = do
        lift $ verifyEffortHolder effortID
        sendID <- lift $ MaybeT $ getKeyBy $ UniqueEffortUsSendDelegator effortID
        return (sendID, resource)

    tryResource (Left (_actorByKey, _actorEntity, itemID)) = do
        Entity delegID (EffortThemDelegateLocal themAcceptID _) <-
            lift $ MaybeT $ getBy $ UniqueEffortThemDelegateLocal itemID
        EffortThemAcceptLocal topicID _ <- lift $ lift $ getJust themAcceptID
        EffortTopicLocal effortID r <- lift $ lift $ getJust topicID
        tryResource' effortID $ Left (topicID, r, delegID, themAcceptID)
    tryResource (Right remoteActivityID) = do
        Entity delegID (EffortThemDelegateRemote themAcceptID _) <-
            lift $ MaybeT $ getBy $ UniqueEffortThemDelegateRemote remoteActivityID
        EffortThemAcceptRemote topicID _ <- lift $ lift $ getJust themAcceptID
        EffortTopicRemote effortID actorID <- lift $ lift $ getJust topicID
        tryResource' effortID $ Right (topicID, actorID, delegID, themAcceptID)

    revokeChild revokedRest (destID, usAcceptID, child, send) = do

        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        unless (author == bimap (LocalActorGroup . snd) snd child) $
            throwE "Sender isn't the child Group the revoked Grant came from"

        unless (null revokedRest) $
            throwE "Child revoking the delegator-Grant and something more"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            maybeRevokeDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRevokeDB $ \ (inboxItemID, _revokeDB) -> do

                maybeStartID <- getKeyBy $ UniqueDestUsStart usAcceptID

                -- Delete uses of this Dest from my Effort records
                --for_ maybeStartID $ \ destStartID ->
                --    deleteWhere [ComponentGatherChild ==. destStartID]

                -- Delete uses of this Dest from my Source records
                for_ maybeStartID $ \ destStartID -> do
                    gatherIDs <- selectKeysList [SourceUsGatherDest ==. destStartID] []
                    deleteWhere [SourceUsGatherFromLocalGather <-. gatherIDs]
                    deleteWhere [SourceUsGatherFromRemoteGather <-. gatherIDs]
                    deleteWhere [SourceUsGatherId <-. gatherIDs]

                -- Delete the whole Dest record
                for_ maybeStartID delete
                case send of
                    Left sendID -> delete sendID
                    Right sendID -> delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueDestOriginUs destID)
                        (getKeyBy $ UniqueDestOriginThem destID)
                        "Neither us nor them"
                        "Both us and them"
                deleteBy $ UniqueDestUsGestureLocal destID
                deleteBy $ UniqueDestUsGestureRemote destID
                case origin of
                    Left usID -> delete usID
                    Right themID -> do
                        deleteBy $ UniqueDestThemAcceptLocal themID
                        deleteBy $ UniqueDestThemAcceptRemote themID
                        deleteBy $ UniqueDestThemGestureLocal themID
                        deleteBy $ UniqueDestThemGestureRemote themID
                        delete themID
                delete usAcceptID
                case child of
                    Left (l, _j) -> do
                        deleteBy $ UniqueDestTopicGroupTopic l
                        delete l
                    Right (r, _) -> delete r
                deleteBy $ UniqueDestHolderGroup destID
                delete destID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid groupID
                    let topicByHash =
                            LocalActorGroup topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare Accept activity
                accept@(actionAccept, _, _, _) <- prepareAccept
                let recipByKey = LocalActorGroup groupID
                acceptID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                _luAccept <- updateOutboxItem' recipByKey acceptID actionAccept

                return (groupActor group, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                let topicByID = LocalActorGroup groupID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $
                    sendActivity
                        topicByID topicActorID localRecipsAccept
                        remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "Deleted the Child/Dest, forwarded Revoke, sent Accept"

        where

        prepareAccept = do
            encodeRouteHome <- getEncodeRouteHome

            audChild <- makeAudSenderWithFollowers authorIdMsig
            audMe <-
                AudLocal [] . pure . LocalStageGroupFollowers <$>
                    encodeKeyHashid groupID
            uRevoke <- lift $ getActivityURI authorIdMsig

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audChild, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRevoke]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uRevoke
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    revokeParent revokedRest (sendID, parent) = do

        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        unless (author == bimap (LocalActorGroup . view _2) (view _2) parent) $
            throwE "Sender isn't the parent Group the revoked Grant came from"

        unless (null revokedRest) $
            throwE "Parent revoking the start/extension-Grant and something more"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            maybeRevokeDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRevokeDB $ \ (inboxItemID, _revokeDB) -> do

                -- Collect the extensions I'll need to revoke
                gatherIDs <-
                    case parent of
                        Left (_, _, delegID, _) ->
                            map (sourceUsGatherFromLocalGather . entityVal) <$>
                                selectList [SourceUsGatherFromLocalFrom ==. delegID] []
                        Right (_, _, delegID, _) ->
                            map (sourceUsGatherFromRemoteGather . entityVal) <$>
                                selectList [SourceUsGatherFromRemoteFrom ==. delegID] []
                gathers <- selectList [SourceUsGatherId <-. gatherIDs] []
                leafIDs <-
                    case parent of
                        Left (_, _, delegID, _) ->
                            map (sourceUsLeafFromLocalLeaf . entityVal) <$>
                                selectList [SourceUsLeafFromLocalFrom ==. delegID] []
                        Right (_, _, delegID, _) ->
                            map (sourceUsLeafFromRemoteLeaf . entityVal) <$>
                                selectList [SourceUsLeafFromRemoteFrom ==. delegID] []
                leafs <- selectList [SourceUsLeafId <-. leafIDs] []

                -- Delete the records of these extensions
                deleteWhere [SourceUsGatherFromLocalGather <-. gatherIDs]
                deleteWhere [SourceUsGatherFromRemoteGather <-. gatherIDs]
                deleteWhere [SourceUsGatherId <-. gatherIDs]
                deleteWhere [SourceUsLeafFromLocalLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafFromRemoteLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafToLocalLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafToRemoteLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafId <-. leafIDs]
                case parent of
                    Left (_, _, delegID, _) -> delete delegID
                    Right (_, _, delegID, _) -> delete delegID

                -- Prepare and insert Revokes on all the extension-Grants
                revokesG <- for gathers $ \ (Entity _ (SourceUsGather _ startID grantID)) -> do
                    DestUsStart acceptID _ <- getJust startID
                    DestUsAccept destID _ <- getJust acceptID
                    parent <- do
                        p <- getDestTopic destID
                        bitraverse
                            (\case
                                Right j -> pure $ LocalActorGroup j
                                Left _ -> error "I'm a group but I have a parent who is a Project"
                            )
                            pure
                            (bimap snd snd p)
                    return (parent, grantID)
                revokesL <- for leafs $ \ (Entity _ (SourceUsLeaf _ enableID grantID)) -> do
                    CollabEnable collabID _ <- getJust enableID
                    recip <- getCollabRecip collabID
                    return
                        ( bimap
                            (LocalActorPerson . collabRecipLocalPerson . entityVal)
                            (collabRecipRemoteActor . entityVal)
                            recip
                        , grantID
                        )
                revokes <- for (revokesG ++ revokesL) $ \ (actor, grantID) -> do
                    ext@(actionExt, _, _, _) <- prepareExtRevoke actor grantID
                    let recipByKey = LocalActorGroup groupID
                    extID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return (groupActor group, revokes, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, revokes, inboxItemID) -> do
                let topicByID = LocalActorGroup groupID
                lift $ for_ revokes $ \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                    sendActivity
                        topicByID topicActorID localRecipsExt
                        remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "Deleted the SourceThemDelegate* record, sent Revokes"

        where

        prepareExtRevoke recipient grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            groupHash <- encodeKeyHashid groupID
            let topicByHash = LocalActorGroup groupHash

            audRecip <-
                case recipient of
                    Left a -> do
                        h <- hashLocalActor a
                        return $ AudLocal [h] [localActorFollowers h]
                    Right actorID -> do
                        actor <- getJust actorID
                        ObjURI h lu <- getRemoteActorURI actor
                        return $
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRecip]

                recips = map encodeRouteHome audLocal ++ audRemote

            uRevoke <- lift $ getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRevoke]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    revokeResource revokedRest (sendID, resource) = do

        resource' <-
            lift $ traverseOf _Left (traverseOf _2 $ withDB . getLocalResource) resource

        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        unless (author == bimap (resourceToActor . view _2) (view _2) resource') $
            throwE "Sender isn't the resource the revoked Grant came from"

        unless (null revokedRest) $
            throwE "Resource revoking the start/extension-Grant and something more"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (group, actorRecip) <- lift $ do
                p <- getJust groupID
                (p,) <$> getJust (groupActor p)

            maybeRevokeDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRevokeDB $ \ (inboxItemID, _revokeDB) -> do

                -- Collect the extensions I'll need to revoke
                distributeIDs <-
                    case resource of
                        Left (_, _, delegID, _) ->
                            map (effortUsDistributeFromLocalDistribute . entityVal) <$>
                                selectList [EffortUsDistributeFromLocalFrom ==. delegID] []
                        Right (_, _, delegID, _) ->
                            map (effortUsDistributeFromRemoteDistribute . entityVal) <$>
                                selectList [EffortUsDistributeFromRemoteFrom ==. delegID] []
                distributes <- selectList [EffortUsDistributeId <-. distributeIDs] []
                leafIDs <-
                    case resource of
                        Left (_, _, delegID, _) ->
                            map (effortUsLeafFromLocalLeaf . entityVal) <$>
                                selectList [EffortUsLeafFromLocalFrom ==. delegID] []
                        Right (_, _, delegID, _) ->
                            map (effortUsLeafFromRemoteLeaf . entityVal) <$>
                                selectList [EffortUsLeafFromRemoteFrom ==. delegID] []
                leafs <- selectList [EffortUsLeafId <-. leafIDs] []

                -- Delete the records of these extensions
                deleteWhere [EffortUsDistributeFromLocalDistribute <-. distributeIDs]
                deleteWhere [EffortUsDistributeFromRemoteDistribute <-. distributeIDs]
                deleteWhere [EffortUsDistributeId <-. distributeIDs]
                deleteWhere [EffortUsLeafFromLocalLeaf <-. leafIDs]
                deleteWhere [EffortUsLeafFromRemoteLeaf <-. leafIDs]
                deleteWhere [EffortUsLeafToLocalLeaf <-. leafIDs]
                deleteWhere [EffortUsLeafToRemoteLeaf <-. leafIDs]
                deleteWhere [EffortUsLeafId <-. leafIDs]
                case resource of
                    Left (_, _, delegID, _) -> delete delegID
                    Right (_, _, delegID, _) -> delete delegID

                -- Prepare and insert Revokes on all the extension-Grants
                revokesD <- for distributes $ \ (Entity _ (EffortUsDistribute _ startID grantID)) -> do
                    DestUsStart acceptID _ <- getJust startID
                    DestUsAccept destID _ <- getJust acceptID
                    child <- do
                        c <- getDestTopic destID
                        bitraverse
                            (\case
                                Right j -> pure $ LocalActorGroup j
                                Left _ -> error "I'm a group but I have a child who is a Project"
                            )
                            pure
                            (bimap snd snd c)
                    return (child, grantID)
                revokesL <- for leafs $ \ (Entity _ (EffortUsLeaf _ enableID grantID)) -> do
                    CollabEnable collabID _ <- getJust enableID
                    recip <- getCollabRecip collabID
                    return
                        ( bimap
                            (LocalActorPerson . collabRecipLocalPerson . entityVal)
                            (collabRecipRemoteActor . entityVal)
                            recip
                        , grantID
                        )
                revokes <- for (revokesD ++ revokesL) $ \ (actor, grantID) -> do
                    ext@(actionExt, _, _, _) <- prepareExtRevoke actor grantID
                    let recipByKey = LocalActorGroup groupID
                    extID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return (groupActor group, revokes, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, revokes, inboxItemID) -> do
                let topicByID = LocalActorGroup groupID
                lift $ for_ revokes $ \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                    sendActivity
                        topicByID topicActorID localRecipsExt
                        remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "Deleted the EffortThemDelegate* record, sent Revokes"

        where

        prepareExtRevoke recipient grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            groupHash <- encodeKeyHashid groupID
            let topicByHash = LocalActorGroup groupHash

            audRecip <-
                case recipient of
                    Left a -> do
                        h <- hashLocalActor a
                        return $ AudLocal [h] [localActorFollowers h]
                    Right actorID -> do
                        actor <- getJust actorID
                        ObjURI h lu <- getRemoteActorURI actor
                        return $
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRecip]

                recips = map encodeRouteHome audLocal ++ audRemote

            uRevoke <- lift $ getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRevoke]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor is undoing some previous action
-- Behavior:
--      * If they're undoing their Following of me:
--          * Record it in my DB
--          * Publish and send an Accept only to the sender
--      * Otherwise respond with an error
groupUndo
    :: UTCTime
    -> GroupId
    -> Verse
    -> AP.Undo URIMode
    -> ActE (Text, Act (), Next)
groupUndo now recipGroupID (Verse authorIdMsig body) (AP.Undo uObject) = do

    -- Check input
    undone <-
        first (\ (actor, _, item) -> (actor, item)) <$>
            parseActivityURI' uObject

    -- Verify the capability URI, if provided, is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCapability <-
        for (AP.activityCapability $ actbActivity body) $ \ uCap ->
            nameExceptT "Undo capability" $
                first (\ (actor, _, item) -> (actor, item)) <$>
                    parseActivityURI' uCap

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (groupRecip, actorRecip) <- lift $ do
            p <- getJust recipGroupID
            (p,) <$> getJust (groupActor p)

        -- Insert the Undo to my inbox
        mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
        for mractid $ \ (inboxItemID, _undoDB) -> do

            maybeUndo <- runMaybeT $ do

                -- Find the undone activity in our DB
                undoneDB <- MaybeT $ getActivity undone

                let followers = actorFollowers actorRecip
                asum
                    [ tryUnfollow followers undoneDB authorIdMsig
                    ]

            (sieve, audience) <-
                fromMaybeE
                    maybeUndo
                    "Undone activity isn't a Follow related to me"

            -- Prepare an Accept activity and insert to group's outbox
            acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorRecip) now
            accept@(actionAccept, _, _, _) <- lift $ lift $ prepareAccept audience
            _luAccept <- lift $ updateOutboxItem' (LocalActorGroup recipGroupID) acceptID actionAccept

            return (groupActor groupRecip, sieve, acceptID, accept, inboxItemID)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (actorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
            forwardActivity
                authorIdMsig body (LocalActorGroup recipGroupID) actorID sieve
            lift $ sendActivity
                (LocalActorGroup recipGroupID) actorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            doneDB inboxItemID
                "Undid the Follow, forwarded the Undo and published Accept"

    where

    tryUnfollow groupFollowersID (Left (_actorByKey, _actorE, outboxItemID)) (Left (_, actorID, _)) = do
        Entity followID follow <-
            MaybeT $ lift $ getBy $ UniqueFollowFollow outboxItemID
        let followerID = followActor follow
            followerSetID = followTarget follow
        verifyTargetMe followerSetID
        unless (followerID == actorID) $
            lift $ throwE "You're trying to Undo someone else's Follow"
        lift $ lift $ delete followID
        audSenderOnly <- lift $ lift $ lift $ makeAudSenderOnly authorIdMsig
        return (makeRecipientSet [] [], [audSenderOnly])
        where
        verifyTargetMe followerSetID = guard $ followerSetID == groupFollowersID
    tryUnfollow groupFollowersID (Right remoteActivityID) (Right (author, _, _)) = do
        Entity remoteFollowID remoteFollow <-
            MaybeT $ lift $ getBy $ UniqueRemoteFollowFollow remoteActivityID
        let followerID = remoteFollowActor remoteFollow
            followerSetID = remoteFollowTarget remoteFollow
        verifyTargetMe followerSetID
        unless (followerID == remoteAuthorId author) $
            lift $ throwE "You're trying to Undo someone else's Follow"
        lift $ lift $ delete remoteFollowID
        audSenderOnly <- lift $ lift $ lift $ makeAudSenderOnly authorIdMsig
        return (makeRecipientSet [] [], [audSenderOnly])
        where
        verifyTargetMe followerSetID = guard $ followerSetID == groupFollowersID
    tryUnfollow _ _ _ = mzero

    prepareAccept audience = do
        encodeRouteHome <- getEncodeRouteHome

        uUndo <- getActivityURI authorIdMsig
        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = []
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uUndo
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

groupVerse :: GroupId -> Verse -> ActE (Text, Act (), Next)
groupVerse groupID verse@(Verse _authorIdMsig body) = do
    now <- liftIO getCurrentTime
    case AP.activitySpecific $ actbActivity body of
        AP.AcceptActivity accept -> groupAccept now groupID verse accept
        AP.AddActivity add       -> groupAdd now groupID verse add
        AP.FollowActivity follow -> groupFollow now groupID verse follow
        AP.GrantActivity grant   -> groupGrant now groupID verse grant
        AP.InviteActivity invite -> groupInvite now groupID verse invite
        AP.JoinActivity join     -> groupJoin now groupID verse join
        AP.RejectActivity reject -> groupReject now groupID verse reject
        AP.RemoveActivity remove -> groupRemove now groupID verse remove
        AP.RevokeActivity revoke -> groupRevoke now groupID verse revoke
        AP.UndoActivity undo     -> groupUndo now groupID verse undo
        _ -> throwE "Unsupported activity type for Group"

instance ActorLaunch Group where
    actorBehavior _ =
        (handleMethod @"verse" := \ groupID verse -> adaptHandlerResult $ do
            errboxID <- lift $ withDB $ do
                resourceID <- groupResource <$> getJust groupID
                Resource actorID <- getJust resourceID
                actorErrbox <$> getJust actorID
            adaptErrbox errboxID False (groupVerse groupID) verse
        )
        `HCons`
        (handleMethod @"init" := \ groupID creator -> adaptHandlerResult $ do
            now <- liftIO getCurrentTime
            let grabResource = pure . groupResource
            topicInit @Group @() AP.ActorTypeTeam (\ _ _ -> pure ()) grabResource LocalResourceGroup now groupID creator Nothing
        )
        `HCons`
        HNil
