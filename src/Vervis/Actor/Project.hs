{- This file is part of Vervis.
 -
 - Written in 2023, 2024 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Actor.Project
    (
    )
where

import Control.Applicative
import Control.Exception.Base hiding (handle)
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Barbie
import Data.Bifoldable
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Either
import Data.Foldable
import Data.HList (HList (..))
import Data.List.NonEmpty (NonEmpty (..))
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Optics.Core
import Yesod.Persist.Core

import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Control.Concurrent.Actor
import Network.FedURI
import Web.Actor
import Web.Actor.Persist
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Database.Persist.Local

import Vervis.Access
import Vervis.ActivityPub
import Vervis.Actor
import Vervis.Actor.Common
import Vervis.Actor2
import Vervis.Cloth
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Data.Discussion
import Vervis.FedURI

import Vervis.Foundation
import Vervis.Model
import Vervis.Recipient (makeRecipientSet, LocalStageBy (..), Aud (..), collectAudience, localActorFollowers, renderLocalActor)
import Vervis.RemoteActorStore
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.Ticket
import Vervis.Web.Collab

-- Meaning: An actor accepted something
-- Behavior:
--      * == Collab mode ==
--         * Is it an Invite to be a collaborator in me?
--             * Verify the Accept is by the Invite target
--         * Is it a Join to be a collaborator in me?
--             * Verify the Accept is authorized
--
--     * In collab mode, verify the Collab isn't enabled yet
--
--     * Insert the Accept to my inbox
--
--     * In collab mode, record the Accept and enable the Collab in DB
--
--     * Forward the Accept to my followers
--
--     * Possibly send a Grant/Accept:
--         * For Invite-collab mode:
--             * Regular collaborator-Grant
--             * To: Accepter (i.e. Invite target)
--             * CC: Invite sender, Accepter's followers, my followers
--         * For Join-as-collab mode:
--             * Regular collaborator-Grant
--             * To: Join sender
--             * CC: Accept sender, Join sender's followers, my followers
--
--      * == Component mode ==
--         * Is it an Invite to be a component of me?
--             * Nothing to check at this point
--         * Is it an Add to be a component of me?
--             * If the sender is the component:
--                 * Verify I haven't seen a component-Accept on this Add
--             * Otherwise, i.e. sender isn't the component:
--                 * Verify I've seen the component-Accept for this Add
--                 * Verify the new Accept is authorized
--
--     * In component mode, verify the Component isn't enabled yet
--
--     * Insert the Accept to my inbox
--
--     * In Invite-component mode,
--         * If sender is component, record the Accept and enable the Component
--           in DB
--         * Otherwise, nothing at this point
--     * In Add-component mode,
--         * If the sender is the component, record the Accept into the
--           Component record in DB
--         * Otherwise, i.e. sender isn't the component, record the Accept and
--           enable the Component in DB
--
--     * Forward the Accept to my followers
--
--     * Possibly send a Grant/Accept:
--         * For Invite-component mode:
--             * Only if sender is the component
--             * delegator-Grant
--             * To: Component
--             * CC:
--                 - Component's followers
--                 - My followers
--         * For Add-component mode:
--             * Only if sender isn't the component
--             * delegator-Grant
--             * To: Component
--             * CC:
--                 - Component's followers
--                 - My followers
--                 - The Accept's sender
--
--      * == Child-parent mode ==
--         * Give me a new child active   SourceOriginUs
--              * Verify we haven't yet seen child's Accept
--         * Give me a new child passive  SourceOriginThem
--              * Option 1: We haven't seen child's Accept yet
--                  * Verify sender is the child
--              * Option 2: We saw it, but not my collaborator's Accept
--                  * Verify the Accept is authorized
--              * Otherwise respond with error, no Accept is needed
--         * Give me a new parent active DestOriginUs
--              * Respond with error, we aren't supposed to get any Accept
--         * Give me a new parent passive DestOriginThem
--              * Option 1: I haven't yet seen parent's Accept
--                  * Verify sender is the parent
--              * Option 2: I saw it, but not my collaborator's Accept
--                  * Verify the accept is authorized
--              * Otherwise respond with error, no Accept is needed
--
--     * Insert the Accept to my inbox
--
--     * In child-active mode,
--          * If sender is the child, record the Accept into the Source record
--              * Prepare to send degelator-Grant
--          * Otherwise nothing to do
--     * In child-passive mode,
--          * Option 1: Record child's Accept in Source record
--          * Option 2: Record my collaborator's Accept
--              * Prepare to send delegator-Grant
--     * In parent-passive mode,
--          * Option 1: Record parent's Accept in the Dest record
--          * Option 2: Record my collaborator's Accept in the Dest record
--              * Prepare to send my own Accept
--
--     * Forward the Accept to my followers
--
--     * Possibly send a Grant/Accept:
--         * Child-active
--              * If sender is the child
--              * delegator-Grant
--              * To: Child
--              * CC:
--                  - Child's followers
--                  - My followers
--         * Child-passive
--              * In option 2
--              * delegator-Grant
--              * To: Child
--              * CC:
--                  - Child's followers
--                  - My followers
--                  - The Accept sender (my collaborator)
--         * Parent-passive
--              * In option 2
--              * Accept
--              * Object: The Add
--              * Fulfills: My collaborator's Accept
--              * To: Parent
--              * CC:
--                  - Parent's followers
--                  - My followers
--                  - The Accept sender (my collaborator)
--
-- * Remove-Child-Passive mode:
--      * Verify the Source is enabled
--      * Verify the sender is the child
--      * Delete the entire Source record
--      * Forward the Accept to my followers
--      * Send a Revoke on the delegator-Grant I had for B:
--          * To: Actor B
--          * CC: Actor A, B's followers, my followers
--      * Send a Revoke on every extention-Grant I extended on every
--        delegation Grant I got from B
--          * To: The parent/collaborator/team to whom I'd sent the Grant
--          * CC: -
--
--  * Add-a-Team mode
--         * Give me a new team active SquadOriginUs
--              * Respond with error, we aren't supposed to get any Accept
--         * Give me a new team passive SquadOriginThem
--              * Option 1: I haven't yet seen parent's Accept
--                  * Verify sender is the parent
--              * Option 2: I saw it, but not my collaborator's Accept
--                  * Verify the accept is authorized
--              * Otherwise respond with error, no Accept is needed
--
--     * Insert the Accept to my inbox
--
--     * In team-passive mode,
--          * Option 1: Record team's Accept in the Dest record
--          * Option 2: Record my collaborator's Accept in the Squad record
--              * Prepare to send my own Accept
--
--     * Forward the Accept to my followers
--
--     * Possibly send a Grant/Accept:
--         * Team-passive
--              * In option 2
--              * Accept
--              * Object: The Add
--              * Fulfills: My collaborator's Accept
--              * To: Team
--              * CC:
--                  - Team's followers
--                  - My followers
--                  - The Accept sender (my collaborator)
projectAccept
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Accept URIMode
    -> ActE (Text, Act (), Next)
projectAccept now projectID (Verse authorIdMsig body) accept = do

    -- Check input
    acceptee <- parseAccept accept

    collabOrComp_or_child <- withDBExcept $ do

        (myInboxID, meResourceID) <- lift $ do
            project <- getJust projectID
            actor <- getJust $ projectActor project
            return (actorInbox actor, projectResource project)

        -- Find the accepted activity in our DB
        accepteeDB <- do
            a <- getActivity acceptee
            fromMaybeE a "Can't find acceptee in DB"

        -- See if the accepted activity is an Invite or Join where my collabs
        -- URI is the resource, grabbing the Collab record from our DB,
        -- Or if the accepted activity is an Invite or Add where my components
        -- URI is the resource, grabbing the Component record from our DB
        let adapt = maybe (Right Nothing) (either Left (Right . Just))
        maybeCollab <-
            ExceptT $ fmap adapt $ runMaybeT $
                runExceptT (Left . Left <$> tryInviteCollab accepteeDB) <|>
                runExceptT (Left . Left <$> tryJoinCollab accepteeDB) <|>
                runExceptT (Left . Right <$> tryInviteComp accepteeDB) <|>
                runExceptT (Left . Right <$> tryAddComp accepteeDB) <|>
                runExceptT (Right . Left <$> tryAddChildActive accepteeDB) <|>
                runExceptT (Right . Left <$> tryAddChildPassive accepteeDB) <|>
                runExceptT (Right . Left <$> tryAddParentActive accepteeDB) <|>
                runExceptT (Right . Left <$> tryAddParentPassive accepteeDB) <|>
                runExceptT (Right . Right . Left <$> tryRemoveChild myInboxID accepteeDB) <|>
                runExceptT (Right . Right . Right <$> tryAddTeamActive meResourceID accepteeDB) <|>
                runExceptT (Right . Right . Right <$> tryAddTeamPassive meResourceID accepteeDB)
        fromMaybeE
            maybeCollab
            "Accepted activity isn't an Invite/Join/Add/Remove I'm aware of"

    case collabOrComp_or_child of
        Left (Left collab) -> addCollab collab
        Left (Right comp) -> addComp comp
        Right (Left cp) -> addChildParent cp
        Right (Right (Left child)) -> removeChild child
        Right (Right (Right team)) -> addTeam team

    where

    verifyCollabTopic collabID = do
        topic <- lift $ getCollabTopic collabID
        unless (LocalResourceProject projectID == topic) $
            throwE "Accept object is an Invite/Join for some other resource"

    verifyInviteCollabTopic fulfillsID = do
        collabID <- lift $ collabFulfillsInviteCollab <$> getJust fulfillsID
        verifyCollabTopic collabID
        return collabID

    verifyJoinCollabTopic fulfillsID = do
        collabID <- lift $ collabFulfillsJoinCollab <$> getJust fulfillsID
        verifyCollabTopic collabID
        return collabID

    tryInviteCollab (Left (actorByKey, _actorEntity, itemID)) = do
        fulfillsID <-
            lift $ collabInviterLocalCollab <$>
                MaybeT (getValBy $ UniqueCollabInviterLocalInvite itemID)
        collabID <-
            ExceptT $ lift $ runExceptT $ verifyInviteCollabTopic fulfillsID
        return (collabID, Left fulfillsID, Left actorByKey)
    tryInviteCollab (Right remoteActivityID) = do
        CollabInviterRemote fulfillsID actorID _ <-
            lift $ MaybeT $ getValBy $
                UniqueCollabInviterRemoteInvite remoteActivityID
        collabID <-
            ExceptT $ lift $ runExceptT $ verifyInviteCollabTopic fulfillsID
        sender <- lift $ lift $ do
            actor <- getJust actorID
            (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (collabID, Left fulfillsID, Right sender)

    tryJoinCollab (Left (actorByKey, _actorEntity, itemID)) = do
        fulfillsID <-
            lift $ collabRecipLocalJoinFulfills <$>
                MaybeT (getValBy $ UniqueCollabRecipLocalJoinJoin itemID)
        collabID <-
            ExceptT $ lift $ runExceptT $ verifyJoinCollabTopic fulfillsID
        return (collabID, Right fulfillsID, Left actorByKey)
    tryJoinCollab (Right remoteActivityID) = do
        CollabRecipRemoteJoin recipID fulfillsID _ <-
            lift $ MaybeT $ getValBy $
                UniqueCollabRecipRemoteJoinJoin remoteActivityID
        collabID <-
            ExceptT $ lift $ runExceptT $ verifyJoinCollabTopic fulfillsID
        joiner <- lift $ lift $ do
            remoteActorID <- collabRecipRemoteActor <$> getJust recipID
            actor <- getJust remoteActorID
            (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (collabID, Right fulfillsID, Right joiner)

    verifyCompTopic :: ComponentId -> ActDBE ()
    verifyCompTopic componentID = do
        Component j _ <- lift $ getJust componentID
        unless (j == projectID) $
            throwE "Accept object is an Invite/Add for some other project"

    tryInviteComp (Left (actorByKey, _actorEntity, itemID)) = do
        ComponentProjectGestureLocal componentID _ <-
            lift $ MaybeT $ getValBy $
                UniqueComponentProjectGestureLocalActivity itemID
        _ <- lift $ MaybeT $ getBy $ UniqueComponentOriginInvite componentID
        ExceptT $ lift $ runExceptT $ verifyCompTopic componentID
        ident <- lift $ lift $ getComponentIdent componentID
        return (componentID, ident, Left ())
    tryInviteComp (Right remoteActivityID) = do
        ComponentProjectGestureRemote componentID _ _ <-
            lift $ MaybeT $ getValBy $
                UniqueComponentProjectGestureRemoteActivity remoteActivityID
        _ <- lift $ MaybeT $ getBy $ UniqueComponentOriginInvite componentID
        ExceptT $ lift $ runExceptT $ verifyCompTopic componentID
        ident <- lift $ lift $ getComponentIdent componentID
        return (componentID, ident, Left ())

    tryAddComp (Left (actorByKey, _actorEntity, itemID)) = do
        ComponentGestureLocal originID _ <-
            lift $ MaybeT $ getValBy $ UniqueComponentGestureLocalAdd itemID
        ComponentOriginAdd componentID <- lift $ lift $ getJust originID
        ExceptT $ lift $ runExceptT $ verifyCompTopic componentID
        ident <- lift $ lift $ getComponentIdent componentID
        return (componentID, ident, Right ())
    tryAddComp (Right remoteActivityID) = do
        ComponentGestureRemote originID _ _ <-
            lift $ MaybeT $ getValBy $
                UniqueComponentGestureRemoteAdd remoteActivityID
        ComponentOriginAdd componentID <- lift $ lift $ getJust originID
        ExceptT $ lift $ runExceptT $ verifyCompTopic componentID
        ident <- lift $ lift $ getComponentIdent componentID
        return (componentID, ident, Right ())

    verifySourceHolder :: SourceId -> MaybeT ActDB ()
    verifySourceHolder sourceID = do
        SourceHolderProject _ j <- MaybeT $ getValBy $ UniqueSourceHolderProject sourceID
        guard $ j == projectID

    tryAddChildActive' usID = do
        SourceOriginUs sourceID <- lift . lift $ getJust usID
        lift $ verifySourceHolder sourceID
        topic <- do
            t <- lift . lift $ getSourceTopic sourceID
            bitraverse
                (\ (l, k) ->
                    case k of
                        Left j -> pure (l, j)
                        Right _ -> error "Project Source topic is a Group, impossible"
                )
                pure
                t
        return $ Left (sourceID, topic, Left ())

    tryAddChildActive (Left (_actorByKey, _actorEntity, itemID)) = do
        SourceUsGestureLocal usID _ <-
            lift $ MaybeT $ getValBy $ UniqueSourceUsGestureLocalAdd itemID
        tryAddChildActive' usID
    tryAddChildActive (Right remoteActivityID) = do
        SourceUsGestureRemote usID _ _ <-
            lift $ MaybeT $ getValBy $ UniqueSourceUsGestureRemoteAdd remoteActivityID
        tryAddChildActive' usID

    tryAddChildPassive' themID = do
        SourceOriginThem sourceID <- lift . lift $ getJust themID
        lift $ verifySourceHolder sourceID
        topic <- do
            t <- lift . lift $ getSourceTopic sourceID
            bitraverse
                (\ (l, k) ->
                    case k of
                        Left j -> pure (l, j)
                        Right _ -> error "Project Source topic is a Group, impossible"
                )
                pure
                t
        return $ Left (sourceID, topic, Right ())

    tryAddChildPassive (Left (_actorByKey, _actorEntity, itemID)) = do
        SourceThemGestureLocal themID _ <-
            lift $ MaybeT $ getValBy $ UniqueSourceThemGestureLocalAdd itemID
        tryAddChildPassive' themID
    tryAddChildPassive (Right remoteActivityID) = do
        SourceThemGestureRemote themID _ _ <-
            lift $ MaybeT $ getValBy $ UniqueSourceThemGestureRemoteAdd remoteActivityID
        tryAddChildPassive' themID

    verifyDestHolder :: DestId -> MaybeT ActDB ()
    verifyDestHolder destID = do
        DestHolderProject _ j <- MaybeT $ getValBy $ UniqueDestHolderProject destID
        guard $ j == projectID

    tryAddParentActive' destID = do
        usID <- lift $ MaybeT $ getKeyBy $ UniqueDestOriginUs destID
        lift $ verifyDestHolder destID
        topic <- do
            t <- lift . lift $ getDestTopic destID
            bitraverse
                (\ (l, k) ->
                    case k of
                        Left j -> pure (l, j)
                        Right _ -> error "Project Dest topic is a Group, impossible"
                )
                pure
                t
        return $ Right (destID, topic, Left ())

    tryAddParentActive (Left (_actorByKey, _actorEntity, itemID)) = do
        DestUsGestureLocal destID _ <-
            lift $ MaybeT $ getValBy $ UniqueDestUsGestureLocalActivity itemID
        tryAddParentActive' destID
    tryAddParentActive (Right remoteActivityID) = do
        DestUsGestureRemote destID _ _ <-
            lift $ MaybeT $ getValBy $ UniqueDestUsGestureRemoteActivity remoteActivityID
        tryAddParentActive' destID

    tryAddParentPassive' themID = do
        DestOriginThem destID <- lift . lift $ getJust themID
        lift $ verifyDestHolder destID
        topic <- do
            t <- lift . lift $ getDestTopic destID
            bitraverse
                (\ (l, k) ->
                    case k of
                        Left j -> pure (l, j)
                        Right _ -> error "Project Dest topic is a Group, impossible"
                )
                pure
                t
        return $ Right (destID, topic, Right themID)

    tryAddParentPassive (Left (_actorByKey, _actorEntity, itemID)) = do
        DestThemGestureLocal themID _ <-
            lift $ MaybeT $ getValBy $ UniqueDestThemGestureLocalAdd itemID
        tryAddParentPassive' themID
    tryAddParentPassive (Right remoteActivityID) = do
        DestThemGestureRemote themID _ _ <-
            lift $ MaybeT $ getValBy $ UniqueDestThemGestureRemoteAdd remoteActivityID
        tryAddParentPassive' themID

    tryRemoveChild' itemID = do
        SourceRemove sendID _ <-
            lift $ MaybeT $ getValBy $ UniqueSourceRemove itemID
        SourceUsSendDelegator sourceID grantID <- lift $ lift $ getJust sendID
        lift $ verifySourceHolder sourceID
        topic <- do
            t <- lift . lift $ getSourceTopic sourceID
            bitraverse
                (\ (l, k) ->
                    case k of
                        Left j -> pure (l, j)
                        Right _ -> error "Project Source topic is a Group, impossible"
                )
                pure
                t
        return (sourceID, sendID, grantID, topic)

    tryRemoveChild inboxID (Left (_actorByKey, _actorEntity, itemID)) = do
        InboxItemLocal _ _ i <-
            lift $ MaybeT $ getValBy $ UniqueInboxItemLocal inboxID itemID
        tryRemoveChild' i
    tryRemoveChild inboxID (Right remoteActivityID) = do
        InboxItemRemote _ _ i <-
            lift $ MaybeT $ getValBy $ UniqueInboxItemRemote inboxID remoteActivityID
        tryRemoveChild' i

    verifySquadHolder :: ResourceId -> SquadId -> MaybeT ActDB ()
    verifySquadHolder meResourceID squadID = do
        Squad _ r <- lift $ getJust squadID
        guard $ r == meResourceID

    tryAddTeamActive' r squadID = do
        usID <- lift $ MaybeT $ getKeyBy $ UniqueSquadOriginUs squadID
        lift $ verifySquadHolder r squadID
        topic <- lift . lift $ getSquadTeam squadID
        return (squadID, topic, Left ())

    tryAddTeamActive r (Left (_actorByKey, _actorEntity, itemID)) = do
        SquadUsGestureLocal squadID _ <-
            lift $ MaybeT $ getValBy $ UniqueSquadUsGestureLocalActivity itemID
        tryAddTeamActive' r squadID
    tryAddTeamActive r (Right remoteActivityID) = do
        SquadUsGestureRemote squadID _ _ <-
            lift $ MaybeT $ getValBy $ UniqueSquadUsGestureRemoteActivity remoteActivityID
        tryAddTeamActive' r squadID

    tryAddTeamPassive' r themID = do
        SquadOriginThem squadID <- lift . lift $ getJust themID
        lift $ verifySquadHolder r squadID
        topic <- lift . lift $ getSquadTeam squadID
        return (squadID, topic, Right themID)

    tryAddTeamPassive r (Left (_actorByKey, _actorEntity, itemID)) = do
        SquadThemGestureLocal themID _ <-
            lift $ MaybeT $ getValBy $ UniqueSquadThemGestureLocalAdd itemID
        tryAddTeamPassive' r themID
    tryAddTeamPassive r (Right remoteActivityID) = do
        SquadThemGestureRemote themID _ _ <-
            lift $ MaybeT $ getValBy $ UniqueSquadThemGestureRemoteAdd remoteActivityID
        tryAddTeamPassive' r themID

    componentIsAuthor ident =
        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        in  author == bimap (resourceToActor . componentResource . snd) snd ident

    theyIsAuthor :: Either (a, ProjectId) (b, RemoteActorId) -> Bool
    theyIsAuthor ident =
        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        in  author == bimap (LocalActorProject . snd) snd ident

    theyIsAuthor' :: Either (a, GroupId) (b, RemoteActorId) -> Bool
    theyIsAuthor' ident =
        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        in  author == bimap (LocalActorGroup . snd) snd ident

    addCollab (collabID, fulfills, inviterOrJoiner) = do

        collab <-
            bitraverse

                -- If accepting an Invite, find the Collab recipient and verify
                -- it's the sender of the Accept
                (\ fulfillsID -> withDBExcept $ do
                    recip <-
                        lift $
                        requireEitherAlt
                            (getBy $ UniqueCollabRecipLocal collabID)
                            (getBy $ UniqueCollabRecipRemote collabID)
                            "Found Collab with no recip"
                            "Found Collab with multiple recips"
                    case (recip, authorIdMsig) of
                        (Left (Entity crlid crl), Left (LocalActorPerson personID, _, _))
                            | collabRecipLocalPerson crl == personID ->
                                return (fulfillsID, Left crlid)
                        (Right (Entity crrid crr), Right (author, _, _))
                            | collabRecipRemoteActor crr == remoteAuthorId author ->
                                return (fulfillsID, Right crrid)
                        _ -> throwE "Accepting an Invite whose recipient is someone else"
                )

                -- If accepting a Join, verify accepter has permission
                (\ fulfillsID -> do
                    let muCap = AP.activityCapability $ actbActivity body
                    uCap <- fromMaybeE muCap "No capability provided"
                    verifyCapability''
                        uCap
                        authorIdMsig
                        (LocalResourceProject projectID)
                        AP.RoleAdmin
                    return fulfillsID
                )

                fulfills

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust projectID
                let actorID = projectActor recip
                (actorID,) <$> getJust actorID

            -- In collab mode, verify the Collab isn't already validated
            maybeEnabled <- lift $ getBy $ UniqueCollabEnable collabID
            verifyNothingE maybeEnabled "I already sent a Grant for this Invite/Join"

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                -- Record the Accept and enable the Collab
                (grantID, enableID) <- do
                    case (collab, acceptDB) of
                        (Left (fulfillsID, Left recipID), Left (_, _, acceptID)) -> do
                            maybeAccept <- lift $ insertUnique $ CollabRecipLocalAccept recipID fulfillsID acceptID
                            unless (isJust maybeAccept) $
                                throwE "This Invite already has an Accept by recip"
                        (Left (fulfillsID, Right recipID), Right (_, _, acceptID)) -> do
                            maybeAccept <- lift $ insertUnique $ CollabRecipRemoteAccept recipID fulfillsID acceptID
                            unless (isJust maybeAccept) $
                                throwE "This Invite already has an Accept by recip"
                        (Right fulfillsID, Left (_, _, acceptID)) -> do
                            maybeAccept <- lift $ insertUnique $ CollabApproverLocal fulfillsID acceptID
                            unless (isJust maybeAccept) $
                                throwE "This Join already has an Accept"
                        (Right fulfillsID, Right (author, _, acceptID)) -> do
                            maybeAccept <- lift $ insertUnique $ CollabApproverRemote fulfillsID (remoteAuthorId author) acceptID
                            unless (isJust maybeAccept) $
                                throwE "This Join already has an Accept"
                        _ -> error "projectAccept impossible"
                    grantID <- lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
                    enableID <- lift $ insert $ CollabEnable collabID grantID
                    return (grantID, enableID)

                -- Prepare forwarding of Accept to my followers
                let recipByID = LocalActorProject projectID
                recipByHash <- hashLocalActor recipByID
                let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

                -- Prepare a regular Grant
                let isInvite = isLeft collab
                grant@(actionGrant, _, _, _) <- lift $ do
                    Collab role _ <- getJust collabID
                    prepareCollabGrant isInvite inviterOrJoiner role
                let recipByKey = LocalActorProject projectID
                _luGrant <- lift $ updateOutboxItem' recipByKey grantID actionGrant

                return (recipActorID, sieve, grantID, grant, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant), inboxItemID) -> do
                let recipByID = LocalActorProject projectID
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $
                    sendActivity
                        recipByID recipActorID localRecipsGrant
                        remoteRecipsGrant fwdHostsGrant grantID actionGrant
                doneDB inboxItemID "[Collab mode] Forwarded the Accept and published a Grant"

    addComp (componentID, ident, inviteOrAdd) = do

        comp <-
            bitraverse

                -- If accepting an Invite-component, there's nothing to check
                -- at this point
                pure

                -- If accepting an Add-component:
                --     * If the sender is the component, verify I haven't seen
                --       a component-Accept on this Add
                --     * Otherwise, verify I've seen the component-Accept for
                --       this Add and that the new Accept is authorized
                (\ () -> do
                    maybeComponentAccept <-
                        lift $ withDB $
                        case bimap fst fst ident of
                            Left localID -> (() <$) <$> getBy (UniqueComponentAcceptLocal localID)
                            Right remoteID -> (() <$) <$> getBy (UniqueComponentAcceptRemote remoteID)
                    if componentIsAuthor ident
                        then
                            verifyNothingE
                                maybeComponentAccept
                                    "I've already seen a ComponentAccept* on \
                                    \that Add"
                        else do
                            fromMaybeE
                                maybeComponentAccept
                                "I haven't yet seen the Component's Accept on \
                                \the Add"
                            let muCap = AP.activityCapability $ actbActivity body
                            uCap <- fromMaybeE muCap "No capability provided"
                            verifyCapability''
                                uCap
                                authorIdMsig
                                (LocalResourceProject projectID)
                                AP.RoleAdmin
                )

                inviteOrAdd

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust projectID
                let actorID = projectActor recip
                (actorID,) <$> getJust actorID

            -- In component mode, verify the Component isn't already validated
            maybeEnabled <- lift $ getBy $ UniqueComponentEnable componentID
            verifyNothingE maybeEnabled "I already sent a delegator-Grant for this Invite/Add"

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                maybeGrantData <-
                    case comp of

                        -- In Invite-component mode, only if the Accept author is the
                        -- component, record the Accept and enable the Component
                        Left () ->
                            lift $ if componentIsAuthor ident
                                then Just <$> do
                                    case (ident, acceptDB) of
                                        (Left (localID, _), Left (_, _, acceptID)) ->
                                            insert_ $ ComponentAcceptLocal localID acceptID
                                        (Right (remoteID, _), Right (_, _, acceptID)) ->
                                            insert_ $ ComponentAcceptRemote remoteID acceptID
                                        _ -> error "personAccept impossible ii"
                                    grantID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                                    enableID <- insert $ ComponentEnable componentID grantID
                                    return (grantID, enableID, False)
                                else pure Nothing

                        -- In Add-component mode:
                        -- * If the sender is the component, record the Accept
                        -- * Otherwise, record the Accept and enable the Component
                        Right () ->
                            lift $ if componentIsAuthor ident
                                then do
                                    case (ident, acceptDB) of
                                        (Left (localID, _), Left (_, _, acceptID)) ->
                                            insert_ $ ComponentAcceptLocal localID acceptID
                                        (Right (remoteID, _), Right (_, _, acceptID)) ->
                                            insert_ $ ComponentAcceptRemote remoteID acceptID
                                        _ -> error "personAccept impossible iii"
                                    return Nothing
                                else Just <$> do
                                    case acceptDB of
                                        Left (_, _, acceptID) ->
                                            insert_ $ ComponentProjectGestureLocal componentID acceptID
                                        Right (author, _, acceptID) ->
                                            insert_ $ ComponentProjectGestureRemote componentID (remoteAuthorId author) acceptID
                                    grantID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                                    enableID <- insert $ ComponentEnable componentID grantID
                                    return (grantID, enableID, True)

                -- Prepare forwarding of Accept to my followers
                let recipByID = LocalActorProject projectID
                recipByHash <- hashLocalActor recipByID
                let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

                -- In Invite-component mode, only if the Accept author is
                -- the component, prepare a delegator-Grant
                --
                -- In Add-component mode, only if the Accept author isn't
                -- the component, prepare a delegator-Grant
                maybeGrant <- for maybeGrantData $ \ (grantID, enableID, includeAuthor) -> lift $ do
                    grant@(actionGrant, _, _, _) <-
                        prepareDelegGrant (bimap snd snd ident) enableID includeAuthor
                    let recipByKey = LocalActorProject projectID
                    _luGrant <- updateOutboxItem' recipByKey grantID actionGrant
                    return (grantID, grant)

                return (recipActorID, sieve, maybeGrant, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, maybeGrant, inboxItemID) -> do
                let recipByID = LocalActorProject projectID
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ for_ maybeGrant $ \ (grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant)) ->
                    sendActivity
                        recipByID recipActorID localRecipsGrant
                        remoteRecipsGrant fwdHostsGrant grantID actionGrant
                doneDB inboxItemID "[Component mode] Forwarded the Accept and maybe published a Grant"

    -- Add-a-child mode
    addChildParent (Left (sourceID, topic, mode)) = do

        mode' <-
            bitraverse

                -- Child-active mode
                -- Verify we haven't yet seen child's Accept
                (\ () -> do
                    maybeChildAccept <-
                        lift $ withDB $
                        case bimap fst fst topic of
                            Left localID -> (() <$) <$> getBy (UniqueSourceThemAcceptLocal localID)
                            Right remoteID -> (() <$) <$> getBy (UniqueSourceThemAcceptRemote remoteID)
                    verifyNothingE maybeChildAccept "I already saw child's Accept"
                )

                -- Child-passive mode
                -- Option 1: We haven't seen child's Accept yet
                --   * Verify sender is the child
                -- Option 2: We saw it, but not my collaborator's Accept
                --   * Verify the Accept is authorized
                -- Otherwise respond with error, no Accept is needed
                (\ () -> do
                    (maybeChildAccept, maybeGrant) <-
                        lift $ withDB $ liftA2 (,)
                            (case bimap fst fst topic of
                                Left localID -> (() <$) <$> getBy (UniqueSourceThemAcceptLocal localID)
                                Right remoteID -> (() <$) <$> getBy (UniqueSourceThemAcceptRemote remoteID)
                            )
                            (getBy $ UniqueSourceUsSendDelegator sourceID)
                    case (isJust maybeChildAccept, isJust maybeGrant) of
                        (False, True) -> error "Impossible/bug, didn't see child's Accept but sent a Grant"
                        (False, False) -> do
                            unless (theyIsAuthor topic) $
                                throwE "The Accept I'm waiting for is from my new child"
                            return $ Left ()
                        (True, False) -> do
                            let muCap = AP.activityCapability $ actbActivity body
                            uCap <- fromMaybeE muCap "No capability provided"
                            verifyCapability''
                                uCap
                                authorIdMsig
                                (LocalResourceProject projectID)
                                AP.RoleAdmin
                            return $ Right ()
                        (True, True) -> throwE "Child already enabled, not needing any further Accept"
                )

                mode

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust projectID
                let actorID = projectActor recip
                (actorID,) <$> getJust actorID

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                idsForGrant <-
                    lift $
                    bitraverse

                        -- Child-active mode
                        -- If sender is child, record the Accept into the
                        -- Source record & prepare to send degelator-Grant
                        -- Othrerwise do nothing
                        (\ () ->
                            if theyIsAuthor topic
                                then Just <$> do
                                    case (topic, acceptDB) of
                                        (Left (localID, _), Left (_, _, acceptID)) ->
                                            insert_ $ SourceThemAcceptLocal localID acceptID
                                        (Right (remoteID, _), Right (_, _, acceptID)) ->
                                            insert_ $ SourceThemAcceptRemote remoteID acceptID
                                        _ -> error "projectAccept impossible iv"
                                    grantID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                                    insert_ $ SourceUsSendDelegator sourceID grantID
                                    return grantID
                                else pure Nothing
                        )

                        -- Child-passive mode
                        (\case

                            -- Getting an Accept from the child
                            -- Record child's Accept in Source record
                            Left () -> do
                                case (topic, acceptDB) of
                                    (Left (localID, _), Left (_, _, acceptID)) ->
                                        insert_ $ SourceThemAcceptLocal localID acceptID
                                    (Right (remoteID, _), Right (_, _, acceptID)) ->
                                        insert_ $ SourceThemAcceptRemote remoteID acceptID
                                    _ -> error "projectAccept impossible v"
                                return Nothing

                            -- Getting an Accept from my collaborator
                            -- Record my collaborator's Accept
                            -- Prepare to send delegator-Grant
                            Right () -> Just <$> do
                                {-
                                case (topic, acceptDB) of
                                    (Left (localID, _), Left (_, _, acceptID)) ->
                                        insert_ $ ? localID acceptID
                                    (Right (remoteID, _), Right (_, _, acceptID)) ->
                                        insert_ $ ? remoteID acceptID
                                    _ -> error "projectAccept impossible iv"
                                -}
                                grantID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                                insert_ $ SourceUsSendDelegator sourceID grantID
                                return grantID
                        )

                        mode'


                -- Prepare forwarding of Accept to my followers
                let recipByID = LocalActorProject projectID
                recipByHash <- hashLocalActor recipByID
                let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

                maybeAct <-
                    case idsForGrant of
                        Left mg -> for mg $ \ grantID -> lift $ do
                            grant@(actionGrant, _, _, _) <-
                                prepareSourceDelegGrant (bimap snd snd topic) False
                            let recipByKey = LocalActorProject projectID
                            _luGrant <- updateOutboxItem' recipByKey grantID actionGrant
                            return (grantID, grant)

                        Right mg -> for mg $ \ grantID -> lift $ do
                            grant@(actionGrant, _, _, _) <-
                                prepareSourceDelegGrant (bimap snd snd topic) True
                            let recipByKey = LocalActorProject projectID
                            _luGrant <- updateOutboxItem' recipByKey grantID actionGrant
                            return (grantID, grant)

                return (recipActorID, sieve, maybeAct, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, maybeGrant, inboxItemID) -> do
                let recipByID = LocalActorProject projectID
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ for_ maybeGrant $ \ (grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant)) ->
                    sendActivity
                        recipByID recipActorID localRecipsGrant
                        remoteRecipsGrant fwdHostsGrant grantID actionGrant
                doneDB inboxItemID "[Child mode] Forwarded the Accept and maybe published a Grant/Accept"

    -- Add-a-parent mode
    addChildParent (Right (destID, topic, mode)) = do

        (themID, mode') <-
            case mode of

                -- Parent-active mode
                -- Respond with error, we aren't supposed to get any Accept
                Left () -> throwE "Parent-active (DestOriginUs) mode, I'm not expecting any Accept"

                -- Parent-passive mode
                -- Option 1: I haven't yet seen parent's Accept
                --   * Verify sender is the parent
                -- Option 2: I saw it, but not my collaborator's Accept
                --   * Verify the accept is authorized
                -- Otherwise respond with error, no Accept is needed
                Right themID -> (themID,) <$> do
                    (maybeParentAccept, maybeUsGesture) <-
                        lift $ withDB $ liftA2 (,)
                            (case bimap fst fst topic of
                                Left localID -> (() <$) <$> getBy (UniqueDestThemAcceptLocalTopic localID)
                                Right remoteID -> (() <$) <$> getBy (UniqueDestThemAcceptRemoteTopic remoteID)
                            )
                            (do l <- getBy $ UniqueDestUsGestureLocal destID
                                r <- getBy $ UniqueDestUsGestureRemote destID
                                case (isJust l, isJust r) of
                                    (False, False) -> pure Nothing
                                    (False, True) -> pure $ Just ()
                                    (True, False) -> pure $ Just ()
                                    (True, True) -> error "Both DestUsGestureLocal and DestUsGestureRemote"
                            )
                    case (isJust maybeParentAccept, isJust maybeUsGesture) of
                        (False, True) -> error "Impossible/bug, didn't see parent's Accept but recorded my collaborator's Accept"
                        (False, False) -> do
                            unless (theyIsAuthor topic) $
                                throwE "The Accept I'm waiting for is from my new parent"
                            return $ Left ()
                        (True, False) -> do
                            let muCap = AP.activityCapability $ actbActivity body
                            uCap <- fromMaybeE muCap "No capability provided"
                            verifyCapability''
                                uCap
                                authorIdMsig
                                (LocalResourceProject projectID)
                                AP.RoleAdmin
                            return $ Right ()
                        (True, True) -> throwE "Just waiting for Grant from parent, or already have it, anyway not needing any further Accept"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust projectID
                let actorID = projectActor recip
                (actorID,) <$> getJust actorID

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                idsForGrant <-
                    lift $ case mode' of

                        -- Getting an Accept from the parent
                        -- Record parent's Accept in the Dest record
                        Left () -> do
                            case (topic, acceptDB) of
                                (Left (localID, _), Left (_, _, acceptID)) ->
                                    insert_ $ DestThemAcceptLocal themID localID acceptID
                                (Right (remoteID, _), Right (_, _, acceptID)) ->
                                    insert_ $ DestThemAcceptRemote themID remoteID acceptID
                                _ -> error "projectAccept impossible v"
                            return Nothing

                        -- Getting an Accept from my collaborator
                        -- Record my collaborator's Accept in the Dest record
                        -- Prepare to send my own Accept
                        Right () -> Just <$> do
                            case acceptDB of
                                Left (_, _, acceptID) ->
                                    insert_ $ DestUsGestureLocal destID acceptID
                                Right (author, _, acceptID) ->
                                    insert_ $ DestUsGestureRemote destID (remoteAuthorId author) acceptID
                            acceptID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                            insert_ $ DestUsAccept destID acceptID
                            return acceptID

                -- Prepare forwarding of Accept to my followers
                let recipByID = LocalActorProject projectID
                recipByHash <- hashLocalActor recipByID
                let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

                maybeAct <-
                    for idsForGrant $ \ acceptID -> lift $ do
                        accept@(actionAccept, _, _, _) <-
                            prepareDestAccept (bimap snd snd topic)
                        let recipByKey = LocalActorProject projectID
                        _luAccept <- updateOutboxItem' recipByKey acceptID actionAccept
                        return (acceptID, accept)

                return (recipActorID, sieve, maybeAct, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, maybeGrant, inboxItemID) -> do
                let recipByID = LocalActorProject projectID
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ for_ maybeGrant $ \ (grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant)) ->
                    sendActivity
                        recipByID recipActorID localRecipsGrant
                        remoteRecipsGrant fwdHostsGrant grantID actionGrant
                doneDB inboxItemID "[Parent mode] Forwarded the Accept and maybe published a Grant/Accept"

    prepareCollabGrant isInvite sender role = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audAccepter <- makeAudSenderWithFollowers authorIdMsig
        audApprover <- lift $ makeAudSenderOnly authorIdMsig
        recipHash <- encodeKeyHashid projectID
        let topicByHash = LocalActorProject recipHash

        senderHash <- bitraverse hashLocalActor pure sender

        uAccepter <- lift $ getActorURI authorIdMsig

        let audience =
                if isInvite
                    then
                        let audInviter =
                                case senderHash of
                                    Left actor -> AudLocal [actor] []
                                    Right (ObjURI h lu, _followers) ->
                                        AudRemote h [lu] []
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audInviter, audAccepter, audTopic]
                    else
                        let audJoiner =
                                case senderHash of
                                    Left actor -> AudLocal [actor] [localActorFollowers actor]
                                    Right (ObjURI h lu, followers) ->
                                        AudRemote h [lu] (maybeToList followers)
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audJoiner, audApprover, audTopic]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [AP.acceptObject accept]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXRole role
                    , AP.grantContext   =
                        encodeRouteHome $ renderLocalActor topicByHash
                    , AP.grantTarget    =
                        if isInvite
                            then uAccepter
                            else case senderHash of
                                Left actor ->
                                    encodeRouteHome $ renderLocalActor actor
                                Right (ObjURI h lu, _) -> ObjURI h lu
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    prepareDelegGrant ident _enableID includeAuthor = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        (uComponent, audComponent) <-
            case ident of
                Left c -> do
                    a <- resourceToActor . componentResource <$> hashComponent c
                    return
                        ( encodeRouteHome $ renderLocalActor a
                        , AudLocal [a] [localActorFollowers a]
                        )
                Right raID -> do
                    ra <- getJust raID
                    u@(ObjURI h lu) <- getRemoteActorURI ra
                    return
                        ( u
                        , AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
                        )
        audAuthor <- lift $ makeAudSenderOnly authorIdMsig
        projectHash <- encodeKeyHashid projectID
        let audProject = AudLocal [] [LocalStageProjectFollowers projectHash]

            audience =
                if includeAuthor
                    then [audComponent, audProject, audAuthor]
                    else [audComponent, audProject]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [AP.acceptObject accept]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXDelegator
                    , AP.grantContext   = encodeRouteHome $ ProjectR projectHash
                    , AP.grantTarget    = uComponent
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    prepareSourceDelegGrant ident includeAuthor = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        (uSource, audSource) <-
            case ident of
                Left j -> do
                    h <- encodeKeyHashid j
                    return
                        ( encodeRouteHome $ ProjectR h
                        , AudLocal [LocalActorProject h] [LocalStageProjectFollowers h]
                        )
                Right raID -> do
                    ra <- getJust raID
                    u@(ObjURI h lu) <- getRemoteActorURI ra
                    return
                        ( u
                        , AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
                        )
        audAuthor <- lift $ makeAudSenderOnly authorIdMsig
        projectHash <- encodeKeyHashid projectID
        let audProject = AudLocal [] [LocalStageProjectFollowers projectHash]

            audience =
                if includeAuthor
                    then [audSource, audProject, audAuthor]
                    else [audSource, audProject]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [AP.acceptObject accept]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXDelegator
                    , AP.grantContext   = encodeRouteHome $ ProjectR projectHash
                    , AP.grantTarget    = uSource
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    prepareDestAccept topic = do
        encodeRouteHome <- getEncodeRouteHome

        audMyCollab <- lift $ makeAudSenderOnly authorIdMsig
        audDest <-
            case topic of
                Left j -> do
                    h <- encodeKeyHashid j
                    return $
                        AudLocal [LocalActorProject h] [LocalStageProjectFollowers h]
                Right raID -> do
                    ra <- getJust raID
                    ObjURI h lu <- getRemoteActorURI ra
                    return $
                        AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
        audMe <-
            AudLocal [] . pure . LocalStageProjectFollowers <$>
                encodeKeyHashid projectID
        uCollabAccept <- lift $ getActivityURI authorIdMsig
        let uAdd = AP.acceptObject accept

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audMyCollab, audDest, audMe]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uCollabAccept]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uAdd
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    removeChild (sourceID, sendID, grantID, child) = do

        -- Verify the sender is the topic
        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        unless (author == bimap (LocalActorProject . snd) snd child) $
            throwE "The Accept isn't by the to-be-removed child project"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                -- Grab extension-Grants that I'm about to revoke
                gathers <- selectList [SourceUsGatherSource ==. sendID] []
                leafs <- selectList [SourceUsLeafSource ==. sendID] []
                conveys <- selectList [SourceUsConveySource ==. sendID] []

                -- Delete the whole Source record
                deleteWhere [SourceRemoveSend ==. sendID]
                let gatherIDs = map entityKey gathers
                deleteWhere [SourceUsGatherFromLocalGather <-. gatherIDs]
                deleteWhere [SourceUsGatherFromRemoteGather <-. gatherIDs]
                deleteWhere [SourceUsGatherId <-. gatherIDs]
                let leafIDs = map entityKey leafs
                deleteWhere [SourceUsLeafFromLocalLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafFromRemoteLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafToLocalLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafToRemoteLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafId <-. leafIDs]
                let conveyIDs = map entityKey conveys
                deleteWhere [SourceUsConveyFromLocalConvey <-. conveyIDs]
                deleteWhere [SourceUsConveyFromRemoteConvey <-. conveyIDs]
                deleteWhere [SourceUsConveyId <-. conveyIDs]
                case child of
                    Left (localID, _) -> do
                        acceptID <- getKeyByJust $ UniqueSourceThemAcceptLocal localID
                        deleteWhere [SourceThemDelegateLocalSource ==. acceptID]
                        delete acceptID
                    Right (remoteID, _) -> do
                        acceptID <- getKeyByJust $ UniqueSourceThemAcceptRemote remoteID
                        deleteWhere [SourceThemDelegateRemoteSource ==. acceptID]
                        delete acceptID
                delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueSourceOriginUs sourceID)
                        (getKeyBy $ UniqueSourceOriginThem sourceID)
                        "Neither us nor them"
                        "Both us and them"
                case origin of
                    Left usID -> do
                        deleteBy $ UniqueSourceUsAccept usID
                        deleteBy $ UniqueSourceUsGestureLocal usID
                        deleteBy $ UniqueSourceUsGestureRemote usID
                        delete usID
                    Right themID -> do
                        deleteBy $ UniqueSourceThemGestureLocal themID
                        deleteBy $ UniqueSourceThemGestureRemote themID
                        delete themID
                case child of
                    Left (l, _) -> do
                        deleteBy $ UniqueSourceTopicProjectTopic l
                        delete l
                    Right (r, _) ->
                        delete r
                deleteBy $ UniqueSourceHolderProject sourceID
                delete sourceID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid projectID
                    let topicByHash =
                            LocalActorProject topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare main Revoke activity and insert to my outbox
                revoke@(actionRevoke, _, _, _) <- prepareMainRevoke (bimap snd snd child) grantID
                let recipByKey = LocalActorProject projectID
                revokeID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                _luRevoke <- updateOutboxItem' recipByKey revokeID actionRevoke

                -- Prepare and insert Revokes on all the extension-Grants
                revokesG <- for gathers $ \ (Entity _ (SourceUsGather _ startID grantID)) -> do
                    DestUsStart acceptID _ <- getJust startID
                    DestUsAccept destID _ <- getJust acceptID
                    parent <- do
                        p <- getDestTopic destID
                        bitraverse
                            (\case
                                Left j -> pure $ LocalActorProject j
                                Right _ -> error "I'm a project but I have a parent who is a Group"
                            )
                            pure
                            (bimap snd snd p)
                    return (parent, grantID)
                revokesL <- for leafs $ \ (Entity _ (SourceUsLeaf _ enableID grantID)) -> do
                    CollabEnable collabID _ <- getJust enableID
                    recip <- getCollabRecip collabID
                    return
                        ( bimap
                            (LocalActorPerson . collabRecipLocalPerson . entityVal)
                            (collabRecipRemoteActor . entityVal)
                            recip
                        , grantID
                        )
                revokesC <- for conveys $ \ (Entity _ (SourceUsConvey _ startID grantID)) -> do
                    SquadUsStart acceptID _ <- getJust startID
                    SquadUsAccept squadID _ <- getJust acceptID
                    team <- bimap (LocalActorGroup . snd) snd <$> getSquadTeam squadID
                    return (team, grantID)
                revokes <- for (revokesG ++ revokesL ++ revokesC) $ \ (actor, grantID) -> do
                    ext@(actionExt, _, _, _) <- prepareExtRevoke actor grantID
                    let recipByKey = LocalActorProject projectID
                    extID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return (projectActor project, sieve, revokeID, revoke, revokes, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, revokeID, (actionRevoke, localRecipsRevoke, remoteRecipsRevoke, fwdHostsRevoke), revokes, inboxItemID) -> do
                let topicByID = LocalActorProject projectID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $ do
                    sendActivity
                        topicByID topicActorID localRecipsRevoke
                        remoteRecipsRevoke fwdHostsRevoke revokeID actionRevoke
                    for_ revokes $ \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            topicByID topicActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "[Remove-Child mode] Deleted the Child/Source, forwarded Accept, sent Revokes"

        where

        prepareMainRevoke child grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            recipHash <- encodeKeyHashid projectID
            let topicByHash = LocalActorProject recipHash

            childHash <- bitraverse encodeKeyHashid pure child

            audRemover <- lift $ makeAudSenderOnly authorIdMsig
            audChild <-
                    case childHash of
                        Left j ->
                            pure $
                            AudLocal [LocalActorProject j] [LocalStageProjectFollowers j]
                        Right actorID -> do
                            actor <- getJust actorID
                            ObjURI h lu <- getRemoteActorURI actor
                            return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)
            let audMe = AudLocal [] [localActorFollowers topicByHash]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRemover, audChild, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote

            let uRemove = AP.acceptObject accept
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtRevoke recipient grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            let topicByHash = LocalActorProject projectHash

            audRecip <-
                case recipient of
                    Left a -> do
                        h <- hashLocalActor a
                        return $ AudLocal [h] [localActorFollowers h]
                    Right actorID -> do
                        actor <- getJust actorID
                        ObjURI h lu <- getRemoteActorURI actor
                        return $
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRecip]

                recips = map encodeRouteHome audLocal ++ audRemote

            let uRemove = AP.acceptObject accept
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    addTeam (squadID, topic, mode) = do

        (themID, mode') <-
            case mode of

                -- Team-active mode
                -- Respond with error, we aren't supposed to get any Accept
                Left () -> throwE "Team-active (SquadOriginUs) mode, I'm not expecting any Accept"

                -- Team-passive mode
                -- Option 1: I haven't yet seen team's Accept
                --   * Verify sender is the team
                -- Option 2: I saw it, but not my collaborator's Accept
                --   * Verify the accept is authorized
                -- Otherwise respond with error, no Accept is needed
                Right themID -> (themID,) <$> do
                    (maybeTeamAccept, maybeUsGesture) <-
                        lift $ withDB $ liftA2 (,)
                            (case bimap fst fst topic of
                                Left localID -> (() <$) <$> getBy (UniqueSquadThemAcceptLocalTopic localID)
                                Right remoteID -> (() <$) <$> getBy (UniqueSquadThemAcceptRemoteTopic remoteID)
                            )
                            (do l <- getBy $ UniqueSquadUsGestureLocal squadID
                                r <- getBy $ UniqueSquadUsGestureRemote squadID
                                case (isJust l, isJust r) of
                                    (False, False) -> pure Nothing
                                    (False, True) -> pure $ Just ()
                                    (True, False) -> pure $ Just ()
                                    (True, True) -> error "Both SquadUsGestureLocal and SquadUsGestureRemote"
                            )
                    case (isJust maybeTeamAccept, isJust maybeUsGesture) of
                        (False, True) -> error "Impossible/bug, didn't see team's Accept but recorded my collaborator's Accept"
                        (False, False) -> do
                            unless (theyIsAuthor' topic) $
                                throwE "The Accept I'm waiting for is from my new team"
                            return $ Left ()
                        (True, False) -> do
                            let muCap = AP.activityCapability $ actbActivity body
                            uCap <- fromMaybeE muCap "No capability provided"
                            verifyCapability''
                                uCap
                                authorIdMsig
                                (LocalResourceProject projectID)
                                AP.RoleAdmin
                            return $ Right ()
                        (True, True) -> throwE "Just waiting for Grant from team, or already have it, anyway not needing any further Accept"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust projectID
                let actorID = projectActor recip
                (actorID,) <$> getJust actorID

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                idsForGrant <-
                    lift $ case mode' of

                        -- Getting an Accept from the team
                        -- Record team's Accept in the Squad record
                        Left () -> do
                            case (topic, acceptDB) of
                                (Left (localID, _), Left (_, _, acceptID)) ->
                                    insert_ $ SquadThemAcceptLocal themID localID acceptID
                                (Right (remoteID, _), Right (_, _, acceptID)) ->
                                    insert_ $ SquadThemAcceptRemote themID remoteID acceptID
                                _ -> error "projectAccept impossible v"
                            return Nothing

                        -- Getting an Accept from my collaborator
                        -- Record my collaborator's Accept in the Squad record
                        -- Prepare to send my own Accept
                        Right () -> Just <$> do
                            case acceptDB of
                                Left (_, _, acceptID) ->
                                    insert_ $ SquadUsGestureLocal squadID acceptID
                                Right (author, _, acceptID) ->
                                    insert_ $ SquadUsGestureRemote squadID (remoteAuthorId author) acceptID
                            acceptID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                            insert_ $ SquadUsAccept squadID acceptID
                            return acceptID

                -- Prepare forwarding of Accept to my followers
                let recipByID = LocalActorProject projectID
                recipByHash <- hashLocalActor recipByID
                let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

                maybeAct <-
                    for idsForGrant $ \ acceptID -> lift $ do
                        accept@(actionAccept, _, _, _) <-
                            prepareSquadAccept (bimap snd snd topic)
                        let recipByKey = LocalActorProject projectID
                        _luAccept <- updateOutboxItem' recipByKey acceptID actionAccept
                        return (acceptID, accept)

                return (recipActorID, sieve, maybeAct, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, maybeGrant, inboxItemID) -> do
                let recipByID = LocalActorProject projectID
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ for_ maybeGrant $ \ (grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant)) ->
                    sendActivity
                        recipByID recipActorID localRecipsGrant
                        remoteRecipsGrant fwdHostsGrant grantID actionGrant
                doneDB inboxItemID "[Team] Forwarded the Accept and maybe published a Grant/Accept"

        where

        prepareSquadAccept topic = do
            encodeRouteHome <- getEncodeRouteHome

            audMyCollab <- lift $ makeAudSenderOnly authorIdMsig
            audSquad <-
                case topic of
                    Left j -> do
                        h <- encodeKeyHashid j
                        return $
                            AudLocal [LocalActorGroup h] [LocalStageGroupFollowers h]
                    Right raID -> do
                        ra <- getJust raID
                        ObjURI h lu <- getRemoteActorURI ra
                        return $
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
            audMe <-
                AudLocal [] . pure . LocalStageProjectFollowers <$>
                    encodeKeyHashid projectID
            uCollabAccept <- lift $ getActivityURI authorIdMsig
            let uAdd = AP.acceptObject accept

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audMyCollab, audSquad, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uCollabAccept]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uAdd
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

checkExistingComponents
    :: ProjectId -> Either (ComponentBy Entity) RemoteActorId -> ActDBE ()
checkExistingComponents projectID componentDB = do

    -- Find existing Component records I have for this component
    componentIDs <- lift $ getExistingComponents $ first localComponentID $ componentDB

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    byEnabled <-
        lift $ for componentIDs $ \ (componentID, _) ->
            isJust <$> runMaybeT (tryComponentEnabled componentID)
    case length $ filter id byEnabled of
        0 -> return ()
        1 -> throwE "I already have a ComponentEnable for this component"
        _ -> error "Multiple ComponentEnable for a component"

    -- Verify none of the Component records are already in
    -- Add-waiting-for-project or Invite-waiting-for-component state
    anyStarted <-
        lift $ runMaybeT $ asum $
            map (\ (componentID, identID) ->
                    tryComponentAddAccept componentID identID <|>
                    tryComponentInviteAccept componentID
                )
                componentIDs
    unless (isNothing anyStarted) $
        throwE
            "One of the Component records is already in Add-Accept or \
            \Invite-Accept state"

    where

    getExistingComponents (Left komponentID) =
        fmap (map $ bimap E.unValue (Left . E.unValue)) $
        E.select $ E.from $ \ (local `E.InnerJoin` comp) -> do
            E.on $ local E.^. ComponentLocalComponent E.==. comp E.^. ComponentId
            E.where_ $
                local E.^. ComponentLocalActor E.==. E.val komponentID E.&&.
                comp E.^. ComponentProject E.==. E.val projectID
            return (comp E.^. ComponentId, local E.^. ComponentLocalId)
    getExistingComponents (Right remoteActorID) =
        fmap (map $ bimap E.unValue (Right . E.unValue)) $
        E.select $ E.from $ \ (ident `E.InnerJoin` comp) -> do
            E.on $ ident E.^. ComponentRemoteComponent E.==. comp E.^. ComponentId
            E.where_ $
                ident E.^. ComponentRemoteActor E.==. E.val remoteActorID E.&&.
                comp E.^. ComponentProject E.==. E.val projectID
            return (comp E.^. ComponentId, ident E.^. ComponentRemoteId)

    tryComponentEnabled componentID =
        const () <$> MaybeT (getBy $ UniqueComponentEnable componentID)

    tryComponentAddAccept componentID identID = do
        _ <- MaybeT $ getBy $ UniqueComponentOriginAdd componentID
        case identID of
            Left localID ->
                const () <$>
                    MaybeT (getBy $ UniqueComponentAcceptLocal localID)
            Right remoteID ->
                const () <$>
                    MaybeT (getBy $ UniqueComponentAcceptRemote remoteID)

    tryComponentInviteAccept componentID = do
        originID <- MaybeT $ getKeyBy $ UniqueComponentOriginInvite componentID
        const () <$> MaybeT (getBy $ UniqueComponentProjectAccept originID)

-- Meaning: An actor is adding some object to some target
-- Behavior:
--      * If the target is my components list:
--           * Verify sender is authorized by me to add components to me
--           * Verify B isn't already an active component of mine
--           * Verify B isn't already in a Add-Accept process waiting for
--             project collab to accept too
--           * Verify B isn't already in an Invite-Accept process waiting for
--             component (or its collaborator) to accept too
--           * Insert the Add to my inbox
--           * Create a Component record in DB
--           * Forward the Add to my followers
--           * Send Accept to sender, component+followers, my-followers
--
--      * If the target is my children list:
--        * Verify the object is a project, find in DB/HTTP
--        * Verify the Add is authorized
--        * Verify it's not already an active child of mine
--        * Verify it's not already an active parent of mine
--        * Verify it's not already in an Origin-Us process where I saw the Add
--          and sent my Accept
--        * Verify it's not already in an Origin-Them process, where I saw the
--          Add and the potential child's Accept
--        * Insert the Add to my inbox
--        * Create a Source record in DB
--        * Forward the Add to my followers
--        * Publish an Accept to:
--          * The object project + followers
--          * Add sender + followers
--          * My followers
--        * Record my Accept in the Source record
--
--      * If the target is my parents list:
--        * Verify the object is a project, find in DB/HTTP
--        * Verify the Add is authorized
--        * Verify it's not already an active parent of mine
--        * Verify it's not already an active child of mine
--        * Verify it's not already in an Origin-Us process where I saw the Add
--          and sent my Accept
--        * Verify it's not already in an Origin-Them process, where I saw the
--          Add and the potential parent's Accept
--        * Insert the Add to my inbox
--        * Create a Dest record in DB
--        * Forward the Add to my followers
--        * Publish an Accept to:
--          * The object project + followers
--          * Add sender + followers
--          * My followers
--        * Record my Accept in the Dest record
--
--      * If the target is my teams list:
--        * Verify the object is a team, find in DB/HTTP
--        * Verify the Add is authorized
--        * Verify it's not already an active team of mine
--        * Verify it's not already in an Origin-Us process where I saw the Add
--          and sent my Accept
--        * Verify it's not already in an Origin-Them process, where I saw the
--          Add and the potential team's Accept
--        * Insert the Add to my inbox
--        * Create a Squad record in DB
--        * Forward the Add to my followers
--        * Publish an Accept to:
--          * The object team + followers
--          * Add sender + followers
--          * My followers
--        * Record my Accept in the Squad record
--
--      * If I'm the object, being added to someone's parents/children list:
--        * Verify the target is a project, find in DB/HTTP
--        * Verify it's not already an active parent of mine
--        * Verify it's not already an active child of mine
--        * Verify it's not already in an Origin-Us process where I saw the Add
--          and sent my Accept
--        * Verify it's not already in an Origin-Them process, where I saw the
--          Add and the potential parent/child's Accept
--        * Insert the Add to my inbox
--        * Create a Source/Dest record in DB
--        * Forward the Add to my followers
--
--      * If I'm the object, being added to some teams' resource list:
--        * Verify the target is a team, find in DB/HTTP
--        * Verify it's not already an active team of mine
--        * Verify it's not already in an Origin-Us process where I saw the Add
--          and sent my Accept
--        * Verify it's not already in an Origin-Them process, where I saw the
--          Add and the potential team's Accept
--        * Insert the Add to my inbox
--        * Create a Squad record in DB
--        * Forward the Add to my followers
--
--      * If I'm the object, being added to someone's projects list:
--        * Verify the object is a component, find in DB/HTTP
--        * Verify it's not already an active component of mine
--        * Verify it's not already in a them-Add-Accept process waiting for
--          project collab to accept too
--        * Verify it's not already in an us-Invite-Accept process waiting for
--          component (or its collaborator) to accept too
--        * Insert the Add to my inbox
--        * Create a Component record in DB
--        * Forward the Add to my followers
--
--      * Otherwise, error
projectAdd
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Add URIMode
    -> ActE (Text, Act (), Next)
projectAdd now projectID (Verse authorIdMsig body) add = do

    let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
    (object, target, role) <- parseAdd author add
    unless (role == AP.RoleAdmin) $
        throwE "Add role isn't admin"
    case (target, object) of
        (Left (ATProjectComponents j), _)| j == projectID -> do
            comp <-
                bitraverse
                    (\ la -> fromMaybeE (resourceToComponent =<< actorToResource la) "Not a component")
                    pure
                    object
            addComponentActive comp
        (Left (ATProjectChildren j), _) | j == projectID ->
            addChildActive object
        (Left (ATProjectParents j), _) | j == projectID ->
            addParentActive object
        (Left (ATProjectTeams j), _) | j == projectID ->
            addTeamActive object
        (_, Left (LocalActorProject j)) | j == projectID ->
            case target of
                Left (ATProjectParents j) | j /= projectID ->
                    addChildPassive $ Left j
                Left (ATProjectChildren j) | j /= projectID ->
                    addParentPassive $ Left j
                Left (ATRepoProjects r) ->
                    addComponentPassive $ Left $ ComponentRepo r
                Left (ATDeckProjects d) ->
                    addComponentPassive $ Left $ ComponentDeck d
                Left (ATLoomProjects l) ->
                    addComponentPassive $ Left $ ComponentLoom l
                Left (ATGroupEfforts g) ->
                    addTeamPassive $ Left g
                Right (ObjURI h luColl) -> do
                    -- NOTE this is HTTP GET done synchronously in the activity
                    -- handler
                    manager <- asksEnv envHttpManager
                    c <- AP.fetchAPID_T manager (AP.collectionId :: AP.Collection FedURI URIMode -> LocalURI) h luColl
                    lu <- fromMaybeE (AP.collectionContext c) "No context"
                    rwc <- AP.fetchRWC_T manager h lu
                    AP.Actor l d <-
                        case AP.rwcResource rwc of
                            AP.ResourceActor a -> pure a
                            AP.ResourceChild _ _ -> throwE "Add.target remote ResourceChild"
                    let typ = AP.actorType d
                    if AP.actorTypeIsComponent typ && Just luColl == AP.rwcParentsOrProjects rwc
                        then addComponentPassive $ Right $ ObjURI h lu
                    else if typ == AP.ActorTypeProject && Just luColl == AP.rwcSubprojects rwc
                        then addParentPassive $ Right $ ObjURI h lu
                    else if typ == AP.ActorTypeProject && Just luColl == AP.rwcParentsOrProjects rwc
                        then addChildPassive $ Right $ ObjURI h lu
                    else if typ == AP.ActorTypeTeam && Just luColl == AP.rwcTeamResources rwc
                        then addTeamPassive $ Right $ ObjURI h lu
                    else throwE "Weird collection situation"
                _ -> throwE "I'm being added somewhere irrelevant"
        _ -> throwE "This Add isn't for me"

    where

    addComponentActive component = do

        -- Check capability
        capability <- do

            -- Verify that a capability is provided
            uCap <- do
                let muCap = AP.activityCapability $ actbActivity body
                fromMaybeE muCap "No capability provided"

            -- Verify the capability URI is one of:
            --   * Outbox item URI of a local actor, i.e. a local activity
            --   * A remote URI
            cap <- nameExceptT "Invite capability" $ parseActivityURI' uCap

            -- Verify the capability is local
            case cap of
                Left (actorByKey, _, outboxItemID) ->
                    return (actorByKey, outboxItemID)
                _ -> throwE "Capability is remote i.e. definitely not by me"

        -- If target is local, find it in our DB
        -- If target is remote, HTTP GET it, verify it's an actor, and store in
        -- our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the Invite handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result (approve/disapprove the Invite) would be sent later in a
        -- separate (e.g. Accept) activity. But for the PoC level, the current
        -- situation will hopefully do.
        invitedDB <-
            bitraverse
                (withDBExcept . flip getComponentE "Invitee not found in DB")
                getRemoteActorFromURI
                component

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            resourceID <- lift $ projectResource <$> getJust projectID
            Resource topicActorID <- lift $ getJust resourceID
            topicActor <- lift $ getJust topicActorID

            -- Verify the specified capability gives relevant access
            verifyCapability'
                capability authorIdMsig (LocalResourceProject projectID) AP.RoleAdmin

            -- Find existing Component records I have for this component
            -- Make sure none are enabled / in Add-Accept mode / in
            -- Invite-Accept mode
            checkExistingComponents projectID invitedDB

            maybeInviteDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
            lift $ for maybeInviteDB $ \ (inboxItemID, inviteDB) -> do

                -- Insert Collab or Component record to DB
                acceptID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
                insertComponent invitedDB inviteDB acceptID

                -- Prepare forwarding Invite to my followers
                sieve <- do
                    projectHash <- encodeKeyHashid projectID
                    return $ makeRecipientSet [] [LocalStageProjectFollowers projectHash]

                -- Prepare an Accept activity and insert to my outbox
                accept@(actionAccept, _, _, _) <- prepareAccept invitedDB
                _luAccept <- updateOutboxItem' (LocalActorProject projectID) acceptID actionAccept

                return (topicActorID, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (projectActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                forwardActivity
                    authorIdMsig body (LocalActorProject projectID) projectActorID sieve
                lift $ sendActivity
                    (LocalActorProject projectID) projectActorID localRecipsAccept
                    remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "[Add-component-active] Recorded and forwarded the Add, sent an Accept"

        where

        getRemoteActorFromURI (ObjURI h lu) = do
            instanceID <-
                lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
            result <-
                ExceptT $ first (T.pack . displayException) <$>
                    fetchRemoteActor' instanceID h lu
            case result of
                Left Nothing -> throwE "Target @id mismatch"
                Left (Just err) -> throwE $ T.pack $ displayException err
                Right Nothing -> throwE "Target isn't an actor"
                Right (Just actor) -> return $ entityKey actor

        insertComponent componentDB inviteDB acceptID = do
            componentID <- insert $ Component projectID AP.RoleAdmin
            originID <- insert $ ComponentOriginInvite componentID
            case inviteDB of
                Left (_, _, inviteID) ->
                    insert_ $ ComponentProjectGestureLocal componentID inviteID
                Right (author, _, inviteID) ->
                    insert_ $ ComponentProjectGestureRemote componentID (remoteAuthorId author) inviteID
            case componentDB of
                Left l ->
                    insert_ $ ComponentLocal componentID (localComponentID l)
                Right remoteActorID ->
                    insert_ $ ComponentRemote componentID remoteActorID
            insert_ $ ComponentProjectAccept originID acceptID

        prepareAccept invitedDB = do
            encodeRouteHome <- getEncodeRouteHome

            audInviter <- lift $ makeAudSenderOnly authorIdMsig
            audInvited <-
                case invitedDB of
                    Left componentByEnt -> do
                        componentByHash <- hashComponent $ bmap entityKey componentByEnt
                        let actor = resourceToActor $ componentResource componentByHash
                        return $ AudLocal [actor] [localActorFollowers actor]
                    Right remoteActorID -> do
                        ra <- getJust remoteActorID
                        ObjURI h lu <- getRemoteActorURI ra
                        return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
            audTopic <-
                AudLocal [] . pure . LocalStageProjectFollowers <$>
                    encodeKeyHashid projectID
            uInvite <- lift $ getActivityURI authorIdMsig

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audInviter, audInvited, audTopic]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uInvite]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uInvite
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    addComponentPassive component = do

        -- If component is local, find it in our DB
        -- If component is remote, HTTP GET it, verify it's an actor of a component
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        componentDB <-
            bitraverse
                (withDBExcept . flip getComponentE "Component not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Target @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Target isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeRepo -> pure ()
                                AP.ActorTypeTicketTracker -> pure ()
                                AP.ActorTypePatchTracker -> pure ()
                                _ -> throwE "Remote component type isn't repo/tt/pt"
                            return $ entityKey actor
                )
                component

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            -- Find existing Component records I have for this component
            -- Make sure none are enabled / in Add-Accept mode / in Invite-Accept
            -- mode
            checkExistingComponents projectID componentDB

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Component record in DB
                insertComponent componentDB addDB

                return (projectActor project, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (actorID, inboxItemID) -> do
                projectHash <- encodeKeyHashid projectID
                let sieve =
                        makeRecipientSet
                            []
                            [LocalStageProjectFollowers projectHash]
                forwardActivity
                    authorIdMsig body (LocalActorProject projectID) actorID sieve
                doneDB inboxItemID
                    "Recorded a Component record; Inserted the Add to inbox; \
                    \Forwarded to followers if addressed"

        where

        insertComponent componentDB addDB = do
            componentID <- insert $ Component projectID AP.RoleAdmin
            originID <- insert $ ComponentOriginAdd componentID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ ComponentGestureLocal originID addID
                Right (author, _, addID) ->
                    insert_ $ ComponentGestureRemote originID (remoteAuthorId author) addID
            case componentDB of
                Left l ->
                    insert_ $ ComponentLocal componentID (localComponentID l)
                Right remoteActorID ->
                    insert_ $ ComponentRemote componentID remoteActorID

    prepareAccept childDB = do
        encodeRouteHome <- getEncodeRouteHome

        audAdder <- makeAudSenderWithFollowers authorIdMsig
        audChild <-
            case childDB of
                Left (Entity j _) -> do
                    jh <- encodeKeyHashid j
                    return $ AudLocal [LocalActorProject jh] []
                Right (ObjURI h lu, Entity _ ra) ->
                    return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
        audMe <-
            AudLocal [] . pure . LocalStageProjectFollowers <$>
                encodeKeyHashid projectID
        uAdd <- lift $ getActivityURI authorIdMsig

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audAdder, audChild, audMe]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uAdd]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uAdd
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    addChildActive child = do

        -- If child is local, find it in our DB
        -- If child is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        childDB <-
            bitraverse
                (\case
                    LocalActorProject j -> withDBExcept $ getEntityE j "Child not found in DB"
                    _ -> throwE "Local proposed child of non-project type"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Child @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Child isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeProject -> pure ()
                                _ -> throwE "Remote child type isn't Project"
                            return (u, actor)
                )
                child
        let childDB' = second (entityKey . snd) childDB

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the sender is authorized by me to add a child
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceProject projectID)
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            -- Verify the object isn't a parent of mine
            verifyNoEnabledProjectParents projectID childDB'

            -- Verify the object isn't already a child of mine, and that no
            -- Source record is already in Add-Accept state
            verifyNoStartedProjectChildren projectID childDB'

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Source record in DB
                acceptID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                insertSource childDB' addDB acceptID

                -- Prepare forwarding the Add to my followers
                sieve <- do
                    projectHash <- encodeKeyHashid projectID
                    return $ makeRecipientSet [] [LocalStageProjectFollowers projectHash]

                -- Prepare an Accept activity and insert to my outbox
                accept@(actionAccept, _, _, _) <- prepareAccept childDB
                _luAccept <- updateOutboxItem' (LocalActorProject projectID) acceptID actionAccept

                return (projectActor project, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (projectActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                forwardActivity
                    authorIdMsig body (LocalActorProject projectID) projectActorID sieve
                lift $ sendActivity
                    (LocalActorProject projectID) projectActorID localRecipsAccept
                    remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "Recorded a child-project-in-progress, forwarded the Add, sent an Accept"

        where

        insertSource topicDB addDB acceptID = do
            sourceID <- insert $ Source AP.RoleAdmin
            holderID <- insert $ SourceHolderProject sourceID projectID
            case topicDB of
                Left (Entity j _) -> do
                    localID <- insert $ SourceTopicLocal sourceID
                    insert_ $ SourceTopicProject holderID localID j
                Right a ->
                    insert_ $ SourceTopicRemote sourceID a
            usID <- insert $ SourceOriginUs sourceID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ SourceUsGestureLocal usID addID
                Right (author, _, addID) ->
                    insert_ $ SourceUsGestureRemote usID (remoteAuthorId author) addID

            insert_ $ SourceUsAccept usID acceptID

    addParentActive parent = do

        -- If parent is local, find it in our DB
        -- If parent is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        parentDB <-
            bitraverse
                (\case
                    LocalActorProject j -> withDBExcept $ getEntityE j "Parent not found in DB"
                    _ -> throwE "Local proposed parent of non-project type"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Parent @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Parent isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeProject -> pure ()
                                _ -> throwE "Remote parent type isn't Project"
                            return (u, actor)
                )
                parent
        let parentDB' = second (entityKey . snd) parentDB

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the sender is authorized by me to add a parent
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceProject projectID)
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            -- Verify the object isn't a child of mine
            verifyNoEnabledProjectChildren projectID parentDB'

            -- Verify the object isn't already a parent of mine, and that no
            -- Dest record is already in Add-Accept state
            verifyNoStartedProjectParents projectID parentDB'

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Dest record in DB
                acceptID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                insertDest parentDB' addDB acceptID

                -- Prepare forwarding the Add to my followers
                sieve <- do
                    projectHash <- encodeKeyHashid projectID
                    return $ makeRecipientSet [] [LocalStageProjectFollowers projectHash]

                -- Prepare an Accept activity and insert to my outbox
                accept@(actionAccept, _, _, _) <- prepareAccept parentDB
                _luAccept <- updateOutboxItem' (LocalActorProject projectID) acceptID actionAccept

                return (projectActor project, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (projectActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                forwardActivity
                    authorIdMsig body (LocalActorProject projectID) projectActorID sieve
                lift $ sendActivity
                    (LocalActorProject projectID) projectActorID localRecipsAccept
                    remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "Recorded a parent-project-in-progress, forwarded the Add, sent an Accept"

        where

        insertDest topicDB addDB acceptID = do
            destID <- insert $ Dest AP.RoleAdmin
            holderID <- insert $ DestHolderProject destID projectID
            case topicDB of
                Left (Entity j _) -> do
                    localID <- insert $ DestTopicLocal destID
                    insert_ $ DestTopicProject holderID localID j
                Right a ->
                    insert_ $ DestTopicRemote destID a
            insert_ $ DestOriginUs destID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ DestUsGestureLocal destID addID
                Right (author, _, addID) ->
                    insert_ $ DestUsGestureRemote destID (remoteAuthorId author) addID

            insert_ $ DestUsAccept destID acceptID

    addTeamActive team = do

        -- If team is local, find it in our DB
        -- If team is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        teamDB <-
            bitraverse
                (\case
                    LocalActorGroup g -> withDBExcept $ getEntityE g "Team not found in DB"
                    _ -> throwE "Local proposed team of non-Group type"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Team @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Team isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote team type isn't Team"
                            return (u, actor)
                )
                team
        let teamDB' = second (entityKey . snd) teamDB

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the sender is authorized by me to add a team
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceProject projectID)
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            -- Verify the object isn't already a team of mine, and that no
            -- Squad record is already in Add-Accept state
            verifyNoStartedResourceTeams (projectResource project) teamDB'

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Squad record in DB
                acceptID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                insertSquad (projectResource project) teamDB' addDB acceptID

                -- Prepare forwarding the Add to my followers
                sieve <- do
                    projectHash <- encodeKeyHashid projectID
                    return $ makeRecipientSet [] [LocalStageProjectFollowers projectHash]

                -- Prepare an Accept activity and insert to my outbox
                accept@(actionAccept, _, _, _) <- prepareAccept teamDB
                _luAccept <- updateOutboxItem' (LocalActorProject projectID) acceptID actionAccept

                return (projectActor project, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (projectActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                forwardActivity
                    authorIdMsig body (LocalActorProject projectID) projectActorID sieve
                lift $ sendActivity
                    (LocalActorProject projectID) projectActorID localRecipsAccept
                    remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "[Team-active] Recorded a team-in-progress, forwarded the Add, sent an Accept"

        where

        insertSquad resourceID topicDB addDB acceptID = do
            squadID <- insert $ Squad AP.RoleAdmin resourceID
            case topicDB of
                Left (Entity g _) -> insert_ $ SquadTopicLocal squadID g
                Right a -> insert_ $ SquadTopicRemote squadID a
            insert_ $ SquadOriginUs squadID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ SquadUsGestureLocal squadID addID
                Right (author, _, addID) ->
                    insert_ $ SquadUsGestureRemote squadID (remoteAuthorId author) addID

            insert_ $ SquadUsAccept squadID acceptID

        prepareAccept teamDB = do
            encodeRouteHome <- getEncodeRouteHome

            audAdder <- makeAudSenderWithFollowers authorIdMsig
            audTeam <-
                case teamDB of
                    Left (Entity g _) -> do
                        gh <- encodeKeyHashid g
                        return $ AudLocal [LocalActorGroup gh] [LocalStageGroupFollowers gh]
                    Right (ObjURI h lu, Entity _ ra) ->
                        return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
            audMe <-
                AudLocal [] . pure . LocalStageProjectFollowers <$>
                    encodeKeyHashid projectID
            uAdd <- lift $ getActivityURI authorIdMsig

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audAdder, audTeam, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uAdd]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uAdd
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    addChildPassive child = do

        -- If child is local, find it in our DB
        -- If child is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        childDB <-
            bitraverse
                (\ j -> withDBExcept $ getEntityE j "Child not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Child @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Child isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeProject -> pure ()
                                _ -> throwE "Remote child type isn't Project"
                            return (u, actor)
                )
                child
        let childDB' = second (entityKey . snd) childDB

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            -- Verify the target isn't a parent of mine
            verifyNoEnabledProjectParents projectID childDB'

            -- Verify the target isn't already a child of mine, and that no
            -- Source record is already in Add-Accept state
            verifyNoStartedProjectChildren projectID childDB'

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Source record in DB
                insertSource childDB' addDB

                -- Prepare forwarding the Add to my followers
                sieve <- do
                    projectHash <- encodeKeyHashid projectID
                    return $ makeRecipientSet [] [LocalStageProjectFollowers projectHash]

                return (projectActor project, sieve, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (projectActorID, sieve, inboxItemID) -> do
                forwardActivity
                    authorIdMsig body (LocalActorProject projectID) projectActorID sieve
                doneDB inboxItemID "Recorded a child-project-in-progress, forwarded the Add"

        where

        insertSource topicDB addDB = do
            sourceID <- insert $ Source AP.RoleAdmin
            holderID <- insert $ SourceHolderProject sourceID projectID
            case topicDB of
                Left (Entity j _) -> do
                    localID <- insert $ SourceTopicLocal sourceID
                    insert_ $ SourceTopicProject holderID localID j
                Right a ->
                    insert_ $ SourceTopicRemote sourceID a
            themID <- insert $ SourceOriginThem sourceID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ SourceThemGestureLocal themID addID
                Right (author, _, addID) ->
                    insert_ $ SourceThemGestureRemote themID (remoteAuthorId author) addID

    addParentPassive parent = do

        -- If parent is local, find it in our DB
        -- If parent is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        parentDB <-
            bitraverse
                (\ j -> withDBExcept $ getEntityE j "Parent not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Parent @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Parent isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeProject -> pure ()
                                _ -> throwE "Remote parent type isn't Project"
                            return (u, actor)
                )
                parent
        let parentDB' = second (entityKey . snd) parentDB

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            -- Verify the object isn't a child of mine
            verifyNoEnabledProjectChildren projectID parentDB'

            -- Verify the object isn't already a parent of mine, and that no
            -- Dest record is already in Add-Accept state
            verifyNoStartedProjectParents projectID parentDB'

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Dest record in DB
                insertDest parentDB' addDB

                -- Prepare forwarding the Add to my followers
                sieve <- do
                    projectHash <- encodeKeyHashid projectID
                    return $ makeRecipientSet [] [LocalStageProjectFollowers projectHash]

                return (projectActor project, sieve, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (projectActorID, sieve, inboxItemID) -> do
                forwardActivity
                    authorIdMsig body (LocalActorProject projectID) projectActorID sieve
                doneDB inboxItemID "Recorded a parent-project-in-progress, forwarded the Add"

        where

        insertDest topicDB addDB = do
            destID <- insert $ Dest AP.RoleAdmin
            holderID <- insert $ DestHolderProject destID projectID
            case topicDB of
                Left (Entity j _) -> do
                    localID <- insert $ DestTopicLocal destID
                    insert_ $ DestTopicProject holderID localID j
                Right a ->
                    insert_ $ DestTopicRemote destID a
            themID <- insert $ DestOriginThem destID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ DestThemGestureLocal themID addID
                Right (author, _, addID) ->
                    insert_ $ DestThemGestureRemote themID (remoteAuthorId author) addID

    addTeamPassive team = do

        -- If team is local, find it in our DB
        -- If team is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        teamDB <-
            bitraverse
                (\ g -> withDBExcept $ getEntityE g "Team not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Team @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Team isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote team type isn't Team"
                            return (u, actor)
                )
                team
        let teamDB' = second (entityKey . snd) teamDB

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            -- Verify the object isn't already a team of mine, and that no
            -- Squad record is already in Add-Accept state
            verifyNoStartedResourceTeams (projectResource project) teamDB'

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Squad record in DB
                insertSquad (projectResource project) teamDB' addDB

                -- Prepare forwarding the Add to my followers
                sieve <- do
                    projectHash <- encodeKeyHashid projectID
                    return $ makeRecipientSet [] [LocalStageProjectFollowers projectHash]

                return (projectActor project, sieve, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (projectActorID, sieve, inboxItemID) -> do
                forwardActivity
                    authorIdMsig body (LocalActorProject projectID) projectActorID sieve
                doneDB inboxItemID "[Team-passive] Recorded a team-in-progress, forwarded the Add"

        where

        insertSquad resourceID topicDB addDB = do
            squadID <- insert $ Squad AP.RoleAdmin resourceID
            case topicDB of
                Left (Entity g _) -> insert_ $ SquadTopicLocal squadID g
                Right a -> insert_ $ SquadTopicRemote squadID a
            themID <- insert $ SquadOriginThem squadID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ SquadThemGestureLocal themID addID
                Right (author, _, addID) ->
                    insert_ $ SquadThemGestureRemote themID (remoteAuthorId author) addID

-- Meaning: An actor is following someone/something
-- Behavior:
--      * Verify the target is me
--      * Record the follow in DB
--      * Publish and send an Accept to the sender and its followers
projectFollow
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Follow URIMode
    -> ActE (Text, Act (), Next)
projectFollow now recipProjectID verse follow = do
    recipProjectHash <- encodeKeyHashid recipProjectID
    actorFollow
        (\case
            ProjectR d | d == recipProjectHash -> pure ()
            _ -> throwE "Asking to follow someone else"
        )
        projectActor
        False
        (\ recipProjectActor () -> pure $ actorFollowers recipProjectActor)
        (\ _ -> pure $ makeRecipientSet [] [])
        LocalActorProject
        (\ _ -> pure [])
        now recipProjectID verse follow

data GrantKind
    = GKDelegationStart AP.Role
    | GKDelegationExtend AP.Role (Either (LocalActorBy Key) FedURI)
    | GKDelegator

-- Meaning: An actor is granting access-to-some-resource to another actor
-- Behavior:
--      * Option 1 - Component sending me a delegation-start - Verify that:
--          * The sender is a component of mine, C
--          * The Grant's context is C
--          * The Grant's target is me
--          * The Grant's usage is gatherAndConvey
--          * The Grant doesn't specify 'delegates'
--          * The activity is authorized via a valid delegator-Grant I had sent
--            to C
--      * Verify the Grant's role is the same one specified in the Invite/Add
--        that added the Component
--      * Verify I don't yet have a delegation from C
--      * Insert the Grant to my inbox
--      * Record the delegation in the Component record in DB
--      * Forward the Grant to my followers
--      * For each person (non-team) collaborator of mine, prepare and send an
--        extension-Grant, and store it in the Component record in DB:
--          * Role: The lower among (1) admin (2) the collaborator's role in me
--          * Resource: C
--          * Target: The collaborator
--          * Delegates: The Grant I just got from C
--          * Result: ProjectCollabLiveR for this collaborator
--          * Usage: invoke
--      * For each parent of mine, prepare and send an extension-Grant:
--          * Role: The lower among (1) the role the component gave me (2) the role I gave the parent
--          * Resource: C
--          * Target: The parent
--          * Delegates: The Grant I just got from C
--          * Result: ProjectParentLiveR for this parent
--          * Usage: gatherAndConvey
--      * For each team collaborator of mine, prepare and send an
--        extension-Grant, and store it in the Component record in DB:
--          * Role: The lower among (1) admin (2) the team's role in me
--          * Resource: C
--          * Target: The team
--          * Delegates: The Grant I just got from C
--          * Result: ProjectTeamLiveR for this team
--          * Usage: distribute
--
--      * Option 2 - Collaborator sending me a delegator-Grant - Verify that:
--          * The sender is a collaborator of mine, A
--          * The Grant's context is A
--          * The Grant's target is me
--          * The Grant's usage is invoke & role is delegate
--          * The Grant doesn't specify 'delegates'
--          * The activity is authorized via a valid direct-Grant I had sent
--            to A
--      * Verify I don't yet have a delegator-Grant from A
--      * Insert the Grant to my inbox
--      * Record the delegator-Grant in the Collab record in DB
--      * Forward the Grant to my followers
--      * For each component of mine C, prepare and send an
--        extension-Grant to A, and store it in the Componet record in DB:
--          * Role: The lower among (1) admin (2) the collaborator's role in me
--          * Resource: C
--          * Target: A
--          * Delegates: The start-Grant I have from C
--          * Result: ProjectCollabLiveR for this collaborator, A
--          * Usage: invoke
--      * For each start-grant or extension-grant G that I received from a
--        child of mine J, prepare and send an extension-Grant to A, and store
--        it in the Source record in DB:
--          * Role: The lower among (1) the role in G (2) the collaborator's role in me
--          * Resource: The one specified in G
--          * Target: A
--          * Delegates: G
--          * Result: ProjectCollabLiveR for this collaborator, A
--          * Usage: invoke
--
--      * Option 3 - Child sending me a delegation-start or delegation-extension
--          * Verify they're authorized, i.e. they're using the delegator-Grant
--            I gave them
--          * Verify the role isn't delegator
--          * Store the Grant in the Source record in DB
--          * Send extension-Grants and record them in the DB:
--              * To each of my direct collaborators
--              * To each of my parents
--              * To each of my teams
--
--      * Option 4 - Almost-Parent sending me the delegator-Grant
--          * Update the Dest record, enabling the parent
--          * Send a start-Grant giving access-to-me
--          * For each of my components, send an extension-Grant to the new
--            parent
--          * For each grant I've been delegated from my children, send an
--            extension-Grant to the new parent
--
--      * Option 5 - Almost-Team sending me the delegator-Grant
--          * Update the Squad record, enabling the team
--          * Send a start-Grant giving access-to-me
--          * For each of my components, send an extension-Grant to the team
--          * For each grant I've been delegated from my children, send an
--            extension-Grant to the team
--
--      * If neither of those, raise an error
projectGrant
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Grant URIMode
    -> ActE (Text, Act (), Next)
projectGrant now projectID (Verse authorIdMsig body) grant = do

    grant' <- checkGrant grant
    let adapt = maybe (Right Nothing) (either Left (Right . Just))
    maybeMode <-
        withDBExcept $ do
            (_myInboxID, meResourceID) <- lift $ do
                project <- getJust projectID
                actor <- getJust $ projectActor project
                return (actorInbox actor, projectResource project)
            ExceptT $ fmap adapt $ runMaybeT $
                runExceptT (Left . Left <$> tryComp grant') <|>
                runExceptT (Left . Right <$> tryCollab grant') <|>
                runExceptT (Right . Left <$> tryChild grant') <|>
                runExceptT (Right . Right . Left <$> tryParent grant') <|>
                runExceptT (Right . Right . Right <$> tryTeam meResourceID grant')
    mode <-
        fromMaybeE
            maybeMode
            "Not a relevant Grant that I'm aware of"
    case mode of
        Left (Left (role, enableID, ident, identForCheck)) ->
            handleComp role enableID ident identForCheck
        Left (Right (enableID, role, recip)) ->
            handleCollab enableID role recip
        Right (Left (role, sendID, topic)) ->
            handleChild role sendID topic
        Right (Right (Left (role, topic, acceptID))) ->
            handleParent role topic acceptID
        Right (Right (Right (role, topic, acceptID))) ->
            handleTeam role topic acceptID

    where

    checkCapability = do
        -- Verify that a capability is provided
        uCap <- lift $ hoistMaybe $ AP.activityCapability $ actbActivity body

        -- Verify the capability URI is one of:
        --   * Outbox item URI of a local actor, i.e. a local activity
        --   * A remote URI
        cap <-
            ExceptT . lift . lift . runExceptT $
                nameExceptT "Grant capability" $ parseActivityURI' uCap

        -- Verify the capability is local
        case cap of
            Left (actorByKey, _, outboxItemID) ->
                return (actorByKey, outboxItemID)
            _ -> lift mzero

    checkGrant g = do
        (role, resource, recipient, _mresult, mstart, mend, usage, mdeleg) <-
            parseGrant' g
        case recipient of
            Left (LocalActorProject j) | j == projectID -> pure ()
            _ -> throwE "Target isn't me"
        for_ mstart $ \ start ->
            unless (start < now) $ throwE "Start time is in the future"
        for_ mend $ \ _ ->
            throwE "End time is specified"

        let resourceIsAuthor =
                case (resource, authorIdMsig) of
                    (Left a, Left (a', _, _)) -> a == a'
                    (Right u, Right (ra, _, _)) -> remoteAuthorURI ra == u
                    _ -> False

        case (role, resourceIsAuthor, usage, mdeleg) of
            (AP.RXRole r, True, AP.GatherAndConvey, Nothing) ->
                pure $ GKDelegationStart r
            (AP.RXRole r, False, AP.GatherAndConvey, Just _) ->
                pure $ GKDelegationExtend r resource
            (AP.RXDelegator, True, AP.Invoke, Nothing) ->
                pure GKDelegator
            _ -> throwE "A kind of Grant that I don't use"

    tryComp (GKDelegationExtend _ _) = lift mzero
    tryComp GKDelegator              = lift mzero
    tryComp (GKDelegationStart role) = do
        capability <- checkCapability
        -- Find the Component record from the capability
        Entity enableID (ComponentEnable componentID _) <- lift $ do
            -- Capability isn't mine
            guard $ fst capability == LocalActorProject projectID
            -- I don't have a Component with this capability
            MaybeT $ getBy $ UniqueComponentEnableGrant $ snd capability
        Component j role' <- lift $ lift $ getJust componentID
        -- Found a Component for this delegator-Grant but it's not mine
        lift $ guard $ j == projectID
        unless (role' == role) $
            throwE "Grant role isn't the same as in the Invite/Add"
        ident <- lift $ lift $ getComponentIdent componentID
        identForCheck <-
            lift $ lift $
            bitraverse
                (pure . snd)
                (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                ident
        unless (first (resourceToActor . componentResource) identForCheck == bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig) $
            throwE "Capability's component and Grant author aren't the same actor"
        return (role, enableID, ident, identForCheck)

    handleComp role enableID ident identForCheck = do

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            resourceID <- lift $ projectResource <$> getJust projectID
            Resource recipActorID <- lift $ getJust resourceID
            recipActor <- lift $ getJust recipActorID

            -- Verify I don't yet have a delegation from the component
            maybeDeleg <-
                lift $ case bimap fst fst ident of
                    Left localID -> (() <$) <$> getBy (UniqueComponentDelegateLocal localID)
                    Right remoteID -> (() <$) <$> getBy (UniqueComponentDelegateRemote remoteID)
            verifyNothingE maybeDeleg "I already have a delegation-start Grant from this component"

            maybeGrantDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeGrantDB $ \ (inboxItemID, grantDB) -> do

                -- Record the delegation in DB
                lift $ case (grantDB, bimap fst fst ident) of
                    (Left (_, _, grantID), Left localID) -> insert_ $ ComponentDelegateLocal localID grantID
                    (Right (_, _, grantID), Right remoteID) -> insert_ $ ComponentDelegateRemote remoteID grantID
                    _ -> error "projectGrant impossible"

                -- Prepare forwarding of Accept to my followers
                projectHash <- encodeKeyHashid projectID
                let sieve = makeRecipientSet [] [LocalStageProjectFollowers projectHash]

                -- For each Collab in me, prepare a delegation-extension Grant
                localCollabs <-
                    lift $
                    E.select $ E.from $ \ (collab `E.InnerJoin` enable `E.InnerJoin` recipL `E.InnerJoin` deleg) -> do
                        E.on $ enable E.^. CollabEnableId E.==. deleg E.^. CollabDelegLocalEnable
                        E.on $ enable E.^. CollabEnableCollab E.==. recipL E.^. CollabRecipLocalCollab
                        E.on $ collab E.^. CollabId E.==. enable E.^. CollabEnableCollab
                        E.where_ $ collab E.^. CollabTopic E.==. E.val resourceID
                        return
                            ( collab E.^. CollabRole
                            , recipL E.^. CollabRecipLocalPerson
                            , deleg
                            )
                localExtensions <- lift $ for localCollabs $ \ (E.Value role', E.Value personID, Entity delegID (CollabDelegLocal enableID' recipID grantID)) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    insert_ $ ComponentFurtherLocal enableID delegID extID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrant identForCheck (Left (personID, grantID)) (min role role') enableID'
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                remoteCollabs <-
                    lift $
                    E.select $ E.from $ \ (collab `E.InnerJoin` enable `E.InnerJoin` recipR `E.InnerJoin` deleg) -> do
                        E.on $ enable E.^. CollabEnableId E.==. deleg E.^. CollabDelegRemoteEnable
                        E.on $ enable E.^. CollabEnableCollab E.==. recipR E.^. CollabRecipRemoteCollab
                        E.on $ collab E.^. CollabId E.==. enable E.^. CollabEnableCollab
                        E.where_ $ collab E.^. CollabTopic E.==. E.val resourceID
                        return
                            ( collab E.^. CollabRole
                            , recipR E.^. CollabRecipRemoteActor
                            , deleg
                            )
                remoteExtensions <- lift $ for remoteCollabs $ \ (E.Value role', E.Value raID, Entity delegID (CollabDelegRemote enableID' recipID grantID)) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    insert_ $ ComponentFurtherRemote enableID delegID extID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrant identForCheck (Right (raID, grantID)) (min role role') enableID'
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                -- For each parent of mine, prepare a delegation-extension Grant
                localParents <-
                    lift $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. DestUsAcceptId E.==. start E.^. DestUsStartDest
                        E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
                        E.on $ topic E.^. DestTopicProjectTopic E.==. deleg E.^. DestThemSendDelegatorLocalTopic
                        E.on $ holder E.^. DestHolderProjectId E.==. topic E.^. DestTopicProjectHolder
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderProjectDest
                        E.where_ $ holder E.^. DestHolderProjectProject E.==. E.val projectID
                        return
                            ( dest E.^. DestRole
                            , topic E.^. DestTopicProjectParent
                            , deleg E.^. DestThemSendDelegatorLocalId
                            , deleg E.^. DestThemSendDelegatorLocalGrant
                            , start E.^. DestUsStartId
                            )
                localExtensionsForParents <- lift $ for localParents $ \ (E.Value role', E.Value parentID, E.Value _delegID, E.Value grantID, E.Value startID) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    insert_ $ ComponentGather enableID startID extID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForParent identForCheck (Left (parentID, grantID)) (min role role') startID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                remoteParents <-
                    lift $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. DestUsAcceptId E.==. start E.^. DestUsStartDest
                        E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
                        E.on $ topic E.^. DestTopicRemoteId E.==. deleg E.^. DestThemSendDelegatorRemoteTopic
                        E.on $ dest E.^. DestId E.==. topic E.^. DestTopicRemoteDest
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderProjectDest
                        E.where_ $ holder E.^. DestHolderProjectProject E.==. E.val projectID
                        return
                            ( dest E.^. DestRole
                            , topic E.^. DestTopicRemoteTopic
                            , deleg E.^. DestThemSendDelegatorRemoteId
                            , deleg E.^. DestThemSendDelegatorRemoteGrant
                            , start E.^. DestUsStartId
                            )
                remoteExtensionsForParents <- lift $ for remoteParents $ \ (E.Value role', E.Value parentID, E.Value _delegID, E.Value grantID, E.Value startID) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    insert_ $ ComponentGather enableID startID extID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForParent identForCheck (Right (parentID, grantID)) (min role role') startID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                -- For each team of mine, prepare a delegation-extension Grant
                localTeams <-
                    lift $
                    E.select $ E.from $ \ (squad `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. SquadUsAcceptId E.==. start E.^. SquadUsStartSquad
                        E.on $ squad E.^. SquadId E.==. accept E.^. SquadUsAcceptSquad
                        E.on $ topic E.^. SquadTopicLocalId E.==. deleg E.^. SquadThemSendDelegatorLocalTopic
                        E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicLocalSquad
                        E.where_ $ squad E.^. SquadHolder E.==. E.val resourceID
                        return
                            ( squad E.^. SquadRole
                            , topic E.^. SquadTopicLocalGroup
                            , deleg E.^. SquadThemSendDelegatorLocalId
                            , deleg E.^. SquadThemSendDelegatorLocalGrant
                            , start E.^. SquadUsStartId
                            )
                localExtensionsForTeams <- lift $ for localTeams $ \ (E.Value role', E.Value groupID, E.Value _delegID, E.Value grantID, E.Value startID) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    insert_ $ ComponentConvey enableID startID extID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForTeam identForCheck (Left (groupID, grantID)) (min role role') startID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                remoteTeams <-
                    lift $
                    E.select $ E.from $ \ (squad `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. SquadUsAcceptId E.==. start E.^. SquadUsStartSquad
                        E.on $ squad E.^. SquadId E.==. accept E.^. SquadUsAcceptSquad
                        E.on $ topic E.^. SquadTopicRemoteId E.==. deleg E.^. SquadThemSendDelegatorRemoteTopic
                        E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicRemoteSquad
                        E.where_ $ squad E.^. SquadHolder E.==. E.val resourceID
                        return
                            ( squad E.^. SquadRole
                            , topic E.^. SquadTopicRemoteTopic
                            , deleg E.^. SquadThemSendDelegatorRemoteId
                            , deleg E.^. SquadThemSendDelegatorRemoteGrant
                            , start E.^. SquadUsStartId
                            )
                remoteExtensionsForTeams <- lift $ for remoteTeams $ \ (E.Value role', E.Value teamID, E.Value _delegID, E.Value grantID, E.Value startID) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    insert_ $ ComponentConvey enableID startID extID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForTeam identForCheck (Right (teamID, grantID)) (min role role') startID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return
                    ( recipActorID
                    , sieve
                    , localExtensions ++ localExtensionsForParents ++ localExtensionsForTeams
                    , remoteExtensions ++ remoteExtensionsForParents ++ remoteExtensionsForTeams
                    , inboxItemID
                    )

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, localExts, remoteExts, inboxItemID) -> do
                let recipByID = LocalActorProject projectID
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ for_ (localExts ++ remoteExts) $
                    \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            recipByID recipActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "Forwarded the start-Grant and published delegation extensions"

        where

        prepareExtensionGrant component collab role enableID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            uStart <- lift $ getActivityURI authorIdMsig

            (uCollab, audCollab, uDeleg) <-
                case collab of
                    Left (personID, itemID) -> do
                        personHash <- encodeKeyHashid personID
                        itemHash <- encodeKeyHashid itemID
                        return
                            ( encodeRouteHome $ PersonR personHash
                            , AudLocal [LocalActorPerson personHash] []
                            , encodeRouteHome $
                                PersonOutboxItemR personHash itemHash
                            )
                    Right (raID, ractID) -> do
                        ra <- getJust raID
                        u@(ObjURI h lu) <- getRemoteActorURI ra
                        uAct <- do
                            ract <- getJust ractID
                            getRemoteActivityURI ract
                        return (u, AudRemote h [lu] [], uAct)

            uComponent <-
                case component of
                    Left c -> do
                        a <- resourceToActor . componentResource <$> hashComponent c
                        return $ encodeRouteHome $ renderLocalActor a
                    Right u -> pure u

            enableHash <- encodeKeyHashid enableID

            let audience = [audCollab]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience audience

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = uComponent
                        , AP.grantTarget    = uCollab
                        , AP.grantResult    =
                            Just
                                (encodeRouteLocal $
                                    ProjectCollabLiveR projectHash enableHash
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Invoke
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtensionGrantForParent component parent role startID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            uStart <- lift $ getActivityURI authorIdMsig

            (uParent, audParent, uDeleg) <-
                case parent of
                    Left (j, itemID) -> do
                        h <- encodeKeyHashid j
                        itemHash <- encodeKeyHashid itemID
                        return
                            ( encodeRouteHome $ ProjectR h
                            , AudLocal [LocalActorProject h] []
                            , encodeRouteHome $
                                ProjectOutboxItemR h itemHash
                            )
                    Right (raID, ractID) -> do
                        ra <- getJust raID
                        u@(ObjURI h lu) <- getRemoteActorURI ra
                        uAct <- do
                            ract <- getJust ractID
                            getRemoteActivityURI ract
                        return (u, AudRemote h [lu] [], uAct)

            uComponent <-
                case component of
                    Left c -> do
                        a <- resourceToActor . componentResource <$> hashComponent c
                        return $ encodeRouteHome $ renderLocalActor a
                    Right u -> pure u

            resultR <- do
                startHash <- encodeKeyHashid startID
                return $ ProjectParentLiveR projectHash startHash

            let audience = [audParent]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience audience

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = uComponent
                        , AP.grantTarget    = uParent
                        , AP.grantResult    =
                            Just (encodeRouteLocal resultR, Nothing)
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.GatherAndConvey
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtensionGrantForTeam component parent role startID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            uStart <- lift $ getActivityURI authorIdMsig

            (uParent, audParent, uDeleg) <-
                case parent of
                    Left (j, itemID) -> do
                        h <- encodeKeyHashid j
                        itemHash <- encodeKeyHashid itemID
                        return
                            ( encodeRouteHome $ GroupR h
                            , AudLocal [LocalActorGroup h] []
                            , encodeRouteHome $ GroupOutboxItemR h itemHash
                            )
                    Right (raID, ractID) -> do
                        ra <- getJust raID
                        u@(ObjURI h lu) <- getRemoteActorURI ra
                        uAct <- do
                            ract <- getJust ractID
                            getRemoteActivityURI ract
                        return (u, AudRemote h [lu] [], uAct)

            uComponent <-
                case component of
                    Left c -> do
                        a <- resourceToActor . componentResource <$> hashComponent c
                        return $ encodeRouteHome $ renderLocalActor a
                    Right u -> pure u

            resultR <- do
                startHash <- encodeKeyHashid startID
                return $ ProjectTeamLiveR projectHash startHash

            let audience = [audParent]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience audience

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = uComponent
                        , AP.grantTarget    = uParent
                        , AP.grantResult    =
                            Just (encodeRouteLocal resultR, Nothing)
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Distribute
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    tryCollab (GKDelegationStart _)    = lift mzero
    tryCollab (GKDelegationExtend _ _) = lift mzero
    tryCollab GKDelegator              = do
        capability <- checkCapability
        -- Find the Collab record from the capability
        Entity enableID (CollabEnable collabID _) <- lift $ do
            -- Capability isn't mine
            guard $ fst capability == LocalActorProject projectID
            -- I don't have a Collab with this capability
            MaybeT $ getBy $ UniqueCollabEnableGrant $ snd capability
        Collab role _ <- lift $ lift $ getJust collabID
        topic <- lift $ lift $ getCollabTopic collabID
        -- Found a Collab for this direct-Grant but it's not mine
        lift $ guard $ topic == LocalResourceProject projectID
        recip <- lift $ lift $ getCollabRecip collabID
        recipForCheck <-
            lift $ lift $
            bitraverse
                (pure . collabRecipLocalPerson . entityVal)
                (getRemoteActorURI <=< getJust . collabRecipRemoteActor . entityVal)
                recip
        unless (first LocalActorPerson recipForCheck == bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig) $
            throwE "Capability's collaborator and Grant author aren't the same actor"
        return (enableID, role, recip)

    handleCollab enableID role recip = do

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust projectID
                let actorID = projectActor recip
                (actorID,) <$> getJust actorID

            -- Verify I don't yet have a delegator-Grant from the collaborator
            maybeDeleg <-
                lift $ case bimap entityKey entityKey recip of
                    Left localID -> (() <$) <$> getBy (UniqueCollabDelegLocalRecip localID)
                    Right remoteID -> (() <$) <$> getBy (UniqueCollabDelegRemoteRecip remoteID)
            verifyNothingE maybeDeleg "I already have a delegator-Grant from this collaborator"

            maybeGrantDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeGrantDB $ \ (inboxItemID, grantDB) -> do

                -- Record the delegator-Grant in the Collab record
                (insertExt, insertLeaf, uDeleg) <-
                    lift $ case (grantDB, bimap entityKey entityKey recip) of
                        (Left (grantActor, _, grantID), Left localID) -> do
                            delegID <- insert $ CollabDelegLocal enableID localID grantID
                            encodeRouteHome <- getEncodeRouteHome
                            delegR <-
                                activityRoute
                                    <$> hashLocalActor grantActor
                                    <*> encodeKeyHashid grantID
                            return
                                (\ enableID furtherID ->
                                    insert_ $ ComponentFurtherLocal enableID delegID furtherID
                                , \ leafID ->
                                    insert_ $ SourceUsLeafToLocal leafID delegID
                                , encodeRouteHome delegR
                                )
                        (Right (_, _, grantID), Right remoteID) -> do
                            delegID <- insert $ CollabDelegRemote enableID remoteID grantID
                            u <- getRemoteActivityURI =<< getJust grantID
                            return
                                (\ enableID furtherID ->
                                    insert_ $ ComponentFurtherRemote enableID delegID furtherID
                                , \ leafID ->
                                    insert_ $ SourceUsLeafToRemote leafID delegID
                                , u
                                )
                        _ -> error "projectGrant impossible 2"

                -- Prepare forwarding of Accept to my followers
                projectHash <- encodeKeyHashid projectID
                let sieve = makeRecipientSet [] [LocalStageProjectFollowers projectHash]

                extensions <- lift $ do
                    -- For each Component of mine, prepare a delegation-extension
                    -- Grant
                    locals <-
                        fmap (map $ over _1 Left) $
                        E.select $ E.from $ \ (deleg `E.InnerJoin` local `E.InnerJoin` comp `E.InnerJoin` enable) -> do
                            E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                            E.on $ local E.^. ComponentLocalComponent E.==. comp E.^. ComponentId
                            E.on $ deleg E.^. ComponentDelegateLocalComponent E.==.local E.^. ComponentLocalId
                            E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
                            return (deleg E.^. ComponentDelegateLocalGrant, comp, enable)
                    remotes <-
                        fmap (map $ over _1 Right) $
                        E.select $ E.from $ \ (deleg `E.InnerJoin` remote `E.InnerJoin` comp `E.InnerJoin` enable) -> do
                            E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                            E.on $ remote E.^. ComponentRemoteComponent E.==. comp E.^. ComponentId
                            E.on $ deleg E.^. ComponentDelegateRemoteComponent E.==.remote E.^. ComponentRemoteId
                            E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
                            return (deleg E.^. ComponentDelegateRemoteGrant, comp, enable)
                    (uCollab, audCollab) <-
                        case recip of
                            Left (Entity _ (CollabRecipLocal _ personID)) -> do
                                personHash <- encodeKeyHashid personID
                                encodeRouteHome <- getEncodeRouteHome
                                return
                                    ( encodeRouteHome $ PersonR personHash
                                    , AudLocal [LocalActorPerson personHash] []
                                    )
                            Right (Entity _ (CollabRecipRemote _ raID)) -> do
                                ra <- getJust raID
                                u@(ObjURI h lu) <- getRemoteActorURI ra
                                return (u, AudRemote h [lu] [])
                    fromComponents <- for (locals ++ remotes) $ \ (start, Entity componentID component, Entity enableID' _) -> do
                        extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                        insertExt enableID' extID
                        componentIdent <- do
                            i <- getComponentIdent componentID
                            bitraverse
                                (pure . snd)
                                (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                                i
                        uStart <-
                            case start of
                                Left (E.Value startID) -> do
                                    encodeRouteHome <- getEncodeRouteHome
                                    c <-
                                        case componentIdent of
                                            Left ci -> hashComponent ci
                                            Right _ -> error "Delegation-start Grant URI is local, but component found to be remote, impossible"
                                    s <- encodeKeyHashid startID
                                    return $ encodeRouteHome $ activityRoute (resourceToActor $ componentResource c) s
                                Right (E.Value remoteActivityID) -> do
                                    ra <- getJust remoteActivityID
                                    getRemoteActivityURI ra
                        ext@(actionExt, _, _, _) <-
                            prepareExtensionGrant uCollab audCollab uDeleg componentIdent uStart (min role (componentRole component)) enableID
                        let recipByKey = LocalActorProject projectID
                        _luExt <- updateOutboxItem' recipByKey extID actionExt
                        return (extID, ext)

                    -- For each Grant I got from a child, prepare a
                    -- delegation-extension Grant
                    l <-
                        fmap (map $ over _2 Left) $
                        E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send `E.InnerJoin` deleg) -> do
                            E.on $ accept E.^. SourceThemAcceptLocalId E.==. deleg E.^. SourceThemDelegateLocalSource
                            E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                            E.on $ topic E.^. SourceTopicLocalId E.==. accept E.^. SourceThemAcceptLocalTopic
                            E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicLocalSource
                            E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
                            E.where_ $ holder E.^. SourceHolderProjectProject E.==. E.val projectID
                            return
                                ( send E.^. SourceUsSendDelegatorId
                                , deleg
                                )
                    r <-
                        fmap (map $ over _2 Right) $
                        E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send `E.InnerJoin` deleg) -> do
                            E.on $ accept E.^. SourceThemAcceptRemoteId E.==. deleg E.^. SourceThemDelegateRemoteSource
                            E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                            E.on $ topic E.^. SourceTopicRemoteId E.==. accept E.^. SourceThemAcceptRemoteTopic
                            E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicRemoteSource
                            E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
                            E.where_ $ holder E.^. SourceHolderProjectProject E.==. E.val projectID
                            return
                                ( send E.^. SourceUsSendDelegatorId
                                , deleg
                                )
                    fromChildren <- for (l ++ r) $ \ (E.Value sendID, deleg) -> do
                        extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                        leafID <- insert $ SourceUsLeaf sendID enableID extID
                        case bimap entityKey entityKey deleg of
                            Left fromID -> insert_ $ SourceUsLeafFromLocal leafID fromID
                            Right fromID -> insert_ $ SourceUsLeafFromRemote leafID fromID
                        insertLeaf leafID
                        (AP.Doc h a, grant) <- getGrantActivityBody $ bimap (sourceThemDelegateLocalGrant . entityVal) (sourceThemDelegateRemoteGrant . entityVal) deleg
                        uStart <-
                            case AP.activityId a of
                                Nothing -> error "SourceThemDelegate grant has no 'id'"
                                Just lu -> pure $ ObjURI h lu
                        ext@(actionExt, _, _, _) <-
                            prepareExtensionGrantFromChild uCollab audCollab uDeleg uStart grant role enableID
                        let recipByKey = LocalActorProject projectID
                        _luExt <- updateOutboxItem' recipByKey extID actionExt
                        return (extID, ext)

                    return $ fromComponents ++ fromChildren

                return (recipActorID, sieve, extensions, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, extensions, inboxItemID) -> do
                let recipByID = LocalActorProject projectID
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ for_ extensions $
                    \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            recipByID recipActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "Forwarded the delegator-Grant, updated DB and published delegation extensions"

        where

        prepareExtensionGrant uCollab audCollab uDeleg component uStart role enableID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID

            uComponent <-
                case component of
                    Left c -> do
                        a <- resourceToActor . componentResource <$> hashComponent c
                        return $ encodeRouteHome $ renderLocalActor a
                    Right u -> pure u

            enableHash <- encodeKeyHashid enableID

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audCollab]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = uComponent
                        , AP.grantTarget    = uCollab
                        , AP.grantResult    =
                            Just
                                (encodeRouteLocal $
                                    ProjectCollabLiveR projectHash enableHash
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Invoke
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtensionGrantFromChild uCollab audCollab uDeleg uStart grant role enableID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            enableHash <- encodeKeyHashid enableID
            finalRole <-
                case AP.grantObject grant of
                    AP.RXRole r -> pure $ min role r
                    AP.RXDelegator -> error "Why was I delegated a Grant with object=delegator?"

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audCollab]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole finalRole
                        , AP.grantContext   = AP.grantContext grant
                        , AP.grantTarget    = uCollab
                        , AP.grantResult    =
                            Just
                                (encodeRouteLocal $
                                    ProjectCollabLiveR projectHash enableHash
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Invoke
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    tryChild gk = do
        capability <- checkCapability
        role <-
            case gk of
                GKDelegationStart role    -> pure role
                GKDelegationExtend role _ -> pure role
                GKDelegator               -> lift mzero
        -- Find the Source record from the capability
        Entity sendID (SourceUsSendDelegator sourceID _) <- lift $ do
            -- Capability isn't mine
            guard $ fst capability == LocalActorProject projectID
            -- I don't have a Source with this capability
            MaybeT $ getBy $ UniqueSourceUsSendDelegatorGrant $ snd capability
        Source role' <- lift $ lift $ getJust sourceID
        SourceHolderProject _ j <-
            lift $ MaybeT $ getValBy $ UniqueSourceHolderProject sourceID
        -- Found a Source for this Grant but it's not mine
        lift $ guard $ j == projectID
        topic <- do
            t <- lift $ lift $ getSourceTopic sourceID
            bitraverse
                (bitraverse
                    pure
                    (\case
                        Left j -> pure j
                        Right _g -> error "I have a SourceTopic that is a Group"
                    )
                )
                pure
                t
        topicForCheck <-
            lift $ lift $
            bitraverse
                (pure . snd)
                (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                topic
        unless (first LocalActorProject topicForCheck == bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig) $
            throwE "Capability's source and Grant author aren't the same actor"
        return (min role role', sendID, topic)

    handleChild role sendID topic = do

        uCap <- lift $ getActivityURI authorIdMsig
        checkCapabilityBeforeExtending uCap (LocalActorProject projectID)

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            resourceID <- lift $ projectResource <$> getJust projectID
            Resource recipActorID <- lift $ getJust resourceID
            recipActor <- lift $ getJust recipActorID

            topicWithAccept <-
                lift $
                bitraverse
                    (\ (localID, jID) ->
                        (localID, jID,) <$>
                            getKeyByJust (UniqueSourceThemAcceptLocal localID)
                    )
                    (\ (remoteID, aID) ->
                        (remoteID, aID,) <$>
                            getKeyByJust (UniqueSourceThemAcceptRemote remoteID)
                    )
                    topic

            maybeGrantDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeGrantDB $ \ (inboxItemID, grantDB) -> do

                -- Record the delegation in DB
                from <- case (grantDB, bimap (view _3) (view _3) topicWithAccept) of
                    (Left (_, _, grantID), Left localID) -> Left <$> do
                        mk <- lift $ insertUnique $ SourceThemDelegateLocal localID grantID
                        fromMaybeE mk "I already have such a SourceThemDelegateLocal"
                    (Right (_, _, grantID), Right remoteID) -> Right <$> do
                        mk <- lift $ insertUnique $ SourceThemDelegateRemote remoteID grantID
                        fromMaybeE mk "I already have such a SourceThemDelegateRemote"
                    _ -> error "projectGrant.child impossible"

                -- For each Collab in me, prepare a delegation-extension Grant
                localCollabs <-
                    lift $
                    E.select $ E.from $ \ (collab `E.InnerJoin` enable `E.InnerJoin` recipL `E.InnerJoin` deleg) -> do
                        E.on $ enable E.^. CollabEnableId E.==. deleg E.^. CollabDelegLocalEnable
                        E.on $ enable E.^. CollabEnableCollab E.==. recipL E.^. CollabRecipLocalCollab
                        E.on $ collab E.^. CollabId E.==. enable E.^. CollabEnableCollab
                        E.where_ $ collab E.^. CollabTopic E.==. E.val resourceID
                        return
                            ( collab E.^. CollabRole
                            , recipL E.^. CollabRecipLocalPerson
                            , deleg
                            )
                localExtensions <- lift $ for localCollabs $ \ (E.Value role', E.Value personID, Entity delegID (CollabDelegLocal enableID _recipID grantID)) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    leafID <- insert $ SourceUsLeaf sendID enableID extID
                    case from of
                        Left localID -> insert_ $ SourceUsLeafFromLocal leafID localID
                        Right remoteID -> insert_ $ SourceUsLeafFromRemote leafID remoteID
                    insert_ $ SourceUsLeafToLocal leafID delegID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrant (Left (personID, grantID)) (min role role') enableID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                remoteCollabs <-
                    lift $
                    E.select $ E.from $ \ (collab `E.InnerJoin` enable `E.InnerJoin` recipR `E.InnerJoin` deleg) -> do
                        E.on $ enable E.^. CollabEnableId E.==. deleg E.^. CollabDelegRemoteEnable
                        E.on $ enable E.^. CollabEnableCollab E.==. recipR E.^. CollabRecipRemoteCollab
                        E.on $ collab E.^. CollabId E.==. enable E.^. CollabEnableCollab
                        E.where_ $ collab E.^. CollabTopic E.==. E.val resourceID
                        return
                            ( collab E.^. CollabRole
                            , recipR E.^. CollabRecipRemoteActor
                            , deleg
                            )
                remoteExtensions <- lift $ for remoteCollabs $ \ (E.Value role', E.Value raID, Entity delegID (CollabDelegRemote enableID _recipID grantID)) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    leafID <- insert $ SourceUsLeaf sendID enableID extID
                    case from of
                        Left localID -> insert_ $ SourceUsLeafFromLocal leafID localID
                        Right remoteID -> insert_ $ SourceUsLeafFromRemote leafID remoteID
                    insert_ $ SourceUsLeafToRemote leafID delegID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrant (Right (raID, grantID)) (min role role') enableID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                -- For each parent of mine, prepare a delegation-extension Grant
                localParents <-
                    lift $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. DestUsAcceptId E.==. start E.^. DestUsStartDest
                        E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
                        E.on $ topic E.^. DestTopicProjectTopic E.==. deleg E.^. DestThemSendDelegatorLocalTopic
                        E.on $ holder E.^. DestHolderProjectId E.==. topic E.^. DestTopicProjectHolder
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderProjectDest
                        E.where_ $ holder E.^. DestHolderProjectProject E.==. E.val projectID
                        return
                            ( dest E.^. DestRole
                            , topic E.^. DestTopicProjectParent
                            , deleg E.^. DestThemSendDelegatorLocalId
                            , deleg E.^. DestThemSendDelegatorLocalGrant
                            , accept E.^. DestUsAcceptId
                            , start E.^. DestUsStartId
                            )
                localExtensionsForParents <- lift $ for localParents $ \ (E.Value role', E.Value parentID, E.Value _delegID, E.Value grantID, E.Value _acceptID, E.Value startID) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    gatherID <- insert $ SourceUsGather sendID startID extID
                    case from of
                        Left localID -> insert_ $ SourceUsGatherFromLocal gatherID localID
                        Right remoteID -> insert_ $ SourceUsGatherFromRemote gatherID remoteID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForParent (Left (parentID, grantID)) (min role role') startID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                remoteParents <-
                    lift $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. DestUsAcceptId E.==. start E.^. DestUsStartDest
                        E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
                        E.on $ topic E.^. DestTopicRemoteId E.==. deleg E.^. DestThemSendDelegatorRemoteTopic
                        E.on $ dest E.^. DestId E.==. topic E.^. DestTopicRemoteDest
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderProjectDest
                        E.where_ $ holder E.^. DestHolderProjectProject E.==. E.val projectID
                        return
                            ( dest E.^. DestRole
                            , topic E.^. DestTopicRemoteTopic
                            , deleg E.^. DestThemSendDelegatorRemoteId
                            , deleg E.^. DestThemSendDelegatorRemoteGrant
                            , accept E.^. DestUsAcceptId
                            , start E.^. DestUsStartId
                            )
                remoteExtensionsForParents <- lift $ for remoteParents $ \ (E.Value role', E.Value parentID, E.Value _delegID, E.Value grantID, E.Value _acceptID, E.Value startID) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    gatherID <- insert $ SourceUsGather sendID startID extID
                    case from of
                        Left localID -> insert_ $ SourceUsGatherFromLocal gatherID localID
                        Right remoteID -> insert_ $ SourceUsGatherFromRemote gatherID remoteID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForParent (Right (parentID, grantID)) (min role role') startID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                -- For each team of mine, prepare a delegation-extension Grant
                localTeams <-
                    lift $
                    E.select $ E.from $ \ (squad `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. SquadUsAcceptId E.==. start E.^. SquadUsStartSquad
                        E.on $ squad E.^. SquadId E.==. accept E.^. SquadUsAcceptSquad
                        E.on $ topic E.^. SquadTopicLocalId E.==. deleg E.^. SquadThemSendDelegatorLocalTopic
                        E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicLocalSquad
                        E.where_ $ squad E.^. SquadHolder E.==. E.val resourceID
                        return
                            ( squad E.^. SquadRole
                            , topic E.^. SquadTopicLocalGroup
                            , deleg E.^. SquadThemSendDelegatorLocalId
                            , deleg E.^. SquadThemSendDelegatorLocalGrant
                            , accept E.^. SquadUsAcceptId
                            , start E.^. SquadUsStartId
                            )
                localExtensionsForTeams <- lift $ for localTeams $ \ (E.Value role', E.Value groupID, E.Value _delegID, E.Value grantID, E.Value _acceptID, E.Value startID) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    conveyID <- insert $ SourceUsConvey sendID startID extID
                    case from of
                        Left localID -> insert_ $ SourceUsConveyFromLocal conveyID localID
                        Right remoteID -> insert_ $ SourceUsConveyFromRemote conveyID remoteID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForTeam (Left (groupID, grantID)) (min role role') startID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                remoteTeams <-
                    lift $
                    E.select $ E.from $ \ (squad `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. SquadUsAcceptId E.==. start E.^. SquadUsStartSquad
                        E.on $ squad E.^. SquadId E.==. accept E.^. SquadUsAcceptSquad
                        E.on $ topic E.^. SquadTopicRemoteId E.==. deleg E.^. SquadThemSendDelegatorRemoteTopic
                        E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicRemoteSquad
                        E.where_ $ squad E.^. SquadHolder E.==. E.val resourceID
                        return
                            ( squad E.^. SquadRole
                            , topic E.^. SquadTopicRemoteTopic
                            , deleg E.^. SquadThemSendDelegatorRemoteId
                            , deleg E.^. SquadThemSendDelegatorRemoteGrant
                            , accept E.^. SquadUsAcceptId
                            , start E.^. SquadUsStartId
                            )
                remoteExtensionsForTeams <- lift $ for remoteTeams $ \ (E.Value role', E.Value teamID, E.Value _delegID, E.Value grantID, E.Value _acceptID, E.Value startID) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    conveyID <- insert $ SourceUsConvey sendID startID extID
                    case from of
                        Left localID -> insert_ $ SourceUsConveyFromLocal conveyID localID
                        Right remoteID -> insert_ $ SourceUsConveyFromRemote conveyID remoteID
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantForTeam (Right (teamID, grantID)) (min role role') startID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return
                    ( recipActorID
                    , localExtensions ++ localExtensionsForParents ++ localExtensionsForTeams
                    , remoteExtensions ++ remoteExtensionsForParents ++ remoteExtensionsForTeams
                    , inboxItemID
                    )

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, localExts, remoteExts, inboxItemID) -> do
                let recipByID = LocalActorProject projectID
                lift $ for_ (localExts ++ remoteExts) $
                    \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            recipByID recipActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "Sent extensions to collabs & parents & teams"

        where

        prepareExtensionGrant collab role enableID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            uStart <- lift $ getActivityURI authorIdMsig

            (uCollab, audCollab, uDeleg) <-
                case collab of
                    Left (personID, itemID) -> do
                        personHash <- encodeKeyHashid personID
                        itemHash <- encodeKeyHashid itemID
                        return
                            ( encodeRouteHome $ PersonR personHash
                            , AudLocal [LocalActorPerson personHash] []
                            , encodeRouteHome $
                                PersonOutboxItemR personHash itemHash
                            )
                    Right (raID, ractID) -> do
                        ra <- getJust raID
                        u@(ObjURI h lu) <- getRemoteActorURI ra
                        uAct <- do
                            ract <- getJust ractID
                            getRemoteActivityURI ract
                        return (u, AudRemote h [lu] [], uAct)

            enableHash <- encodeKeyHashid enableID

            let audience = [audCollab]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience audience

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = AP.grantContext grant
                        , AP.grantTarget    = uCollab
                        , AP.grantResult    =
                            Just
                                (encodeRouteLocal $
                                    ProjectCollabLiveR projectHash enableHash
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Invoke
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtensionGrantForParent parent role startID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            uStart <- lift $ getActivityURI authorIdMsig

            (uParent, audParent, uDeleg) <-
                case parent of
                    Left (j, itemID) -> do
                        h <- encodeKeyHashid j
                        itemHash <- encodeKeyHashid itemID
                        return
                            ( encodeRouteHome $ ProjectR h
                            , AudLocal [LocalActorProject h] []
                            , encodeRouteHome $
                                ProjectOutboxItemR h itemHash
                            )
                    Right (raID, ractID) -> do
                        ra <- getJust raID
                        u@(ObjURI h lu) <- getRemoteActorURI ra
                        uAct <- do
                            ract <- getJust ractID
                            getRemoteActivityURI ract
                        return (u, AudRemote h [lu] [], uAct)

            resultR <- do
                startHash <- encodeKeyHashid startID
                return $
                    ProjectParentLiveR projectHash startHash

            let audience = [audParent]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience audience

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = AP.grantContext grant
                        , AP.grantTarget    = uParent
                        , AP.grantResult    =
                            Just (encodeRouteLocal resultR, Nothing)
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.GatherAndConvey
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtensionGrantForTeam parent role startID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            uStart <- lift $ getActivityURI authorIdMsig

            (uParent, audParent, uDeleg) <-
                case parent of
                    Left (j, itemID) -> do
                        h <- encodeKeyHashid j
                        itemHash <- encodeKeyHashid itemID
                        return
                            ( encodeRouteHome $ GroupR h
                            , AudLocal [LocalActorGroup h] []
                            , encodeRouteHome $ GroupOutboxItemR h itemHash
                            )
                    Right (raID, ractID) -> do
                        ra <- getJust raID
                        u@(ObjURI h lu) <- getRemoteActorURI ra
                        uAct <- do
                            ract <- getJust ractID
                            getRemoteActivityURI ract
                        return (u, AudRemote h [lu] [], uAct)

            resultR <- do
                startHash <- encodeKeyHashid startID
                return $ ProjectTeamLiveR projectHash startHash

            let audience = [audParent]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience audience

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = AP.grantContext grant
                        , AP.grantTarget    = uParent
                        , AP.grantResult    =
                            Just (encodeRouteLocal resultR, Nothing)
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Distribute
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    tryParent (GKDelegationStart _)    = lift mzero
    tryParent (GKDelegationExtend _ _) = lift mzero
    tryParent GKDelegator              = do
        uFulfills <-
            case AP.activityFulfills $ actbActivity body of
                [] -> throwE "No fulfills"
                [u] -> pure u
                _ -> throwE "Multiple fulfills"
        fulfills <- ExceptT $ lift $ lift $ runExceptT $ first (\ (a, _, i) -> (a, i)) <$> parseActivityURI' uFulfills
        fulfillsDB <- ExceptT $ MaybeT $ either (Just . Left) (fmap Right) <$> runExceptT (getActivity fulfills)
        -- Find the Dest record from the fulfills
        destID <-
            lift $
            case fulfillsDB of
                Left (_, _, addID) ->
                    (do DestUsGestureLocal destID _ <- MaybeT $ getValBy $ UniqueDestUsGestureLocalActivity addID
                        _ <- MaybeT $ getBy $ UniqueDestOriginUs destID
                        return destID
                    )
                    <|>
                    (do DestThemGestureLocal themID _ <- MaybeT $ getValBy $ UniqueDestThemGestureLocalAdd addID
                        DestOriginThem destID <- lift $ getJust themID
                        return destID
                    )
                Right addID ->
                    (do DestUsGestureRemote destID _ _ <- MaybeT $ getValBy $ UniqueDestUsGestureRemoteActivity addID
                        _ <- MaybeT $ getBy $ UniqueDestOriginUs destID
                        return destID
                    )
                    <|>
                    (do DestThemGestureRemote themID _ _ <- MaybeT $ getValBy $ UniqueDestThemGestureRemoteAdd addID
                        DestOriginThem destID <- lift $ getJust themID
                        return destID
                    )
        -- Verify this Dest record is mine
        DestHolderProject _ j <- lift $ MaybeT $ getValBy $ UniqueDestHolderProject destID
        lift $ guard $ j == projectID
        -- Verify the Grant sender is the Dest topic
        topic <- do
            t <- lift $ lift $ getDestTopic destID
            bitraverse
                (bitraverse
                    pure
                    (\case
                        Left j -> pure j
                        Right _g -> error "I have a DestTopic that is a Group"
                    )
                )
                pure
                t
        topicForCheck <-
            lift $ lift $
            bitraverse
                (pure . snd)
                (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                topic
        unless (first LocalActorProject topicForCheck == bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig) $
            throwE "Dest topic and Grant author aren't the same actor"
        -- Verify I sent my Accept
        maybeMe <- lift $ lift $ getKeyBy $ UniqueDestUsAccept destID
        meAcceptID <- fromMaybeE maybeMe "I haven't sent my Accept"
        -- Verify I haven't yet seen a delegator-Grant from the parent
        case bimap fst fst topic of
            Left localID -> do
                m <- lift $ lift $ getBy $ UniqueDestThemSendDelegatorLocalTopic localID
                verifyNothingE m "Already have a DestThemSendDelegatorLocal"
            Right remoteID -> do
                m <- lift $ lift $ getBy $ UniqueDestThemSendDelegatorRemoteTopic remoteID
                verifyNothingE m "Already have a DestThemSendDelegatorRemote"
        Dest role <- lift $ lift $ getJust destID
        return (role, topic, meAcceptID)

    handleParent role topic acceptID = do

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust projectID
                let actorID = projectActor recip
                (actorID,) <$> getJust actorID

            maybeGrantDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeGrantDB $ \ (inboxItemID, grantDB) -> do

                -- Record the delegator-Grant in DB
                to <- case (grantDB, bimap fst fst topic) of
                    (Left (_, _, grantID), Left localID) -> Left <$> do
                        mk <- lift $ insertUnique $ DestThemSendDelegatorLocal acceptID localID grantID
                        fromMaybeE mk "I already have such a DestThemSendDelegatorLocal"
                    (Right (_, _, grantID), Right remoteID) -> Right <$> do
                        mk <- lift $ insertUnique $ DestThemSendDelegatorRemote acceptID remoteID grantID
                        fromMaybeE mk "I already have such a DestThemSendDelegatorRemote"
                    _ -> error "projectGrant.parent impossible"

                startID <- lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
                destStartID <- lift $ insert $ DestUsStart acceptID startID

                -- Prepare a start-Grant
                start@(actionStart, _, _, _) <- lift $ prepareStartGrant role destStartID
                let recipByKey = LocalActorProject projectID
                _luStart <- lift $ updateOutboxItem' recipByKey startID actionStart

                -- For each Component in me, prepare a delegation-extension Grant
                localComponents <-
                    lift $
                    E.select $ E.from $ \ (deleg `E.InnerJoin` local `E.InnerJoin` comp `E.InnerJoin` enable) -> do
                        E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                        E.on $ local E.^. ComponentLocalComponent E.==. comp E.^. ComponentId
                        E.on $ deleg E.^. ComponentDelegateLocalComponent E.==.local E.^. ComponentLocalId
                        E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
                        return
                            ( deleg E.^. ComponentDelegateLocalGrant
                            , comp
                            , enable
                            )
                localExtensions <- lift $ for localComponents $ \ (E.Value startID, Entity componentID component, Entity enableID _) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    insert_ $ ComponentGather enableID destStartID extID
                    componentIdent <- do
                        i <- getComponentIdent componentID
                        bitraverse
                            (pure . snd)
                            (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                            i
                    uStart <- do
                        encodeRouteHome <- getEncodeRouteHome
                        c <-
                            case componentIdent of
                                Left ci -> hashComponent ci
                                Right _ -> error "Delegation-start Grant URI is local, but component found to be remote, impossible"
                        s <- encodeKeyHashid startID
                        return $ encodeRouteHome $ activityRoute (resourceToActor $ componentResource c) s
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrant componentIdent uStart (min role (componentRole component)) enableID destStartID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                remoteComponents <-
                    lift $
                    E.select $ E.from $ \ (deleg `E.InnerJoin` remote `E.InnerJoin` comp `E.InnerJoin` enable) -> do
                        E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                        E.on $ remote E.^. ComponentRemoteComponent E.==. comp E.^. ComponentId
                        E.on $ deleg E.^. ComponentDelegateRemoteComponent E.==.remote E.^. ComponentRemoteId
                        E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
                        return
                            ( deleg E.^. ComponentDelegateRemoteGrant
                            , comp
                            , enable
                            )
                remoteExtensions <- lift $ for remoteComponents $ \ (E.Value startID, Entity componentID component, Entity enableID _) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    insert_ $ ComponentGather enableID destStartID extID
                    componentIdent <- do
                        i <- getComponentIdent componentID
                        bitraverse
                            (pure . snd)
                            (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                            i
                    uStart <- do
                        ra <- getJust startID
                        getRemoteActivityURI ra
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrant componentIdent uStart (min role (componentRole component)) enableID destStartID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                -- For each Grant I got from a child, prepare a
                -- delegation-extension Grant
                l <-
                    lift $ fmap (map $ over _2 Left) $
                    E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send `E.InnerJoin` deleg) -> do
                        E.on $ accept E.^. SourceThemAcceptLocalId E.==. deleg E.^. SourceThemDelegateLocalSource
                        E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                        E.on $ topic E.^. SourceTopicLocalId E.==. accept E.^. SourceThemAcceptLocalTopic
                        E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicLocalSource
                        E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
                        E.where_ $ holder E.^. SourceHolderProjectProject E.==. E.val projectID
                        return
                            ( send E.^. SourceUsSendDelegatorId
                            , deleg
                            )
                r <-
                    lift $ fmap (map $ over _2 Right) $
                    E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send `E.InnerJoin` deleg) -> do
                        E.on $ accept E.^. SourceThemAcceptRemoteId E.==. deleg E.^. SourceThemDelegateRemoteSource
                        E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                        E.on $ topic E.^. SourceTopicRemoteId E.==. accept E.^. SourceThemAcceptRemoteTopic
                        E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicRemoteSource
                        E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
                        E.where_ $ holder E.^. SourceHolderProjectProject E.==. E.val projectID
                        return
                            ( send E.^. SourceUsSendDelegatorId
                            , deleg
                            )
                fromChildren <- lift $ for (l ++ r) $ \ (E.Value sendID, deleg) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now

                    gatherID <- insert $ SourceUsGather sendID destStartID extID
                    case bimap entityKey entityKey deleg of
                        Left localID -> insert_ $ SourceUsGatherFromLocal gatherID localID
                        Right remoteID -> insert_ $ SourceUsGatherFromRemote gatherID remoteID

                    (AP.Doc h a, grant) <- getGrantActivityBody $ bimap (sourceThemDelegateLocalGrant . entityVal) (sourceThemDelegateRemoteGrant . entityVal) deleg
                    uStart <-
                        case AP.activityId a of
                            Nothing -> error "SourceThemDelegate grant has no 'id'"
                            Just lu -> pure $ ObjURI h lu
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantFromChild uStart grant role destStartID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return
                    ( recipActorID
                    , (startID, start) : localExtensions ++ remoteExtensions ++ fromChildren
                    , inboxItemID
                    )

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, exts, inboxItemID) -> do
                let recipByID = LocalActorProject projectID
                lift $ for_ exts $
                    \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            recipByID recipActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "Sent start-Grant and extensions from components and children"

        where

        prepareExtensionGrant component uStart role enableID startID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID

            uDeleg <- lift $ getActivityURI authorIdMsig
            uComponent <-
                case component of
                    Left c -> do
                        a <- resourceToActor . componentResource <$> hashComponent c
                        return $ encodeRouteHome $ renderLocalActor a
                    Right u -> pure u

            enableHash <- encodeKeyHashid enableID

            audParent <- lift $ makeAudSenderOnly authorIdMsig
            uParent <- lift $ getActorURI authorIdMsig

            resultR <- do
                startHash <- encodeKeyHashid startID
                return $ ProjectParentLiveR projectHash startHash

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audParent]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = uComponent
                        , AP.grantTarget    = uParent
                        , AP.grantResult    =
                            Just
                                ( encodeRouteLocal resultR
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.GatherAndConvey
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareStartGrant role startID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID

            uDeleg <- lift $ getActivityURI authorIdMsig

            audParent <- lift $ makeAudSenderOnly authorIdMsig
            uParent <- lift $ getActorURI authorIdMsig

            resultR <- do
                startHash <- encodeKeyHashid startID
                return $ ProjectParentLiveR projectHash startHash

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audParent]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uDeleg]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = encodeRouteHome $ ProjectR projectHash
                        , AP.grantTarget    = uParent
                        , AP.grantResult    =
                            Just
                                ( encodeRouteLocal resultR
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.GatherAndConvey
                        , AP.grantDelegates = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtensionGrantFromChild uStart grant role startID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            finalRole <-
                case AP.grantObject grant of
                    AP.RXRole r -> pure $ min role r
                    AP.RXDelegator -> error "Why was I delegated a Grant with object=delegator?"

            uDeleg <- lift $ getActivityURI authorIdMsig
            audParent <- lift $ makeAudSenderOnly authorIdMsig
            uParent <- lift $ getActorURI authorIdMsig

            resultR <- do
                startHash <- encodeKeyHashid startID
                return $ ProjectParentLiveR projectHash startHash

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audParent]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole finalRole
                        , AP.grantContext   = AP.grantContext grant
                        , AP.grantTarget    = uParent
                        , AP.grantResult    =
                            Just
                                ( encodeRouteLocal resultR
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.GatherAndConvey
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    tryTeam _            (GKDelegationStart _)    = lift mzero
    tryTeam _            (GKDelegationExtend _ _) = lift mzero
    tryTeam meResourceID GKDelegator              = do
        uFulfills <-
            case AP.activityFulfills $ actbActivity body of
                [] -> throwE "No fulfills"
                [u] -> pure u
                _ -> throwE "Multiple fulfills"
        fulfills <- ExceptT $ lift $ lift $ runExceptT $ first (\ (a, _, i) -> (a, i)) <$> parseActivityURI' uFulfills
        fulfillsDB <- ExceptT $ MaybeT $ either (Just . Left) (fmap Right) <$> runExceptT (getActivity fulfills)
        -- Find the Squad record from the fulfills
        squadID <-
            lift $
            case fulfillsDB of
                Left (_, _, addID) ->
                    (do SquadUsGestureLocal squadID _ <- MaybeT $ getValBy $ UniqueSquadUsGestureLocalActivity addID
                        _ <- MaybeT $ getBy $ UniqueSquadOriginUs squadID
                        return squadID
                    )
                    <|>
                    (do SquadThemGestureLocal themID _ <- MaybeT $ getValBy $ UniqueSquadThemGestureLocalAdd addID
                        SquadOriginThem squadID <- lift $ getJust themID
                        return squadID
                    )
                Right addID ->
                    (do SquadUsGestureRemote squadID _ _ <- MaybeT $ getValBy $ UniqueSquadUsGestureRemoteActivity addID
                        _ <- MaybeT $ getBy $ UniqueSquadOriginUs squadID
                        return squadID
                    )
                    <|>
                    (do SquadThemGestureRemote themID _ _ <- MaybeT $ getValBy $ UniqueSquadThemGestureRemoteAdd addID
                        SquadOriginThem squadID <- lift $ getJust themID
                        return squadID
                    )
        -- Verify this Squad record is mine
        Squad role r <- lift $ lift $ getJust squadID
        lift $ guard $ r == meResourceID
        -- Verify the Grant sender is the Squad topic
        topic <- lift $ lift $ getSquadTeam squadID
        topicForCheck <-
            lift $ lift $
            bitraverse
                (pure . snd)
                (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                topic
        unless (first LocalActorGroup topicForCheck == bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig) $
            throwE "Squad topic and Grant author aren't the same actor"
        -- Verify I sent my Accept
        maybeMe <- lift $ lift $ getKeyBy $ UniqueSquadUsAccept squadID
        meAcceptID <- fromMaybeE maybeMe "I haven't sent my Accept"
        -- Verify I haven't yet seen a delegator-Grant from the team
        case bimap fst fst topic of
            Left localID -> do
                m <- lift $ lift $ getBy $ UniqueSquadThemSendDelegatorLocalTopic localID
                verifyNothingE m "Already have a SquadThemSendDelegatorLocal"
            Right remoteID -> do
                m <- lift $ lift $ getBy $ UniqueSquadThemSendDelegatorRemoteTopic remoteID
                verifyNothingE m "Already have a SquadThemSendDelegatorRemote"
        return (role, topic, meAcceptID)

    handleTeam role topic acceptID = do

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (recipActorID, recipActor) <- lift $ do
                recip <- getJust projectID
                let actorID = projectActor recip
                (actorID,) <$> getJust actorID

            maybeGrantDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeGrantDB $ \ (inboxItemID, grantDB) -> do

                -- Record the delegator-Grant in DB
                to <- case (grantDB, bimap fst fst topic) of
                    (Left (_, _, grantID), Left localID) -> Left <$> do
                        mk <- lift $ insertUnique $ SquadThemSendDelegatorLocal acceptID localID grantID
                        fromMaybeE mk "I already have such a SquadThemSendDelegatorLocal"
                    (Right (_, _, grantID), Right remoteID) -> Right <$> do
                        mk <- lift $ insertUnique $ SquadThemSendDelegatorRemote acceptID remoteID grantID
                        fromMaybeE mk "I already have such a SquadThemSendDelegatorRemote"
                    _ -> error "projectGrant.team impossible"

                startID <- lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
                squadStartID <- lift $ insert $ SquadUsStart acceptID startID

                -- Prepare a start-Grant
                start@(actionStart, _, _, _) <- lift $ prepareStartGrant role squadStartID
                let recipByKey = LocalActorProject projectID
                _luStart <- lift $ updateOutboxItem' recipByKey startID actionStart

                -- For each Component in me, prepare a delegation-extension Grant
                localComponents <-
                    lift $
                    E.select $ E.from $ \ (deleg `E.InnerJoin` local `E.InnerJoin` comp `E.InnerJoin` enable) -> do
                        E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                        E.on $ local E.^. ComponentLocalComponent E.==. comp E.^. ComponentId
                        E.on $ deleg E.^. ComponentDelegateLocalComponent E.==.local E.^. ComponentLocalId
                        E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
                        return
                            ( deleg E.^. ComponentDelegateLocalGrant
                            , comp
                            , enable
                            )
                localExtensions <- lift $ for localComponents $ \ (E.Value startID, Entity componentID component, Entity enableID _) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    insert_ $ ComponentConvey enableID squadStartID extID
                    componentIdent <- do
                        i <- getComponentIdent componentID
                        bitraverse
                            (pure . snd)
                            (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                            i
                    uStart <- do
                        encodeRouteHome <- getEncodeRouteHome
                        c <-
                            case componentIdent of
                                Left ci -> hashComponent ci
                                Right _ -> error "Delegation-start Grant URI is local, but component found to be remote, impossible"
                        s <- encodeKeyHashid startID
                        return $ encodeRouteHome $ activityRoute (resourceToActor $ componentResource c) s
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrant componentIdent uStart (min role (componentRole component)) enableID squadStartID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                remoteComponents <-
                    lift $
                    E.select $ E.from $ \ (deleg `E.InnerJoin` remote `E.InnerJoin` comp `E.InnerJoin` enable) -> do
                        E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                        E.on $ remote E.^. ComponentRemoteComponent E.==. comp E.^. ComponentId
                        E.on $ deleg E.^. ComponentDelegateRemoteComponent E.==.remote E.^. ComponentRemoteId
                        E.where_ $ comp E.^. ComponentProject E.==. E.val projectID
                        return
                            ( deleg E.^. ComponentDelegateRemoteGrant
                            , comp
                            , enable
                            )
                remoteExtensions <- lift $ for remoteComponents $ \ (E.Value startID, Entity componentID component, Entity enableID _) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                    insert_ $ ComponentConvey enableID squadStartID extID
                    componentIdent <- do
                        i <- getComponentIdent componentID
                        bitraverse
                            (pure . snd)
                            (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                            i
                    uStart <- do
                        ra <- getJust startID
                        getRemoteActivityURI ra
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrant componentIdent uStart (min role (componentRole component)) enableID squadStartID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                -- For each Grant I got from a child, prepare a
                -- delegation-extension Grant
                l <-
                    lift $ fmap (map $ over _2 Left) $
                    E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send `E.InnerJoin` deleg) -> do
                        E.on $ accept E.^. SourceThemAcceptLocalId E.==. deleg E.^. SourceThemDelegateLocalSource
                        E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                        E.on $ topic E.^. SourceTopicLocalId E.==. accept E.^. SourceThemAcceptLocalTopic
                        E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicLocalSource
                        E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
                        E.where_ $ holder E.^. SourceHolderProjectProject E.==. E.val projectID
                        return
                            ( send E.^. SourceUsSendDelegatorId
                            , deleg
                            )
                r <-
                    lift $ fmap (map $ over _2 Right) $
                    E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send `E.InnerJoin` deleg) -> do
                        E.on $ accept E.^. SourceThemAcceptRemoteId E.==. deleg E.^. SourceThemDelegateRemoteSource
                        E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                        E.on $ topic E.^. SourceTopicRemoteId E.==. accept E.^. SourceThemAcceptRemoteTopic
                        E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicRemoteSource
                        E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
                        E.where_ $ holder E.^. SourceHolderProjectProject E.==. E.val projectID
                        return
                            ( send E.^. SourceUsSendDelegatorId
                            , deleg
                            )
                fromChildren <- lift $ for (l ++ r) $ \ (E.Value sendID, deleg) -> do
                    extID <- insertEmptyOutboxItem' (actorOutbox recipActor) now

                    conveyID <- insert $ SourceUsConvey sendID squadStartID extID
                    case bimap entityKey entityKey deleg of
                        Left localID -> insert_ $ SourceUsConveyFromLocal conveyID localID
                        Right remoteID -> insert_ $ SourceUsConveyFromRemote conveyID remoteID

                    (AP.Doc h a, grant) <- getGrantActivityBody $ bimap (sourceThemDelegateLocalGrant . entityVal) (sourceThemDelegateRemoteGrant . entityVal) deleg
                    uStart <-
                        case AP.activityId a of
                            Nothing -> error "SourceThemDelegate grant has no 'id'"
                            Just lu -> pure $ ObjURI h lu
                    ext@(actionExt, _, _, _) <-
                        prepareExtensionGrantFromChild uStart grant role squadStartID
                    let recipByKey = LocalActorProject projectID
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return
                    ( recipActorID
                    , (startID, start) : localExtensions ++ remoteExtensions ++ fromChildren
                    , inboxItemID
                    )

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, exts, inboxItemID) -> do
                let recipByID = LocalActorProject projectID
                lift $ for_ exts $
                    \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            recipByID recipActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "[Team] Sent start-Grant and extensions from components and children"

        where

        prepareExtensionGrant component uStart role enableID startID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID

            uDeleg <- lift $ getActivityURI authorIdMsig
            uComponent <-
                case component of
                    Left c -> do
                        a <- resourceToActor . componentResource <$> hashComponent c
                        return $ encodeRouteHome $ renderLocalActor a
                    Right u -> pure u

            enableHash <- encodeKeyHashid enableID

            audTeam <- lift $ makeAudSenderOnly authorIdMsig
            uTeam <- lift $ getActorURI authorIdMsig

            resultR <- do
                startHash <- encodeKeyHashid startID
                return $ ProjectTeamLiveR projectHash startHash

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audTeam]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = uComponent
                        , AP.grantTarget    = uTeam
                        , AP.grantResult    =
                            Just
                                ( encodeRouteLocal resultR
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Distribute
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareStartGrant role startID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID

            uDeleg <- lift $ getActivityURI authorIdMsig

            audTeam <- lift $ makeAudSenderOnly authorIdMsig
            uTeam <- lift $ getActorURI authorIdMsig

            resultR <- do
                startHash <- encodeKeyHashid startID
                return $ ProjectTeamLiveR projectHash startHash

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audTeam]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uDeleg]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = encodeRouteHome $ ProjectR projectHash
                        , AP.grantTarget    = uTeam
                        , AP.grantResult    =
                            Just
                                ( encodeRouteLocal resultR
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Distribute
                        , AP.grantDelegates = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtensionGrantFromChild uStart grant role startID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            finalRole <-
                case AP.grantObject grant of
                    AP.RXRole r -> pure $ min role r
                    AP.RXDelegator -> error "Why was I delegated a Grant with object=delegator?"

            uDeleg <- lift $ getActivityURI authorIdMsig
            audTeam <- lift $ makeAudSenderOnly authorIdMsig
            uTeam <- lift $ getActorURI authorIdMsig

            resultR <- do
                startHash <- encodeKeyHashid startID
                return $ ProjectTeamLiveR projectHash startHash

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audTeam]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uStart]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole finalRole
                        , AP.grantContext   = AP.grantContext grant
                        , AP.grantTarget    = uTeam
                        , AP.grantResult    =
                            Just
                                ( encodeRouteLocal resultR
                                , Nothing
                                )
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Distribute
                        , AP.grantDelegates = Just uStart
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor A invited actor B to a resource
-- Behavior:
--      * Verify the resource is my collabs list
--      * If B is local, verify it's a Person
--      * Verify A isn't inviting themselves
--      * Verify A is authorized by me to invite collabs to me
--
--      * Verify B doesn't already have an invite/join/grant for me
--
--      * Insert the Invite to my inbox
--
--      * Insert a Collab record to DB
--
--      * Forward the Invite to my followers
--      * Send Accept to A, B, my-followers
projectInvite
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Invite URIMode
    -> ActE (Text, Act (), Next)
projectInvite now projectID (Verse authorIdMsig body) invite = do

    -- Check capability
    capability <- do

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the capability URI is one of:
        --   * Outbox item URI of a local actor, i.e. a local activity
        --   * A remote URI
        cap <- nameExceptT "Invite capability" $ parseActivityURI' uCap

        -- Verify the capability is local
        case cap of
            Left (actorByKey, _, outboxItemID) ->
                return (actorByKey, outboxItemID)
            _ -> throwE "Capability is remote i.e. definitely not by me"

    -- Check invite
    (role, invited) <- do
        let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
        (role, resourceOrComps, recipientOrComp) <- parseInvite author invite
        mode <-
            case resourceOrComps of
                Left (Left (LocalResourceProject j)) | j == projectID ->
                    bitraverse
                        (\case
                            Left r -> pure r
                            Right _ -> throwE "Not accepting local component actors as collabs"
                        )
                        pure
                        recipientOrComp
                _ -> throwE "Invite topic isn't my collabs URI"
        return (role, mode)

    -- If target is local, find it in our DB
    -- If target is remote, HTTP GET it, verify it's an actor, and store in
    -- our DB (if it's already there, no need for HTTP)
    --
    -- NOTE: This is a blocking HTTP GET done right here in the Invite handler,
    -- which is NOT a good idea. Ideally, it would be done async, and the
    -- handler result (approve/disapprove the Invite) would be sent later in a
    -- separate (e.g. Accept) activity. But for the PoC level, the current
    -- situation will hopefully do.
    collab <-
        bitraverse
            (withDBExcept . flip getGrantRecip "Invitee not found in DB")
            getRemoteActorFromURI
            invited

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        resourceID <- lift $ projectResource <$> getJust projectID
        Resource topicActorID <- lift $ getJust resourceID
        topicActor <- lift $ getJust topicActorID

        -- Verify the specified capability gives relevant access
        verifyCapability'
            capability authorIdMsig (LocalResourceProject projectID) AP.RoleAdmin

        -- Verify that target doesn't already have a Collab for me
        existingCollabIDs <- lift $ getExistingCollabs resourceID collab
        case existingCollabIDs of
            [] -> pure ()
            [_] -> throwE "I already have a Collab for the target"
            _ -> error "Multiple collabs found for target"

        maybeInviteDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
        lift $ for maybeInviteDB $ \ (inboxItemID, inviteDB) -> do

            -- Insert Collab or Component record to DB
            acceptID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
            insertCollab resourceID role collab inviteDB acceptID

            -- Prepare forwarding Invite to my followers
            sieve <- do
                projectHash <- encodeKeyHashid projectID
                return $ makeRecipientSet [] [LocalStageProjectFollowers projectHash]

            -- Prepare an Accept activity and insert to my outbox
            accept@(actionAccept, _, _, _) <- prepareAccept collab
            _luAccept <- updateOutboxItem' (LocalActorProject projectID) acceptID actionAccept

            return (topicActorID, sieve, acceptID, accept, inboxItemID)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (projectActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
            forwardActivity
                authorIdMsig body (LocalActorProject projectID) projectActorID sieve
            lift $ sendActivity
                (LocalActorProject projectID) projectActorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            doneDB inboxItemID "Recorded and forwarded the Invite, sent an Accept"

    where

    getRemoteActorFromURI (ObjURI h lu) = do
        instanceID <-
            lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
        result <-
            ExceptT $ first (T.pack . displayException) <$>
                fetchRemoteActor' instanceID h lu
        case result of
            Left Nothing -> throwE "Target @id mismatch"
            Left (Just err) -> throwE $ T.pack $ displayException err
            Right Nothing -> throwE "Target isn't an actor"
            Right (Just actor) -> return $ entityKey actor

    getExistingCollabs resourceID (Left (GrantRecipPerson (Entity personID _))) =
        E.select $ E.from $ \ (collab `E.InnerJoin` recipl) -> do
            E.on $
                collab E.^. CollabId E.==.
                recipl E.^. CollabRecipLocalCollab
            E.where_ $
                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                recipl E.^. CollabRecipLocalPerson E.==. E.val personID
            return $ recipl E.^. CollabRecipLocalCollab
    getExistingCollabs resourceID (Right remoteActorID) =
        E.select $ E.from $ \ (collab `E.InnerJoin` recipr) -> do
            E.on $
                collab E.^. CollabId E.==.
                recipr E.^. CollabRecipRemoteCollab
            E.where_ $
                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                recipr E.^. CollabRecipRemoteActor E.==. E.val remoteActorID
            return $ recipr E.^. CollabRecipRemoteCollab

    insertCollab resourceID role recipient inviteDB acceptID = do
        collabID <- insert $ Collab role resourceID
        fulfillsID <- insert $ CollabFulfillsInvite collabID acceptID
        case inviteDB of
            Left (_, _, inviteID) ->
                insert_ $ CollabInviterLocal fulfillsID inviteID
            Right (author, _, inviteID) -> do
                let authorID = remoteAuthorId author
                insert_ $ CollabInviterRemote fulfillsID authorID inviteID
        case recipient of
            Left (GrantRecipPerson (Entity personID _)) ->
                insert_ $ CollabRecipLocal collabID personID
            Right remoteActorID ->
                insert_ $ CollabRecipRemote collabID remoteActorID

    prepareAccept invitedDB = do
        encodeRouteHome <- getEncodeRouteHome

        audInviter <- lift $ makeAudSenderOnly authorIdMsig
        audInvited <-
            case invitedDB of
                Left (GrantRecipPerson (Entity p _)) -> do
                    ph <- encodeKeyHashid p
                    return $ AudLocal [LocalActorPerson ph] []
                Right remoteActorID -> do
                    ra <- getJust remoteActorID
                    ObjURI h lu <- getRemoteActorURI ra
                    return $ AudRemote h [lu] []
        audTopic <-
            AudLocal [] . pure . LocalStageProjectFollowers <$>
                encodeKeyHashid projectID
        uInvite <- lift $ getActivityURI authorIdMsig

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audInviter, audInvited, audTopic]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uInvite]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uInvite
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor A asked to join a resource
-- Behavior:
--      * Verify the resource is me
--      * Verify A doesn't already have an invite/join/grant for me
--      * Remember the join in DB
--      * Forward the Join to my followers
projectJoin
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Join URIMode
    -> ActE (Text, Act (), Next)
projectJoin = topicJoin projectResource LocalResourceProject

-- Meaning: An actor rejected something
-- Behavior:
--     * If it's on an Invite where I'm the resource:
--         * Verify the Reject is by the Invite target
--         * Remove the relevant Collab record from DB
--         * Forward the Reject to my followers
--         * Send a Reject on the Invite:
--             * To: Rejecter (i.e. Invite target)
--             * CC: Invite sender, Rejecter's followers, my followers
--     * If it's on a Join where I'm the resource:
--         * Verify the Reject is authorized
--         * Remove the relevant Collab record from DB
--         * Forward the Reject to my followers
--         * Send a Reject:
--             * To: Join sender
--             * CC: Reject sender, Join sender's followers, my followers
--     * Otherwise respond with error
projectReject
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Reject URIMode
    -> ActE (Text, Act (), Next)
projectReject = topicReject projectResource LocalResourceProject

-- Meaning: An actor A is removing actor B from collection C
-- Behavior:
--      * If C is my collaborators collection:
--          * Verify A isn't removing themselves
--          * Verify A is authorized by me to remove actors from me
--          * Verify B already has a Grant for me
--          * Remove the whole Collab record from DB
--          * Forward the Remove to my followers
--          * Send a Revoke:
--              * To: Actor B
--              * CC: Actor A, B's followers, my followers
--          * Send revokes on my extensions from:
--              * My components
--              * My children
--
--      * If C is my components collection:
--          * Verify A is authorized by me
--          * Verify B is an active component of mine
--          * Remove the whole Component record from DB
--          * Forward the Remove to my followers
--          * Send a Revoke on the delegator-Grant:
--              * To: Actor B
--              * CC: Actor A, B's followers, my followers
--          * Send revokes on my extensions of the start-Grant to:
--              * My collaborators
--              * My teams
--              * My parents
--
--      * If C is my children collection:
--          * Verify A isn't removing themselves
--          * Verify A is authorized by me to remove actors from me
--          * Verify B is an active child of mine
--          * Remove the whole Source record from DB
--          * Forward the Remove to my followers
--          * Send a Revoke on the delegator-Grant I had for B:
--              * To: Actor B
--              * CC: Actor A, B's followers, my followers
--          * Send a Revoke on every extention-Grant I extended on every
--            delegation Grant I got from B
--              * To: The parent/collaborator/team to whom I'd sent the Grant
--              * CC: -
--
--      * If C is my parents collection:
--          * Verify A isn't removing themselves
--          * Verify A is authorized by me to remove actors from me
--          * Verify B is an active parent of mine
--          * Remove the whole Dest record from DB
--          * Forward the Remove to my followers
--          * Send an Accept on the Remove:
--              * To: Actor B
--              * CC: Actor A, B's followers, my followers
--
--      * If C is my teams collection:
--          * Verify A is authorized by me to remove teams from me
--          * Verify B is an active team of mine
--          * Remove the whole Squad record from DB
--          * Forward the Remove to my followers
--          * Send an Accept on the Remove:
--              * To: Actor B
--              * CC: Actor A, B's followers, my followers
--
--      * If I'm B, being removed from the parents of a child of mine:
--          * Record this Remove in the Source record
--          * Forward to followers
--
--      * If I'm B, being removed from the children of a parent of mine:
--          * Do nothing, just waiting for parent to send a Revoke on the
--            delegator-Grant
--
--      * If I'm B, being removed from the projects of a component of mine:
--          * Do nothing, just waiting for component to send a Revoke on the
--            start-Grant
--
--      * If I'm B, being removed from the resources of a team of mine:
--          * Do nothing, just waiting for team to send a Revoke on the
--            delegator-Grant
projectRemove
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Remove URIMode
    -> ActE (Text, Act (), Next)
projectRemove now projectID (Verse authorIdMsig body) remove = do

    let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
    (collection, item) <- parseRemove author remove
    case (collection, item) of
        (Left (Left (LocalResourceProject j)), _) | j == projectID ->
            removeCollab item
        (Left (Right (ATProjectComponents j)), _) | j == projectID ->
            removeComponentActive item
        (Left (Right (ATProjectChildren j)), _) | j == projectID ->
            removeChildActive item
        (Left (Right (ATProjectParents j)), _) | j == projectID ->
            removeParentActive item
        (Left (Right (ATProjectTeams j)), _) | j == projectID ->
            removeTeamActive item
        (_, Left (LocalActorProject j)) | j == projectID ->
            case collection of
                Left (Right (ATProjectParents j)) | j /= projectID ->
                    removeChildPassive $ Left j
                Left (Right (ATProjectChildren j)) | j /= projectID ->
                    removeParentPassive $ Left j
                Left (Right (ATGroupEfforts g)) ->
                    removeTeamPassive $ Left g
                Left (Right at) | isJust (addTargetComponentProjects at) ->
                    removeComponentPassive $ Left $ fromJust $ addTargetComponentProjects at
                Right (ObjURI h luColl) -> do
                    -- NOTE this is HTTP GET done synchronously in the activity
                    -- handler
                    manager <- asksEnv envHttpManager
                    c <- AP.fetchAPID_T manager (AP.collectionId :: AP.Collection FedURI URIMode -> LocalURI) h luColl
                    lu <- fromMaybeE (AP.collectionContext c) "No context"
                    rwc <- AP.fetchRWC_T manager h lu
                    AP.Actor l d <-
                        case AP.rwcResource rwc of
                            AP.ResourceActor a -> pure a
                            AP.ResourceChild _ _ -> throwE "Remove.origin remote ResourceChild"
                    let typ = AP.actorType d
                    if typ == AP.ActorTypeProject && Just luColl == AP.rwcSubprojects rwc
                        then removeParentPassive $ Right $ ObjURI h lu
                    else if typ == AP.ActorTypeProject && Just luColl == AP.rwcParentsOrProjects rwc
                        then removeChildPassive $ Right $ ObjURI h lu
                    else if AP.actorTypeIsComponent typ && Just luColl == AP.rwcParentsOrProjects rwc
                        then removeComponentPassive $ Right $ ObjURI h lu
                    else if typ == AP.ActorTypeTeam && Just luColl == AP.rwcTeamResources rwc
                        then removeTeamPassive $ Right $ ObjURI h lu
                    else throwE "Weird collection situation"
                _ -> throwE "I'm being removed from somewhere irrelevant"
        _ -> throwE "This Remove isn't for me"

    where

    removeCollab member = do

        -- Check remove
        memberByKey <-
            bitraverse
                (\case
                    LocalActorPerson p -> pure p
                    _ -> throwE "Not accepting non-person actors as collabs"
                )
                pure
                member

        -- Verify the specified capability gives relevant access
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceProject projectID)
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Find member in our DB
            memberDB <-
                bitraverse
                    (flip getEntityE "Member not found in DB")
                    (\ u@(ObjURI h lu) -> (,u) <$> do
                        maybeActor <- lift $ runMaybeT $ do
                            iid <- MaybeT $ getKeyBy $ UniqueInstance h
                            roid <- MaybeT $ getKeyBy $ UniqueRemoteObject iid lu
                            MaybeT $ getBy $ UniqueRemoteActor roid
                        fromMaybeE maybeActor "Remote removee not found in DB"
                    )
                    memberByKey

            -- Grab me from DB
            resourceID <- lift $ projectResource <$> getJust projectID
            Resource topicActorID <- lift $ getJust resourceID
            topicActor <- lift $ getJust topicActorID

            -- Find the collab that the member already has for me
            existingCollabIDs <-
                lift $ case memberDB of
                    Left (Entity personID _) ->
                        fmap (map $ over _1 Left) $
                        E.select $ E.from $ \ (collab `E.InnerJoin` recipl) -> do
                            E.on $
                                collab E.^. CollabId E.==.
                                recipl E.^. CollabRecipLocalCollab
                            E.where_ $
                                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                                recipl E.^. CollabRecipLocalPerson E.==. E.val personID
                            return
                                ( recipl E.^. persistIdField
                                , recipl E.^. CollabRecipLocalCollab
                                )
                    Right (Entity remoteActorID _, _) ->
                        fmap (map $ over _1 Right) $
                        E.select $ E.from $ \ (collab `E.InnerJoin` recipr) -> do
                            E.on $
                                collab E.^. CollabId E.==.
                                recipr E.^. CollabRecipRemoteCollab
                            E.where_ $
                                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                                recipr E.^. CollabRecipRemoteActor E.==. E.val remoteActorID
                            return
                                ( recipr E.^. persistIdField
                                , recipr E.^. CollabRecipRemoteCollab
                                )
            (recipID, E.Value collabID) <-
                case existingCollabIDs of
                    [] -> throwE "Remove object isn't a member of me"
                    [collab] -> return collab
                    _ -> error "Multiple collabs found for removee"

            -- Verify the Collab is enabled
            maybeEnabled <- lift $ getBy $ UniqueCollabEnable collabID
            Entity enableID (CollabEnable _ grantID) <-
                fromMaybeE maybeEnabled "Remove object isn't a member of me yet"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                -- Grab grants that I'm about to revoke
                maybeDeleg <-
                    case recipID of
                        Left (E.Value localID) -> fmap Left <$> getKeyBy (UniqueCollabDelegLocalRecip localID)
                        Right (E.Value remoteID) -> fmap Right <$> getKeyBy (UniqueCollabDelegRemoteRecip remoteID)
                grantIDs <-
                    case maybeDeleg of
                        Nothing -> pure []
                        Just deleg -> do
                            fromComponents <-
                                case deleg of
                                    Left localID -> do
                                        furthersL <- selectList [ComponentFurtherLocalCollab ==. localID] []
                                        deleteWhere [ComponentFurtherLocalCollab ==. localID]
                                        return $ map (componentFurtherLocalGrant . entityVal) furthersL
                                    Right remoteID -> do
                                        furthersL <- selectList [ComponentFurtherRemoteCollab ==. remoteID] []
                                        deleteWhere [ComponentFurtherRemoteCollab ==. remoteID]
                                        return $ map (componentFurtherRemoteGrant . entityVal) furthersL
                            fromChildren <-
                                case deleg of
                                    Left localID -> do
                                        tos <- selectList [SourceUsLeafToLocalTo ==. localID] []
                                        leafs <- selectList [SourceUsLeafId <-. map (sourceUsLeafToLocalLeaf . entityVal) tos] []
                                        deleteWhere [SourceUsLeafFromLocalLeaf <-. map entityKey leafs]
                                        deleteWhere [SourceUsLeafFromRemoteLeaf <-. map entityKey leafs]
                                        deleteWhere [SourceUsLeafToLocalId <-. map entityKey tos]
                                        deleteWhere [SourceUsLeafId <-. map entityKey leafs]
                                        return $ map (sourceUsLeafGrant . entityVal) leafs
                                    Right remoteID -> do
                                        tos <- selectList [SourceUsLeafToRemoteTo ==. remoteID] []
                                        leafs <- selectList [SourceUsLeafId <-. map (sourceUsLeafToRemoteLeaf . entityVal) tos] []
                                        deleteWhere [SourceUsLeafFromLocalLeaf <-. map entityKey leafs]
                                        deleteWhere [SourceUsLeafFromRemoteLeaf <-. map entityKey leafs]
                                        deleteWhere [SourceUsLeafToRemoteId <-. map entityKey tos]
                                        deleteWhere [SourceUsLeafId <-. map entityKey leafs]
                                        return $ map (sourceUsLeafGrant . entityVal) leafs
                            return $ fromComponents ++ fromChildren

                -- Delete the whole Collab record
                deleteBy $ UniqueCollabDelegLocal enableID
                deleteBy $ UniqueCollabDelegRemote enableID
                delete enableID
                case recipID of
                    Left (E.Value l) -> do
                        deleteBy $ UniqueCollabRecipLocalJoinCollab l
                        deleteBy $ UniqueCollabRecipLocalAcceptCollab l
                        delete l
                    Right (E.Value r) -> do
                        deleteBy $ UniqueCollabRecipRemoteJoinCollab r
                        deleteBy $ UniqueCollabRecipRemoteAcceptCollab r
                        delete r
                fulfills <- do
                    mf <- runMaybeT $ asum
                        [ Left <$> MaybeT (getKeyBy $ UniqueCollabFulfillsLocalTopicCreation collabID)
                        , Right . Left <$> MaybeT (getKeyBy $ UniqueCollabFulfillsInvite collabID)
                        , Right . Right <$> MaybeT (getKeyBy $ UniqueCollabFulfillsJoin collabID)
                        ]
                    maybe (error $ "No fulfills for collabID#" ++ show collabID) pure mf
                case fulfills of
                    Left fc -> delete fc
                    Right (Left fi) -> do
                        deleteBy $ UniqueCollabInviterLocal fi
                        deleteBy $ UniqueCollabInviterRemote fi
                        delete fi
                    Right (Right fj) -> do
                        deleteBy $ UniqueCollabApproverLocal fj
                        deleteBy $ UniqueCollabApproverRemote fj
                        delete fj
                delete collabID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid projectID
                    let topicByHash =
                            LocalActorProject topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare Revoke activities on extensions sent from components
                -- and children
                audCollab <-
                    case memberDB of
                        Left (Entity personID _) -> do
                            personHash <- encodeKeyHashid personID
                            return $ AudLocal [LocalActorPerson personHash] []
                        Right (_, ObjURI h lu) ->
                            return $ AudRemote h [lu] []
                extensions <- for grantIDs $ \ grantID -> do
                    revokeID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
                    revoke@(actionRevoke, _, _, _) <- lift $ prepareExtRevoke audCollab grantID
                    let recipByKey = LocalActorProject projectID
                    _luRevoke <- updateOutboxItem' recipByKey revokeID actionRevoke
                    return (revokeID, revoke)

                -- Prepare a Revoke activity and insert to my outbox
                revoke@(actionRevoke, _, _, _) <-
                    lift $ prepareMainRevoke memberDB grantID
                let recipByKey = LocalActorProject projectID
                revokeID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
                _luRevoke <- updateOutboxItem' recipByKey revokeID actionRevoke

                return (topicActorID, sieve, revokeID, revoke, extensions, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, revokeID, (actionRevoke, localRecipsRevoke, remoteRecipsRevoke, fwdHostsRevoke), extensions, inboxItemID) -> do
                let topicByID = LocalActorProject projectID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $ do
                    sendActivity
                        topicByID topicActorID localRecipsRevoke
                        remoteRecipsRevoke fwdHostsRevoke revokeID actionRevoke
                    for_ extensions $ \ (revokeID, (actionRevoke, localRecipsRevoke, remoteRecipsRevoke, fwdHostsRevoke)) ->
                        sendActivity
                            topicByID topicActorID localRecipsRevoke
                            remoteRecipsRevoke fwdHostsRevoke revokeID actionRevoke
                doneDB inboxItemID "[Collab] Deleted the Grant/Collab, forwarded Remove, sent Revokes"

        where

        prepareMainRevoke member grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            recipHash <- encodeKeyHashid projectID
            let topicByHash = LocalActorProject recipHash

            memberHash <- bitraverse (encodeKeyHashid . entityKey) pure member

            audRemover <- makeAudSenderOnly authorIdMsig
            let audience =
                    let audMember =
                            case memberHash of
                                Left p ->
                                    AudLocal [LocalActorPerson p] [LocalStagePersonFollowers p]
                                Right (Entity _ actor, ObjURI h lu) ->
                                    AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)
                        audTopic = AudLocal [] [localActorFollowers topicByHash]
                    in  [audRemover, audMember, audTopic]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience audience

                recips = map encodeRouteHome audLocal ++ audRemote
            uRemove <- getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtRevoke audCollab grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            let topicByHash = LocalActorProject projectHash
                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audCollab]
                recips = map encodeRouteHome audLocal ++ audRemote

            uRemove <- getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    removeComponentActive item = do

        -- Check remove
        component <-
            bitraverse
                (\ la ->
                    fromMaybeE
                        (resourceToComponent =<< actorToResource la)
                        "Local component isn't of a component type"
                )
                pure
                item

        -- Verify the specified capability gives relevant access
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceProject projectID)
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Find member in our DB
            componentDB <-
                bitraverse
                    (flip getComponentE "Local removee not found in DB")
                    (\ u@(ObjURI h lu) -> (,u) <$> do
                        maybeActor <- lift $ runMaybeT $ do
                            iid <- MaybeT $ getKeyBy $ UniqueInstance h
                            roid <- MaybeT $ getKeyBy $ UniqueRemoteObject iid lu
                            MaybeT $ getBy $ UniqueRemoteActor roid
                        fromMaybeE maybeActor "Remote removee not found in DB"
                    )
                    component

            -- Grab me from DB
            resourceID <- lift $ projectResource <$> getJust projectID
            Resource topicActorID <- lift $ getJust resourceID
            topicActor <- lift $ getJust topicActorID

            -- Find the Component record
            existingComponentIDs <-
                lift $ case first localComponentID componentDB of
                    Left komponentID ->
                        fmap (map $ over _1 Left) $
                        E.select $ E.from $ \ (comp `E.InnerJoin` local `E.InnerJoin` enable) -> do
                            E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                            E.on $ comp E.^. ComponentId E.==. local E.^. ComponentLocalComponent
                            E.where_ $
                                comp E.^. ComponentProject E.==. E.val projectID E.&&.
                                local E.^. ComponentLocalActor E.==. E.val komponentID
                            return
                                ( local E.^. ComponentLocalId
                                , comp E.^. ComponentId
                                , enable
                                )
                    Right (Entity remoteActorID _, _) ->
                        fmap (map $ over _1 Right) $
                        E.select $ E.from $ \ (comp `E.InnerJoin` remote `E.InnerJoin` enable) -> do
                            E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                            E.on $ comp E.^. ComponentId E.==. remote E.^. ComponentRemoteComponent
                            E.where_ $
                                comp E.^. ComponentProject E.==. E.val projectID E.&&.
                                remote E.^. ComponentRemoteActor E.==. E.val remoteActorID
                            return
                                ( remote E.^. ComponentRemoteId
                                , comp E.^. ComponentId
                                , enable
                                )
            (recipID, E.Value componentID, Entity enableID (ComponentEnable _ grantID)) <-
                case existingComponentIDs of
                    [] -> throwE "Remove object isn't a collaborator of me"
                    [c] -> return c
                    _ -> error "Multiple enabled Components found for removee"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                usOrThem <-
                    requireEitherAlt
                        (getKeyBy $ UniqueComponentOriginInvite componentID)
                        (getKeyBy $ UniqueComponentOriginAdd componentID)
                        "Neither us nor them"
                        "Both us and them"

                -- Grab extension-Grants that I'm about to revoke
                furthersL <- selectList [ComponentFurtherLocalComponent ==. enableID] []
                furthersR <- selectList [ComponentFurtherRemoteComponent ==. enableID] []
                gathers <- selectList [ComponentGatherComponent ==. enableID] []
                conveys <- selectList [ComponentConveyComponent ==. enableID] []

                -- Delete the whole Component record
                deleteWhere [ComponentFurtherLocalComponent ==. enableID]
                deleteWhere [ComponentFurtherRemoteComponent ==. enableID]
                deleteWhere [ComponentGatherComponent ==. enableID]
                deleteWhere [ComponentConveyComponent ==. enableID]
                case recipID of
                    Left (E.Value localID) -> deleteBy $ UniqueComponentDelegateLocal localID
                    Right (E.Value remoteID) -> deleteBy $ UniqueComponentDelegateRemote remoteID
                delete enableID
                case usOrThem of
                    Left usID -> do
                        deleteBy $ UniqueComponentProjectAccept usID
                        delete usID
                    Right themID -> do
                        deleteBy $ UniqueComponentGestureLocal themID
                        deleteBy $ UniqueComponentGestureRemote themID
                        delete themID
                deleteBy $ UniqueComponentProjectGestureLocal componentID
                deleteBy $ UniqueComponentProjectGestureRemote componentID
                case recipID of
                    Left (E.Value localID) -> do
                        deleteBy $ UniqueComponentAcceptLocal localID
                        delete localID
                    Right (E.Value remoteID) -> do
                        deleteBy $ UniqueComponentAcceptRemote remoteID
                        delete remoteID
                delete componentID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid projectID
                    let topicByHash =
                            LocalActorProject topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare main Revoke activity and insert to my outbox
                revoke@(actionRevoke, _, _, _) <-
                    lift $ prepareMainRevoke componentDB grantID
                let recipByKey = LocalActorProject projectID
                revokeID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
                _luRevoke <- updateOutboxItem' recipByKey revokeID actionRevoke

                -- Prepare and insert Revokes on all the extension-Grants
                revokesG <- for gathers $ \ (Entity _ (ComponentGather _ startID grantID)) -> do
                    DestUsStart acceptID _ <- getJust startID
                    DestUsAccept destID _ <- getJust acceptID
                    parent <- do
                        p <- getDestTopic destID
                        bitraverse
                            (\case
                                Left j -> pure $ LocalActorProject j
                                Right _ -> error "I'm a project but I have a parent who is a Group"
                            )
                            pure
                            (bimap snd snd p)
                    return (parent, grantID)
                revokesFL <- for furthersL $ \ (Entity _ (ComponentFurtherLocal _ delegID grantID)) -> do
                    CollabDelegLocal _ recipLocalID _ <- getJust delegID
                    CollabRecipLocal _ personID <- getJust recipLocalID
                    return
                        ( Left $ LocalActorPerson personID
                        , grantID
                        )
                revokesFR <- for furthersR $ \ (Entity _ (ComponentFurtherRemote _ delegID grantID)) -> do
                    CollabDelegRemote _ recipRemoteID _ <- getJust delegID
                    CollabRecipRemote _ actorID <- getJust recipRemoteID
                    return
                        ( Right actorID
                        , grantID
                        )
                revokesC <- for conveys $ \ (Entity _ (ComponentConvey _ startID grantID)) -> do
                    SquadUsStart acceptID _ <- getJust startID
                    SquadUsAccept squadID _ <- getJust acceptID
                    team <- bimap (LocalActorGroup . snd) snd <$> getSquadTeam squadID
                    return (team, grantID)
                revokes <- for (revokesG ++ revokesFL ++ revokesFR ++ revokesC) $ \ (actor, grantID) -> do
                    ext@(actionExt, _, _, _) <- prepareExtRevoke actor grantID
                    let recipByKey = LocalActorProject projectID
                    extID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return (topicActorID, sieve, revokeID, revoke, revokes, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, revokeID, (actionRevoke, localRecipsRevoke, remoteRecipsRevoke, fwdHostsRevoke), revokes, inboxItemID) -> do
                let topicByID = LocalActorProject projectID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $ do
                    sendActivity
                        topicByID topicActorID localRecipsRevoke
                        remoteRecipsRevoke fwdHostsRevoke revokeID actionRevoke
                    for_ revokes $ \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            topicByID topicActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "[Component-active] Deleted the Component, forwarded Remove, sent Revokes"

        where

        prepareMainRevoke component grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            recipHash <- encodeKeyHashid projectID
            let topicByHash = LocalActorProject recipHash

            componentHash <- bitraverse (hashComponent . bmap entityKey) pure component

            audRemover <- makeAudSenderOnly authorIdMsig
            let audChild =
                    case componentHash of
                        Left c ->
                            let a = resourceToActor $ componentResource c
                            in  AudLocal [a] [localActorFollowers a]
                        Right (Entity _ actor, ObjURI h lu) ->
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)
                audMe = AudLocal [] [localActorFollowers topicByHash]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRemover, audChild, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote

            uRemove <- getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtRevoke recipient grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            let topicByHash = LocalActorProject projectHash

            audRecip <-
                case recipient of
                    Left a -> do
                        h <- hashLocalActor a
                        return $ AudLocal [h] [localActorFollowers h]
                    Right actorID -> do
                        actor <- getJust actorID
                        ObjURI h lu <- getRemoteActorURI actor
                        return $
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRecip]

                recips = map encodeRouteHome audLocal ++ audRemote

            uRemove <- lift $ getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    removeChildActive child = do

        -- If child is local, find it in our DB
        -- If child is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        childDB <-
            bitraverse
                (\case
                    LocalActorProject j -> withDBExcept $ getEntityE j "Child not found in DB"
                    _ -> throwE "Local proposed child of non-project type"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Child @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Child isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeProject -> pure ()
                                _ -> throwE "Remote child type isn't Project"
                            return (u, actor)
                )
                child

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the sender is authorized by me to remove a child
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceProject projectID)
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            -- Verify it's an active child of mine
            sources <- lift $ case childDB of
                Left (Entity j _) ->
                    fmap (map $ \ (s, h, d, E.Value a, E.Value t, E.Value i) -> (s, h, d, Left (a, t, i))) $
                    E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send) -> do
                        E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                        E.on $ topic E.^. SourceTopicProjectTopic E.==. accept E.^. SourceThemAcceptLocalTopic
                        E.on $ holder E.^. SourceHolderProjectId E.==. topic E.^. SourceTopicProjectHolder
                        E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
                        E.where_ $
                            holder E.^. SourceHolderProjectProject E.==. E.val projectID E.&&.
                            topic E.^. SourceTopicProjectChild E.==. E.val j
                        return
                            ( source E.^. SourceId
                            , holder E.^. SourceHolderProjectId
                            , send
                            , accept E.^. SourceThemAcceptLocalId
                            , topic E.^. SourceTopicProjectTopic
                            , topic E.^. SourceTopicProjectId
                            )
                Right (_, Entity a _) ->
                    fmap (map $ \ (s, h, d, E.Value a, E.Value t) -> (s, h, d, Right (a, t))) $
                    E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send) -> do
                        E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                        E.on $ topic E.^. SourceTopicRemoteId E.==. accept E.^. SourceThemAcceptRemoteTopic
                        E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicRemoteSource
                        E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
                        E.where_ $
                            holder E.^. SourceHolderProjectProject E.==. E.val projectID E.&&.
                            topic E.^. SourceTopicRemoteTopic E.==. E.val a
                        return
                            ( source E.^. SourceId
                            , holder E.^. SourceHolderProjectId
                            , send
                            , accept E.^. SourceThemAcceptRemoteId
                            , topic E.^. SourceTopicRemoteId
                            )
            (E.Value sourceID, E.Value holderID, Entity sendID (SourceUsSendDelegator _ grantID), topic) <-
                verifySingleE sources "No source" "Multiple sources"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                -- Grab extension-Grants that I'm about to revoke
                gathers <- selectList [SourceUsGatherSource ==. sendID] []
                leafs <- selectList [SourceUsLeafSource ==. sendID] []
                conveys <- selectList [SourceUsConveySource ==. sendID] []

                -- Delete the whole Source record
                deleteWhere [SourceRemoveSend ==. sendID]
                let gatherIDs = map entityKey gathers
                deleteWhere [SourceUsGatherFromLocalGather <-. gatherIDs]
                deleteWhere [SourceUsGatherFromRemoteGather <-. gatherIDs]
                deleteWhere [SourceUsGatherId <-. gatherIDs]
                let leafIDs = map entityKey leafs
                deleteWhere [SourceUsLeafFromLocalLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafFromRemoteLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafToLocalLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafToRemoteLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafId <-. leafIDs]
                let conveyIDs = map entityKey conveys
                deleteWhere [SourceUsConveyFromLocalConvey <-. conveyIDs]
                deleteWhere [SourceUsConveyFromRemoteConvey <-. conveyIDs]
                deleteWhere [SourceUsConveyId <-. conveyIDs]
                case topic of
                    Left (localID, _, _) -> do
                        deleteWhere [SourceThemDelegateLocalSource ==. localID]
                        delete localID
                    Right (remoteID, _) -> do
                        deleteWhere [SourceThemDelegateRemoteSource ==. remoteID]
                        delete remoteID
                delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueSourceOriginUs sourceID)
                        (getKeyBy $ UniqueSourceOriginThem sourceID)
                        "Neither us nor them"
                        "Both us and them"
                case origin of
                    Left usID -> do
                        deleteBy $ UniqueSourceUsAccept usID
                        deleteBy $ UniqueSourceUsGestureLocal usID
                        deleteBy $ UniqueSourceUsGestureRemote usID
                        delete usID
                    Right themID -> do
                        deleteBy $ UniqueSourceThemGestureLocal themID
                        deleteBy $ UniqueSourceThemGestureRemote themID
                        delete themID
                case topic of
                    Left (_, l, j) -> delete j >> delete l
                    Right (_, r) -> delete r
                delete holderID
                delete sourceID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid projectID
                    let topicByHash =
                            LocalActorProject topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare main Revoke activity and insert to my outbox
                revoke@(actionRevoke, _, _, _) <-
                    lift $ prepareMainRevoke childDB grantID
                let recipByKey = LocalActorProject projectID
                revokeID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                _luRevoke <- updateOutboxItem' recipByKey revokeID actionRevoke

                -- Prepare and insert Revokes on all the extension-Grants
                revokesG <- for gathers $ \ (Entity _ (SourceUsGather _ startID grantID)) -> do
                    DestUsStart acceptID _ <- getJust startID
                    DestUsAccept destID _ <- getJust acceptID
                    parent <- do
                        p <- getDestTopic destID
                        bitraverse
                            (\case
                                Left j -> pure $ LocalActorProject j
                                Right _ -> error "I'm a project but I have a parent who is a Group"
                            )
                            pure
                            (bimap snd snd p)
                    return (parent, grantID)
                revokesL <- for leafs $ \ (Entity _ (SourceUsLeaf _ enableID grantID)) -> do
                    CollabEnable collabID _ <- getJust enableID
                    recip <- getCollabRecip collabID
                    return
                        ( bimap
                            (LocalActorPerson . collabRecipLocalPerson . entityVal)
                            (collabRecipRemoteActor . entityVal)
                            recip
                        , grantID
                        )
                revokesC <- for conveys $ \ (Entity _ (SourceUsConvey _ startID grantID)) -> do
                    SquadUsStart acceptID _ <- getJust startID
                    SquadUsAccept squadID _ <- getJust acceptID
                    team <- bimap snd snd <$> getSquadTeam squadID
                    return (first LocalActorGroup team, grantID)
                revokes <- for (revokesG ++ revokesL ++ revokesC) $ \ (actor, grantID) -> do
                    ext@(actionExt, _, _, _) <- prepareExtRevoke actor grantID
                    let recipByKey = LocalActorProject projectID
                    extID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return (projectActor project, sieve, revokeID, revoke, revokes, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, revokeID, (actionRevoke, localRecipsRevoke, remoteRecipsRevoke, fwdHostsRevoke), revokes, inboxItemID) -> do
                let topicByID = LocalActorProject projectID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $ do
                    sendActivity
                        topicByID topicActorID localRecipsRevoke
                        remoteRecipsRevoke fwdHostsRevoke revokeID actionRevoke
                    for_ revokes $ \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                        sendActivity
                            topicByID topicActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "Deleted the Child/Source, forwarded Remove, sent Revokes"

        where

        prepareMainRevoke child grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            recipHash <- encodeKeyHashid projectID
            let topicByHash = LocalActorProject recipHash

            childHash <- bitraverse (encodeKeyHashid . entityKey) pure child

            audRemover <- makeAudSenderOnly authorIdMsig
            let audChild =
                    case childHash of
                        Left j ->
                            AudLocal [LocalActorProject j] [LocalStageProjectFollowers j]
                        Right (ObjURI h lu, Entity _ actor) ->
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)
                audMe = AudLocal [] [localActorFollowers topicByHash]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRemover, audChild, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote

            uRemove <- getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

        prepareExtRevoke recipient grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            let topicByHash = LocalActorProject projectHash

            audRecip <-
                case recipient of
                    Left a -> do
                        h <- hashLocalActor a
                        return $ AudLocal [h] [localActorFollowers h]
                    Right actorID -> do
                        actor <- getJust actorID
                        ObjURI h lu <- getRemoteActorURI actor
                        return $
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRecip]

                recips = map encodeRouteHome audLocal ++ audRemote

            uRemove <- lift $ getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    removeParentActive parent = do

        -- If parent is local, find it in our DB
        -- If parent is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        parentDB <-
            bitraverse
                (\case
                    LocalActorProject j -> withDBExcept $ getEntityE j "Parent not found in DB"
                    _ -> throwE "Local proposed parent of non-project type"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Parent @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Parent isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeProject -> pure ()
                                _ -> throwE "Remote parent type isn't Project"
                            return (u, actor)
                )
                parent

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the sender is authorized by me to remove a parent
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceProject projectID)
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            -- Verify it's an active parent of mine
            dests <- lift $ case parentDB of
                Left (Entity j _) ->
                    fmap (map $ \ (d, h, a, z, E.Value l, E.Value t, E.Value s) -> (d, h, a, z, Left (l, t, s))) $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` send `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. DestUsAcceptId E.==. start E.^. DestUsStartDest
                        E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
                        E.on $ topic E.^. DestTopicProjectTopic E.==. send E.^. DestThemSendDelegatorLocalTopic
                        E.on $ holder E.^. DestHolderProjectId E.==. topic E.^. DestTopicProjectHolder
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderProjectDest
                        E.where_ $
                            holder E.^. DestHolderProjectProject E.==. E.val projectID E.&&.
                            topic E.^. DestTopicProjectParent E.==. E.val j
                        return
                            ( dest E.^. DestId
                            , holder E.^. DestHolderProjectId
                            , send E.^. DestThemSendDelegatorLocalDest
                            , start E.^. DestUsStartId
                            , topic E.^. DestTopicProjectTopic
                            , topic E.^. DestTopicProjectId
                            , send E.^. DestThemSendDelegatorLocalId
                            )
                Right (_, Entity a _) ->
                    fmap (map $ \ (d, h, a, z, E.Value t, E.Value s) -> (d, h, a, z, Right (t, s))) $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` send `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. DestUsAcceptId E.==. start E.^. DestUsStartDest
                        E.on $ dest E.^. DestId E.==. accept E.^. DestUsAcceptDest
                        E.on $ topic E.^. DestTopicRemoteId E.==. send E.^. DestThemSendDelegatorRemoteTopic
                        E.on $ dest E.^. DestId E.==. topic E.^. DestTopicRemoteDest
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderProjectDest
                        E.where_ $
                            holder E.^. DestHolderProjectProject E.==. E.val projectID E.&&.
                            topic E.^. DestTopicRemoteTopic E.==. E.val a
                        return
                            ( dest E.^. DestId
                            , holder E.^. DestHolderProjectId
                            , send E.^. DestThemSendDelegatorRemoteDest
                            , start E.^. DestUsStartId
                            , topic E.^. DestTopicRemoteId
                            , send E.^. DestThemSendDelegatorRemoteId
                            )

            (E.Value destID, E.Value holderID, E.Value usAcceptID, E.Value destStartID, topic) <-
                verifySingleE dests "No dest" "Multiple dests"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                -- Delete uses of this Dest from my Component records
                deleteWhere [ComponentGatherParent ==. destStartID]

                -- Delete uses of this Dest from my Source records
                gatherIDs <- selectKeysList [SourceUsGatherDest ==. destStartID] []
                deleteWhere [SourceUsGatherFromLocalGather <-. gatherIDs]
                deleteWhere [SourceUsGatherFromRemoteGather <-. gatherIDs]
                deleteWhere [SourceUsGatherId <-. gatherIDs]

                -- Delete the whole Dest record
                delete destStartID
                case topic of
                    Left (_, _, sendID) -> delete sendID
                    Right (_, sendID) -> delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueDestOriginUs destID)
                        (getKeyBy $ UniqueDestOriginThem destID)
                        "Neither us nor them"
                        "Both us and them"
                deleteBy $ UniqueDestUsGestureLocal destID
                deleteBy $ UniqueDestUsGestureRemote destID
                case origin of
                    Left usID -> delete usID
                    Right themID -> do
                        deleteBy $ UniqueDestThemAcceptLocal themID
                        deleteBy $ UniqueDestThemAcceptRemote themID
                        deleteBy $ UniqueDestThemGestureLocal themID
                        deleteBy $ UniqueDestThemGestureRemote themID
                        delete themID
                delete usAcceptID
                case topic of
                    Left (l, j, _) -> delete j >> delete l
                    Right (r, _) -> delete r
                delete holderID
                delete destID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid projectID
                    let topicByHash =
                            LocalActorProject topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare Accept activity
                accept@(actionAccept, _, _, _) <- prepareAccept parentDB
                let recipByKey = LocalActorProject projectID
                acceptID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                _luAccept <- updateOutboxItem' recipByKey acceptID actionAccept

                return (projectActor project, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                let topicByID = LocalActorProject projectID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $
                    sendActivity
                        topicByID topicActorID localRecipsAccept
                        remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "Deleted the Parent/Dest, forwarded Remove, sent Accept"

        where

        prepareAccept parentDB = do
            encodeRouteHome <- getEncodeRouteHome

            audRemover <- lift $ makeAudSenderOnly authorIdMsig
            audParent <-
                case parentDB of
                    Left (Entity j _) -> do
                        h <- encodeKeyHashid j
                        return $ AudLocal [LocalActorProject h] [LocalStageProjectFollowers h]
                    Right (ObjURI h lu, Entity _ ra) ->
                        return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
            audMe <-
                AudLocal [] . pure . LocalStageProjectFollowers <$>
                    encodeKeyHashid projectID
            uRemove <- lift $ getActivityURI authorIdMsig

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRemover, audParent, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uRemove
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    removeChildPassive child = do

        -- If child is local, find it in our DB
        -- If child is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        childDB <-
            bitraverse
                (\ j -> withDBExcept $ getEntityE j "Child not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Child @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Child isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeProject -> pure ()
                                _ -> throwE "Remote child type isn't Project"
                            return (u, actor)
                )
                child

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            -- Verify it's an active child of mine
            sources <- lift $ case childDB of
                Left (Entity j _) ->
                    fmap (map $ \ (s, h, d, E.Value a, E.Value t, E.Value i) -> (s, h, d, Left (a, t, i))) $
                    E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send) -> do
                        E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                        E.on $ topic E.^. SourceTopicProjectTopic E.==. accept E.^. SourceThemAcceptLocalTopic
                        E.on $ holder E.^. SourceHolderProjectId E.==. topic E.^. SourceTopicProjectHolder
                        E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
                        E.where_ $
                            holder E.^. SourceHolderProjectProject E.==. E.val projectID E.&&.
                            topic E.^. SourceTopicProjectChild E.==. E.val j
                        return
                            ( source E.^. SourceId
                            , holder E.^. SourceHolderProjectId
                            , send
                            , accept E.^. SourceThemAcceptLocalId
                            , topic E.^. SourceTopicProjectTopic
                            , topic E.^. SourceTopicProjectId
                            )
                Right (_, Entity a _) ->
                    fmap (map $ \ (s, h, d, E.Value a, E.Value t) -> (s, h, d, Right (a, t))) $
                    E.select $ E.from $ \ (source `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` send) -> do
                        E.on $ source E.^. SourceId E.==. send E.^. SourceUsSendDelegatorSource
                        E.on $ topic E.^. SourceTopicRemoteId E.==. accept E.^. SourceThemAcceptRemoteTopic
                        E.on $ source E.^. SourceId E.==. topic E.^. SourceTopicRemoteSource
                        E.on $ source E.^. SourceId E.==. holder E.^. SourceHolderProjectSource
                        E.where_ $
                            holder E.^. SourceHolderProjectProject E.==. E.val projectID E.&&.
                            topic E.^. SourceTopicRemoteTopic E.==. E.val a
                        return
                            ( source E.^. SourceId
                            , holder E.^. SourceHolderProjectId
                            , send
                            , accept E.^. SourceThemAcceptRemoteId
                            , topic E.^. SourceTopicRemoteId
                            )
            (E.Value sourceID, E.Value holderID, Entity sendID (SourceUsSendDelegator _ grantID), topic) <-
                verifySingleE sources "No source" "Multiple sources"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRemoveDB $ \ (removeID, _) -> do

                -- Record the removal attempt
                insert_ $ SourceRemove sendID removeID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid projectID
                    let topicByHash =
                            LocalActorProject topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                return (projectActor project, sieve, removeID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, inboxItemID) -> do
                let topicByID = LocalActorProject projectID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                doneDB inboxItemID "Recorded removal attempt, forwarded Remove"

    removeParentPassive parent = do

        -- If parent is local, find it in our DB
        -- If parent is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        parentDB <-
            bitraverse
                (\ j -> withDBExcept $ getEntityE j "Parent not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Parent @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Parent isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeProject -> pure ()
                                _ -> throwE "Remote parent type isn't Project"
                            return (u, actor)
                )
                parent

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            -- Verify it's an active parent of mine
            dests <- lift $ case parentDB of
                Left (Entity j _) ->
                    fmap (map $ \ (d, h, a, E.Value l, E.Value t, E.Value s) -> (d, h, a, Left (l, t, s))) $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` send) -> do
                        E.on $ topic E.^. DestTopicProjectTopic E.==. send E.^. DestThemSendDelegatorLocalTopic
                        E.on $ holder E.^. DestHolderProjectId E.==. topic E.^. DestTopicProjectHolder
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderProjectDest
                        E.where_ $
                            holder E.^. DestHolderProjectProject E.==. E.val projectID E.&&.
                            topic E.^. DestTopicProjectParent E.==. E.val j
                        return
                            ( dest E.^. DestId
                            , holder E.^. DestHolderProjectId
                            , send E.^. DestThemSendDelegatorLocalDest
                            , topic E.^. DestTopicProjectTopic
                            , topic E.^. DestTopicProjectId
                            , send E.^. DestThemSendDelegatorLocalId
                            )
                Right (_, Entity a _) ->
                    fmap (map $ \ (d, h, a, E.Value t, E.Value s) -> (d, h, a, Right (t, s))) $
                    E.select $ E.from $ \ (dest `E.InnerJoin` holder `E.InnerJoin` topic `E.InnerJoin` send) -> do
                        E.on $ topic E.^. DestTopicRemoteId E.==. send E.^. DestThemSendDelegatorRemoteTopic
                        E.on $ dest E.^. DestId E.==. topic E.^. DestTopicRemoteDest
                        E.on $ dest E.^. DestId E.==. holder E.^. DestHolderProjectDest
                        E.where_ $
                            holder E.^. DestHolderProjectProject E.==. E.val projectID E.&&.
                            topic E.^. DestTopicRemoteTopic E.==. E.val a
                        return
                            ( dest E.^. DestId
                            , holder E.^. DestHolderProjectId
                            , send E.^. DestThemSendDelegatorRemoteDest
                            , topic E.^. DestTopicRemoteId
                            , send E.^. DestThemSendDelegatorRemoteId
                            )

            (E.Value destID, E.Value holderID, E.Value usAcceptID, topic) <-
                verifySingleE dests "No dest" "Multiple dests"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                return inboxItemID

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just inboxItemID ->
                doneDB inboxItemID "Saw the removal attempt, just waiting for the Revoke"

    removeComponentPassive component = do

        -- If component is local, find it in our DB
        -- If component is remote, HTTP GET it, verify it's an actor of
        -- component type, and store in our DB (if it's already there, no need
        -- for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        componentDB <-
            bitraverse
                (\ c -> withDBExcept $ getComponentE c "Component not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Component @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Component isn't an actor"
                        Right (Just actor) -> do
                            if AP.actorTypeIsComponent $ remoteActorType $ entityVal actor
                                then pure ()
                                else throwE "Remote component type isn't of component type"
                            return (u, actor)
                )
                component

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            -- Verify it's an active component of mine
            components <- lift $ case componentDB of
                Left c ->
                    E.select $ E.from $ \ (comp `E.InnerJoin` topic `E.InnerJoin` enable) -> do
                        E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                        E.on $ comp E.^. ComponentId E.==. topic E.^. ComponentLocalComponent
                        E.where_ $
                            comp E.^. ComponentProject E.==. E.val projectID E.&&.
                            topic E.^. ComponentLocalActor E.==. E.val (localComponentID c)
                        return $ comp E.^. ComponentId
                Right (_, Entity a _) ->
                    E.select $ E.from $ \ (comp `E.InnerJoin` topic `E.InnerJoin` enable) -> do
                        E.on $ comp E.^. ComponentId E.==. enable E.^. ComponentEnableComponent
                        E.on $ comp E.^. ComponentId E.==. topic E.^. ComponentRemoteComponent
                        E.where_ $
                            comp E.^. ComponentProject E.==. E.val projectID E.&&.
                            topic E.^. ComponentRemoteActor E.==. E.val a
                        return $ comp E.^. ComponentId

            _ <- verifySingleE components "No component" "Multiple components"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                return inboxItemID

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just inboxItemID ->
                doneDB inboxItemID "[Component-passive] Saw the removal attempt, just waiting for the Revoke"

    removeTeamActive team = do

        -- If team is local, find it in our DB
        -- If team is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        teamDB <-
            bitraverse
                (\case
                    LocalActorGroup g -> withDBExcept $ getEntityE g "Team not found in DB"
                    _ -> throwE "Local proposed team of non-Group type"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Team @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Team isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote team type isn't Team"
                            return (u, actor)
                )
                team

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the sender is authorized by me to remove a team
        verifyCapability''
            uCap
            authorIdMsig
            (LocalResourceProject projectID)
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            -- Verify it's an active team of mine
            squads <- lift $ case teamDB of
                Left (Entity g _) ->
                    fmap (map $ \ (d, a, z, E.Value t, E.Value s) -> (d, a, z, Left (t, s))) $
                    E.select $ E.from $ \ (squad `E.InnerJoin` topic `E.InnerJoin` send `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. SquadUsAcceptId E.==. start E.^. SquadUsStartSquad
                        E.on $ squad E.^. SquadId E.==. accept E.^. SquadUsAcceptSquad
                        E.on $ topic E.^. SquadTopicLocalId E.==. send E.^. SquadThemSendDelegatorLocalTopic
                        E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicLocalSquad
                        E.where_ $
                            squad E.^. SquadHolder E.==. E.val (projectResource project) E.&&.
                            topic E.^. SquadTopicLocalGroup E.==. E.val g
                        return
                            ( squad E.^. SquadId
                            , send E.^. SquadThemSendDelegatorLocalSquad
                            , start E.^. SquadUsStartId
                            , topic E.^. SquadTopicLocalId
                            , send E.^. SquadThemSendDelegatorLocalId
                            )
                Right (_, Entity a _) ->
                    fmap (map $ \ (d, a, z, E.Value t, E.Value s) -> (d, a, z, Right (t, s))) $
                    E.select $ E.from $ \ (squad `E.InnerJoin` topic `E.InnerJoin` send `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. SquadUsAcceptId E.==. start E.^. SquadUsStartSquad
                        E.on $ squad E.^. SquadId E.==. accept E.^. SquadUsAcceptSquad
                        E.on $ topic E.^. SquadTopicRemoteId E.==. send E.^. SquadThemSendDelegatorRemoteTopic
                        E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicRemoteSquad
                        E.where_ $
                            squad E.^. SquadHolder E.==. E.val (projectResource project) E.&&.
                            topic E.^. SquadTopicRemoteTopic E.==. E.val a
                        return
                            ( squad E.^. SquadId
                            , send E.^. SquadThemSendDelegatorRemoteSquad
                            , start E.^. SquadUsStartId
                            , topic E.^. SquadTopicRemoteId
                            , send E.^. SquadThemSendDelegatorRemoteId
                            )

            (E.Value squadID, E.Value usAcceptID, E.Value squadStartID, topic) <-
                verifySingleE squads "No squad" "Multiple squads"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                -- Delete uses of this Squad from my Component records
                deleteWhere [ComponentConveyTeam ==. squadStartID]

                -- Delete uses of this Squad from my Source records
                conveyIDs <- selectKeysList [SourceUsConveyTeam ==. squadStartID] []
                deleteWhere [SourceUsConveyFromLocalConvey <-. conveyIDs]
                deleteWhere [SourceUsConveyFromRemoteConvey <-. conveyIDs]
                deleteWhere [SourceUsConveyId <-. conveyIDs]

                -- Delete the whole Squad record
                delete squadStartID
                case topic of
                    Left (_, sendID) -> delete sendID
                    Right (_, sendID) -> delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueSquadOriginUs squadID)
                        (getKeyBy $ UniqueSquadOriginThem squadID)
                        "Neither us nor them"
                        "Both us and them"
                deleteBy $ UniqueSquadUsGestureLocal squadID
                deleteBy $ UniqueSquadUsGestureRemote squadID
                case origin of
                    Left usID -> delete usID
                    Right themID -> do
                        deleteBy $ UniqueSquadThemAcceptLocal themID
                        deleteBy $ UniqueSquadThemAcceptRemote themID
                        deleteBy $ UniqueSquadThemGestureLocal themID
                        deleteBy $ UniqueSquadThemGestureRemote themID
                        delete themID
                delete usAcceptID
                case topic of
                    Left (l, _) -> delete l
                    Right (r, _) -> delete r
                delete squadID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid projectID
                    let topicByHash =
                            LocalActorProject topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare Accept activity
                accept@(actionAccept, _, _, _) <- prepareAccept teamDB
                let recipByKey = LocalActorProject projectID
                acceptID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                _luAccept <- updateOutboxItem' recipByKey acceptID actionAccept

                return (projectActor project, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                let topicByID = LocalActorProject projectID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $
                    sendActivity
                        topicByID topicActorID localRecipsAccept
                        remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "[Team-active] Deleted the Team/Squad, forwarded Remove, sent Accept"

        where

        prepareAccept teamDB = do
            encodeRouteHome <- getEncodeRouteHome

            audRemover <- lift $ makeAudSenderOnly authorIdMsig
            audTeam <-
                case teamDB of
                    Left (Entity g _) -> do
                        h <- encodeKeyHashid g
                        return $ AudLocal [LocalActorGroup h] [LocalStageGroupFollowers h]
                    Right (ObjURI h lu, Entity _ ra) ->
                        return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
            audMe <-
                AudLocal [] . pure . LocalStageProjectFollowers <$>
                    encodeKeyHashid projectID
            uRemove <- lift $ getActivityURI authorIdMsig

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRemover, audTeam, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uRemove
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    removeTeamPassive team = do

        -- If team is local, find it in our DB
        -- If team is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        teamDB <-
            bitraverse
                (\ g -> withDBExcept $ getEntityE g "Team not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Team @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Team isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote team type isn't Team"
                            return (u, actor)
                )
                team

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            -- Verify it's an active team of mine
            squads <- lift $ case teamDB of
                Left (Entity g _) ->
                    fmap (map $ \ (d, a, z, E.Value t, E.Value s) -> (d, a, z, Left (t, s))) $
                    E.select $ E.from $ \ (squad `E.InnerJoin` topic `E.InnerJoin` send `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. SquadUsAcceptId E.==. start E.^. SquadUsStartSquad
                        E.on $ squad E.^. SquadId E.==. accept E.^. SquadUsAcceptSquad
                        E.on $ topic E.^. SquadTopicLocalId E.==. send E.^. SquadThemSendDelegatorLocalTopic
                        E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicLocalSquad
                        E.where_ $
                            squad E.^. SquadHolder E.==. E.val (projectResource project) E.&&.
                            topic E.^. SquadTopicLocalGroup E.==. E.val g
                        return
                            ( squad E.^. SquadId
                            , send E.^. SquadThemSendDelegatorLocalSquad
                            , start E.^. SquadUsStartId
                            , topic E.^. SquadTopicLocalId
                            , send E.^. SquadThemSendDelegatorLocalId
                            )
                Right (_, Entity a _) ->
                    fmap (map $ \ (d, a, z, E.Value t, E.Value s) -> (d, a, z, Right (t, s))) $
                    E.select $ E.from $ \ (squad `E.InnerJoin` topic `E.InnerJoin` send `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. SquadUsAcceptId E.==. start E.^. SquadUsStartSquad
                        E.on $ squad E.^. SquadId E.==. accept E.^. SquadUsAcceptSquad
                        E.on $ topic E.^. SquadTopicRemoteId E.==. send E.^. SquadThemSendDelegatorRemoteTopic
                        E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicRemoteSquad
                        E.where_ $
                            squad E.^. SquadHolder E.==. E.val (projectResource project) E.&&.
                            topic E.^. SquadTopicRemoteTopic E.==. E.val a
                        return
                            ( squad E.^. SquadId
                            , send E.^. SquadThemSendDelegatorRemoteSquad
                            , start E.^. SquadUsStartId
                            , topic E.^. SquadTopicRemoteId
                            , send E.^. SquadThemSendDelegatorRemoteId
                            )

            (E.Value squadID, E.Value usAcceptID, E.Value squadStartID, topic) <-
                verifySingleE squads "No squad" "Multiple squads"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                return inboxItemID

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just inboxItemID ->
                doneDB inboxItemID "[Team-passive] Saw the removal attempt, just waiting for the Revoke"

-- Meaning: An actor is revoking Grant activities
-- Behavior:
--  * For each revoked activity:
--      * If it's a parent revoking a delegator-Grant it gave me:
--          * Delete the whole Dest record
--          * Forward the Revoke to my followers
--          * Send Accept to parent+followers & my followers
--      * If it's a child revoking a Grant it had extended to me:
--          * Delete that extension from my Source record
--          * For each further extension I did on that Grant (to a
--            parent/collab/team), send a Revoke
--      * If it's a team revoking a delegator-Grant it gave me:
--          * Delete the whole Squad record
--          * Forward the Revoke to my followers
--          * Send Accept to team+followers & my followers
--      * If it's a component revoking a Grant it had extended to me:
--          * Delete the whole Component record
--          * For each further extension I did on that Grant (to a
--            parent/collab/team), send a Revoke
projectRevoke
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Revoke URIMode
    -> ActE (Text, Act (), Next)
projectRevoke now projectID (Verse authorIdMsig body) (AP.Revoke (luFirst :| lusRest)) = do

    ObjURI h _ <- lift $ getActorURI authorIdMsig
    parseRevoked <- do
        hl <- hostIsLocal h
        return $
            \ lu ->
                if hl
                    then
                        Left . (\ (a, _, i) -> (a, i)) <$>
                            parseLocalActivityURI' lu
                    else pure $ Right lu
    revokedFirst <- parseRevoked luFirst
    revokedRest <- traverse parseRevoked lusRest

    mode <- withDBExcept $ do

        revokedFirstDB <- do
            a <- getActivity $ second (ObjURI h) revokedFirst
            fromMaybeE a "Can't find revoked in DB"

        meResourceID <- lift $ projectResource <$> getJust projectID

        let adapt = maybe (Right Nothing) (either Left (Right . Just))
        maybeMode <-
            ExceptT $ fmap adapt $ runMaybeT $
                runExceptT (Left . Left <$> tryParent revokedFirstDB) <|>
                runExceptT (Left . Right <$> tryChild revokedFirstDB) <|>
                runExceptT (Right . Left <$> tryTeam meResourceID revokedFirstDB) <|>
                runExceptT (Right . Right <$> tryComponent revokedFirstDB)
        fromMaybeE
            maybeMode
            "Revoked activity isn't a relevant Grant I'm aware of"

    case mode of
        Left (Left p) -> revokeParent revokedRest p
        Left (Right c) -> revokeChild revokedRest c
        Right (Left t) -> revokeTeam revokedRest t
        Right (Right c) -> revokeComponent revokedRest c

    where

    verifyDestHolder :: DestId -> MaybeT ActDB ()
    verifyDestHolder destID = do
        DestHolderProject _ j <- MaybeT $ getValBy $ UniqueDestHolderProject destID
        guard $ j == projectID

    tryParent' usAcceptID send = do
        DestUsAccept destID _ <- lift $ lift $ getJust usAcceptID
        lift $ verifyDestHolder destID
        topic <- do
            t <- lift . lift $ getDestTopic destID
            bitraverse
                (\ (l, k) ->
                    case k of
                        Left j -> pure (l, j)
                        Right _ -> error "Project Dest topic is a Group, impossible"
                )
                pure
                t
        return (destID, usAcceptID, topic, send)

    tryParent (Left (_actorByKey, _actorEntity, itemID)) = do
        Entity sendID (DestThemSendDelegatorLocal usAcceptID _localID _) <-
            lift $ MaybeT $ getBy $ UniqueDestThemSendDelegatorLocalGrant itemID
        tryParent' usAcceptID (Left sendID) --(Left localID)
    tryParent (Right remoteActivityID) = do
        Entity sendID (DestThemSendDelegatorRemote usAcceptID _remoteID _) <-
            lift $ MaybeT $ getBy $ UniqueDestThemSendDelegatorRemoteGrant remoteActivityID
        tryParent' usAcceptID (Right sendID) --(Right remoteID)

    verifySourceHolder :: SourceId -> MaybeT ActDB ()
    verifySourceHolder sourceID = do
        SourceHolderProject _ j <- MaybeT $ getValBy $ UniqueSourceHolderProject sourceID
        guard $ j == projectID

    tryChild' sourceID child = do
        lift $ verifySourceHolder sourceID
        sendID <- lift $ MaybeT $ getKeyBy $ UniqueSourceUsSendDelegator sourceID
        return (sendID, child)

    tryChild (Left (_actorByKey, _actorEntity, itemID)) = do
        Entity delegID (SourceThemDelegateLocal themAcceptID _) <-
            lift $ MaybeT $ getBy $ UniqueSourceThemDelegateLocal itemID
        SourceThemAcceptLocal topicID _ <- lift $ lift $ getJust themAcceptID
        SourceTopicLocal sourceID <- lift $ lift $ getJust topicID
        SourceTopicProject _ _ j <- do
            mj <- lift $ lift $ getValBy $ UniqueSourceTopicProjectTopic topicID
            fromMaybeE mj "The parent to whom this revoked Grant was sent isn't a Project"
        tryChild' sourceID $ Left (topicID, j, delegID, themAcceptID)
    tryChild (Right remoteActivityID) = do
        Entity delegID (SourceThemDelegateRemote themAcceptID _) <-
            lift $ MaybeT $ getBy $ UniqueSourceThemDelegateRemote remoteActivityID
        SourceThemAcceptRemote topicID _ <- lift $ lift $ getJust themAcceptID
        SourceTopicRemote sourceID actorID <- lift $ lift $ getJust topicID
        tryChild' sourceID $ Right (topicID, actorID, delegID, themAcceptID)

    verifySquadHolder :: ResourceId -> SquadId -> MaybeT ActDB ()
    verifySquadHolder meResourceID squadID = do
        Squad _ resourceID <- lift $ getJust squadID
        guard $ resourceID == meResourceID

    tryTeam' meResourceID usAcceptID send = do
        SquadUsAccept squadID _ <- lift $ lift $ getJust usAcceptID
        lift $ verifySquadHolder meResourceID squadID
        topic <- lift . lift $ getSquadTeam squadID
        return (squadID, usAcceptID, topic, send)

    tryTeam r (Left (_actorByKey, _actorEntity, itemID)) = do
        Entity sendID (SquadThemSendDelegatorLocal usAcceptID _localID _) <-
            lift $ MaybeT $ getBy $ UniqueSquadThemSendDelegatorLocalGrant itemID
        tryTeam' r usAcceptID (Left sendID) --(Left localID)
    tryTeam r (Right remoteActivityID) = do
        Entity sendID (SquadThemSendDelegatorRemote usAcceptID _remoteID _) <-
            lift $ MaybeT $ getBy $ UniqueSquadThemSendDelegatorRemoteGrant remoteActivityID
        tryTeam' r usAcceptID (Right sendID) --(Right remoteID)

    verifyComponentHolder :: ComponentId -> MaybeT ActDB ()
    verifyComponentHolder componentID = do
        Component j _ <- lift $ getJust componentID
        guard $ j == projectID

    tryComponent' componentID component = do
        lift $ verifyComponentHolder componentID
        enableID <- lift $ MaybeT $ getKeyBy $ UniqueComponentEnable componentID
        return (componentID, enableID, component)

    tryComponent (Left (_actorByKey, _actorEntity, itemID)) = do
        Entity delegID (ComponentDelegateLocal topicID _) <-
            lift $ MaybeT $ getBy $ UniqueComponentDelegateLocalGrant itemID
        ComponentLocal componentID komponentID <- lift $ lift $ getJust topicID
        compByKey <- lift $ lift $ getLocalComponent komponentID
        tryComponent' componentID $ Left (topicID, compByKey, komponentID, delegID)
    tryComponent (Right remoteActivityID) = do
        Entity delegID (ComponentDelegateRemote topicID _) <-
            lift $ MaybeT $ getBy $ UniqueComponentDelegateRemoteGrant remoteActivityID
        ComponentRemote componentID actorID <- lift $ lift $ getJust topicID
        tryComponent' componentID $ Right (topicID, actorID, delegID)

    revokeParent revokedRest (destID, usAcceptID, parent, send) = do

        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        unless (author == bimap (LocalActorProject . snd) snd parent) $
            throwE "Sender isn't the parent Project the revoked Grant came from"

        unless (null revokedRest) $
            throwE "Parent revoking the delegator-Grant and something more"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            maybeRevokeDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRevokeDB $ \ (inboxItemID, _revokeDB) -> do

                maybeStartID <- getKeyBy $ UniqueDestUsStart usAcceptID

                -- Delete uses of this Dest from my Component records
                for_ maybeStartID $ \ destStartID ->
                    deleteWhere [ComponentGatherParent ==. destStartID]

                -- Delete uses of this Dest from my Source records
                for_ maybeStartID $ \ destStartID -> do
                    gatherIDs <- selectKeysList [SourceUsGatherDest ==. destStartID] []
                    deleteWhere [SourceUsGatherFromLocalGather <-. gatherIDs]
                    deleteWhere [SourceUsGatherFromRemoteGather <-. gatherIDs]
                    deleteWhere [SourceUsGatherId <-. gatherIDs]

                -- Delete the whole Dest record
                for_ maybeStartID delete
                case send of
                    Left sendID -> delete sendID
                    Right sendID -> delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueDestOriginUs destID)
                        (getKeyBy $ UniqueDestOriginThem destID)
                        "Neither us nor them"
                        "Both us and them"
                deleteBy $ UniqueDestUsGestureLocal destID
                deleteBy $ UniqueDestUsGestureRemote destID
                case origin of
                    Left usID -> delete usID
                    Right themID -> do
                        deleteBy $ UniqueDestThemAcceptLocal themID
                        deleteBy $ UniqueDestThemAcceptRemote themID
                        deleteBy $ UniqueDestThemGestureLocal themID
                        deleteBy $ UniqueDestThemGestureRemote themID
                        delete themID
                delete usAcceptID
                case parent of
                    Left (l, _j) -> do
                        deleteBy $ UniqueDestTopicProjectTopic l
                        delete l
                    Right (r, _) -> delete r
                deleteBy $ UniqueDestHolderProject destID
                delete destID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid projectID
                    let topicByHash =
                            LocalActorProject topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare Accept activity
                accept@(actionAccept, _, _, _) <- prepareAccept
                let recipByKey = LocalActorProject projectID
                acceptID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                _luAccept <- updateOutboxItem' recipByKey acceptID actionAccept

                return (projectActor project, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                let topicByID = LocalActorProject projectID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $
                    sendActivity
                        topicByID topicActorID localRecipsAccept
                        remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "Deleted the Parent/Dest, forwarded Revoke, sent Accept"

        where

        prepareAccept = do
            encodeRouteHome <- getEncodeRouteHome

            audParent <- makeAudSenderWithFollowers authorIdMsig
            audMe <-
                AudLocal [] . pure . LocalStageProjectFollowers <$>
                    encodeKeyHashid projectID
            uRevoke <- lift $ getActivityURI authorIdMsig

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audParent, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRevoke]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uRevoke
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    revokeChild revokedRest (sendID, child) = do

        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        unless (author == bimap (LocalActorProject . view _2) (view _2) child) $
            throwE "Sender isn't the child Project the revoked Grant came from"

        unless (null revokedRest) $
            throwE "Child revoking the start/extension-Grant and something more"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            maybeRevokeDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRevokeDB $ \ (inboxItemID, _revokeDB) -> do

                -- Collect the extensions I'll need to revoke
                gatherIDs <-
                    case child of
                        Left (_, _, delegID, _) ->
                            map (sourceUsGatherFromLocalGather . entityVal) <$>
                                selectList [SourceUsGatherFromLocalFrom ==. delegID] []
                        Right (_, _, delegID, _) ->
                            map (sourceUsGatherFromRemoteGather . entityVal) <$>
                                selectList [SourceUsGatherFromRemoteFrom ==. delegID] []
                gathers <- selectList [SourceUsGatherId <-. gatherIDs] []
                leafIDs <-
                    case child of
                        Left (_, _, delegID, _) ->
                            map (sourceUsLeafFromLocalLeaf . entityVal) <$>
                                selectList [SourceUsLeafFromLocalFrom ==. delegID] []
                        Right (_, _, delegID, _) ->
                            map (sourceUsLeafFromRemoteLeaf . entityVal) <$>
                                selectList [SourceUsLeafFromRemoteFrom ==. delegID] []
                leafs <- selectList [SourceUsLeafId <-. leafIDs] []

                -- Delete the records of these extensions
                deleteWhere [SourceUsGatherFromLocalGather <-. gatherIDs]
                deleteWhere [SourceUsGatherFromRemoteGather <-. gatherIDs]
                deleteWhere [SourceUsGatherId <-. gatherIDs]
                deleteWhere [SourceUsLeafFromLocalLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafFromRemoteLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafToLocalLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafToRemoteLeaf <-. leafIDs]
                deleteWhere [SourceUsLeafId <-. leafIDs]
                case child of
                    Left (_, _, delegID, _) -> delete delegID
                    Right (_, _, delegID, _) -> delete delegID

                -- Prepare and insert Revokes on all the extension-Grants
                revokesG <- for gathers $ \ (Entity _ (SourceUsGather _ startID grantID)) -> do
                    DestUsStart acceptID _ <- getJust startID
                    DestUsAccept destID _ <- getJust acceptID
                    parent <- do
                        p <- getDestTopic destID
                        bitraverse
                            (\case
                                Left j -> pure $ LocalActorProject j
                                Right _ -> error "I'm a project but I have a parent who is a Group"
                            )
                            pure
                            (bimap snd snd p)
                    return (parent, grantID)
                revokesL <- for leafs $ \ (Entity _ (SourceUsLeaf _ enableID grantID)) -> do
                    CollabEnable collabID _ <- getJust enableID
                    recip <- getCollabRecip collabID
                    return
                        ( bimap
                            (LocalActorPerson . collabRecipLocalPerson . entityVal)
                            (collabRecipRemoteActor . entityVal)
                            recip
                        , grantID
                        )
                revokes <- for (revokesG ++ revokesL) $ \ (actor, grantID) -> do
                    ext@(actionExt, _, _, _) <- prepareExtRevoke actor grantID
                    let recipByKey = LocalActorProject projectID
                    extID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return (projectActor project, revokes, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, revokes, inboxItemID) -> do
                let topicByID = LocalActorProject projectID
                lift $ for_ revokes $ \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                    sendActivity
                        topicByID topicActorID localRecipsExt
                        remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "Deleted the SourceThemDelegate* record, sent Revokes"

        where

        prepareExtRevoke recipient grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            let topicByHash = LocalActorProject projectHash

            audRecip <-
                case recipient of
                    Left a -> do
                        h <- hashLocalActor a
                        return $ AudLocal [h] [localActorFollowers h]
                    Right actorID -> do
                        actor <- getJust actorID
                        ObjURI h lu <- getRemoteActorURI actor
                        return $
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRecip]

                recips = map encodeRouteHome audLocal ++ audRemote

            uRevoke <- lift $ getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRevoke]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    revokeTeam revokedRest (squadID, usAcceptID, team, send) = do

        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        unless (author == bimap (LocalActorGroup . snd) snd team) $
            throwE "Sender isn't the Team the revoked Grant came from"

        unless (null revokedRest) $
            throwE "Team revoking the delegator-Grant and something more"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            maybeRevokeDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRevokeDB $ \ (inboxItemID, _revokeDB) -> do

                maybeStartID <- getKeyBy $ UniqueSquadUsStart usAcceptID

                -- Delete uses of this Squad from my Component records
                for_ maybeStartID $ \ squadStartID ->
                    deleteWhere [ComponentConveyTeam ==. squadStartID]

                -- Delete uses of this Squad from my Source records
                for_ maybeStartID $ \ squadStartID -> do
                    conveyIDs <- selectKeysList [SourceUsConveyTeam ==. squadStartID] []
                    deleteWhere [SourceUsConveyFromLocalConvey <-. conveyIDs]
                    deleteWhere [SourceUsConveyFromRemoteConvey <-. conveyIDs]
                    deleteWhere [SourceUsConveyId <-. conveyIDs]

                -- Delete the whole Squad record
                for_ maybeStartID delete
                case send of
                    Left sendID -> delete sendID
                    Right sendID -> delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueSquadOriginUs squadID)
                        (getKeyBy $ UniqueSquadOriginThem squadID)
                        "Neither us nor them"
                        "Both us and them"
                deleteBy $ UniqueSquadUsGestureLocal squadID
                deleteBy $ UniqueSquadUsGestureRemote squadID
                case origin of
                    Left usID -> delete usID
                    Right themID -> do
                        deleteBy $ UniqueSquadThemAcceptLocal themID
                        deleteBy $ UniqueSquadThemAcceptRemote themID
                        deleteBy $ UniqueSquadThemGestureLocal themID
                        deleteBy $ UniqueSquadThemGestureRemote themID
                        delete themID
                delete usAcceptID
                case team of
                    Left (l, _) -> delete l
                    Right (r, _) -> delete r
                delete squadID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid projectID
                    let topicByHash =
                            LocalActorProject topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare Accept activity
                accept@(actionAccept, _, _, _) <- prepareAccept
                let recipByKey = LocalActorProject projectID
                acceptID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                _luAccept <- updateOutboxItem' recipByKey acceptID actionAccept

                return (projectActor project, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                let topicByID = LocalActorProject projectID
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $
                    sendActivity
                        topicByID topicActorID localRecipsAccept
                        remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "Deleted the Team/Squad, forwarded Revoke, sent Accept"

        where

        prepareAccept = do
            encodeRouteHome <- getEncodeRouteHome

            audTeam <- makeAudSenderWithFollowers authorIdMsig
            audMe <-
                AudLocal [] . pure . LocalStageProjectFollowers <$>
                    encodeKeyHashid projectID
            uRevoke <- lift $ getActivityURI authorIdMsig

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audTeam, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRevoke]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uRevoke
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    revokeComponent revokedRest (componentID, enableID, component) = do

        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        unless (author == bimap (resourceToActor . componentResource . view _2) (view _2) component) $
            throwE "Sender isn't the component the revoked Grant came from"

        unless (null revokedRest) $
            throwE "Component revoking the start-Grant and something more"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            (project, actorRecip) <- lift $ do
                p <- getJust projectID
                (p,) <$> getJust (projectActor p)

            maybeRevokeDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
            lift $ for maybeRevokeDB $ \ (inboxItemID, _revokeDB) -> do

                -- Collect the extensions I'll need to revoke
                furthersL <- selectList [ComponentFurtherLocalComponent ==. enableID] []
                furthersR <- selectList [ComponentFurtherRemoteComponent ==. enableID] []
                gathers <- selectList [ComponentGatherComponent ==. enableID] []
                conveys <- selectList [ComponentConveyComponent ==. enableID] []

                -- Delete the records of these extensions
                deleteWhere [ComponentFurtherLocalComponent ==. enableID]
                deleteWhere [ComponentFurtherRemoteComponent ==. enableID]
                deleteWhere [ComponentGatherComponent ==. enableID]
                deleteWhere [ComponentConveyComponent ==. enableID]
                case component of
                    Left (_, _, _, delegID) -> delete delegID
                    Right (_, _, delegID) -> delete delegID

                -- Delete the whole Component record
                usOrThem <-
                    requireEitherAlt
                        (getKeyBy $ UniqueComponentOriginInvite componentID)
                        (getKeyBy $ UniqueComponentOriginAdd componentID)
                        "Neither us nor them"
                        "Both us and them"
                delete enableID
                case usOrThem of
                    Left usID -> do
                        deleteBy $ UniqueComponentProjectAccept usID
                        delete usID
                    Right themID -> do
                        deleteBy $ UniqueComponentGestureLocal themID
                        deleteBy $ UniqueComponentGestureRemote themID
                        delete themID
                deleteBy $ UniqueComponentProjectGestureLocal componentID
                deleteBy $ UniqueComponentProjectGestureRemote componentID
                case component of
                    Left (localID, _, _, _) -> do
                        deleteBy $ UniqueComponentAcceptLocal localID
                        delete localID
                    Right (remoteID, _, _) -> do
                        deleteBy $ UniqueComponentAcceptRemote remoteID
                        delete remoteID
                delete componentID

                -- Prepare and insert Revokes on all the extension-Grants
                revokesFL <- for furthersL $ \ (Entity _ (ComponentFurtherLocal _ delegID grantID)) -> do
                    CollabDelegLocal _ recipID _ <- getJust delegID
                    CollabRecipLocal _ personID <- getJust recipID
                    return (Left $ LocalActorPerson personID, grantID)
                revokesFR <- for furthersR $ \ (Entity _ (ComponentFurtherRemote _ delegID grantID)) -> do
                    CollabDelegRemote _ recipID _ <- getJust delegID
                    CollabRecipRemote _ actorID <- getJust recipID
                    return (Right actorID, grantID)
                revokesG <- for gathers $ \ (Entity _ (ComponentGather _ startID grantID)) -> do
                    DestUsStart acceptID _ <- getJust startID
                    DestUsAccept destID _ <- getJust acceptID
                    parent <- do
                        p <- bimap snd snd <$> getDestTopic destID
                        bitraverse
                            (\case
                                Left j -> pure $ LocalActorProject j
                                Right _ -> error "I'm a project but I have a parent who is a Group"
                            )
                            pure
                            p
                    return (parent, grantID)
                revokesC <- for conveys $ \ (Entity _ (ComponentConvey _ startID grantID)) -> do
                    SquadUsStart acceptID _ <- getJust startID
                    SquadUsAccept squadID _ <- getJust acceptID
                    team <- bimap snd snd <$> getSquadTeam squadID
                    return (first LocalActorGroup team, grantID)
                revokes <- for (revokesFL ++ revokesFR ++ revokesG ++ revokesC) $ \ (actor, grantID) -> do
                    ext@(actionExt, _, _, _) <- prepareExtRevoke actor grantID
                    let recipByKey = LocalActorProject projectID
                    extID <- insertEmptyOutboxItem' (actorOutbox actorRecip) now
                    _luExt <- updateOutboxItem' recipByKey extID actionExt
                    return (extID, ext)

                return (projectActor project, revokes, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, revokes, inboxItemID) -> do
                let topicByID = LocalActorProject projectID
                lift $ for_ revokes $ \ (extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt)) ->
                    sendActivity
                        topicByID topicActorID localRecipsExt
                        remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "Deleted the Component record, sent Revokes"

        where

        prepareExtRevoke recipient grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            projectHash <- encodeKeyHashid projectID
            let topicByHash = LocalActorProject projectHash

            audRecip <-
                case recipient of
                    Left a -> do
                        h <- hashLocalActor a
                        return $ AudLocal [h] [localActorFollowers h]
                    Right actorID -> do
                        actor <- getJust actorID
                        ObjURI h lu <- getRemoteActorURI actor
                        return $
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRecip]

                recips = map encodeRouteHome audLocal ++ audRemote

            uRevoke <- lift $ getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRevoke]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor is undoing some previous action
-- Behavior:
--      * If they're undoing their Following of me:
--          * Record it in my DB
--          * Publish and send an Accept only to the sender
--      * Otherwise respond with an error
projectUndo
    :: UTCTime
    -> ProjectId
    -> Verse
    -> AP.Undo URIMode
    -> ActE (Text, Act (), Next)
projectUndo now recipProjectID (Verse authorIdMsig body) (AP.Undo uObject) = do

    -- Check input
    undone <-
        first (\ (actor, _, item) -> (actor, item)) <$>
            parseActivityURI' uObject

    -- Verify the capability URI, if provided, is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCapability <-
        for (AP.activityCapability $ actbActivity body) $ \ uCap ->
            nameExceptT "Undo capability" $
                first (\ (actor, _, item) -> (actor, item)) <$>
                    parseActivityURI' uCap

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        (projectRecip, actorRecip) <- lift $ do
            p <- getJust recipProjectID
            (p,) <$> getJust (projectActor p)

        -- Insert the Undo to my inbox
        mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actorRecip) False
        for mractid $ \ (inboxItemID, _undoDB) -> do

            maybeUndo <- runMaybeT $ do

                -- Find the undone activity in our DB
                undoneDB <- MaybeT $ getActivity undone

                let followers = actorFollowers actorRecip
                asum
                    [ tryUnfollow followers undoneDB authorIdMsig
                    ]

            (sieve, audience) <-
                fromMaybeE
                    maybeUndo
                    "Undone activity isn't a Follow related to me"

            -- Prepare an Accept activity and insert to project's outbox
            acceptID <- lift $ insertEmptyOutboxItem' (actorOutbox actorRecip) now
            accept@(actionAccept, _, _, _) <- lift $ lift $ prepareAccept audience
            _luAccept <- lift $ updateOutboxItem' (LocalActorProject recipProjectID) acceptID actionAccept

            return (projectActor projectRecip, sieve, acceptID, accept, inboxItemID)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (actorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
            forwardActivity
                authorIdMsig body (LocalActorProject recipProjectID) actorID sieve
            lift $ sendActivity
                (LocalActorProject recipProjectID) actorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            doneDB inboxItemID
                "Undid the Follow, forwarded the Undo and published Accept"

    where

    tryUnfollow projectFollowersID (Left (_actorByKey, _actorE, outboxItemID)) (Left (_, actorID, _)) = do
        Entity followID follow <-
            MaybeT $ lift $ getBy $ UniqueFollowFollow outboxItemID
        let followerID = followActor follow
            followerSetID = followTarget follow
        verifyTargetMe followerSetID
        unless (followerID == actorID) $
            lift $ throwE "You're trying to Undo someone else's Follow"
        lift $ lift $ delete followID
        audSenderOnly <- lift $ lift $ lift $ makeAudSenderOnly authorIdMsig
        return (makeRecipientSet [] [], [audSenderOnly])
        where
        verifyTargetMe followerSetID = guard $ followerSetID == projectFollowersID
    tryUnfollow projectFollowersID (Right remoteActivityID) (Right (author, _, _)) = do
        Entity remoteFollowID remoteFollow <-
            MaybeT $ lift $ getBy $ UniqueRemoteFollowFollow remoteActivityID
        let followerID = remoteFollowActor remoteFollow
            followerSetID = remoteFollowTarget remoteFollow
        verifyTargetMe followerSetID
        unless (followerID == remoteAuthorId author) $
            lift $ throwE "You're trying to Undo someone else's Follow"
        lift $ lift $ delete remoteFollowID
        audSenderOnly <- lift $ lift $ lift $ makeAudSenderOnly authorIdMsig
        return (makeRecipientSet [] [], [audSenderOnly])
        where
        verifyTargetMe followerSetID = guard $ followerSetID == projectFollowersID
    tryUnfollow _ _ _ = mzero

    prepareAccept audience = do
        encodeRouteHome <- getEncodeRouteHome

        uUndo <- getActivityURI authorIdMsig
        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = []
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uUndo
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

projectVerse :: ProjectId -> Verse -> ActE (Text, Act (), Next)
projectVerse projectID verse@(Verse _authorIdMsig body) = do
    now <- liftIO getCurrentTime
    case AP.activitySpecific $ actbActivity body of
        AP.AcceptActivity accept -> projectAccept now projectID verse accept
        AP.AddActivity add       -> projectAdd now projectID verse add
        AP.FollowActivity follow -> projectFollow now projectID verse follow
        AP.GrantActivity grant   -> projectGrant now projectID verse grant
        AP.InviteActivity invite -> projectInvite now projectID verse invite
        AP.JoinActivity join     -> projectJoin now projectID verse join
        AP.RejectActivity reject -> projectReject now projectID verse reject
        AP.RemoveActivity remove -> projectRemove now projectID verse remove
        AP.RevokeActivity revoke -> projectRevoke now projectID verse revoke
        AP.UndoActivity undo     -> projectUndo now projectID verse undo
        _ -> throwE "Unsupported activity type for Project"

instance ActorLaunch Project where
    actorBehavior _ =
        (handleMethod @"verse" := \ projectID verse -> adaptHandlerResult $ do
            errboxID <- lift $ withDB $ do
                resourceID <- projectResource <$> getJust projectID
                Resource actorID <- getJust resourceID
                actorErrbox <$> getJust actorID
            adaptErrbox errboxID False (projectVerse projectID) verse
        )
        `HCons`
        (handleMethod @"init" := \ projectID creator -> adaptHandlerResult $ do
            now <- liftIO getCurrentTime
            let grabResource = pure . projectResource
            topicInit @Project @() AP.ActorTypeProject (\ _ _ -> pure ()) grabResource LocalResourceProject now projectID creator Nothing
        )
        `HCons`
        HNil
