{- This file is part of Vervis.
 -
 - Written in 2019, 2020, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Vervis.Actor.Common
    ( actorFollow
    , actorFollow'
    , topicAccept
    , topicReject
    , componentInvite
    , componentRemove
    , topicJoin
    , topicCreateMe
    , topicInit
    , componentGrant
    , componentAdd
    , componentRevoke
    )
where

import Control.Applicative
import Control.Exception.Base
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Aeson (FromJSON)
import Data.Barbie
import Data.Bifoldable
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Either
import Data.Foldable
import Data.List.NonEmpty (NonEmpty (..))
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Network.HTTP.Client hiding (Proxy, proxy)
import Network.HTTP.Client.Conduit.F3
import Optics.Core
import Yesod.Persist.Core

import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Control.Concurrent.Actor hiding (Actor)
import Network.FedURI
import Web.Actor
import Web.Actor.Persist
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Database.Persist.Local

import Vervis.Access
import Vervis.ActivityPub
import Vervis.Actor
import Vervis.Actor2
import Vervis.Cloth
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Data.Discussion
import Vervis.FedURI

import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.Foundation
import Vervis.Model
import Vervis.Model.Ident
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Persist.Discussion
import Vervis.Recipient (makeRecipientSet, LocalStageBy (..), Aud (..), collectAudience, localActorFollowers, renderLocalActor)
import Vervis.RemoteActorStore
import Vervis.Settings
import Vervis.Ticket
import Vervis.Web.Collab

data GrantKind
    = GKDelegationStart AP.Role
    | GKDelegationExtend AP.Role (Either (LocalActorBy Key) FedURI)
    | GKDelegator

actorFollow
    :: (PersistRecordBackend r SqlBackend, ToBackendKey SqlBackend r)
    => (Route App -> ActE a)
    -> (r -> ActorId)
    -> Bool
    -> (Actor -> a -> ActDBE FollowerSetId)
    -> (a -> ActDB RecipientRoutes)
    -> (forall f. f r -> LocalActorBy f)
    -> (a -> Act [Aud URIMode])
    -> UTCTime
    -> Key r
    -> Verse
    -> AP.Follow URIMode
    -> ActE (Text, Act (), Next)
actorFollow parseFollowee grabActor =
    actorFollow' parseFollowee (pure . grabActor)

actorFollow'
    :: (PersistRecordBackend r SqlBackend, ToBackendKey SqlBackend r)
    => (Route App -> ActE a)
    -> (r -> ActDB ActorId)
    -> Bool
    -> (Actor -> a -> ActDBE FollowerSetId)
    -> (a -> ActDB RecipientRoutes)
    -> (forall f. f r -> LocalActorBy f)
    -> (a -> Act [Aud URIMode])
    -> UTCTime
    -> Key r
    -> Verse
    -> AP.Follow URIMode
    -> ActE (Text, Act (), Next)
actorFollow' parseFollowee grabActor unread getFollowee getSieve makeLocalActor makeAudience now recipID (Verse authorIdMsig body) (AP.Follow uObject _ hide) = do

    -- Check input
    followee <- nameExceptT "Follow object" $ do
        route <- do
            routeOrRemote <- parseFedURI uObject
            case routeOrRemote of
                Left route -> pure route
                Right _ -> throwE "Remote, so definitely not me/mine"
        -- Verify the followee is me or a subobject of mine
        parseFollowee route
    verifyNothingE
        (AP.activityCapability $ actbActivity body)
        "Capability not needed"

    maybeFollow <- withDBExcept $ do

        -- Find me in DB
        recip <- lift $ getJust recipID
        recipActorID <- lift $ grabActor recip
        recipActor <- lift $ getJust recipActorID

        -- Insert the Follow to my inbox
        maybeFollowDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) unread
        for maybeFollowDB $ \ (inboxItemID, followDB) -> do

            -- Find followee in DB
            followerSetID <- getFollowee recipActor followee

            -- Verify not already following me
            case followDB of
                Left (_, followerID, followID) -> do
                    maybeFollow <- lift $ getBy $ UniqueFollow followerID followerSetID
                    verifyNothingE maybeFollow "You're already following this object"
                Right (author, _, followID) -> do
                    let followerID = remoteAuthorId author
                    maybeFollow <- lift $ getBy $ UniqueRemoteFollow followerID followerSetID
                    verifyNothingE maybeFollow "You're already following this object"

            -- Record the new follow in DB
            acceptID <-
                lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
            lift $ case followDB of
                Left (_actorByKey, actorID, followID) ->
                    insert_ $ Follow actorID followerSetID (not hide) followID acceptID
                Right (author, _luFollow, followID) -> do
                    let authorID = remoteAuthorId author
                    insert_ $ RemoteFollow authorID followerSetID (not hide) followID acceptID

            -- Prepare an Accept activity and insert to actor's outbox
            accept@(actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept) <-
                lift $ prepareAccept followee
            _luAccept <- lift $ updateOutboxItem' (makeLocalActor recipID) acceptID actionAccept

            sieve <- lift $ getSieve followee
            return (recipActorID, acceptID, sieve, accept, inboxItemID)

    case maybeFollow of
        Nothing -> done "I already have this activity in my inbox"
        Just (actorID, acceptID, sieve, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
            forwardActivity authorIdMsig body (makeLocalActor recipID) actorID sieve
            lift $ sendActivity
                (makeLocalActor recipID) actorID localRecipsAccept
                remoteRecipsAccept fwdHostsAccept acceptID actionAccept
            doneDB inboxItemID "Recorded Follow and published Accept"

    where

    prepareAccept followee = do
        encodeRouteHome <- getEncodeRouteHome

        audSender <- makeAudSenderWithFollowers authorIdMsig
        uFollow <- lift $ getActivityURI authorIdMsig

        audsRecip <- lift $ makeAudience followee

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience $ audSender : audsRecip

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = []
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uFollow
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor accepted something
-- Behavior:
--     * If it's on an Invite where I'm the resource:
--         * Verify the Accept is by the Invite target
--         * Forward the Accept to my followers
--         * Send a Grant:
--             * To: Accepter (i.e. Invite target)
--             * CC: Invite sender, Accepter's followers, my followers
--     * If it's on a Join where I'm the resource:
--         * Verify the Accept is authorized
--         * Forward the Accept to my followers
--         * Send a Grant:
--             * To: Join sender
--             * CC: Accept sender, Join sender's followers, my followers
--     * If it's an Invite (that I know about) where I'm invited to a project:
--          * If I haven't yet seen the project's approval:
--              * Verify the author is the project
--              * Record the approval in the Stem record in DB
--          * If I saw project's approval, but not my collaborators' approval:
--              * Verify the Accept is authorized
--              * Record the approval in the Stem record in DB
--              * Forward to my followers
--              * Publish and send an Accept:
--                  * To: Inviter, project, Accept author
--                  * CC: Project followers, my followers
--              * Record it in the Stem record in DB as well
--          * If I already saw both approvals, respond with error
--     * If it's an Add (that I know about and already Accepted) where I'm
--       invited to a project:
--          * If I've already seen the project's accept, respond with error
--          * Otherwise, just ignore the Accept
--     * Otherwise respond with error
--
--  * Add-a-Team mode
--         * Give me a new team active SquadOriginUs
--              * Respond with error, we aren't supposed to get any Accept
--         * Give me a new team passive SquadOriginThem
--              * Option 1: I haven't yet seen parent's Accept
--                  * Verify sender is the parent
--              * Option 2: I saw it, but not my collaborator's Accept
--                  * Verify the accept is authorized
--              * Otherwise respond with error, no Accept is needed
--
--     * Insert the Accept to my inbox
--
--     * In team-passive mode,
--          * Option 1: Record team's Accept in the Dest record
--          * Option 2: Record my collaborator's Accept in the Squad record
--              * Prepare to send my own Accept
--
--     * Forward the Accept to my followers
--
--     * Possibly send a Grant/Accept:
--         * Team-passive
--              * In option 2
--              * Accept
--              * Object: The Add
--              * Fulfills: My collaborator's Accept
--              * To: Team
--              * CC:
--                  - Team's followers
--                  - My followers
--                  - The Accept sender (my collaborator)
--
--  * If it's a Factory approving creation of an actor forked from me
--      * Record in DB
--      * Forward to my followers
topicAccept
    :: forall topic.
       (PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic)
    => (topic -> ResourceId)
    -> (forall f. f topic -> ComponentBy f)
    -> UTCTime
    -> Key topic
    -> Verse
    -> AP.Accept URIMode
    -> ActE (Text, Act (), Next)
topicAccept grabResource topicComponent now recipKey (Verse authorIdMsig body) accept = do

    -- Check input
    acceptee <- parseAccept accept

    -- Grab me from DB
    (resourceID, recipActorID, recipActor) <- lift $ withDB $ do
        resourceID <- grabResource <$> getJust recipKey
        Resource recipActorID <- getJust resourceID
        recipActor <- getJust recipActorID
        return (resourceID, recipActorID, recipActor)

    mode <- withDBExcept $ do

        -- Find the accepted activity in our DB
        accepteeDB <- do
            a <- getActivity acceptee
            fromMaybeE a "Can't find acceptee in DB"

        -- See if the accepted activity is an Invite or Join to a local
        -- resource, grabbing the Collab record from our DB
        -- See if the accepted activity is an Invite or Add on a local
        -- component, grabbing the Stem record from our DB
        maybeCollabOrStem <-
            lift $ runMaybeT $
                Left . Left . Left <$> tryInviteCollab accepteeDB <|>
                Left . Left . Right <$> tryJoinCollab accepteeDB <|>
                Left . Right . Left <$> tryInviteComp accepteeDB <|>
                Left . Right . Right <$> tryAddComp accepteeDB <|>
                Right . Left <$> tryAddTeamActive resourceID accepteeDB <|>
                Right . Left <$> tryAddTeamPassive resourceID accepteeDB <|>
                Right . Right <$> tryForkMe resourceID (actorInbox recipActor) accepteeDB
        fromMaybeE maybeCollabOrStem "Accepted activity isn't an Invite/Join/Add I'm aware of"

    case mode of
        Left (Left collab) ->
            topicAcceptCollab recipActorID recipActor collab
        Left (Right stem) ->
            topicAcceptStem recipActorID recipActor stem
        Right (Left team) ->
            addTeam team
        Right (Right fork) ->
            recordFork recipActorID recipActor fork

    where

    meID = recipKey
    toComponent = topicComponent

    meComponent = toComponent recipKey
    meResource = componentResource meComponent
    meActor = resourceToActor meResource

    topicResource :: forall f. f topic -> LocalResourceBy f
    topicResource = componentResource . topicComponent

    tryInviteCollab (Left (actorByKey, _actorEntity, itemID)) =
        (,Left actorByKey) . collabInviterLocalCollab <$>
            MaybeT (getValBy $ UniqueCollabInviterLocalInvite itemID)
    tryInviteCollab (Right remoteActivityID) = do
        CollabInviterRemote collab actorID _ <-
            MaybeT $ getValBy $
                UniqueCollabInviterRemoteInvite remoteActivityID
        actor <- lift $ getJust actorID
        sender <-
            lift $ (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (collab, Right sender)

    tryJoinCollab (Left (actorByKey, _actorEntity, itemID)) =
        (,Left actorByKey) . collabRecipLocalJoinFulfills <$>
            MaybeT (getValBy $ UniqueCollabRecipLocalJoinJoin itemID)
    tryJoinCollab (Right remoteActivityID) = do
        CollabRecipRemoteJoin recipID fulfillsID _ <-
            MaybeT $ getValBy $
                UniqueCollabRecipRemoteJoinJoin remoteActivityID
        remoteActorID <- lift $ collabRecipRemoteActor <$> getJust recipID
        actor <- lift $ getJust remoteActorID
        joiner <-
            lift $ (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (fulfillsID, Right joiner)

    tryInviteComp (Left (actorByKey, _actorEntity, itemID)) =
        (,Left (actorByKey, itemID)) . stemProjectGestureLocalOrigin <$>
            MaybeT (getValBy $ UniqueStemProjectGestureLocalInvite itemID)
    tryInviteComp (Right remoteActivityID) = do
        StemProjectGestureRemote originID actorID _ <-
            MaybeT $ getValBy $
                UniqueStemProjectGestureRemoteInvite remoteActivityID
        actor <- lift $ getJust actorID
        inviter <-
            lift $ (,remoteActorFollowers actor, remoteActivityID) <$> getRemoteActorURI actor
        return (originID, Right inviter)

    tryAddComp (Left (actorByKey, _actorEntity, itemID)) = do
        StemComponentGestureLocal stemID _ <-
            MaybeT $ getValBy $ UniqueStemComponentGestureLocalActivity itemID
        originID <- MaybeT $ getKeyBy $ UniqueStemOriginAdd stemID
        return (stemID, originID, Left (actorByKey, itemID))
    tryAddComp (Right remoteActivityID) = do
        StemComponentGestureRemote stemID actorID _ <-
            MaybeT $ getValBy $
                UniqueStemComponentGestureRemoteActivity remoteActivityID
        originID <- MaybeT $ getKeyBy $ UniqueStemOriginAdd stemID
        actor <- lift $ getJust actorID
        adder <-
            lift $ (,remoteActorFollowers actor,remoteActivityID) <$> getRemoteActorURI actor
        return (stemID, originID, Right adder)

    verifySquadHolder :: ResourceId -> SquadId -> MaybeT ActDB ()
    verifySquadHolder meResourceID squadID = do
        Squad _ r <- lift $ getJust squadID
        guard $ r == meResourceID

    tryAddTeamActive' r squadID = do
        usID <- MaybeT $ getKeyBy $ UniqueSquadOriginUs squadID
        verifySquadHolder r squadID
        topic <- lift $ getSquadTeam squadID
        return (squadID, topic, Left ())

    tryAddTeamActive r (Left (_actorByKey, _actorEntity, itemID)) = do
        SquadUsGestureLocal squadID _ <-
            MaybeT $ getValBy $ UniqueSquadUsGestureLocalActivity itemID
        tryAddTeamActive' r squadID
    tryAddTeamActive r (Right remoteActivityID) = do
        SquadUsGestureRemote squadID _ _ <-
            MaybeT $ getValBy $ UniqueSquadUsGestureRemoteActivity remoteActivityID
        tryAddTeamActive' r squadID

    tryAddTeamPassive' r themID = do
        SquadOriginThem squadID <- lift $ getJust themID
        verifySquadHolder r squadID
        topic <- lift $ getSquadTeam squadID
        return (squadID, topic, Right themID)

    tryAddTeamPassive r (Left (_actorByKey, _actorEntity, itemID)) = do
        SquadThemGestureLocal themID _ <-
            MaybeT $ getValBy $ UniqueSquadThemGestureLocalAdd itemID
        tryAddTeamPassive' r themID
    tryAddTeamPassive r (Right remoteActivityID) = do
        SquadThemGestureRemote themID _ _ <-
            MaybeT $ getValBy $ UniqueSquadThemGestureRemoteAdd remoteActivityID
        tryAddTeamPassive' r themID

    tryForkMe resourceID inboxID accepteeDB = do
        inboxItemID <-
            case accepteeDB of
                Left (_actorByKey, _actorEntity, itemID) ->
                    inboxItemLocalItem <$>
                        MaybeT (getValBy $ UniqueInboxItemLocal inboxID itemID)
                Right remoteActivityID ->
                    inboxItemRemoteItem <$>
                        MaybeT (getValBy $ UniqueInboxItemRemote inboxID remoteActivityID)
        Entity createID (ResourceForkCreate rid _) <-
            MaybeT $ getBy $ UniqueResourceForkCreate inboxItemID
        guard $ rid == resourceID
        return createID

    prepareGrant isInvite sender role = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audAccepter <- makeAudSenderWithFollowers authorIdMsig
        audApprover <- lift $ makeAudSenderOnly authorIdMsig
        recipHash <- encodeKeyHashid recipKey
        let topicByHash = resourceToActor $ topicResource recipHash

        senderHash <- bitraverse hashLocalActor pure sender

        uAccepter <- lift $ getActorURI authorIdMsig

        let audience =
                if isInvite
                    then
                        let audInviter =
                                case senderHash of
                                    Left actor -> AudLocal [actor] []
                                    Right (ObjURI h lu, _followers) ->
                                        AudRemote h [lu] []
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audInviter, audAccepter, audTopic]
                    else
                        let audJoiner =
                                case senderHash of
                                    Left actor -> AudLocal [actor] [localActorFollowers actor]
                                    Right (ObjURI h lu, followers) ->
                                        AudRemote h [lu] (maybeToList followers)
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audJoiner, audApprover, audTopic]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [AP.acceptObject accept]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXRole role
                    , AP.grantContext   =
                        encodeRouteHome $ renderLocalActor topicByHash
                    , AP.grantTarget    =
                        if isInvite
                            then uAccepter
                            else case senderHash of
                                Left actor ->
                                    encodeRouteHome $ renderLocalActor actor
                                Right (ObjURI h lu, _) -> ObjURI h lu
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    topicAcceptCollab recipActorID recipActor collab = do

        -- Verify the capability URI, if provided, is one of:
        --   * Outbox item URI of a local actor, i.e. a local activity
        --   * A remote URI
        maybeCap <-
            traverse
                (nameExceptT "Accept capability" . parseActivityURI')
                (AP.activityCapability $ actbActivity body)

        maybeNew <- withDBExcept $ do

            -- Find the local resource and verify it's me
            collabID <-
                lift $ case collab of
                    Left (fulfillsID, _) ->
                        collabFulfillsInviteCollab <$> getJust fulfillsID
                    Right (fulfillsID, _) ->
                        collabFulfillsJoinCollab <$> getJust fulfillsID
            topic <- lift $ getCollabTopic collabID
            unless (topicResource recipKey == topic) $
                throwE "Accept object is an Invite/Join for some other resource"

            idsForAccept <-
                case collab of

                    -- If accepting an Invite, find the Collab recipient and verify
                    -- it's the sender of the Accept
                    Left (fulfillsID, _) -> Left <$> do
                        recip <-
                            lift $
                            requireEitherAlt
                                (getBy $ UniqueCollabRecipLocal collabID)
                                (getBy $ UniqueCollabRecipRemote collabID)
                                "Found Collab with no recip"
                                "Found Collab with multiple recips"
                        case (recip, authorIdMsig) of
                            (Left (Entity crlid crl), Left (LocalActorPerson personID, _, _))
                                | collabRecipLocalPerson crl == personID ->
                                    return (fulfillsID, Left crlid)
                            (Right (Entity crrid crr), Right (author, _, _))
                                | collabRecipRemoteActor crr == remoteAuthorId author ->
                                    return (fulfillsID, Right crrid)
                            _ -> throwE "Accepting an Invite whose recipient is someone else"

                    -- If accepting a Join, verify accepter has permission
                    Right (fulfillsID, _) -> Right <$> do
                        capID <- fromMaybeE maybeCap "No capability provided"
                        capability <-
                            case capID of
                                Left (capActor, _, capItem) -> return (capActor, capItem)
                                Right _ -> throwE "Capability is a remote URI, i.e. not authored by the local resource"
                        verifyCapability'
                            capability
                            authorIdMsig
                            (topicResource recipKey)
                            AP.RoleAdmin
                        return fulfillsID

            -- Verify the Collab isn't already validated
            maybeEnabled <- lift $ getBy $ UniqueCollabEnable collabID
            verifyNothingE maybeEnabled "I already sent a Grant for this Invite/Join"

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                -- Record the Accept on the Collab
                case (idsForAccept, acceptDB) of
                    (Left (fulfillsID, Left recipID), Left (_, _, acceptID)) -> do
                        maybeAccept <- lift $ insertUnique $ CollabRecipLocalAccept recipID fulfillsID acceptID
                        unless (isNothing maybeAccept) $
                            throwE "This Invite already has an Accept by recip"
                    (Left (fulfillsID, Right recipID), Right (_, _, acceptID)) -> do
                        maybeAccept <- lift $ insertUnique $ CollabRecipRemoteAccept recipID fulfillsID acceptID
                        unless (isJust maybeAccept) $
                            throwE "This Invite already has an Accept by recip"
                    (Right fulfillsID, Left (_, _, acceptID)) -> do
                        maybeAccept <- lift $ insertUnique $ CollabApproverLocal fulfillsID acceptID
                        unless (isJust maybeAccept) $
                            throwE "This Join already has an Accept"
                    (Right fulfillsID, Right (author, _, acceptID)) -> do
                        maybeAccept <- lift $ insertUnique $ CollabApproverRemote fulfillsID (remoteAuthorId author) acceptID
                        unless (isJust maybeAccept) $
                            throwE "This Join already has an Accept"
                    _ -> error "topicAccept impossible"

                -- Prepare forwarding of Accept to my followers
                let recipByID = resourceToActor $ topicResource recipKey
                recipByHash <- hashLocalActor recipByID
                let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

                grantInfo <- do

                    -- Enable the Collab in our DB
                    grantID <- lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
                    lift $ insert_ $ CollabEnable collabID grantID

                    -- Prepare a Grant activity and insert to my outbox
                    let inviterOrJoiner = either snd snd collab
                        isInvite = isLeft collab
                    grant@(actionGrant, _, _, _) <- do
                        Collab role _ <- lift $ getJust collabID
                        lift $ prepareGrant isInvite inviterOrJoiner role
                    let recipByKey = resourceToActor $ topicResource recipKey
                    _luGrant <- lift $ updateOutboxItem' recipByKey grantID actionGrant
                    return (grantID, grant)

                return (recipActorID, sieve, grantInfo, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, (grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant)), inboxItemID) -> do
                let recipByID = resourceToActor $ topicResource recipKey
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ sendActivity
                    recipByID recipActorID localRecipsGrant
                    remoteRecipsGrant fwdHostsGrant grantID actionGrant
                doneDB inboxItemID "Forwarded the Accept and published a Grant"

    prepareReact project inviter = do
        encodeRouteHome <- getEncodeRouteHome

        (audInviter, uInvite) <-
            case inviter of
                Left (byKey, itemID) -> do
                    byHash <- hashLocalActor byKey
                    itemHash <- encodeKeyHashid itemID
                    return
                        ( AudLocal [byHash] []
                        , encodeRouteHome $ activityRoute byHash itemHash
                        )
                Right (ObjURI h lu, _followers, activityID) -> do
                    objectID <- remoteActivityIdent <$> getJust activityID
                    luAct <- remoteObjectIdent <$> getJust objectID
                    return (AudRemote h [lu] [], ObjURI h luAct)
        audProject <-
            case project of
                Left (Entity _ (StemProjectLocal _ projectID)) -> do
                    projectHash <- encodeKeyHashid projectID
                    return $
                        AudLocal
                            [LocalActorProject projectHash]
                            [LocalStageProjectFollowers projectHash]
                Right (Entity _ (StemProjectRemote _ actorID)) -> do
                    actor <- getJust actorID
                    ObjURI h lu <- getRemoteActorURI actor
                    let followers = remoteActorFollowers actor
                    return $ AudRemote h [lu] (maybeToList followers)
        audAccepter <- lift $ makeAudSenderOnly authorIdMsig
        audMe <-
            AudLocal [] . pure . localActorFollowers .
            resourceToActor . topicResource <$>
                encodeKeyHashid recipKey

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audInviter, audProject, audAccepter, audMe]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = []
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uInvite
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    topicAcceptStem recipActorID recipActor stem = do

        -- Verify the capability URI, if provided, is one of:
        --   * Outbox item URI of a local actor, i.e. a local activity
        --   * A remote URI
        maybeCap <-
            traverse
                (nameExceptT "Accept capability" . parseActivityURI')
                (AP.activityCapability $ actbActivity body)

        maybeNew <- withDBExcept $ do

            -- Find the local component and verify it's me
            stemID <-
                lift $ case stem of
                    Left (originInviteID, _inviter) ->
                        stemOriginInviteStem <$> getJust originInviteID
                    Right (stemID, _originAddID, _adder) ->
                        return stemID
            ident <- lift $ getStemIdent stemID
            unless (topicComponent recipKey == ident) $
                throwE "Accept object is an Invite/Add for some other component"

            project <-
                lift $
                requireEitherAlt
                    (getBy $ UniqueStemProjectLocal stemID)
                    (getBy $ UniqueStemProjectRemote stemID)
                    "Found Stem with no project"
                    "Found Stem with multiple projects"

            idsForLater <- bitraverse

                -- Accepting an Invite
                -- If I haven't seen the project's approval, verify
                -- the author is the project
                -- Otherwise, verify the Accept is authorized
                (\ (originInviteID, inviter) -> do
                    scgl <- lift $ getBy $ UniqueStemComponentGestureLocal stemID
                    scgr <- lift $ getBy $ UniqueStemComponentGestureRemote stemID
                    unless (isNothing scgl && isNothing scgr) $
                        throwE "I've already recorded my collaborator's Accept on the Invite, no need for further Accepts from anyone"
                    seen <-
                        lift $ case project of
                            Left (Entity k _) -> isJust <$> getBy (UniqueStemProjectAcceptLocalProject k)
                            Right (Entity k _) -> isJust <$> getBy (UniqueStemProjectAcceptRemoteProject k)
                    if seen
                        then do
                            capID <- fromMaybeE maybeCap "No capability provided"
                            capability <-
                                case capID of
                                    Left (capActor, _, capItem) -> return (capActor, capItem)
                                    Right _ -> throwE "Capability is a remote URI, i.e. not authored by the local resource"
                            verifyCapability'
                                capability
                                authorIdMsig
                                (topicResource recipKey)
                                AP.RoleAdmin
                        else case (project, authorIdMsig) of
                            (Left (Entity _ sjl), Left (LocalActorProject projectID, _, _))
                                | stemProjectLocalProject sjl == projectID ->
                                    return ()
                            (Right (Entity _ sjr), Right (author, _, _))
                                | stemProjectRemoteProject sjr == remoteAuthorId author ->
                                    return ()
                            _ -> throwE "The Accept I'm waiting for is by the project"
                    return (originInviteID, seen, inviter)
                )

                (\ (_stemID, _originAddID, _adder) -> do
                    seen <-
                        lift $ case project of
                            Left (Entity k _) -> isJust <$> getBy (UniqueStemProjectGrantLocalProject k)
                            Right (Entity k _) -> isJust <$> getBy (UniqueStemProjectGrantRemoteProject k)
                    when seen $
                        throwE "Already saw project's Grant, no need for any Accepts"
                )

                stem

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            for maybeAcceptDB $ \ (inboxItemID, acceptDB) ->

                (inboxItemID,) <$> case idsForLater of

                    Left (originInviteID, seen, inviter) -> do

                        if not seen
                            then do
                                lift $ case (project, acceptDB) of
                                    (Left (Entity j _), Left (_, _, acceptID)) ->
                                        insert_ $ StemProjectAcceptLocal originInviteID j acceptID
                                    (Right (Entity j _), Right (_, _, acceptID)) ->
                                        insert_ $ StemProjectAcceptRemote originInviteID j acceptID
                                    _ -> error "topicAccept Impossible"
                                return Nothing
                            else do
                                lift $ case acceptDB of
                                    Left (_, _, acceptID) ->
                                        insert_ $ StemComponentGestureLocal stemID acceptID
                                    Right (author, _, acceptID) ->
                                        insert_ $ StemComponentGestureRemote stemID (remoteAuthorId author) acceptID

                                -- Prepare forwarding of Accept to my followers
                                let recipByID = resourceToActor $ topicResource recipKey
                                recipByHash <- hashLocalActor recipByID
                                let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

                                reactInfo <- do

                                    -- Record the fresh Accept in our DB
                                    reactID <- lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
                                    lift $ insert_ $ StemComponentAccept stemID reactID

                                    -- Prepare an Accept activity and insert to my outbox
                                    react@(actionReact, _, _, _) <- lift $ prepareReact project inviter
                                    let recipByKey = resourceToActor $ topicResource recipKey
                                    _luReact <- lift $ updateOutboxItem' recipByKey reactID actionReact
                                    return (reactID, react)

                                return $ Just (sieve, reactInfo)

                    Right () -> return Nothing

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (inboxItemID, Nothing) -> doneDB inboxItemID "Done"
            Just (inboxItemID, (Just (sieve, (reactID, (actionReact, localRecipsReact, remoteRecipsReact, fwdHostsReact))))) -> do
                let recipByID = resourceToActor $ topicResource recipKey
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ sendActivity
                    recipByID recipActorID localRecipsReact
                    remoteRecipsReact fwdHostsReact reactID actionReact
                doneDB inboxItemID "Forwarded the Accept and published an Accept"

    theyIsAuthor' :: Either (a, GroupId) (b, RemoteActorId) -> Bool
    theyIsAuthor' ident =
        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        in  author == bimap (LocalActorGroup . snd) snd ident

    addTeam (squadID, topic, mode) = do

        (themID, mode') <-
            case mode of

                -- Team-active mode
                -- Respond with error, we aren't supposed to get any Accept
                Left () -> throwE "Team-active (SquadOriginUs) mode, I'm not expecting any Accept"

                -- Team-passive mode
                -- Option 1: I haven't yet seen team's Accept
                --   * Verify sender is the team
                -- Option 2: I saw it, but not my collaborator's Accept
                --   * Verify the accept is authorized
                -- Otherwise respond with error, no Accept is needed
                Right themID -> (themID,) <$> do
                    (maybeTeamAccept, maybeUsGesture) <-
                        lift $ withDB $ liftA2 (,)
                            (case bimap fst fst topic of
                                Left localID -> (() <$) <$> getBy (UniqueSquadThemAcceptLocalTopic localID)
                                Right remoteID -> (() <$) <$> getBy (UniqueSquadThemAcceptRemoteTopic remoteID)
                            )
                            (do l <- getBy $ UniqueSquadUsGestureLocal squadID
                                r <- getBy $ UniqueSquadUsGestureRemote squadID
                                case (isJust l, isJust r) of
                                    (False, False) -> pure Nothing
                                    (False, True) -> pure $ Just ()
                                    (True, False) -> pure $ Just ()
                                    (True, True) -> error "Both SquadUsGestureLocal and SquadUsGestureRemote"
                            )
                    case (isJust maybeTeamAccept, isJust maybeUsGesture) of
                        (False, True) -> error "Impossible/bug, didn't see team's Accept but recorded my collaborator's Accept"
                        (False, False) -> do
                            unless (theyIsAuthor' topic) $
                                throwE "The Accept I'm waiting for is from my new team"
                            return $ Left ()
                        (True, False) -> do
                            let muCap = AP.activityCapability $ actbActivity body
                            uCap <- fromMaybeE muCap "No capability provided"
                            verifyCapability''
                                uCap
                                authorIdMsig
                                meResource
                                AP.RoleAdmin
                            return $ Right ()
                        (True, True) -> throwE "Just waiting for Grant from team, or already have it, anyway not needing any further Accept"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            meResourceID <- lift $ grabResource <$> getJust meID
            Resource meActorID <- lift $ getJust meResourceID
            meActorDB <- lift $ getJust meActorID

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox meActorDB) False
            for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                idsForGrant <-
                    lift $ case mode' of

                        -- Getting an Accept from the team
                        -- Record team's Accept in the Squad record
                        Left () -> do
                            case (topic, acceptDB) of
                                (Left (localID, _), Left (_, _, acceptID)) ->
                                    insert_ $ SquadThemAcceptLocal themID localID acceptID
                                (Right (remoteID, _), Right (_, _, acceptID)) ->
                                    insert_ $ SquadThemAcceptRemote themID remoteID acceptID
                                _ -> error "topicAccept impossible v"
                            return Nothing

                        -- Getting an Accept from my collaborator
                        -- Record my collaborator's Accept in the Squad record
                        -- Prepare to send my own Accept
                        Right () -> Just <$> do
                            case acceptDB of
                                Left (_, _, acceptID) ->
                                    insert_ $ SquadUsGestureLocal squadID acceptID
                                Right (author, _, acceptID) ->
                                    insert_ $ SquadUsGestureRemote squadID (remoteAuthorId author) acceptID
                            acceptID <- insertEmptyOutboxItem' (actorOutbox meActorDB) now
                            insert_ $ SquadUsAccept squadID acceptID
                            return acceptID

                -- Prepare forwarding of Accept to my followers
                sieve <- do
                    h <- hashLocalActor meActor
                    return $ makeRecipientSet [] [localActorFollowers h]

                maybeAct <-
                    for idsForGrant $ \ acceptID -> lift $ do
                        accept@(actionAccept, _, _, _) <-
                            prepareSquadAccept (bimap snd snd topic)
                        _luAccept <- updateOutboxItem' meActor acceptID actionAccept
                        return (acceptID, accept)

                return (meActorID, sieve, maybeAct, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, maybeGrant, inboxItemID) -> do
                forwardActivity authorIdMsig body meActor recipActorID sieve
                lift $ for_ maybeGrant $ \ (grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant)) ->
                    sendActivity
                        meActor recipActorID localRecipsGrant
                        remoteRecipsGrant fwdHostsGrant grantID actionGrant
                doneDB inboxItemID "[Team] Forwarded the Accept and maybe published a Grant/Accept"

        where

        prepareSquadAccept topic = do
            encodeRouteHome <- getEncodeRouteHome

            audMyCollab <- lift $ makeAudSenderOnly authorIdMsig
            audSquad <-
                case topic of
                    Left j -> do
                        h <- encodeKeyHashid j
                        return $
                            AudLocal [LocalActorGroup h] [LocalStageGroupFollowers h]
                    Right raID -> do
                        ra <- getJust raID
                        ObjURI h lu <- getRemoteActorURI ra
                        return $
                            AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
            audMe <-
                AudLocal [] . pure . localActorFollowers <$>
                    hashLocalActor meActor
            uCollabAccept <- lift $ getActivityURI authorIdMsig
            let uAdd = AP.acceptObject accept

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audMyCollab, audSquad, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uCollabAccept]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uAdd
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    recordFork actorMeID actorMe createID = do

        -- Grab the Accept's result via DB/HTTP
        new <- do
            ObjURI hAccept _luAccept <- lift $ getActorURI authorIdMsig
            luNew <- fromMaybeE (AP.acceptResult accept) "No Accept.result"
            let uNew = ObjURI hAccept luNew
            n <- parseActorURI' uNew
            let typ = localActorType $ resourceToActor $ componentResource $ topicComponent recipKey
            bitraverse
                (\ la -> do
                    unless (localActorType la == typ) $
                        throwE "New fork's type differs from mine"
                    lr <- fromMaybeE (actorToResource la) "New fork isn't a Resource"
                    rid <- withDBExcept $ do
                        lre <- getLocalResourceEntityE lr "New fork not found in DB"
                        lift $ grabLocalResourceID lre
                    return (la, rid)
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Factory @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Factory isn't an actor"
                        Right (Just actor) -> do
                            unless (typ == remoteActorType (entityVal actor)) $
                                throwE "New actor's type differs from mine"
                            return (u, actor)
                )
                n

        maybeNew <- withDBExcept $ do

            -- Detect from DB the Factory of the Create
            factoryAndNew <-
                lift $
                bitraverse
                    (\ (la, rid) -> do
                        Entity k (ResourceForkFactoryLocal _ factoryID) <-
                            getByJust $ UniqueResourceForkFactoryLocal createID
                        return (k, factoryID, la, rid)
                    )
                    (\ (u, actor) -> do
                        Entity k (ResourceForkFactoryRemote _ factoryID) <-
                            getByJust $ UniqueResourceForkFactoryRemote createID
                        return (k, factoryID, u, actor)
                    )
                    new

            -- Make sure the Accept sender is the Factory
            case (authorIdMsig, factoryAndNew) of
                (Left (LocalActorFactory f, _, _), Left (_, f', _, _))
                    | f == f' -> pure ()
                (Right (ra, _, _), Right (_, f, _, _))
                    | remoteAuthorId ra == f -> pure ()
                _ -> throwE "Accept sender isn't the Factory"

            maybeAcceptDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorMe) False
            lift $ for maybeAcceptDB $ \ (inboxItemID, acceptDB) -> do

                -- Record the completion of the Fork
                acceptID <- insert $ ResourceForkAccept createID inboxItemID
                case factoryAndNew of
                    Left (factory, _, _, rid) ->
                        insert_ $ ResourceForkLocal acceptID factory rid
                    Right (factory, _, _, Entity raid _) ->
                        insert_ $ ResourceForkRemote acceptID factory raid

                -- Prepare forwarding of Accept to my followers
                let recipByID = resourceToActor $ topicResource recipKey
                recipByHash <- hashLocalActor recipByID
                let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

                return (actorMeID, sieve, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (actorMeID, sieve, inboxItemID) -> do
                let recipByID = resourceToActor $ topicResource recipKey
                forwardActivity authorIdMsig body recipByID actorMeID sieve
                doneDB inboxItemID "Recorded successful fork"

topicReject
    :: (PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic)
    => (topic -> ResourceId)
    -> (forall f. f topic -> LocalResourceBy f)
    -> UTCTime
    -> Key topic
    -> Verse
    -> AP.Reject URIMode
    -> ActE (Text, Act (), Next)
topicReject grabResource topicResource now recipKey (Verse authorIdMsig body) reject = do

    -- Check input
    rejectee <- parseReject reject

    -- Verify the capability URI is one of:
    --   * Outbox item URI of a local actor, i.e. a local activity
    --   * A remote URI
    maybeCap <-
        traverse
            (nameExceptT "Accept capability" . parseActivityURI')
            (AP.activityCapability $ actbActivity body)

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        resourceID <- lift $ grabResource <$> getJust recipKey
        Resource recipActorID <- lift $ getJust resourceID
        recipActor <- lift $ getJust recipActorID

        -- Find the rejected activity in our DB
        rejecteeDB <- do
            a <- getActivity rejectee
            fromMaybeE a "Can't find rejectee in DB"

        -- See if the rejected activity is an Invite or Join to a local
        -- resource, grabbing the Collab record from our DB
        collab <- do
            maybeCollab <-
                lift $ runMaybeT $
                    Left <$> tryInvite rejecteeDB <|>
                    Right <$> tryJoin rejecteeDB
            fromMaybeE maybeCollab "Rejected activity isn't an Invite or Join I'm aware of"

        -- Find the local resource and verify it's me
        collabID <-
            lift $ case collab of
                Left (fulfillsID, _, _) ->
                    collabFulfillsInviteCollab <$> getJust fulfillsID
                Right (fulfillsID, _, _, _) ->
                    collabFulfillsJoinCollab <$> getJust fulfillsID
        topic <- lift $ getCollabTopic collabID
        unless (topicResource recipKey == topic) $
            throwE "Accept object is an Invite/Join for some other resource"

        idsForReject <-
            case collab of

                -- If rejecting an Invite, find the Collab recipient and verify
                -- it's the sender of the Reject
                Left (fulfillsID, _, deleteInviter) -> Left <$> do
                    recip <-
                        lift $
                        requireEitherAlt
                            (getBy $ UniqueCollabRecipLocal collabID)
                            (getBy $ UniqueCollabRecipRemote collabID)
                            "Found Collab with no recip"
                            "Found Collab with multiple recips"
                    case (recip, authorIdMsig) of
                        (Left (Entity crlid crl), Left (LocalActorPerson personID, _, _))
                            | collabRecipLocalPerson crl == personID ->
                                return (fulfillsID, Left crlid, deleteInviter)
                        (Right (Entity crrid crr), Right (author, _, _))
                            | collabRecipRemoteActor crr == remoteAuthorId author ->
                                return (fulfillsID, Right crrid, deleteInviter)
                        _ -> throwE "Rejecting an Invite whose recipient is someone else"

                -- If rejecting a Join, verify accepter has permission
                Right (fulfillsID, _, deleteRecipJoin, deleteRecip) -> Right <$> do
                    capID <- fromMaybeE maybeCap "No capability provided"
                    capability <-
                        case capID of
                            Left (capActor, _, capItem) -> return (capActor, capItem)
                            Right _ -> throwE "Capability is a remote URI, i.e. not authored by the local resource"
                    verifyCapability'
                        capability
                        authorIdMsig
                        (topicResource recipKey)
                        AP.RoleAdmin
                    return (fulfillsID, deleteRecipJoin, deleteRecip)

        -- Verify the Collab isn't already validated
        maybeEnabled <- lift $ getBy $ UniqueCollabEnable collabID
        verifyNothingE maybeEnabled "I already sent a Grant for this Invite/Join"

        -- Verify the Collab isn't already accepted/approved
        case idsForReject of
            Left (_fulfillsID, Left recipID, _) -> do
                mval <-
                    lift $ getBy $ UniqueCollabRecipLocalAcceptCollab recipID
                verifyNothingE mval "Invite is already accepted"
            Left (_fulfillsID, Right recipID, _) -> do
                mval <-
                    lift $ getBy $ UniqueCollabRecipRemoteAcceptCollab recipID
                verifyNothingE mval "Invite is already accepted"
            Right (fulfillsID, _, _) -> do
                mval1 <- lift $ getBy $ UniqueCollabApproverLocal fulfillsID
                mval2 <- lift $ getBy $ UniqueCollabApproverRemote fulfillsID
                unless (isNothing mval1 && isNothing mval2) $
                    throwE "Join is already approved"

        maybeRejectDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
        for maybeRejectDB $ \ (inboxItemID, rejectDB) -> do

            -- Delete the whole Collab record
            case idsForReject of
                Left (fulfillsID, recipID, deleteInviter) -> lift $ do
                    bitraverse_ delete delete recipID
                    deleteInviter
                    delete fulfillsID
                Right (fulfillsID, deleteRecipJoin, deleteRecip) -> lift $ do
                    deleteRecipJoin
                    deleteRecip
                    delete fulfillsID
            lift $ delete collabID

            -- Prepare forwarding of Reject to my followers
            let recipByID = resourceToActor $ topicResource recipKey
            recipByHash <- hashLocalActor recipByID
            let sieve = makeRecipientSet [] [localActorFollowers recipByHash]

            newRejectInfo <- do

                -- Prepare a Reject activity and insert to my outbox
                newRejectID <- lift $ insertEmptyOutboxItem' (actorOutbox recipActor) now
                let inviterOrJoiner = either (view _2) (view _2) collab
                    isInvite = isLeft collab
                newReject@(actionReject, _, _, _) <-
                    lift $ prepareReject isInvite inviterOrJoiner
                let recipByKey = resourceToActor $ topicResource recipKey
                _luNewReject <- lift $ updateOutboxItem' recipByKey newRejectID actionReject
                return (newRejectID, newReject)

            return (recipActorID, sieve, newRejectInfo, inboxItemID)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (recipActorID, sieve, (newRejectID, (action, localRecips, remoteRecips, fwdHosts)), inboxItemID) -> do
            let recipByID = resourceToActor $ topicResource recipKey
            forwardActivity authorIdMsig body recipByID recipActorID sieve
            lift $ sendActivity
                recipByID recipActorID localRecips
                remoteRecips fwdHosts newRejectID action
            doneDB inboxItemID "Forwarded the Reject and published my own Reject"

    where

    tryInvite (Left (actorByKey, _actorEntity, itemID)) = do
        Entity k (CollabInviterLocal f _) <-
            MaybeT $ getBy $ UniqueCollabInviterLocalInvite itemID
        return (f, Left actorByKey, delete k)
    tryInvite (Right remoteActivityID) = do
        Entity k (CollabInviterRemote collab actorID _) <-
            MaybeT $ getBy $
                UniqueCollabInviterRemoteInvite remoteActivityID
        actor <- lift $ getJust actorID
        sender <-
            lift $ (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (collab, Right sender, delete k)

    tryJoin (Left (actorByKey, _actorEntity, itemID)) = do
        Entity k (CollabRecipLocalJoin recipID fulfillsID _) <-
            MaybeT $ getBy $ UniqueCollabRecipLocalJoinJoin itemID
        return (fulfillsID, Left actorByKey, delete k, delete recipID)
    tryJoin (Right remoteActivityID) = do
        Entity k (CollabRecipRemoteJoin recipID fulfillsID _) <-
            MaybeT $ getBy $
                UniqueCollabRecipRemoteJoinJoin remoteActivityID
        remoteActorID <- lift $ collabRecipRemoteActor <$> getJust recipID
        actor <- lift $ getJust remoteActorID
        joiner <-
            lift $ (,remoteActorFollowers actor) <$> getRemoteActorURI actor
        return (fulfillsID, Right joiner, delete k, delete recipID)

    prepareReject isInvite sender = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audRejecter <- makeAudSenderWithFollowers authorIdMsig
        audForbidder <- lift $ makeAudSenderOnly authorIdMsig
        recipHash <- encodeKeyHashid recipKey
        let topicByHash = resourceToActor $ topicResource recipHash

        senderHash <- bitraverse hashLocalActor pure sender

        uReject <- lift $ getActivityURI authorIdMsig

        let audience =
                if isInvite
                    then
                        let audInviter =
                                case senderHash of
                                    Left actor -> AudLocal [actor] []
                                    Right (ObjURI h lu, _followers) ->
                                        AudRemote h [lu] []
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audInviter, audRejecter, audTopic]
                    else
                        let audJoiner =
                                case senderHash of
                                    Left actor -> AudLocal [actor] [localActorFollowers actor]
                                    Right (ObjURI h lu, followers) ->
                                        AudRemote h [lu] (maybeToList followers)
                            audTopic = AudLocal [] [localActorFollowers topicByHash]
                        in  [audJoiner, audForbidder, audTopic]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uReject]
                , AP.actionSpecific   = AP.RejectActivity AP.Reject
                    { AP.rejectObject = AP.rejectObject reject
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor A invited actor B to a resource
-- Behavior:
--      * If resource is my collaborators collection:
--          * Verify A isn't inviting themselves
--          * Verify A is authorized by me to invite actors to me
--          * Verify B doesn't already have an invite/join/grant for me
--          * Remember the invite in DB
--          * Forward the Invite to my followers
--          * Send Accept to A, B, my-followers
componentInvite
    :: forall topic.
       (PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic)
    => (topic -> KomponentId)
    -> (forall f. f topic -> ComponentBy f)
    -> UTCTime
    -> Key topic
    -> Verse
    -> AP.Invite URIMode
    -> ActE (Text, Act (), Next)
componentInvite grabKomponent topicComponent now topicKey (Verse authorIdMsig body) invite = do

    -- Check invite
    (role, targetByKey) <- do
        let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
        (role, resourceOrComps, recipientOrComp) <- parseInvite author invite
        if Left (Left $ topicResource topicKey) == resourceOrComps
            then (role,) <$>
                bitraverse
                    (\case
                        Left r -> pure r
                        Right _ -> throwE "Not accepting component actors as collabs"
                    )
                    pure
                    recipientOrComp
            else throwE "Invite is unrelated to me"

    (capability, targetDB) <- do

        -- Check capability
        capability <- do

            -- Verify that a capability is provided
            uCap <- do
                let muCap = AP.activityCapability $ actbActivity body
                fromMaybeE muCap "No capability provided"

            -- Verify the capability URI is one of:
            --   * Outbox item URI of a local actor, i.e. a local activity
            --   * A remote URI
            cap <- nameExceptT "Invite capability" $ parseActivityURI' uCap

            -- Verify the capability is local
            case cap of
                Left (actorByKey, _, outboxItemID) ->
                    return (actorByKey, outboxItemID)
                _ -> throwE "Capability is remote i.e. definitely not by me"

        -- If target is local, find it in our DB
        -- If target is remote, HTTP GET it, verify it's an actor, and store in
        -- our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the Invite handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result (approve/disapprove the Invite) would be sent later in a
        -- separate (e.g. Accept) activity. But for the PoC level, the current
        -- situation will hopefully do.
        targetDB <-
            bitraverse
                (withDBExcept . flip getGrantRecip "Invitee not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Target @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Target isn't an actor"
                        Right (Just actor) -> return $ entityKey actor
                )
                targetByKey

        return (capability, targetDB)

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        komponentID <- lift $ grabKomponent <$> getJust topicKey
        Komponent resourceID <- lift $ getJust komponentID
        Resource topicActorID <- lift $ getJust resourceID
        topicActor <- lift $ getJust topicActorID

        -- Verify the specified capability gives relevant access
        verifyCapability'
            capability authorIdMsig (topicResource topicKey) AP.RoleAdmin

        -- Verify that target doesn't already have a Collab for me
        existingCollabIDs <-
            lift $ case targetDB of
                Left (GrantRecipPerson (Entity personID _)) ->
                    E.select $ E.from $ \ (collab `E.InnerJoin` recipl) -> do
                        E.on $
                            collab E.^. CollabId E.==.
                            recipl E.^. CollabRecipLocalCollab
                        E.where_ $
                            collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                            recipl E.^. CollabRecipLocalPerson E.==. E.val personID
                        return $ recipl E.^. CollabRecipLocalCollab
                Right remoteActorID ->
                    E.select $ E.from $ \ (collab `E.InnerJoin` recipr) -> do
                        E.on $
                            collab E.^. CollabId E.==.
                            recipr E.^. CollabRecipRemoteCollab
                        E.where_ $
                            collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                            recipr E.^. CollabRecipRemoteActor E.==. E.val remoteActorID
                        return $ recipr E.^. CollabRecipRemoteCollab
        case existingCollabIDs of
            [] -> pure ()
            [_] -> throwE "I already have a Collab for the target"
            _ -> error "Multiple collabs found for target"

        maybeInviteDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
        lift $ for maybeInviteDB $ \ (inboxItemID, inviteDB) -> do

            -- Prepare forwarding Invite to my followers
            sieve <- do
                topicHash <- encodeKeyHashid topicKey
                let topicByHash =
                        resourceToActor $ topicResource topicHash
                return $ makeRecipientSet [] [localActorFollowers topicByHash]

            -- Insert Collab record to DB
            -- Prepare an Accept activity and insert to my outbox
            (acceptID, accept) <- do
                acceptID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
                insertCollab resourceID role targetDB inviteDB acceptID
                accept@(actionAccept, _, _, _) <- lift $ prepareAccept targetByKey
                let topicByKey = resourceToActor $ topicResource topicKey
                _luAccept <- updateOutboxItem' topicByKey acceptID actionAccept
                return (acceptID, accept)

            return (topicActorID, sieve, acceptID, accept, inboxItemID)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (topicActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
            let topicByID = resourceToActor $ topicResource topicKey
            forwardActivity authorIdMsig body topicByID topicActorID sieve
            lift $ sendActivity
                    topicByID topicActorID localRecipsAccept remoteRecipsAccept
                    fwdHostsAccept acceptID actionAccept
            doneDB inboxItemID "Recorded and forwarded the Invite, sent an Accept"

    where

    topicResource :: forall f. f topic -> LocalResourceBy f
    topicResource = componentResource . topicComponent

    insertCollab resourceID role recipient inviteDB acceptID = do
        collabID <- insert $ Collab role resourceID
        fulfillsID <- insert $ CollabFulfillsInvite collabID acceptID
        case inviteDB of
            Left (_, _, inviteID) ->
                insert_ $ CollabInviterLocal fulfillsID inviteID
            Right (author, _, inviteID) -> do
                let authorID = remoteAuthorId author
                insert_ $ CollabInviterRemote fulfillsID authorID inviteID
        case recipient of
            Left (GrantRecipPerson (Entity personID _)) ->
                insert_ $ CollabRecipLocal collabID personID
            Right remoteActorID ->
                insert_ $ CollabRecipRemote collabID remoteActorID

    prepareAccept invited = do
        encodeRouteHome <- getEncodeRouteHome

        audInviter <- makeAudSenderOnly authorIdMsig
        audInvited <-
            case invited of
                Left (GrantRecipPerson p) -> do
                    ph <- encodeKeyHashid p
                    return $ AudLocal [LocalActorPerson ph] []
                Right (ObjURI h lu) -> return $ AudRemote h [lu] []
        audTopic <-
            AudLocal [] . pure . localActorFollowers .
            resourceToActor . topicResource <$> encodeKeyHashid topicKey
        uInvite <- getActivityURI authorIdMsig

        let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audInviter, audInvited, audTopic]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uInvite]
                , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                    { AP.acceptObject   = uInvite
                    , AP.acceptResult   = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor A is removing actor B from a collection C
-- Behavior:
--  * If C is my collaborators collection
--      * Verify A isn't removing themselves
--      * Verify A is authorized by me to remove actors from me
--      * Verify B already has a Grant for me
--      * Remove the whole Collab record from DB
--      * Forward the Remove to my followers
--      * Send a Revoke:
--          * To: Actor B
--          * CC: Actor A, B's followers, my followers
--
--  * If C is my projects collection
--      * Verify A's request is authorized
--      * Verify B is an enabled project of mine
--      * Remove the whole Stem record from DB
--      * Forward to followers
--      * Publish a Revoke on the start-Grant I'd sent to B
--          * To: Actor B
--          * CC: Actor A, B's followers, my followers
--
--  * If C is my teams collection:
--          * Verify A is authorized by me to remove teams from me
--          * Verify B is an active team of mine
--          * Remove the whole Squad record from DB
--          * Forward the Remove to my followers
--          * Send an Accept on the Remove:
--              * To: Actor B
--              * CC: Actor A, B's followers, my followers
--
--  * If I'm B, and C is some project's components collection
--      * Just forward to my followers
--
--  * If I'm B, being removed from the resources of a team of mine:
--          * Do nothing, just waiting for team to send a Revoke on the
--            delegator-Grant
componentRemove
    :: forall topic.
       (PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic)
    => (topic -> KomponentId)
    -> (forall f. f topic -> ComponentBy f)
    -> UTCTime
    -> Key topic
    -> Verse
    -> AP.Remove URIMode
    -> ActE (Text, Act (), Next)
componentRemove grabKomponent topicComponent now topicKey (Verse authorIdMsig body) remove = do

    let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
    (collection, item) <- parseRemove author remove
    case (collection, item) of
        (Left (Left lr), _)
            | lr == topicResource topicKey ->
                removeCollab item
        (Left (Right target), _)
            | addTargetComponentProjects target == Just (topicComponent topicKey) ->
                removeProjectActive item
        (Left (Right target), _)
            | (resourceFromNG <$> addTargetResourceTeams target) == Just meResource ->
                removeTeamActive item
        (_, Left la) | la == resourceToActor (topicResource topicKey) ->
            case collection of
                Left (Right (ATProjectComponents j)) ->
                    removeProjectPassive $ Left j
                Left (Right (ATGroupEfforts g)) ->
                    removeTeamPassive $ Left g
                Right (ObjURI h luColl) -> do
                    -- NOTE this is HTTP GET done synchronously in the activity
                    -- handler
                    manager <- asksEnv envHttpManager
                    c <- AP.fetchAPID_T manager (AP.collectionId :: AP.Collection FedURI URIMode -> LocalURI) h luColl
                    lu <- fromMaybeE (AP.collectionContext c) "No context"
                    rwc <- AP.fetchRWC_T manager h lu
                    AP.Actor l d <-
                        case AP.rwcResource rwc of
                            AP.ResourceActor a -> pure a
                            AP.ResourceChild _ _ -> throwE "Remove.target remote ResourceChild"
                    let typ = AP.actorType d
                    if typ == AP.ActorTypeProject && Just luColl == AP.rwcComponents rwc
                        then removeProjectPassive $ Right $ ObjURI h lu
                    else if typ == AP.ActorTypeTeam && Just luColl == AP.rwcTeamResources rwc
                        then removeTeamPassive $ Right $ ObjURI h lu
                    else throwE "Weird collection situation"
                _ -> throwE "I'm being removed from somewhere irrelevant"
        _ -> throwE "This Remove isn't for me"

    where

    toComponent = topicComponent
    meID = topicKey

    meComponent = toComponent meID
    meResource = componentResource meComponent
    meActor = resourceToActor meResource

    topicResource :: forall f. f topic -> LocalResourceBy f
    topicResource = componentResource . topicComponent

    removeCollab item = do

        memberByKey <-
            bitraverse
                (\case
                    LocalActorPerson p -> pure p
                    _ -> throwE "Not accepting non-person actors as collabs"
                )
                pure
                item

        -- Check capability
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the specified capability gives relevant access
        verifyCapability'' uCap authorIdMsig (topicResource topicKey) AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Find member in our DB
            memberDB <-
                bitraverse
                    (flip getEntityE "Member not found in DB")
                    (\ u@(ObjURI h lu) -> (,u) <$> do
                        maybeActor <- lift $ runMaybeT $ do
                            iid <- MaybeT $ getKeyBy $ UniqueInstance h
                            roid <- MaybeT $ getKeyBy $ UniqueRemoteObject iid lu
                            MaybeT $ getBy $ UniqueRemoteActor roid
                        fromMaybeE maybeActor "Remote removee not found in DB"
                    )
                    memberByKey

            -- Grab me from DB
            komponentID <- lift $ grabKomponent <$> getJust topicKey
            Komponent resourceID <- lift $ getJust komponentID
            Resource topicActorID <- lift $ getJust resourceID
            topicActor <- lift $ getJust topicActorID

            -- Find the collab that the member already has for me
            existingCollabIDs <-
                lift $ case memberDB of
                    Left (Entity personID _) ->
                        fmap (map $ over _1 Left) $
                        E.select $ E.from $ \ (collab `E.InnerJoin` recipl) -> do
                            E.on $
                                collab E.^. CollabId E.==.
                                recipl E.^. CollabRecipLocalCollab
                            E.where_ $
                                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                                recipl E.^. CollabRecipLocalPerson E.==. E.val personID
                            return
                                ( recipl E.^. persistIdField
                                , recipl E.^. CollabRecipLocalCollab
                                )
                    Right (Entity remoteActorID _, _) ->
                        fmap (map $ over _1 Right) $
                        E.select $ E.from $ \ (collab `E.InnerJoin` recipr) -> do
                            E.on $
                                collab E.^. CollabId E.==.
                                recipr E.^. CollabRecipRemoteCollab
                            E.where_ $
                                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                                recipr E.^. CollabRecipRemoteActor E.==. E.val remoteActorID
                            return
                                ( recipr E.^. persistIdField
                                , recipr E.^. CollabRecipRemoteCollab
                                )
            (recipID, E.Value collabID) <-
                case existingCollabIDs of
                    [] -> throwE "Remove object isn't a member of me"
                    [collab] -> return collab
                    _ -> error "Multiple collabs found for removee"

            -- Verify the Collab is enabled
            maybeEnabled <- lift $ getBy $ UniqueCollabEnable collabID
            Entity enableID (CollabEnable _ grantID) <-
                fromMaybeE maybeEnabled "Remove object isn't a member of me yet"

            -- Verify that at least 1 more enabled Admin collab for me exists
            otherCollabIDs <-
                lift $ E.select $ E.from $ \ (collab `E.InnerJoin` enable) -> do
                    E.on $
                        collab E.^. CollabId E.==.
                        enable E.^. CollabEnableCollab
                    E.where_ $
                        collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                        collab E.^. CollabId E.!=. E.val collabID
                    return $ collab E.^. CollabId
            when (null otherCollabIDs) $
                throwE "No other admins exist, can't remove"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                -- Delete the whole Collab record
                deleteBy $ UniqueCollabDelegLocal enableID
                deleteBy $ UniqueCollabDelegRemote enableID
                delete enableID
                case recipID of
                    Left (E.Value l) -> do
                        deleteBy $ UniqueCollabRecipLocalJoinCollab l
                        deleteBy $ UniqueCollabRecipLocalAcceptCollab l
                        delete l
                    Right (E.Value r) -> do
                        deleteBy $ UniqueCollabRecipRemoteJoinCollab r
                        deleteBy $ UniqueCollabRecipRemoteAcceptCollab r
                        delete r
                fulfills <- do
                    mf <- runMaybeT $ asum
                        [ Left <$> MaybeT (getKeyBy $ UniqueCollabFulfillsLocalTopicCreation collabID)
                        , Right . Left <$> MaybeT (getKeyBy $ UniqueCollabFulfillsInvite collabID)
                        , Right . Right <$> MaybeT (getKeyBy $ UniqueCollabFulfillsJoin collabID)
                        ]
                    maybe (error $ "No fulfills for collabID#" ++ show collabID) pure mf
                case fulfills of
                    Left fc -> delete fc
                    Right (Left fi) -> do
                        deleteBy $ UniqueCollabInviterLocal fi
                        deleteBy $ UniqueCollabInviterRemote fi
                        delete fi
                    Right (Right fj) -> do
                        deleteBy $ UniqueCollabApproverLocal fj
                        deleteBy $ UniqueCollabApproverRemote fj
                        delete fj
                delete collabID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid topicKey
                    let topicByHash = resourceToActor $ topicResource topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare a Revoke activity and insert to my outbox
                revoke@(actionRevoke, _, _, _) <-
                    lift $ prepareRevoke memberDB grantID
                let recipByKey = resourceToActor $ topicResource topicKey
                revokeID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
                _luRevoke <- updateOutboxItem' recipByKey revokeID actionRevoke

                return (topicActorID, sieve, revokeID, revoke, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, revokeID, (actionRevoke, localRecipsRevoke, remoteRecipsRevoke, fwdHostsRevoke), inboxItemID) -> do
                let topicByID = resourceToActor $ topicResource topicKey
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $ sendActivity
                    topicByID topicActorID localRecipsRevoke
                    remoteRecipsRevoke fwdHostsRevoke revokeID actionRevoke
                doneDB inboxItemID "[Collab] Deleted the Grant/Collab, forwarded Remove, sent Revoke"

        where

        prepareRevoke member grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            recipHash <- encodeKeyHashid topicKey
            let topicByHash = resourceToActor $ topicResource recipHash

            memberHash <- bitraverse (encodeKeyHashid . entityKey) pure member

            audRemover <- makeAudSenderOnly authorIdMsig
            let audience =
                    let audMember =
                            case memberHash of
                                Left p ->
                                    AudLocal [LocalActorPerson p] [LocalStagePersonFollowers p]
                                Right (Entity _ actor, ObjURI h lu) ->
                                    AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)
                        audTopic = AudLocal [] [localActorFollowers topicByHash]
                    in  [audRemover, audMember, audTopic]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience audience

                recips = map encodeRouteHome audLocal ++ audRemote
            uRemove <- getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    removeProjectActive item = do

        project <-
            bitraverse
                (\case
                    LocalActorProject j -> pure j
                    _ -> throwE "Local object isn't a Project"
                )
                pure
                item

        -- Check capability
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the specified capability gives relevant access
        verifyCapability'' uCap authorIdMsig (topicResource topicKey) AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Find project in our DB
            projectDB <-
                bitraverse
                    (flip getEntityE "Member not found in DB")
                    (\ u@(ObjURI h lu) -> (,u) <$> do
                        maybeActor <- lift $ runMaybeT $ do
                            iid <- MaybeT $ getKeyBy $ UniqueInstance h
                            roid <- MaybeT $ getKeyBy $ UniqueRemoteObject iid lu
                            MaybeT $ getBy $ UniqueRemoteActor roid
                        fromMaybeE maybeActor "Remote removee not found in DB"
                    )
                    project

            -- Grab me from DB
            komponentID <- lift $ grabKomponent <$> getJust topicKey
            Komponent resourceID <- lift $ getJust komponentID
            Resource topicActorID <- lift $ getJust resourceID
            topicActor <- lift $ getJust topicActorID

            -- Find my Stem record for this project
            existingStemIDs <-
                lift $ case projectDB of
                    Left (Entity projectID _) ->
                        fmap (map $ over _1 Left) $
                        E.select $ E.from $ \ (stem `E.InnerJoin` project) -> do
                            E.on $ stem E.^. StemId E.==. project E.^. StemProjectLocalStem
                            E.where_ $
                                project E.^. StemProjectLocalProject E.==. E.val projectID E.&&.
                                stem E.^. StemHolder E.==. E.val komponentID
                            return
                                ( project E.^. StemProjectLocalId
                                , project E.^. StemProjectLocalStem
                                )
                    Right (Entity remoteActorID _, _) ->
                        fmap (map $ over _1 Right) $
                        E.select $ E.from $ \ (stem `E.InnerJoin` project) -> do
                            E.on $ stem E.^. StemId E.==. project E.^. StemProjectRemoteStem
                            E.where_ $
                                project E.^. StemProjectRemoteProject E.==. E.val remoteActorID E.&&.
                                stem E.^. StemHolder E.==. E.val komponentID
                            return
                                ( project E.^. StemProjectRemoteId
                                , project E.^. StemProjectRemoteStem
                                )
            (recipID, E.Value stemID) <-
                case existingStemIDs of
                    [] -> throwE "Remove object isn't a project of mine"
                    [stem] -> return stem
                    _ -> error "Multiple Stems found for removee"

            -- Verify the Stem is enabled
            maybeDelegatorGrant <-
                lift $
                case recipID of
                    Left (E.Value localID) -> fmap Left <$> getBy (UniqueStemProjectGrantLocalProject localID)
                    Right (E.Value remoteID) -> fmap Right <$> getBy (UniqueStemProjectGrantRemoteProject remoteID)
            delegatorGrant <- fromMaybeE maybeDelegatorGrant "Stem not enabled yet"

            -- Grab start-Grant that I'm going to revoke
            let componentAcceptID =
                    case delegatorGrant of
                        Left (Entity _ (StemProjectGrantLocal ca _ _)) -> ca
                        Right (Entity _ (StemProjectGrantRemote ca _ _)) -> ca
            Entity startID (StemDelegateLocal _ grantID) <- do
                maybeStart <-
                    lift $ getBy $ UniqueStemDelegateLocal componentAcceptID
                fromMaybeE maybeStart "Start-Grant not sent yet"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                -- Delete the whole Stem record
                usOrThem <-
                    requireEitherAlt
                        (getKeyBy $ UniqueStemOriginAdd stemID)
                        (getKeyBy $ UniqueStemOriginInvite stemID)
                        "Neither us nor them"
                        "Both us and them"

                delete startID
                case delegatorGrant of
                    Left (Entity localID _) -> delete localID
                    Right (Entity remoteID _) -> delete remoteID
                case usOrThem of
                    Left usID -> delete usID
                    Right themID -> do
                        deleteBy $ UniqueStemProjectAcceptLocal themID
                        deleteBy $ UniqueStemProjectGestureRemote themID
                        deleteBy $ UniqueStemProjectGestureLocal themID
                        delete themID
                delete componentAcceptID
                deleteBy $ UniqueStemComponentGestureLocal stemID
                deleteBy $ UniqueStemComponentGestureRemote stemID
                case recipID of
                    Left (E.Value localID) -> delete localID
                    Right (E.Value remoteID) -> delete remoteID
                delete stemID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid topicKey
                    let topicByHash = resourceToActor $ topicResource topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare a Revoke activity and insert to my outbox
                revoke@(actionRevoke, _, _, _) <-
                    lift $ prepareRevoke projectDB grantID
                let recipByKey = resourceToActor $ topicResource topicKey
                revokeID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
                _luRevoke <- updateOutboxItem' recipByKey revokeID actionRevoke

                return (topicActorID, sieve, revokeID, revoke, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, revokeID, (actionRevoke, localRecipsRevoke, remoteRecipsRevoke, fwdHostsRevoke), inboxItemID) -> do
                let topicByID = resourceToActor $ topicResource topicKey
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $ sendActivity
                    topicByID topicActorID localRecipsRevoke
                    remoteRecipsRevoke fwdHostsRevoke revokeID actionRevoke
                doneDB inboxItemID "[Project-active] Deleted the Stem, forwarded Remove, sent Revoke"

        where

        prepareRevoke project grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            recipHash <- encodeKeyHashid topicKey
            let topicByHash = resourceToActor $ topicResource recipHash

            projectHash <- bitraverse (encodeKeyHashid . entityKey) pure project

            audRemover <- makeAudSenderOnly authorIdMsig
            let audience =
                    let audProject =
                            case projectHash of
                                Left j ->
                                    AudLocal [LocalActorProject j] [LocalStageProjectFollowers j]
                                Right (Entity _ actor, ObjURI h lu) ->
                                    AudRemote h [lu] (maybeToList $ remoteActorFollowers actor)
                        audMe = AudLocal [] [localActorFollowers topicByHash]
                    in  [audRemover, audProject, audMe]

                (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience audience

                recips = map encodeRouteHome audLocal ++ audRemote
            uRemove <- getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash
            let action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    removeProjectPassive _project = do

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            komponentID <- lift $ grabKomponent <$> getJust topicKey
            Komponent resourceID <- lift $ getJust komponentID
            Resource topicActorID <- lift $ getJust resourceID
            topicActor <- lift $ getJust topicActorID

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid topicKey
                    let topicByHash = resourceToActor $ topicResource topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                return (topicActorID, sieve, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, inboxItemID) -> do
                let topicByID = resourceToActor $ topicResource topicKey
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                doneDB inboxItemID "[Project-passive] Just forwarded Remove"

    removeTeamActive team = do

        -- If team is local, find it in our DB
        -- If team is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        teamDB <-
            bitraverse
                (\case
                    LocalActorGroup g -> withDBExcept $ getEntityE g "Team not found in DB"
                    _ -> throwE "Local proposed team of non-Group type"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Team @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Team isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote team type isn't Team"
                            return (u, actor)
                )
                team

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the sender is authorized by me to remove a team
        verifyCapability''
            uCap
            authorIdMsig
            meResource
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            meKomponentID <- lift $ grabKomponent <$> getJust meID
            Komponent meResourceID <- lift $ getJust meKomponentID
            Resource meActorID <- lift $ getJust meResourceID
            meActorDB <- lift $ getJust meActorID

            -- Verify it's an active team of mine
            squads <- lift $ case teamDB of
                Left (Entity g _) ->
                    fmap (map $ \ (d, a, z, E.Value t, E.Value s) -> (d, a, z, Left (t, s))) $
                    E.select $ E.from $ \ (squad `E.InnerJoin` topic `E.InnerJoin` send `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. SquadUsAcceptId E.==. start E.^. SquadUsStartSquad
                        E.on $ squad E.^. SquadId E.==. accept E.^. SquadUsAcceptSquad
                        E.on $ topic E.^. SquadTopicLocalId E.==. send E.^. SquadThemSendDelegatorLocalTopic
                        E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicLocalSquad
                        E.where_ $
                            squad E.^. SquadHolder E.==. E.val meResourceID E.&&.
                            topic E.^. SquadTopicLocalGroup E.==. E.val g
                        return
                            ( squad E.^. SquadId
                            , send E.^. SquadThemSendDelegatorLocalSquad
                            , start E.^. SquadUsStartId
                            , topic E.^. SquadTopicLocalId
                            , send E.^. SquadThemSendDelegatorLocalId
                            )
                Right (_, Entity a _) ->
                    fmap (map $ \ (d, a, z, E.Value t, E.Value s) -> (d, a, z, Right (t, s))) $
                    E.select $ E.from $ \ (squad `E.InnerJoin` topic `E.InnerJoin` send `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. SquadUsAcceptId E.==. start E.^. SquadUsStartSquad
                        E.on $ squad E.^. SquadId E.==. accept E.^. SquadUsAcceptSquad
                        E.on $ topic E.^. SquadTopicRemoteId E.==. send E.^. SquadThemSendDelegatorRemoteTopic
                        E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicRemoteSquad
                        E.where_ $
                            squad E.^. SquadHolder E.==. E.val meResourceID E.&&.
                            topic E.^. SquadTopicRemoteTopic E.==. E.val a
                        return
                            ( squad E.^. SquadId
                            , send E.^. SquadThemSendDelegatorRemoteSquad
                            , start E.^. SquadUsStartId
                            , topic E.^. SquadTopicRemoteId
                            , send E.^. SquadThemSendDelegatorRemoteId
                            )

            (E.Value squadID, E.Value usAcceptID, E.Value squadStartID, topic) <-
                verifySingleE squads "No squad" "Multiple squads"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox meActorDB) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                -- Delete the whole Squad record
                delete squadStartID
                case topic of
                    Left (_, sendID) -> delete sendID
                    Right (_, sendID) -> delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueSquadOriginUs squadID)
                        (getKeyBy $ UniqueSquadOriginThem squadID)
                        "Neither us nor them"
                        "Both us and them"
                deleteBy $ UniqueSquadUsGestureLocal squadID
                deleteBy $ UniqueSquadUsGestureRemote squadID
                case origin of
                    Left usID -> delete usID
                    Right themID -> do
                        deleteBy $ UniqueSquadThemAcceptLocal themID
                        deleteBy $ UniqueSquadThemAcceptRemote themID
                        deleteBy $ UniqueSquadThemGestureLocal themID
                        deleteBy $ UniqueSquadThemGestureRemote themID
                        delete themID
                delete usAcceptID
                case topic of
                    Left (l, _) -> delete l
                    Right (r, _) -> delete r
                delete squadID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    h <- hashLocalActor meActor
                    return $ makeRecipientSet [] [localActorFollowers h]

                -- Prepare Accept activity
                accept@(actionAccept, _, _, _) <- prepareAccept teamDB
                acceptID <- insertEmptyOutboxItem' (actorOutbox meActorDB) now
                _luAccept <- updateOutboxItem' meActor acceptID actionAccept

                return (meActorID, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                forwardActivity authorIdMsig body meActor topicActorID sieve
                lift $
                    sendActivity
                        meActor topicActorID localRecipsAccept
                        remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "[Team-active] Deleted the Team/Squad, forwarded Remove, sent Accept"

        where

        prepareAccept teamDB = do
            encodeRouteHome <- getEncodeRouteHome

            audRemover <- lift $ makeAudSenderOnly authorIdMsig
            audTeam <-
                case teamDB of
                    Left (Entity g _) -> do
                        h <- encodeKeyHashid g
                        return $ AudLocal [LocalActorGroup h] [LocalStageGroupFollowers h]
                    Right (ObjURI h lu, Entity _ ra) ->
                        return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
            audMe <-
                AudLocal [] . pure . localActorFollowers <$>
                    hashLocalActor meActor
            uRemove <- lift $ getActivityURI authorIdMsig

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audRemover, audTeam, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRemove]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uRemove
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    removeTeamPassive team = do

        -- If team is local, find it in our DB
        -- If team is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        teamDB <-
            bitraverse
                (\ g -> withDBExcept $ getEntityE g "Team not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Team @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Team isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote team type isn't Team"
                            return (u, actor)
                )
                team

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            meKomponentID <- lift $ grabKomponent <$> getJust meID
            Komponent meResourceID <- lift $ getJust meKomponentID
            Resource meActorID <- lift $ getJust meResourceID
            meActorDB <- lift $ getJust meActorID

            -- Verify it's an active team of mine
            squads <- lift $ case teamDB of
                Left (Entity g _) ->
                    fmap (map $ \ (d, a, z, E.Value t, E.Value s) -> (d, a, z, Left (t, s))) $
                    E.select $ E.from $ \ (squad `E.InnerJoin` topic `E.InnerJoin` send `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. SquadUsAcceptId E.==. start E.^. SquadUsStartSquad
                        E.on $ squad E.^. SquadId E.==. accept E.^. SquadUsAcceptSquad
                        E.on $ topic E.^. SquadTopicLocalId E.==. send E.^. SquadThemSendDelegatorLocalTopic
                        E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicLocalSquad
                        E.where_ $
                            squad E.^. SquadHolder E.==. E.val meResourceID E.&&.
                            topic E.^. SquadTopicLocalGroup E.==. E.val g
                        return
                            ( squad E.^. SquadId
                            , send E.^. SquadThemSendDelegatorLocalSquad
                            , start E.^. SquadUsStartId
                            , topic E.^. SquadTopicLocalId
                            , send E.^. SquadThemSendDelegatorLocalId
                            )
                Right (_, Entity a _) ->
                    fmap (map $ \ (d, a, z, E.Value t, E.Value s) -> (d, a, z, Right (t, s))) $
                    E.select $ E.from $ \ (squad `E.InnerJoin` topic `E.InnerJoin` send `E.InnerJoin` accept `E.InnerJoin` start) -> do
                        E.on $ accept E.^. SquadUsAcceptId E.==. start E.^. SquadUsStartSquad
                        E.on $ squad E.^. SquadId E.==. accept E.^. SquadUsAcceptSquad
                        E.on $ topic E.^. SquadTopicRemoteId E.==. send E.^. SquadThemSendDelegatorRemoteTopic
                        E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicRemoteSquad
                        E.where_ $
                            squad E.^. SquadHolder E.==. E.val meResourceID E.&&.
                            topic E.^. SquadTopicRemoteTopic E.==. E.val a
                        return
                            ( squad E.^. SquadId
                            , send E.^. SquadThemSendDelegatorRemoteSquad
                            , start E.^. SquadUsStartId
                            , topic E.^. SquadTopicRemoteId
                            , send E.^. SquadThemSendDelegatorRemoteId
                            )

            (E.Value squadID, E.Value usAcceptID, E.Value squadStartID, topic) <-
                verifySingleE squads "No squad" "Multiple squads"

            maybeRemoveDB <- lift $ insertToInbox now authorIdMsig body (actorInbox meActorDB) False
            lift $ for maybeRemoveDB $ \ (inboxItemID, _removeDB) -> do

                return inboxItemID

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just inboxItemID ->
                doneDB inboxItemID "[Team-passive] Saw the removal attempt, just waiting for the Revoke"

-- Meaning: An actor A asked to join a resource
-- Behavior:
--      * Verify the resource is me
--      * Verify A doesn't already have an invite/join/grant for me
--      * Remember the join in DB
--      * Forward the Join to my followers
topicJoin
    :: (PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic)
    => (topic -> ResourceId)
    -> (forall f. f topic -> LocalResourceBy f)
    -> UTCTime
    -> Key topic
    -> Verse
    -> AP.Join URIMode
    -> ActE (Text, Act (), Next)
topicJoin grabResource topicResource now topicKey (Verse authorIdMsig body) join = do

    -- Check input
    (role, resource) <- parseJoin join
    unless (resource == Left (topicResource topicKey)) $
        throwE "Join's object isn't my collabs URI, don't need this Join"

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        resourceID <- lift $ grabResource <$> getJust topicKey
        Resource topicActorID <- lift $ getJust resourceID
        topicActor <- lift $ getJust topicActorID

        -- Verify that target doesn't already have a Collab for me
        existingCollabIDs <- lift $
            case authorIdMsig of
                Left (LocalActorPerson personID, _, _) ->
                    E.select $ E.from $ \ (collab `E.InnerJoin` recipl) -> do
                        E.on $
                            collab E.^. CollabId E.==.
                            recipl E.^. CollabRecipLocalCollab
                        E.where_ $
                            collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                            recipl E.^. CollabRecipLocalPerson E.==. E.val personID
                        return $ recipl E.^. CollabRecipLocalCollab
                Left (_, _, _) -> pure []
                Right (author, _, _) -> do
                    let targetID = remoteAuthorId author
                    E.select $ E.from $ \ (collab `E.InnerJoin` recipr) -> do
                        E.on $
                            collab E.^. CollabId E.==.
                            recipr E.^. CollabRecipRemoteCollab
                        E.where_ $
                            collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                            recipr E.^. CollabRecipRemoteActor E.==. E.val targetID
                        return $ recipr E.^. CollabRecipRemoteCollab
        case existingCollabIDs of
            [] -> pure ()
            [_] -> throwE "I already have a Collab for the target"
            _ -> error "Multiple collabs found for target"

        maybeJoinDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
        for maybeJoinDB $ \ (inboxItemID, joinDB) -> do

            -- Insert Collab record to DB
            joinDB' <-
                bitraverse
                    (\ (authorByKey, _, joinID) ->
                        case authorByKey of
                            LocalActorPerson personID -> pure (personID, joinID)
                            _ -> throwE "Non-person local actors can't get Grants currently"
                    )
                    pure
                    joinDB
            lift $ insertCollab resourceID role joinDB'

            -- Prepare forwarding Join to my followers
            sieve <- lift $ do
                topicHash <- encodeKeyHashid topicKey
                let topicByHash = resourceToActor $ topicResource topicHash
                return $ makeRecipientSet [] [localActorFollowers topicByHash]
            return (topicActorID, sieve, inboxItemID)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (topicActorID, sieve, inboxItemID) -> do
            let topicByID = resourceToActor $ topicResource topicKey
            forwardActivity authorIdMsig body topicByID topicActorID sieve
            doneDB inboxItemID "Recorded and forwarded the Join"

    where

    insertCollab resourceID role joinDB = do
        collabID <- insert $ Collab role resourceID
        fulfillsID <- insert $ CollabFulfillsJoin collabID
        case joinDB of
            Left (personID, joinID) -> do
                recipID <- insert $ CollabRecipLocal collabID personID
                insert_ $ CollabRecipLocalJoin recipID fulfillsID joinID
            Right (author, _, joinID) -> do
                let authorID = remoteAuthorId author
                recipID <- insert $ CollabRecipRemote collabID authorID
                insert_ $ CollabRecipRemoteJoin recipID fulfillsID joinID

-- Meaning: Someone has created an actor with my ID URI
-- Behavior:
--     * Verify I have no Collab records, implying just-been-created state
--     * Verify my (local!) creator and the Create sender are the same actor
--     * Possibly: Verify the creator is in the can-create-factories list
--     * Possibly:
--          If I'm the first in my table and listed as resident (or no
--          residents are listed),
--          send out develop-Grants (and create Collab records) to all verified
--          local Persons
--     * Create an admin Collab record in DB
--     * Send an admin Grant to the creator
topicCreateMe
    :: (PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic)
    => Bool
    -> Bool
    -> (topic -> ResourceId)
    -> (forall f. f topic -> LocalResourceBy f)
    -> UTCTime
    -> Key topic
    -> Verse
    -> ActE (Text, Act (), Next)
topicCreateMe checkCan sendGrants grabResource meToResource now meID (Verse authorIdMsig body) = do

    maybeNew <- withDBExcept $ do

        -- Grab me from DB
        resourceMeID <- lift $ grabResource <$> getJust meID
        Resource actorMeID <- lift $ getJust resourceMeID
        actorMe <- lift $ getJust actorMeID

        -- Verify I'm in initial state
        creatorActorID <- do
            create <-
                lift $
                requireEitherAlt
                    (getValBy $ UniqueActorCreateLocalActor actorMeID)
                    (getValBy $ UniqueActorCreateRemoteActor actorMeID)
                    "Neither local nor remote"
                    "Both local and remote"
            case create of
                Left (ActorCreateLocal _ createID) -> do
                    OutboxItem outboxID _ _ <- lift $ getJust createID
                    mk <- lift $ getKeyBy $ UniqueActorOutbox outboxID
                    fromMaybeE mk "Creator actor not found"
                Right _ -> error "topicCreateMe used on a remotely-created actor"
        creatorPersonID <- do
            mp <- lift $ getKeyBy $ UniquePersonActor creatorActorID
            fromMaybeE mp "Granting access to local non-Person actors isn't suppported currently"
        existingCollabIDs <-
            lift $ selectList [CollabTopic ==. resourceMeID] []
        unless (null existingCollabIDs) $
            throwE "I already have Collab records"

        -- Verify the Create author is my creator indeed
        case authorIdMsig of
            Left (_, actorID, _) | actorID == creatorActorID -> pure ()
            _ -> throwE "Create author isn't why I believe my creator is - is this Create fake?"

        -- Verify creator is in can-create-factories list
        when checkCan $ do
            u <- lift $ personUsername <$> getJust creatorPersonID
            cans <- asksEnv $ appCanCreateFactories . envSettings
            unless (u `elem` map text2username cans) $
                throwE "Creator person isn't in can-create-factories list"

        maybeCreateDB <- lift $ insertToInbox now authorIdMsig body (actorInbox actorMe) False
        lift $ for maybeCreateDB $ \ (inboxItemID, _createDB) -> do

            -- Create a Collab record
            grantID <- insertEmptyOutboxItem' (actorOutbox actorMe) now
            insertCollab resourceMeID creatorPersonID grantID

            -- Prepare a Grant activity and insert to my outbox
            grant@(actionGrant, _, _, _) <- lift prepareGrant
            let recipByKey = resourceToActor $ meToResource meID
            _luGrant <- updateOutboxItem' recipByKey grantID actionGrant

            writes <-
                if sendGrants
                    then do
                        residents <- asksEnv $ appResidentFactories . envSettings
                        meHash <- encodeKeyHashid meID
                        let meHashText = keyHashidText meHash
                        ids <- selectKeysList [] []
                        let meResident =
                                null residents || meHashText `elem` residents
                        if meResident && ids == [meID]
                            then do
                                ps <- selectList [PersonId !=. creatorPersonID, PersonVerified ==. True] []
                                for ps $ \ p@(Entity personID _) -> do

                                    writeID <- insertEmptyOutboxItem' (actorOutbox actorMe) now
                                    insertResidentCollab resourceMeID personID writeID

                                    write@(actionWrite, _, _, _) <- prepareResidentGrant p
                                    let recipByKey = resourceToActor $ meToResource meID
                                    _luWrite <- updateOutboxItem' recipByKey writeID actionWrite

                                    return (writeID, write)
                            else pure []
                    else pure []

            return (actorMeID, grantID, grant, writes, inboxItemID)

    case maybeNew of
        Nothing -> done "I already have this activity in my inbox"
        Just (actorMeID, grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant), writes, inboxItemID) -> do
            let recipByID = resourceToActor $ meToResource meID
            lift $ do
                sendActivity
                    recipByID actorMeID localRecipsGrant
                    remoteRecipsGrant fwdHostsGrant grantID actionGrant
                for_ writes $ \ (writeID, (actionWrite, localRecipsWrite, remoteRecipsWrite, fwdHostsWrite)) ->
                    sendActivity
                        recipByID actorMeID localRecipsWrite
                        remoteRecipsWrite fwdHostsWrite writeID actionWrite
            doneDB inboxItemID "Created a Collab record and published a Grant, possibly sent write-Grants"

    where

    insertCollab resourceMeID personID grantID = do
        collabID <- insert $ Collab AP.RoleAdmin resourceMeID
        insert_ $ CollabEnable collabID grantID
        insert_ $ CollabRecipLocal collabID personID
        insert_ $ CollabFulfillsLocalTopicCreation collabID

    insertResidentCollab resourceMeID personID grantID = do
        collabID <- insert $ Collab AP.RoleWrite resourceMeID
        insert_ $ CollabEnable collabID grantID
        insert_ $ CollabRecipLocal collabID personID
        insert_ $ CollabFulfillsResidentFactory collabID

    prepareGrant = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        audCreator <- makeAudSenderOnly authorIdMsig
        recipHash <- encodeKeyHashid meID
        uCreator <- getActorURI authorIdMsig
        uCreate <- getActivityURI authorIdMsig
        let topicByHash = resourceToActor $ meToResource recipHash
            audience =
                let audTopic = AudLocal [] [localActorFollowers topicByHash]
                in  [audCreator, audTopic]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uCreate]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXRole AP.RoleAdmin
                    , AP.grantContext   =
                        encodeRouteHome $ renderLocalActor topicByHash
                    , AP.grantTarget    = uCreator
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    prepareResidentGrant (Entity personID person) = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        personHash <- encodeKeyHashid personID
        let audPerson = AudLocal [LocalActorPerson personHash] []
        meHash <- encodeKeyHashid meID
        uCreate <- do
            selfCreateID <- do
                mc <- getValBy $ UniqueActorCreateLocalActor $ personActor person
                case mc of
                    Nothing -> error "Person doesn't have an ActorCreateLocal record"
                    Just c -> pure $ actorCreateLocalCreate c
            createHash <- encodeKeyHashid selfCreateID
            return $ encodeRouteHome $ PersonOutboxItemR personHash createHash
        let topicByHash = resourceToActor $ meToResource meHash
            audience =
                let audTopic = AudLocal [] [localActorFollowers topicByHash]
                in  [audPerson, audTopic]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uCreate]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXRole AP.RoleWrite
                    , AP.grantContext   =
                        encodeRouteHome $ renderLocalActor topicByHash
                    , AP.grantTarget    = encodeRouteHome $ PersonR personHash
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: I've just been created
-- Behavior:
--     * Verify my creator and the Create sender are the same actor
--     * Create an admin Collab record in DB
--     * Send an admin Grant to the creator
--     * If migration is allowed and source specified, pull data from source
--       and fill my data with copies
topicInit
    :: ( PersistRecordBackend topic SqlBackend
       , ToBackendKey SqlBackend topic
       , FromJSON f3
       )
    => AP.ActorType
    -> (Key topic -> Either (Entity topic) (FedURI, Entity RemoteActor, f3) -> ActDBE ())
    -> (topic -> ActDB ResourceId)
    -> (forall f. f topic -> LocalResourceBy f)
    -> UTCTime
    -> Key topic
    -> Either (LocalActorBy Key, ActorId, OutboxItemId) (RemoteAuthor, LocalURI)
    -> Maybe (Either (Key topic) FedURI)
    -> ActE (Text, Act (), Next)
topicInit meType fillData grabResource meToResource now meID creator maybeOrigin = do

    allowOrigin <- asksEnv $ appDeckOrigin . envSettings
    when (not allowOrigin && isJust maybeOrigin) $
        throwE "Migration disabled but origin provided"

    maybeOrigin' <-
        for maybeOrigin $
            bitraverse
                (\ originID ->
                    withDBExcept $ getEntityE originID "Local origin not found in DB"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Origin @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Origin isn't an actor"
                        Right (Just actor) -> do
                            if remoteActorType (entityVal actor) == meType
                                then pure ()
                                else throwE "Remote origin type doesn't match mine"
                            f3 <- do
                                manager <- asksEnv envHttpManager
                                req <- liftIO $ requestFromURI $ uriFromObjURI u
                                responseBody <$> httpF3 manager req
                            return (u, actor, f3)
                )

    (actorMeID, grantID, (actionGrant, localRecipsGrant, remoteRecipsGrant, fwdHostsGrant)) <- withDBExcept $ do

        -- Grab me from DB
        resourceMeID <- lift $ grabResource =<< getJust meID
        Resource actorMeID <- lift $ getJust resourceMeID
        actorMe <- lift $ getJust actorMeID

        -- Verify I don't have any Collab records
        collabIDs <- lift $ selectKeysList [CollabTopic ==. resourceMeID] []
        unless (null collabIDs) $
            throwE "I already have Collab records"

        -- Verify my creator in DB and the one passed to me are the same actor
        create <-
            lift $
            requireEitherAlt
                (getValBy $ UniqueActorCreateLocalActor actorMeID)
                (getValBy $ UniqueActorCreateRemoteActor actorMeID)
                "Neither local nor remote"
                "Both local and remote"
        let create' =
                bimap actorCreateLocalCreate actorCreateRemoteSender create
            creator' =
                bimap (view _3) (remoteAuthorId . fst) creator
        unless (create' == creator') $
            throwE "Creator in DB and in argument aren't the same"

        -- If creator is local, verify it's a Person, because the DB schema
        -- currently allows only Person to be the recipient of a Collab
        let verifyIsPerson = \case
                LocalActorPerson p -> pure p
                _ -> throwE "Local creator isn't a Person"
        creatorPerson <-
            traverseOf _Left (traverseOf _1 verifyIsPerson) creator

        -- Create a Collab record
        grantID <- lift $ insertEmptyOutboxItem' (actorOutbox actorMe) now
        lift $ insertCollab resourceMeID creatorPerson grantID

        -- Prepare a Grant activity and insert to my outbox
        grant@(actionGrant, _, _, _) <- lift $ lift prepareGrant
        let recipByKey = resourceToActor $ meToResource meID
        _luGrant <- lift $ updateOutboxItem' recipByKey grantID actionGrant

        traverse_ (fillData meID) maybeOrigin'

        return (actorMeID, grantID, grant)

    let recipByID = resourceToActor $ meToResource meID
    lift $ sendActivity
        recipByID actorMeID localRecipsGrant
        remoteRecipsGrant fwdHostsGrant grantID actionGrant
    done "Created a Collab record and published a Grant"

    where

    insertCollab resourceMeID creatorPerson grantID = do
        collabID <- insert $ Collab AP.RoleAdmin resourceMeID
        insert_ $ CollabEnable collabID grantID
        case creatorPerson of
            Left (personID, _, _) ->
                insert_ $ CollabRecipLocal collabID personID
            Right (author, _) ->
                insert_ $ CollabRecipRemote collabID (remoteAuthorId author)
        insert_ $ CollabFulfillsLocalTopicCreation collabID

    prepareGrant = do
        encodeRouteHome <- getEncodeRouteHome
        encodeRouteLocal <- getEncodeRouteLocal

        let creatorMsig = second (\ (ra, lu) -> (ra, lu, Nothing)) creator
        audCreator <- makeAudSenderOnly creatorMsig
        meHash <- encodeKeyHashid meID
        uCreator <- getActorURI creatorMsig
        uCreate <- getActivityURI creatorMsig
        let meActorHash = resourceToActor $ meToResource meHash
            audience =
                let audMe = AudLocal [] [localActorFollowers meActorHash]
                in  [audCreator, audMe]

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience audience

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Nothing
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uCreate]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXRole AP.RoleAdmin
                    , AP.grantContext   =
                        encodeRouteHome $ renderLocalActor meActorHash
                    , AP.grantTarget    = uCreator
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.Invoke
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor is granting access-to-some-resource to another actor
-- Behavior:
--      * If I approved an Add-to-project where I'm the component, and the
--        project is now giving me the delegator-Grant:
--          * Record this in the Stem record in DB
--          * Forward to my followers
--          * Start a delegation chain giving access-to-me, send this new Grant
--            to the project to distribute further, and use the delegator-Grant
--            as the capability
--              * To: Project
--              * CC: My followers, project followers
--
--      * If I approved an Invite-to-project where I'm the component, and the
--        project is now giving me the delegator-Grant:
--          * Record this in the Stem record in DB
--          * Forward to my followers
--          * Start a delegation chain giving access-to-me, send this new Grant
--            to the project to distribute further, and use the delegator-Grant
--            as the capability
--              * To: Project
--              * CC: My followers, project followers
--
--      * If the Grant is for an Add/Invite that hasn't had the full approval
--        chain, or I already got the delegator-Grant, raise an error
--
--      * Almost-Team sending me the delegator-Grant
--          * Update the Squad record, enabling the team
--          * Send a start-Grant giving access-to-me
--
--      * Otherwise, if I've already seen this Grant or it's simply not related
--        to me, ignore it
componentGrant
    :: forall topic.
       (PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic)
    => (topic -> ResourceId)
    -> (forall f. f topic -> ComponentBy f)
    -> UTCTime
    -> Key topic
    -> Verse
    -> AP.Grant URIMode
    -> ActE (Text, Act (), Next)
componentGrant grabResource topicComponent now recipKey (Verse authorIdMsig body) grant = do

    grant' <- checkGrant grant
    let adapt = maybe (Right Nothing) (either Left (Right . Just))
    maybeMode <-
        withDBExcept $ do
            meResourceID <- lift $ grabResource <$> getJust recipKey
            ExceptT $ fmap adapt $ runMaybeT $
                runExceptT (Left <$> tryStem grant') <|>
                runExceptT (Right <$> tryTeam meResourceID grant')
    mode <-
        fromMaybeE
            maybeMode
            "Not a relevant Grant that I'm aware of"
    case mode of
        Left stem -> handleStem stem
        Right team -> handleTeam team

    where

    tryStem (GKDelegationStart _)    = lift mzero
    tryStem (GKDelegationExtend _ _) = lift mzero
    tryStem GKDelegator              = do
        uFulfills <-
            case AP.activityFulfills $ actbActivity body of
                [] -> throwE "No fulfills"
                [u] -> pure u
                _ -> throwE "Multiple fulfills"
        fulfills <- ExceptT $ lift $ lift $ runExceptT $ first (\ (a, _, i) -> (a, i)) <$> parseActivityURI' uFulfills
        fulfillsDB <- ExceptT $ MaybeT $ either (Just . Left) (fmap Right) <$> runExceptT (getActivity fulfills)
        -- See if the fulfilled activity is an Invite or Add on a local
        -- component, grabbing the Stem record from our DB
        stem <-
            lift $
                Left <$> tryInviteComp fulfillsDB <|>
                Right <$> tryAddComp fulfillsDB
        -- Find the local component and verify it's me
        let stemID = either id id stem
        ident <- lift $ lift $ getStemIdent stemID
        lift $ guard $ topicComponent recipKey == ident
        -- Find the project, verify it's identical to the Grant sender
        stemProject <-
            lift $ lift $
            requireEitherAlt
                (getBy $ UniqueStemProjectLocal stemID)
                (getBy $ UniqueStemProjectRemote stemID)
                "Found Stem with no project"
                "Found Stem with multiple projects"
        case (stemProject, authorIdMsig) of
            (Left (Entity _ sjl), Left (LocalActorProject projectID, _, _))
                | stemProjectLocalProject sjl == projectID ->
                    return ()
            (Right (Entity _ sjr), Right (author, _, _))
                | stemProjectRemoteProject sjr == remoteAuthorId author ->
                    return ()
            _ -> throwE "The Grant I'm waiting for is by the project"
        -- Verify I sent the Component's Accept but haven't already received
        -- the delegator-Grant
        compAccept <- do
            mk <- lift $ lift $ getKeyBy $ UniqueStemComponentAccept stemID
            fromMaybeE mk "Getting a delegator-Grant but never approved this Invite/Add"
        gl <- lift $ lift $ getBy $ UniqueStemProjectGrantLocal compAccept
        gr <- lift $ lift $ getBy $ UniqueStemProjectGrantRemote compAccept
        unless (isNothing gl && isNothing gr) $
            throwE "I already received a delegator-Grant for this Invite/Add"
        return (stemID, stemProject, compAccept)

    handleStem (stemID, stemProject, compAccept) = do

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            resourceID <- lift $ grabResource <$> getJust recipKey
            Resource recipActorID <- lift $ getJust resourceID
            recipActor <- lift $ getJust recipActorID

            maybeGrantDB <- lift $ insertToInbox now authorIdMsig body (actorInbox recipActor) False
            lift $ for maybeGrantDB $ \ (inboxItemID, grantDB) -> do

                -- Prepare forwarding to my followers
                sieve <- do
                    recipHash <- encodeKeyHashid recipKey
                    let recipByHash = resourceToActor $ topicResource recipHash
                    return $ makeRecipientSet [] [localActorFollowers recipByHash]

                -- Update the Stem record in DB
                case (stemProject, grantDB) of
                    (Left (Entity j _), Left (_, _, g)) -> insert_ $ StemProjectGrantLocal compAccept j g
                    (Right (Entity j _), Right (_, _, g)) -> insert_ $ StemProjectGrantRemote compAccept j g
                    _ -> error "componentGrant impossible"
                chainID <- insertEmptyOutboxItem' (actorOutbox recipActor) now
                insert_ $ StemDelegateLocal compAccept chainID

                -- Prepare a Grant activity and insert to my outbox
                chain <- do
                    Stem role _ <- getJust stemID
                    chain@(actionChain, _, _, _) <- prepareChain role
                    let recipByKey = resourceToActor $ topicResource recipKey
                    _luChain <- updateOutboxItem' recipByKey chainID actionChain
                    return chain

                return (recipActorID, sieve, chainID, chain, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, sieve, chainID, (actionChain, localRecipsChain, remoteRecipsChain, fwdHostsChain), inboxItemID) -> do
                let recipByID = resourceToActor $ topicResource recipKey
                forwardActivity authorIdMsig body recipByID recipActorID sieve
                lift $ sendActivity
                    recipByID recipActorID localRecipsChain remoteRecipsChain
                    fwdHostsChain chainID actionChain
                doneDB inboxItemID "Recorded and forwarded the delegator-Grant, sent a delegation-starter Grant"

    meID = recipKey
    toComponent = topicComponent

    meComponent = toComponent recipKey
    meResource = componentResource meComponent
    meActor = resourceToActor meResource

    topicResource :: forall f. f topic -> LocalResourceBy f
    topicResource = componentResource . topicComponent

    checkGrant g = do
        (role, resource, recipient, _mresult, mstart, mend, usage, mdeleg) <-
            parseGrant' g
        case recipient of
            Left la | la == meActor -> pure ()
            _ -> throwE "Target isn't me"
        for_ mstart $ \ start ->
            unless (start < now) $ throwE "Start time is in the future"
        for_ mend $ \ _ ->
            throwE "End time is specified"

        let resourceIsAuthor =
                case (resource, authorIdMsig) of
                    (Left a, Left (a', _, _)) -> a == a'
                    (Right u, Right (ra, _, _)) -> remoteAuthorURI ra == u
                    _ -> False

        case (role, resourceIsAuthor, usage, mdeleg) of
            (AP.RXRole r, True, AP.GatherAndConvey, Nothing) ->
                pure $ GKDelegationStart r
            (AP.RXRole r, False, AP.GatherAndConvey, Just _) ->
                pure $ GKDelegationExtend r resource
            (AP.RXDelegator, True, AP.Invoke, Nothing) ->
                pure GKDelegator
            _ -> throwE "A kind of Grant that I don't use"

    tryInviteComp (Left (_, _, itemID)) = do
        originInviteID <-
            stemProjectGestureLocalOrigin <$>
                MaybeT (getValBy $ UniqueStemProjectGestureLocalInvite itemID)
        lift $ stemOriginInviteStem <$> getJust originInviteID
    tryInviteComp (Right remoteActivityID) = do
        StemProjectGestureRemote originInviteID _ _ <-
            MaybeT $ getValBy $
                UniqueStemProjectGestureRemoteInvite remoteActivityID
        lift $ stemOriginInviteStem <$> getJust originInviteID

    tryAddComp (Left (_, __, itemID)) = do
        StemComponentGestureLocal stemID _ <-
            MaybeT $ getValBy $ UniqueStemComponentGestureLocalActivity itemID
        _originID <- MaybeT $ getKeyBy $ UniqueStemOriginAdd stemID
        return stemID
    tryAddComp (Right remoteActivityID) = do
        StemComponentGestureRemote stemID _ _ <-
            MaybeT $ getValBy $
                UniqueStemComponentGestureRemoteActivity remoteActivityID
        _originID <- MaybeT $ getKeyBy $ UniqueStemOriginAdd stemID
        return stemID

    prepareChain role = do
        encodeRouteHome <- getEncodeRouteHome

        audProject <- makeAudSenderWithFollowers authorIdMsig
        audMe <-
            AudLocal [] . pure . localActorFollowers .
            resourceToActor . topicResource <$> encodeKeyHashid recipKey
        uProject <- lift $ getActorURI authorIdMsig
        uGrant <- lift $ getActivityURI authorIdMsig
        recipHash <- encodeKeyHashid recipKey
        let topicByHash = resourceToActor $ topicResource recipHash

            (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                collectAudience [audProject, audMe]

            recips = map encodeRouteHome audLocal ++ audRemote
            action = AP.Action
                { AP.actionCapability = Just uGrant
                , AP.actionSummary    = Nothing
                , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                , AP.actionFulfills   = [uGrant]
                , AP.actionSpecific   = AP.GrantActivity AP.Grant
                    { AP.grantObject    = AP.RXRole role
                    , AP.grantContext   =
                        encodeRouteHome $ renderLocalActor topicByHash
                    , AP.grantTarget    = uProject
                    , AP.grantResult    = Nothing
                    , AP.grantStart     = Just now
                    , AP.grantEnd       = Nothing
                    , AP.grantAllows    = AP.GatherAndConvey
                    , AP.grantDelegates = Nothing
                    }
                }

        return (action, recipientSet, remoteActors, fwdHosts)

    tryTeam _            (GKDelegationStart _)    = lift mzero
    tryTeam _            (GKDelegationExtend _ _) = lift mzero
    tryTeam meResourceID GKDelegator              = do
        uFulfills <-
            case AP.activityFulfills $ actbActivity body of
                [] -> throwE "No fulfills"
                [u] -> pure u
                _ -> throwE "Multiple fulfills"
        fulfills <- ExceptT $ lift $ lift $ runExceptT $ first (\ (a, _, i) -> (a, i)) <$> parseActivityURI' uFulfills
        fulfillsDB <- ExceptT $ MaybeT $ either (Just . Left) (fmap Right) <$> runExceptT (getActivity fulfills)
        -- Find the Squad record from the fulfills
        squadID <-
            lift $
            case fulfillsDB of
                Left (_, _, addID) ->
                    (do SquadUsGestureLocal squadID _ <- MaybeT $ getValBy $ UniqueSquadUsGestureLocalActivity addID
                        _ <- MaybeT $ getBy $ UniqueSquadOriginUs squadID
                        return squadID
                    )
                    <|>
                    (do SquadThemGestureLocal themID _ <- MaybeT $ getValBy $ UniqueSquadThemGestureLocalAdd addID
                        SquadOriginThem squadID <- lift $ getJust themID
                        return squadID
                    )
                Right addID ->
                    (do SquadUsGestureRemote squadID _ _ <- MaybeT $ getValBy $ UniqueSquadUsGestureRemoteActivity addID
                        _ <- MaybeT $ getBy $ UniqueSquadOriginUs squadID
                        return squadID
                    )
                    <|>
                    (do SquadThemGestureRemote themID _ _ <- MaybeT $ getValBy $ UniqueSquadThemGestureRemoteAdd addID
                        SquadOriginThem squadID <- lift $ getJust themID
                        return squadID
                    )
        -- Verify this Squad record is mine
        Squad role r <- lift $ lift $ getJust squadID
        lift $ guard $ r == meResourceID
        -- Verify the Grant sender is the Squad topic
        topic <- lift $ lift $ getSquadTeam squadID
        topicForCheck <-
            lift $ lift $
            bitraverse
                (pure . snd)
                (\ (_, raID) -> getRemoteActorURI =<< getJust raID)
                topic
        unless (first LocalActorGroup topicForCheck == bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig) $
            throwE "Squad topic and Grant author aren't the same actor"
        -- Verify I sent my Accept
        maybeMe <- lift $ lift $ getKeyBy $ UniqueSquadUsAccept squadID
        meAcceptID <- fromMaybeE maybeMe "I haven't sent my Accept"
        -- Verify I haven't yet seen a delegator-Grant from the team
        case bimap fst fst topic of
            Left localID -> do
                m <- lift $ lift $ getBy $ UniqueSquadThemSendDelegatorLocalTopic localID
                verifyNothingE m "Already have a SquadThemSendDelegatorLocal"
            Right remoteID -> do
                m <- lift $ lift $ getBy $ UniqueSquadThemSendDelegatorRemoteTopic remoteID
                verifyNothingE m "Already have a SquadThemSendDelegatorRemote"
        return (role, topic, meAcceptID)

    handleTeam (role, topic, acceptID) = do

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            meResourceID <- lift $ grabResource <$> getJust meID
            Resource meActorID <- lift $ getJust meResourceID
            meActorDB <- lift $ getJust meActorID

            maybeGrantDB <- lift $ insertToInbox now authorIdMsig body (actorInbox meActorDB) False
            for maybeGrantDB $ \ (inboxItemID, grantDB) -> do

                -- Record the delegator-Grant in DB
                to <- case (grantDB, bimap fst fst topic) of
                    (Left (_, _, grantID), Left localID) -> Left <$> do
                        mk <- lift $ insertUnique $ SquadThemSendDelegatorLocal acceptID localID grantID
                        fromMaybeE mk "I already have such a SquadThemSendDelegatorLocal"
                    (Right (_, _, grantID), Right remoteID) -> Right <$> do
                        mk <- lift $ insertUnique $ SquadThemSendDelegatorRemote acceptID remoteID grantID
                        fromMaybeE mk "I already have such a SquadThemSendDelegatorRemote"
                    _ -> error "componentGrant.team impossible"

                startID <- lift $ insertEmptyOutboxItem' (actorOutbox meActorDB) now
                squadStartID <- lift $ insert $ SquadUsStart acceptID startID

                -- Prepare a start-Grant
                start@(actionStart, _, _, _) <- lift $ prepareStartGrant role squadStartID
                _luStart <- lift $ updateOutboxItem' meActor startID actionStart

                return (meActorID, startID, start, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (recipActorID, extID, (actionExt, localRecipsExt, remoteRecipsExt, fwdHostsExt), inboxItemID) -> do
                lift $ sendActivity
                            meActor recipActorID localRecipsExt
                            remoteRecipsExt fwdHostsExt extID actionExt
                doneDB inboxItemID "[Team] Sent start-Grant"

        where

        prepareStartGrant role startID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            meHash <- hashLocalActor meActor

            uDeleg <- lift $ getActivityURI authorIdMsig

            audTeam <- lift $ makeAudSenderOnly authorIdMsig
            uTeam <- lift $ getActorURI authorIdMsig

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audTeam]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Just uDeleg
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uDeleg]
                    , AP.actionSpecific   = AP.GrantActivity AP.Grant
                        { AP.grantObject    = AP.RXRole role
                        , AP.grantContext   = encodeRouteHome $ renderLocalActor meHash
                        , AP.grantTarget    = uTeam
                        , AP.grantResult    = Nothing
                        , AP.grantStart     = Just now
                        , AP.grantEnd       = Nothing
                        , AP.grantAllows    = AP.Distribute
                        , AP.grantDelegates = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

-- Meaning: An actor is adding some object to some target
-- Behavior:
--  * If target is my context (i.e. parents) collection:
--      * Verify the object is a project
--      * Verify the Add is authorized
--      * For all the Stem records I have for this project:
--          * Verify I'm not yet a member of the project
--          * Verify I haven't already Accepted an our-Add to this project
--          * Verify I haven't already seen an them-Invite-and-Project-accept for
--            this project
--      * Insert the Add to my inbox
--      * Create a Stem record in DB
--      * Forward the Add activity to my followers
--      * Send an Accept on the Add:
--          * To:
--              * The author of the Add
--              * The project
--          * CC:
--              * Author's followers
--              * Project's followers
--              * My followers
--
--  * If the target is my teams list:
--        * Verify the object is a team, find in DB/HTTP
--        * Verify the Add is authorized
--        * Verify it's not already an active team of mine
--        * Verify it's not already in an Origin-Us process where I saw the Add
--          and sent my Accept
--        * Verify it's not already in an Origin-Them process, where I saw the
--          Add and the potential team's Accept
--        * Insert the Add to my inbox
--        * Create a Squad record in DB
--        * Forward the Add to my followers
--        * Publish an Accept to:
--          * The object team + followers
--          * Add sender + followers
--          * My followers
--        * Record my Accept in the Squad record
--
--  * If the object is me & target is some project's components collection URI
--      * For each Stem record I have for this project:
--          * Verify it's not enabled yet, i.e. I'm not already a component
--            of this project
--          * Verify it's not in them-Invite-Accept state, already got the
--            project's Accept and waiting for my approval
--          * Verify it's not in us-Add-Accept state, has my approval and
--            waiting for the project's side
--      * Create a Stem record in DB
--      * Insert the Add to my inbox
--      * Forward the Add to my followers
--
--  * If I'm the object, being added to some teams' resource list:
--        * Verify the target is a team, find in DB/HTTP
--        * Verify it's not already an active team of mine
--        * Verify it's not already in an Origin-Us process where I saw the Add
--          and sent my Accept
--        * Verify it's not already in an Origin-Them process, where I saw the
--          Add and the potential team's Accept
--        * Insert the Add to my inbox
--        * Create a Squad record in DB
--        * Forward the Add to my followers
componentAdd
    :: (PersistRecordBackend topic SqlBackend, ToBackendKey SqlBackend topic)
    => (topic -> KomponentId)
    -> (forall f. f topic -> ComponentBy f)
    -> UTCTime
    -> Key topic
    -> Verse
    -> AP.Add URIMode
    -> ActE (Text, Act (), Next)
componentAdd grabKomponent toComponent now meID (Verse authorIdMsig body) add = do

    let author = bimap (view _1) (remoteAuthorURI . view _1) authorIdMsig
    (object, target, role) <- parseAdd author add
    unless (role == AP.RoleAdmin) $
        throwE "Add role isn't admin"
    case (target, object) of
        (Left at, _)
            | addTargetComponentProjects at == Just meComponent -> do
                project <-
                    bitraverse
                        (\case
                            LocalActorProject j -> pure j
                            _ -> throwE "Adding me to a local non-project"
                        )
                        pure
                        object
                addProjectActive role project
            | (resourceFromNG <$> addTargetResourceTeams at) == Just meResource ->
                addTeamActive object
        (_, Left la)
            | resourceToActor (componentResource $ toComponent meID) == la -> do
                case target of
                    Left (ATProjectComponents j) ->
                        addProjectPassive role $ Left j
                    Left (ATGroupEfforts g) ->
                        addTeamPassive $ Left g
                    Right (ObjURI h luColl) -> do
                        -- NOTE this is HTTP GET done synchronously in the activity
                        -- handler
                        manager <- asksEnv envHttpManager
                        c <- AP.fetchAPID_T manager (AP.collectionId :: AP.Collection FedURI URIMode -> LocalURI) h luColl
                        lu <- fromMaybeE (AP.collectionContext c) "No context"
                        rwc <- AP.fetchRWC_T manager h lu
                        AP.Actor l d <-
                            case AP.rwcResource rwc of
                                AP.ResourceActor a -> pure a
                                AP.ResourceChild _ _ -> throwE "Add.target remote ResourceChild"
                        let typ = AP.actorType d
                        if typ == AP.ActorTypeProject && Just luColl == AP.rwcComponents rwc
                            then addProjectPassive role $ Right $ ObjURI h lu
                        else if typ == AP.ActorTypeTeam && Just luColl == AP.rwcTeamResources rwc
                            then addTeamPassive $ Right $ ObjURI h lu
                        else throwE "Non-components collection"
                    _ -> throwE "I'm being added somewhere irrelevant"
        _ -> throwE "This Add isn't for me"

    where

    meComponent = toComponent meID
    meResource = componentResource meComponent
    meActor = resourceToActor meResource

    addProjectActive role project = do

        -- Check capability
        capability <- do

            -- Verify that a capability is provided
            uCap <- do
                let muCap = AP.activityCapability $ actbActivity body
                fromMaybeE muCap "No capability provided"

            -- Verify the capability URI is one of:
            --   * Outbox item URI of a local actor, i.e. a local activity
            --   * A remote URI
            cap <- nameExceptT "Add capability" $ parseActivityURI' uCap

            -- Verify the capability is local
            case cap of
                Left (actorByKey, _, outboxItemID) ->
                    return (actorByKey, outboxItemID)
                _ -> throwE "Capability is remote i.e. definitely not by me"

        -- Check input
        unless (role == AP.RoleAdmin) $ throwE "Add role isn't admin"

        -- If project is local, find it in our DB
        -- If project is remote, HTTP GET it and store in our DB (if it's already
        -- there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        projectDB <-
            bitraverse
                (withDBExcept . flip getEntityE "Project not found in DB")
                (\ u@(ObjURI h luProject) -> do
                    manager <- asksEnv envHttpManager
                    project <-
                        ExceptT $ first T.pack <$>
                            AP.fetchAPID manager (AP.actorId . AP.actorLocal . AP.projectActor) h luProject
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h luProject
                    case result of
                        Left Nothing -> throwE "Target @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Target isn't an actor"
                        Right (Just actor) -> do
                            unless (remoteActorType (entityVal actor) == AP.ActorTypeProject) $
                                throwE "Remote project type isn't Project"
                            return $ entityKey actor
                )
                project

        meHash <- encodeKeyHashid meID
        let meComponentHash = toComponent meHash
            meResourceHash = componentResource meComponentHash
            meActorHash = resourceToActor meResourceHash

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            komponentID <- lift $ grabKomponent <$> getJust meID
            Komponent resourceID <- lift $ getJust komponentID
            Resource meActorID <- lift $ getJust resourceID
            actor <- lift $ getJust meActorID

            -- Find existing Stem records I have for this project
            -- Make sure none are enabled / in Add-Accept mode / in Invite-Accept
            -- mode
            checkExistingStems komponentID projectDB

            -- Verify the specified capability gives relevant access
            verifyCapability' capability authorIdMsig meResource AP.RoleAdmin

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actor) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Stem record in DB
                acceptID <- insertEmptyOutboxItem' (actorOutbox actor) now
                insertStem komponentID projectDB addDB acceptID

                -- Prepare forwarding Add to my followers
                let sieve = makeRecipientSet [] [localActorFollowers meActorHash]

                -- Prepare an Accept activity and insert to my outbox
                accept@(actionAccept, _, _, _) <- prepareAccept projectDB
                _luAccept <- updateOutboxItem' meActor acceptID actionAccept

                return (meActorID, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (actorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                forwardActivity authorIdMsig body meActor actorID sieve
                lift $ sendActivity
                    meActor actorID localRecipsAccept
                    remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "[Add-project-active] Recorded and forwarded the Add, sent an Accept"

        where

        insertStem komponentID projectDB addDB acceptID = do
            stemID <- insert $ Stem AP.RoleAdmin komponentID
            case projectDB of
                Left (Entity projectID _) ->
                    insert_ $ StemProjectLocal stemID projectID
                Right remoteActorID ->
                    insert_ $ StemProjectRemote stemID remoteActorID
            insert_ $ StemOriginAdd stemID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ StemComponentGestureLocal stemID addID
                Right (author, _, addID) ->
                    insert_ $ StemComponentGestureRemote stemID (remoteAuthorId author) addID
            insert_ $ StemComponentAccept stemID acceptID

        prepareAccept projectDB = do
            encodeRouteHome <- getEncodeRouteHome

            audAdder <- makeAudSenderWithFollowers authorIdMsig
            audProject <-
                case projectDB of
                    Left (Entity j _) -> do
                        jh <- encodeKeyHashid j
                        return $
                            AudLocal
                                [LocalActorProject jh]
                                [LocalStageProjectFollowers jh]
                    Right remoteActorID -> do
                        ra <- getJust remoteActorID
                        ObjURI h lu <- getRemoteActorURI ra
                        return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
            audComponent <-
                AudLocal [] . pure . localActorFollowers <$>
                    hashLocalActor (resourceToActor $ componentResource $ toComponent meID)
            uAdd <- lift $ getActivityURI authorIdMsig

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audAdder, audProject, audComponent]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uAdd]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uAdd
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    addProjectPassive role project = do

        let meComponent = toComponent meID
            meResource = componentResource meComponent
            meActor = resourceToActor meResource

        -- Check input
        unless (role == AP.RoleAdmin) $ throwE "Add role isn't admin"

        -- If project is local, find it in our DB
        -- If project is remote, HTTP GET it and store in our DB (if it's already
        -- there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        projectDB <-
            bitraverse
                (withDBExcept . flip getEntityE "Project not found in DB")
                (\ u@(ObjURI h luProject) -> do
                    manager <- asksEnv envHttpManager
                    project <-
                        ExceptT $ first T.pack <$>
                            AP.fetchAPID manager (AP.actorId . AP.actorLocal . AP.projectActor) h luProject
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h luProject
                    case result of
                        Left Nothing -> throwE "Target @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Target isn't an actor"
                        Right (Just actor) -> do
                            unless (remoteActorType (entityVal actor) == AP.ActorTypeProject) $
                                throwE "Remote project type isn't Project"
                            return $ entityKey actor
                )
                project

        meHash <- encodeKeyHashid meID
        let meComponentHash = toComponent meHash
            meResourceHash = componentResource meComponentHash
            meActorHash = resourceToActor meResourceHash

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            komponentID <- lift $ grabKomponent <$> getJust meID
            Komponent resourceID <- lift $ getJust komponentID
            Resource meActorID <- lift $ getJust resourceID
            actor <- lift $ getJust meActorID

            -- Find existing Stem records I have for this project
            -- Make sure none are enabled / in Add-Accept mode / in Invite-Accept
            -- mode
            checkExistingStems komponentID projectDB

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox actor) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Stem record in DB
                insertStem komponentID projectDB addDB

                -- Prepare forwarding Add to my followers
                let sieve = makeRecipientSet [] [localActorFollowers meActorHash]

                return (meActorID, sieve, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (actorID, sieve, inboxItemID) -> do
                forwardActivity authorIdMsig body meActor actorID sieve
                doneDB inboxItemID "[Add-parent-passive] Recorded and forwarded the Add"

        where

        insertStem komponentID projectDB addDB = do
            stemID <- insert $ Stem AP.RoleAdmin komponentID
            case projectDB of
                Left (Entity projectID _) ->
                    insert_ $ StemProjectLocal stemID projectID
                Right remoteActorID ->
                    insert_ $ StemProjectRemote stemID remoteActorID
            originID <- insert $ StemOriginInvite stemID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ StemProjectGestureLocal originID addID
                Right (author, _, addID) ->
                    insert_ $ StemProjectGestureRemote originID (remoteAuthorId author) addID

    addTeamActive team = do

        -- If team is local, find it in our DB
        -- If team is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        teamDB <-
            bitraverse
                (\case
                    LocalActorGroup g -> withDBExcept $ getEntityE g "Team not found in DB"
                    _ -> throwE "Local proposed team of non-Group type"
                )
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Team @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Team isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote team type isn't Team"
                            return (u, actor)
                )
                team
        let teamDB' = second (entityKey . snd) teamDB

        -- Verify that a capability is provided
        uCap <- do
            let muCap = AP.activityCapability $ actbActivity body
            fromMaybeE muCap "No capability provided"

        -- Verify the sender is authorized by me to add a team
        verifyCapability''
            uCap
            authorIdMsig
            meResource
            AP.RoleAdmin

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            meKomponentID <- lift $ grabKomponent <$> getJust meID
            Komponent meResourceID <- lift $ getJust meKomponentID
            Resource meActorID <- lift $ getJust meResourceID
            meActorDB <- lift $ getJust meActorID

            -- Verify the object isn't already a team of mine, and that no
            -- Squad record is already in Add-Accept state
            verifyNoStartedResourceTeams meResourceID teamDB'

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox meActorDB) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Squad record in DB
                acceptID <- insertEmptyOutboxItem' (actorOutbox meActorDB) now
                insertSquad meResourceID teamDB' addDB acceptID

                -- Prepare forwarding the Add to my followers
                sieve <- do
                    h <- hashLocalActor meActor
                    return $ makeRecipientSet [] [localActorFollowers h]

                -- Prepare an Accept activity and insert to my outbox
                accept@(actionAccept, _, _, _) <- prepareAccept teamDB
                _luAccept <- updateOutboxItem' meActor  acceptID actionAccept

                return (meActorID, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (meActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                forwardActivity authorIdMsig body meActor meActorID sieve
                lift $ sendActivity
                    meActor meActorID localRecipsAccept
                    remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "[Team-active] Recorded a team-in-progress, forwarded the Add, sent an Accept"

        where

        insertSquad resourceID topicDB addDB acceptID = do
            squadID <- insert $ Squad AP.RoleAdmin resourceID
            case topicDB of
                Left (Entity g _) -> insert_ $ SquadTopicLocal squadID g
                Right a -> insert_ $ SquadTopicRemote squadID a
            insert_ $ SquadOriginUs squadID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ SquadUsGestureLocal squadID addID
                Right (author, _, addID) ->
                    insert_ $ SquadUsGestureRemote squadID (remoteAuthorId author) addID

            insert_ $ SquadUsAccept squadID acceptID

        prepareAccept teamDB = do
            encodeRouteHome <- getEncodeRouteHome

            audAdder <- makeAudSenderWithFollowers authorIdMsig
            audTeam <-
                case teamDB of
                    Left (Entity g _) -> do
                        gh <- encodeKeyHashid g
                        return $ AudLocal [LocalActorGroup gh] [LocalStageGroupFollowers gh]
                    Right (ObjURI h lu, Entity _ ra) ->
                        return $ AudRemote h [lu] (maybeToList $ remoteActorFollowers ra)
            audMe <-
                AudLocal [] . pure . localActorFollowers <$>
                    hashLocalActor meActor
            uAdd <- lift $ getActivityURI authorIdMsig

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audAdder, audTeam, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uAdd]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uAdd
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    addTeamPassive team = do

        -- If team is local, find it in our DB
        -- If team is remote, HTTP GET it, verify it's an actor of Project
        -- type, and store in our DB (if it's already there, no need for HTTP)
        --
        -- NOTE: This is a blocking HTTP GET done right here in the handler,
        -- which is NOT a good idea. Ideally, it would be done async, and the
        -- handler result would be sent later in a separate (e.g. Accept) activity.
        -- But for the PoC level, the current situation will hopefully do.
        teamDB <-
            bitraverse
                (\ g -> withDBExcept $ getEntityE g "Team not found in DB")
                (\ u@(ObjURI h lu) -> do
                    instanceID <-
                        lift $ withDB $ either entityKey id <$> insertBy' (Instance h)
                    result <-
                        ExceptT $ first (T.pack . displayException) <$>
                            fetchRemoteActor' instanceID h lu
                    case result of
                        Left Nothing -> throwE "Team @id mismatch"
                        Left (Just err) -> throwE $ T.pack $ displayException err
                        Right Nothing -> throwE "Team isn't an actor"
                        Right (Just actor) -> do
                            case remoteActorType $ entityVal actor of
                                AP.ActorTypeTeam -> pure ()
                                _ -> throwE "Remote team type isn't Team"
                            return (u, actor)
                )
                team
        let teamDB' = second (entityKey . snd) teamDB

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            meKomponentID <- lift $ grabKomponent <$> getJust meID
            Komponent meResourceID <- lift $ getJust meKomponentID
            Resource meActorID <- lift $ getJust meResourceID
            meActorDB <- lift $ getJust meActorID

            -- Verify the object isn't already a team of mine, and that no
            -- Squad record is already in Add-Accept state
            verifyNoStartedResourceTeams meResourceID teamDB'

            -- Insert the Add to my inbox
            mractid <- lift $ insertToInbox now authorIdMsig body (actorInbox meActorDB) False
            lift $ for mractid $ \ (inboxItemID, addDB) -> do

                -- Create a Squad record in DB
                insertSquad meResourceID teamDB' addDB

                -- Prepare forwarding the Add to my followers
                sieve <- do
                    h <- hashLocalActor meActor
                    return $ makeRecipientSet [] [localActorFollowers h]

                return (meActorID, sieve, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (meActorID, sieve, inboxItemID) -> do
                forwardActivity authorIdMsig body meActor meActorID sieve
                doneDB inboxItemID "[Team-passive] Recorded a team-in-progress, forwarded the Add"

        where

        insertSquad resourceID topicDB addDB = do
            squadID <- insert $ Squad AP.RoleAdmin resourceID
            case topicDB of
                Left (Entity g _) -> insert_ $ SquadTopicLocal squadID g
                Right a -> insert_ $ SquadTopicRemote squadID a
            themID <- insert $ SquadOriginThem squadID
            case addDB of
                Left (_, _, addID) ->
                    insert_ $ SquadThemGestureLocal themID addID
                Right (author, _, addID) ->
                    insert_ $ SquadThemGestureRemote themID (remoteAuthorId author) addID

-- Meaning: An actor is revoking Grant activities
-- Behavior:
--  * For each revoked activity:
--      * If it's a project revoking a delegator-Grant it gave me:
--          * Delete the whole Stem record
--          * Forward the Revoke to my followers
--          * Send Revoke-start-Grant to project+followers & my followers
--      * If it's a team revoking a delegator-Grant it gave me:
--          * Delete the whole Squad record
--          * Forward the Revoke to my followers
--          * Send Accept to team+followers & my followers
componentRevoke
    :: forall comp.
       (PersistRecordBackend comp SqlBackend, ToBackendKey SqlBackend comp)
    => (comp -> KomponentId)
    -> (forall f. f comp -> ComponentBy f)
    -> UTCTime
    -> Key comp
    -> Verse
    -> AP.Revoke URIMode
    -> ActE (Text, Act (), Next)
componentRevoke grabKomponent toComponent now compKey (Verse authorIdMsig body) (AP.Revoke (luFirst :| lusRest)) = do

    ObjURI h _ <- lift $ getActorURI authorIdMsig
    parseRevoked <- do
        hl <- hostIsLocal h
        return $
            \ lu ->
                if hl
                    then
                        Left . (\ (a, _, i) -> (a, i)) <$>
                            parseLocalActivityURI' lu
                    else pure $ Right lu
    revokedFirst <- parseRevoked luFirst
    revokedRest <- traverse parseRevoked lusRest

    mode <- withDBExcept $ do

        revokedFirstDB <- do
            a <- getActivity $ second (ObjURI h) revokedFirst
            fromMaybeE a "Can't find revoked in DB"

        meKomponentID <- lift $ grabKomponent <$> getJust compKey
        Komponent meResourceID <- lift $ getJust meKomponentID

        let adapt = maybe (Right Nothing) (either Left (Right . Just))
        maybeMode <-
            ExceptT $ fmap adapt $ runMaybeT $
                runExceptT (Left <$> tryProject meKomponentID revokedFirstDB) <|>
                runExceptT (Right <$> tryTeam meResourceID revokedFirstDB)
        fromMaybeE
            maybeMode
            "Revoked activity isn't a relevant Grant I'm aware of"

    case mode of
        Left j -> revokeProject revokedRest j
        Right t -> revokeTeam revokedRest t

    where

    verifyStemHolder :: KomponentId -> StemId -> MaybeT ActDB ()
    verifyStemHolder meKomponentID stemID = do
        Stem _ k <- lift $ getJust stemID
        guard $ k == meKomponentID

    tryProject' meKomponentID usAcceptID send = do
        StemComponentAccept stemID _ <- lift $ lift $ getJust usAcceptID
        lift $ verifyStemHolder meKomponentID stemID
        send' <-
            lift $ lift $
            bitraverse
                (traverseOf _1 $ \ localID -> do
                    StemProjectLocal _ projectID <- getJust localID
                    return (localID, projectID)
                )
                (traverseOf _1 $ \ remoteID -> do
                    StemProjectRemote _ actorID <- getJust remoteID
                    return (remoteID, actorID)
                )
                send
        maybeStart <- lift $ lift $ getBy $ UniqueStemDelegateLocal usAcceptID
        return (stemID, usAcceptID, send', maybeStart)

    tryProject k (Left (_actorByKey, _actorEntity, itemID)) = do
        Entity sendID (StemProjectGrantLocal usAcceptID localID _) <-
            lift $ MaybeT $ getBy $ UniqueStemProjectGrantLocalGrant itemID
        tryProject' k usAcceptID $ Left (localID, sendID)
    tryProject k (Right remoteActivityID) = do
        Entity sendID (StemProjectGrantRemote usAcceptID remoteID _) <-
            lift $ MaybeT $ getBy $ UniqueStemProjectGrantRemoteGrant remoteActivityID
        tryProject' k usAcceptID $ Right (remoteID, sendID)

    verifySquadHolder :: ResourceId -> SquadId -> MaybeT ActDB ()
    verifySquadHolder meResourceID squadID = do
        Squad _ resourceID <- lift $ getJust squadID
        guard $ resourceID == meResourceID

    tryTeam' meResourceID usAcceptID send = do
        SquadUsAccept squadID _ <- lift $ lift $ getJust usAcceptID
        lift $ verifySquadHolder meResourceID squadID
        topic <- lift . lift $ getSquadTeam squadID
        return (squadID, usAcceptID, topic, send)

    tryTeam r (Left (_actorByKey, _actorEntity, itemID)) = do
        Entity sendID (SquadThemSendDelegatorLocal usAcceptID _localID _) <-
            lift $ MaybeT $ getBy $ UniqueSquadThemSendDelegatorLocalGrant itemID
        tryTeam' r usAcceptID (Left sendID) --(Left localID)
    tryTeam r (Right remoteActivityID) = do
        Entity sendID (SquadThemSendDelegatorRemote usAcceptID _remoteID _) <-
            lift $ MaybeT $ getBy $ UniqueSquadThemSendDelegatorRemoteGrant remoteActivityID
        tryTeam' r usAcceptID (Right sendID) --(Right remoteID)

    revokeProject revokedRest (stemID, usAcceptID, project, maybeStart) = do

        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        unless (author == bimap (LocalActorProject . snd . fst) (snd . fst) project) $
            throwE "Sender isn't the Project the revoked Grant came from"

        unless (null revokedRest) $
            throwE "Project revoking the delegator-Grant and something more"

        Entity startID (StemDelegateLocal _ grantID) <- fromMaybeE maybeStart "No recorded start-Grant"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            komponentID <- lift $ grabKomponent <$> getJust compKey
            Komponent resourceID <- lift $ getJust komponentID
            Resource topicActorID <- lift $ getJust resourceID
            topicActor <- lift $ getJust topicActorID

            maybeRevokeDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
            lift $ for maybeRevokeDB $ \ (inboxItemID, _revokeDB) -> do

                -- Delete the whole Stem record
                delete startID
                case project of
                    Left (_, sendID) -> delete sendID
                    Right (_, sendID) -> delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueStemOriginAdd stemID)
                        (getKeyBy $ UniqueStemOriginInvite stemID)
                        "Neither us nor them"
                        "Both us and them"
                case origin of
                    Left usID -> delete usID
                    Right themID -> do
                        deleteBy $ UniqueStemProjectAcceptLocal themID
                        deleteBy $ UniqueStemProjectAcceptRemote themID
                        deleteBy $ UniqueStemProjectGestureLocal themID
                        deleteBy $ UniqueStemProjectGestureRemote themID
                        delete themID
                delete usAcceptID
                deleteBy $ UniqueStemComponentGestureLocal stemID
                deleteBy $ UniqueStemComponentGestureRemote stemID
                case project of
                    Left ((localID, _), _) -> delete localID
                    Right ((remoteID, _), _) -> delete remoteID
                delete stemID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid compKey
                    let topicByHash = resourceToActor $ componentResource $ toComponent topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare Accept activity
                revoke@(actionRevoke, _, _, _) <- prepareRevoke grantID
                let recipByKey = resourceToActor $ componentResource $ toComponent compKey
                revokeID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
                _luRevoke <- updateOutboxItem' recipByKey revokeID actionRevoke

                return (topicActorID, sieve, revokeID, revoke, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, revokeID, (actionRevoke, localRecipsRevoke, remoteRecipsRevoke, fwdHostsRevoke), inboxItemID) -> do
                let topicByID = resourceToActor $ componentResource $ toComponent compKey
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $
                    sendActivity
                        topicByID topicActorID localRecipsRevoke
                        remoteRecipsRevoke fwdHostsRevoke revokeID actionRevoke
                doneDB inboxItemID "Deleted the Project/Stem, forwarded Revoke, sent Revoke-start-Grant"

        where

        prepareRevoke grantID = do
            encodeRouteHome <- getEncodeRouteHome
            encodeRouteLocal <- getEncodeRouteLocal

            recipHash <- encodeKeyHashid compKey
            let topicByHash = resourceToActor $ componentResource $ toComponent recipHash

            audProject <- makeAudSenderWithFollowers authorIdMsig
            let audMe = AudLocal [] [localActorFollowers topicByHash]
            uRevoke <- lift $ getActivityURI authorIdMsig
            luGrant <- do
                grantHash <- encodeKeyHashid grantID
                return $ encodeRouteLocal $ activityRoute topicByHash grantHash

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audProject, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRevoke]
                    , AP.actionSpecific   = AP.RevokeActivity AP.Revoke
                        { AP.revokeObject = luGrant :| []
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)

    revokeTeam revokedRest (squadID, usAcceptID, team, send) = do

        let author = bimap (view _1) (remoteAuthorId . view _1) authorIdMsig
        unless (author == bimap (LocalActorGroup . snd) snd team) $
            throwE "Sender isn't the Team the revoked Grant came from"

        unless (null revokedRest) $
            throwE "Team revoking the delegator-Grant and something more"

        maybeNew <- withDBExcept $ do

            -- Grab me from DB
            komponentID <- lift $ grabKomponent <$> getJust compKey
            Komponent resourceID <- lift $ getJust komponentID
            Resource topicActorID <- lift $ getJust resourceID
            topicActor <- lift $ getJust topicActorID

            maybeRevokeDB <- lift $ insertToInbox now authorIdMsig body (actorInbox topicActor) False
            lift $ for maybeRevokeDB $ \ (inboxItemID, _revokeDB) -> do

                maybeStartID <- getKeyBy $ UniqueSquadUsStart usAcceptID

                -- Delete the whole Squad record
                for_ maybeStartID delete
                case send of
                    Left sendID -> delete sendID
                    Right sendID -> delete sendID
                origin <-
                    requireEitherAlt
                        (getKeyBy $ UniqueSquadOriginUs squadID)
                        (getKeyBy $ UniqueSquadOriginThem squadID)
                        "Neither us nor them"
                        "Both us and them"
                deleteBy $ UniqueSquadUsGestureLocal squadID
                deleteBy $ UniqueSquadUsGestureRemote squadID
                case origin of
                    Left usID -> delete usID
                    Right themID -> do
                        deleteBy $ UniqueSquadThemAcceptLocal themID
                        deleteBy $ UniqueSquadThemAcceptRemote themID
                        deleteBy $ UniqueSquadThemGestureLocal themID
                        deleteBy $ UniqueSquadThemGestureRemote themID
                        delete themID
                delete usAcceptID
                case team of
                    Left (l, _) -> delete l
                    Right (r, _) -> delete r
                delete squadID

                -- Prepare forwarding Remove to my followers
                sieve <- lift $ do
                    topicHash <- encodeKeyHashid compKey
                    let topicByHash = resourceToActor $ componentResource $ toComponent topicHash
                    return $ makeRecipientSet [] [localActorFollowers topicByHash]

                -- Prepare Accept activity
                accept@(actionAccept, _, _, _) <- prepareAccept
                let recipByKey = resourceToActor $ componentResource $ toComponent compKey
                acceptID <- insertEmptyOutboxItem' (actorOutbox topicActor) now
                _luAccept <- updateOutboxItem' recipByKey acceptID actionAccept

                return (topicActorID, sieve, acceptID, accept, inboxItemID)

        case maybeNew of
            Nothing -> done "I already have this activity in my inbox"
            Just (topicActorID, sieve, acceptID, (actionAccept, localRecipsAccept, remoteRecipsAccept, fwdHostsAccept), inboxItemID) -> do
                let topicByID = resourceToActor $ componentResource $ toComponent compKey
                forwardActivity authorIdMsig body topicByID topicActorID sieve
                lift $
                    sendActivity
                        topicByID topicActorID localRecipsAccept
                        remoteRecipsAccept fwdHostsAccept acceptID actionAccept
                doneDB inboxItemID "Deleted the Team/Squad, forwarded Revoke, sent Accept"

        where

        prepareAccept = do
            encodeRouteHome <- getEncodeRouteHome

            recipHash <- encodeKeyHashid compKey
            let topicByHash = resourceToActor $ componentResource $ toComponent recipHash

            audTeam <- makeAudSenderWithFollowers authorIdMsig
            let audMe = AudLocal [] [localActorFollowers topicByHash]
            uRevoke <- lift $ getActivityURI authorIdMsig

            let (recipientSet, remoteActors, fwdHosts, audLocal, audRemote) =
                    collectAudience [audTeam, audMe]

                recips = map encodeRouteHome audLocal ++ audRemote
                action = AP.Action
                    { AP.actionCapability = Nothing
                    , AP.actionSummary    = Nothing
                    , AP.actionAudience   = AP.Audience recips [] [] [] [] []
                    , AP.actionFulfills   = [uRevoke]
                    , AP.actionSpecific   = AP.AcceptActivity AP.Accept
                        { AP.acceptObject   = uRevoke
                        , AP.acceptResult   = Nothing
                        }
                    }

            return (action, recipientSet, remoteActors, fwdHosts)
