{- This file is part of Vervis.
 -
 - Written in 2023, 2024 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Serve.Collab
    ( serveTeamsCollection
    , serveCollabs
    , serveInviteCollab
    , serveRemoveCollab
    )
where

import Control.Applicative
import Control.Exception.Base
import Control.Monad
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Aeson
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Default.Class
import Data.Foldable
import Data.Maybe (fromMaybe, isJust)
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Network.HTTP.Client hiding (Proxy, proxy)
import Network.HTTP.Types.Method
import Network.HTTP.Types.Status
import Optics.Core
import Text.Blaze.Html (Html)
import Yesod.Auth
import Yesod.Core
import Yesod.Core.Handler (redirect, setMessage, lookupPostParam, notFound)
import Yesod.Form.Functions (runFormPost, runFormGet)
import Yesod.Form.Types (FormResult (..))
import Yesod.Persist.Core (runDB, get404, getBy404)

import qualified Data.ByteString.Lazy as BL
import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Control.Concurrent.Actor
import Database.Persist.JSON
import Development.PatchMediaType
import Network.FedURI
import Web.ActivityPub hiding (Project (..), Repo (..), Actor (..), ActorDetail (..), ActorLocal (..))
import Yesod.ActivityPub
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Data.Paginate.Local
import Database.Persist.Local
import Yesod.Form.Local
import Yesod.Persist.Local

import Vervis.Actor
import Vervis.Actor2
import Vervis.API
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.FedURI
import Vervis.Form.Tracker
import Vervis.Foundation
import Vervis.Model
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Settings
import Vervis.Ticket
import Vervis.TicketFilter
import Vervis.Time
import Vervis.Widget.Tracker

import qualified Vervis.Client as C
import qualified Vervis.Recipient as VR

serveTeamsCollection meR hereR resourceID = do
    teams <- runDB $ getResourceTeams resourceID
    h <- asksSite siteInstanceHost
    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    hashGroup <- getEncodeKeyHashid
    let makeItem (role, time, team, _) = AP.Relationship
            { AP.relationshipId           = Nothing
            , AP.relationshipExtraTypes   = []
            , AP.relationshipSubject      = encodeRouteHome meR
            , AP.relationshipProperty     = Left AP.RelHasRecCollab
            , AP.relationshipObject       =
                case team of
                    Left (groupID, _) ->
                        encodeRouteHome $ GroupR $ hashGroup groupID
                    Right (i, ro, _) ->
                        ObjURI (instanceHost i) (remoteObjectIdent ro)
            , AP.relationshipAttributedTo = encodeRouteLocal meR
            , AP.relationshipPublished    = Just time
            , AP.relationshipUpdated      = Nothing
            , AP.relationshipInstrument   = Just role
            }
        teamsAP = AP.Collection
            { AP.collectionId         = encodeRouteLocal hereR
            , AP.collectionType       = CollectionTypeUnordered
            , AP.collectionTotalItems = Just $ length teams
            , AP.collectionCurrent    = Nothing
            , AP.collectionFirst      = Nothing
            , AP.collectionLast       = Nothing
            , AP.collectionItems      = map (Doc h . makeItem) teams
            , AP.collectionContext    = Just $ encodeRouteLocal meR
            }
    provideHtmlAndAP teamsAP $ redirectToPrettyJSON hereR

serveCollabs
    :: AP.RelationshipProperty
    -> ResourceId
    -> Route App
    -> Route App
    -> (CollabId -> Route App)
    -> Route App
    -> Maybe
        ( SquadId -> Route App
        , Route App
        , SquadId -> Route App
        )
    -> Widget
    -> Handler TypedContent
serveCollabs rel resourceID meR hereR removeR inviteR maybeTeams navW = do
    collabs <- runDB $ getCollabs resourceID
    h <- asksSite siteInstanceHost
    encodeRouteLocal <- getEncodeRouteLocal
    encodeRouteHome <- getEncodeRouteHome
    hashPerson <- getEncodeKeyHashid
    let makeItem (role, time, collab, _) = AP.Relationship
            { AP.relationshipId           = Nothing
            , AP.relationshipExtraTypes   = []
            , AP.relationshipSubject      = encodeRouteHome meR
            , AP.relationshipProperty     = Left rel
            , AP.relationshipObject       =
                case collab of
                    Left (Entity personID _, _) ->
                        encodeRouteHome $ PersonR $ hashPerson personID
                    Right (i, ro, _) ->
                        ObjURI (instanceHost i) (remoteObjectIdent ro)
            , AP.relationshipAttributedTo = encodeRouteLocal meR
            , AP.relationshipPublished    = Just time
            , AP.relationshipUpdated      = Nothing
            , AP.relationshipInstrument   = Just role
            }
        collabsAP = AP.Collection
            { AP.collectionId         = encodeRouteLocal hereR
            , AP.collectionType       = CollectionTypeUnordered
            , AP.collectionTotalItems = Just $ length collabs
            , AP.collectionCurrent    = Nothing
            , AP.collectionFirst      = Nothing
            , AP.collectionLast       = Nothing
            , AP.collectionItems      = map (Doc h . makeItem) collabs
            , AP.collectionContext    = Just $ encodeRouteLocal meR
            }
    provideHtmlAndAP collabsAP $ getHtml collabs
    where
    getHtml collabs = do
        mp <- maybeAuthId
        haveAdmin <- fmap isJust $ handlerToWidget $ runDB $ runMaybeT $ do
            personID <- MaybeT $ pure mp
            MaybeT $ getCapability personID (Left resourceID) AP.RoleAdmin
        (invites, joins, teamsAndDrafts) <- handlerToWidget $ runDB $ (,,)
            <$> getCollabInvites resourceID
            <*> getCollabJoins resourceID
            <*> (for maybeTeams $ \ r -> (r,,)
                    <$> getResourceTeams resourceID
                    <*> getResourceTeamDrafts resourceID
                )
        [whamlet|
            ^{navW}
            ^{collabsW haveAdmin collabs invites joins teamsAndDrafts removeR inviteR}
        |]

serveInviteCollab :: ResourceId -> Route App -> Handler Html
serveInviteCollab resourceID collabsR = do
    (uRecipient, role) <- runFormPostRedirect collabsR inviteForm

    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        (maybeSummary, audience, invite) <- do
            let uResourceCollabs = encodeRouteHome collabsR
            C.invite personID uRecipient uResourceCollabs role
        cap <- do
            maybeItem <-
                lift $ runDB $
                    getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need admin access to the resource to invite people"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.InviteActivity invite
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> setMessage $ toHtml e
        Right _inviteID -> setMessage "Invite sent"
    redirect collabsR

serveRemoveCollab :: ResourceId -> Route App -> CollabId -> Handler Html
serveRemoveCollab resourceID collabsR collabID = do
    personEntity@(Entity personID person) <- requireAuth
    personHash <- encodeKeyHashid personID
    encodeRouteHome <- getEncodeRouteHome

    result <- runExceptT $ do
        mpidOrU <- lift $ runDB $ runMaybeT $ do
            Collab _ resourceID' <- MaybeT $ get collabID
            guard $ resourceID' == resourceID
            _ <- MaybeT $ getBy $ UniqueCollabEnable collabID
            member <-
                Left <$> MaybeT (getValBy $ UniqueCollabRecipLocal collabID) <|>
                Right <$> MaybeT (getValBy $ UniqueCollabRecipRemote collabID)
            lift $
                bitraverse
                    (pure . collabRecipLocalPerson)
                    (getRemoteActorURI <=< getJust . collabRecipRemoteActor)
                    member
        pidOrU <- maybe notFound pure mpidOrU
        (maybeSummary, audience, remove) <- do
            uRecipient <-
                case pidOrU of
                    Left pid -> encodeRouteHome . PersonR <$> encodeKeyHashid pid
                    Right u -> pure u
            let uResourceCollabs = encodeRouteHome collabsR
            C.remove personID uRecipient uResourceCollabs
        cap <- do
            maybeItem <- lift $ runDB $ getCapability personID (Left resourceID) AP.RoleAdmin
            fromMaybeE maybeItem "You need to have admin access to the resource to remove collaborators"
        uCap <- lift $ renderActivityURI cap
        (localRecips, remoteRecips, fwdHosts, action) <-
            C.makeServerInput (Just uCap) maybeSummary audience $ AP.RemoveActivity remove
        let cap' = first (\ (la, i) -> (la, error "lah", i)) cap
        handleViaActor
            personID (Just cap') localRecips remoteRecips fwdHosts action

    case result of
        Left e -> setMessage $ toHtml e
        Right _removeID -> setMessage "Remove sent"
    redirect collabsR
