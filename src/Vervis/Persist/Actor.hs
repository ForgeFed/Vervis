{- This file is part of Vervis.
 -
 - Written in 2019, 2020, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Persist.Actor
    ( getLocalActor
    , getLocalResource
    , getLocalComponent
    , getLocalActorEnt
    --, getLocalResourceEnt
    --, getLocalComponentEnt
    , getLocalActorEntity
    , getLocalActorEntityE
    , getLocalActorEntity404
    , getLocalResourceEntity
    , getLocalResourceEntityE
    , getLocalResourceEntity404
    , getLocalComponentEntity
    , getLocalComponentEntityE
    , getLocalComponentEntity404
    , verifyLocalActivityExistsInDB
    , getRemoteObjectURI
    , getRemoteActorURI
    , getRemoteActivityURI
    , insertActor
    , updateOutboxItem
    , updateOutboxItem'
    , fillPerActorKeys
    , getPersonWidgetInfo
    , getActivityBody
    , getRemoteActor
    , getRemoteActorM
    , getRemoteActorE
    , doneDB
    , insertToInbox
    , adaptErrbox
    , getActivityIdent
    , getRemoteActorData
    , getResourceOrigin
    )
where

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Logger.CallStack
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Aeson hiding (Key)
import Data.Barbie
import Data.ByteString (ByteString)
import Data.Bitraversable
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Yesod.Core.Handler

import qualified Data.Text as T
import qualified Database.Esqueleto as E

import Control.Concurrent.Actor hiding (Actor)
import Database.Persist.JSON
import Network.FedURI
import Yesod.ActivityPub
import Yesod.FedURI
import Yesod.Hashids
import Yesod.MonadSite

import qualified Crypto.ActorKey as AK
import qualified Web.ActivityPub as AP
import qualified Web.Actor as WA
import qualified Web.Actor.Persist as WAP

import Control.Monad.Trans.Except.Local
import Data.Maybe.Local
import Database.Persist.Local

import Vervis.Actor (Verse)
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.FedURI
import Vervis.Foundation
import Vervis.Model
import Vervis.Recipient
import Vervis.Settings

import qualified Vervis.Actor as VA

getLocalActor
    :: MonadIO m => ActorId -> ReaderT SqlBackend m (LocalActorBy Key)
getLocalActor = fmap (bmap entityKey) . getLocalActorEnt

getLocalResource
    :: MonadIO m => ResourceId -> ReaderT SqlBackend m (LocalResourceBy Key)
getLocalResource = fmap (bmap entityKey) . getLocalResourceEnt

getLocalComponent
    :: MonadIO m => KomponentId -> ReaderT SqlBackend m (ComponentBy Key)
getLocalComponent = fmap (bmap entityKey) . getLocalComponentEnt

getLocalActorEnt
    :: MonadIO m => ActorId -> ReaderT SqlBackend m (LocalActorBy Entity)
getLocalActorEnt actorID = do
    m <- getKeyBy $ UniqueResource actorID

    mp <- getBy $ UniquePersonActor actorID
    mg <- getBy $ UniqueGroupActor actorID
    mr <- getBy $ UniqueRepoActor actorID
    md <- getBy $ UniqueDeckActor actorID
    ml <- getBy $ UniqueLoomActor actorID
    mj <- getBy $ UniqueProjectActor actorID
    mf <- runMaybeT $ do
        resourceID <- hoistMaybe m
        MaybeT $ getBy $ UniqueFactory resourceID

    return $
        case (mp, mg, mr, md, ml, mj, mf) of
            (Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing) -> error "Unused ActorId"
            (Just p, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing) -> LocalActorPerson p
            (Nothing, Just g, Nothing, Nothing, Nothing, Nothing, Nothing) -> LocalActorGroup g
            (Nothing, Nothing, Just r, Nothing, Nothing, Nothing, Nothing) -> LocalActorRepo r
            (Nothing, Nothing, Nothing, Just d, Nothing, Nothing, Nothing) -> LocalActorDeck d
            (Nothing, Nothing, Nothing, Nothing, Just l, Nothing, Nothing) -> LocalActorLoom l
            (Nothing, Nothing, Nothing, Nothing, Nothing, Just j, Nothing) -> LocalActorProject j
            (Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Just f) -> LocalActorFactory f
            _ -> error "Multi-usage of an ActorId"

getLocalResourceEnt
    :: MonadIO m => ResourceId -> ReaderT SqlBackend m (LocalResourceBy Entity)
getLocalResourceEnt resourceID = do
    Resource actorID <- getJust resourceID
    options <-
        sequence
            [ fmap LocalResourceRepo <$> getBy (UniqueRepoActor actorID)
            , fmap LocalResourceDeck <$> getBy (UniqueDeckActor actorID)
            , fmap LocalResourceLoom <$> getBy (UniqueLoomActor actorID)
            , fmap LocalResourceProject <$> getBy (UniqueProjectActor actorID)
            , fmap LocalResourceGroup <$> getBy (UniqueGroupActor actorID)
            , fmap LocalResourceFactory <$> getBy (UniqueFactory resourceID)
            ]
    exactlyOneJust
        options
        "Found Resource without specific actor"
        "Found Resource with multiple actors"

getLocalComponentEnt
    :: MonadIO m => KomponentId -> ReaderT SqlBackend m (ComponentBy Entity)
getLocalComponentEnt komponentID = do
    Komponent resourceID <- getJust komponentID
    Resource actorID <- getJust resourceID
    options <-
        sequence
            [ fmap ComponentRepo <$> getBy (UniqueRepoActor actorID)
            , fmap ComponentDeck <$> getBy (UniqueDeckActor actorID)
            , fmap ComponentLoom <$> getBy (UniqueLoomActor actorID)
            ]
    exactlyOneJust
        options
        "Found Komponent without specific actor"
        "Found Komponent with multiple actors"

getLocalActorEntity
    :: MonadIO m
    => LocalActorBy Key
    -> ReaderT SqlBackend m (Maybe (LocalActorBy Entity))
getLocalActorEntity (LocalActorPerson p) =
    fmap (LocalActorPerson . Entity p) <$> get p
getLocalActorEntity (LocalActorGroup g) =
    fmap (LocalActorGroup . Entity g) <$> get g
getLocalActorEntity (LocalActorRepo r) =
    fmap (LocalActorRepo . Entity r) <$> get r
getLocalActorEntity (LocalActorDeck d) =
    fmap (LocalActorDeck . Entity d) <$> get d
getLocalActorEntity (LocalActorLoom l) =
    fmap (LocalActorLoom . Entity l) <$> get l
getLocalActorEntity (LocalActorProject r) =
    fmap (LocalActorProject . Entity r) <$> get r
getLocalActorEntity (LocalActorFactory f) =
    fmap (LocalActorFactory . Entity f) <$> get f

getLocalActorEntityE a e = do
    m <- lift $ getLocalActorEntity a
    case m of
        Nothing -> throwE e
        Just a' -> return a'

getLocalActorEntity404 = maybe notFound return <=< getLocalActorEntity

getLocalResourceEntity
    :: MonadIO m
    => LocalResourceBy Key
    -> ReaderT SqlBackend m (Maybe (LocalResourceBy Entity))
getLocalResourceEntity (LocalResourceGroup g) =
    fmap (LocalResourceGroup . Entity g) <$> get g
getLocalResourceEntity (LocalResourceRepo r) =
    fmap (LocalResourceRepo . Entity r) <$> get r
getLocalResourceEntity (LocalResourceDeck d) =
    fmap (LocalResourceDeck . Entity d) <$> get d
getLocalResourceEntity (LocalResourceLoom l) =
    fmap (LocalResourceLoom . Entity l) <$> get l
getLocalResourceEntity (LocalResourceProject r) =
    fmap (LocalResourceProject . Entity r) <$> get r
getLocalResourceEntity (LocalResourceFactory f) =
    fmap (LocalResourceFactory . Entity f) <$> get f

getLocalResourceEntityE a e = do
    m <- lift $ getLocalResourceEntity a
    case m of
        Nothing -> throwE e
        Just a' -> return a'

getLocalResourceEntity404 = maybe notFound return <=< getLocalResourceEntity

getLocalComponentEntity
    :: MonadIO m
    => ComponentBy Key
    -> ReaderT SqlBackend m (Maybe (ComponentBy Entity))
getLocalComponentEntity (ComponentLoom l) =
    fmap (ComponentLoom . Entity l) <$> get l
getLocalComponentEntity (ComponentRepo r) =
    fmap (ComponentRepo . Entity r) <$> get r
getLocalComponentEntity (ComponentDeck d) =
    fmap (ComponentDeck . Entity d) <$> get d

getLocalComponentEntityE a e = do
    m <- lift $ getLocalComponentEntity a
    case m of
        Nothing -> throwE e
        Just a' -> return a'

getLocalComponentEntity404 = maybe notFound return <=< getLocalComponentEntity

verifyLocalActivityExistsInDB
    :: MonadIO m
    => LocalActorBy Key
    -> OutboxItemId
    -> ExceptT Text (ReaderT SqlBackend m) ()
verifyLocalActivityExistsInDB actorByKey outboxItemID = do
    outboxID <- outboxItemOutbox <$> getE outboxItemID "No such OutboxItemId in DB"
    itemActorID <- do
        maybeActorID <-
            lift $ getKeyBy $ UniqueActorOutbox outboxID
        fromMaybeE maybeActorID "Outbox item's outbox doesn't belong to any Actor"
    itemActorByKey <- lift $ getLocalActor itemActorID
    unless (itemActorByKey == actorByKey) $
        throwE "Actor-in-URI and Actor-owning-the-outbox-item-in-DB mismatch"

getRemoteObjectURI object = do
    inztance <- getJust $ remoteObjectInstance object
    return $
        ObjURI
            (instanceHost inztance)
            (remoteObjectIdent object)

getRemoteActorURI actor = do
    object <- getJust $ remoteActorIdent actor
    getRemoteObjectURI object

getRemoteActivityURI act = do
    object <- getJust $ remoteActivityIdent act
    getRemoteObjectURI object

insertActor now name desc create = do
    ibid <- insert Inbox
    rbid <- insert Inbox
    obid <- insert Outbox
    fsid <- insert FollowerSet
    let actor = Actor
            { actorName      = name
            , actorDesc      = desc
            , actorCreatedAt = now
            , actorInbox     = ibid
            , actorOutbox    = obid
            , actorFollowers = fsid
            , actorErrbox    = rbid
            }
    actorID <- insert actor
    case create of
        Left (_, _, obiid) -> insert_ $ ActorCreateLocal actorID obiid
        Right (ra, _, act) -> insert_ $ ActorCreateRemote actorID act (VA.remoteAuthorId ra)
    return $ Entity actorID actor

updateOutboxItem
    :: (MonadSite m, SiteEnv m ~ App)
    => LocalActorBy Key
    -> OutboxItemId
    -> AP.Action URIMode
    -> ReaderT SqlBackend m LocalURI
updateOutboxItem actorByKey itemID action = do
    encodeRouteLocal <- getEncodeRouteLocal
    hLocal <- asksSite siteInstanceHost
    actorByHash <- hashLocalActor actorByKey
    itemHash <- encodeKeyHashid itemID
    let luId = encodeRouteLocal $ activityRoute actorByHash itemHash
        luActor = encodeRouteLocal $ renderLocalActor actorByHash
        doc = AP.Doc hLocal $ AP.makeActivity luId luActor action
    update itemID [OutboxItemActivity =. persistJSONObjectFromDoc doc]
    return luId

updateOutboxItem'
    :: WA.StageRoute VA.Staje ~ Route App
    => LocalActorBy Key
    -> OutboxItemId
    -> AP.Action URIMode
    -> VA.ActDB LocalURI
updateOutboxItem' actorByKey itemID action = do
    encodeRouteLocal <- WA.getEncodeRouteLocal
    hLocal <- asksEnv WA.stageInstanceHost
    actorByHash <- VA.hashLocalActor actorByKey
    itemHash <- WAP.encodeKeyHashid itemID
    let luId = encodeRouteLocal $ activityRoute actorByHash itemHash
        luActor = encodeRouteLocal $ renderLocalActor actorByHash
        doc = AP.Doc hLocal $ AP.makeActivity luId luActor action
    update itemID [OutboxItemActivity =. persistJSONObjectFromDoc doc]
    return luId

fillPerActorKeys :: Worker ()
fillPerActorKeys = do
    perActor <- asksSite $ appPerActorKeys . appSettings
    when perActor $ do
        actorIDs <- runSiteDB $ E.select $ E.from $ \ (actor `E.LeftOuterJoin` sigkey) -> do
            E.on $ E.just (actor E.^. ActorId) E.==. sigkey E.?. SigKeyActor
            E.where_ $ E.isNothing $ sigkey E.?. SigKeyId
            return $ actor E.^. ActorId
        keys <- for actorIDs $ \ (E.Value actorID) -> do
            key <- liftIO AK.generateActorKey
            return $ SigKey actorID key
        runSiteDB $ insertMany_ keys
        logInfo $
            T.concat ["Filled ", T.pack (show $ length keys), " actor keys"]

getPersonWidgetInfo
    :: MonadIO m
    => Either PersonId RemoteActorId
    -> ReaderT SqlBackend m
        (Either (Entity Person, Actor) (Instance, RemoteObject, RemoteActor))
getPersonWidgetInfo = bitraverse getLocal getRemote
    where
    getLocal personID = do
        person <- getJust personID
        actor <- getJust $ personActor person
        return (Entity personID person, actor)
    getRemote remoteActorID = do
        remoteActor <- getJust remoteActorID
        remoteObject <- getJust $ remoteActorIdent remoteActor
        inztance <- getJust $ remoteObjectInstance remoteObject
        return (inztance, remoteObject, remoteActor)

getActivityBody
    :: MonadIO m
    => Either OutboxItemId RemoteActivityId
    -> ReaderT SqlBackend m (AP.Doc AP.Activity URIMode)
getActivityBody k = do
    obj <-
        persistJSONDoc <$>
            case k of
                Left itemID -> outboxItemActivity <$> getJust itemID
                Right itemID -> remoteActivityContent <$> getJust itemID
    case fromJSON $ Object obj of
        Error s -> error $ "Parsing activity " ++ show k ++ " failed: " ++ s
        Success doc -> return doc

getRemoteActor = runMaybeT . getRemoteActorM

getRemoteActorM (ObjURI h lu) = do
    Entity instanceID inztance <- MaybeT $ getBy $ UniqueInstance h
    Entity objectID object <- MaybeT $ getBy $ UniqueRemoteObject instanceID lu
    actor <- MaybeT $ getBy $ UniqueRemoteActor objectID
    return (inztance, object, actor)

getRemoteActorE u e = do
    ma <- lift $ getRemoteActor u
    fromMaybeE ma e

doneDB :: InboxItemId -> Text -> VA.ActE (Text, VA.Act (), Next)
doneDB itemID msg = do
    lift $ VA.withDB $ update itemID [InboxItemResult =. msg]
    done msg

-- | Insert an activity delivered to us into our inbox. Return its
-- database ID if the activity wasn't already in our inbox.
insertToInbox
    :: UTCTime
    -> Either
        (LocalActorBy Key, ActorId, OutboxItemId)
        (VA.RemoteAuthor, LocalURI, Maybe ByteString)
    -> VA.ActivityBody
    -> InboxId
    -> Bool
    -> VA.ActDB
        (Maybe
            ( InboxItemId
            , Either
                (LocalActorBy Key, ActorId, OutboxItemId)
                (VA.RemoteAuthor, LocalURI, RemoteActivityId)
            )
        )
insertToInbox now (Left a@(_, _, outboxItemID)) _body inboxID unread = do
    inboxItemID <- insert $ InboxItem unread now "No result yet"
    maybeItem <- insertUnique $ InboxItemLocal inboxID outboxItemID inboxItemID
    case maybeItem of
        Nothing -> do
            delete inboxItemID
            return Nothing
        Just _ -> return $ Just (inboxItemID, Left a)
insertToInbox now (Right (author, luAct, _)) body inboxID unread = do
    let iidAuthor = VA.remoteAuthorInstance author
    roid <-
        either entityKey id <$> insertBy' (RemoteObject iidAuthor luAct)
    ractid <- either entityKey id <$> insertBy' RemoteActivity
        { remoteActivityIdent    = roid
        , remoteActivityContent  = persistJSONFromBL $ VA.actbBL body
        , remoteActivityReceived = now
        }
    ibiid <- insert $ InboxItem unread now "No result yet"
    mibrid <- insertUnique $ InboxItemRemote inboxID ractid ibiid
    case mibrid of
        Nothing -> do
            delete ibiid
            return Nothing
        Just _ -> return $ Just (ibiid, Right (author, luAct, ractid))

adaptErrbox
    :: InboxId
    -> Bool
    -> (Verse -> VA.ActE (Text, VA.Act (), Next))
    -> Verse -> VA.ActE (Text, VA.Act (), Next)
adaptErrbox inboxID unread behavior verse@(VA.Verse authorIdMsig body) = do
    result <- lift $ runExceptT $ behavior verse
    case result of
        Right success -> return success
        Left err -> do
            now <- liftIO getCurrentTime
            _ <- lift $ VA.withDB $ runMaybeT $ do
                _ <- MaybeT $ get inboxID
                (itemID, _) <- MaybeT $ insertToInbox now authorIdMsig body inboxID unread
                lift $ update itemID [InboxItemResult =. err]
            throwE err

getActivityIdent
    :: MonadIO m
    => Either OutboxItemId RemoteActivityId
    -> ReaderT SqlBackend m (Either (LocalActorBy Key, OutboxItemId) FedURI)
getActivityIdent =
    bitraverse
        (\ itemID -> do
            OutboxItem outboxID _ time <- getJust itemID
            Entity actorID actor <- getByJust $ UniqueActorOutbox outboxID
            (,itemID) <$> getLocalActor actorID
        )
        (\ actID -> do
            act <- getJust actID
            getRemoteActivityURI act
        )

getRemoteActorData actorID = do
    actor <- getJust actorID
    object <- getJust $ remoteActorIdent actor
    inztance <- getJust $ remoteObjectInstance object
    return (inztance, object, actor)

getResourceOrigin
    :: MonadIO m
    => ResourceId
    -> SqlPersistT m
        (Maybe
            (Either
                (Entity ResourceOriginLocal)
                (Entity ResourceOriginRemote)
            )
        )
getResourceOrigin k = do
    l <- getBy $ UniqueResourceOriginLocal k
    r <- getBy $ UniqueResourceOriginRemote k
    case (l, r) of
        (Nothing, Nothing) -> pure Nothing
        (Just o, Nothing) -> pure $ Just $ Left o
        (Nothing, Just o) -> pure $ Just $ Right o
        (Just _, Just _) -> error "Both local and remote resource origin"
