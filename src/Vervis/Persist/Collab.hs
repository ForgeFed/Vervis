{- This file is part of Vervis.
 -
 - Written in 2022, 2023, 2024 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Persist.Collab
    ( getCollabTopic
    , getCollabRecip
    , getPermitTopicLocal
    , getPermitTopic
    , getStemIdent
    , getStemProject
    , getStemAdd
    , getGrantRecip
    , getComponentE
    , getCollabs
    , getCollabInvites
    , getCollabJoins

    , verifyCapability
    , verifyCapability'

    , getGrant

    , getComponentIdent
    , getComponentAdd
    , getSourceTopic
    , getSourceAdd
    , getDestTopic
    , getDestHolder
    , getDestAdd

    , checkExistingStems
    , checkExistingPermits

    , verifyNoStartedProjectChildren
    , verifyNoStartedGroupParents
    , verifyNoEnabledProjectChildren
    , verifyNoEnabledGroupParents

    , verifyNoStartedProjectParents
    , verifyNoStartedGroupChildren
    , verifyNoEnabledProjectParents
    , verifyNoEnabledGroupChildren

    , getGrantActivityBody

    , getPermitsForResource
    , getPermitsForResources
    , getCapability

    , getStems
    , getStemDrafts

    , getResourceTeams
    , getResourceTeamDrafts

    , getSquadAdd
    , getSquadTeam

    , getTeamResources
    , getTeamResourceDrafts

    , getEffortAdd
    , getEffortTopic

    , verifyNoStartedGroupResources
    , verifyNoStartedResourceTeams
    )
where

import Control.Applicative
import Control.Arrow ((&&&))
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Foldable
import Data.List (sortOn)
import Data.Maybe
import Data.Text (Text)
import Data.Time.Clock
import Data.Traversable
import Database.Persist.Sql
import Optics.Core

import qualified Database.Esqueleto as E

import Network.FedURI

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Data.Maybe.Local
import Database.Persist.Local

import Vervis.Actor
import Vervis.Data.Actor
import Vervis.Data.Collab
import Vervis.FedURI
import Vervis.Model
import Vervis.Persist.Actor

getCollabTopic
    :: MonadIO m => CollabId -> ReaderT SqlBackend m (LocalResourceBy Key)
getCollabTopic collabID = do
    Collab _ resourceID <- getJust collabID
    getLocalResource resourceID

getCollabRecip
    :: MonadIO m
    => CollabId
    -> ReaderT SqlBackend m
        (Either (Entity CollabRecipLocal) (Entity CollabRecipRemote))
getCollabRecip collabID =
    requireEitherAlt
        (getBy $ UniqueCollabRecipLocal collabID)
        (getBy $ UniqueCollabRecipRemote collabID)
        "Collab without recip"
        "Collab with both local and remote recip"

getPermitTopicLocal
    :: MonadIO m
    => PermitTopicLocalId
    -> ReaderT SqlBackend m (LocalResourceBy Key)
getPermitTopicLocal localID = do
    PermitTopicLocal _ resourceID <- getJust localID
    getLocalResource resourceID

getPermitTopic
    :: MonadIO m
    => PermitId
    -> ReaderT SqlBackend m
        (Either
            (PermitTopicLocalId, LocalResourceBy Key)
            (PermitTopicRemoteId, RemoteActorId)
        )
getPermitTopic permitID = do
    topic <-
        requireEitherAlt
            (getKeyBy $ UniquePermitTopicLocal permitID)
            (getBy $ UniquePermitTopicRemote permitID)
            "Permit without topic"
            "Permit with both local and remote topic"
    bitraverse
        (\ localID -> (localID,) <$> getPermitTopicLocal localID)
        (\ (Entity topicID (PermitTopicRemote _ actorID)) ->
            return (topicID, actorID)
        )
        topic

getStemIdent :: MonadIO m => StemId -> ReaderT SqlBackend m (ComponentBy Key)
getStemIdent stemID = do
    Stem _ komponentID <- getJust stemID
    getLocalComponent komponentID

getStemProject
    :: MonadIO m
    => StemId
    -> ReaderT SqlBackend m (Either ProjectId RemoteActorId)
getStemProject stemID =
    requireEitherAlt
        (fmap stemProjectLocalProject <$> getValBy (UniqueStemProjectLocal stemID))
        (fmap stemProjectRemoteProject <$> getValBy (UniqueStemProjectRemote stemID))
        "Found Stem without project"
        "Found Stem with multiple projects"

getStemAdd
    :: MonadIO m
    => StemId
    -> ReaderT SqlBackend m
        (Either
            (LocalActorBy Key, OutboxItemId)
            FedURI
        )
getStemAdd stemID = do
    usOrThem <-
        requireEitherAlt
            (getKeyBy $ UniqueStemOriginAdd stemID)
            (getKeyBy $ UniqueStemOriginInvite stemID)
            "Neither us nor them"
            "Both us and them"
    add <-
        case usOrThem of
            Left _usID ->
                requireEitherAlt
                    (fmap stemComponentGestureLocalActivity <$> getValBy (UniqueStemComponentGestureLocal stemID))
                    (fmap stemComponentGestureRemoteActivity <$> getValBy (UniqueStemComponentGestureRemote stemID))
                    "Neither local not remote"
                    "Both local and remote"
            Right themID ->
                requireEitherAlt
                    (fmap stemProjectGestureLocalInvite <$> getValBy (UniqueStemProjectGestureLocal themID))
                    (fmap stemProjectGestureRemoteInvite <$> getValBy (UniqueStemProjectGestureRemote themID))
                    "Neither local not remote"
                    "Both local and remote"
    getActivityIdent add

getGrantRecip (GrantRecipPerson k) e = GrantRecipPerson <$> getEntityE k e

getComponentE (ComponentRepo k) e = ComponentRepo <$> getEntityE k e
getComponentE (ComponentDeck k) e = ComponentDeck <$> getEntityE k e
getComponentE (ComponentLoom k) e = ComponentLoom <$> getEntityE k e

getCollabs
    :: MonadIO m
    => ResourceId
    -> ReaderT SqlBackend m
        [ ( AP.Role
          , UTCTime
          , Either (Entity Person, Actor) (Instance, RemoteObject, RemoteActor)
          , CollabId
          )
        ]
getCollabs resourceID =
    fmap (sortOn $ view _2) $ liftA2 (++)
        (map (\ (E.Value role, E.Value time, person, Entity _ actor, E.Value collabID) ->
                (role, time, Left (person, actor), collabID)
             )
            <$> getLocals
        )
        (map (\ (E.Value role, E.Value time, Entity _ i, Entity _ ro, Entity _ ra, E.Value collabID) ->
                (role, time, Right (i, ro, ra), collabID)
             )
            <$> getRemotes
        )
    where
    getLocals =
        E.select $ E.from $ \ (collab `E.InnerJoin` enable `E.InnerJoin` grant `E.InnerJoin` recip `E.InnerJoin` person `E.InnerJoin` actor) -> do
            E.on $ person E.^. PersonActor E.==. actor E.^. ActorId
            E.on $ recip E.^. CollabRecipLocalPerson E.==. person E.^. PersonId
            E.on $ collab E.^. CollabId E.==. recip E.^. CollabRecipLocalCollab
            E.on $ enable E.^. CollabEnableGrant E.==. grant E.^. OutboxItemId
            E.on $ collab E.^. CollabId E.==. enable E.^. CollabEnableCollab
            E.where_ $ collab E.^. CollabTopic E.==. E.val resourceID
            E.orderBy [E.desc $ enable E.^. CollabEnableId]
            return
                ( collab E.^. CollabRole
                , grant E.^. OutboxItemPublished
                , person
                , actor
                , collab E.^. CollabId
                )
    getRemotes =
        E.select $ E.from $ \ (collab `E.InnerJoin` enable `E.InnerJoin` grant `E.InnerJoin` recip `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ recip E.^. CollabRecipRemoteActor E.==. ra E.^. RemoteActorId
            E.on $ collab E.^. CollabId E.==. recip E.^. CollabRecipRemoteCollab
            E.on $ enable E.^. CollabEnableGrant E.==. grant E.^. OutboxItemId
            E.on $ collab E.^. CollabId E.==. enable E.^. CollabEnableCollab
            E.where_ $ collab E.^. CollabTopic E.==. E.val resourceID
            E.orderBy [E.desc $ enable E.^. CollabEnableId]
            return
                ( collab E.^. CollabRole
                , grant E.^. OutboxItemPublished
                , i
                , ro
                , ra
                , collab E.^. CollabId
                )

getCollabInvites
    :: MonadIO m
    => ResourceId
    -> ReaderT SqlBackend m
        [ ( AP.Role
          , UTCTime
          , Either (Entity Person, Actor) (Instance, RemoteObject, RemoteActor)
          , Either (Entity Person, Actor) (Instance, RemoteObject, RemoteActor)
          , CollabId
          )
        ]
getCollabInvites resourceID = sortOn (view _2) . concat <$> sequence
    [ map
        (\ (E.Value role, E.Value time, mperson_I, Entity _ actor_I, person, Entity _ actor, E.Value collabID) ->
            let person_I = fromMaybe (error "getCollabInvites LL local inviter isn't a Person") mperson_I
            in  (role, time, Left (person_I, actor_I), Left (person, actor), collabID)
        )
        <$> getLL
    , map
        (\ (E.Value role, E.Value time, Entity _ iI, Entity _ roI, Entity _ raI, person, Entity _ actor, E.Value collabID) ->
            (role, time, Right (iI, roI, raI), Left (person, actor), collabID)
        )
        <$> getLR
    , map
        (\ (E.Value role, E.Value time, mperson_I, Entity _ actor_I, Entity _ i, Entity _ ro, Entity _ ra, E.Value collabID) ->
            let person_I = fromMaybe (error "getCollabInvites RL local inviter isn't a Person") mperson_I
            in  (role, time, Left (person_I, actor_I), Right (i, ro, ra), collabID)
        )
        <$> getRL
    , map
        (\ (E.Value role, E.Value time, Entity _ iI, Entity _ roI, Entity _ raI, Entity _ i, Entity _ ro, Entity _ ra, E.Value collabID) ->
            (role, time, Right (iI, roI, raI), Right (i, ro, ra), collabID)
        )
        <$> getRR
    ]
    where
    getLL =
        E.select $ E.from $ \ (collab `E.LeftOuterJoin` enable `E.InnerJoin` fulfills `E.InnerJoin` recip `E.InnerJoin` personR `E.InnerJoin` actorR `E.InnerJoin` inviter `E.InnerJoin` item `E.InnerJoin` actorI `E.LeftOuterJoin` personI) -> do
            E.on $ E.just (actorI E.^. ActorId) E.==. personI E.?. PersonActor
            E.on $ item E.^. OutboxItemOutbox E.==. actorI E.^. ActorOutbox
            E.on $ inviter E.^. CollabInviterLocalInvite E.==. item E.^. OutboxItemId
            E.on $ fulfills E.^. CollabFulfillsInviteId E.==. inviter E.^. CollabInviterLocalCollab
            E.on $ personR E.^. PersonActor E.==. actorR E.^. ActorId
            E.on $ recip E.^. CollabRecipLocalPerson E.==. personR E.^. PersonId
            E.on $ collab E.^. CollabId E.==. recip E.^. CollabRecipLocalCollab
            E.on $ collab E.^. CollabId E.==. fulfills E.^. CollabFulfillsInviteCollab
            E.on $ E.just (collab E.^. CollabId) E.==. enable E.?. CollabEnableCollab
            E.where_ $
                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                E.isNothing (enable E.?. CollabEnableId)
            E.orderBy [E.desc $ collab E.^. CollabId]
            return
                ( collab E.^. CollabRole
                , item E.^. OutboxItemPublished
                , personI
                , actorI
                , personR
                , actorR
                , collab E.^. CollabId
                )
    getLR =
        E.select $ E.from $ \ (collab `E.LeftOuterJoin` enable `E.InnerJoin` fulfills `E.InnerJoin` recip `E.InnerJoin` personR `E.InnerJoin` actorR `E.InnerJoin` inviter `E.InnerJoin` activity `E.InnerJoin` raI `E.InnerJoin` roI `E.InnerJoin` iI) -> do
            E.on $ roI E.^. RemoteObjectInstance E.==. iI E.^. InstanceId
            E.on $ raI E.^. RemoteActorIdent E.==. roI E.^. RemoteObjectId
            E.on $ inviter E.^. CollabInviterRemoteActor E.==. raI E.^. RemoteActorId
            E.on $ inviter E.^. CollabInviterRemoteInvite E.==. activity E.^. RemoteActivityId
            E.on $ fulfills E.^. CollabFulfillsInviteId E.==. inviter E.^. CollabInviterRemoteCollab
            E.on $ personR E.^. PersonActor E.==. actorR E.^. ActorId
            E.on $ recip E.^. CollabRecipLocalPerson E.==. personR E.^. PersonId
            E.on $ collab E.^. CollabId E.==. recip E.^. CollabRecipLocalCollab
            E.on $ collab E.^. CollabId E.==. fulfills E.^. CollabFulfillsInviteCollab
            E.on $ E.just (collab E.^. CollabId) E.==. enable E.?. CollabEnableCollab
            E.where_ $
                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                E.isNothing (enable E.?. CollabEnableId)
            E.orderBy [E.desc $ collab E.^. CollabId]
            return
                ( collab E.^. CollabRole
                , activity E.^. RemoteActivityReceived
                , iI
                , roI
                , raI
                , personR
                , actorR
                , collab E.^. CollabId
                )
    getRL =
        E.select $ E.from $ \ (collab `E.LeftOuterJoin` enable `E.InnerJoin` fulfills `E.InnerJoin` recip `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i `E.InnerJoin` inviter `E.InnerJoin` item `E.InnerJoin` actorI `E.LeftOuterJoin` personI) -> do
            E.on $ E.just (actorI E.^. ActorId) E.==. personI E.?. PersonActor
            E.on $ item E.^. OutboxItemOutbox E.==. actorI E.^. ActorOutbox
            E.on $ inviter E.^. CollabInviterLocalInvite E.==. item E.^. OutboxItemId
            E.on $ fulfills E.^. CollabFulfillsInviteId E.==. inviter E.^. CollabInviterLocalCollab
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ recip E.^. CollabRecipRemoteActor E.==. ra E.^. RemoteActorId
            E.on $ collab E.^. CollabId E.==. recip E.^. CollabRecipRemoteCollab
            E.on $ collab E.^. CollabId E.==. fulfills E.^. CollabFulfillsInviteCollab
            E.on $ E.just (collab E.^. CollabId) E.==. enable E.?. CollabEnableCollab
            E.where_ $
                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                E.isNothing (enable E.?. CollabEnableId)
            E.orderBy [E.desc $ collab E.^. CollabId]
            return
                ( collab E.^. CollabRole
                , item E.^. OutboxItemPublished
                , personI
                , actorI
                , i
                , ro
                , ra
                , collab E.^. CollabId
                )
    getRR =
        E.select $ E.from $ \ (collab `E.LeftOuterJoin` enable `E.InnerJoin` fulfills `E.InnerJoin` recip `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i `E.InnerJoin` inviter `E.InnerJoin` activity `E.InnerJoin` raI `E.InnerJoin` roI `E.InnerJoin` iI) -> do
            E.on $ roI E.^. RemoteObjectInstance E.==. iI E.^. InstanceId
            E.on $ raI E.^. RemoteActorIdent E.==. roI E.^. RemoteObjectId
            E.on $ inviter E.^. CollabInviterRemoteActor E.==. raI E.^. RemoteActorId
            E.on $ inviter E.^. CollabInviterRemoteInvite E.==. activity E.^. RemoteActivityId
            E.on $ fulfills E.^. CollabFulfillsInviteId E.==. inviter E.^. CollabInviterRemoteCollab
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ recip E.^. CollabRecipRemoteActor E.==. ra E.^. RemoteActorId
            E.on $ collab E.^. CollabId E.==. recip E.^. CollabRecipRemoteCollab
            E.on $ collab E.^. CollabId E.==. fulfills E.^. CollabFulfillsInviteCollab
            E.on $ E.just (collab E.^. CollabId) E.==. enable E.?. CollabEnableCollab
            E.where_ $
                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                E.isNothing (enable E.?. CollabEnableId)
            E.orderBy [E.desc $ collab E.^. CollabId]
            return
                ( collab E.^. CollabRole
                , activity E.^. RemoteActivityReceived
                , iI
                , roI
                , raI
                , i
                , ro
                , ra
                , collab E.^. CollabId
                )

getCollabJoins
    :: MonadIO m
    => ResourceId
    -> ReaderT SqlBackend m
        [ ( AP.Role
          , UTCTime
          , Either (Entity Person, Actor) (Instance, RemoteObject, RemoteActor)
          , CollabId
          )
        ]
getCollabJoins resourceID =
    fmap (sortOn $ view _2) $ liftA2 (++)
        (map (\ (E.Value role, E.Value time, person, Entity _ actor, E.Value collabID) ->
                (role, time, Left (person, actor), collabID)
             )
            <$> getLocals
        )
        (map (\ (E.Value role, E.Value time, Entity _ i, Entity _ ro, Entity _ ra, E.Value collabID) ->
                (role, time, Right (i, ro, ra), collabID)
             )
            <$> getRemotes
        )
    where
    getLocals =
        E.select $ E.from $ \ (collab `E.LeftOuterJoin` enable `E.InnerJoin` fulfills `E.InnerJoin` join `E.InnerJoin` recip `E.InnerJoin` person `E.InnerJoin` actor `E.InnerJoin` item) -> do
            E.on $ join E.^. CollabRecipLocalJoinJoin E.==. item E.^. OutboxItemId
            E.on $ person E.^. PersonActor E.==. actor E.^. ActorId
            E.on $ recip E.^. CollabRecipLocalPerson E.==. person E.^. PersonId
            E.on $ join E.^. CollabRecipLocalJoinCollab E.==. recip E.^. CollabRecipLocalId
            E.on $ fulfills E.^. CollabFulfillsJoinId E.==. join E.^. CollabRecipLocalJoinFulfills
            E.on $ collab E.^. CollabId E.==. fulfills E.^. CollabFulfillsJoinCollab
            E.on $ E.just (collab E.^. CollabId) E.==. enable E.?. CollabEnableCollab
            E.where_ $
                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                E.isNothing (enable E.?. CollabEnableId)
            E.orderBy [E.desc $ collab E.^. CollabId]
            return
                ( collab E.^. CollabRole
                , item E.^. OutboxItemPublished
                , person
                , actor
                , collab E.^. CollabId
                )
    getRemotes =
        E.select $ E.from $ \ (collab `E.LeftOuterJoin` enable `E.InnerJoin` fulfills `E.InnerJoin` join `E.InnerJoin` recip `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i `E.InnerJoin` activity) -> do
            E.on $ join E.^. CollabRecipRemoteJoinJoin E.==. activity E.^. RemoteActivityId
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ recip E.^. CollabRecipRemoteActor E.==. ra E.^. RemoteActorId
            E.on $ join E.^. CollabRecipRemoteJoinCollab E.==. recip E.^. CollabRecipRemoteId
            E.on $ fulfills E.^. CollabFulfillsJoinId E.==. join E.^. CollabRecipRemoteJoinFulfills
            E.on $ collab E.^. CollabId E.==. fulfills E.^. CollabFulfillsJoinCollab
            E.on $ E.just (collab E.^. CollabId) E.==. enable E.?. CollabEnableCollab
            E.where_ $
                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                E.isNothing (enable E.?. CollabEnableId)
            E.orderBy [E.desc $ collab E.^. CollabId]
            return
                ( collab E.^. CollabRole
                , activity E.^. RemoteActivityReceived
                , i
                , ro
                , ra
                , collab E.^. CollabId
                )

verifyCapability
    :: MonadIO m
    => (LocalActorBy Key, OutboxItemId)
    -> Either PersonId RemoteActorId
    -> LocalResourceBy Key
    -> AP.Role
    -> ExceptT Text (ReaderT SqlBackend m) ()
verifyCapability (capActor, capItem) actor resource requiredRole = do

    -- Find the activity itself by URI in the DB
    nameExceptT "Capability activity not found" $
        verifyLocalActivityExistsInDB capActor capItem

    -- Find the Collab record for that activity
    collabID <- do
        maybeEnable <- lift $ getValBy $ UniqueCollabEnableGrant capItem
        collabEnableCollab <$>
            fromMaybeE maybeEnable "No CollabEnable for this activity"

    -- Find the recipient of that Collab
    recipID <-
        lift $ bimap collabRecipLocalPerson collabRecipRemoteActor <$>
            requireEitherAlt
                (getValBy $ UniqueCollabRecipLocal collabID)
                (getValBy $ UniqueCollabRecipRemote collabID)
                "No collab recip"
                "Both local and remote recips for collab"

    -- Verify the recipient is the expected one
    unless (recipID == actor) $
        throwE "Collab recipient is someone else"

    -- Find the local topic, on which this Collab gives access
    topic <- lift $ getCollabTopic collabID

    -- Verify that topic is indeed the sender of the Grant
    unless (resourceToActor topic == capActor) $
        error "Grant sender isn't the topic"

    -- Verify the topic matches the resource specified
    unless (topic == resource) $
        throwE "Capability topic is some other local resource"

    -- Verify that the granted role is equal or greater than the required role
    Collab givenRole _ <- lift $ getJust collabID
    unless (givenRole >= requiredRole) $
        throwE "The granted role doesn't allow the requested operation"

verifyCapability'
    :: MonadIO m
    => (LocalActorBy Key, OutboxItemId)
    -> Either
        (LocalActorBy Key, ActorId, OutboxItemId)
        (RemoteAuthor, LocalURI, Maybe ByteString)
    -> LocalResourceBy Key
    -> AP.Role
    -> ExceptT Text (ReaderT SqlBackend m) ()
verifyCapability' cap actor resource role = do
    actorP <- processActor actor
    verifyCapability cap actorP resource role
    where
    processActor = bitraverse processLocal processRemote
        where
        processLocal (actorByKey, _, _) =
            case actorByKey of
                LocalActorPerson personID -> return personID
                _ -> throwE "Non-person local actors can't get Grants at the moment"
        processRemote (author, _, _) = pure $ remoteAuthorId author

getGrant
    :: MonadIO m
    => ResourceId
    -> PersonId
    -> ReaderT SqlBackend m (Maybe OutboxItemId)
getGrant resourceID personID = do
    items <-
        E.select $ E.from $ \ (collab `E.InnerJoin` enable `E.InnerJoin` grant `E.InnerJoin` recipL) -> do
            E.on $ enable E.^. CollabEnableCollab E.==. recipL E.^. CollabRecipLocalCollab
            E.on $ enable E.^. CollabEnableGrant E.==. grant E.^. OutboxItemId
            E.on $ collab E.^. CollabId E.==. enable E.^. CollabEnableCollab
            E.where_ $
                collab E.^. CollabTopic E.==. E.val resourceID E.&&.
                recipL E.^. CollabRecipLocalPerson E.==. E.val personID
            return $ grant E.^. OutboxItemId
    case items of
        [] -> return Nothing
        [E.Value i] -> return $ Just i
        _ -> error $ "Multiple grants for a Person in " ++ show resourceID

getComponentIdent
    :: MonadIO m
    => ComponentId
    -> ReaderT SqlBackend m
        (Either
            (ComponentLocalId, ComponentBy Key)
            (ComponentRemoteId, RemoteActorId)
        )
getComponentIdent componentID = do
    ident <-
        requireEitherAlt
            (getBy $ UniqueComponentLocal componentID)
            (getBy $ UniqueComponentRemote componentID)
            "Found Component without ident"
            "Found Component with both local and remote ident"
    bitraverse
        (\ (Entity localID (ComponentLocal _ komponentID)) ->
            (localID,) <$> getLocalComponent komponentID
        )
        (\ (Entity k v) -> pure (k, componentRemoteActor v))
        ident

getComponentAdd
    :: MonadIO m
    => ComponentId
    -> ReaderT SqlBackend m
        (Either
            (LocalActorBy Key, OutboxItemId)
            FedURI
        )
getComponentAdd componentID = do
    usOrThem <-
        requireEitherAlt
            (getKeyBy $ UniqueComponentOriginInvite componentID)
            (getKeyBy $ UniqueComponentOriginAdd componentID)
            "Neither us nor them"
            "Both us and them"
    add <-
        case usOrThem of
            Left _usID ->
                requireEitherAlt
                    (fmap componentProjectGestureLocalActivity <$> getValBy (UniqueComponentProjectGestureLocal componentID))
                    (fmap componentProjectGestureRemoteActivity <$> getValBy (UniqueComponentProjectGestureRemote componentID))
                    "Neither local not remote"
                    "Both local and remote"
            Right themID ->
                requireEitherAlt
                    (fmap componentGestureLocalAdd <$> getValBy (UniqueComponentGestureLocal themID))
                    (fmap componentGestureRemoteAdd <$> getValBy (UniqueComponentGestureRemote themID))
                    "Neither local not remote"
                    "Both local and remote"
    getActivityIdent add

getSourceTopic
    :: MonadIO m
    => SourceId
    -> ReaderT SqlBackend m
        (Either
            (SourceTopicLocalId, Either ProjectId GroupId)
            (SourceTopicRemoteId, RemoteActorId)
        )
getSourceTopic sourceID = do
    ident <-
        requireEitherAlt
            (getKeyBy $ UniqueSourceTopicLocal sourceID)
            (getBy $ UniqueSourceTopicRemote sourceID)
            "Found Source without topic"
            "Found Source with both local and remote topic"
    bitraverse
        (\ localID -> (localID,) <$> do
            requireEitherAlt
                (fmap sourceTopicProjectChild <$> getValBy (UniqueSourceTopicProjectTopic localID))
                (fmap sourceTopicGroupParent <$> getValBy (UniqueSourceTopicGroupTopic localID))
                "Found SourceTopicLocal without topic"
                "Found SourceTopicLocal with multiple topics"
        )
        (\ (Entity k v) -> pure (k, sourceTopicRemoteTopic v))
        ident

getSourceAdd
    :: MonadIO m
    => SourceId
    -> ReaderT SqlBackend m
        (Either
            (LocalActorBy Key, OutboxItemId)
            FedURI
        )
getSourceAdd sourceID = do
    usOrThem <-
        requireEitherAlt
            (getKeyBy $ UniqueSourceOriginUs sourceID)
            (getKeyBy $ UniqueSourceOriginThem sourceID)
            "Neither us nor them"
            "Both us and them"
    add <-
        case usOrThem of
            Left usID ->
                requireEitherAlt
                    (fmap sourceUsGestureLocalAdd <$> getValBy (UniqueSourceUsGestureLocal usID))
                    (fmap sourceUsGestureRemoteAdd <$> getValBy (UniqueSourceUsGestureRemote usID))
                    "Neither local not remote"
                    "Both local and remote"
            Right themID ->
                requireEitherAlt
                    (fmap sourceThemGestureLocalAdd <$> getValBy (UniqueSourceThemGestureLocal themID))
                    (fmap sourceThemGestureRemoteAdd <$> getValBy (UniqueSourceThemGestureRemote themID))
                    "Neither local not remote"
                    "Both local and remote"
    getActivityIdent add

getDestTopic
    :: MonadIO m
    => DestId
    -> ReaderT SqlBackend m
        (Either
            (DestTopicLocalId, Either ProjectId GroupId)
            (DestTopicRemoteId, RemoteActorId)
        )
getDestTopic destID = do
    ident <-
        requireEitherAlt
            (getKeyBy $ UniqueDestTopicLocal destID)
            (getBy $ UniqueDestTopicRemote destID)
            "Found Dest without topic"
            "Found Dest with both local and remote topic"
    bitraverse
        (\ localID -> (localID,) <$> do
            requireEitherAlt
                (fmap destTopicProjectParent <$> getValBy (UniqueDestTopicProjectTopic localID))
                (fmap destTopicGroupChild <$> getValBy (UniqueDestTopicGroupTopic localID))
                "Found DestTopicLocal without topic"
                "Found DestTopicLocal with multiple topics"
        )
        (\ (Entity k v) -> pure (k, destTopicRemoteTopic v))
        ident

getDestHolder
    :: MonadIO m
    => DestId
    -> ReaderT SqlBackend m
        (Either
            (DestHolderProjectId, ProjectId)
            (DestHolderGroupId, GroupId)
        )
getDestHolder destID =
    bimap
        (\ (Entity k (DestHolderProject _ j)) -> (k, j))
        (\ (Entity k (DestHolderGroup _ g)) -> (k, g))
        <$>
    requireEitherAlt
        (getBy $ UniqueDestHolderProject destID)
        (getBy $ UniqueDestHolderGroup destID)
        "Found Dest without holder"
        "Found Dest with both project and team holder"

getDestAdd
    :: MonadIO m
    => DestId
    -> ReaderT SqlBackend m
        (Either
            (LocalActorBy Key, OutboxItemId)
            FedURI
        )
getDestAdd destID = do
    usOrThem <-
        requireEitherAlt
            (getKeyBy $ UniqueDestOriginUs destID)
            (getKeyBy $ UniqueDestOriginThem destID)
            "Neither us nor them"
            "Both us and them"
    add <-
        case usOrThem of
            Left _usID ->
                requireEitherAlt
                    (fmap destUsGestureLocalActivity <$> getValBy (UniqueDestUsGestureLocal destID))
                    (fmap destUsGestureRemoteActivity <$> getValBy (UniqueDestUsGestureRemote destID))
                    "Neither local not remote"
                    "Both local and remote"
            Right themID ->
                requireEitherAlt
                    (fmap destThemGestureLocalAdd <$> getValBy (UniqueDestThemGestureLocal themID))
                    (fmap destThemGestureRemoteAdd <$> getValBy (UniqueDestThemGestureRemote themID))
                    "Neither local not remote"
                    "Both local and remote"
    getActivityIdent add

checkExistingStems
    :: KomponentId -> Either (Entity Project) RemoteActorId -> ActDBE ()
checkExistingStems komponentID projectDB = do

    -- Find existing Stem records I have for this project
    stemIDs <- lift getExistingStems

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    byEnabled <-
        lift $ for stemIDs $ \ (_, stem) ->
            isJust <$> runMaybeT (tryStemEnabled stem)
    case length $ filter id byEnabled of
        0 -> return ()
        1 -> throwE "I already have a StemProjectGrant* for this project"
        _ -> error "Multiple StemProjectGrant* for a project"

    -- Verify none of the Stem records are already in
    -- Add-waiting-for-project or Invite-waiting-for-my-collaborator state
    anyStarted <-
        lift $ runMaybeT $ asum $
            map (\ (stemID, project) ->
                    tryStemAddAccept stemID <|>
                    tryStemInviteAccept stemID project
                )
                stemIDs
    unless (isNothing anyStarted) $
        throwE
            "One of the Stem records is already in Add-Accept or \
            \Invite-Accept state"

    where

    getExistingStems' kompID (Left (Entity projectID _)) =
        fmap (map $ bimap E.unValue (Left . E.unValue)) $
        E.select $ E.from $ \ (stem `E.InnerJoin` project) -> do
            E.on $ stem E.^. StemId E.==. project E.^. StemProjectLocalStem
            E.where_ $
                project E.^. StemProjectLocalProject E.==. E.val projectID E.&&.
                stem E.^. StemHolder E.==. E.val kompID
            return
                ( project E.^. StemProjectLocalStem
                , project E.^. StemProjectLocalId
                )
    getExistingStems' kompID (Right remoteActorID) =
        fmap (map $ bimap E.unValue (Right . E.unValue)) $
        E.select $ E.from $ \ (stem `E.InnerJoin` project) -> do
            E.on $ stem E.^. StemId E.==. project E.^. StemProjectRemoteStem
            E.where_ $
                project E.^. StemProjectRemoteProject E.==. E.val remoteActorID E.&&.
                stem E.^. StemHolder E.==. E.val kompID
            return
                ( project E.^. StemProjectRemoteStem
                , project E.^. StemProjectRemoteId
                )

    getExistingStems = getExistingStems' komponentID projectDB

    tryStemEnabled (Left localID) =
        const () <$> MaybeT (getBy $ UniqueStemProjectGrantLocalProject localID)
    tryStemEnabled (Right remoteID) =
        const () <$> MaybeT (getBy $ UniqueStemProjectGrantRemoteProject remoteID)

    tryStemAddAccept stemID = do
        _ <- MaybeT $ getBy $ UniqueStemOriginAdd stemID
        _ <- MaybeT $ getBy $ UniqueStemComponentAccept stemID
        pure ()

    tryStemInviteAccept stemID project = do
        originID <- MaybeT $ getKeyBy $ UniqueStemOriginInvite stemID
        case project of
            Left localID ->
                const () <$> MaybeT (getBy $ UniqueStemProjectAcceptLocalProject localID)
            Right remoteID ->
                const () <$> MaybeT (getBy $ UniqueStemProjectAcceptRemoteProject remoteID)

checkExistingPermits
    :: PersonId -> Either ResourceId RemoteActorId -> ActDBE ()
checkExistingPermits personID topicDB = do

    -- Find existing Permit records I have for this topic
    permitIDs <- lift $ getExistingPermits topicDB

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    byEnabled <-
        lift $ for permitIDs $ \ (_, permit) ->
            isJust <$> runMaybeT (tryPermitEnabled permit)
    case length $ filter id byEnabled of
        0 -> return ()
        1 -> throwE "I already have a PermitTopicEnable* for this topic"
        _ -> error "Multiple PermitTopicEnable* for a topic"

    -- Verify none of the Permit records are already in Join or
    -- Invite-and-Accept state
    anyStarted <-
        lift $ runMaybeT $ asum $
            map (\ (permitID, topic) ->
                    tryPermitJoin permitID <|>
                    tryPermitInviteAccept permitID topic
                )
                permitIDs
    unless (isNothing anyStarted) $
        throwE
            "One of the Permit records is already in Join or Invite-Accept \
            \state"

    where

    getExistingPermits (Left resourceID) =
        fmap (map $ bimap E.unValue (Left . E.unValue)) $
        E.select $ E.from $ \ (permit `E.InnerJoin` local) -> do
            E.on $ permit E.^. PermitId E.==. local E.^. PermitTopicLocalPermit
            E.where_ $
                permit E.^. PermitPerson E.==. E.val personID E.&&.
                local E.^. PermitTopicLocalTopic E.==. E.val resourceID
            return
                ( permit E.^. PermitId
                , local E.^. PermitTopicLocalId
                )
    getExistingPermits (Right remoteActorID) =
        fmap (map $ bimap E.unValue (Right . E.unValue)) $
        E.select $ E.from $ \ (permit `E.InnerJoin` remote) -> do
            E.on $ permit E.^. PermitId E.==. remote E.^. PermitTopicRemotePermit
            E.where_ $
                permit E.^. PermitPerson E.==. E.val personID E.&&.
                remote E.^. PermitTopicRemoteActor E.==. E.val remoteActorID
            return
                ( permit E.^. PermitId
                , remote E.^. PermitTopicRemoteId
                )

    tryPermitEnabled (Left localID) =
        const () <$> MaybeT (getBy $ UniquePermitTopicEnableLocalTopic localID)
    tryPermitEnabled (Right remoteID) =
        const () <$> MaybeT (getBy $ UniquePermitTopicEnableRemoteTopic remoteID)

    tryPermitJoin permitID = do
        _ <- MaybeT $ getBy $ UniquePermitFulfillsJoin permitID
        pure ()

    tryPermitInviteAccept permitID topic = do
        _fulfillsID <- MaybeT $ getKeyBy $ UniquePermitFulfillsInvite permitID
        case topic of
            Left localID ->
                const () <$> MaybeT (getBy $ UniquePermitTopicAcceptLocalTopic localID)
            Right remoteID ->
                const () <$> MaybeT (getBy $ UniquePermitTopicAcceptRemoteTopic remoteID)

getExistingProjectSources projectID (Left (Entity childID _)) =
    fmap (map $ bimap E.unValue (Left . E.unValue)) $
    E.select $ E.from $ \ (holder `E.InnerJoin` topic) -> do
        E.on $ holder E.^. SourceHolderProjectId E.==. topic E.^. SourceTopicProjectHolder
        E.where_ $
            holder E.^. SourceHolderProjectProject E.==. E.val projectID E.&&.
            topic E.^. SourceTopicProjectChild E.==. E.val childID
        return
            ( holder E.^. SourceHolderProjectSource
            , topic E.^. SourceTopicProjectTopic
            )
getExistingProjectSources projectID (Right childID) =
    fmap (map $ bimap E.unValue (Right . E.unValue)) $
    E.select $ E.from $ \ (holder `E.InnerJoin` topic) -> do
        E.on $ holder E.^. SourceHolderProjectSource E.==. topic E.^. SourceTopicRemoteSource
        E.where_ $
            holder E.^. SourceHolderProjectProject E.==. E.val projectID E.&&.
            topic E.^. SourceTopicRemoteTopic E.==. E.val childID
        return
            ( holder E.^. SourceHolderProjectSource
            , topic E.^. SourceTopicRemoteId
            )

getExistingGroupSources groupID (Left (Entity parentID _)) =
    fmap (map $ bimap E.unValue (Left . E.unValue)) $
    E.select $ E.from $ \ (holder `E.InnerJoin` topic) -> do
        E.on $ holder E.^. SourceHolderGroupId E.==. topic E.^. SourceTopicGroupHolder
        E.where_ $
            holder E.^. SourceHolderGroupGroup E.==. E.val groupID E.&&.
            topic E.^. SourceTopicGroupParent E.==. E.val parentID
        return
            ( holder E.^. SourceHolderGroupSource
            , topic E.^. SourceTopicGroupTopic
            )
getExistingGroupSources groupID (Right parentID) =
    fmap (map $ bimap E.unValue (Right . E.unValue)) $
    E.select $ E.from $ \ (holder `E.InnerJoin` topic) -> do
        E.on $ holder E.^. SourceHolderGroupSource E.==. topic E.^. SourceTopicRemoteSource
        E.where_ $
            holder E.^. SourceHolderGroupGroup E.==. E.val groupID E.&&.
            topic E.^. SourceTopicRemoteTopic E.==. E.val parentID
        return
            ( holder E.^. SourceHolderGroupSource
            , topic E.^. SourceTopicRemoteId
            )

verifySourcesNotEnabled sourceIDs = do
    byEnabled <-
        lift $ for sourceIDs $ \ (sourceID, _) ->
            isJust <$> runMaybeT (trySourceEnabled sourceID)
    case length $ filter id byEnabled of
        0 -> return ()
        1 -> throwE "I already have a SourceUsSendDelegator for this source"
        _ -> error "Multiple SourceUsSendDelegator for a source"
    where
    trySourceEnabled sourceID =
        const () <$> MaybeT (getBy $ UniqueSourceUsSendDelegator sourceID)

verifySourcesNotStarted sourceIDs = do
    anyStarted <-
        lift $ runMaybeT $ asum $
            map (\ (sourceID, topic) ->
                    trySourceUs sourceID <|>
                    trySourceThem sourceID topic
                )
                sourceIDs
    unless (isNothing anyStarted) $
        throwE "One of the Source records is already in Add-Accept state"
    where
    trySourceUs sourceID = do
        usID <- MaybeT $ getKeyBy $ UniqueSourceOriginUs sourceID
        const () <$> MaybeT (getBy $ UniqueSourceUsAccept usID)

    trySourceThem sourceID topic = do
        _ <- MaybeT $ getBy $ UniqueSourceOriginThem sourceID
        case topic of
            Left localID ->
                const () <$>
                    MaybeT (getBy $ UniqueSourceThemAcceptLocal localID)
            Right remoteID ->
                const () <$>
                    MaybeT (getBy $ UniqueSourceThemAcceptRemote remoteID)

verifyNoStartedProjectChildren
    :: ProjectId -> Either (Entity Project) RemoteActorId -> ActDBE ()
verifyNoStartedProjectChildren projectID sourceDB = do

    -- Find existing Source records I have for this source
    sourceIDs <- lift $ getExistingProjectSources projectID sourceDB

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    verifySourcesNotEnabled sourceIDs

    -- Verify none of the Source records are already in
    -- Our-Add+Accept-waiting-for-them or Their-Add+Accept-waiting-for-us state
    verifySourcesNotStarted sourceIDs

verifyNoStartedGroupParents
    :: GroupId -> Either (Entity Group) RemoteActorId -> ActDBE ()
verifyNoStartedGroupParents groupID sourceDB = do

    -- Find existing Source records I have for this source
    sourceIDs <- lift $ getExistingGroupSources groupID sourceDB

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    verifySourcesNotEnabled sourceIDs

    -- Verify none of the Source records are already in
    -- Our-Add+Accept-waiting-for-them or Their-Add+Accept-waiting-for-us state
    verifySourcesNotStarted sourceIDs

verifyNoEnabledProjectChildren
    :: ProjectId -> Either (Entity Project) RemoteActorId -> ActDBE ()
verifyNoEnabledProjectChildren projectID sourceDB = do

    -- Find existing Source records I have for this source
    sourceIDs <- lift $ getExistingProjectSources projectID sourceDB

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    verifySourcesNotEnabled sourceIDs

verifyNoEnabledGroupParents
    :: GroupId -> Either (Entity Group) RemoteActorId -> ActDBE ()
verifyNoEnabledGroupParents groupID sourceDB = do

    -- Find existing Source records I have for this source
    sourceIDs <- lift $ getExistingGroupSources groupID sourceDB

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    verifySourcesNotEnabled sourceIDs

getExistingProjectDests projectID (Left (Entity parentID _)) =
    fmap (map $ bimap E.unValue (Left . E.unValue)) $
    E.select $ E.from $ \ (holder `E.InnerJoin` topic) -> do
        E.on $ holder E.^. DestHolderProjectId E.==. topic E.^. DestTopicProjectHolder
        E.where_ $
            holder E.^. DestHolderProjectProject E.==. E.val projectID E.&&.
            topic E.^. DestTopicProjectParent E.==. E.val parentID
        return
            ( holder E.^. DestHolderProjectDest
            , topic E.^. DestTopicProjectTopic
            )
getExistingProjectDests projectID (Right parentID) =
    fmap (map $ bimap E.unValue (Right . E.unValue)) $
    E.select $ E.from $ \ (holder `E.InnerJoin` topic) -> do
        E.on $ holder E.^. DestHolderProjectDest E.==. topic E.^. DestTopicRemoteDest
        E.where_ $
            holder E.^. DestHolderProjectProject E.==. E.val projectID E.&&.
            topic E.^. DestTopicRemoteTopic E.==. E.val parentID
        return
            ( holder E.^. DestHolderProjectDest
            , topic E.^. DestTopicRemoteId
            )

getExistingGroupDests groupID (Left (Entity childID _)) =
    fmap (map $ bimap E.unValue (Left . E.unValue)) $
    E.select $ E.from $ \ (holder `E.InnerJoin` topic) -> do
        E.on $ holder E.^. DestHolderGroupId E.==. topic E.^. DestTopicGroupHolder
        E.where_ $
            holder E.^. DestHolderGroupGroup E.==. E.val groupID E.&&.
            topic E.^. DestTopicGroupChild E.==. E.val childID
        return
            ( holder E.^. DestHolderGroupDest
            , topic E.^. DestTopicGroupTopic
            )
getExistingGroupDests groupID (Right childID) =
    fmap (map $ bimap E.unValue (Right . E.unValue)) $
    E.select $ E.from $ \ (holder `E.InnerJoin` topic) -> do
        E.on $ holder E.^. DestHolderGroupDest E.==. topic E.^. DestTopicRemoteDest
        E.where_ $
            holder E.^. DestHolderGroupGroup E.==. E.val groupID E.&&.
            topic E.^. DestTopicRemoteTopic E.==. E.val childID
        return
            ( holder E.^. DestHolderGroupDest
            , topic E.^. DestTopicRemoteId
            )

verifyDestsNotEnabled destIDs = do
    byEnabled <-
        lift $ for destIDs $ \ (_, dest) ->
            isJust <$> runMaybeT (tryDestEnabled dest)
    case length $ filter id byEnabled of
        0 -> return ()
        1 -> throwE "I already have a DestThemSendDelegator* for this dest"
        _ -> error "Multiple DestThemSendDelegator* for a dest"
    where
    tryDestEnabled (Left localID) =
        const () <$>
            MaybeT (getBy $ UniqueDestThemSendDelegatorLocalTopic localID)
    tryDestEnabled (Right remoteID) =
        const () <$>
            MaybeT (getBy $ UniqueDestThemSendDelegatorRemoteTopic remoteID)

verifyDestsNotStarted destIDs = do
    anyStarted <-
        lift $ runMaybeT $ asum $
            map (\ (destID, topic) ->
                    tryDestUs destID <|>
                    tryDestThem destID topic
                )
                destIDs
    unless (isNothing anyStarted) $
        throwE "One of the Dest records is already in Add-Accept state"
    where
    tryDestUs destID = do
        _ <- MaybeT $ getBy $ UniqueDestOriginUs destID
        const () <$> MaybeT (getBy $ UniqueDestUsAccept destID)

    tryDestThem destID topic = do
        _ <- MaybeT $ getBy $ UniqueDestOriginThem destID
        case topic of
            Left localID ->
                const () <$>
                    MaybeT (getBy $ UniqueDestThemAcceptLocalTopic localID)
            Right remoteID ->
                const () <$>
                    MaybeT (getBy $ UniqueDestThemAcceptRemoteTopic remoteID)

verifyNoStartedProjectParents
    :: ProjectId -> Either (Entity Project) RemoteActorId -> ActDBE ()
verifyNoStartedProjectParents projectID destDB = do

    -- Find existing Dest records I have for this dest
    destIDs <- lift $ getExistingProjectDests projectID destDB

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    verifyDestsNotEnabled destIDs

    -- Verify none of the Dest records are already in
    -- Our-Add+Accept-waiting-for-them or Their-Add+Accept-waiting-for-us state
    verifyDestsNotStarted destIDs

verifyNoStartedGroupChildren
    :: GroupId -> Either (Entity Group) RemoteActorId -> ActDBE ()
verifyNoStartedGroupChildren groupID destDB = do

    -- Find existing Dest records I have for this dest
    destIDs <- lift $ getExistingGroupDests groupID destDB

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    verifyDestsNotEnabled destIDs

    -- Verify none of the Dest records are already in
    -- Our-Add+Accept-waiting-for-them or Their-Add+Accept-waiting-for-us state
    verifyDestsNotStarted destIDs

verifyNoEnabledProjectParents
    :: ProjectId -> Either (Entity Project) RemoteActorId -> ActDBE ()
verifyNoEnabledProjectParents projectID destDB = do

    -- Find existing Dest records I have for this dest
    destIDs <- lift $ getExistingProjectDests projectID destDB

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    verifyDestsNotEnabled destIDs

verifyNoEnabledGroupChildren
    :: GroupId -> Either (Entity Group) RemoteActorId -> ActDBE ()
verifyNoEnabledGroupChildren groupID destDB = do

    -- Find existing Dest records I have for this dest
    destIDs <- lift $ getExistingGroupDests groupID destDB

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    verifyDestsNotEnabled destIDs

getGrantActivityBody
    :: MonadIO m
    => Either OutboxItemId RemoteActivityId
    -> ReaderT SqlBackend m (AP.Doc AP.Activity URIMode, AP.Grant URIMode)
getGrantActivityBody k = do
    doc@(AP.Doc _ act) <- getActivityBody k
    case AP.activitySpecific act of
        AP.GrantActivity g -> return (doc, g)
        _ -> error "Not a Grant activity"

getPermitsForResource
    :: MonadIO m
    => PersonId
    -> Either ResourceId RemoteActorId
    -> ReaderT SqlBackend m
        [ ( Either (LocalActorBy Key, OutboxItemId) FedURI
          , AP.Role
          , Maybe
                (Either
                    (LocalActorBy Key, Actor)
                    (Instance, RemoteObject, RemoteActor)
                )
          )
        ]
getPermitsForResource personID actor = do
    direct <-
        bitraverse
            (\ resourceID ->
                fmap (resourceID,) $
                E.select $ E.from $ \ (permit `E.InnerJoin` topic `E.InnerJoin` enable) -> do
                    E.on $ topic E.^. PermitTopicLocalId E.==. enable E.^. PermitTopicEnableLocalTopic
                    E.on $ permit E.^. PermitId E.==. topic E.^. PermitTopicLocalPermit
                    E.where_ $
                        permit E.^. PermitPerson E.==. E.val personID E.&&.
                        topic E.^. PermitTopicLocalTopic E.==. E.val resourceID
                    return
                        ( permit E.^. PermitRole
                        , enable E.^. PermitTopicEnableLocalGrant
                        )
            )
            (\ actorID ->
                E.select $ E.from $ \ (permit `E.InnerJoin` topic `E.InnerJoin` enable) -> do
                    E.on $ topic E.^. PermitTopicRemoteId E.==. enable E.^. PermitTopicEnableRemoteTopic
                    E.on $ permit E.^. PermitId E.==. topic E.^. PermitTopicRemotePermit
                    E.where_ $
                        permit E.^. PermitPerson E.==. E.val personID E.&&.
                        topic E.^. PermitTopicRemoteActor E.==. E.val actorID
                    return
                        ( permit E.^. PermitRole
                        , enable E.^. PermitTopicEnableRemoteGrant
                        )
            )
            actor
    directsFinal <-
        case direct of
            Left (resourceID, ds) -> do
                lr <- getLocalResource resourceID
                let la = resourceToActor lr
                return $
                    map
                        (\ (E.Value role, E.Value grantID) ->
                            (Left (la, grantID), role, Nothing)
                        )
                        ds
            Right ds -> for ds $ \ (E.Value role, E.Value grantID) -> do
                grant <- getJust grantID
                u <- getRemoteActivityURI grant
                return (Right u, role, Nothing)
    exts <-
        case actor of
            Left resourceID ->
                E.select $ E.from $ \ (permit `E.InnerJoin` gesture `E.InnerJoin` send `E.InnerJoin` extend `E.InnerJoin` resource) -> do
                    E.on $ extend E.^. PermitTopicExtendId E.==. resource E.^. PermitTopicExtendResourceLocalPermit
                    E.on $ send E.^. PermitPersonSendDelegatorId E.==. extend E.^. PermitTopicExtendPermit
                    E.on $ gesture E.^. PermitPersonGestureId E.==. send E.^. PermitPersonSendDelegatorPermit
                    E.on $ permit E.^. PermitId E.==. gesture E.^. PermitPersonGesturePermit
                    E.where_ $
                        permit E.^. PermitPerson E.==. E.val personID E.&&.
                        resource E.^. PermitTopicExtendResourceLocalResource E.==. E.val resourceID
                    return
                        ( permit E.^. PermitId
                        , extend E.^. PermitTopicExtendId
                        , extend E.^. PermitTopicExtendRole
                        )
            Right actorID ->
                E.select $ E.from $ \ (permit `E.InnerJoin` gesture `E.InnerJoin` send `E.InnerJoin` extend `E.InnerJoin` resource) -> do
                    E.on $ extend E.^. PermitTopicExtendId E.==. resource E.^. PermitTopicExtendResourceRemotePermit
                    E.on $ send E.^. PermitPersonSendDelegatorId E.==. extend E.^. PermitTopicExtendPermit
                    E.on $ gesture E.^. PermitPersonGestureId E.==. send E.^. PermitPersonSendDelegatorPermit
                    E.on $ permit E.^. PermitId E.==. gesture E.^. PermitPersonGesturePermit
                    E.where_ $
                        permit E.^. PermitPerson E.==. E.val personID E.&&.
                        resource E.^. PermitTopicExtendResourceRemoteActor E.==. E.val actorID
                    return
                        ( permit E.^. PermitId
                        , extend E.^. PermitTopicExtendId
                        , extend E.^. PermitTopicExtendRole
                        )
    extsFinal <-
        for exts $ \ (E.Value permitID, E.Value extendID, E.Value role) -> do
            sender <-
                requireEitherAlt
                    (getValBy $ UniquePermitTopicExtendLocal extendID)
                    (getValBy $ UniquePermitTopicExtendRemote extendID)
                    "PermitTopicExtend* neither"
                    "PermitTopicExtend* both"
            (uExt, via) <-
                case sender of
                    Left (PermitTopicExtendLocal _ enableID grantID) -> do
                        PermitTopicEnableLocal _ topicID _ <- getJust enableID
                        byk <- getPermitTopicLocal topicID
                        bye <- do
                            m <- getLocalResourceEntity byk
                            case m of
                                Nothing -> error "I just found this PermitTopicLocal in DB but now the specific actor ID isn't found"
                                Just bye -> pure bye
                        Resource aid <- getJust $ localResourceID bye
                        a <- getJust aid
                        let byk' = resourceToActor byk
                        return (Left (byk', grantID), Left (byk', a))
                    Right (PermitTopicExtendRemote _ enableID grantID) -> do
                        PermitTopicEnableRemote _ topicID _ <- getJust enableID
                        PermitTopicRemote _ remoteActorID <- getJust topicID
                        remoteActor <- getJust remoteActorID
                        remoteObject <- getJust $ remoteActorIdent remoteActor
                        inztance <- getJust $ remoteObjectInstance remoteObject
                        grant <- getJust grantID
                        u <- getRemoteActivityURI grant
                        return (Right u, Right (inztance, remoteObject, remoteActor))
            return (uExt, role, Just via)
    return $ directsFinal ++ extsFinal

getPermitsForResources
    :: MonadIO m
    => PersonId
    -> Maybe [ResourceId]
    -> Maybe [RemoteActorId]
    -> AP.Role
    -> ReaderT SqlBackend m
        [ ( Either ResourceId RemoteActorId
          , Either (LocalActorBy Key, OutboxItemId) FedURI
          , AP.Role
          , Maybe
                (Either
                    (LocalActorBy Key, Actor)
                    (Instance, RemoteObject, RemoteActor)
                )
          )
        ]
getPermitsForResources personID localTopics remoteTopics minRole = do

    directsLocal <-
        E.select $ E.from $ \ (permit `E.InnerJoin` topic `E.InnerJoin` enable) -> do
            E.on $ topic E.^. PermitTopicLocalId E.==. enable E.^. PermitTopicEnableLocalTopic
            E.on $ permit E.^. PermitId E.==. topic E.^. PermitTopicLocalPermit
            E.where_ $
                permit E.^. PermitPerson E.==. E.val personID E.&&.
                permit E.^. PermitRole `E.in_` E.valList [minRole .. maxBound]
            for_ localTopics $ \ ids ->
                E.where_ $ topic E.^. PermitTopicLocalTopic `E.in_` E.valList ids
            return
                ( topic E.^. PermitTopicLocalTopic
                , permit E.^. PermitRole
                , enable E.^. PermitTopicEnableLocalGrant
                )
    directsRemote <-
        E.select $ E.from $ \ (permit `E.InnerJoin` topic `E.InnerJoin` enable) -> do
            E.on $ topic E.^. PermitTopicRemoteId E.==. enable E.^. PermitTopicEnableRemoteTopic
            E.on $ permit E.^. PermitId E.==. topic E.^. PermitTopicRemotePermit
            E.where_ $
                permit E.^. PermitPerson E.==. E.val personID E.&&.
                permit E.^. PermitRole `E.in_` E.valList [minRole .. maxBound]
            for_ remoteTopics $ \ ids ->
                E.where_ $ topic E.^. PermitTopicRemoteActor `E.in_` E.valList ids
            return
                ( topic E.^. PermitTopicRemoteActor
                , permit E.^. PermitRole
                , enable E.^. PermitTopicEnableRemoteGrant
                )

    directsLocalFinal <-
        for directsLocal $ \ (E.Value resourceID, E.Value role, E.Value grantID) -> do
            lr <- getLocalResource resourceID
            let la = resourceToActor lr
            return (Left resourceID, Left (la, grantID), role, Nothing)
    directsRemoteFinal <-
        for directsRemote $ \ (E.Value actorID, E.Value role, E.Value grantID) -> do
            grant <- getJust grantID
            u <- getRemoteActivityURI grant
            return (Right actorID, Right u, role, Nothing)

    extsLocal <-
        fmap (map $ over _1 Left) $
        E.select $ E.from $ \ (permit `E.InnerJoin` gesture `E.InnerJoin` send `E.InnerJoin` extend `E.InnerJoin` resource) -> do
            E.on $ extend E.^. PermitTopicExtendId E.==. resource E.^. PermitTopicExtendResourceLocalPermit
            E.on $ send E.^. PermitPersonSendDelegatorId E.==. extend E.^. PermitTopicExtendPermit
            E.on $ gesture E.^. PermitPersonGestureId E.==. send E.^. PermitPersonSendDelegatorPermit
            E.on $ permit E.^. PermitId E.==. gesture E.^. PermitPersonGesturePermit
            E.where_ $
                permit E.^. PermitPerson E.==. E.val personID E.&&.
                extend E.^. PermitTopicExtendRole `E.in_` E.valList [minRole .. maxBound]
            for_ localTopics $ \ ids ->
                E.where_ $ resource E.^. PermitTopicExtendResourceLocalResource `E.in_` E.valList ids
            return
                ( resource E.^. PermitTopicExtendResourceLocalResource
                , permit E.^. PermitId
                , extend E.^. PermitTopicExtendId
                , extend E.^. PermitTopicExtendRole
                )
    extsRemote <-
        fmap (map $ over _1 Right) $
        E.select $ E.from $ \ (permit `E.InnerJoin` gesture `E.InnerJoin` send `E.InnerJoin` extend `E.InnerJoin` resource) -> do
            E.on $ extend E.^. PermitTopicExtendId E.==. resource E.^. PermitTopicExtendResourceRemotePermit
            E.on $ send E.^. PermitPersonSendDelegatorId E.==. extend E.^. PermitTopicExtendPermit
            E.on $ gesture E.^. PermitPersonGestureId E.==. send E.^. PermitPersonSendDelegatorPermit
            E.on $ permit E.^. PermitId E.==. gesture E.^. PermitPersonGesturePermit
            E.where_ $
                permit E.^. PermitPerson E.==. E.val personID E.&&.
                extend E.^. PermitTopicExtendRole `E.in_` E.valList [minRole .. maxBound]
            for_ remoteTopics $ \ ids ->
                E.where_ $ resource E.^. PermitTopicExtendResourceRemoteActor `E.in_` E.valList ids
            return
                ( resource E.^. PermitTopicExtendResourceRemoteActor
                , permit E.^. PermitId
                , extend E.^. PermitTopicExtendId
                , extend E.^. PermitTopicExtendRole
                )
    extsFinal <-
        for (extsLocal ++ extsRemote) $ \ (resource, E.Value permitID, E.Value extendID, E.Value role) -> do
            sender <-
                requireEitherAlt
                    (getValBy $ UniquePermitTopicExtendLocal extendID)
                    (getValBy $ UniquePermitTopicExtendRemote extendID)
                    "PermitTopicExtend* neither"
                    "PermitTopicExtend* both"
            (uExt, via) <-
                case sender of
                    Left (PermitTopicExtendLocal _ enableID grantID) -> do
                        PermitTopicEnableLocal _ topicID _ <- getJust enableID
                        byk <- getPermitTopicLocal topicID
                        bye <- do
                            m <- getLocalResourceEntity byk
                            case m of
                                Nothing -> error "I just found this PermitTopicLocal in DB but now the specific actor ID isn't found"
                                Just bye -> pure bye
                        rid <- grabLocalResourceID bye
                        Resource aid <- getJust rid
                        a <- getJust aid
                        let byk' = resourceToActor byk
                        return (Left (byk', grantID), Left (byk', a))
                    Right (PermitTopicExtendRemote _ enableID grantID) -> do
                        PermitTopicEnableRemote _ topicID _ <- getJust enableID
                        PermitTopicRemote _ remoteActorID <- getJust topicID
                        remoteActor <- getJust remoteActorID
                        remoteObject <- getJust $ remoteActorIdent remoteActor
                        inztance <- getJust $ remoteObjectInstance remoteObject
                        grant <- getJust grantID
                        u <- getRemoteActivityURI grant
                        return (Right u, Right (inztance, remoteObject, remoteActor))
            return (bimap E.unValue E.unValue resource, uExt, role, Just via)
    return $ directsLocalFinal ++ directsRemoteFinal ++ extsFinal

getCapability
    :: MonadIO m
    => PersonId
    -> Either ResourceId RemoteActorId
    -> AP.Role
    -> ReaderT SqlBackend m
        (Maybe (Either (LocalActorBy Key, OutboxItemId) FedURI))
getCapability personID actor role = do
    maybeDirect <-
        case actor of
            Left resourceID ->
                fmap (fmap (Left . (resourceID,)) . listToMaybe) $
                E.select $ E.from $ \ (permit `E.InnerJoin` topic `E.InnerJoin` enable) -> do
                    E.on $ topic E.^. PermitTopicLocalId E.==. enable E.^. PermitTopicEnableLocalTopic
                    E.on $ permit E.^. PermitId E.==. topic E.^. PermitTopicLocalPermit
                    E.where_ $
                        permit E.^. PermitPerson E.==. E.val personID E.&&.
                        topic E.^. PermitTopicLocalTopic E.==. E.val resourceID E.&&.
                        permit E.^. PermitRole `E.in_` E.valList [role .. maxBound]
                    E.orderBy [E.desc $ enable E.^. PermitTopicEnableLocalId]
                    E.limit 1
                    return $ enable E.^. PermitTopicEnableLocalGrant
            Right actorID ->
                fmap (fmap Right . listToMaybe) $
                E.select $ E.from $ \ (permit `E.InnerJoin` topic `E.InnerJoin` enable) -> do
                    E.on $ topic E.^. PermitTopicRemoteId E.==. enable E.^. PermitTopicEnableRemoteTopic
                    E.on $ permit E.^. PermitId E.==. topic E.^. PermitTopicRemotePermit
                    E.where_ $
                        permit E.^. PermitPerson E.==. E.val personID E.&&.
                        topic E.^. PermitTopicRemoteActor E.==. E.val actorID E.&&.
                        permit E.^. PermitRole E.>=. E.val role
                    E.orderBy [E.desc $ enable E.^. PermitTopicEnableRemoteId]
                    E.limit 1
                    return $ enable E.^. PermitTopicEnableRemoteGrant
    maybeDirect' <- for maybeDirect $ \case
        Left (resourceID, E.Value grantID) -> do
            lr <- getLocalResource resourceID
            let la = resourceToActor lr
            return $ Left (la, grantID)
        Right (E.Value grantID) -> do
            grant <- getJust grantID
            u <- getRemoteActivityURI grant
            return $ Right u
    maybeExt <-
        listToMaybe <$>
        case actor of
            Left resourceID ->
                E.select $ E.from $ \ (permit `E.InnerJoin` gesture `E.InnerJoin` send `E.InnerJoin` extend `E.InnerJoin` resource) -> do
                    E.on $ extend E.^. PermitTopicExtendId E.==. resource E.^. PermitTopicExtendResourceLocalPermit
                    E.on $ send E.^. PermitPersonSendDelegatorId E.==. extend E.^. PermitTopicExtendPermit
                    E.on $ gesture E.^. PermitPersonGestureId E.==. send E.^. PermitPersonSendDelegatorPermit
                    E.on $ permit E.^. PermitId E.==. gesture E.^. PermitPersonGesturePermit
                    E.where_ $
                        permit E.^. PermitPerson E.==. E.val personID E.&&.
                        resource E.^. PermitTopicExtendResourceLocalResource E.==. E.val resourceID E.&&.
                        extend E.^. PermitTopicExtendRole E.>=. E.val role
                    E.orderBy [E.desc $ extend E.^. PermitTopicExtendId]
                    E.limit 1
                    return $ extend E.^. PermitTopicExtendId
            Right actorID ->
                E.select $ E.from $ \ (permit `E.InnerJoin` gesture `E.InnerJoin` send `E.InnerJoin` extend `E.InnerJoin` resource) -> do
                    E.on $ extend E.^. PermitTopicExtendId E.==. resource E.^. PermitTopicExtendResourceRemotePermit
                    E.on $ send E.^. PermitPersonSendDelegatorId E.==. extend E.^. PermitTopicExtendPermit
                    E.on $ gesture E.^. PermitPersonGestureId E.==. send E.^. PermitPersonSendDelegatorPermit
                    E.on $ permit E.^. PermitId E.==. gesture E.^. PermitPersonGesturePermit
                    E.where_ $
                        permit E.^. PermitPerson E.==. E.val personID E.&&.
                        resource E.^. PermitTopicExtendResourceRemoteActor E.==. E.val actorID E.&&.
                        extend E.^. PermitTopicExtendRole E.>=. E.val role
                    E.orderBy [E.desc $ extend E.^. PermitTopicExtendId]
                    E.limit 1
                    return $ extend E.^. PermitTopicExtendId
    maybeExt' <- for maybeExt $ \ (E.Value extendID) -> do
        sender <-
            requireEitherAlt
                (getValBy $ UniquePermitTopicExtendLocal extendID)
                (getValBy $ UniquePermitTopicExtendRemote extendID)
                "PermitTopicExtend* neither"
                "PermitTopicExtend* both"
        case sender of
            Left (PermitTopicExtendLocal _ enableID grantID) -> do
                PermitTopicEnableLocal _ topicID _ <- getJust enableID
                PermitTopicLocal _ resourceID <- getJust topicID
                byk <- getLocalResource resourceID
                let byk' = resourceToActor byk
                return $ Left (byk', grantID)
            Right (PermitTopicExtendRemote _ _enableID grantID) -> do
                grant <- getJust grantID
                u <- getRemoteActivityURI grant
                return $ Right u
    return $ maybeDirect' <|> maybeExt'

getStems
    :: MonadIO m
    => KomponentId
    -> ReaderT SqlBackend m
        [ ( Either (ProjectId, Actor) (Instance, RemoteObject, RemoteActor)
          , AP.Role
          , UTCTime
          , StemId
          )
        ]
getStems komponentID = do
    stems <-
        E.select $ E.from $ \ (stem `E.InnerJoin` accept `E.InnerJoin` deleg `E.InnerJoin` grant) -> do
            E.on $ deleg E.^. StemDelegateLocalGrant E.==. grant E.^. OutboxItemId
            E.on $ accept E.^. StemComponentAcceptId E.==. deleg E.^. StemDelegateLocalStem
            E.on $ stem E.^. StemId E.==. accept E.^. StemComponentAcceptStem
            E.where_ $ stem E.^. StemHolder E.==. E.val komponentID
            return
                ( stem
                , grant E.^. OutboxItemPublished
                )
    for stems $ \ (Entity stemID stem, E.Value time) -> do
        j <- getStemProject stemID
        projectView <-
            bitraverse
                (\ projectID -> do
                    actorID <- projectActor <$> getJust projectID
                    actor <- getJust actorID
                    return (projectID, actor)
                )
                getRemoteActorData
                j
        return (projectView, stemRole stem, time, stemID)

getStemDrafts
    :: MonadIO m
    => KomponentId
    -> ReaderT SqlBackend m
        [ ( Either (LocalActorBy Key, Actor) (Instance, RemoteObject, RemoteActor)
          , Bool
          , Either (ProjectId, Actor) (Instance, RemoteObject, RemoteActor)
          , Bool
          , UTCTime
          , AP.Role
          , StemId
          )
        ]
getStemDrafts komponentID = do
    drafts <-
        E.select $ E.from $ \ (stem `E.LeftOuterJoin` accept `E.LeftOuterJoin` deleg) -> do
            E.on $ accept E.?. StemComponentAcceptId E.==. deleg E.?. StemDelegateLocalStem
            E.on $ E.just (stem E.^. StemId) E.==. accept E.?. StemComponentAcceptStem
            E.where_ $
                stem E.^. StemHolder E.==. E.val komponentID E.&&.
                E.isNothing (deleg E.?. StemDelegateLocalId)
            return stem
    for drafts $ \ (Entity stemID (Stem role _)) -> do
        (project, accept) <- do
            project <- getStemProject stemID
            accept <- isJust <$> getBy (UniqueStemComponentAccept stemID)
            (,accept) <$> bitraverse
                (\ j -> do
                    resourceID <- projectResource <$> getJust j
                    Resource actorID <- getJust resourceID
                    actor <- getJust actorID
                    return (j, actor)
                )
                getRemoteActorData
                project
        ((inviter, time), us) <- do
            usOrThem <-
                requireEitherAlt
                    (getKeyBy $ UniqueStemOriginAdd stemID)
                    (getKeyBy $ UniqueStemOriginInvite stemID)
                    "Neither us nor them"
                    "Both us and them"
            (addOrActor, us) <-
                case usOrThem of
                    Left _usID -> (,True) <$>
                        requireEitherAlt
                            (fmap stemComponentGestureLocalActivity <$> getValBy (UniqueStemComponentGestureLocal stemID))
                            (fmap (stemComponentGestureRemoteActor &&& stemComponentGestureRemoteActivity) <$> getValBy (UniqueStemComponentGestureRemote stemID))
                            "Neither local not remote"
                            "Both local and remote"
                    Right themID -> (,False) <$>
                        requireEitherAlt
                            (fmap stemProjectGestureLocalInvite <$> getValBy (UniqueStemProjectGestureLocal themID))
                            (fmap (stemProjectGestureRemoteActor &&& stemProjectGestureRemoteInvite) <$> getValBy (UniqueStemProjectGestureRemote themID))
                            "Neither local not remote"
                            "Both local and remote"
            (,us) <$> case addOrActor of
                Left addID -> do
                    OutboxItem outboxID _ time <- getJust addID
                    Entity actorID actor <- getByJust $ UniqueActorOutbox outboxID
                    (,time) . Left . (,actor) <$> getLocalActor actorID
                Right (actorID, addID) -> do
                    RemoteActivity _ _ time <- getJust addID
                    (,time) . Right <$> getRemoteActorData actorID
        return (inviter, us, project, accept, time, role, stemID)

getResourceTeams
    :: MonadIO m
    => ResourceId
    -> ReaderT SqlBackend m
        [ ( AP.Role
          , UTCTime
          , Either (GroupId, Actor) (Instance, RemoteObject, RemoteActor)
          , SquadId
          )
        ]
getResourceTeams resourceID =
    fmap (sortOn $ view _2) $ liftA2 (++)
        (map (\ (E.Value role, E.Value time, E.Value groupID, Entity _ actor, E.Value squadID) ->
                (role, time, Left (groupID, actor), squadID)
             )
            <$> getLocals
        )
        (map (\ (E.Value role, E.Value time, Entity _ i, Entity _ ro, Entity _ ra, E.Value squadID) ->
                (role, time, Right (i, ro, ra), squadID)
             )
            <$> getRemotes
        )
    where
    getLocals =
        E.select $ E.from $ \ (squad `E.InnerJoin` topic `E.InnerJoin` group `E.InnerJoin` actor `E.InnerJoin` accept `E.InnerJoin` deleg `E.InnerJoin` grant) -> do
            E.on $ deleg E.^. SquadThemSendDelegatorLocalGrant E.==. grant E.^. OutboxItemId
            E.on $ accept E.^. SquadUsAcceptId E.==. deleg E.^. SquadThemSendDelegatorLocalSquad
            E.on $ squad E.^. SquadId E.==. accept E.^. SquadUsAcceptSquad
            E.on $ group E.^. GroupActor E.==. actor E.^. ActorId
            E.on $ topic E.^. SquadTopicLocalGroup E.==. group E.^. GroupId
            E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicLocalSquad
            E.where_ $ squad E.^. SquadHolder E.==. E.val resourceID
            E.orderBy [E.asc $ grant E.^. OutboxItemPublished]
            return
                ( squad E.^. SquadRole
                , grant E.^. OutboxItemPublished
                , topic E.^. SquadTopicLocalGroup
                , actor
                , squad E.^. SquadId
                )
    getRemotes =
        E.select $ E.from $ \ (squad `E.InnerJoin` topic `E.InnerJoin` accept `E.InnerJoin` deleg `E.InnerJoin` grant `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ topic E.^. SquadTopicRemoteTopic E.==. ra E.^. RemoteActorId
            E.on $ deleg E.^. SquadThemSendDelegatorRemoteGrant E.==. grant E.^. RemoteActivityId
            E.on $ accept E.^. SquadUsAcceptId E.==. deleg E.^. SquadThemSendDelegatorRemoteSquad
            E.on $ squad E.^. SquadId E.==. accept E.^. SquadUsAcceptSquad
            E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicRemoteSquad
            E.where_ $ squad E.^. SquadHolder E.==. E.val resourceID
            E.orderBy [E.asc $ grant E.^. RemoteActivityReceived]
            return
                ( squad E.^. SquadRole
                , grant E.^. RemoteActivityReceived
                , i
                , ro
                , ra
                , squad E.^. SquadId
                )

getResourceTeamDrafts
    :: MonadIO m
    => ResourceId
    -> ReaderT SqlBackend m
        [ ( Either (LocalActorBy Key, Actor) (Instance, RemoteObject, RemoteActor)
          , Bool
          , Either (GroupId, Actor) (Instance, RemoteObject, RemoteActor)
          , Bool
          , UTCTime
          , AP.Role
          , SquadId
          )
        ]
getResourceTeamDrafts resourceID = do
    squads <- E.select $ E.from $ \ (squad `E.LeftOuterJoin` accept `E.LeftOuterJoin` delegl `E.LeftOuterJoin` delegr) -> do
        E.on $ accept E.?. SquadUsAcceptId E.==. delegr E.?. SquadThemSendDelegatorRemoteSquad
        E.on $ accept E.?. SquadUsAcceptId E.==. delegl E.?. SquadThemSendDelegatorLocalSquad
        E.on $ E.just (squad E.^. SquadId) E.==. accept E.?. SquadUsAcceptSquad
        E.where_ $
            squad E.^. SquadHolder E.==. E.val resourceID E.&&.
            E.isNothing (delegl E.?. SquadThemSendDelegatorLocalId) E.&&.
            E.isNothing (delegr E.?. SquadThemSendDelegatorRemoteId)
        E.orderBy [E.asc $ squad E.^. SquadId]
        return squad
    for squads $ \ (Entity squadID (Squad role _)) -> do
        team <- do
            topic <- getSquadTeam squadID
            bitraverse
                (\ (_, gID) -> do
                    g <- getJust gID
                    actor <- getJust $ groupActor g
                    return (gID, actor)
                )
                (\ (_, actorID) -> getRemoteActorData actorID)
                topic
        accept <- isJust <$> getBy (UniqueSquadUsAccept squadID)
        ((inviter, time), us) <- do
            usOrThem <-
                requireEitherAlt
                    (getKeyBy $ UniqueSquadOriginUs squadID)
                    (getKeyBy $ UniqueSquadOriginThem squadID)
                    "Neither us nor them"
                    "Both us and them"
            (addOrActor, us) <-
                case usOrThem of
                    Left _usID -> (,True) <$>
                        requireEitherAlt
                            (fmap squadUsGestureLocalActivity <$> getValBy (UniqueSquadUsGestureLocal squadID))
                            (fmap (squadUsGestureRemoteActor &&& squadUsGestureRemoteActivity) <$> getValBy (UniqueSquadUsGestureRemote squadID))
                            "Neither local not remote"
                            "Both local and remote"
                    Right themID -> (,False) <$>
                        requireEitherAlt
                            (fmap squadThemGestureLocalAdd <$> getValBy (UniqueSquadThemGestureLocal themID))
                            (fmap (squadThemGestureRemoteActor &&& squadThemGestureRemoteAdd) <$> getValBy (UniqueSquadThemGestureRemote themID))
                            "Neither local not remote"
                            "Both local and remote"
            (,us) <$> case addOrActor of
                Left addID -> do
                    OutboxItem outboxID _ time <- getJust addID
                    Entity actorID actor <- getByJust $ UniqueActorOutbox outboxID
                    (,time) . Left . (,actor) <$> getLocalActor actorID
                Right (actorID, addID) -> do
                    RemoteActivity _ _ time <- getJust addID
                    (,time) . Right <$> getRemoteActorData actorID
        return (inviter, us, team, accept, time, role, squadID)

getSquadAdd
    :: MonadIO m
    => SquadId
    -> ReaderT SqlBackend m
        (Either
            (LocalActorBy Key, OutboxItemId)
            FedURI
        )
getSquadAdd squadID = do
    usOrThem <-
        requireEitherAlt
            (getKeyBy $ UniqueSquadOriginUs squadID)
            (getKeyBy $ UniqueSquadOriginThem squadID)
            "Neither us nor them"
            "Both us and them"
    add <-
        case usOrThem of
            Left _usID ->
                requireEitherAlt
                    (fmap squadUsGestureLocalActivity <$> getValBy (UniqueSquadUsGestureLocal squadID))
                    (fmap squadUsGestureRemoteActivity <$> getValBy (UniqueSquadUsGestureRemote squadID))
                    "Neither local not remote"
                    "Both local and remote"
            Right themID ->
                requireEitherAlt
                    (fmap squadThemGestureLocalAdd <$> getValBy (UniqueSquadThemGestureLocal themID))
                    (fmap squadThemGestureRemoteAdd <$> getValBy (UniqueSquadThemGestureRemote themID))
                    "Neither local not remote"
                    "Both local and remote"
    getActivityIdent add

getSquadTeam
    :: MonadIO m
    => SquadId
    -> ReaderT SqlBackend m
        (Either
            (SquadTopicLocalId, GroupId)
            (SquadTopicRemoteId, RemoteActorId)
        )
getSquadTeam squadID =
    bimap
        (\ (Entity k v) -> (k, squadTopicLocalGroup v))
        (\ (Entity k v) -> (k, squadTopicRemoteTopic v))
        <$>
        requireEitherAlt
            (getBy $ UniqueSquadTopicLocal squadID)
            (getBy $ UniqueSquadTopicRemote squadID)
            "Found Squad without topic"
            "Found Squad with both local and remote topic"

getTeamResources
    :: MonadIO m
    => GroupId
    -> ReaderT SqlBackend m
        [ ( AP.Role
          , UTCTime
          , Either (LocalActorBy Key, Actor) (Instance, RemoteObject, RemoteActor)
          , EffortId
          )
        ]
getTeamResources groupID =
    fmap (sortOn $ view _2) $ liftA2 (++)
        (map (\ (E.Value role, E.Value time, resource, Entity _ actor, E.Value effortID) ->
                (role, time, Left (resource, actor), effortID)
             )
            <$> (do ls <- getLocals
                    for ls $ \ (role, time, E.Value r, E.Value d, E.Value l, E.Value j, actor, effort) -> do
                        resource <-
                            exactlyOneJust
                                [ LocalActorRepo <$> r
                                , LocalActorDeck <$> d
                                , LocalActorLoom <$> l
                                , LocalActorProject <$> j
                                ]
                                "EffortTopicLocal: Specific actor not found"
                                "EffortTopicLocal: Multiple specific actors not found"
                        return (role, time, resource, actor, effort)
                )
        )
        (map (\ (E.Value role, E.Value time, Entity _ i, Entity _ ro, Entity _ ra, E.Value effortID) ->
                (role, time, Right (i, ro, ra), effortID)
             )
            <$> getRemotes
        )
    where
    getLocals =
        E.select $ E.from $ \ (effort `E.InnerJoin` topic `E.InnerJoin` resource `E.InnerJoin` actor `E.InnerJoin` deleg `E.InnerJoin` grant `E.LeftOuterJoin` repo `E.LeftOuterJoin` deck `E.LeftOuterJoin` loom `E.LeftOuterJoin` project) -> do
            E.on $ E.just (resource E.^. ResourceId) E.==. project E.?. ProjectResource
            E.on $ E.just (resource E.^. ResourceId) E.==. loom E.?. LoomResource
            E.on $ E.just (resource E.^. ResourceId) E.==. deck E.?. DeckResource
            E.on $ E.just (resource E.^. ResourceId) E.==. repo E.?. RepoResource
            E.on $ deleg E.^. EffortUsSendDelegatorGrant E.==. grant E.^. OutboxItemId
            E.on $ effort E.^. EffortId E.==. deleg E.^. EffortUsSendDelegatorEffort
            E.on $ resource E.^. ResourceActor E.==. actor E.^. ActorId
            E.on $ topic E.^. EffortTopicLocalTopic E.==. resource E.^. ResourceId
            E.on $ effort E.^. EffortId E.==. topic E.^. EffortTopicLocalEffort
            E.where_ $ effort E.^. EffortHolder E.==. E.val groupID
            E.orderBy [E.asc $ deleg E.^. EffortUsSendDelegatorId]
            return
                ( effort E.^. EffortRole
                , grant E.^. OutboxItemPublished
                , repo E.?. RepoId
                , deck E.?. DeckId
                , loom E.?. LoomId
                , project E.?. ProjectId
                , actor
                , effort E.^. EffortId
                )
    getRemotes =
        E.select $ E.from $ \ (effort `E.InnerJoin` topic `E.InnerJoin` deleg `E.InnerJoin` grant `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i) -> do
            E.on $ ro E.^. RemoteObjectInstance E.==. i E.^. InstanceId
            E.on $ ra E.^. RemoteActorIdent E.==. ro E.^. RemoteObjectId
            E.on $ topic E.^. EffortTopicRemoteTopic E.==. ra E.^. RemoteActorId
            E.on $ deleg E.^. EffortUsSendDelegatorGrant E.==. grant E.^. OutboxItemId
            E.on $ effort E.^. EffortId E.==. deleg E.^. EffortUsSendDelegatorEffort
            E.on $ effort E.^. EffortId E.==. topic E.^. EffortTopicRemoteEffort
            E.where_ $ effort E.^. EffortHolder E.==. E.val groupID
            E.orderBy [E.asc $ deleg E.^. EffortUsSendDelegatorId]
            return
                ( effort E.^. EffortRole
                , grant E.^. OutboxItemPublished
                , i
                , ro
                , ra
                , effort E.^. EffortId
                )

getTeamResourceDrafts
    :: MonadIO m
    => GroupId
    -> ReaderT SqlBackend m
        [ ( Either (LocalActorBy Key, Actor) (Instance, RemoteObject, RemoteActor)
          , Bool
          , Either (LocalActorBy Key, Actor) (Instance, RemoteObject, RemoteActor)
          , Bool
          , UTCTime
          , AP.Role
          , EffortId
          )
        ]
getTeamResourceDrafts groupID = do
    efforts <- E.select $ E.from $ \ (effort `E.LeftOuterJoin` deleg) -> do
        E.on $ E.just (effort E.^. EffortId) E.==. deleg E.?. EffortUsSendDelegatorEffort
        E.where_ $
            effort E.^. EffortHolder E.==. E.val groupID E.&&.
            E.isNothing (deleg E.?. EffortUsSendDelegatorId)
        E.orderBy [E.asc $ effort E.^. EffortId]
        return effort
    for efforts $ \ (Entity effortID (Effort role _)) -> do
        (resource, accept) <- do
            topic <- getEffortTopic effortID
            accept <-
                case bimap fst fst topic of
                    Left localID -> isJust <$> getBy (UniqueEffortThemAcceptLocal localID)
                    Right remoteID -> isJust <$> getBy (UniqueEffortThemAcceptRemote remoteID)
            (,accept) <$> bitraverse
                (\ (_, resourceID) -> do
                    lr <- getLocalResource resourceID
                    Resource actorID <- getJust resourceID
                    actor <- getJust actorID
                    return (resourceToActor lr, actor)
                )
                (\ (_, actorID) -> getRemoteActorData actorID)
                topic
        ((inviter, time), us) <- do
            usOrThem <-
                requireEitherAlt
                    (getKeyBy $ UniqueEffortOriginUs effortID)
                    (getKeyBy $ UniqueEffortOriginThem effortID)
                    "Neither us nor them"
                    "Both us and them"
            (addOrActor, us) <-
                case usOrThem of
                    Left usID -> (,True) <$>
                        requireEitherAlt
                            (fmap effortUsGestureLocalAdd <$> getValBy (UniqueEffortUsGestureLocal usID))
                            (fmap (effortUsGestureRemoteActor &&& effortUsGestureRemoteAdd) <$> getValBy (UniqueEffortUsGestureRemote usID))
                            "Neither local not remote"
                            "Both local and remote"
                    Right themID -> (,False) <$>
                        requireEitherAlt
                            (fmap effortThemGestureLocalAdd <$> getValBy (UniqueEffortThemGestureLocal themID))
                            (fmap (effortThemGestureRemoteActor &&& effortThemGestureRemoteAdd) <$> getValBy (UniqueEffortThemGestureRemote themID))
                            "Neither local not remote"
                            "Both local and remote"
            (,us) <$> case addOrActor of
                Left addID -> do
                    OutboxItem outboxID _ time <- getJust addID
                    Entity actorID actor <- getByJust $ UniqueActorOutbox outboxID
                    (,time) . Left . (,actor) <$> getLocalActor actorID
                Right (actorID, addID) -> do
                    RemoteActivity _ _ time <- getJust addID
                    (,time) . Right <$> getRemoteActorData actorID
        return (inviter, us, resource, accept, time, role, effortID)

getEffortAdd
    :: MonadIO m
    => EffortId
    -> ReaderT SqlBackend m
        (Either
            (LocalActorBy Key, OutboxItemId)
            FedURI
        )
getEffortAdd effortID = do
    usOrThem <-
        requireEitherAlt
            (getKeyBy $ UniqueEffortOriginUs effortID)
            (getKeyBy $ UniqueEffortOriginThem effortID)
            "Neither us nor them"
            "Both us and them"
    add <-
        case usOrThem of
            Left usID ->
                requireEitherAlt
                    (fmap effortUsGestureLocalAdd <$> getValBy (UniqueEffortUsGestureLocal usID))
                    (fmap effortUsGestureRemoteAdd <$> getValBy (UniqueEffortUsGestureRemote usID))
                    "Neither local not remote"
                    "Both local and remote"
            Right themID ->
                requireEitherAlt
                    (fmap effortThemGestureLocalAdd <$> getValBy (UniqueEffortThemGestureLocal themID))
                    (fmap effortThemGestureRemoteAdd <$> getValBy (UniqueEffortThemGestureRemote themID))
                    "Neither local not remote"
                    "Both local and remote"
    getActivityIdent add

getEffortTopic
    :: MonadIO m
    => EffortId
    -> ReaderT SqlBackend m
        (Either
            (EffortTopicLocalId, ResourceId)
            (EffortTopicRemoteId, RemoteActorId)
        )
getEffortTopic effortID =
    bimap
        (\ (Entity k v) -> (k, effortTopicLocalTopic v))
        (\ (Entity k v) -> (k, effortTopicRemoteTopic v))
        <$>
        requireEitherAlt
            (getBy $ UniqueEffortTopicLocal effortID)
            (getBy $ UniqueEffortTopicRemote effortID)
            "Found Effort without topic"
            "Found Effort with both local and remote topic"

getExistingGroupEfforts groupID (Left resourceID) =
    fmap (map $ bimap E.unValue (Left . E.unValue)) $
    E.select $ E.from $ \ (effort `E.InnerJoin` topic) -> do
        E.on $ effort E.^. EffortId E.==. topic E.^. EffortTopicLocalEffort
        E.where_ $
            effort E.^. EffortHolder E.==. E.val groupID E.&&.
            topic E.^. EffortTopicLocalTopic E.==. E.val resourceID
        return
            ( effort E.^. EffortId
            , topic E.^. EffortTopicLocalId
            )
getExistingGroupEfforts groupID (Right actorID) =
    fmap (map $ bimap E.unValue (Right . E.unValue)) $
    E.select $ E.from $ \ (effort `E.InnerJoin` topic) -> do
        E.on $ effort E.^. EffortId E.==. topic E.^. EffortTopicRemoteEffort
        E.where_ $
            effort E.^. EffortHolder E.==. E.val groupID E.&&.
            topic E.^. EffortTopicRemoteTopic E.==. E.val actorID
        return
            ( effort E.^. EffortId
            , topic E.^. EffortTopicRemoteId
            )

verifyEffortsNotEnabled effortIDs = do
    byEnabled <-
        lift $ for effortIDs $ \ (effortID, _) ->
            isJust <$> runMaybeT (tryEffortEnabled effortID)
    case length $ filter id byEnabled of
        0 -> return ()
        1 -> throwE "I already have a EffortUsSendDelegator for this effort"
        _ -> error "Multiple EffortUsSendDelegator for a effort"
    where
    tryEffortEnabled effortID =
        const () <$> MaybeT (getBy $ UniqueEffortUsSendDelegator effortID)

verifyEffortsNotStarted effortIDs = do
    anyStarted <-
        lift $ runMaybeT $ asum $
            map (\ (effortID, topic) ->
                    tryEffortUs effortID <|>
                    tryEffortThem effortID topic
                )
                effortIDs
    unless (isNothing anyStarted) $
        throwE "One of the Effort records is already in Add-Accept state"
    where
    tryEffortUs effortID = do
        usID <- MaybeT $ getKeyBy $ UniqueEffortOriginUs effortID
        const () <$> MaybeT (getBy $ UniqueEffortUsAccept usID)

    tryEffortThem effortID topic = do
        _ <- MaybeT $ getBy $ UniqueEffortOriginThem effortID
        case topic of
            Left localID ->
                const () <$>
                    MaybeT (getBy $ UniqueEffortThemAcceptLocal localID)
            Right remoteID ->
                const () <$>
                    MaybeT (getBy $ UniqueEffortThemAcceptRemote remoteID)

verifyNoStartedGroupResources
    :: GroupId -> Either ResourceId RemoteActorId -> ActDBE ()
verifyNoStartedGroupResources groupID resource = do

    -- Find existing Effort records I have for this resource
    effortIDs <- lift $ getExistingGroupEfforts groupID resource

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    verifyEffortsNotEnabled effortIDs

    -- Verify none of the Effort records are already in
    -- Our-Add+Accept-waiting-for-them or Their-Add+Accept-waiting-for-us state
    verifyEffortsNotStarted effortIDs

getExistingResourceSquads resourceID (Left (Entity teamID _)) =
    fmap (map $ bimap E.unValue (Left . E.unValue)) $
    E.select $ E.from $ \ (squad `E.InnerJoin` topic) -> do
        E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicLocalSquad
        E.where_ $
            squad E.^. SquadHolder E.==. E.val resourceID E.&&.
            topic E.^. SquadTopicLocalGroup E.==. E.val teamID
        return
            ( squad E.^. SquadId
            , topic E.^. SquadTopicLocalId
            )
getExistingResourceSquads resourceID (Right teamID) =
    fmap (map $ bimap E.unValue (Right . E.unValue)) $
    E.select $ E.from $ \ (squad `E.InnerJoin` topic) -> do
        E.on $ squad E.^. SquadId E.==. topic E.^. SquadTopicRemoteSquad
        E.where_ $
            squad E.^. SquadHolder E.==. E.val resourceID E.&&.
            topic E.^. SquadTopicRemoteTopic E.==. E.val teamID
        return
            ( squad E.^. SquadId
            , topic E.^. SquadTopicRemoteId
            )

verifySquadsNotEnabled squadIDs = do
    byEnabled <-
        lift $ for squadIDs $ \ (_, squad) ->
            isJust <$> runMaybeT (trySquadEnabled squad)
    case length $ filter id byEnabled of
        0 -> return ()
        1 -> throwE "I already have a SquadThemSendDelegator* for this squad"
        _ -> error "Multiple SquadThemSendDelegator* for a squad"
    where
    trySquadEnabled (Left localID) =
        const () <$>
            MaybeT (getBy $ UniqueSquadThemSendDelegatorLocalTopic localID)
    trySquadEnabled (Right remoteID) =
        const () <$>
            MaybeT (getBy $ UniqueSquadThemSendDelegatorRemoteTopic remoteID)

verifySquadsNotStarted squadIDs = do
    anyStarted <-
        lift $ runMaybeT $ asum $
            map (\ (squadID, topic) ->
                    trySquadUs squadID <|>
                    trySquadThem squadID topic
                )
                squadIDs
    unless (isNothing anyStarted) $
        throwE "One of the Squad records is already in Add-Accept state"
    where
    trySquadUs squadID = do
        _ <- MaybeT $ getBy $ UniqueSquadOriginUs squadID
        const () <$> MaybeT (getBy $ UniqueSquadUsAccept squadID)

    trySquadThem squadID topic = do
        _ <- MaybeT $ getBy $ UniqueSquadOriginThem squadID
        case topic of
            Left localID ->
                const () <$>
                    MaybeT (getBy $ UniqueSquadThemAcceptLocalTopic localID)
            Right remoteID ->
                const () <$>
                    MaybeT (getBy $ UniqueSquadThemAcceptRemoteTopic remoteID)

verifyNoStartedResourceTeams
    :: ResourceId -> Either (Entity Group) RemoteActorId -> ActDBE ()
verifyNoStartedResourceTeams resourceID squadDB = do

    -- Find existing Squad records I have for this squad
    squadIDs <- lift $ getExistingResourceSquads resourceID squadDB

    -- Grab all the enabled ones, make sure none are enabled, and even if
    -- any are enabled, make sure there's at most one (otherwise it's a
    -- bug)
    verifySquadsNotEnabled squadIDs

    -- Verify none of the Squad records are already in
    -- Our-Add+Accept-waiting-for-them or Their-Add+Accept-waiting-for-us state
    verifySquadsNotStarted squadIDs
