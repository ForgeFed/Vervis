{- This file is part of Vervis.
 -
 - Written in 2016, 2018, 2019, 2020, 2021, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Persist.Ticket
    ( TicketSummary (..)
    , ClothSummary (..)
    , getTicketResolve
    , getWorkItem
    , checkApplyDB
    , tryUnresolve
    , getTicketOrigin
    , getTicketSummaries
    , getClothSummaries
    , getTicket
    , getTicket404
    )
where

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Bitraversable
import Data.Foldable
import Data.List.NonEmpty (NonEmpty (..))
import Data.Maybe
import Data.Text (Text)
import Data.These.Combinators
import Data.Time.Clock
import Data.Traversable
import Database.Persist
import Database.Persist.Sql
import Yesod.Core

import qualified Data.List.NonEmpty as NE
import qualified Database.Esqueleto as E

import Development.PatchMediaType
import Yesod.Hashids

import qualified Web.ActivityPub as AP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Database.Persist.Local

import Vervis.Cloth
import Vervis.Data.Collab
import Vervis.Data.Ticket
import Vervis.FedURI
import Vervis.Foundation
import Vervis.Model
import Vervis.Persist.Actor
import Vervis.Persist.Collab
import Vervis.Recipient

data TicketSummary = TicketSummary
    { tsId        :: TicketDeckId
    , tsCreatedBy :: Either
                        (LocalActorBy Key, Actor)
                        (Instance, RemoteObject, RemoteActor)
    , tsCreatedAt :: UTCTime
    , tsTitle     :: Text
    , tsLabels    :: [WorkflowField]
    , tsClosed    :: Bool
    , tsComments  :: Int
    }

data ClothSummary = ClothSummary
    { csId        :: TicketLoomId
    , csCreatedBy :: Either
                        (LocalActorBy Key, Actor)
                        (Instance, RemoteObject, RemoteActor)
    , csCreatedAt :: UTCTime
    , csTitle     :: Text
    , csLabels    :: [WorkflowField]
    , csClosed    :: Bool
    , csComments  :: Int
    }

getTicketResolve (Entity _ tr, resolve) = do
    time <- outboxItemPublished <$> getJust (ticketResolveAccept tr)
    closer <- bitraverse getCloserLocal getCloserRemote resolve
    return (time, closer)
    where
    getCloserLocal (Entity _ trl) = do
        outboxID <-
            outboxItemOutbox <$>
                getJust (ticketResolveLocalActivity trl)
        Entity actorID actor <- do
            maybeActor <- getBy $ UniqueActorOutbox outboxID
            case maybeActor of
                Nothing -> error "No actor for outbox"
                Just a -> pure a
        actorByEntity <- getLocalActorEnt actorID
        person <-
            case actorByEntity of
                LocalActorPerson p -> pure p
                _ -> error "Surprise! Ticket closer isn't a Person"
        return (person, actor)
    getCloserRemote (Entity _ trr) = do
        ra <- getJust $ ticketResolveRemoteActor trr
        ro <- getJust $ remoteActorIdent ra
        i <- getJust $ remoteObjectInstance ro
        return (i, ro, ra)

getWorkItem :: MonadIO m => TicketId -> ReaderT SqlBackend m (WorkItemBy Key)
getWorkItem tid = do
    tracker <-
        requireEitherAlt
            (getBy $ UniqueTicketDeck tid)
            (getBy $ UniqueTicketLoom tid)
            "Neither TD nor TD found"
            "Both TD and TL found"
    return $
        case tracker of
            Left (Entity tdid td) -> WorkItemTicket (ticketDeckDeck td) tdid
            Right (Entity tlid tl) -> WorkItemCloth (ticketLoomLoom tl) tlid

-- | Given:
--
-- * A local tip (i.e. a repository or a branch), parsed from a URI
-- * A local bundle to apply to it, parsed from a URI
-- * A local or remote actor requesting to apply the bundle to the tip, already
--   known to be in our DB
-- * An activity URI provided by that actor as a capability, parsed from URI
--
-- Find the tip and the bundle in our DB, and verify that the loom hosting the
-- bundle is willing to accept the request from that specific actor to apply
-- that bundle to that repo. More specifically:
--
-- * Verify the tip matches the MR target
-- * Verify that the loom and the repo are linked
-- * Verify that a branch is specified if repo is Git, isn't specified if Darcs
-- * Verify the MR isn't already resolved
-- * Verify bundle is the latest version of the MR
-- * Verify the requester actor is authorized to apply
-- * Verify that patch type matches repo VCS type
--
-- Returns:
--
-- * The loom (so it can send an Accept after applying)
-- * The MR's ticket ID (so it can be marked as resolved after applying)
-- * The actual patch diffs, in first-to-last order
checkApplyDB
    :: Either PersonId RemoteActorId    -- ^ Actor requesting to apply
    -> (Either
            (LocalActorBy Key, LocalActorBy KeyHashid, OutboxItemId)
            FedURI
       )                                -- ^ Capability specified by the actor
    -> (RepoId, Maybe Text)             -- ^ Repository (or branch) to apply to
    -> (LoomId, TicketLoomId, BundleId) -- ^ Parsed bundle URI to apply
    -> ExceptT Text AppDB (Loom, TicketId, NonEmpty Text)
checkApplyDB actor capID (repoID, maybeBranch) (loomID, clothID, bundleID) = do

    -- Find the bundle and its loom in DB
    (loom, clothBranch, ticketID, maybeResolve, latest) <- do
        maybeBundle <- lift $ runMaybeT $ do
            (Entity _ loom, Entity _ cloth, Entity ticketID _, _author, resolve, proposal) <-
                MaybeT $ getCloth loomID clothID
            bundle <- MaybeT $ get bundleID
            guard $ bundleTicket bundle == clothID
            latest :| _prevs <-
                case justHere proposal of
                    Nothing ->
                        error "Why didn't getCloth find any bundles"
                    Just bundles -> return bundles
            return (loom, ticketLoomBranch cloth, ticketID, resolve, latest)
        fromMaybeE maybeBundle "Apply object bundle not found in DB"

    -- Verify the target repo/branch of the Apply is identical to the
    -- target repo/branch of the MR
    unless (maybeBranch == clothBranch) $
        throwE "Apply target != MR target"

    -- Find target repo in DB and verify it consents to being served by
    -- the loom
    unless (repoID == loomRepo loom) $
        throwE "MR target repo isn't the one served by the Apply object bundle's loom"
    repo <- getE repoID "Apply target: No such local repo in DB"
    unless (repoLoom repo == Just loomID) $
        throwE "Apply object bunde's loom doesn't have repo's consent to serve it"

    -- Verify that VCS type matches the presence of a branch:
    -- Branch specified for Git, isn't specified for Darcs
    case (repoVcs repo, maybeBranch) of
        (VCSDarcs, Nothing) -> pure ()
        (VCSGit, Just _) -> pure ()
        _ -> throwE "VCS type and branch presence mismatch"

    -- Verify the MR isn't already resolved and the bundle is the
    -- latest version
    unless (isNothing maybeResolve) $
        throwE "MR is already resolved"
    unless (bundleID == latest) $
        throwE "Bundle isn't the latest version"

    -- Verify the sender is authorized by the loom to apply a patch
    capability <-
        case capID of
            Left (capActor, _, capItem) -> return (capActor, capItem)
            Right _ -> throwE "Capability is a remote URI, i.e. not authored by the local loom"
    verifyCapability capability actor (LocalResourceLoom loomID) AP.RoleWrite

    -- Get the patches from DB, verify VCS match just in case
    diffs <- do
        ps <-
            lift $ map entityVal <$>
                selectList [PatchBundle ==. bundleID] [Asc PatchId]
        let patchVCS = patchMediaTypeVCS . patchType
        case NE.nonEmpty ps of
            Nothing -> error "Bundle without patches"
            Just ne ->
                if all ((== repoVcs repo) . patchVCS) ne
                    then return $ NE.map patchContent ne
                    else throwE "Patch type mismatch with repo VCS type"

    return (loom, ticketID, diffs)

tryUnresolve (Left (_actorByKey, _actorEntity, itemID)) = do
    maybeResolve <- getBy $ UniqueTicketResolveLocalActivity itemID
    for maybeResolve $ \ (Entity resolveLocalID resolveLocal) -> do
        let resolveID = ticketResolveLocalTicket resolveLocal
        resolve <- getJust resolveID
        let ticketID = ticketResolveTicket resolve
        return
            ( delete resolveLocalID >> delete resolveID
            , ticketID
            )
tryUnresolve (Right remoteActivityID) = do
    maybeResolve <- getBy $ UniqueTicketResolveRemoteActivity remoteActivityID
    for maybeResolve $ \ (Entity resolveRemoteID resolveRemote) -> do
        let resolveID = ticketResolveRemoteTicket resolveRemote
        resolve <- getJust resolveID
        let ticketID = ticketResolveTicket resolve
        return
            ( delete resolveRemoteID >> delete resolveID
            , ticketID
            )

getTicketOrigin
    :: MonadIO m
    => TicketId
    -> SqlPersistT m
        (Maybe
            (Either
                (Entity TicketOriginLocal)
                (Entity TicketOriginRemote)
            )
        )
getTicketOrigin t = do
    l <- getBy $ UniqueTicketOriginLocal t
    r <- getBy $ UniqueTicketOriginRemote t
    case (l, r) of
        (Nothing, Nothing) -> pure Nothing
        (Just o, Nothing) -> pure $ Just $ Left o
        (Nothing, Just o) -> pure $ Just $ Right o
        (Just _, Just _) -> error "Both local and remote ticket origin"

-- | Get summaries of all the tickets in the given project.
getTicketSummaries
    :: Maybe (E.SqlExpr (Maybe (Entity TicketResolve)) -> E.SqlExpr (E.Value Bool))
    -> Maybe (E.SqlExpr (Entity Ticket) -> [E.SqlExpr E.OrderBy])
    -> Maybe (Int, Int)
    -> DeckId
    -> AppDB [TicketSummary]
getTicketSummaries mfilt morder offlim deckID = do
    tickets <- E.select $ E.from $
        \ ( t
            `E.InnerJoin` td
            `E.LeftOuterJoin` (tal `E.InnerJoin` obi `E.InnerJoin` a)
            `E.LeftOuterJoin` (tar `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i)
            `E.LeftOuterJoin` tr
            `E.InnerJoin` d
            `E.LeftOuterJoin` m
          ) -> do
            E.on $ E.just (d E.^. DiscussionId) E.==. m E.?. MessageRoot
            E.on $ t E.^. TicketDiscuss E.==. d E.^. DiscussionId

            E.on $ E.just (t E.^. TicketId) E.==. tr E.?. TicketResolveTicket

            E.on $ ro E.?. RemoteObjectInstance E.==. i E.?. InstanceId
            E.on $ ra E.?. RemoteActorIdent E.==. ro E.?. RemoteObjectId
            E.on $ tar E.?. TicketAuthorRemoteAuthor E.==. ra E.?. RemoteActorId
            E.on $ E.just (t E.^. TicketId) E.==. tar E.?. TicketAuthorRemoteTicket

            E.on $ obi E.?. OutboxItemOutbox E.==. a E.?. ActorOutbox
            E.on $ tal E.?. TicketAuthorLocalOpen E.==. obi E.?. OutboxItemId
            E.on $ E.just (t E.^. TicketId) E.==. tal E.?. TicketAuthorLocalTicket

            E.on $ t E.^. TicketId E.==. td E.^. TicketDeckTicket

            E.where_ $ td E.^. TicketDeckDeck E.==. E.val deckID
            E.groupBy
                ( (t E.^. TicketId, td E.^. TicketDeckId)
                , tr E.?. TicketResolveId
                , tal E.?. TicketAuthorLocalId, obi E.?. OutboxItemId, a E.?. ActorId
                , ra E.?. RemoteActorId, ro E.?. RemoteObjectId, i E.?. InstanceId
                )
            for_ mfilt $ \ filt -> E.where_ $ filt tr
            for_ morder $ \ order -> E.orderBy $ order t
            for_ offlim $ \ (off, lim) -> do
                E.offset $ fromIntegral off
                E.limit $ fromIntegral lim

            return
                ( t E.^. TicketId
                , td E.^. TicketDeckId
                , a
                , i, ro, ra
                , t E.^. TicketCreated
                , t E.^. TicketTitle
                , tr E.?. TicketResolveId
                , E.count $ m E.?. MessageId
                )

    for tickets $
        \ (E.Value tid, E.Value tdid, ma, mi, mro, mra, E.Value c, E.Value t, E.Value mc, E.Value r) -> do
            labels <- E.select $ E.from $ \ (tpc `E.InnerJoin` wf) -> do
                E.on $ tpc E.^. TicketParamClassField E.==. wf E.^. WorkflowFieldId
                E.where_ $ tpc E.^. TicketParamClassTicket E.==. E.val tid
                return wf
            created <-
                case (ma, mi, mro, mra) of
                    (Just a, Nothing, Nothing, Nothing) -> do
                        la <- getLocalActor $ entityKey a
                        return $ Left (la, entityVal a)
                    (Nothing, Just i, Just ro, Just ra) ->
                        pure $ Right (entityVal i, entityVal ro, entityVal ra)
                    _ -> error "Ticket author DB invalid state"
            return TicketSummary
                { tsId        = tdid
                , tsCreatedBy = created
                , tsCreatedAt = c
                , tsTitle     = t
                , tsLabels    = map entityVal labels
                , tsClosed    = isJust mc
                , tsComments  = r
                }

getClothSummaries
    :: Maybe (E.SqlExpr (Maybe (Entity TicketResolve)) -> E.SqlExpr (E.Value Bool))
    -> Maybe (E.SqlExpr (Entity Ticket) -> [E.SqlExpr E.OrderBy])
    -> Maybe (Int, Int)
    -> LoomId
    -> AppDB [ClothSummary]
getClothSummaries mfilt morder offlim loomID = do
    tickets <- E.select $ E.from $
        \ ( t
            `E.InnerJoin` tl
            `E.LeftOuterJoin` (tal `E.InnerJoin` obi `E.InnerJoin` a)
            `E.LeftOuterJoin` (tar `E.InnerJoin` ra `E.InnerJoin` ro `E.InnerJoin` i)
            `E.LeftOuterJoin` tr
            `E.InnerJoin` d
            `E.LeftOuterJoin` m
          ) -> do
            E.on $ E.just (d E.^. DiscussionId) E.==. m E.?. MessageRoot
            E.on $ t E.^. TicketDiscuss E.==. d E.^. DiscussionId

            E.on $ E.just (t E.^. TicketId) E.==. tr E.?. TicketResolveTicket

            E.on $ ro E.?. RemoteObjectInstance E.==. i E.?. InstanceId
            E.on $ ra E.?. RemoteActorIdent E.==. ro E.?. RemoteObjectId
            E.on $ tar E.?. TicketAuthorRemoteAuthor E.==. ra E.?. RemoteActorId
            E.on $ E.just (t E.^. TicketId) E.==. tar E.?. TicketAuthorRemoteTicket

            E.on $ obi E.?. OutboxItemOutbox E.==. a E.?. ActorOutbox
            E.on $ tal E.?. TicketAuthorLocalOpen E.==. obi E.?. OutboxItemId
            E.on $ E.just (t E.^. TicketId) E.==. tal E.?. TicketAuthorLocalTicket

            E.on $ t E.^. TicketId E.==. tl E.^. TicketLoomTicket

            E.where_ $ tl E.^. TicketLoomLoom E.==. E.val loomID
            E.groupBy
                ( (t E.^. TicketId, tl E.^. TicketLoomId)
                , tr E.?. TicketResolveId
                , tal E.?. TicketAuthorLocalId, obi E.?. OutboxItemId, a E.?. ActorId
                , ra E.?. RemoteActorId, ro E.?. RemoteObjectId, i E.?. InstanceId
                )
            for_ mfilt $ \ filt -> E.where_ $ filt tr
            for_ morder $ \ order -> E.orderBy $ order t
            for_ offlim $ \ (off, lim) -> do
                E.offset $ fromIntegral off
                E.limit $ fromIntegral lim

            return
                ( t E.^. TicketId
                , tl E.^. TicketLoomId
                , a
                , i, ro, ra
                , t E.^. TicketCreated
                , t E.^. TicketTitle
                , tr E.?. TicketResolveId
                , E.count $ m E.?. MessageId
                )

    for tickets $
        \ (E.Value tid, E.Value tlid, ma, mi, mro, mra, E.Value c, E.Value t, E.Value mc, E.Value r) -> do
            labels <- E.select $ E.from $ \ (tpc `E.InnerJoin` wf) -> do
                E.on $ tpc E.^. TicketParamClassField E.==. wf E.^. WorkflowFieldId
                E.where_ $ tpc E.^. TicketParamClassTicket E.==. E.val tid
                return wf
            created <-
                case (ma, mi, mro, mra) of
                    (Just a, Nothing, Nothing, Nothing) -> do
                        la <- getLocalActor $ entityKey a
                        return $ Left (la, entityVal a)
                    (Nothing, Just i, Just ro, Just ra) ->
                        pure $ Right (entityVal i, entityVal ro, entityVal ra)
                    _ -> error "Ticket author DB invalid state"
            return ClothSummary
                { csId        = tlid
                , csCreatedBy = created
                , csCreatedAt = c
                , csTitle     = t
                , csLabels    = map entityVal labels
                , csClosed    = isJust mc
                , csComments  = r
                }

getTicket
    :: MonadIO m
    => DeckId
    -> TicketDeckId
    -> ReaderT SqlBackend m
        ( Maybe
            ( Entity Deck
            , Entity TicketDeck
            , Entity Ticket
            , Either (Entity TicketAuthorLocal) (Entity TicketAuthorRemote)
            , Maybe
                ( Entity TicketResolve
                , Either
                    (Entity TicketResolveLocal)
                    (Entity TicketResolveRemote)
                )
            )
        )
getTicket did tdid = runMaybeT $ do
    d <- MaybeT $ get did
    td <- MaybeT $ get tdid
    guard $ ticketDeckDeck td == did

    let tid = ticketDeckTicket td
    t <- lift $ getJust tid

    author <-
        lift $
            requireEitherAlt
                (getBy $ UniqueTicketAuthorLocal tid)
                (getBy $ UniqueTicketAuthorRemote tid)
                "Ticket doesn't have author"
                "Ticket has both local and remote author"

    mresolved <- lift $ getResolved tid

    return (Entity did d, Entity tdid td, Entity tid t, author, mresolved)

    where

    getResolved
        :: MonadIO m
        => TicketId
        -> ReaderT SqlBackend m
            (Maybe
                ( Entity TicketResolve
                , Either
                    (Entity TicketResolveLocal)
                    (Entity TicketResolveRemote)
                )
            )
    getResolved tid = do
        metr <- getBy $ UniqueTicketResolve tid
        for metr $ \ etr@(Entity trid _) ->
            (etr,) <$>
                requireEitherAlt
                    (getBy $ UniqueTicketResolveLocal trid)
                    (getBy $ UniqueTicketResolveRemote trid)
                    "No TRX"
                    "Both TRL and TRR"

getTicket404
    :: KeyHashid Deck
    -> KeyHashid TicketDeck
    -> AppDB
        ( Entity Deck
        , Entity TicketDeck
        , Entity Ticket
        , Either (Entity TicketAuthorLocal) (Entity TicketAuthorRemote)
        , Maybe
            ( Entity TicketResolve
            , Either
                (Entity TicketResolveLocal)
                (Entity TicketResolveRemote)
            )
        )
getTicket404 dkhid tdkhid = do
    did <- decodeKeyHashid404 dkhid
    tdid <- decodeKeyHashid404 tdkhid
    mticket <- getTicket did tdid
    case mticket of
        Nothing -> notFound
        Just ticket -> return ticket
