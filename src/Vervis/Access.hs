{- This file is part of Vervis.
 -
 - Written in 2019, 2022, 2023, 2024 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

-- | In this module I'd like to collect all the operation access checks. When a
-- given user asks to perform a certain operation, do we accept the request and
-- perform the changes to our database etc.? The functions here should provide
-- the answer.
--
-- Vervis uses a role-based access control system (RBAC) with role inheritance.
-- In order to determine access to a given operation, conceptually the
-- following two steps happen:
--
--     (1) Determine the actor's role
--     (2) Determine whether that role has access to the operation
--
-- There are 3 mechanisms for assigning a role to actors:
--
--     (1) Local:
--         A given project or repo may keep a list of users on the same server.
--         to which they are assigning roles.
--     (2) Capability:
--         For users from other instances, we provide signed capability
--         documents when they get assigned a role, and we verify them when the
--         user requests to perform an operation. We keep a token for each
--         capability we grant, so that we can revoke it, and so that we can
--         have a list of remote project/repo members.
--     (3) Public:
--         If an actor doesn't have a role through one of the previous two
--         methods, we may still assign a role to them using automatic
--         assignment. It's called _Public_ because it's generally meant for
--         assigning to the general public, people who aren't listed in our
--         role assignment lists, and to give public access to resources. A
--         project or repo may define a role to be assigned automatically
--         depending on the status of the actor. For example, assign a certain
--         role if it's a local logged-in user, or if it's an anonymous
--         not-logged-in client POSTing some operation, or if it's a remote
--         user from another instance, verified with a valid signature approved
--         by their server.
--
-- Conceptually, the default if none of these methods assign a role, is to
-- assume a "null role" i.e. a hypothetical role that can't access any
-- operations.
module Vervis.Access
    ( ObjectAccessStatus (..)
    , checkRepoAccess'
    , checkRepoAccess
    , checkProjectAccess
    )
where

import Control.Applicative ((<|>))
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.Reader
import Data.Barbie
import Data.Bifunctor
import Data.Bitraversable
import Data.ByteString (ByteString)
import Data.Maybe
import Data.Text (Text)
import Database.Persist
import Database.Persist.Sql
import GHC.Generics
import Yesod.Core.Handler

import qualified Database.Esqueleto as E

import Control.Concurrent.Actor
import Network.FedURI
import Web.Actor.Persist (stageHashidsContext)
import Yesod.Hashids
import Yesod.MonadSite

import qualified Web.Actor.Persist as WAP

import Control.Monad.Trans.Except.Local
import Data.Either.Local
import Database.Persist.Local

import Vervis.Actor
import Vervis.Foundation
import Vervis.Model
import Vervis.Model.Role
import Vervis.Persist.Actor
import Vervis.Recipient

data ObjectAccessStatus =
    NoSuchObject | ObjectAccessDenied | ObjectAccessAllowed
        deriving Eq

data PersonRole = Developer | User | Guest

{-
data RepoAuthorization
    = RepoAuthorizationLocal PersonId
    | RepoAuthorizationRemote RepoRemoteCollabId

data ProjectAuthorization
    = ProjectAuthorizationLocal PersonId
    | ProjectAuthorizationRemote ProjectRemoteCollabId
-}

roleHasAccess
    :: MonadIO m
    => PersonRole
    -> ProjectOperation
    -> ReaderT SqlBackend m Bool
roleHasAccess Developer     _  = pure True
roleHasAccess User          op = pure $ userAccess op
    where
    userAccess ProjOpOpenTicket      = True
    userAccess ProjOpAcceptTicket    = False
    userAccess ProjOpCloseTicket     = False
    userAccess ProjOpReopenTicket    = False
    userAccess ProjOpRequestTicket   = True
    userAccess ProjOpClaimTicket     = False
    userAccess ProjOpUnclaimTicket   = True
    userAccess ProjOpAssignTicket    = False
    userAccess ProjOpUnassignTicket  = False
    userAccess ProjOpAddTicketDep    = False
    userAccess ProjOpRemoveTicketDep = False
    userAccess ProjOpPush            = False
    userAccess ProjOpApplyPatch      = False
roleHasAccess Guest         _  = pure False

status :: Bool -> ObjectAccessStatus
status True  = ObjectAccessAllowed
status False = ObjectAccessDenied

checkRepoAccess'
    :: MonadIO m
    => Maybe PersonId
    -> ProjectOperation
    -> RepoId
    -> ReaderT SqlBackend m ObjectAccessStatus
checkRepoAccess' mpid op repoID = do
    mer <- runMaybeT $ do
        repo <- MaybeT $ get repoID
        return $ Entity repoID repo
    case mer of
        Nothing -> return NoSuchObject
        Just (Entity rid repo) -> do
            role <- do
                case mpid of
                    Just pid -> fromMaybe User <$> asCollab (repoResource repo) pid
                    Nothing -> pure Guest
            status <$> roleHasAccess role op
    where
    asCollab rid pid = do
        fmap (const Developer) . listToMaybe <$> do
            E.select $ E.from $ \ (collab `E.InnerJoin` recip `E.InnerJoin` enable) -> do
                E.on $ collab E.^. CollabId E.==. enable E.^. CollabEnableCollab
                E.on $ collab E.^. CollabId E.==. recip E.^. CollabRecipLocalCollab
                E.where_ $
                    collab E.^. CollabTopic E.==. E.val rid E.&&.
                    recip E.^. CollabRecipLocalPerson E.==. E.val pid
                E.limit 1
                return $ collab E.^. CollabId

checkRepoAccess
    :: (MonadIO m, MonadSite m, YesodHashids (SiteEnv m))
    => Maybe PersonId
    -> ProjectOperation
    -> KeyHashid Repo
    -> ReaderT SqlBackend m ObjectAccessStatus
checkRepoAccess mpid op repoHash = do
    mer <- runMaybeT $ do
        repoID <- decodeKeyHashidM repoHash
        repo <- MaybeT $ get repoID
        return $ Entity repoID repo
    case mer of
        Nothing -> return NoSuchObject
        Just (Entity rid repo) -> do
            role <- do
                case mpid of
                    Just pid -> fromMaybe User <$> asCollab (repoResource repo) pid
                    Nothing -> pure Guest
            status <$> roleHasAccess role op
    where
    asCollab rid pid = do
        fmap (const Developer) . listToMaybe <$> do
            E.select $ E.from $ \ (collab `E.InnerJoin` recip `E.InnerJoin` enable) -> do
                E.on $ collab E.^. CollabId E.==. enable E.^. CollabEnableCollab
                E.on $ collab E.^. CollabId E.==. recip E.^. CollabRecipLocalCollab
                E.where_ $
                    collab E.^. CollabTopic E.==. E.val rid E.&&.
                    recip E.^. CollabRecipLocalPerson E.==. E.val pid
                E.limit 1
                return $ collab E.^. CollabId

checkProjectAccess
    :: (MonadIO m, MonadSite m, YesodHashids (SiteEnv m))
    => Maybe PersonId
    -> ProjectOperation
    -> KeyHashid Deck
    -> ReaderT SqlBackend m ObjectAccessStatus
checkProjectAccess mpid op deckHash = do
    mej <- runMaybeT $ do
        deckID <- decodeKeyHashidM deckHash
        deck <- MaybeT $ get deckID
        return $ Entity deckID deck
    case mej of
        Nothing -> return NoSuchObject
        Just (Entity jid project) -> do
            role <- do
                case mpid of
                    Just pid -> fromMaybe User <$> asCollab (deckResource project) pid
                    Nothing -> pure Guest
            status <$> roleHasAccess role op
    where
    asCollab rid pid = do
        fmap (const Developer) . listToMaybe <$> do
            E.select $ E.from $ \ (collab `E.InnerJoin` recip `E.InnerJoin` enable) -> do
                E.on $ collab E.^. CollabId E.==. enable E.^. CollabEnableCollab
                E.on $ collab E.^. CollabId E.==. recip E.^. CollabRecipLocalCollab
                E.where_ $
                    collab E.^. CollabTopic E.==. E.val rid E.&&.
                    recip E.^. CollabRecipLocalPerson E.==. E.val pid
                E.limit 1
                return $ collab E.^. CollabId
