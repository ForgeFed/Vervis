{- This file is part of Vervis.
 -
 - Written in 2018, 2019, 2020, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE UndecidableInstances #-}

module Vervis.Migration.Model2024
    {-
    ( EntityField (..)
    , Unique (..)
    )
    -}
where

import Data.ByteString (ByteString)
import Data.Text (Text)
import Data.Time (UTCTime)
import Database.Persist.Class (EntityField, Unique)
import Database.Persist.EmailAddress ()
import Database.Persist.Schema.Types (Entity)
import Database.Persist.Schema ()
import Database.Persist.Schema.TH (makeEntitiesMigration)
import Database.Persist.Sql (SqlBackend)
import Text.Email.Validate (EmailAddress)
import Web.Text (HTML, PandocMarkdown)

import Crypto.ActorKey
import Development.PatchMediaType
import Development.PatchMediaType.Persist

import Vervis.FedURI
import Vervis.Migration.TH (schema)
import Vervis.Model.Group
import Vervis.Model.Ident
import Vervis.Model.Role
import Vervis.Model.TH
import Vervis.Model.Ticket
import Vervis.Model.Workflow

-- For migrations 77, 114

import Data.Int

import Database.Persist.JSON
import Network.FedURI
import Web.ActivityPub

makeEntitiesMigration "584"
    $(modelFile "migrations/584_2024-04-14_delete_gather.model")

makeEntitiesMigration "593"
    $(modelFile "migrations/593_2024-04-18_permit_extend.model")

makeEntitiesMigration "604"
    $(modelFile "migrations/604_2024-04-20_resource.model")

makeEntitiesMigration "611"
    $(modelFile "migrations/611_2024-04-20_permit_resource.model")

makeEntitiesMigration "625"
    $(modelFile "migrations/625_2024-04-27_errbox.model")

makeEntitiesMigration "627"
    $(modelFile "migrations/627_2024-04-29_komponent_mig.model")

makeEntitiesMigration "630"
    $(modelFile "migrations/630_2024-04-29_component_komponent.model")

makeEntitiesMigration "634"
    $(modelFile "migrations/634_2024-04-29_stem_holder.model")

makeEntitiesMigration "651"
    $(modelFile "migrations/651_2024-08-03_actor_create.model")

makeEntitiesMigration "675"
    $(modelFile "migrations/675_2024-11-28_acl.model")
