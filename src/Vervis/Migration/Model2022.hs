{- This file is part of Vervis.
 -
 - Written in 2018, 2019, 2020, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE UndecidableInstances #-}

module Vervis.Migration.Model2022
    {-
    ( EntityField (..)
    , Unique (..)
    , Collab285Generic (..)
    , CollabRecipLocal285Generic (..)
    , CollabRoleLocal285Generic (..)
    , CollabSenderLocal285Generic (..)
    , CollabTopicLocalProject285Generic (..)
    , CollabTopicLocalRepo285Generic (..)
    , OutboxItem285Generic (..)
    , Project285Generic (..)
    , ProjectCollab285
    , ProjectCollab285Generic (..)
    , Repo285Generic (..)
    , RepoCollab285
    , RepoCollab285Generic (..)
    , Project289
    , Inbox289Generic (..)
    , Outbox289Generic (..)
    , FollowerSet289Generic (..)
    , Actor289Generic (..)
    , Project289Generic (..)
    , Outbox297Generic (..)
    , OutboxItem297Generic (..)
    , Project297
    , Project297Generic (..)
    , Person297Generic (..)
    , CollabTopicLocalProject300
    , CollabTopicLocalProject300Generic (..)
    , CollabTopicLocalRepo300
    , CollabTopicLocalRepo300Generic (..)
    , CollabRecipLocal300Generic (..)
    , Person300Generic (..)
    , Project300Generic (..)
    , Repo300Generic (..)
    , CollabFulfillsLocalTopicCreation300Generic (..)
    )
    -}
where

import Data.ByteString (ByteString)
import Data.Text (Text)
import Data.Time (UTCTime)
import Database.Persist.Class (EntityField, Unique)
import Database.Persist.EmailAddress ()
import Database.Persist.Schema.Types (Entity)
import Database.Persist.Schema ()
import Database.Persist.Schema.TH (makeEntitiesMigration)
import Database.Persist.Sql (SqlBackend)
import Text.Email.Validate (EmailAddress)
import Web.Text (HTML, PandocMarkdown)

import Crypto.ActorKey
import Development.PatchMediaType
import Development.PatchMediaType.Persist

import Vervis.FedURI
import Vervis.Migration.TH (schema)
import Vervis.Model.Group
import Vervis.Model.Ident
import Vervis.Model.Role
import Vervis.Model.TH
import Vervis.Model.Ticket
import Vervis.Model.Workflow

import Data.Int
import Database.Persist.JSON

makeEntitiesMigration "285"
    $(modelFile "migrations/2022_06_14_collab_mig.model")

makeEntitiesMigration "289"
    $(modelFile "migrations/2022_07_17_project_actor.model")

makeEntitiesMigration "297"
    $(modelFile "migrations/2022_07_24_project_create.model")

makeEntitiesMigration "300"
    $(modelFile "migrations/2022_07_25_collab_fulfills_mig.model")

makeEntitiesMigration "303"
    $(modelFile "migrations/303_2022-08-04_username.model")

makeEntitiesMigration "308"
    $(modelFile "migrations/308_2022-08-04_remove_tcr.model")

makeEntitiesMigration "310"
    $(modelFile "migrations/310_2022-08-04_move_ticket_discuss.model")

makeEntitiesMigration "312"
    $(modelFile "migrations/312_2022-08-04_move_ticket_followers.model")

makeEntitiesMigration "316"
    $(modelFile "migrations/316_2022-08-04_move_ticket_accept.model")

makeEntitiesMigration "318"
    $(modelFile "migrations/318_2022-08-04_tal_ticket.model")

makeEntitiesMigration "323"
    $(modelFile "migrations/323_2022-08-04_tar_ticket.model")

makeEntitiesMigration "328"
    $(modelFile "migrations/328_2022-08-04_tjl_ticket.model")

makeEntitiesMigration "332"
    $(modelFile "migrations/332_2022-08-04_trl_ticket.model")

makeEntitiesMigration "338"
    $(modelFile "migrations/338_2022-08-04_rtd_child.model")

makeEntitiesMigration "342"
    $(modelFile "migrations/342_2022-08-04_ltd_parent.model")

makeEntitiesMigration "345"
    $(modelFile "migrations/345_2022-08-04_tdcl_child.model")

makeEntitiesMigration "348"
    $(modelFile "migrations/348_2022-08-04_tr_ticket.model")

makeEntitiesMigration "356"
    $(modelFile "migrations/356_2022-08-04_person_actor.model")

makeEntitiesMigration "365"
    $(modelFile "migrations/365_2022-08-04_group_actor.model")

makeEntitiesMigration "367"
    $(modelFile "migrations/367_2022-08-04_repo_actor.model")

makeEntitiesMigration "388"
    $(modelFile "migrations/388_2022-08-04_ticket_loom.model")

makeEntitiesMigration "396"
    $(modelFile "migrations/396_2022-08-04_repo_dir.model")

makeEntitiesMigration "409"
    $(modelFile "migrations/409_2022-08-05_repo_create.model")

makeEntitiesMigration "414"
    $(modelFile "migrations/414_2022-08-05_followremote_actor.model")

makeEntitiesMigration "418"
    $(modelFile "migrations/418_2022-08-06_follow_actor.model")

makeEntitiesMigration "426"
    $(modelFile "migrations/426_2022-08-21_collab_accept_mig.model")

makeEntitiesMigration "429"
    $(modelFile "migrations/429_2022-08-30_collab_repo.model")

makeEntitiesMigration "430"
    $(modelFile "migrations/430_2022-08-30_collab_deck.model")

makeEntitiesMigration "431"
    $(modelFile "migrations/431_2022-08-30_collab_loom.model")

makeEntitiesMigration "447"
    $(modelFile "migrations/447_2022-08-30_collab_accept.model")

makeEntitiesMigration "466"
    $(modelFile "migrations/466_2022-09-04_collab_topic_repo.model")

makeEntitiesMigration "467"
    $(modelFile "migrations/467_2022-09-04_collab_topic_deck.model")

makeEntitiesMigration "468"
    $(modelFile "migrations/468_2022-09-04_collab_topic_loom.model")

makeEntitiesMigration "486"
    $(modelFile "migrations/486_2022-09-04_collab_enable.model")

makeEntitiesMigration "495"
    $(modelFile "migrations/495_2022-09-21_ticket_title.model")

makeEntitiesMigration "498"
    $(modelFile "migrations/498_2022-10-03_forwarder.model")

makeEntitiesMigration "504"
    $(modelFile "migrations/504_2022-10-16_message_author.model")

makeEntitiesMigration "507"
    $(modelFile "migrations/507_2022-10-16_workflow.model")

makeEntitiesMigration "515"
    $(modelFile "migrations/515_2022-10-19_inviter_local.model")

makeEntitiesMigration "520"
    $(modelFile "migrations/520_2022-10-19_inviter_remote.model")

makeEntitiesMigration "525"
    $(modelFile "migrations/525_2022-10-19_collab_accept_local.model")

makeEntitiesMigration "527"
    $(modelFile "migrations/527_2022-10-20_collab_accept_remote.model")
