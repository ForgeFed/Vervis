{- This file is part of Vervis.
 -
 - Written in 2018, 2019, 2020, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Vervis.Migration.Entities
    ( model_2016_08_04
    , model_2016_09_01_just_workflow
    , model_2016_09_01_rest
    , model_2019_02_03_verifkey
    , model_2019_03_19
    , model_2019_03_30
    , model_2019_04_11
    , model_2019_04_12
    , model_2019_04_22
    , model_2019_05_03
    , model_2019_05_17
    , model_2019_06_06
    , model_2019_09_25
    , model_2019_11_04
    , model_2020_01_05
    , model_2020_02_05
    , model_2020_02_07
    , model_2020_02_09
    , model_2020_02_22
    , model_2020_04_07
    , model_2020_04_09
    , model_2020_05_12
    , model_2020_05_16
    , model_2020_05_17
    , model_2020_05_25
    , model_2020_05_28
    , model_2020_06_01
    , model_2020_06_18
    , model_2020_07_23
    , model_2020_07_27
    , model_2020_08_10
    , model_2022_06_14
    , model_2022_07_17
    , model_2022_07_24
    , model_384_loom
    , model_386_assignee
    , model_399_fwder
    , model_408_collab_loom
    , model_425_collab_accept
    , model_428_collab_topic_local
    , model_451_collab_remote_accept
    , model_453_collab_receive
    , model_494_mr_origin
    , model_497_sigkey
    , model_508_invite
    , model_530_join
    , model_531_follow_request
    , model_541_project
    , model_542_component
    , model_551_group_collab
    , model_552_collab_deleg
    , model_564_permit
    , model_570_source_dest
    , model_577_component_gather
    , model_578_source_remove
    , model_583_dest_start
    , model_591_component_gather
    , model_592_permit_extend
    , model_601_permit_extend_resource
    , model_603_resource
    , model_626_komponent
    , model_638_effort_squad
    , model_639_component_convey
    , model_648_report
    , model_649_factory
    , model_650_fulfills_resident
    , model_670_origin
    , model_671_origin
    , model_673_fork
    )
where

import Data.ByteString (ByteString)
import Data.Text (Text)
import Data.Time (UTCTime)
import Database.Persist.Class (EntityField, Unique)
import Database.Persist.EmailAddress ()
import Database.Persist.Schema.Types
import Database.Persist.Schema ()
import Database.Persist.Schema.TH (makeEntitiesMigration)
import Database.Persist.Sql hiding (Entity)
import Text.Email.Validate (EmailAddress)
import Web.Text (HTML, PandocMarkdown)

import Crypto.ActorKey
import Development.PatchMediaType
import Development.PatchMediaType.Persist

import Vervis.FedURI
import Vervis.Migration.TH (schema)
import Vervis.Model.Group
import Vervis.Model.Ident
import Vervis.Model.Role
import Vervis.Model.TH
import Vervis.Model.Ticket
import Vervis.Model.Workflow

-- For migrations 77, 114

import Data.Int

import Database.Persist.JSON
import Network.FedURI
import Web.ActivityPub

type PersistActivity = PersistJSON (Doc Activity URIMode)

model_2016_08_04 :: [Entity]
model_2016_08_04 = $$(schema "2016_08_04")

model_2016_09_01_just_workflow :: [Entity]
model_2016_09_01_just_workflow = $$(schema "2016_09_01_just_workflow")

model_2016_09_01_rest :: [Entity]
model_2016_09_01_rest = $$(schema "2016_09_01_rest")

model_2019_02_03_verifkey :: [Entity]
model_2019_02_03_verifkey = $$(schema "2019_02_03_verifkey")

model_2019_03_19 :: [Entity]
model_2019_03_19 = $$(schema "2019_03_19")

model_2019_03_30 :: [Entity]
model_2019_03_30 = $$(schema "2019_03_30")

model_2019_04_11 :: [Entity]
model_2019_04_11 = $$(schema "2019_04_11")

model_2019_04_12 :: [Entity]
model_2019_04_12 = $$(schema "2019_04_12")

model_2019_04_22 :: [Entity]
model_2019_04_22 = $$(schema "2019_04_22")

model_2019_05_03 :: [Entity]
model_2019_05_03 = $$(schema "2019_05_03")

model_2019_05_17 :: [Entity]
model_2019_05_17 = $$(schema "2019_05_17")

model_2019_06_06 :: [Entity]
model_2019_06_06 = $$(schema "2019_06_06")

model_2019_09_25 :: [Entity]
model_2019_09_25 = $$(schema "2019_09_25")

model_2019_11_04 :: [Entity]
model_2019_11_04 = $$(schema "2019_11_04")

model_2020_01_05 :: [Entity]
model_2020_01_05 = $$(schema "2020_01_05")

model_2020_02_05 :: [Entity]
model_2020_02_05 = $$(schema "2020_02_05_local_ticket")

model_2020_02_07 :: [Entity]
model_2020_02_07 = $$(schema "2020_02_07_tpl")

model_2020_02_09 :: [Entity]
model_2020_02_09 = $$(schema "2020_02_09_tup")

model_2020_02_22 :: [Entity]
model_2020_02_22 = $$(schema "2020_02_22_tpr")

model_2020_04_07 :: [Entity]
model_2020_04_07 = $$(schema "2020_04_07_tpra")

model_2020_04_09 :: [Entity]
model_2020_04_09 = $$(schema "2020_04_09_rt")

model_2020_05_12 :: [Entity]
model_2020_05_12 = $$(schema "2020_05_12_fwd_sender")

model_2020_05_16 :: [Entity]
model_2020_05_16 = $$(schema "2020_05_16_tcl")

model_2020_05_17 :: [Entity]
model_2020_05_17 = $$(schema "2020_05_17_patch")

model_2020_05_25 :: [Entity]
model_2020_05_25 = $$(schema "2020_05_25_fwd_sender_repo")

model_2020_05_28 :: [Entity]
model_2020_05_28 = $$(schema "2020_05_28_tda")

model_2020_06_01 :: [Entity]
model_2020_06_01 = $$(schema "2020_06_01_tdc")

model_2020_06_18 :: [Entity]
model_2020_06_18 = $$(schema "2020_06_18_tdo")

model_2020_07_23 :: [Entity]
model_2020_07_23 = $$(schema "2020_07_23_remote_collection_reboot")

model_2020_07_27 :: [Entity]
model_2020_07_27 = $$(schema "2020_07_27_ticket_resolve")

model_2020_08_10 :: [Entity]
model_2020_08_10 = $$(schema "2020_08_10_bundle")

model_2022_06_14 :: [Entity]
model_2022_06_14 = $$(schema "2022_06_14_collab")

model_2022_07_17 :: [Entity]
model_2022_07_17 = $$(schema "2022_07_17_actor")

model_2022_07_24 :: [Entity]
model_2022_07_24 = $$(schema "2022_07_24_collab_fulfills")

model_384_loom :: [Entity]
model_384_loom = $$(schema "384_2022-08-04_loom")

model_386_assignee :: [Entity]
model_386_assignee = $$(schema "386_2022-08-04_assignee")

model_399_fwder :: [Entity]
model_399_fwder = $$(schema "399_2022-08-04_fwder")

model_408_collab_loom :: [Entity]
model_408_collab_loom = $$(schema "408_2022-08-04_collab_loom")

model_425_collab_accept :: [Entity]
model_425_collab_accept = $$(schema "425_2022-08-21_collab_accept")

model_428_collab_topic_local :: [Entity]
model_428_collab_topic_local = $$(schema "428_2022-08-29_collab_topic_local")

model_451_collab_remote_accept :: [Entity]
model_451_collab_remote_accept = $$(schema "451_2022-08-30_collab_remote_accept")

model_453_collab_receive :: [Entity]
model_453_collab_receive = $$(schema "453_2022-09-01_collab_receive")

model_494_mr_origin :: [Entity]
model_494_mr_origin = $$(schema "494_2022-09-17_mr_origin")

model_497_sigkey :: [Entity]
model_497_sigkey = $$(schema "497_2022-09-29_sigkey")

model_508_invite :: [Entity]
model_508_invite = $$(schema "508_2022-10-19_invite")

model_530_join :: [Entity]
model_530_join = $$(schema "530_2022-11-01_join")

model_531_follow_request :: [Entity]
model_531_follow_request = $$(schema "531_2023-06-15_follow_request")

model_541_project :: [Entity]
model_541_project = $$(schema "541_2023-06-26_project")

model_542_component :: [Entity]
model_542_component = $$(schema "542_2023-06-26_component")

model_551_group_collab :: [Entity]
model_551_group_collab = $$(schema "551_2023-11-21_group_collab")

model_552_collab_deleg :: [Entity]
model_552_collab_deleg = $$(schema "552_2023-11-21_collab_deleg")

model_564_permit :: [Entity]
model_564_permit = $$(schema "564_2023-11-22_permit")

model_570_source_dest :: [Entity]
model_570_source_dest = $$(schema "570_2023-12-09_source_dest")

model_577_component_gather :: [Entity]
model_577_component_gather = $$(schema "577_2024-03-13_component_gather")

model_578_source_remove :: [Entity]
model_578_source_remove = $$(schema "578_2024-04-03_source_remove")

model_583_dest_start :: [Entity]
model_583_dest_start = $$(schema "583_2024-04-13_dest_start")

model_591_component_gather :: [Entity]
model_591_component_gather = $$(schema "591_2024-04-14_component_gather")

model_592_permit_extend :: [Entity]
model_592_permit_extend = $$(schema "592_2024-04-18_permit_extend")

model_601_permit_extend_resource :: [Entity]
model_601_permit_extend_resource =
    $$(schema "601_2024-04-18_permit_extend_resource")

model_603_resource :: [Entity]
model_603_resource = $$(schema "603_2024-04-20_resource")

model_626_komponent :: [Entity]
model_626_komponent = $$(schema "626_2024-04-29_komponent")

model_638_effort_squad :: [Entity]
model_638_effort_squad = $$(schema "638_2024-05-14_effort_squad")

model_639_component_convey :: [Entity]
model_639_component_convey = $$(schema "639_2024-05-14_component_convey")

type ListOfByteStrings = [ByteString]

model_648_report :: [Entity]
model_648_report = $$(schema "648_2024-07-06_report")

model_649_factory :: [Entity]
model_649_factory = $$(schema "649_2024-07-29_factory")

model_650_fulfills_resident :: [Entity]
model_650_fulfills_resident = $$(schema "650_2024-08-03_fulfills_resident")

model_670_origin :: [Entity]
model_670_origin = $$(schema "670_2024-10-28_origin")

model_671_origin :: [Entity]
model_671_origin = $$(schema "671_2024-10-28_origin")

model_673_fork :: [Entity]
model_673_fork = $$(schema "673_2024-11-14_fork")
