{- This file is part of Vervis.
 -
 - Written in 2018, 2019, 2020, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE UndecidableInstances #-}

module Vervis.Migration.Model2020
    {-
    ( EntityField (..)
    , Unique (..)
    , Ticket189
    , Ticket189Generic (..)
    , LocalTicket189Generic (..)
    , Sharer194Generic (..)
    , Outbox194Generic (..)
    , OutboxItem194Generic (..)
    , Inbox194Generic (..)
    , FollowerSet194Generic (..)
    , Project194Generic (..)
    , Workflow194Generic (..)
    , Ticket194Generic (..)
    , LocalTicket194Generic (..)
    , TicketAuthorLocal194
    , TicketAuthorLocal194Generic (..)
    , Discussion194Generic (..)
    , Ticket201
    , Ticket201Generic (..)
    , TicketProjectLocal201Generic (..)
    , Sharer205Generic (..)
    , Outbox205Generic (..)
    , OutboxItem205Generic (..)
    , Inbox205Generic (..)
    , FollowerSet205Generic (..)
    , Project205Generic (..)
    , Workflow205Generic (..)
    , Ticket205Generic (..)
    , TicketProjectLocal205Generic (..)
    , TicketAuthorRemote205
    , TicketAuthorRemote205Generic (..)
    , Instance215Generic (..)
    , RemoteObject215Generic (..)
    , RemoteDiscussion215
    , RemoteDiscussion215Generic (..)
    , TicketUnderProject223Generic (..)
    , Instance227Generic (..)
    , RemoteObject227Generic (..)
    , RemoteMessage227
    , RemoteMessage227Generic (..)
    , RemoteTicket238
    , RemoteTicket238Generic (..)
    , Instance238Generic (..)
    , RemoteObject238Generic (..)
    , Discussion238Generic (..)
    , RemoteDiscussion238Generic (..)
    , Forwarding241
    , Forwarding241Generic (..)
    , ForwarderProject241Generic (..)
    , TicketContextLocal247
    , TicketContextLocal247Generic (..)
    , TicketProjectLocal247Generic (..)
    , OutboxItem255Generic (..)
    , Person255Generic (..)
    , TicketDependency255
    , TicketDependency255Generic (..)
    , TicketDependencyAuthorLocal255Generic (..)
    , RemoteTicket260Generic (..)
    , LocalTicketDependency260
    , LocalTicketDependency260Generic (..)
    , TicketDependencyChildLocal260Generic (..)
    , TicketDependencyChildRemote260Generic (..)
    , Discussion263Generic (..)
    , FollowerSet263Generic (..)
    , Ticket263Generic (..)
    , LocalTicket263Generic (..)
    , LocalTicketDependency263
    , LocalTicketDependency263Generic (..)
    , Outbox266Generic (..)
    , OutboxItem266Generic (..)
    , LocalTicketDependency266
    , LocalTicketDependency266Generic (..)
    , LocalTicket266Generic (..)
    , TicketContextLocal266Generic (..)
    , TicketUnderProject266Generic (..)
    , TicketProjectLocal266Generic (..)
    , Project266Generic (..)
    , TicketResolve276Generic (..)
    , TicketResolveLocal276Generic (..)
    , Ticket276Generic (..)
    , LocalTicket276
    , LocalTicket276Generic (..)
    , Person276Generic (..)
    , OutboxItem276Generic (..)
    , TicketProjectLocal276Generic (..)
    , Project276Generic (..)
    , Ticket280Generic (..)
    , Bundle280Generic (..)
    , Patch280
    , Patch280Generic (..)
    , Repo282
    , Repo282Generic (..)
    )
    -}
where

import Data.ByteString (ByteString)
import Data.Text (Text)
import Data.Time (UTCTime)
import Database.Persist.Class (EntityField, Unique)
import Database.Persist.EmailAddress ()
import Database.Persist.Schema.Types (Entity)
import Database.Persist.Schema ()
import Database.Persist.Schema.TH (makeEntitiesMigration)
import Database.Persist.Sql (SqlBackend)
import Text.Email.Validate (EmailAddress)
import Web.Text (HTML, PandocMarkdown)

import Crypto.ActorKey
import Development.PatchMediaType
import Development.PatchMediaType.Persist

import Vervis.FedURI
import Vervis.Migration.TH (schema)
import Vervis.Model.Group
import Vervis.Model.Ident
import Vervis.Model.Role
import Vervis.Model.TH
import Vervis.Model.Ticket
import Vervis.Model.Workflow

-- For migrations 77, 114

import Data.Int

import Database.Persist.JSON
import Network.FedURI
import Web.ActivityPub

makeEntitiesMigration "189" $(modelFile "migrations/2020_02_05_mig.model")

makeEntitiesMigration "194"
    $(modelFile "migrations/2020_02_06_tal_point_to_lt.model")

makeEntitiesMigration "201"
    $(modelFile "migrations/2020_02_07_tpl_mig.model")

makeEntitiesMigration "205"
    $(modelFile "migrations/2020_02_08_tar_point_to_tpl.model")

makeEntitiesMigration "215"
    $(modelFile "migrations/2020_02_09_rd_point_to_ro.model")

makeEntitiesMigration "223"
    $(modelFile "migrations/2020_02_09_tup_mig.model")

makeEntitiesMigration "227"
    $(modelFile "migrations/2020_02_10_rm_point_to_ro.model")

makeEntitiesMigration "238" $(modelFile "migrations/2020_04_10_rt_rd.model")

makeEntitiesMigration "241"
    $(modelFile "migrations/2020_05_12_fwd_sender_mig.model")

makeEntitiesMigration "247"
    $(modelFile "migrations/2020_05_16_tcl_mig.model")

makeEntitiesMigration "255" $(modelFile "migrations/2020_05_28_tda_mig.model")

makeEntitiesMigration "260" $(modelFile "migrations/2020_06_01_tdc_mig.model")

makeEntitiesMigration "263" $(modelFile "migrations/2020_06_02_tdp.model")

makeEntitiesMigration "266"
    $(modelFile "migrations/2020_06_15_td_accept.model")

makeEntitiesMigration "276"
    $(modelFile "migrations/2020_07_27_ticket_resolve_mig.model")

makeEntitiesMigration "280"
    $(modelFile "migrations/2020_08_10_bundle_mig.model")

makeEntitiesMigration "282"
    $(modelFile "migrations/2020_08_13_vcs.model")
