{- This file is part of Vervis.
 -
 - Written in 2018, 2019, 2020, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE UndecidableInstances #-}

module Vervis.Migration.Model2019
    {-
    ( EntityField (..)
    , Unique (..)
    , VerifKey2019Generic (..)
    , VerifKey2019
    , VerifKeySharedUsage2019Generic (..)
    , VerifKeySharedUsage2019
    , Message2019Generic (..)
    , Message2019
    , LocalMessage2019Generic (..)
    , LocalMessage2019
    , FollowerSet2019Generic (..)
    , Ticket2019
    , Sharer201905Generic (..)
    , Person201905Generic (..)
    , OutboxItem201905Generic (..)
    , OutboxItem201905
    , LocalMessage201905Generic (..)
    , LocalMessage201905
    , Message201905Generic (..)
    , Project201905Generic (..)
    , Ticket201905Generic (..)
    , Instance201905Generic (..)
    , RemoteDiscussion201905Generic (..)
    , RemoteMessage201905Generic (..)
    , Message201906Generic (..)
    , Message201906
    , Ticket201906Generic (..)
    , Ticket201906
    , Ticket20190606Generic (..)
    , Ticket20190606
    , TicketAuthorLocal20190606Generic (..)
    , Person20190607Generic (..)
    , Person20190607
    , Inbox20190607Generic (..)
    , InboxItemLocal20190607Generic (..)
    , InboxItemLocal20190607
    , InboxItemRemote20190607Generic (..)
    , InboxItemRemote20190607
    , Project20190609
    , Inbox20190609Generic (..)
    , InboxItem2019FillGeneric (..)
    , InboxItem2019Fill
    , InboxItemLocal2019FillGeneric (..)
    , InboxItemRemote2019FillGeneric (..)
    , Project2019FillGeneric (..)
    , Ticket2019FillGeneric (..)
    , Message2019FillGeneric (..)
    , LocalMessage2019FillGeneric (..)
    , RemoteMessage2019FillGeneric (..)
    , FollowerSet20190610Generic (..)
    , Project20190610
    , Sharer20190612Generic (..)
    , Person20190612Generic (..)
    , OutboxItem20190612Generic (..)
    , Inbox20190612Generic (..)
    , InboxItem20190612Generic (..)
    , InboxItemLocal20190612Generic (..)
    , Project20190612Generic (..)
    , Ticket20190612Generic (..)
    , Ticket20190612
    , TicketAuthorLocal20190612Generic (..)
    , Person20190615Generic (..)
    , Person20190615
    , Outbox20190615Generic (..)
    , OutboxItem20190615Generic (..)
    , OutboxItem20190615
    , Project20190616Generic (..)
    , Project20190616
    , Outbox20190616Generic (..)
    , Sharer20190624Generic (..)
    , Person20190624Generic (..)
    , Outbox20190624Generic (..)
    , OutboxItem20190624Generic (..)
    , Inbox20190624Generic (..)
    , InboxItem20190624Generic (..)
    , InboxItemLocal20190624Generic (..)
    , Project20190624Generic (..)
    , Ticket20190624Generic (..)
    , Ticket20190624
    , TicketAuthorLocal20190624Generic (..)
    , Sharer127Generic (..)
    , Person127Generic (..)
    , Outbox127Generic (..)
    , Inbox127Generic (..)
    , Project127Generic (..)
    , Ticket127Generic (..)
    , TicketDependency127Generic (..)
    , TicketDependency127
    , Inbox130Generic (..)
    , FollowerSet130Generic (..)
    , Repo130
    , Person130
    , Outbox138Generic (..)
    , Repo138
    , Instance152Generic (..)
    , RemoteObject152Generic (..)
    , RemoteActivity152Generic (..)
    , RemoteActivity152
    , Instance159Generic (..)
    , RemoteObject159Generic (..)
    , RemoteActor159Generic (..)
    , RemoteActor159
    , UnfetchedRemoteActor159Generic (..)
    , UnfetchedRemoteActor159
    , RemoteCollection159Generic (..)
    , RemoteCollection159
    )
    -}
where

import Data.ByteString (ByteString)
import Data.Text (Text)
import Data.Time (UTCTime)
import Database.Persist.Class (EntityField, Unique)
import Database.Persist.EmailAddress ()
import Database.Persist.Schema.Types (Entity)
import Database.Persist.Schema ()
import Database.Persist.Schema.TH (makeEntitiesMigration)
import Database.Persist.Sql (SqlBackend)
import Text.Email.Validate (EmailAddress)
import Web.Text (HTML, PandocMarkdown)

import Crypto.ActorKey
import Development.PatchMediaType
import Development.PatchMediaType.Persist

import Vervis.FedURI
import Vervis.Migration.TH (schema)
import Vervis.Model.Group
import Vervis.Model.Ident
import Vervis.Model.Role
import Vervis.Model.TH
import Vervis.Model.Ticket
import Vervis.Model.Workflow

-- For migrations 77, 114

import Data.Int

import Database.Persist.JSON
import Network.FedURI
import Web.ActivityPub

makeEntitiesMigration "2019"
    $(modelFile "migrations/2019_02_03_verifkey.model")

makeEntitiesMigration "2019"
    $(modelFile "migrations/2019_03_18_message.model")

makeEntitiesMigration "2019"
    $(modelFile "migrations/2019_03_30_follower_set.model")

makeEntitiesMigration "201905"
    $(modelFile "migrations/2019_05_24.model")

makeEntitiesMigration "201906"
    $(modelFile "migrations/2019_06_02.model")

makeEntitiesMigration "201906"
    $(modelFile "migrations/2019_06_03.model")

makeEntitiesMigration "20190606"
    $(modelFile "migrations/2019_06_06_mig.model")

makeEntitiesMigration "20190607"
    $(modelFile "migrations/2019_06_07.model")

makeEntitiesMigration "20190609"
    $(modelFile "migrations/2019_06_09.model")

makeEntitiesMigration "2019Fill"
    $(modelFile "migrations/2019_06_09_fill.model")

makeEntitiesMigration "20190610"
    $(modelFile "migrations/2019_06_10.model")

makeEntitiesMigration "20190612"
    $(modelFile "migrations/2019_06_12.model")

makeEntitiesMigration "20190615"
    $(modelFile "migrations/2019_06_15.model")

makeEntitiesMigration "20190616"
    $(modelFile "migrations/2019_06_16.model")

makeEntitiesMigration "20190624"
    $(modelFile "migrations/2019_06_24.model")

makeEntitiesMigration "127"
    $(modelFile "migrations/2019_07_11.model")

makeEntitiesMigration "130"
    $(modelFile "migrations/2019_09_06.model")

makeEntitiesMigration "138"
    $(modelFile "migrations/2019_09_10.model")

makeEntitiesMigration "152"
    $(modelFile "migrations/2019_11_04_remote_activity_ident.model")

makeEntitiesMigration "159"
    $(modelFile "migrations/2019_11_05_remote_actor_ident.model")
