{- This file is part of Vervis.
 -
 - Written in 2016, 2018, 2019, 2022, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Data.ObjId
    ( ObjId (..)
    , parseObjId
    , renderObjId
    )
where

import Control.Applicative
import Control.Exception
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Class
import Control.Monad.Trans.Except
import Control.Monad.Trans.Reader
import Data.Bits
import Data.Char
import Data.Either
import Data.Foldable
import Data.List
import Data.List.NonEmpty (NonEmpty (..))
import Data.Maybe
import Data.Map (Map)
import Data.Set (Set)
import Data.Text (Text)
import Data.Time.Format
import Data.Traversable
import Data.Tree
import System.Directory
import System.FilePath
import System.Posix.Files
import System.Process.Typed
import Text.Email.Validate
import Text.Read (readMaybe)
import Text.XML.Light

import qualified Data.Attoparsec.Text as A
import qualified Data.ByteString as B
import qualified Data.ByteString.Base16 as B16
import qualified Data.ByteString.Lazy as BL
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import qualified Data.Text.IO as TIO

import qualified Data.VersionControl as VC

import Control.Monad.Trans.Except.Local

data ObjId = ObjId { unObjId :: B.ByteString } deriving Eq

parseObjId :: Text -> IO ObjId
parseObjId t =
    case B16.decode $ TE.encodeUtf8 t of
        Left e -> error $ "parseObjId: " ++ e
        Right b -> pure $ ObjId b

renderObjId :: ObjId -> Text
renderObjId (ObjId b) =
    either (error . displayException) id $ TE.decodeUtf8' $ B16.encode b
