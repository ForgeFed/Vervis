{- This file is part of Vervis.
 -
 - Written in 2019, 2022, 2024 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

{-# LANGUAGE DeriveGeneric #-}

module Data.VersionControl
    ( Author (..)
    , Commit (..)
    )
where

import Control.Applicative
import Control.Exception
import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Trans.Except
import Crypto.Random
import Data.Aeson
import Data.Bifunctor
import Data.ByteString (ByteString)
import Data.Char
import Data.Graph.Inductive.Graph
import Data.Int
import Data.Maybe
import Data.List.NonEmpty (NonEmpty, nonEmpty)
import Data.Text (Text)
import Data.Time.Clock
import Data.Time.Clock.POSIX
import Data.Time.Format
import Data.Word
import GHC.Generics
import Network.HTTP.Client
import Network.HTTP.Types.Header
import System.Directory
import System.Environment
import System.Exit
import System.FilePath
import System.IO
import Text.Email.Aeson.Instances ()
import Text.Email.Validate
import Text.Read
import Text.XML.Light
import Yesod.Core.Content

import qualified Data.Attoparsec.Text as A
import qualified Data.ByteString as B
import qualified Data.ByteString.Base16 as B16
import qualified Data.DList as D
import qualified Data.List.NonEmpty as NE
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import qualified Data.Text.IO as TIO

import Data.KeyFile
import Network.FedURI

import Control.Monad.Trans.Except.Local
--import Data.DList.Local
import Data.List.NonEmpty.Local

data Author = Author
    { authorName  :: Text
    , authorEmail :: EmailAddress
    }
    deriving Generic

instance FromJSON Author

instance ToJSON Author

data Commit = Commit
    { commitWritten     :: (Author, UTCTime)
    , commitCommitted   :: Maybe (Author, UTCTime)
    , commitHash        :: Text
    , commitTitle       :: Text
    , commitDescription :: Text
    }
    deriving Generic

instance FromJSON Commit

instance ToJSON Commit
