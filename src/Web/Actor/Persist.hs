{- This file is part of Vervis.
 -
 - Written in 2019, 2020, 2022, 2023, 2024
 - by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module Web.Actor.Persist
    ( StageHashids (..)
    , KeyHashid ()
    , keyHashidText

    , encodeKeyHashidPure
    --, getEncodeKeyHashid
    , encodeKeyHashid

    , decodeKeyHashidPure
    --, decodeKeyHashid
    --, decodeKeyHashidF
    --, decodeKeyHashidM
    , decodeKeyHashidE
    )
where

import Prelude hiding (fail)

import Control.Monad.Fail
import Control.Monad.Trans.Except
import Control.Monad.Trans.Maybe
import Data.Text (Text)
import Data.Text.Encoding
import Database.Persist.Class
import Database.Persist.Sql
import Web.Hashids
import Web.PathPieces

import Control.Concurrent.Actor
import Web.Actor
--import Yesod.MonadActor

import Web.Hashids.Local

class StageWeb s => StageHashids s where
    stageHashidsContext :: StageEnv s -> HashidsContext

newtype KeyHashid record = KeyHashid
    { keyHashidText :: Text
    }
    deriving (Eq, Ord, Read, Show)

instance PersistEntity record => PathPiece (KeyHashid record) where
    fromPathPiece t = KeyHashid <$> fromPathPiece t
    toPathPiece (KeyHashid t) = toPathPiece t

encodeKeyHashidPure
    :: ToBackendKey SqlBackend record
    => HashidsContext -> Key record -> KeyHashid record
encodeKeyHashidPure ctx = KeyHashid . decodeUtf8 . encodeInt64 ctx . fromSqlKey

getEncodeKeyHashid
    :: ( MonadActor m
       , StageHashids (MonadActorStage m)
       , ToBackendKey SqlBackend record
       )
    => m (Key record -> KeyHashid record)
getEncodeKeyHashid = do
    ctx <- asksEnv stageHashidsContext
    return $ encodeKeyHashidPure ctx

encodeKeyHashid
    :: ( MonadActor m
       , StageHashids (MonadActorStage m)
       , ToBackendKey SqlBackend record
       )
    => Key record
    -> m (KeyHashid record)
encodeKeyHashid k = do
    enc <- getEncodeKeyHashid
    return $ enc k

decodeKeyHashidPure
    :: ToBackendKey SqlBackend record
    => HashidsContext
    -> KeyHashid record
    -> Maybe (Key record)
decodeKeyHashidPure ctx (KeyHashid t) =
    fmap toSqlKey $ decodeInt64 ctx $ encodeUtf8 t

decodeKeyHashid
    :: ( MonadActor m
       , StageHashids (MonadActorStage m)
       , ToBackendKey SqlBackend record
       )
    => KeyHashid record
    -> m (Maybe (Key record))
decodeKeyHashid khid = do
    ctx <- asksEnv stageHashidsContext
    return $ decodeKeyHashidPure ctx khid

decodeKeyHashidF
    :: ( MonadFail m
       , MonadActor m
       , StageHashids (MonadActorStage m)
       , ToBackendKey SqlBackend record
       )
    => KeyHashid record
    -> String
    -> m (Key record)
decodeKeyHashidF khid e = maybe (fail e) return =<< decodeKeyHashid khid

decodeKeyHashidM
    :: ( MonadActor m
       , StageHashids (MonadActorStage m)
       , ToBackendKey SqlBackend record
       )
    => KeyHashid record
    -> MaybeT m (Key record)
decodeKeyHashidM = MaybeT . decodeKeyHashid

decodeKeyHashidE
    :: ( MonadActor m
       , StageHashids (MonadActorStage m)
       , ToBackendKey SqlBackend record
       )
    => KeyHashid record
    -> e
    -> ExceptT e m (Key record)
decodeKeyHashidE khid e =
    ExceptT $ maybe (Left e) Right <$> decodeKeyHashid khid
