{- This file is part of Vervis.
 -
 - Written in 2022 by fr33domlover <fr33domlover@riseup.net>.
 -
 - ♡ Copying is an act of love. Please copy, reuse and share.
 -
 - The author(s) have dedicated all copyright and related and neighboring
 - rights to this software to the public domain worldwide. This software is
 - distributed without any warranty.
 -
 - You should have received a copy of the CC0 Public Domain Dedication along
 - with this software. If not, see
 - <http://creativecommons.org/publicdomain/zero/1.0/>.
 -}

module System.Process.Typed.Local
    ( runProcessE
    , readProcessE
    )
where

import Control.Monad.Trans.Except
import System.Exit
import System.Process.Typed

import qualified Data.Text as T
import qualified Data.ByteString.Lazy as BL

import qualified Data.Text.UTF8.Local as TU

runProcessE name spec = do
    exitCode <- runProcess spec
    case exitCode of
        ExitFailure n ->
            throwE $
                T.concat
                    [ "`", name, "` failed with exit code "
                    , T.pack (show n)
                    ]
        ExitSuccess -> return ()

readProcessE name spec = do
    (exitCode, out) <- readProcessStdout spec
    case exitCode of
        ExitFailure n ->
            throwE $
                T.concat
                    [ "`", name, "` failed with exit code "
                    , T.pack (show n)
                    ]
        ExitSuccess -> return $ TU.decodeStrict $ BL.toStrict out
