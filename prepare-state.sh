#!/bin/sh

mkdir state state/repos state/deliveries
ssh-keygen -t rsa -m PEM -N '' -f state/ssh-host-key
sudo chown -R 981:981 state
