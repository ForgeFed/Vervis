#!/bin/sh

VERVIS='https://vervis.peers.community/repos'

DEPS="6r4Ao   ssh"

mkdir -p lib
cd lib
echo $DEPS | while read -r dep
do
    slug=$(echo $dep | cut --fields=1 --delimiter=' ')
    name=$(echo $dep | cut --fields=2 --delimiter=' ')

    echo
    if [ -d "$name" ]; then
        echo "Updating dependency $name"
        darcs pull --repodir="$name" --all
    else
        echo "Cloning dependency '$name' from $VERVIS/$slug"
        darcs clone "$VERVIS/$slug" "$name"
    fi
done
