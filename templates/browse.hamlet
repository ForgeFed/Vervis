$# This file is part of Vervis.
$#
$# Written in 2016, 2018, 2019, 2022, 2023, 2024
$# by fr33domlover <fr33domlover@riseup.net>.
$#
$# ♡ Copying is an act of love. Please copy, reuse and share.
$#
$# The author(s) have dedicated all copyright and related and neighboring
$# rights to this software to the public domain worldwide. This software is
$# distributed without any warranty.
$#
$# You should have received a copy of the CC0 Public Domain Dedication along
$# with this software. If not, see
$# <http://creativecommons.org/publicdomain/zero/1.0/>.

<p>
  Vervis is a work-in-progress federated project and repository hosting and
  collaboration platform. Its development is meant to help form the ForgeFed
  specification for project hosting platform federation, and hopefully get
  existing platforms (such as Forgejo, Gitea, GitLab CE, etc.) to support it.

<p>
  Vervis is meant to be just a backend. We're working on the
  <a href="https://codeberg.org/Anvil/Anvil">
    Anvil
  frontend application.

<p>
  Vervis is a web application written in the Haskell programming language and
  the Yesod web framework. It's free as in freedom, under AGPLv3. It's being
  developed primarily by Pere (he/him), who can be found in the
  <a href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">
    Forge Federation Matrix room
  and on the
  <a href="https://micro.towards.vision/@pere">
    Fediverse.

<p>
  Vervis currently supports Git and Darcs repositories.

<p>
  Contributions very welcome!

<ul>
  <li>
    Vervis web app
    <a href="https://vervis.peers.community/repos/rjQ3E">
      Git repository
    with the source code
  <li>
    <a href="https://codeberg.org/ForgeFed/Vervis">
      Mirror on Codeberg
$#  <li>
$#    IRC:
$#    <a href="https://web.libera.chat/#forgefed">
$#      #forgefed @ Libera Chat
  <li>
    Matrix:
    <a href="https://matrix.to/#/#general-forgefed:matrix.batsense.net">
      #general-forgefed:matrix.batsense.net
  <li>
    ForgeFed
    <a href="https://forgefed.org">website

<p>
  This project is funded through
  <a href="https://nlnet.nl/entrust">NGI0 Entrust</a>, a fund established by
  <a href="https://nlnet.nl">NLnet</a> with financial support from the European
  Commission's <a href="https://ngi.eu">Next Generation Internet</a> program.
  Learn more at the
  <a href="https://nlnet.nl/project/ForgeFed-RPC">NLnet project page</a>.

<h2>People

<ul>
  $forall (Entity personID person, Entity _ actor) <- people
    <li>
      $if canCreateFactories person
        <span>👑
      <a href=@{PersonR $ hashPerson personID}>
        ~#{username2text $ personUsername person} #{actorName actor}

<h2>Teams

<ul>
  $forall (Entity groupID _, Entity _ actor, parents) <- groups
    <li>
      <a href=@{GroupR $ hashGroup groupID}>
        &#{keyHashidText $ hashGroup groupID} #{actorName actor}
      <ul>
        $if not $ null parents
          <li>Parents
            <ul>
              $forall p <- parents
                <li>
                  ^{groupLinkFedW p}

<h2>Repos

<ul>
  $forall (Entity repoID _, Entity _ actor) <- repos
    <li>
      <a href=@{RepoR $ hashRepo repoID}>
        ^#{keyHashidText $ hashRepo repoID} #{actorName actor}

<h2>Ticket Trackers

<ul>
  $forall (Entity deckID _, Entity _ actor) <- decks
    <li>
      <a href=@{DeckR $ hashDeck deckID}>
        =#{keyHashidText $ hashDeck deckID} #{actorName actor}

<h2>Merge Request Trackers

<ul>
  $forall (Entity loomID _, Entity _ actor) <- looms
    <li>
      <a href=@{LoomR $ hashLoom loomID}>
        +#{keyHashidText $ hashLoom loomID} #{actorName actor}

<h2>Projects

<ul>
  $forall (Entity projectID _, Entity _ actor, components, children) <- projects
    <li>
      <a href=@{ProjectR $ hashProject projectID}>
        \$#{keyHashidText $ hashProject projectID} #{actorName actor}
      <ul>
        $if not $ null components
          <li>Components
            <ul>
              $forall c <- components
                <li>
                  ^{componentLinkFedW c}
        $if not $ null children
          <li>Children
            <ul>
              $forall c <- children
                <li>
                  ^{projectLinkFedW c}
