$# This file is part of Vervis.
$#
$# Written in 2016, 2019, 2022, 2023, 2024
$# by fr33domlover <fr33domlover@riseup.net>.
$#
$# ♡ Copying is an act of love. Please copy, reuse and share.
$#
$# The author(s) have dedicated all copyright and related and neighboring
$# rights to this software to the public domain worldwide. This software is
$# distributed without any warranty.
$#
$# You should have received a copy of the CC0 Public Domain Dedication along
$# with this software. If not, see
$# <http://creativecommons.org/publicdomain/zero/1.0/>.

^{projectNavW (Entity projectID project) actor}

<h2>Components

<table>
  <tr>
    <th>Role
    <th>Component
    <th>Since
    $if haveAdmin
      <th>Remove
  $forall (comp, role, since, compID) <- comps
    <tr>
      <td>#{show role}
      <td>^{componentLinkFedW comp}
      <td>#{showDate since}
      $if haveAdmin
        <td>^{buttonW POST "Remove" (ProjectRemoveComponentR projectHash compID)}

$if haveAdmin
  <p>Invite a component:
  <form method=POST action=@{ProjectInviteCompR projectHash} enctype=#{enctypeIC}>
    ^{widgetIC}
    <input type="submit">

<h2>Component requests in progress

<table>
  <tr>
    <th>Inviter
    <th>Via
    <th>Invited component
    <th>Component accepted?
    <th>Role
    <th>Time
    $if haveAdmin
      <th>Approve
  $forall (inviter, us, component, accept, time, role, componentID) <- drafts
    <tr>
      <td>^{actorLinkFedW inviter}
      <td>
        $if us
          Us
        $else
          Them
      <td>^{componentLinkFedW component}
      <td>
        $if accept
          [x]
        $else
          [_]
      <td>#{show role}
      <td>#{showDate time}
      $if haveAdmin && (accept && not us)
        <td>^{buttonW POST "Approve" (ProjectApproveComponentR projectHash componentID)}
