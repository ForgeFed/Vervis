$# This file is part of Vervis.
$#
$# Written in 2016, 2019, 2022, 2023, 2024
$# by fr33domlover <fr33domlover@riseup.net>.
$#
$# ♡ Copying is an act of love. Please copy, reuse and share.
$#
$# The author(s) have dedicated all copyright and related and neighboring
$# rights to this software to the public domain worldwide. This software is
$# distributed without any warranty.
$#
$# You should have received a copy of the CC0 Public Domain Dedication along
$# with this software. If not, see
$# <http://creativecommons.org/publicdomain/zero/1.0/>.

^{deckNavW (Entity deckID deck) actor}

<h2>Projects

<table>
  <tr>
    <th>Role
    <th>Project
    <th>Since
    $if haveAdmin
      <th>Remove
  $forall (project, role, since, stemID) <- stems
    <tr>
      <td>#{show role}
      <td>^{projectLinkFedW project}
      <td>#{showDate since}
      $if haveAdmin
        <td>^{buttonW POST "Remove" (DeckRemoveProjectR deckHash stemID)}

$if haveAdmin
  <p>Add deck to a project:
  <form method=POST action=@{DeckAddProjectR deckHash} enctype=#{enctypeAP}>
    ^{widgetAP}
    <input type="submit">

<h2>Invites

<table>
  <tr>
    <th>Inviter
    <th>Via
    <th>Invited project
    <th>I accepted?
    <th>Role
    <th>Time
    $if haveAdmin
      <th>Approve
  $forall (inviter, us, project, accept, time, role, stemID) <- drafts
    <tr>
      <td>^{actorLinkFedW inviter}
      <td>
        $if us
          Us
        $else
          Them
      <td>^{projectLinkFedW project}
      <td>
        $if accept
          [x]
        $else
          [_]
      <td>#{show role}
      <td>#{showDate time}
      $if haveAdmin && (not accept && not us)
        <td>^{buttonW POST "Approve" (DeckApproveProjectR deckHash stemID)}
