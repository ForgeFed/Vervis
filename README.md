Vervis is a **free, open-source federated forge backend server**:

- A platform for hosting and collaboration on software projects
- A reference implementation of the [ForgeFed][] forge federation protocol,
  which is an extension of ActivityPub, the protocol that drives the Fediverse
- A decentralized network in which people on one server can seamlessly
  collaborate with people from another server, including non-Vervis software
  that implements ForgeFed
- Vervis is still in **pre-alpha phase** and not ready for production use!
- Provides a temporary static UI, to be replaced by the [Anvil][] frontend
  client

## Navigation

- [Project on Codeberg][Repo]
- Support the development via [Liberapay][] or [OpenCollective][]
- [Official Docker image][Image]
- Demo instances:
  - [Fig][]
  - [Grape][]
  - [Walnut][]

## Features

The current focus is the classic core features for collaboration:

- Repositories
  - Git
  - Darcs
- Issue tracking
- Pull requests
- And of course federation! Including, eventually, with other forge projects
  working on implementing federation, such as [Forgejo][] and [GitLab][]

## SSH Client Config

Vervis comes with its own SSH server, which usually requires some configuration
to be set in your SSH client, if you'd like to be able to push and pull repos
via SSH.

Add the following to your `~/.ssh/config` file (or create it if it doesn't
exist), replacing the Host with your instance's domain:

```
Host fig.fr33domlover.site
    KexAlgorithms +diffie-hellman-group1-sha1
    ControlMaster no
    ForwardAgent no
    ForwardX11 no
    Ciphers +aes256-cbc
    MACs +hmac-sha1
    PubkeyAcceptedKeyTypes +ssh-rsa
    HostKeyAlgorithms +ssh-rsa
```

Cloning repositories works over (unauthenticated) HTTPS and (authenticated)
SSH.

**HTTPS clone:**

    $ git clone https://vervis.peers.community/repos/rjQ3E vervis

**SSH clone and push:**

If your system username and Vervis username are identical:

    $ git clone vervis.peers.community:rjQ3E vervis

If usernames are different:

    $ git clone luke@vervis.peers.community:rjQ3E vervis

## Deployment

### Tech stack

- **Haskell** programming language
- **Yesod** web framework
- **PostgreSQL** database for most data
- Some data in **SQLite** databases
- The work-in-progress [Anvil][] frontend uses:
  - **Svelte**
  - **Skeleton**
  - **Tauri**

### Requirements

- **PostgreSQL** 9.5+

### Docker

The repository includes deployment configurations for **Docker and
docker-compose**.

1. Install [Docker][]
2. In `create-db.sql`, update the `vervis` DB user's password
3. Create and edit `config/settings.yml` based on
   `config/settings-sample-prod.yml`, remember to set the same DB password here
4. In `docker-compose.yml`, update the database superuser password (it's the
   password for the `postgres` superuser, not `vervis`)
5. Create initial state: `./prepare-state.sh`
6. Ready for launch! `docker-compose up -d`

```shell
sudo apt install docker-compose docker.io
vim create-db.sql
cp config/settings-sample-prod.yml config/settings.yml
vim config/settings.yml
vim docker-compose.yml
./prepare-state.sh
docker-compose up -d
```

### Manual Installation

For manual/custom installation steps, see [INSTALL.md](INSTALL.md).

## Development

To prepare a development environment, follow the instructions in
[INSTALL.md](INSTALL.md).

## Federation & API

See `FEDERATION.md` & `API.md`.

## Contributing

Vervis is **free, open-source software** licensed under **AGPLv3**.

You can open issues for bugs you've found or features you think are missing.
You can also submit pull requests to this repository.

**Matrix room**: [#general-forgefed:matrix.batsense.net][Matrix]

### Finding your way in the codebase

Folders:

- `app`:
  *main* functions of the web app, for regular build and for development modes
- `config`:
  Settings the app reads when it launches, some defined by a human and some
  auto-generated
- `.git`:
  Internal version control data of this repo
- `embed`:
  Files embedded into the app source code at build time (using TH)
- `hook-darcs` & `hook-git`:
  *main* functions for VCS hook programs to be inserted into every repo hosted
  by the app
- `migrations`:
  Helper files for DB migrations: New entities added & model files for
  migrations involving data manipulation
- `src`:
  Web app Haskell source code, built as a library whose modules are used by the
  various executables
- `static`:
  Static files (such as CSS and fonts) served by the web app (no such files at
  the moment)
- `templates`:
  UI page and widget templates (`.hamlet` for HTML, `.cassius` for CSS)
- `test`:
  No tests have been written yet
- `th`:
  Domain-specific language snippets, from which code is generated using TH

Files:

- `.gitignore`
- `th/routes`
- `th/models`
- `update-deps.sh`
- `bindist.sh`

Haskell modules in `src`:

- Non-`Vervis` modules:
  - Modules ending with `Local`:
  - The rest:
- `Vervis.Data`
- `Vervis.Persist`
- `Vervis.Fetch`
- `Vervis.Serve`
- `Vervis.Query`
- `Vervis.Web`
- Primary web app support modules:
  - `Vervis.Foundation`
  - `Vervis.Application`
  - `Vervis.Settings`
  - `Vervis.Model`
  - `Vervis.Migration`
- Primary web app logic modules:
  - `Vervis.Actor`
  - `Vervis.Handler`
  - `Vervis.Client`
- `Vervis.Ssh`

### Testing the production image

When Vervis is built in development mode (the flag is set in `stack.yml` and
can be overriden when running `stack build`), it works with plain HTTP, but in
production mode (e.g. when using the official prebuilt image), it requires
HTTPS, a domain, and the standard HTTP(S) ports.

Instructions for testing a production image, assuming we'll use `dev.example`
as the server's domain name:

1. The usual `./prepare-state.sh` and
   `cp config/settings-sample-prod.yml config/settings.yml`
2. On the host system, edit `/etc/hosts`, adding the line
  `127.0.0.1 dev.example`
3. In `config/settings.yml`, set the domain to `dev.example`
4. Run `./create-self-cert.sh` to generate a self-signed certificate for
   `dev.example`
5. In `docker-compose.yml`, uncomment the `nginx` service
6. In `nginx.conf`,
    - Update the domain to `dev.example`
    - Uncomment the self-signed cert paths
7. `docker-compose up`

## License

AGPLv3+. See `COPYING`.

## Funding

This project is funded through the
[NGI Zero Entrust Fund](https://nlnet.nl/entrust), a fund established by
[NLnet](https://nlnet.nl) with financial support from the European Commission's
[Next Generation Internet](https://ngi.eu) program. Learn more at the
[NLnet project page](https://nlnet.nl/project/ForgeFed-RPC).

[<img src="https://nlnet.nl/logo/banner.png" alt="NLnet foundation logo" width="20%" />](https://nlnet.nl)
[<img src="https://nlnet.nl/image/logos/NGI0Entrust_tag.svg" alt="NGI Zero Entrust Logo" width="20%" />](https://nlnet.nl/entrust)

[Anvil]: https://codeberg.org/Anvil/Anvil
[Docker]: https://docs.docker.com
[ForgeFed]: https://forgefed.org
[Forgejo]: https://forgejo.org
[GitLab]: https://gitlab.com
[Image]: https://codeberg.org/ForgeFed/Vervis/packages
[Liberapay]: https://liberapay.com/ForgeFed
[Matrix]: https://matrix.to/#/#general-forgefed:matrix.batsense.net
[OpenCollective]: https://opencollective.com/forgefed
[Repo]: https://codeberg.org/ForgeFed/Vervis
[fig]: https://fig.fr33domlover.site
[grape]: https://grape.fr33domlover.site
[walnut]: https://walnut.fr33domlover.site
