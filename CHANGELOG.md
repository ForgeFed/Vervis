This file lists the user-visible interesting changes between releases. For a
full list of changes to the source, see `darcs log`.



vervis 0.2        unreleased
============================

* ...

vervis 0.1        2024-10-20
============================

* First release!
* Official Docker image now exists
* Still pre-alpha! Not ready for reliable production use
